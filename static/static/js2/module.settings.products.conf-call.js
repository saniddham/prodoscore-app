
var moduleSettingsProductConfCall = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		eventLogger('start', 'confCallSettings');
		var formContainer = new scrollOpenClose(context.q('div.one-row'));
		var form = context.q('form')[0];
		var bridges = {},
			dropdowns = [];
		var employeedata = context.q('table.employeedata tbody')[0];
		sorttable.makeSortable(employeedata.parentNode);

		//	---------------------------------------------------------------------------------------------------------
		//	Save Conf call settings
		//	---------------------------------------------------------------------------------------------------------
		form.onsubmit = function () {
			if (form.account_id.value.trim() == '') {
				showAlert('error', 'Please enter the Account ID');
				form.account_id.focus();
			}
			else if (form.admin.value.trim() == '') {
				showAlert('error', 'Please enter the Admin Email');
				form.admin.focus();
			}
			else if (form.password.value.trim() == ''){
			    showAlert('error', 'Please enter the Password');
			    form.password.focus();
			}
			else {
				eventLogger('start', 'saveConfCallSettings');
				new arc.ajax(base_url + 'turbobridge-settings/', {
					method: 'POST',
					data: {
						//system: form.system.value,
						partner: form.partner_id.value,
						account: form.account_id.value,
						admin: form.admin.value,
						password: form.password.value
					},
					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						if (data.result == 'success'){
							showAlert('success', 'Conference Call settings are saved.');
							form.q('input.button.red')[0].removeAttribute('disabled');
							moduleSettingsProductConfCall.onload(context, params);
						}
						else{
							showAlert('error', 'Invalid Credentials.');
							eventLogger('end', 'saveConfCallSettings');
						}

					}
				});
			}
			return false;
		}
		//	---------------------------------------------------------------------------------------------------------
		//	Disconnect
		//	---------------------------------------------------------------------------------------------------------
		form.q('input.button.red')[0].onclick = function () {
			new arc.ajax(base_url + 'disconnect-products/?product=turbobridge', {
				callback: function (data){
					formContainer.hide();
					moduleSettingsProductConfCall.onload(context, params);
					showAlert('success', 'Disconnected successfully.');
				}
			});
		}
		//	---------------------------------------------------------------------------------------------------------
		//	Cancel credential form
		//	---------------------------------------------------------------------------------------------------------
		form.q('input.button.cancel.gray')[0].onclick = function () {
			formContainer.hide();
			return false;
		}
		//	---------------------------------------------------------------------------------------------------------
		//	Save mapping bridges to employees
		//	---------------------------------------------------------------------------------------------------------
		var form2 = context.q('form')[1];
		form2.onsubmit = function () {
			var selBridges = form2.q('tr.tablerow[data-id]'),
				bridge;
			var uploadData = {};
			for (var i = 0; i < selBridges.length; i++) {
				bridge = selBridges[i].q('select[data-value]')[0];
				if (bridge.className != '')
					uploadData[selBridges[i].getAttribute('data-id')] = bridge.value;
			}
			eventLogger('start', 'saveConfCallBridgeSettings');
			new arc.ajax(base_url + 'turbobridge-settings/', {
				method: 'POST',
				data: {
					bridges: uploadData
				},
				callback: function (data) {
					eventLogger('end', 'saveConfCallBridgeSettings');
					for (var i = 0; i < selBridges.length; i++) {
						bridge = selBridges[i].q('select[data-value]')[0];
						bridge.className = '';
						bridge.setAttribute('data-value', bridge.value);
					}
					showAlert('success', 'Conference Call bridges are assigned.');
					scrollToTop();
				}
			});
			return false;
		}

		//	---------------------------------------------------------------------------------------------------------
		//	Reset mappings
		//	---------------------------------------------------------------------------------------------------------
		form2.q('input.button.cancel')[0].onclick = function () {
			var selects = form2.q('select[data-value]');
			for (var i = 0; i < selects.length; i++) {
				selects[i].value = selects[i].getAttribute('data-value');
				selects[i].className = '';
				selects[i].parentNode.setAttribute('sorttable_customkey', bridges[selects[i].getAttribute('data-value')]);
			}
			scrollToTop();
		}
		//	---------------------------------------------------------------------------------------------------------
		//	Load Conf call details
		//	---------------------------------------------------------------------------------------------------------
		hideSaveCancelButtons(context);
		new arc.ajax(base_url + 'turbobridge-settings/', {
			callback: function (data) {
				eventLogger('end', 'confCallSettings');
				data = eval('(' + data.responseText + ')');
				//form.system.value = data.settings.system;
				form.partner_id.value = data.settings.partner;
				form.account_id.value = data.settings.account;
				form.admin.value = data.settings.admin;

				if ( data.settings.account !== "" || data.settings.admin !== "") {
					form.password.value = '[ENCRYPTED]';
				}

				var formFilled = new checkFilled(form);
				//
				employeedata.innerHTML = '<tr class="no-records"><td colspan="2"><i>No records to display</i></td></tr>';
				if (typeof data.bridges.error != 'undefined') {
					form.q('input.button.red')[0].removeAttribute('disabled');
					if (data.bridges.error == 'credentials-not-set') {
						// showAlert('warning', 'Please enter Conference Call admin email and password');
						form.q('input.button.red')[0].setAttribute('disabled', true);
					}
					else if (data.bridges.error == 'credentials-wrong') {
						showAlert('error', 'The admin email and password you have entered is wrong');
					}
					else {
						showAlert('error', data.bridges.error.message);
					}
				}
				else if (data.bridges.length > 0) {
					formContainer.hide();
					showSaveCancelButtons(context);
					bridges = {}, dropdowns = [];
					for (var i = 0; i < data.bridges.length; i++)
						bridges[data.bridges[i].conferenceID] = (data.bridges[i].name == '' ? 'Unnamed - ' : data.bridges[i].name + ' - ') + data.bridges[i].conferenceID + ' - ' + (new Date(data.bridges[i].createdDate * 1000)).toString().substring(4, 15); // ['+data.bridges[i].tollNumber+']
					//
					employeedata.innerHTML = '';
					var dropdown;
					for (var i in data.employees) {
						var icon = usernameIcon(data.employees[i].fullname);
						dropdown = makeDropdown(bridges, data.employees[i].conf_id,
							function (sel) {
								enforceSingleAssignment(dropdowns);
							}, 'null')
						dropdowns.push(dropdown);
						var employee = arc.reactor({
							tr: {
								class: 'tablerow',
								'data-id': i,
								content: [
									{
										td: {
											content: {
												div: {
													class: 'user-name-wrap',
													content: {
														p: {
															class: 'user-name',
															content: data.employees[i].fullname
														}
													}
												}
											},
											style: 'border-left:2px solid #' + icon[1]
										}
									},
									{
										td: {
											content: roles[data.employees[i].role]
										}
									},
									{
										td: {
											content: dropdown,
											'sorttable_customkey': bridges[data.employees[i].conf_id]
										}
									},
									]
							}
						});
						employeedata.appendChild(employee);
					}
					//
					sortColumn(employeedata.parentNode, 0);
					enforceSingleAssignment(dropdowns);
				}
				//	Not Else-If : If there were any error or no data
				if (typeof data.bridges.error != 'undefined' || data.bridges.length == 0)
					setTimeout(function () {
						formContainer.show();
					}, 250);
				//
			}
		});
		// -----------------------
		form.elements[form.elements.length - 1].onclick = function () {
			formContainer.hide();
			return false;
		}
		context.q('input[type="button"]')[0].onclick = function () {
			formContainer.show();
			return false;
		}
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		if (typeof loaded == 'function')
			loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};