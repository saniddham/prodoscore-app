
var moduleSettingsOrganization = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		hidePrevAlerts();
		var settingsForm = context.q('form')[0];//employeeSettings
		var buttons = settingsForm.q('button.primary, button.gray');
		var weekdays = settingsForm.q('button.btngroup.btn');

		var preVals = ["09:00", "17:00"];

		function setFullDay () {
			settingsForm.start_time.value = '00:00';
			settingsForm.end_time.value = '23:59';

			settingsForm.start_time.setAttribute("disabled", true);
			settingsForm.end_time.setAttribute("disabled", true);
		}

		function unsetFullDay () {
			settingsForm.start_time.removeAttribute("disabled");
			settingsForm.end_time.removeAttribute("disabled");
			settingsForm.start_time.value = preVals[0];
			settingsForm.end_time.value = preVals[1];
		}

		settingsForm.full_day.onchange = function(){
			if (this.checked) {
				setFullDay();
			} else {
				unsetFullDay();
			}
		};
		//
		settingsForm.start_time.onchange = function (e) {
			preVals[0] = e.target.value;
		};
		//
		settingsForm.end_time.onchange = function (e) {
			preVals[1] = e.target.value;
		};
		//
		new function(){
			if (typeof organizationSettings == 'undefined'){
				setTimeout(arguments.callee, 10);
				return false;
			}
			new arc.ajax(base_url+'timezone/', {
				method: 'GET',
				callback: function(data){
					eventLogger('end', 'timeSettings');
					var region = context.q('form #region')[0], timezone = context.q('form #timezone')[0], tmp, tmpi;
					region.innerHTML = '';
					timezone.innerHTML = '';
					data = eval('('+data.responseText+')');

					for (var i = 0; i < data.length; i++){
						tmp = arc.elem('option', data[i][0], {value: i});

						if ((typeof organizationSettings['timezone'].split('/')[1] === 'undefined' && data[i][0] === 'Other')	|| data[i][0] == organizationSettings['timezone'].split('/')[0] ) {
							tmp.selected = true;
							for (var j = 0; j < data[i][1].length; j++){
								tmpi = arc.elem('option', data[i][1][j][1], {value: data[i][1][j][0]});
								if (data[i][1][j][0] == organizationSettings['timezone']) {
									tmpi.selected = true;
								}
								timezone.appendChild(tmpi);
							}
						}
						region.appendChild(tmp);
					}

					region.onchange = function(){
						timezone.innerHTML = '';
						for (var i = 0; i < data[this.value][1].length; i++){
							tmp = arc.elem('option', data[this.value][1][i][1], {value: data[this.value][1][i][0]});
							if (data[this.value][1][i][0] == organizationSettings['timezone'])//.substring(organizationSettings['timezone'].indexOf('/')+1)
								tmp.selected = true;
							timezone.appendChild(tmp);
						}
					}
					/*for (var i in data){
						tmp = arc.elem('option', i, {value: i});
						if (i == organizationSettings['timezone'].split('/')[0]){
							tmp.selected = true;
							for (var j in data[i]){
								tmpi = arc.elem('option', data[i][j], {value: j});
								if (j == organizationSettings['timezone'].substring(organizationSettings['timezone'].indexOf('/')+1))
									tmpi.selected = true;
								timezone.appendChild(tmpi);
							}
						}
						region.appendChild(tmp);
					}
					region.onchange = function(){
						timezone.innerHTML = '';
						for (var i in data[this.value]){
							tmp = arc.elem('option', data[this.value][i], {value: i});
							if (data[this.value][i] == organizationSettings['timezone'].substring(organizationSettings['timezone'].indexOf('/')+1))
								tmp.selected = true;
							timezone.appendChild(tmp);
						}
					}*/
				}
			});
			for (var i = 0; i < weekdays.length; i++){
				weekdays[i].onclick = function(i){
					var btn = this;
					return function(){
						if (weekdays[i].className.indexOf('active') == -1)
							weekdays[i].addClass('active');
						else
							weekdays[i].removeClass('active');
						return false;
					}
				}(i);
				if (organizationSettings['workingdays'].indexOf(i) > -1)
					weekdays[i].addClass('active');
			}
			settingsForm.start_time.value = preVals[0] = organizationSettings['daystart'];
			settingsForm.end_time.value = preVals[1] = organizationSettings['dayend'];
			settingsForm.timezone.value = organizationSettings['timezone'];
			settingsForm.show_details.checked = organizationSettings['show_details'] == 1;

			if (settingsForm.start_time.value == '00:00' && settingsForm.end_time.value == '23:59') {
				settingsForm.full_day.checked = true;
				setFullDay();
			}
			else{
				settingsForm.full_day.checked = false;
				unsetFullDay();
			}
		}();
		//
		settingsForm.onsubmit = buttons[0].onclick = function(){
			var workingdays = [];
			for (var i = 0; i < weekdays.length; i++)
				if (weekdays[i].className.indexOf('active') > -1)
					workingdays.push(i);
			//
			//	Validate form data
			if (settingsForm.start_time.value == ''){
				showAlert('error', 'Please select work hours start time.');
				return false;
			}
			else if (settingsForm.end_time.value == ''){
				showAlert('error', 'Please select work hours end time.');
				return false;
			}
			else if (settingsForm.start_time.value >= settingsForm.end_time.value) {
				showAlert('error', 'Change the start time to be before the end time.');
				return false;
			}
			else if (workingdays.length == 0){
				showAlert('error', 'You have to select at least one working day.');
				return false;
			}
			else if (workingdays.length < 3){
				if (!confirm('Are you sure you want to proceed with less than 3 working days.?'))
					return false;
			}
			//
			eventLogger('start', 'orgSettings');
			new arc.ajax(base_url+'update-organization/', {
					method: 'POST',
					data: {
						workingdays: workingdays,
						daystart: settingsForm.start_time.value,
						dayend: settingsForm.end_time.value,
						timezone: settingsForm.timezone.value,
						show_details: settingsForm.show_details.checked ? 1 : 0
					},
					callback: function(data){
						eventLogger('end', 'orgSettings');
						organizationSettings['workingdays'] = workingdays;
						organizationSettings['daystart'] = settingsForm.start_time.value;
						organizationSettings['dayend'] = settingsForm.end_time.value;
						organizationSettings['timezone'] = settingsForm.timezone.value;
						organizationSettings['show_details'] = settingsForm.show_details.checked ? 1 : 0;
						if (settingsForm.start_time.value == '00:00' && settingsForm.end_time.value == '23:59') {
							settingsForm.full_day.checked = true;
							setFullDay();
						} else {
							settingsForm.full_day.checked = false;
							unsetFullDay();
						}
						scrollToTop();
						showAlert('success', 'Organization settings are saved');
					},
					fallback: function(data){
						scrollToTop();
						showAlert('error', 'Organization settings could not be saved');
						eventLogger('end', 'orgSettings');
					}
				});
			return false;
		}
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};