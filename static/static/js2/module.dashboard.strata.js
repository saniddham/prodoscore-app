
module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		//	If dashboard not already loaded, redirect to dashboard.
		var strataHeadings = contentMain.q('h3.heading');
		if (strataHeadings.length == 0){
			setTimeout(function(){document.location = '#dashboard';}, 100);
			return false;
		}
		//
		//	Breadcrumbs
		var currentText = '';
		if (params[0] == 'above') {
			currentText = 'Prodoscore above average score';
		} else if (params[0] == 'below') {
			currentText = 'Prodoscore below average score';
		} else if (params[0] == 'baseline') {
			currentText = 'Prodoscore within average score';
		}
		breadCrumbViewModel();
		breadCrumbViewModel.reset();
		breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
		breadCrumbViewModel.addCrumb('#dashboard/strata/' + params[0], currentText, true);
		breadCrumbViewModel.render(context);
		//
		//	Initialize
		params = ['above', 'baseline', 'below'].indexOf(params[0]);
		var employees = context.q('ul.employee-list.strata')[0];
		employees.innerHTML = '';
		contentMain.style.opacity = 0;
		//
		doFetchData('strata',
			function(data, self, startD, endD){
				//	-----------------------------------------------------------------------------------------------
				//	Render Title Lines
				//	-----------------------------------------------------------------------------------------------
				let description = strataHeadings[params].innerHTML.replace('Productivity is ', '');
				let headings = context.q('h2.card-heading span');
				headings[0].innerHTML = (description[0] == 'a' ? 'n ' : ' ') + description.toLowerCase();
				headings[1].innerHTML = description.capitalizeFirstLetter();
				//	-----------------------------------------------------------------------------------------------
				//	Fill Employees List
				//	-----------------------------------------------------------------------------------------------
				employees.innerHTML = '';
				for (let i in data.employees){
					if (data.employees[i].status > 0 && data.employees[i].role > 0){
						let icon = usernameIcon(data.employees[i].fullname);
						let className = strataStr(data.employees[i].strata);
						if (data.employees[i].strata == 2-params){
							let employee = arc.reactor(
								{li: {content:
									{a: {'data-id': i,
										'data-role': data.employees[i].role,
										href: '#employee/'+i+'/'+data.employees[i].email,
										style: 'border-left:2px solid #'+icon[1],
										content: [
											{
												div: {class: 'user-name-wrap',
													content: {p: {class: 'user-name', content: data.employees[i].fullname}}
												}
											},
											{
												div: {class: 'right-holder',
													content: [
														{p: {class: 'score '+className[0], content: data.employees[i].scr.l.toFixed(0)}},
														{p: {class: 'text '+className[0], content: className[1]}}
													]
												}
											}
										]
									}}
								}}
							);
							employees.a(employee);
						}
					}
				}
				//	-----------------------------------------------------------------------------------------------
				//	Prepare Chart Data
				//	-----------------------------------------------------------------------------------------------
				var graphData = [['Date', '# Employees']];
				for (let i  = startD; i < endD+1; i++){
					let date = DateFromShort(i);
					if (isWorkingDay(date.getDay()) && typeof data.days[i] != 'undefined'){
						graphData.push([date, data.days[i].strata[2-params].length]);
					}
					else
						graphData.push([date]);
				}
				//	-----------------------------------------------------------------------------------------------
				//	Draw the Chart
				//	-----------------------------------------------------------------------------------------------
				drawChartStrata(context.q('#chart_strata')[0], graphData, ['#1e86d9', '#494949', '#f1433c'][params],
					function(row, tooltip, e){
						let tmp = data.days[DateToShort(row[0])].strata[2-params];
						let employees = [],
								limit = 10,
								extra = 0;
						for (let k = 0, l=0; k < tmp.length; k++) {
							if (typeof data.employees[tmp[k]] != 'undefined' && data.employees[tmp[k]].status > 0 && data.employees[tmp[k]].role > 0){
								if (l >= limit) {
									extra++;
								} else {
									let icon = usernameIcon(data.employees[tmp[k]].fullname);
									employees.push('<li><a style="border-left:2px solid #'+icon[1]+';">'+data.employees[tmp[k]].fullname+'</a></li>');
								}
								l++;
							}
						}
						if ( extra > 0 ) {
							employees.push('<li><a>+'+extra+' more</a></li>');
						}
						tooltip.innerHTML = '<h2 class="card-heading">'+row[0].toString().substring(0, 15)+'<br/>'+tmp.length+' Employee'+(tmp.length == 1 ? '' : 's')+'</h2><ul class="employee-list tooltip">'+employees.join('')+'</ul>';
					}
				);
				//	-----------------------------------------------------------------------------------------------
			}
		);
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		setTimeout(loaded, 550);
		//	-----------------------------------------------------------------------------------------------
	},
	//	-----------------------------------------------------------------------------------------------
	/*onunload: function(){
		if (document.location.hash == '#dashboard')
			contentMain.style.opacity = 1;
	}*/
	//	-----------------------------------------------------------------------------------------------
};

function drawChartStrata(context, chartData, color, columnHover){
	context.innerHTML = loadingIndicator;
	new ArcChart(context, chartData, [color],
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnHover: columnHover,
			tooltipPosition: 'to-right',
		}).draw('column');
}
