
let billingPlans = {
	'-1': ['Suspended', 	[0, 0, 0, 0]],
	0: ['Trial', 		[0, 0, 0, 0]],
	1: ['Basic', 		[1, 1, 0, 0]],
	2: ['Business', 	[1, 1, 1, 0]],
	3: ['Enterprise', 	[1, 1, 1, 1]],
	4: ['Custom', 		[1, 1, 1, 1]]
};

module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		hidePrevAlerts();
		eventLogger('start', 'billing');
		//
		let selPlan = context.q('#selPlan')[0];
		let plan = billingPlans[ organizationSettings.subscription.type ];
		selPlan.value = plan[0];
		//
		let selVolume = context.q('#selVolume')[0];
		selVolume.value = organizationSettings.subscription.volume == Infinity ? 'Unlimited' : organizationSettings.subscription.volume + ' Users';
		//
		let enabledFeatures = [(app.q('#crmSettings').length > 0), (app.q('#turboBridgeSettings').length > 0 || app.q('#broadSoftSettings').length > 0), organizationSettings.subscription.features.nlp, (app.q('#scoreSettings').length > 0)];
		let features = context.q('button.btngroup.btn');
		for (let i = 0; i < 4; i++){
			if (enabledFeatures[i] == 1){
				features[i].className = 'btngroup btn active';//plan[1][i]
				features[i].title = 'Activated';
			}
		}
		//
		new arc.ajax(base_url + 'update-employee/', {
			callback: function (resp) {
				data = eval('(' + resp.responseText + ')');
				//console.log(data);
				selVolume.value = data.seats == Infinity ? 'Unlimited' : data.seats + ' Users - ('+(data.seats - data.activated)+' Remaining)';
				eventLogger('end', 'billing');
			}
		});
		//
		//
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};