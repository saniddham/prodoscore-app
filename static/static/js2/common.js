
var products = {
/*+00*/		gm: ['gmail', 		{'weight': 5, 'name': 'Emails sent', 		'name-2': 'Gmail'}, '#F04444', {'To': 'To'}],//'Date': 'Date', , 'body': 'Body'
/*+01*/		ch: ['hangouts', 	{'weight': 1, 'name': 'Chat lines', 			'name-2': 'Hangouts Chat'}, '#44D044', {'from': 'From'}],//{}
/*+02*/		dc: ['docs', 		{'weight': 2, 'name': 'Google Drive activities','name-2': 'Drive Activity'}, '#66DDFF', {}], // 'author': 'Author', 'comment': 'Comment', 'content': 'Content &#916;'
/*+03*/		cl: ['calendar',   	{'weight': 3, 'name': 'Calendar event time', 'name-2': 'Calendar Time (minutes)'}, '#4466DD', {'attendees_status': 'Status', 'duration': 'Duration', 'description': 'Description'}],
/*+04*/		tb: ['turbobridge',	{'weight': 4, 'name': 'Conference calls', 	'name-2': 'TurboBridge: Call (minutes)'}, '#44FF66', {'fromNumber': 'Number', 'duration': 'Duration', 'location': 'Location'}],////'toNumber': 'To Number', From
/*+05*/		bs: ['broadsoft', 	{'weight': 4, 'name': 'Phone system', 		'name-2': 'Broadsoft: Call (minutes)'}, '#FF6644', {'name': 'Name', 'address' : 'Number', 'duration': 'Duration', 'network': 'Network', 'Status': 'Status', 'Direction': 'Direction'}],
/*+06*/		sms: ['smss',     	{'weight': 2, 'name': 'Mobile Text Messages', 'name-2': 'Android: SMS'}, '#44FF66', {'content': 'Message'}],
/*+07*/		cll: ['calls', 		{'weight': 2, 'name': 'Mobile Phone Calls', 	'name-2': 'Android: Call (minutes)'}, '#44FF66', {'type': 'Type', 'duration': 'Duration'}],
/*+08*/		rcc: ['ringc_calls', 	{'weight': 4, 'name': 'Phone Calls', 	'name-2': 'Phone Call (minutes)'}, '#FF6644', {'result': 'Status', 'type': 'Type', 'direction': 'Direction', 'duration': 'Duration'}],
/*+09*/		rcs: ['ringc_sms', 	{'weight': 4, 'name': 'SMS', 	'name-2': 'SMS'}, '#FF6644', {'readStatus': 'Status', 'subject': 'Message'}],
/*+10*/		zc: ['zcalls', 		{'weight': 4, 'name': 'CRM calls', 			'name-2': 'Zoho: Calls (minutes)'}, '#44FF66', {'Call Type': 'Call Type', 'Call Status': 'Call Status', 'Call Duration': 'Call Duration', 'Related To': 'Related To', 'Billable': 'Billable'}],
/*+11*/		ze: ['zevents',   	{'weight': 3, 'name': 'CRM events', 		'name-2': 'Zoho: Events'}, '#44FF66', {'Created By': 'Event Creator', 'Venue': 'Venue', 'duration': 'Duration', 'Description': 'Description'}],
/*+12*/		zt: ['ztasks', 		{'weight': 2, 'name': 'CRM tasks', 			'name-2': 'Zoho: Tasks'}, '#44FF66', {'Status': 'Status', 'Priority': 'Priority', 'Contact Name': 'Contact Name', 'Related To': 'Related To', 'Due Date': 'Due Date'}],
/*+13*/		zl: ['zleads', 		{'weight': 4, 'name': 'CRM leads', 			'name-2': 'Zoho: Leads'}, '#44FF66', {'Email': 'Email', 'Company': 'Company', 'Description': 'Description', 'Industry' : 'Industry', 'Lead Status' : 'Lead Status', 'Lead Source' : 'Lead Source'}],
/*+14*/		zi: ['zinvoices',   	{'weight': 3, 'name': 'CRM invoices', 		'name-2': 'Zoho: Invoices'}, '#44FF66', {}],
/*+15*/		za: ['zaccounts', 	{'weight': 2, 'name': 'CRM accounts', 		'name-2': 'Zoho: Conversions'}, '#44FF66', {'Account Number' : 'Account Number', 'Account Type' : 'Account Type', 'Industry': 'Industry', 'Description': 'Description'}],
/*+16*/		sf: ['salesforce', 	{'weight': 4, 'name': 'Opportunity activities', 'name-2': 'Salesforce: Oportunity Act. (depr.)'}, '#44FF66', { 'AccountName': 'Account Name', 'Type': 'Type', 'LeadSource': 'Lead source', 'Probability': 'Probability', 'Amount': 'Amount', 'StageName': 'Stage', 'Description': 'Description', 'CloseDate': 'Closing Date'}],
/*+17*/		sfl: ['sfleads', 		{'weight': 3, 'name': 'CRM leads', 			'name-2': 'Salesforce: Leads (depr.)'}, '#44FF66', {'Status': 'Status', 'Email': 'Email', 'LeadSource': 'Lead Source', 'Title': 'Title','Company': 'Company', 'NumberOfEmployees': '# Employees', 'Attachment': 'Attachment'}],
/*+18*/		pl: ['pwleads',  	{'weight': 4, 'name': 'CRM leads', 			'name-2': 'ProsperWorks: Leads'}, '#44FF66', {'status': 'Status', 'company_name': 'Company', 'details': 'Details'}],
/*+19*/		po: ['pwoprtunts', 	{'weight': 4, 'name': 'CRM opportunities', 	'name-2': 'ProsperWorks: Opportunities'}, '#44FF66', {'status': 'Status', 'priority': 'Priority', 'company_name': 'Company', 'monetary_value': 'Value', 'details': 'Details'}],
/*+20*/		scl: ['sugar_leads', {'weight': 4, 'name': 'CRM leads', 			'name-2': 'SugarCRM: Leads'}, '#44FF66', {'full_name': 'Full name', 'account_name': 'Account name', 'email1' : 'Email','lead_source':'Lead Source','status':'Status'}],
/*+21*/		sco: ['sugar_opps', {'weight': 4, 'name': 'CRM opportunities', 	'name-2': 'SugarCRM: Opportunities'}, '#44FF66', {'account_name': 'Account name', 'opportunity_type': 'Opportunity type','lead_source': 'Lead Source'}],
/*+22*/		scc: ['sugar_calls', {'weight': 4, 'name': 'CRM calls', 			'name-2': 'SugarCRM: Calls (minutes)'}, '#44FF66', {'status': 'Status', 'duration': 'Duration', 'description': 'Description','direction':'Direction', 'accept_status_calls': 'Participant Status'}],
/*+23*/		scm: ['sugar_meets', {'weight': 4, 'name': 'CRM meetings', 		'name-2': 'SugarCRM: Meetings (minutes)'}, '#44FF66', {'status': 'Status', 'description': 'Description', 'accept_status_meetings': 'Participant Status'}],
/*+24*/		chm: ['chmsgs', 	{'weight': 1, 'name': 'Chatter messages', 	'name-2': 'Salesforce: Chatter Messages'}, '#44FF66', {'Body': 'Body'}],
/*+25*/		cha: ['chacts',   	{'weight': 1, 'name': 'Chatter activities', 	'name-2': 'Salesforce: Chatter Activities'}, '#44FF66', {'Body': 'Body'}],
/*+26*/		sfc: ['sfcalls',     	{'weight': 4, 'name': 'Salesforce Calls', 		'name-2': 'Salesforce: Calls (no. of calls)'}, '#44FF66', {'Status': 'Status', 'Priority': 'Priority', 'Type': 'Related To',  'Description': 'Description', 'CallType': 'Call Type'}],
/*+27*/		sfi: ['sfints', 		{'weight':1, 'name': 'Salesforce Interactions','name-2': 'Salesforce: Activities'}, '#44FF66',  {'Type': 'Module',  'Description': 'Description'}],
/*+xx*/		//
/*+28*/		sf_n: ['salesforce_n',{'weight': 5, 'name': 'Opportunity activities','name-2': 'Salesforce: New Opportunities'}, '#44FF66', { 'AccountName': 'Account Name', 'Type': 'Type', 'LeadSource': 'Lead source', 'Probability': 'Probability', 'Amount': 'Amount', 'StageName': 'Stage', 'Description': 'Description', 'CloseDate': 'Closing Date'}],
/*+29*/		sfl_n: ['sfleads_n', 	{'weight': 3, 'name': 'CRM leads', 			'name-2': 'Salesforce: New Leads'}, '#44FF66', {'Status': 'Status', 'Email': 'Email', 'LeadSource': 'Lead Source', 'Title': 'Title','Company': 'Company', 'NumberOfEmployees': '# Employees', 'Attachment': 'Attachment'}],
/*+30*/		vbc: ['vbc', 		{'weight': 3, 'name': 'VBC calls', 			'name-2': 'VBC: Calls'}, '#44FF66', {'result': 'Status', 'direction': 'Direction', 'length': 'Duration'}],
/*+31*/		otl: ['otl', 		{'weight': 5, 'name': 'Emails sent', 			'name-2': 'Microsoft Outlook'}, '#44FF66',
				{'To': 'To', 'bodyPreview': 'Body'}],
/*+32*/		odr: ['odr', 		{'weight': 3, 'name': 'OneDrive activities', 			'name-2': 'OneDrive Activity'}, '#44FF66',
				{}],
/*+33*/		oev: ['oev', 		{'weight': 2, 'name': 'Calendar event time', 			'name-2': 'Calendar Time (minutes)'}, '#44FF66',
				{'attendees_status': 'Status', 'duration': 'Duration', 'bodyPreview': 'Description'}]
};

var prodInputsLiMapping = [2, 0, 3, 1, 4, 10, 11, 12, 13, 14, 15, 5, 16, 6, 7, 18, 19, 8, 9, 17, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33];

var icons = {
	'cl': static_url+'img/icons/cal.png',
	'gm': static_url+'img/icons/mail.png',
	'dc': static_url+'img/icons/logo_drive_64px.png',
	'ch': static_url+'img/icons/chat.png',
	'tb': static_url+'img/icons/conf_call-01.svg',
	'bs': static_url+'img/icons/calls-01.svg',
	'zc': static_url+'img/icons/crm_conversions-01.svg',
	'ze': static_url+'img/icons/crm_events-01.svg',
	'zt': static_url+'img/icons/crm_tasks-01.svg',
	'zl': static_url+'img/icons/crm_leads-01.svg',
	'zi': static_url+'img/icons/crm_invoice-01.svg',
	'za': static_url+'img/icons/crm-01.svg',
	'sms': static_url+'img/icons/mobile_message.svg',
	'cll': static_url+'img/icons/mobile_call.svg',
	'pl': static_url+'img/icons/crm_leads-01.svg',
	'po': static_url+'img/icons/crm_opportunity.svg',
	'rcc': static_url+'img/icons/calls-01.svg',
	'rcs': static_url+'img/icons/sms.svg',
	'chm': static_url+'img/icons/chatter_msg.svg',
	'cha': static_url+'img/icons/chatter_act.svg',
	'scm': static_url+'img/icons/crm_events-01.svg',
	'scl': static_url+'img/icons/crm_leads-01.svg',
	'sco': static_url+'img/icons/crm_opportunity.svg',
	'scc': static_url+'img/icons/crm_conversions-01.svg',
	'sfc': static_url+'img/icons/crm_calls.svg',
	'sfi': static_url+'img/icons/crm_activity-01.svg',
	//
	'sf_n': static_url+'img/icons/crm_opportunity.svg',
	'sfl_n': static_url+'img/icons/crm_leads-01.svg',
	'sf': static_url+'img/icons/crm_opportunity.svg',
	'sfl': static_url+'img/icons/crm_leads-01.svg',
	'vbc': static_url+'img/icons/calls-01.svg',
	'otl': static_url+'img/icons/outlook.png',
	'odr': static_url+'img/icons/onedrive.png',
	'oev': static_url+'img/icons/officecalendar.png'
};

var iconsMap = {
	'application/vnd.google-apps.audio': 'icon_1_audio_x32.png',
	'application/vnd.google-apps.document': 'logo_docs_64px.png',
	'application/vnd.google-apps.drawing': 'icon_1_drawing_x32.png',
	'application/vnd.google-apps.file': 'docs.svg',
	'application/vnd.google-apps.form': 'logo_forms_64px.png',
	'application/vnd.google-apps.fusiontable': 'logo_sheets_64px.png',
	'application/vnd.google-apps.map': '',
	'application/vnd.google-apps.photo': '',
	'application/vnd.google-apps.presentation': 'logo_slides_64px.png',
	'application/vnd.google-apps.script': '',
	'application/vnd.google-apps.sites': 'logo_sites_64px.png',
	'application/vnd.google-apps.spreadsheet': 'logo_sheets_64px.png',
	'application/vnd.google-apps.video': 'icon_1_video_x32.png',
};

var taxonomyDictionary = {'consumer_goods': 'Item', 'events': 'Event', 'locations': 'Location', 'organizations': 'Organization', 'others': 'Other', 'persons': 'Person', 'work_of_arts': 'Work'};
var colorDictionary = {
	'locations': 		'hsl(0, 72%, 50%)',
	'persons': 			'hsl(281, 78%, 53%)',
	'organizations': 		'hsl(207, 76%, 48%)',
	'others': 			'hsl(134, 69%, 39%)',
	/*'consumer_goods': 	'hsl(1, 72%, 58%)',
	'events': 			'hsl(180, 85%, 42%)',
	'work_of_arts': 		'hsl(220, 72%, 58%)'*/
};
var faDictionary = {
	'locations': 		'fa-map-marker',
	'persons': 			'fa-user',
	'organizations':		'fa-building',
	'others': 			'fa-tag'
};

var roles = {};
var statuses = {1: 'Visible'};
statuses[-1] = 'Hidden';

document.forms[0].onsubmit = function(){
	return false;
}

function date_filter_onchange(value){;
	if (typeof organizationSettings == 'undefined' || typeof organizationSettings.workingdays == 'undefined'){
		setTimeout(function(){
			date_filter_onchange(value);
		}, 520);
		return false;
	}
	var yesterday = new Date(organizationSettings.upto),
		to_date = document.forms[0].to_date,
		from_date = document.forms[0].from_date;
	yesterday.setDate(yesterday.getDate()+1);
	yesterday.setMinutes(yesterday.getTimezoneOffset() + organizationSettings.tzoffset);
	//
	//	Get the last working date to start back from
	while (!isWorkingDay(yesterday.getDay()))
		yesterday.setDate(yesterday.getDate()-1);
	//
	if (value == 'yesterday'){
		to_date.value = yesterday.sqlFormatted();
		from_date.value = yesterday.sqlFormatted();
	}
	else if (value == 'last-7-days'){
		to_date.value = yesterday.sqlFormatted();
		yesterday.setDate(yesterday.getDate()-6);
		from_date.value = yesterday.sqlFormatted();
	}
	else if (value == 'this-week'){
		to_date.value = yesterday.sqlFormatted();
		yesterday.setDate(yesterday.getDate()-yesterday.getDay() + organizationSettings.workingdays[0]);
		from_date.value = yesterday.sqlFormatted();
	}
	else if (value == 'last-30-days'){
		to_date.value = yesterday.sqlFormatted();
		yesterday.setDate(yesterday.getDate()-29);
		from_date.value = yesterday.sqlFormatted();
	}
	else if (value == 'this-month'){
		to_date.value = yesterday.sqlFormatted();
		yesterday.setDate(yesterday.getDate()-yesterday.getDate()+1);
		from_date.value = yesterday.sqlFormatted();
	}
	else if (value == 'year-to-date'){
		to_date.value = yesterday.sqlFormatted();
		yesterday = new Date(yesterday.getFullYear(), 0, 1);
		from_date.value = yesterday.sqlFormatted();
	}
	else if (value == 'custom') {
		from_date.focus();
	}
}


// ==============================================================================================
// ==============================================================================================
//													SCROLLING
// ==============================================================================================

function scrollToTop(){
	scrollTarget = 0;
	setTimeout(function(){
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}, 120);
}

var lastScrollPos = 0, scrollTimeout;
function scrollToMiddle(top){
	scrollTarget = top;
	setTimeout(function(){
		document.body.scrollTop = top;
		document.documentElement.scrollTop = top;
	}, 120);
}


// ==============================================================================================
// ==============================================================================================
//													AJAX CONGESTION CONTROL AND CACHING OPTIONS
// ==============================================================================================

var cacheKey = '', cacheData = {}, delayedAutoReLoad = false;
var connectionRetry = false, prevXMLhttpRequest = false;
//	---------------------------------------------------------------------------------------------
//	FETCH FROM SERVER AND PREPARE RAW DATA FOR THE SELECTED DATE RANGE
//	---------------------------------------------------------------------------------------------
function doFetchData(page, callback, progress){
	clearTimeout(delayedAutoReLoad);
	startD = DateToShort(document.forms[0].from_date.value);
	endD = DateToShort(document.forms[0].to_date.value);// + 1;
	//
	if (startD > endD){
		delayedAutoReLoad = setTimeout(function(){
			endD = startD;//+1
			var tmp = DateFromShort(endD).toString().split(' ');//-1
			document.forms[0].to_date.value = tmp[3]+'-'+monthsReverse[tmp[1]]+'-'+tmp[2];
			doFetchData(page, callback, progress);
		}, 2400);
		showAlert('warning', 'Please set a To date after the From date.', 2400);
		return false;
	}
	processedData = {};
	//
	//	Do the actual callback to the frame loader function
	eventLogger('start', page);
	function doCallback(){
		var self = organizationSettings.myself;
		var days = Object.keys(processedData.days);
		//
		//	Load managers and teams (role based) in a seperate thread
		fillMgrsNTeamsLists(self);
		//
		callback(processedData, self, 1*days[0], 1*days[days.length-1]);
		//
		//	Close if any alerts displayed in the notification area
		for (var i = messages.childNodes.length-1; i > -1; i--)
			messages.childNodes[i].click();
		//
		scrollToTop();
		eventLogger('end', page);
	}
	//
	if (cacheKey == startD+':'+endD){
		processedData = cacheData;
		doCallback();
		return false;
	}
	//
	if (prevXMLhttpRequest)
		prevXMLhttpRequest.abort();
	//
	prevXMLhttpRequest = new arc.ajax(base_url+'dashboard-ajax/?fromdate='+document.forms[0].from_date.value+'&todate='+document.forms[0].to_date.value, {
		callback: function(data){
			processedData = JSON.parse(data.responseText);
			cacheKey = startD+':'+endD;
			cacheData = processedData;
			//
			doCallback();
		},
		/*onprogress: function(progress){
			progressiveSubLoaded = 100 + progress;
			displayProgress();
		},*/
		fallback: function(xmlhttp){
			if (xmlhttp.status == 500)
				showAlert('error', 'Server Error.');
			else
				showAlert('warning', 'Couldn\'t connect to server. Please check your internet connection and retry.');/*connectionWarning = */
			//	Display warning, Invalidate cache and retry same in 2.4 seconds
			cacheKey = '';
			clearTimeout(connectionRetry);
			connectionRetry = setTimeout(
				function(){
					doFetchData(page, callback, progress);
				}, 1200);
		}
	});
	return false;
}

let doFetchEmployeeListRunning = false;
let doFetchEmployeeListCallbacks = [];
let doFetchEmployeeListKey = '';
function doFetchEmployeeList(period, date, callback){
	if (doFetchEmployeeListKey != period+'-'+date){
		doFetchEmployeeListKey = period+'-'+date;
		doFetchEmployeeListCallbacks = [];
	}
	doFetchEmployeeListCallbacks.push(callback);
	if (doFetchEmployeeListRunning){
		return false;
	}
	eventLogger('start', 'EmployeeList');
	doFetchEmployeeListRunning = true;
	//
	new arc.ajax(base_url+'dashboard-employee-list-ajax/?period='+period.replace(/-/g, '_')+'&date='+date, {
		callback: function(resp){
			eventLogger('end', 'EmployeeList');
			let data = JSON.parse(resp.responseText);
			for (let i = 0; i < doFetchEmployeeListCallbacks.length; i++)
				doFetchEmployeeListCallbacks[i](data, DateToShort(document.forms[0].from_date.value), DateToShort(document.forms[0].to_date.value));
			doFetchEmployeeListCallbacks = [];
			doFetchEmployeeListRunning = false;
		},
		fallback: function(xmlhttp){
			if (xmlhttp.status == 500)
				showAlert('error', 'Server Error.');
			else
				showAlert('warning', 'Couldn\'t connect to server. Please check your internet connection and retry.');/*connectionWarning = */
			//	Display warning, Invalidate cache and retry same in 2.4 seconds
			cacheKey = '';
			clearTimeout(connectionRetry);
			connectionRetry = setTimeout(
				function(){
					doFetchEmployeeList(period, date, callback);
				}, 1200);
		}
	});
}

//	Obtain required data for all employees to be compared.
//	fillComparingEmployeeData
function fillMissingEmployeeData(employeesShortlist, data, startD, endD, callback){
	let empIds = Object.keys(employeesShortlist);
	let progress = 0;
	let checkpoint = function(){
		progress += 1;
		//
		if (progress == empIds.length)
			//	Callback to chart draw - we have all required data.
			callback(employeesShortlist, data);
	};
	//
	for (let i = 0; i < empIds.length; i++)
		new (function(empId){
			new arc.ajax(base_url+'employee-ajax/?employee='+empId+'&fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted(), {
				cache: true,
				callback: function(empDay){
					empDays = eval('('+empDay.responseText+')');
					if (typeof processedData.employees[empId] == 'undefined')
						processedData.employees[empId] = {};
					processedData.employees[empId].days = empDays.days;
					checkpoint();
				},
				fallback: function(xmlhttp){
					if (xmlhttp.status == 500)
						showAlert('error', 'Server Error.');
					else
						showAlert('warning', 'Couldn\'t connect to server. Please check your internet connection and retry.');
				}
			});
		})(empIds[i]);
}

//	Fill Managers list and Roles(teams) list on the LHS navigation
var RoleList = {}, mgrList = {};
function fillMgrsNTeamsLists(self){
	//
	if (self == null || typeof self == 'undefined')
		self = organizationSettings.myself;
	let mgrsList = q('li[data-tag="managers"] ul')[0];
	let mgrsToggle = q('li[data-tag="managers"] i')[0];
	let teamList = q('li[data-tag="employees"] ul')[0];
	let teamToggle = q('li[data-tag="employees"] i')[0];
	if (typeof mgrsList == 'undefined' || typeof teamList == 'undefined')
		return false;
	//
	let hasManagers = false;
	for (let i in processedData.employees){
		//	Viewer is admin and this user has a manager OR this users manager is viewer OR this users managers manager is viewer
		//	If admin is viewing or 	this users manager is the viewer 	or 		this users managers manager is the viewer
		if ( ( self.role == 80 || processedData.employees[i].manager == uid || (typeof processedData.employees[ processedData.employees[i].manager ] != 'undefined' && processedData.employees[ processedData.employees[i].manager ].manager == uid) ) ){
			if (processedData.employees[i].manager != uid && typeof processedData.employees[ processedData.employees[i].manager ] != 'undefined'){
				hasManagers = true;
				mgrList[processedData.employees[i].manager] = true;
			}
			if (processedData.employees[i].role > 0 && typeof processedData.employees[i].scr != 'undefined' && processedData.employees[i].scr){
				RoleList[processedData.employees[i].role] = true;
			}
		}
	}
	//
	//*/	Managers tab and sub-menu
	mgrsList.innerHTML = '';
	for (let i in mgrList)
		if (typeof processedData.employees[i] != 'undefined' && typeof processedData.employees[i].fullname != 'undefined')
			mgrsList.appendChild(arc.elem('li',
				//'<img src="'+processedData.employees[i].picture+'">'+
				'<a href="#employees/under/'+i+'/'+processedData.employees[i].email+'">'+processedData.employees[i].fullname+'</a>'+
				(typeof processedData.employees[i].picture == 'undefined' || processedData.employees[i].picture == '' ?
					'<img src="'+static_url+'img/icons/profile.jpeg" />' :
					'<img src="'+processedData.employees[i].picture+'" style="border:1px solid #'+myIcon[1]+'" />'
				)
			));//<span title="'+mgrList[i]+' subordinates">'+mgrList[i]+'</span>
	sortList(mgrsList);
	//
	mgrsToggle.onclick = function(){
		if (Number.isNaN(Number(parseInt(mgrsList.style.height))) || parseInt(mgrsList.style.height) === 0) {
			mgrsList.slideDown(320, function () {
				mgrsToggle.className = 'fa fa-caret-down';
			});
		} else {
			mgrsList.slideUp(320, function () {
				mgrsToggle.className = 'fa fa-caret-right';
			});
		}
	}
	q('li[data-tag="managers"]')[0].style.display = hasManagers ? 'block' : 'none';
	//
	//*/	Roles sub-menu under employees tab
	teamList.innerHTML = '';
	for (var i in RoleList)
		teamList.appendChild(arc.elem('li', '<a href="#employees/role/'+i+'/'+roles[i].toLowerCase().replace(/ /g, '-')+'">' + roles[i] + '</a>'));//parseInt(RoleList[i].tot / RoleList[i].cou)//<span title="'+RoleList[i]+' employees">'+RoleList[i]+'</span>
	sortList(teamList);
	//
	teamToggle.onclick = function(){
		if (Number.isNaN(Number(parseInt(teamList.style.height))) || parseInt(teamList.style.height) === 0) {
			teamList.slideDown(320, function () {
				teamToggle.className = 'fa fa-caret-down';
			});
		} else {
			teamList.slideUp(320, function () {
				teamToggle.className = 'fa fa-caret-right';
			});
		}
	}
}


// -------------------------------------------------------------------------
//	Employee name, role, manager, score, delta
// -------------------------------------------------------------------------
function fillEmployeeDetails(context, data, i, subScore) {
	var user = (i in data.employees && parseInt(i)!==data.self.id) ? data.employees[i] : data.self;
	var userInfo = context.q('.one-row.emp-header')[0],
		userInfoCards = userInfo.q('.emp-header-card');
	var icon = usernameIcon(user.fullname);
	userInfo.style.display = 'flex';
	userInfo.q('.circle')[0].style.backgroundColor = '#' + icon[1];
	userInfo.q('.circle')[0].style.border = '2px solid #' + icon[1];
	if (user.picture == '') {
		userInfo.q('.circle')[0].innerHTML = '<span>' + icon[0] + '</span>';
		userInfo.q('.circle')[0].style.backgroundImage = '';
	}
	else{
		userInfo.q('.circle')[0].innerHTML = '';
		userInfo.q('.circle')[0].style.backgroundImage = "url('" + user.picture + "')";
	}
	userInfo.q('.user-name')[0].innerHTML = user.fullname;
	userInfo.q('.role')[0].innerHTML = roles[user.role];
	if (typeof data.employees[user.manager] == 'undefined'){
		if (typeof data.self !== 'undefined' && user.manager == data.self.id){
			userInfo.q('.manager')[0].style.display = 'block';
			userInfo.q('.manager a')[0].innerHTML = data.self.fullname;
			userInfo.q('.manager a')[0].removeAttribute('href');
		}
		else if (typeof data.self !== 'undefined' && i == data.self.id && typeof data.self.manager !== 'undefined'){
			userInfo.q('.manager')[0].style.display = 'block';
			userInfo.q('.manager a')[0].innerHTML = data.self.manager.fullname;
			userInfo.q('.manager a')[0].removeAttribute('href');
		}
		else{
			userInfo.q('.manager')[0].style.display = 'none';
		}
	}
	else{
		userInfo.q('.manager')[0].style.display = 'block';
		userInfo.q('.manager a')[0].innerHTML = data.employees[user.manager].fullname;
		userInfo.q('.manager a')[0].setAttribute('href', '#employees/under/' + user.manager + '/' + data.employees[user.manager].email);
	}
	//
	var score = userInfo.q('.emp-status-details div');
	var showEmpScores = false;
	//
	if (typeof subScore !== 'undefined') {
		score[0].className = 'score';
		score[1].className = score[2].className = '-';
		score[1].q('span')[0].innerHTML = score[2].q('span')[0].innerHTML = '- &nbsp;';
		//
		if (isNaN(subScore)) {
			score[0].q('span')[0].innerHTML = '-';
		}
		else{
			score[0].q('span')[0].innerHTML = subScore.toFixed(0);
			score[0].classList.add(strataClassname4Score(subScore));
		}
		showEmpScores = true;
	}
	//
	if (typeof user.scr != 'undefined' && user.scr) {
		if (typeof subScore == 'undefined') {
			score[0].className = 'score';
			score[1].className = 'delta';
			score[0].q('span')[0].innerHTML = user.scr.l.toFixed(0);
			score[0].classList.add(strataClassname4Score(user.scr.l));
			//
			if (!user.scr.delta || user.scr.delta == 0) {
				score[1].style.display = 'none';
			}
			else{
				score[1].style.display = 'block';
				score[1].innerHTML = '&nbsp;<span>' + Math.abs(user.scr.delta.toFixed(0)) + '</span>%&nbsp;Performance&nbsp;' + (user.scr.delta > 0 ? 'increase' : 'decrease');
				score[1].classList.add( (!user.scr.delta || user.scr.delta == 0 ? 'same' : (user.scr.delta > 0 ? 'increase' : 'decrease')) );
			}
		}
		else {
			score[0].className = 'score';
			score[1].className = 'score';
			score[2].className = 'delta';
			if (isNaN(subScore)) {
				score[0].q('span')[0].innerHTML = '-';
			}
			else{
				score[0].q('span')[0].innerHTML = subScore.toFixed(0);
				score[0].classList.add(strataClassname4Score(subScore));
			}
			//
			score[1].q('span')[0].innerHTML = user.scr.l.toFixed(0);
			score[1].classList.add(strataClassname4Score(user.scr.l));
			//
			if (!user.scr.delta || user.scr.delta == 0) {
				score[2].style.display = 'none';
			}
			else{
				score[2].style.display = 'block';
				score[2].innerHTML = '&nbsp;<span>' + Math.abs(user.scr.delta.toFixed(0)) + '</span>%&nbsp;Performance&nbsp;' + (user.scr.delta > 0 ? 'increase' : 'decrease');
				score[2].classList.add( (!user.scr.delta || user.scr.delta == 0 ? 'same' : (user.scr.delta > 0 ? 'increase' : 'decrease')) );
			}
		}
		showEmpScores = true;
	}
	userInfo.q('.emp-status-details')[0].parentNode.style.display = (showEmpScores) ?  'block' : 'none';
}


//	Allow submitting feedbacks and suggestions with a screenshot
var debug;
function initializeFeedbackForm(){
	var popupBG = q('#popup-bg')[0];
	var popupBGClose = popupBG.q('i.fa-times')[0];
	var loading = popupBG.q('.loading')[0];
	var screens = popupBG.q('.content.active');
	screens[0].style.display = 'block';
	screens[1].style.display = 'none';
	loading.style.display = 'none';
	//
	var notificationsBtn = q('a#feedback')[0];
	var serializeScreencapture;
	//
	notificationsBtn.onclick = function(){
		popupBG.q('textarea')[0].value = '';
		var screenshot = popupBG.q('#screenshot')[0].contentWindow.document;
		var initialWidth = window.innerWidth - 12;
		screenshot.body.innerHTML = '';
		/*screenshot.body.className = '';
		screenshot.body.style.zoom = 0.47;
		screenshot.body.style.cursor = 'crosshair';*/
		var container = screenshot.body.appendChild(arc.elem('div', '', {style: 'width:'+initialWidth+'px;', class: 'screenshot'}));
		//
		var drawTools = popupBG.q('.draw-tools a'), drawClass = 'highlight', selDrawTool;
		selDrawTool = drawTools[0];
		drawTools[0].onclick = function(){
			drawClass = 'highlight';
			selectDrawTool(this);
		};
		drawTools[1].onclick = function(){
			drawClass = 'hide';
			selectDrawTool(this);
		};
		drawTools[2].onclick = function(){
			drawClass = '';//erase
			selectDrawTool(this);
		};
		drawTools[3].onclick = function(){
			//drawClass = '';//reset
			var drawElems = container.q('.draw-elem');
			for (var i = 0; i < drawElems.length; i++)
				drawElems[i].parentNode.removeChild(drawElems[i]);
			serializeScreencapture();
		};
		var selectDrawTool = function(tool){
			if (selDrawTool)
				selDrawTool.removeClass('active');
			selDrawTool = tool;
			tool.addClass('active');
		}
		//
		var sx, sy, ex = 0, ey = 0, drawElem = false;
		screenshot.body.onmousedown = function(e){
			if (drawClass == '' && e.target.className.indexOf('draw-elem') > -1)
				e.target.parentNode.removeChild(e.target);
			else{
				//sx = 200 * (e.clientX + this.scrollLeft) / this.offsetWidth;
				sx = 2.13 * (e.clientX + this.scrollLeft);
				sy = 2.13 * (e.clientY + this.scrollTop);
				ex = 0;
				ey = 0;
				drawElem = arc.elem('div', null, {class: 'draw-elem '+drawClass, style: 'top:'+sy+'px; left:'+sx+'px;'});
				container.appendChild(drawElem);
			}
			return cancelElents(e);
		};
		screenshot.body.onmousemove = function(e){
			if (!drawElem)
				return false;
			//drawElem.style.width = (200 * (e.clientX + this.scrollLeft) / this.offsetWidth) - sx + '%';
			ex = 2.13*e.clientX - sx + 2.13*this.scrollLeft;
			ey = 2.13*e.clientY - sy + 2.13*this.scrollTop;
			if (ex < 0){
				//sx -= ex;
				drawElem.style.left = (sx + ex) + 'px';
				ex *= -1;
			}
			if (ey < 0){
				//sy -= ey;
				drawElem.style.top = (sy + ey) + 'px';
				ey *= -1;
			}
			drawElem.style.width = ex + 'px';
			drawElem.style.height = ey + 'px';
			return cancelElents(e);
		};
		screenshot.body.onmouseup = function(e){
			//try{
			if (drawElem && ex < 4 && ey < 4)
				drawElem.parentNode.removeChild(drawElem);
			//}catch(e){}
			drawElem = false;
			serializeScreencapture();
			return cancelElents(e);
			//console.log(e.clientX + ', ' + e.clientY);
		};
		screenshot.body.onclick = screenshot.body.onmouseover = function(e){
			return cancelElents(e);
		};
		var cancelElents = function(e){
			e.preventDefault();
			e.stopPropagation();
			e.cancelBubble = true;
			return false;
		}
		//
		//	Append active elements to screen capture
		var showElems = q('div.sidebar, header.header.cf, div.page-content');
		for (var i = 0; i < 3; i++)
			container.appendChild(arc.elem(showElems[i].tagName, showElems[i].innerHTML, {class: showElems[i].className}));
		container.querySelectorAll('div.page-content')[0].style.position = 'relative';
		//
		//	Remove inactive/hidden elements from screen capture
		var inactives = popupBG.q('#screenshot')[0].q('[style^="display: none;"]');//div.content
		for (var i = 0; i < inactives.length; i++)
			inactives[i].parentNode.removeChild(inactives[i]);
		//
		var inputs = container.querySelectorAll('[data-value]');
		for (var i = 0; i < inputs.length; i++)
			if (inputs[i].tagName == 'INPUT' && inputs[i].type == 'checkbox')
				inputs[i].checked = inputs[i].getAttribute('data-value') == 1;
			else
				inputs[i].value = inputs[i].getAttribute('data-value');
		//
		inputs = container.querySelectorAll('.header-left select#date-filter, .header-left input[type="date"]');
		if (inputs.length > 0){
			inputs[0].value = document.forms[0].filter.value,
			inputs[1].value = document.forms[0].from_date.value,
			inputs[2].value = document.forms[0].to_date.value;
		}
		//
		//	Include stylesheets
		var stylesheets = q('link[rel="stylesheet"]');
		for (var i = 0; i < stylesheets.length-1; i++)
			screenshot.body.appendChild(arc.elem('link', null, {rel: 'stylesheet', type: 'text/css', href: stylesheets[i].getAttribute('href')}));
			/*new arc.ajax(stylesheets[i].getAttribute('href'), {
				callback: function(data){
					screenshot.body.appendChild(arc.elem('style', data.responseText));//{rel: 'stylesheet', type: 'text/css', href: }
				}
			});*/
		screenshot.body.appendChild(arc.elem('style', '.draw-elem{position:absolute; z-index:100;}'+
											'.draw-elem.highlight{border:2px solid #FFC060; background-color:rgba(255, 255, 40, 0.3);}'+
											'.draw-elem.hide{border:2px solid #808080; background-color:#000000;}'+
											'nav.sidebar-nav{height:calc(100vh + 44px) !important;} .page-content{padding-top:0px;} .header{position:relative;}'+
											'body{zoom:0.47;}body *{cursor:crosshair !important;}'));
		//
		//	Serialize screen capture to base64
		serializeScreencapture = function(){
			popupBG.q('input[type="hidden"]')[0].value = btoa(unescape(encodeURIComponent(screenshot.body.innerHTML)));
			while (popupBG.q('input[type="hidden"]')[0].value.length % 4 != 0)
				popupBG.q('input[type="hidden"]')[0].value += '=';
		}
		serializeScreencapture();
		//*/
		popupBG.style.display = 'block';
		notificationsBtn.style.opacity = 0;
		setTimeout(function(){
			popupBG.style.opacity = 1;
		}, 10);
		document.body.addClass('popup-on');
		screenshot.body.scrollTop = document.body.scrollTop;
		// ======================================================
		/*html2canvas(screenshot.body,
			{
				height: window.innerHeight, width: window.innerWidth, top: 0, background: '#FFFFFF',
				onrendered: function(canvas){
					downloadPNG(canvas, 'screenshot');
				}
			});*/
		// ======================================================
		//
		popupBG.q('.card-wrap')[0].style.top = 'calc(50vh - 270px)';//'+(popupBG.q('.one-row')[0].offsetHeight/2)+'px
		popupBG.q('.card-wrap')[0].style.left = '25%';
		popupBG.q('.card-wrap')[0].style.zoom = 1;
	};

	var drawTools = popupBG.q('.draw-tools a');
	drawTools[0].onclick = function(){
		drawableHandler.color('rgba(255, 0, 0, 0.4)', 6);
	};
	drawTools[1].onclick = function(){
		drawableHandler.eraser();
	};

	popupBG.q('input[type="button"]')[0].onclick = popupBGClose.onclick = function(){
		popupBG.style.opacity = 0;
		notificationsBtn.style.opacity = 1;
		popupBG.q('.card-wrap')[0].style.top = '100vh';
		popupBG.q('.card-wrap')[0].style.left = '85%';
		setTimeout(function(){
			popupBG.q('.card-wrap')[0].style.height = '';
			popupBG.style.display = 'none';
			//popupBG.q('.card-wrap')[0].style.top = '100vh';
			document.body.removeClass('popup-on');
			submitBtn.disabled = false;
			screens[0].style.display = 'block';
			screens[1].style.display = 'none';
			loading.style.display = 'none';
			popupBG.q('input[type="hidden"]')[0].value = '';
		}, 510);
	};
	var feedbackForm = popupBG.q('form')[0];
	var submitBtn = feedbackForm.q('input[type="submit"]')[0];
	var chkScreenshot = popupBG.q('input[type="checkbox"]')[0]
	feedbackForm.onsubmit = function(){
		serializeScreencapture();
		submitBtn.disabled = true;
		screens[1].innerHTML = 'Sending your feedback<div class="loading"></div>';
		screens[1].style.display = 'block';
		let sendData = {
			feedback: popupBG.q('textarea')[0].value, useragent: navigator.userAgent, hash: document.location.hash,
			screenshot: chkScreenshot.checked ? popupBG.q('input[type="hidden"]')[0].value : '[NONE]'
		};
		if (typeof document.forms[0].from_date != 'undefined' && typeof document.forms[0].to_date != 'undefined'){
			sendData['from_date'] = document.forms[0].from_date.value;
			sendData['to_date'] = document.forms[0].to_date.value;
		}
		new arc.ajax(base_url+'feedback/',
			{method: 'POST',
			data: sendData,
			callback: function(data){
				screens[0].style.display = 'none';
				loading.style.display = 'block';
				popupBG.q('.card-wrap')[0].style.height = '80px';
				screens[1].innerHTML = '<i class="fa fa-check-circle-o"></i>Your feedback is received. Thank you!';
				//	Hide the thank you message after 4 seconds
				setTimeout(function(){
					//scrollTarget = 0;
					//showAlert('success', 'Your feedback is received. Thank you!');
					popupBGClose.onclick();
					popupBG.q('input[type="hidden"]')[0].value = '';
				}, 4200);
			},
			fallback: function(){
				alert('There was an error connecting to the server. Please retry after checking your connectivity.');
				submitBtn.disabled = false;
			}});
		return false;
	};
	chkScreenshot.onclick = function(){
		popupBG.q('#screenshot')[0].style.display = chkScreenshot.checked ? 'block' : 'none';
		popupBG.q('.draw-tools')[0].style.display = chkScreenshot.checked ? 'block' : 'none';
		popupBG.q('.card-wrap')[0].style.height = (popupBG.q('.one-row')[0].offsetHeight + 60) + 'px';
		popupBG.q('.card-wrap')[0].style.top = 'calc(50vh - 270px)';
	};
}

//	Collect usage statistics
var clicksBuffer = [];
document.body.onmousedown = function(e){
	var path = '';
	var inSVG = false;
	if (typeof e.path == 'undefined'){
		var tmp = e.target;
		while (tmp.parentNode != null){
			if (tmp.tagName == 'svg')
				inSVG = true;
			path = tmp.tagName + (tmp.id=='' ? '' : '#'+tmp.id) + (inSVG || typeof tmp != 'string' || tmp.className == '' ? '' : '.'+tmp.className.trim().replace(/ /g, '.')) + (path=='' ? '' : ' > ') + path;
			tmp = tmp.parentNode;
		}
	}
	else
		for (var i = e.path.length-6; i > -1; i--){
			if (e.path[i].tagName == 'svg')
				inSVG = true;
			path += (path=='' ? '' : ' > ') + e.path[i].tagName + (e.path[i].id=='' ? '' : '#'+e.path[i].id) + (inSVG || e.path[i].className=='' ? '' : '.'+e.path[i].className.trim().replace(/ /g, '.'));
		}
	if (arc.navigationActiveStack.length == 0)
		return true;
	var dat = [(new Date()).getTime(), arc.navigationActiveStack[arc.navigationActiveStack.length-1][0].id, path, e.screenX, e.screenY, window.innerWidth, document.body.scrollTop];
	clicksBuffer.push(dat);
};
function callDaddy(){
	if (clicksBuffer.length > 0){
		var dataToSend = clicksBuffer;
		clicksBuffer = [];
		new arc.ajax(base_url+'usage/', {
			method: 'POST',
			data: dataToSend,
			callback: function(){
				setTimeout(function(){callDaddy();}, 6600);
			},
			fallback: function(){
				setTimeout(function(){callDaddy();}, 12000);
			}
		});
	}
	else
		setTimeout(function(){callDaddy();}, 8500);
}
callDaddy();
window.onbeforeunload = function(){
	callDaddy();
};

// notifications
function Notifications () {

	var self = this;

	self.trigger = document.body.q('#notificationTrigger')[0];
	self.bar = {
		el: document.body.q('#notificationBar')[0],
		time: 200,
		maxHeight: 0,
		shown: false
	};

	self.stream = self.bar.el.q('.notifications_bar_content')[0];

	self.panels = null;

	self.rendering = false;

	self.data = null;

	self.append = function (elem) {
		self.stream.appendChild(elem);
	}

	self.toggleBar = function () {

		let init = null,
				instantHeight = parseInt(self.bar.el.style.height),
				height = (self.bar.shown = !self.bar.shown) ? 167 : 0;//self.bar.maxHeight

		let disp = height - parseInt(self.bar.el.style.height);

		self.stream.scrollTop = 0;
		closePanels();

		function animate (timestamp) {
			timestamp = timestamp || new Date().getTime();

			let runtime = timestamp - init;
			let progress = runtime / self.bar.time;
			progress = Math.min(progress, 1);

			let h = instantHeight + Math.floor(disp * progress);
			self.bar.el.style.height = h + 'px';

			if (runtime < self.bar.time) {
				window.requestAnimationFrame(function(timestamp) {
					animate(timestamp);
				});
			}
			else {
				if (self.bar.shown) {
					initPanels();
					self.trigger.addClass('open');
					self.bar.el.addClass('open');
				}
				else {
					self.trigger.removeClass('open');
					self.bar.el.removeClass('open');
				}
			}
		}

		window.requestAnimationFrame(function (timestamp) {
			init = timestamp || new Date().getTime();
			animate(timestamp);
		});

	}

	self.createPanel = function (options = {}) {

		let defaults = {
			id: null,
			title: {
				text: '',
				class: '',
				badge: null
			},
			body: [],
			type: 'collapse',
			hash: null,
			class: ''
		};

		let opts = Object.assign({}, defaults, options);

		let main_el = '',
				title_el = '',
				body_el = '';

		if (!opts.id) {
			throw "'id' is required for the 'createPanel' method.";
			return false;
		}

		if (opts.title.text != '') {
			// opts.title.badge = 100;
			if ( Number.isInteger(opts.title.badge) && opts.title.badge > 99) {
				opts.title.badge = '99<sup>+</sup>';
			}
			title_el += '<div class="panel_title ' + ((opts.title.badge) ? 'has-badge ' : '') + opts.title.class + '">';

			title_el += '<h3>';
			title_el += ((opts.title.badge) ? '<span class="badge">' + opts.title.badge + '</span> ' : '');
			title_el += '<span class="text">' + opts.title.text + '</span>';
			title_el += '</h3>';

			title_el += '<button class="discard"></button>';
			title_el += '</div>';
		}

		main_el = arc.elem('div', title_el, {class: 'panel ' + opts.type + ' ' + opts.class, 'data-id': opts.id, 'data-type': opts.type});

		if (opts.hash != null) {
			main_el.onclick = function (e) {
				location.hash = opts.hash;
			}
		}

		if (opts.body.length > 0) {
			let bd = '',
					ul = null;

			bd += '<div class="overflow">';
			bd += '<ul>';
			bd += '</ul>';
			bd += '</div>';

			body_el = arc.elem('div', bd, {class: 'panel_body'});

			ul = body_el.q('ul')[0];

			for (let el of opts.body) {
				let li = arc.elem('li','', {});
				li.append(el);
				ul.append(li);
			}

			main_el.append(body_el);
		}

		self.append(main_el);

		let p = getPanelObj(main_el);

		self.panels.push(p);
		addPanelFunctionality(p);
		refreshNotifications();
		return main_el;
	}

	self.removePanel = function (i) {
		let panelObj = self.panels[i],
				panel = panelObj.panel,
				init = null,
				instanceHeight = 0,
				time = 320,
				disp = 0,
				today = DateToShort(new Date());

		panel.style.overflow = 'hidden';
		panel.style.height = panel.clientHeight + 'px';

		instanceHeight = parseInt(panel.style.height);
		disp = 0 - parseInt(panel.style.height);

		function animate (timestamp) {
			timestamp = timestamp || new Date().getTime();

			let runtime = timestamp - init;
			let progress = runtime / self.bar.time;
			progress = Math.min(progress, 1);

			let h = instanceHeight + Math.floor(disp * progress);

			panel.style.height = h + 'px';
			panel.style.opacity = h / instanceHeight;

			if (runtime < time) {
				window.requestAnimationFrame(function(timestamp) {
			    animate(timestamp);
			  });
			} else {
				localStorage['notification-' + panelObj.id + '-closed-on'] = today;
				panel.style.height = '0px';
				panel.style.opacity = 0;
				self.stream.removeChild(panel);
				self.panels.splice(i, 1);
				refreshNotifications();
			}
		}

		window.requestAnimationFrame(function (timestamp) {
			init = timestamp || new Date().getTime();
			animate(timestamp);
		});

	}

	self.clearNotifications = function () {
		//*for (let panel of self.panels) {self.stream.removeChild(panel.panel);}
		self.stream.innerHTML = '';
		self.panels = [];
		refreshNotifications();
	}

	function getPanelObj(node) {
		let trigger = node.q('.panel_title')[0],
				body = node.q('.panel_body')[0],
				panel = {};

		panel.id = node.dataset.id;
		panel.type = node.dataset.type;
		panel.panel = node;
		panel.trigger = trigger;
		panel.body = body;
		panel.open = false;

		return panel;
	}

	function getPanels() {
		let panel_nodes = self.stream.q('.panel'),
				panels = [];

		for (let node of panel_nodes) {
			panels.push(getPanel(node));
		}

		return panels;
	}

	function closePanels () {
		for (let panel of self.panels) {
			if (panel.type === 'collapse') {
				panel.body.style.height = '0px';
				panel.trigger.removeClass('open');
				panel.open = false;
			}
		}
	}

	function addPanelFunctionality(panel) {
		let discard_btn = panel.trigger.q('button.discard')[0];

		if (panel.type === 'collapse') {
			panel.body.style.transition = 'height 320ms ease-in-out';
			panel.body.style.height = '0px';
		}

		panel.trigger.onclick = function (e) {
			if (e.target === discard_btn) {
				let i = self.panels.findIndex(function (z) {
					return panel.id === z.id;
				});
				if ( i > -1) {
					self.removePanel(i);
				}
				return false;
			}
			if (panel.type === 'collapse') {
				if (!panel.open) {
					panel.body.style.height = panel.body.scrollHeight + 'px';
					panel.panel.addClass('open');
					panel.open = true;
				} else {
					panel.body.style.height = '0px';
					panel.panel.removeClass('open');
					panel.open = false;
				}
			}

		}
	}

	function refreshNotifications () {
		if (self.panels.length === 0 && !self.rendering) {
			notificationsEmpty();
		} else {
			let hasEmptyEl = self.stream.q('.empty-notifications');
			if ( hasEmptyEl.length > 0 ) {
				for (let el of hasEmptyEl) {
					self.stream.removeChild(el);
				}
			}
		}
	}

	function notificationsEmpty () {
		let hasEl = self.stream.q('.empty-notifications').length;
		if (hasEl === 0) {
			if (organizationSettings.alert_new_employee == 0 &&
			organizationSettings.alert_below_yesterday == 0 &&
			organizationSettings.alert_below_period == 0 &&
			organizationSettings.alert_inactive_yesterday == 0 &&
			organizationSettings.alert_inactive_period == 0)
				var empty_el = arc.elem('div', 'You have disabled alerts for your activity feed, you can set it back up <a href="/settings/#alert">here</a>.', {class: 'empty-notifications'});
			else
				var empty_el = arc.elem('div', 'There are no alerts that need your attention as of now.', {class: 'empty-notifications'});
			self.append(empty_el);
		}
	}

	function initPanels () {
		refreshNotifications();
		for (let panel of self.panels) {
			addPanelFunctionality(panel);
		}
	}

	function setDimensions () {
		let h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		self.bar.maxHeight = h - (72 + 45); // (headerHeight + Feedback Btn)
		if (self.bar.shown) {
			self.bar.el.style.height = self.bar.maxHeight + 'px';
		}
	}

	function eventListeners () {
		window.addEventListener('resize', function () {
			setDimensions();
		});

		window.addEventListener('hashchange', function () {
			if (self.bar.shown) {
				self.toggleBar();
			}
		}, false);

		self.trigger.onclick = function () {
			self.toggleBar();
		}

		document.addEventListener('click', function (event) {
			let isClickedInside = self.bar.el.contains(event.target) || self.trigger.contains(event.target);
			if (!isClickedInside) {
				if (self.bar.shown) {
					self.toggleBar();
				}
			}
		});
	}

	self.getFeed = function (cb) {
		let startD = DateToShort(document.forms[0].from_date.value);
		let endD = DateToShort(document.forms[0].to_date.value);

		new arc.ajax( base_url + 'dashboard-alerts-ajax/?fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted(), {
			callback: function (data) {
				data = eval('('+data.responseText+')');
				cb(data);
			}
		});
	}

	self.renderNotification = function (getData = true) {
		let render = self.renderNotification;
		let period = dateFilter.options[dateFilter.selectedIndex].innerHTML.toLowerCase();
		self.rendering = true;
		self.clearNotifications();
		self.stream.innerHTML = '<div class="loading-notifications"><div class="loading-graph"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div></div></div>';

		function alertClosed (name) {
			let today = DateToShort(new Date());
			if (typeof localStorage['notification-'+name+'-closed-on'] == 'undefined') {
				return true;
			}

			if (localStorage['notification-'+name+'-closed-on'] - today == 0) {
				return false;
			} else {
				return true;
			}

		}

		function createAlert (key, title, cls) {
			let ne = [],
				href_base = (key === 'new_emp' ? '/settings/#employee/' : '#employee/');
			let emps = [];

			for (let k of self.data[key]) {
				let emp = self.data.employees[k[0]];
				emps.push([k[0], emp.fullname, k[1]]);
			}

			emps.sort(function (a, b) {
				return a[2] - b[2];
			});

			for (let i of emps) {
				let icon = usernameIcon(i[1]);
				let el = arc.elem('a', '<span class="bdr" style="background:#' + icon[1] + '">' + '</span>' + '<span class="user-name">' + i[1] + '</span><span class="score">' + (i[2] === '-' ? '-' : i[2].toFixed(0)) + '</span>', {
					href: href_base + i[0]
				});
				ne.push(el);
			}

			self.createPanel({
				id: key,
				title: {
					text: title,
					class: cls,
					badge: self.data[key].length
				},
				body: ne,
				class: cls
			});
		}

		function draw () {
			self.stream.innerHTML = '';

			// new employees
			if (
				organizationSettings.alert_new_employee === 1 &&
				self.data.new_emp.length > 0 &&
				alertClosed('new_emp')
			) {
				createAlert('new_emp', 'New employees', 'above');
			}

			// Yesterdays below
			if (
				organizationSettings.alert_below_yesterday == 1 &&
				self.data.yester_below.length > 0 &&
				alertClosed('yester_below')
			) {
				createAlert('yester_below', 'Below average for yesterday', 'below');
			}

			// Below for period
			if (
				organizationSettings.alert_below_period == 1 &&
				self.data.below.length > 0 &&
				alertClosed('below')
			) {
				createAlert('below', 'Below average for ' + (period === 'custom' ? 'period' : period), 'below');
			}

			// Inactive yesterday
			if (
				organizationSettings.alert_inactive_yesterday == 1 &&
				self.data.yester_inact.length > 0 &&
				alertClosed('yester_inact')
			) {
				createAlert('yester_inact', 'Inactive yesterday', 'below');
			}

			// Inactive
			if (
				organizationSettings.alert_inactive_period == 1 &&
				self.data.inact.length > 0 &&
				alertClosed('inact')
			) {
				createAlert('inact', 'Inactive for ' + (period === 'custom' ? 'period' : period), 'below');
			}

			self.rendering = false;
			refreshNotifications();
		}



		function constructor () {
			if (getData) {
				self.getFeed(function (data) {
					self.data = data;
					draw();
				});
			} else {
				draw();
			}
		}

		constructor();
	}

	function constructor () {
		self.bar.el.style.height = '0px';
		self.panels = getPanels();
		setDimensions();
		eventListeners();
	}

	constructor();

}

HTMLElement.prototype.slideDown = function (duration, callback) {

	let that = this,
			instanceHeight = 0,
			disp = 0,
			init = null,
			overflow = 'auto';

	duration = typeof duration !== 'undefined' ? duration : 320;
	overflow = this.style.overflow;

	this.style.overflow = 'hidden';
	this.style.height = '0px';

	instanceHeight = parseInt(this.style.height);
	disp = this.scrollHeight - parseInt(this.style.height);

	function animate (timestamp) {
		timestamp = timestamp || new Date().getTime();

		let runtime = timestamp - init;
		let progress = runtime / duration;
		progress = Math.min(progress, 1);

		let h = instanceHeight + Math.floor(disp * progress);

		that.style.height = h + 'px';

		if (runtime < duration) {
			window.requestAnimationFrame(function(timestamp) {
				animate(timestamp);
			});
		} else {
			that.style.overflow = overflow;
			that.style.height =  this.scrollHeight + 'px';
			if (typeof callback === "function") {
				callback(this);
			}
		}
	}

	window.requestAnimationFrame(function (timestamp) {
		init = timestamp || new Date().getTime();
		animate(timestamp);
	});

}

HTMLElement.prototype.slideUp = function (duration, callback) {
	let that = this,
			instanceHeight = 0,
			disp = 0,
			init = null,
			overflow = 'auto';

	duration = typeof duration !== 'undefined' ? duration : 320;
	overflow = this.style.overflow;

	this.style.overflow = 'hidden';
	this.style.height = this.scrollHeight + 'px';

	instanceHeight = parseInt(this.style.height);
	disp = 0 - parseInt(this.style.height);

	function animate (timestamp) {
		timestamp = timestamp || new Date().getTime();

		let runtime = timestamp - init;
		let progress = runtime / duration;
		progress = Math.min(progress, 1);

		let h = instanceHeight + Math.floor(disp * progress);

		that.style.height = h + 'px';

		if (runtime < duration) {
			window.requestAnimationFrame(function(timestamp) {
				animate(timestamp);
			});
		} else {
			that.style.overflow = overflow;
			that.style.height =  this.scrollHeight + 'px';
			if (typeof callback === "function") {
				callback(this);
			}
		}
	}

	window.requestAnimationFrame(function (timestamp) {
		init = timestamp || new Date().getTime();
		animate(timestamp);
	});
}

function hideSaveCancelButtons(context){
	context.q('div.fix-at-bottom')[0].style.opacity = 0;
}

function showSaveCancelButtons(context){
	context.q('div.fix-at-bottom')[0].style.opacity = 1;
}

function haveManagerRelationship(data, empId, managerId){
    if (typeof data.employees[empId] !== 'undefined' && typeof data.employees[empId].manager !== 'undefined' && parseInt(data.employees[empId].manager)===managerId){
        return true;
    } else if (typeof data.employees[empId] !== 'undefined' && typeof data.employees[empId].manager !== 'undefined'){
        // check for super manager relationship
        return haveManagerRelationship(data, parseInt(data.employees[empId].manager), managerId);
    } else {
    	return false;
    }
}
