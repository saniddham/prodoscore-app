
var pDict = {gm: 1, ch: 3, dc: 0, cl: 2, tb: 4, zc: 5, ze: 6, zt: 7, bs: 11, zl: 8, zi: 9, za: 10, sf: 12, sms: 13, cll: 14, pl: 15, po: 16, rcc: 17, rcs: 18, sfl:19, chm:20, cha:21};
var corrSearchQ = false, corrSelETypes = ['coos', 'locations', 'persons', 'organizations', 'others'], corrIntExtFilter = '', corrELimitFilter = 50;
var correlationsFilter = 'simple';

var moduleCorrelations = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		/*corrSelETypes = ['coos', 'locations', 'persons', 'organizations', 'others'];
		corrIntExtFilter = '';
		corrSearchQ = false;*/
		// -----------------------------------------------------------------------------------------------------
		let submitFiltersForm = context.q('form.employee-filters_left')[0];
		submitFiltersForm.onsubmit = function(){
			if (searchInput.value.trim() == '')
				corrSearchQ = false;
			else
				corrSearchQ = searchInput.value.trim().toLowerCase();
			//
			//loadCorrelations(context, params, true);
			return false;
		};
		// -----------------------------------------------------------------------------------------------------
		let filterMenuHeads = submitFiltersForm.q('.employee-filters_left nav.menu label[xclass="label"]');
		let resetAdvancedFilters = function(){
			let intExtFilters = submitFiltersForm.q('.employee-filters_left .visibility-toggle input[type="checkbox"][name^="channel-"]');
			intExtFilters[0].onclick = intExtFilters[1].onclick = function(){
				if (!intExtFilters[0].checked && !intExtFilters[1].checked){
					if (this == intExtFilters[0])
						intExtFilters[1].checked = true;
					else
						intExtFilters[0].checked = true;
				}
				filterMenuHeads[1].style.backgroundColor = '#afd9f1';
				if (intExtFilters[0].checked && !intExtFilters[1].checked)
					corrIntExtFilter = '&intext=1';
				else if (!intExtFilters[0].checked && intExtFilters[1].checked)
					corrIntExtFilter = '&intext=2';
				else if (intExtFilters[0].checked && intExtFilters[1].checked){
					corrIntExtFilter = '';
					filterMenuHeads[1].style.backgroundColor = '';
				}
				//
				loadCorrelations(context, params, true);
			};
			if (corrIntExtFilter == ''){
				intExtFilters[0].checked = true;
				intExtFilters[1].checked = true;
				filterMenuHeads[1].style.backgroundColor = '';
			}
			else if (corrIntExtFilter == '&intext=1'){
				intExtFilters[1].checked = false;
				filterMenuHeads[1].style.backgroundColor = '#afd9f1';
			}
			else if (corrIntExtFilter == '&intext=2'){
				intExtFilters[0].checked = false;
				filterMenuHeads[1].style.backgroundColor = '#afd9f1';
			}
			// -----------------------------------------------------------------------------------------------------
			let entropyLimit = submitFiltersForm.q('.employee-filters_left .visibility-toggle input[type="checkbox"][name^="top-"]');
			//submitFiltersForm.q('.employee-filters_left .visibility-toggle input[type="checkbox"][name="top-'+corrELimitFilter+'"]')[0].checked = true;
			entropyLimit[0].checked = true;
			entropyLimit[1].checked = false;
			entropyLimit[2].checked = false;
			entropyLimit[0].onclick = entropyLimit[1].onclick = entropyLimit[2].onclick = function(){
				if (!this.checked){
					this.checked = true;
					return false;
				}
				for (let i = 0; i < 3; i++)
					if (entropyLimit[i] != this)
						entropyLimit[i].checked = false;
				corrELimitFilter = 1 * this.name.split('-')[1];
				//
				loadCorrelations(context, params, true);
			};
		};
		resetAdvancedFilters();
		// -----------------------------------------------------------------------------------------------------
		let filterSections = submitFiltersForm.q('.filter-wrap');
		filterSections[3].style.display = 'none';
		filterSections[4].style.display = 'none';
		filterSections[5].style.display = 'none';
		filterSections[2].q('a')[0].onclick = function(){
			filterSections[2].style.display = 'none';
			filterSections[3].style.display = '';
			filterSections[4].style.display = '';
			filterSections[5].style.display = '';
			correlationsFilter = 'advanced';
			//corrIntExtFilter = ''; corrELimitFilter = 50;
			resetAdvancedFilters();
			return false;
		};
		filterSections[5].q('a')[0].onclick = function(){
			filterSections[2].style.display = '';
			filterSections[3].style.display = 'none';
			filterSections[4].style.display = 'none';
			filterSections[5].style.display = 'none';
			correlationsFilter = 'simple';
			corrIntExtFilter = '';
			corrELimitFilter = 50;
			loadCorrelations(context, params, true);
			resetAdvancedFilters();
			return false;
		};
		if (correlationsFilter == 'advanced')
			filterSections[2].q('a')[0].onclick();
		// -----------------------------------------------------------------------------------------------------
		etypeFilters = submitFiltersForm.q('.employee-filters_left .visibility-toggle input[type="checkbox"][name^="etype-"]');
		//for (let i = 0; i < etypeFilters.length; i++){let etype = etypeFilters[i].name.split('-')[1]+'s';etypeFilters[i].checked = corrSelETypes.indexOf( etype ) > -1;}
		let filterByEtype = function(){
			corrSelETypes = ['coos'];
			let hasAnyDeselect = false;
			for (let i = 0; i < etypeFilters.length; i++){
				if (etypeFilters[i].checked)
					corrSelETypes.push(etypeFilters[i].name.split('-')[1]+'s');
				else
					hasAnyDeselect = true;
			}
			filterMenuHeads[0].style.backgroundColor = hasAnyDeselect ? '#afd9f1' : '';
			//
			loadCorrelations(context, params, true);
		}
		for (let i = 0; i < etypeFilters.length; i++){
			let etype = etypeFilters[i].name.split('-')[1]+'s';
			let isChecked = corrSelETypes.indexOf( etype ) > -1;
			etypeFilters[i].onclick = filterByEtype;
			//etypeFilters[i].parentNode.style.color = colorDictionary[ etype ];
			let etypeItem = etypeFilters[i].parentNode.q('span.longer span')[0];
			etypeItem.innerHTML = '<i class="fa '+faDictionary[ etype ]+'" style="color:'+colorDictionary[ etype ]+';"></i> ' + etypeItem.innerHTML;
			etypeFilters[i].checked = isChecked;
			if (!isChecked)
				filterMenuHeads[0].style.backgroundColor = '#afd9f1';
		}
		// -----------------------------------------------------------------------------------------------------
		let searchInput = submitFiltersForm.q('#searchCorrelations')[0];
		let searchInputimeout = false;
		searchInput.onkeyup = function(){
			if (this.value.trim() == '')
				corrSearchQ = false;
			else
				corrSearchQ = this.value.trim().toLowerCase();
			//
			clearTimeout(searchInputimeout);
			searchInputimeout = setTimeout(function(){
				loadCorrelations(context, params, false);
			}, 3200);
		};
		if (corrSearchQ)
			searchInput.value = corrSearchQ;
		// -----------------------------------------------------------------------------------------------------
		/*new arc.nav('correlations/o', q('#correlationDetailsSubFrame')[0],
			function(context, params){
				let etype = params.shift();
				loadCorrelationDetails(context, etype, params);
			}
		);*/
		//
		loadCorrelations(context, params, true);
		sorttable.makeSortable(context.q('table.correlationsEntityData')[0]);
		sorttable.makeSortable(context.q('table.correlationsPplData')[0]);
		//
		if (typeof loaded == 'function')
			loaded();
	}
};

//	Data adapter for "correlations-ajax"
let selRow = false,
	correlationAjaxRequests = [false, false, false, false];
let connectionRetryCr = false,
	LoadCorrelationsAjaxData,
	LoadCorrelationsAjaxKey = false;
function LoadCorrelationsAjax(startD, endD, corrSelETypes, corrIntExtFilter, corrSearchQ, backendSearch, params, callback, fallback){
	let selETypes = [];
	for (let i = 0; i < corrSelETypes.length; i++)
		if (corrSelETypes[i] != 'coos')
			selETypes.push(corrSelETypes[i].substring(0,corrSelETypes[i].length-1));
	//
	if (LoadCorrelationsAjaxKey == startD+'-'+endD+'-'+corrIntExtFilter+'-'+corrELimitFilter+'-'+(backendSearch ? selETypes.join('-')+'-'+corrSearchQ : ''))
		callback(LoadCorrelationsAjaxData, false);
	else{
		if (correlationAjaxRequests[0])
			correlationAjaxRequests[0].abort();
		//
		correlationAjaxRequests[0] = new arc.ajax(base_url+'correlations-ajax/'+
				'?fromdate='+DateFromShort(startD).sqlFormatted()+
				'&todate='+DateFromShort(endD).sqlFormatted()+
				(backendSearch ?
					'&etype='+selETypes.join(',')+
					(corrSearchQ ? '&q='+corrSearchQ : '')
				: '')+
				corrIntExtFilter+
				'&limit='+corrELimitFilter+
				(typeof params.limit=='undefined'?'':'&limit='+params.limit), {
			callback: function(data){
				LoadCorrelationsAjaxKey = startD+'-'+endD+'-'+corrIntExtFilter+'-'+corrELimitFilter+'-'+(backendSearch ? selETypes.join('-')+'-'+corrSearchQ : '');
				LoadCorrelationsAjaxData = data.responseText;
				callback(data.responseText, true);
			},
			fallback: function(xmlhttp){
				fallback(xmlhttp.status);
				//	Display warning, Invalidate cache and retry same in 2.4 seconds
				clearTimeout(connectionRetryCr);
				connectionRetryCr = setTimeout(
					function(){
						LoadCorrelationsAjax(startD, endD, corrSelETypes, corrIntExtFilter, corrSearchQ, backendSearch, params, callback, fallback);
					}, 1200);
			}
		});
	}
}

let correlationDateRange = false;
function loadCorrelations(context, params, backendSearch){
	eventLogger('start', 'correlations');
	if (typeof backendSearch == 'undefined')
		backendSearch = false;
	//
	context.q('#correlationDetails')[0].style.display = 'none';
	context.q('#chart_correlations')[0].style.display = 'none';
	let loadCorrDetailsCont = function(){
		//if (params[0] == 'o') params.shift();
		//
		if (params.length > 1){
			let etype = params[1];
			loadCorrelationDetails(context, etype, params, corrIntExtFilter);
		}
		//
		let tableEntities = context.q('table.correlationsEntityData tbody')[0];
		let tableCOO = context.q('table.correlationsPplData tbody')[0];
		tableEntities.innerHTML = '';
		tableCOO.innerHTML = '';
		sorttable.makeSortable(tableEntities.parentNode);
		sorttable.makeSortable(tableCOO.parentNode);
		//
		let scrollTo = [false, 0];
		for (let etype in correlationData)
			if (corrSelETypes.indexOf(etype) > -1)
				for (eid in correlationData[ etype ])
					if (!corrSearchQ || (correlationData[ etype ][ eid ].title || (correlationData[ etype ][ eid ].name+' '+('' || correlationData[ etype ][ eid ].email)+' '+('' || correlationData[ etype ][ eid ].phone))).toLowerCase().indexOf(corrSearchQ) > -1){
						let label, row;
						if (etype == 'coos' || etype == 'persons'){
							label = (correlationData[ etype ][ eid ].name || correlationData[ etype ][ eid ].email || correlationData[ etype ][ eid ].phone || correlationData[ etype ][ eid ].title || '').trim();//'<div>'+//+'</div>'
							let line2 = (correlationData[ etype ][ eid ].phone || correlationData[ etype ][ eid ].email || '').trim();
							if (line2 != '' && line2 != label)
								label += '<small>('+line2+')</small>';
						}
						else
							label = correlationData[ etype ][ eid ].title;
						//
						let isThis = document.location.href.split('#')[1].split('/')[3] == eid;
						let className = (isThis ? 'highlight' : '');
						if (etype == 'coos'){
							row = arc.elem('tr', '<td>'+label+'</td>'+
											//'<td>'+taxonomyDictionary[ etype ]+'</td>'+
											'<td class="score" sorttable_customkey="'+correlationData[ etype ][ eid ].significance+'">'+correlationData[ etype ][ eid ].significance+'</td>',
										{'data-id': eid, 'data-etype': etype, 'data-label': label.split('<small>')[0], 'class': className});
							tableCOO.appendChild(row);
							if (isThis)
								scrollTo = [tableCOO, row];
						}
						else{
							row = arc.elem('tr', '<td><span class="circle fa '+faDictionary[ etype ]+'" style="background-color:'+colorDictionary[ etype ]+';" title="'+taxonomyDictionary[ etype ]+'"></span>'+label+'</td>'+
											//'<td>'+taxonomyDictionary[ etype ]+'</td>'+
											'<td class="score" sorttable_customkey="'+correlationData[ etype ][ eid ].significance+'">'+correlationData[ etype ][ eid ].significance+'</td>',
										{'data-id': eid, 'data-etype': etype, 'data-label': label, 'class': className});
							tableEntities.appendChild(row);
							if (isThis)
								scrollTo = [tableEntities, row];
						}
						row.onclick = function(){
							var eid = this.getAttribute('data-id');
							var etype = this.getAttribute('data-etype');
							if (selRow)
								selRow.removeClass('highlight');
							selRow = this;
							selRow.addClass('highlight');
							//
							document.location.href = '#correlations/o/'+etype+'/'+eid+'/'+this.getAttribute('data-label').toLocaleLowerCase().replace(/ /g, '-');
						};
					}
		sortColumn(tableEntities.parentNode, 1, false);
		sortColumn(tableCOO.parentNode, 1, false);
		if (scrollTo[0])
			scrollTo[0].scrollTop = scrollTo[1].offsetTop - (scrollTo[0].parentNode.offsetHeight / 3);
		//
		if (corrIntExtFilter == '&intext=1' && tableCOO.innerHTML == '')
			tableCOO.innerHTML = '<tr><td colspan="2" class="center" style="width:100%"><i>Filter internal does not apply for Contacts Outside Organization</i></td></tr>';
		//
		eventLogger('end', 'correlations');
	};
	//
	startD = DateToShort(document.forms[0].from_date.value);
	endD = DateToShort(document.forms[0].to_date.value);
	if (!backendSearch && correlationDateRange == startD+'-'+endD){
		loadCorrDetailsCont();
		return false;
	}
	correlationDateRange = startD+'-'+endD;
	if (startD > endD){
		showAlert('warning', 'Please set a To date after the From date.', 2400);
		return false;
	}
	//
	new LoadCorrelationsAjax(startD, endD, corrSelETypes, corrIntExtFilter, corrSearchQ, backendSearch, params,
		function(responseText, hasChanges){
			//if (!hasChanges)	return false;
			//
			context.q('table#empEngagement tbody')[0].innerHTML = '<td colspan="3"><i>Select a topic/contact</i></td>';
			context.q('table#prodUsage tbody')[0].innerHTML = '<td colspan="3"><i>Select a topic/contact</i></td>';
			//
			correlationData = JSON.parse(responseText);
			loadCorrDetailsCont();
		},
		function(status){
			if (status == 500)
				showAlert('error', 'Server Error.');
			else
				showAlert('warning', 'Couldn\'t connect to server. Please check your internet connection and retry.');
		}
	);
}

function loadCorrelationDetails(context, etype, params, corrIntExtFilter){
	let id = params[2];
	let correlationData = JSON.parse(LoadCorrelationsAjaxData);
	let collection = correlationData[etype];
	if (typeof collection == 'undefined' || typeof collection[id] == 'undefined'){
		//showAlert('warning', 'Selected entity was not found on the given period.');
		document.location.href = '#correlations';
		return false;
	}
	let correlationDetailTitle = (collection[id].title || collection[id].name || collection[id].email || collection[id].phone);
	//
	eventLogger('start', 'correlations-details');
	//
	correlationDetails.q('tbody')[0].innerHTML = '<tr><td class="center" colspan="4"><div class="chart">'+loadingIndicator+'</div></td></tr>';//'<i>Loading...</i>'
	//
	//	Load aggregated percentages
	empEngagement = context.q('table#empEngagement tbody')[0];
	loadAggregatedPercentages(params[2], context, corrIntExtFilter,
		function(dayData){
			empEngagement.style.height = '0px';
			setTimeout(function(){
				empEngagement.style.height = 'auto';
				empEngagement.style.transition = '';
			}, 500);
			//
			//	Draw chart
			let graphData = [['Date', (etype == 'coos' ? 'Interactions' : 'Mentions')]];
			let days = Object.keys(dayData);
			for (let j  = days.min(); j < days.max()+1; j++){
				let date = DateFromShort(j);
				//
				//if ( isWorkingDay(date.getDay()) )
				graphData.push([date, dayData[j]]);
				/*else
					graphData.push([date, 0]);*/
			}
			drawChartCorrelationChange(context.q('#chart_correlations .chart')[0], graphData,
				function(date, col){
					date = new Date(date[0]);
					filterCorrelationDetails('date', date.sqlFormatted(), params[2], corrIntExtFilter);
				}
			);
			//
			setTimeout(function () {
				eventLogger('end', 'correlations-details');
			}, 3);
		});
	//
	//	Load Details
	loadCorrelationTopicDetails(params[2]);
	//
	empEngagement.style.height = empEngagement.offsetHeight+'px';
	empEngagement.style.transition = 'height 0.5s';
	context.q('#chart_correlations')[0].style.display = 'block';
	context.q('#chart_correlations h2')[0].innerHTML = 'Trend for "'+correlationDetailTitle+'"';
	context.q('#engagement-title')[0].innerHTML = 'Engagement on "'+correlationDetailTitle+'"';
}

function loadAggregatedPercentages(id, context, corrIntExtFilter, callback) {
	//onScrolledToBottom = function(){};
	empEngagement = context.q('table#empEngagement tbody')[0];
	prodUsage = context.q('table#prodUsage tbody')[0];
	context.q('#correlationDetails')[0].style.display = 'none';

	if (correlationAjaxRequests[2]){
		correlationAjaxRequests[2].abort();
	}
	correlationAjaxRequests[2] = new arc.ajax(base_url+'correlations-ajax-aggr/?fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted()+'&entity='+id+corrIntExtFilter, {
		callback: function(data){
			eventLogger('end', 'correlations-details');
			statDetails = JSON.parse(data.responseText);
			//correlationDetailPassDownData.days = statDetails.days;
			callback(statDetails.days);
			//
			// clear the tables
			empEngagement.innerHTML = "";
			prodUsage.innerHTML = "";
			var tmpr, lastRow = false;

			for (var i = 0; i < statDetails.emp.length; i++) {
				var icon = usernameIcon(statDetails.emp[i].fullname);
				if (statDetails.emp[i].fullname == 'Others')
					statDetails.emp[i].picture = static_url + 'img/icons/others.jpeg';
				tmpr = arc.elem('tr',
					'<td>'+
						'<i class="self-icon" style="background-color:#'+icon[1]+';">'+
							(typeof statDetails.emp[i].picture == 'undefined' || statDetails.emp[i].picture == '' ? '<img src="'+static_url+'img/icons/profile.jpeg" />' : '<img src="'+statDetails.emp[i].picture+'" />')+
						'</i>'+
						statDetails.emp[i].fullname+'</td>'+
					'<td class="score">'+statDetails.emp[i].perc+'%</td>',
					{'data-id': statDetails.emp[i].id}
				);
				tmpr.onclick = function () {
					filterCorrelationDetails('employee', this.getAttribute('data-id'), id, corrIntExtFilter);	//	innerHTML.split('</td>')[0].split('</i>')[1]
				}
				if (statDetails.emp[i].fullname == 'Others')
					lastRow = tmpr;
				else
					empEngagement.appendChild(tmpr);
			}
			if (lastRow)
				empEngagement.appendChild(lastRow);

			for (var i = 0; i < statDetails.prod.length; i++){
				var icon = icons[statDetails.prod[i].name];
				tmpr = arc.elem('tr', '<td class="icon-col"><div class="icon icon-' +statDetails.prod[i].name + '"  title="'+products[statDetails.prod[i].name][0]+'"></div></td>'+
							'<td>'+products[ statDetails.prod[i].name ][1].name+'</td>'+//products[statDetails.prod[i].name][0]
							'<td class="score">'+(statDetails.prod[i].perc > 1 ? statDetails.prod[i].perc+'%' : '< 1%')+'</td>', {'data-id': products[statDetails.prod[i].name][0]});
				tmpr.onclick = function(){
					filterCorrelationDetails('product', this.getAttribute('data-id'), id, corrIntExtFilter);
				}
				prodUsage.appendChild(tmpr);
			}
			// ---------------------------
			var height = empEngagement.offsetHeight;
			empEngagement.style.height = '0px';
			empEngagement.style.transition = 'height 0.5s';
			//
			setTimeout(
				function(){
					empEngagement.style.height = height+'px';
					setTimeout(
						function(){
							empEngagement.style.height = 'auto';
							window.scrollTo( 0, window.innerHeight / 2 );
						}, 200);
				}, 10);
			//
			callback(statDetails.days);
		}
	});
}

function filterCorrelationDetails(col, val, entity_id, corrIntExtFilter, page){
	if (typeof page == 'undefined')
		page = 1;
	//
	/*let filter = '?entity='+entity_id+corrIntExtFilter;
	if (col == 'date'){
		filter += '&fromdate='+val+'&todate='+val;
	}
	else if (col == 'employee'){
		filter += '&employee='+val+'&fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted();
	}
	else if (col == 'product'){
		filter += '&product='+val+'&fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted();
	}*/
	//
	correlationRows = correlationDetails.q('tbody tr.tablerow.hasDetails');//context.q('#correlationDetails')[0]
	if (col == 'product'){
		for (var i = 0; i < correlationRows.length; i++)
			if (typeof val == 'object')
				correlationRows[i].style.display = (val.indexOf(correlationRows[i].cells[0].innerText.trim()) == -1) ? 'table-row' : 'none';
			else
				correlationRows[i].style.display = (correlationRows[i].cells[0].q('div.icon')[0].getAttribute('title') == val) ? 'table-row' : 'none';
	}
	else if (col == 'employee'){
		if (val.indexOf(',') > -1){
			val = val.split(',');
			for (var i = 0; i < correlationRows.length; i++)
				correlationRows[i].style.display = (val.indexOf(correlationRows[i].cells[2].getAttribute('data-emp-id')) > -1) ? 'table-row' : 'none';
		}
		else
			for (var i = 0; i < correlationRows.length; i++)
				correlationRows[i].style.display = (val == correlationRows[i].cells[2].getAttribute('data-emp-id')) ? 'table-row' : 'none';
				//correlationRows[i].style.display = (val == 'Others' || val == correlationRows[i].cells[2].innerText) ? 'table-row' : 'none';
	}
	//
	if (page == 1)
		window.scrollTo( 0, window.innerHeight );
}

function loadCorrelationTopicDetails(id, page) {
	if (typeof page == 'undefined')
		page = 1;
	//
	//if (typeof filter == 'undefined')
	filter = '?entity='+id+'&fromdate='+document.forms[0].from_date.value+'&todate='+document.forms[0].to_date.value+corrIntExtFilter;
	//
	if (correlationAjaxRequests[3])
		correlationAjaxRequests[3].abort();
	//
	correlationAjaxRequests[3] = new arc.ajax(base_url+'correlations-ajax-details/'+filter+'&page='+page, {
		callback: function(data){
			statDetails = JSON.parse(data.responseText);
			var table = correlationDetails.q('tbody')[0];
			correlationDetails.style.display = 'block';
			//*/
			if (page == 1)
				table.innerHTML = '';
			for (var i = 0; i < statDetails.length; i++){
				var icon = icons[statDetails[i].product];
				var row = arc.elem('tr',
								'<td width="20" class="icon-col" sorttable_customkey="'+statDetails[i].product+'">'+
									'<div class="icon" style="background-image:url(\''+icon+'\');" title="'+products[statDetails[i].product][0]+'"></div>'+
									(statDetails[i].x > 1 ? '<span class="icon-x">x'+statDetails[i].x+'</span>' : '')+
								'</td>'+
								'<td title="' + statDetails[i].title + '">'+
									'<span class="ellipsis">'+statDetails[i].title+'</span>'+
									'<table class="stat-details-inner"><tbody></tbody></table>'+
								'</td>'+
								'<td width="120" data-emp-id="'+statDetails[i].emp_id+'">'+statDetails[i].employee+'</td>'+//
								'<td width="180">'+statDetails[i].date+' '+mtsToHrs(statDetails[i].start_time)+'</td>'+
								'<td width="210" sorttable_customkey="' + statDetails[i].sentiment.score + '">'+sentimentWidget(statDetails[i].sentiment)+'</td>',
							{'class': 'tablerow hasDetails'+(statDetails[i].flag==1?' exclude':''), 'data-id': statDetails[i].id, 'data-product': statDetails[i].product});
				table.appendChild(row);
				row.onclick = expandRow;
			}
			// ---------------------------
			var height = table.offsetHeight;
			table.style.height = '0px';
			table.style.transition = 'height 0.5s';
			setTimeout(
				function(){
					table.style.height = height+'px';
					setTimeout(
						function(){
							table.style.height = 'auto';
						}, 500);
				}, 10);
			//
			sortColumn(table.parentNode, 3);
		}
	});
	var expandRow = function(){
		if (!organizationSettings['show_details'])
			return false;
		//
		var row = this;
		var detable = row.cells[1].q('table')[0];
		var id = row.getAttribute('data-id');
		var product = row.getAttribute('data-product');
		//	Toggle display of details of detail record
		if (row.className.indexOf('details') > -1 || row.className.indexOf('highlight') > -1){
			detable.style.height = detable.offsetHeight+'px';
			detable.style.transition = 'height 0.5s';
			setTimeout(
				function(){
					detable.style.height = '0px';
					setTimeout(
						function(){
							row.removeClass('details');
							row.removeClass('highlight');
							detable.style.height = 'auto';
							detable.style.transition = '';
						}, 500);
				}, 10);
		}
		else{
			row.addClass('details');
			loadDetailData(id, row, product, statDetails[id], function(){
				var height = detable.offsetHeight;
				detable.style.height = '16px';
				detable.style.transition = 'height 0.5s';
				setTimeout(
					function(){
						detable.style.height = height+'px';
						setTimeout(
							function(){
								detable.style.height = 'auto';
							}, 500);
					}, 10);
			});
		}
	};
}

// ===============================================================
//	Load metadata for a stat detail row - From JSON file in Cloud Storage file
function loadDetailData(id, row, product, detail, callback){
	if (!organizationSettings['show_details'])
		return false;
	//
	if (row.className.indexOf('sub') > -1){
		//	Especially for broadsoft, find the mail row
		row.removeClass('details highlight');
		row = (function(row){
				if (row.tagName == 'TR' && row.className.indexOf('tablerow') > -1 && row.className.indexOf('sub') == -1)
					return row;
				else
					return arguments.callee(row.parentNode);
			})(row);
		row.addClass('details highlight');
	}
	var detable, detables = row.cells[1].q('table tbody');
	//	Especially for broadsoft, there are sub rows inside a single detail row. We need to iterate for those
	var loaded = 0;
	for (var i = 0; i < detables.length; i++){
		detable = detables[i];
		if (detable.innerHTML != '' && detable.innerHTML != '<small>Loading ...</small>'){
			callback();
			return false;
		}
		//	Recursively navigate up to the TR with data-id to  find the detail record id
		id = (function(node){
				if (node.tagName == 'TR' && node.className.indexOf('tablerow') > -1 && node.getAttribute('data-id') != null)
					return node.getAttribute('data-id');
				else
					return arguments.callee(node.parentNode);
			})(detable);
		detable.innerHTML = '<small>Loading ...</small>';
		new arc.ajax(base_url + 'dashboard-detail-data/?id=' + id, {
			callback: function (data, ref) {//[id, detable, callback]
				var detable = ref[1];
				data = JSON.parse(data.responseText);
				var entities = data.entities;
				var sentiment = data.sentiment;
				data = data.data;
				detable.innerHTML = '';

				// Replace 'null' or empty string with '-'
				for (var key in data) {
					if (data[key] == null || data[key] == "") {
						data[key] = '-';
					}
				}

				// Google Drive file type icons
				if (product == 'dc'){
					if (typeof data.mime != 'undefined')
						detable.appendChild(arc.reactor({tr: {content: [
							{td: {content: 'File Type'}},
							{td: {content: '<img src="'+getIcon(data, data.name)+'" height="16" width="16" title="'+data.mime+'" /> ' + data.mime.replace('application/vnd.google-apps.', '')}}
						]}}));
				}
				//
				// Display phone call type
				else if (product == 'cll'){
					data.duration /= 60;
					if (typeof data.type != 'undefined'){
						if (data.type == 3 || isNaN(data.duration))//data.duration == '-'
							delete data.duration;
						data.type = ['', 'Incoming', 'Outgoing', 'Missed'][data.type];
					}
				}
				//
				// Prepend doller sign for ProsperWorks oppertunities and leads
				else if (product == 'po' ||  product == 'pl') {
					if (data.monetary_value != null) {
						data.monetary_value = '$' + data.monetary_value;
					}
					else {
						data.monetary_value = '-';
					}
				}
				//
				//	Hide duration for missed and failed RingCentral calls
				else if ( product == 'rcc' ){
					if (['Missed', 'Call Failed', 'Hang Up', 'Unknown', 'Rejected',
					'Receive Error', 'Blocked', 'No Answer', 'International Disabled',
					'Busy', 'Send Error', 'No Fax Machine', 'ResultEmpty', 'Suspended',
					'Call Failure', 'Internal Error', 'IP Phone Offline', 'Stopped',
					'International Restriction', 'Abandoned', 'Declined',
					'Fax Receipt Error', 'Fax Send Error'].indexOf(data.result) > -1) {
						delete data.duration;
					}
				}
				//
				// Show transcript of TurboBridge call
				else if ( product == 'tb' && typeof data.transcript != 'undefined' ){
					var transcriptRow = arc.reactor(
						{	tr: {content: [
								{td: {content: 'Transcript'}},
								{td: {content: '<div class="transcript-expand">'+data.transcript.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/ï»¿/g, '').replace(/\n/g, '<br/>')+'</div>'}}
							]}
						});
					detable.parentNode.parentNode.parentNode.parentNode.appendChild(transcriptRow);
					new showMoreBtn( transcriptRow.q('.transcript-expand')[0] );
					//transcriptRow.style.height = '100px';
					delete data.transcript;
				}
				//
				else if (product === 'cl') {
					if ( typeof data.duration !== 'undefined') data.duration = Math.abs(data.duration);
				}
				// ---------------------------------------------------------------------------------------------------------
				//	Actually insert rows
				// ---------------------------------------------------------------------------------------------------------
				if (data.constructor === Array){
					console.warn('Product '+product+' gives data in an array, not dict.!')
					for (var i = 0; i < data.length; i++)
						if (products[product][3].length==0 || typeof products[product][3][ data[i].name ] != 'undefined') {
							detable.appendChild(arc.reactor({tr: {content: [
								{td: {content: products[product][3][ data[i].name ]}},
								{td: {content: data[i].name == 'duration' ? mtsToTime(data[i].value) : data[i].value.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\n/g, '<br/>')}}//.replace(/ï»¿/g, '')
							]}}));
						}
				}
				else{
					var attribs = Object.keys(products[product][3]);
					for (var i = 0; i < attribs.length; i++){
						if (typeof data[attribs[i]] == 'undefined'){
							data[attribs[i]] = '-';
						}
						else{
							var attributeValue = data[attribs[i]];
							if (product == 'zc' &&  attributeValue.indexOf("Dialled") != -1){
								var newAttrbuteValue = attributeValue.replace("Dialled","Dialed");
								data[attribs[i]] = newAttrbuteValue;
							}
						}
						detable.appendChild(arc.reactor({
							tr: {
								content: [
									{td: {content: products[product][3][attribs[i]]}},
									{
										td: {
											content:
											attribs[i] == 'duration' ? // then
												mtsToTime(data[attribs[i]]) :
											// else
												(typeof data[attribs[i]] == 'object' ? // then
													JSON.stringify(data[attribs[i]]) :
												// else
													data[attribs[i]].toString().replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/ï»¿/g, '').replace(/\n/g, '<br/>')
												)
										}
									}
								]
							}
						}));
					}
				}
				// ---------------------------------------------------------------------------------------------------------
				//	Additional Rows
				// ---------------------------------------------------------------------------------------------------------
				if (product == 'gm' && typeof data.body != 'undefined'){
					var GmailBody = data.body.replace(/ï»¿/g, '');
					if (GmailBody != 'TEXT TOO LONG'){
						/*try{	//	This is temporary, untill the memcached old email bodies are removed.
							GmailBody = atob(GmailBody.replace(/_/g, '/').replace(/-/g, '+')).replace(/â/g, '').replace(/Â/g, ''); GmailBody = arc.elem('div', GmailBody); GmailBody = GmailBody.textContent || GmailBody.innerText || ''; }catch(e){}*/
						if (GmailBody.trim() == '')
							GmailBody = '<i>No text content</i>';//The email body contains either images or tables // THAT'S WRONG MESSAGE
					}
					detable.appendChild(arc.reactor({tr: {content: [
						{td: {content: 'Body'}},
						{td: {content: GmailBody}}
					]}}));
				}
				else if (product == 'ch'){
					detable.innerHTML = '';	//	Should We .?
					if (typeof data.from != 'undefined')
						detable.appendChild(arc.reactor({tr: {content: [
							{td: {content: 'From'}},
							{td: {content: data.from[0]+' &lt;'+data.from[1]+'&gt;'}}
						]}}));
					//
					if (typeof data.data != 'undefined'){
						var tmp = document.createElement("div"),
							tmpTitle = showEncrypted(data.data);
						tmp.innerHTML = tmpTitle;
						title = tmp.textContent || tmp.innerText || "<i>NO TEXT CONTENT</i>";
						if (title.length > 280) {
							title = title.substring(0,280) + '&hellip;';
						}
						try {
							title = decodeURIComponent(escape(title));
						} catch (err) {
							// some text cannot be decoded. Shouldn't cause an issue.
							// URIError: malformed URI sequence
						}
						//
						detable.appendChild(arc.reactor({tr: {content: [
							{td: {content: 'Message'}},
							{td: {content: title}}
						]}}));
					}
				}
				else if ( product == 'rcs' ){
					for (var j = 0; j < data.to.length; j++)
						detable.appendChild(arc.reactor({tr: {content: [
							{td: {content: 'To'}},
							{td: {content: data.to[j].phoneNumber+' ('+data.to[j].location+')'}}
						]}}));
				}
				//
				//	Load NLP entities related to this detail record
				if (Object.keys(entities).length > 0){
					var entitySpace = detable.appendChild(arc.reactor({tr: {content: [
						{td: {colspan: 2, class: 'entities'}}
					], style: 'width:calc(100vw - 280px);'}})).q('td')[0];
					for (entity in entities)
						if (isset(faDictionary[ entities[entity].etype+'s' ]))
							entitySpace.appendChild(arc.elem('span',
								'<i class="fa '+faDictionary[ entities[entity].etype+'s' ]+'" style="color:'+colorDictionary[ entities[entity].etype+'s' ]+';"></i> '+
								(entities[entity].title || entities[entity].name || entities[entity].email || entities[entity].phone),
								{class: 'nlpentity '+entities[entity].etype+' title'}));
/* TO DO: commenting out to enable this later since the accuracy of the data is not satisfying at the moment
						else if (entities[entity].etype == 'person'){
							if (typeof entities[entity].name != 'undefined')
								entitySpace.appendChild(arc.elem('span', '<i class="fa fa-user"></i> '+entities[entity].name, {class: 'nlpentity person name'}));
							else if (typeof entities[entity].email != 'undefined')
								entitySpace.appendChild(arc.elem('span', '<i class="fa fa-user"></i> '+entities[entity].email, {class: 'nlpentity person email'}));
							else if (typeof entities[entity].phone != 'undefined')
								entitySpace.appendChild(arc.elem('span', '<i class="fa fa-user"></i> '+entities[entity].phone, {class: 'nlpentity person phone'}));
						}
//*/
				}
				ref[2]();
			}
		},
		//	Reference object to be passed to Ajax callback
		[id, detable, function(){
						loaded += 1;
						if (detables.length == loaded)
							callback();
					}]);
	}
};

/*var CorrelationsWidget = function () {
	let self = this;
	self.el = null;
	self.entitiesEl = null;
	self.prevRequest = false;
	self.processedData = {};
	self.dataCallback = function () {
		if (organizationSettings.correlations_enabled !== 1) {
			return false;
		}
		var corrEntities = [];
		for (var type in self.processedData){
			for (var eid in self.processedData[type]){
				if (type == 'persons'){
					self.processedData[type][eid].title = '';
					if ( typeof self.processedData[type][eid].name != 'undefined' && self.processedData[type][eid].name != '' ) {
						self.processedData[type][eid].title += '<span class="person-name">' + self.processedData[type][eid].name + '</span>'
					}
					if ( typeof self.processedData[type][eid].email != 'undefined' && self.processedData[type][eid].email != '' ) {
						self.processedData[type][eid].title += '<span class="person-email">' + self.processedData[type][eid].email + '</span>'
					}
					if ( typeof self.processedData[type][eid].phone != 'undefined' && self.processedData[type][eid].phone != '' ) {
						self.processedData[type][eid].title += '<span class="person-phone">' + self.processedData[type][eid].phone + '</span>'
					}
				}
				else
					corrEntities.push([eid, type, self.processedData[type][eid]])
			}
		}
		corrEntities.sort(function (a, b) {
			return b[2].significance - a[2].significance;
		});
		if ( corrEntities.length > 0 ) {
			self.entitiesEl.innerHTML = '';
			for ( var t = 0; t < corrEntities.length; t++ ) {
				if (t > 4) {
					break;
				}
				var row = arc.elem('tr',
					'<td sorttable_customkey="' + corrEntities[t][2].title.toLowerCase() +'" data-type="'+corrEntities[t][1]+'">'+corrEntities[t][2].title+'</td>'+
					'<td class="type">'+taxonomyDictionary[ corrEntities[t][1] ]+'</td>'+
					'<td class="score">'+corrEntities[t][2].significance+'</td>',
					{ class: 'corr-widget-row' });
				row.addEventListener('click',(function (id) {
					return function () {
						document.location.hash = 'correlations/o/'+this.q('td')[0].getAttribute('data-type')+'/' + id + '/' + encodeURIComponent(this.q('td')[0].getAttribute('sorttable_customkey'));
					};
				}(corrEntities[t][0])));
				self.entitiesEl.appendChild(row);
			}
		}
		else {
			self.entitiesEl.innerHTML = '<tr><td colspan="3">No records to display</td></tr>';
		}
	};
	self.loadData = function (from, to, emp = '') {
		if (organizationSettings.correlations_enabled !== 1) {
			return false;
		}
		let q  = '', empQ = '';
		//empQ = '&employee=' + emp.join(',');
		if (Array.isArray(emp) && emp.length > 0) {
			empQ = '&employee=' + emp.join(',');
		} else if ( emp != '' ) {
			empQ = '&employee=' + emp;
		}
		q = 'fromdate=' + from + '&todate=' + to + '&limit=55' + empQ;
		self.prevRequest = new arc.ajax(
			base_url + 'correlations-ajax/?' + q,
			{
				callback: function (data) {
					correlationData = self.processedData = self.data = JSON.parse(data.responseText),
					self.dataCallback();
				}
			}
		);
	}
	self.init = function (el) {
		if (organizationSettings.correlations_enabled !== 1) {
			return false;
		}
		self.el = el;
		self.entitiesEl = el.q('table.card-table tbody')[0];
		// show loading bars
		self.entitiesEl.innerHTML = '<tr class="data-loading"><td colspan="3"><div class="loading-graph"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div></div></td></tr>';
	}
	return self;
}*/
