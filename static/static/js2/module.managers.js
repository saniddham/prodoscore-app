
module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		//	Breadcrumbs
		breadCrumbViewModel();
		breadCrumbViewModel.reset();
		breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
		breadCrumbViewModel.addCrumb('#managers', 'Managers', true);
		breadCrumbViewModel.render(context);
		//
		//	Initialize
		var employees = context.q('ul.employee-list');
		employees[0].innerHTML = employees[1].innerHTML = employees[2].innerHTML = '';
		//
		let continueLoading = function(data, self, startD, endD){
			let managers = [];
			employees[0].innerHTML = employees[1].innerHTML = employees[2].innerHTML = '';
			//	-----------------------------------------------------------------------------------------------
			//	Calculate Subordinate Scores
			//	-----------------------------------------------------------------------------------------------
			for (let i in data.employees)
				if (data.employees[i].status > 0 && typeof data.employees[i].scr != 'undefined' && data.employees[i].scr
					&& typeof data.employees[data.employees[i].manager] != 'undefined' && (organizationSettings.myself.role == 80 || data.employees[data.employees[i].manager].manager == uid)
				){
					if (typeof data.employees[data.employees[i].manager].subScore == 'undefined')
						data.employees[data.employees[i].manager].subScore = [data.employees[i].scr.l];
					else
						data.employees[data.employees[i].manager].subScore.push(data.employees[i].scr.l);
				}
			//	-----------------------------------------------------------------------------------------------
			//	Fill Managers to Strata Lists
			//	-----------------------------------------------------------------------------------------------
			for (let i in data.employees){
				if (typeof data.employees[i].subScore != 'undefined' && typeof data.employees[i].scr != 'undefined' && data.employees[i].scr){
					let icon = usernameIcon(data.employees[i].fullname);
					let className = strataStr(data.employees[i].strata);
					let employee = arc.reactor({
						li: {content:
							{a: {'data-id': i, 'data-role': data.employees[i].role, href: '#employees/under/'+i+'/'+data.employees[i].email, style: 'border-left:2px solid #'+icon[1],
								content: [{
										div: {class: 'user-name-wrap',
											content: {p: {class: 'user-name', content: data.employees[i].fullname}}
										}
									},
									{
										div: {class: 'right-holder',
											content: [
												{p: {class: 'score '+className[0], content: data.employees[i].scr.l.toFixed(0)}},
												{p: {}}
											]
										}
									}
								]
							}}
						}}
					);
					employees[2-data.employees[i].strata].a(employee);
					managers[i] = [data.employees[i].fullname, icon[1]];
				}
			}
			//	-----------------------------------------------------------------------------------------------
			var chartData = [['Date']], chartIndex = [];
			var chartDataBlank = [['Date']];//, chartIndexBlank = [];
			let colors = [];
			for (let id in managers)
				if (arrayIgnore.indexOf(id) == -1){
					chartData[0].push(managers[id][0]);
					chartDataBlank[0].push(managers[id][0]);
					colors.push('#'+managers[id][1]);
					chartIndex[id] = chartData[0].length-1;
					//chartIndexBlank[id] = chartDataBlank[0].length-1;
				}
			//
			//	Draw chart background only
			for (let i  = startD; i < endD+1; i++){
				let date = DateFromShort(i);
				if (isWorkingDay(date.getDay())){
					let row = [date];
					for (let id in managers)
						if (arrayIgnore.indexOf(id) == -1)
							row.push(0);
					chartDataBlank.push(row);
				}
				else
					chartDataBlank.push([date]);
			}
			new ArcChart(context.q('#managerChart')[0], chartDataBlank, colors,
				{
					chartArea:{left:40, right:20, bottom:60, top:10},
					fontFamily: 'Linotte-Regular',
					verticalRanges: verticalRanges,
					additionalData: {chartIndex: chartIndex},
					maxy: 100
				}).draw('column');
			//
			//	-----------------------------------------------------------------------------------------------
			//	Fill Average Subordinates Scores
			//	-----------------------------------------------------------------------------------------------
			for (let i in data.employees){
				let user = context.q('[data-id="'+i+'"]')[0];
				if (user != undefined){
					user = user.q('p')[2];
					user.className = 'score ';
					if (data.employees[i].subScore.length == 0)
						user.innerHTML = '-';
					else{
						let avg = data.employees[i].subScore.avg().toFixed(0);
						user.innerHTML = avg;
						user.className += strataClassname4Score(avg);
					}
				}
				//	-----------------------------------------------------------------------------------------------
				//	Fetch /employees-ajax/ [Prodoscore for a set of employees for a given period]
				//	-----------------------------------------------------------------------------------------------
				var chartData = [['Date']], chartIndex = [];
				var chartDataBlank = [['Date']];//, chartIndexBlank = [];
				let colors = [];
				for (let id in managers)
					if (arrayIgnore.indexOf(id) == -1){
						chartData[0].push(managers[id][0]);
						chartDataBlank[0].push(managers[id][0]);
						colors.push('#'+managers[id][1]);
						chartIndex[id] = chartData[0].length-1;
						//chartIndexBlank[id] = chartDataBlank[0].length-1;
					}
			}
			//
			//	-----------------------------------------------------------------------------------------------
			//	Fetch /employees-ajax/ [Prodoscore for a set of employees for a given period]
			//	-----------------------------------------------------------------------------------------------
			fillMissingEmployeeData(managers, data, startD, endD,
				function(managers, data){
					for (let i  = startD; i < endD+1; i++){
						let date = DateFromShort(i);
						if (isWorkingDay(date.getDay())){
							//
							let row = [date];
							for (let id in managers)
								if (arrayIgnore.indexOf(id) == -1)
									row.push(typeof data.employees[id].days[i] == 'undefined' ? 0 : data.employees[id].days[i].scr.l.toFixed(0));
							chartData.push(row);
						}
						else
							chartData.push([date]);
					}
					//	-----------------------------------------------------------------------------------------------
					//	Draw the Chart
					//	-----------------------------------------------------------------------------------------------
					//setTimeout(function(){
					drawChartManagers(context.q('#managerChart')[0], chartData, colors,
						function(row, col, options){
							let column = options.additionalData.chartIndex.indexOf(col);
							document.location.hash = 'employees/under/'+column+'/'+processedData.employees[column].email;
						},
						{chartIndex: chartIndex}
						);
					//}, 5);
				}
			);
		};
		//
		//	ENTRY POINT
		let normalLoading = function(){
			doFetchData('managers',
				function(data, self, startD, endD){
					continueLoading(data, self, startD, endD);
				}
			);
		};
		if (document.forms[0].filter.value == 'custom')
			normalLoading();
		else
			doFetchEmployeeList(document.forms[0].filter.value, document.forms[0].to_date.value,
				function(data, startD,endD ){
					processedData.employees = data;
					continueLoading({'employees': data}, organizationSettings.myself, startD, endD);
				}
			);
			/*new arc.ajax(base_url+'dashboard-employee-list-ajax/?period='+document.forms[0].filter.value.replace(/-/g, '_')+'&date='+document.forms[0].to_date.value, {
				callback: function(resp){
					let data = JSON.parse(resp.responseText);
					let startD = DateToShort(document.forms[0].from_date.value);
					let endD = DateToShort(document.forms[0].to_date.value);
					processedData.employees = data;
					continueLoading({'employees': data}, organizationSettings.myself, startD, endD);
				}
			});*/
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};

function drawChartManagers(context, chartData, colors, callback, additionalData){
	context.innerHTML = loadingIndicator;
	new ArcChart(context, chartData, colors,
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnclick: callback,
			verticalRanges: verticalRanges,
			additionalData: additionalData,
			maxy: 100
		}).draw('column');
}
