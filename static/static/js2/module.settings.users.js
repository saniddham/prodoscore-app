
var moduleSettingsUsers = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		hidePrevAlerts();
		eventLogger('start', 'loadSettingsUsers');
		new arc.ajax(base_url + 'update-employee/?filter=appusers', {
			callback: function (resp) {
				eventLogger('end', 'loadSettingsUsers');
				data = eval('(' + resp.responseText + ')');
		/*doFetchData('user-accounts',
			function (data, self, startD, endD) {*/
				var employees = context.querySelector('.employeedata').tBodies[0];
				sorttable.makeSortable(employees.parentNode);
				employees.innerHTML = '';
				//
				var addUser = context.querySelector('button.primary, button.gray');
				addUser.onclick = function () {
					document.location.hash = 'user/new';
				}
				//
				let found = false;
				for (var i in data.employees)
					if (data.employees[i].role > 69 && data.employees[i].is_app_user){
						//var stats = reProcessUserStats(data, i, ['icon', 'change']);
						var icon = usernameIcon(data.employees[i].fullname);
						var employee = arc.reactor({
							tr: {
								class: 'tablerow',
								'data-id': i,
								'data-role': data.employees[i].role,
								'data-manager': data.employees[i].manager,
								content: [
									{
										td: {
											content: {
												div: {
													class: 'user-name-wrap',
													content: {
														p: {
															class: 'user-name',
															content: data.employees[i].fullname
														}
													}
												}
											},
											style: 'border-left:2px solid #' + icon[1]
										}
									},
									{
										td: {
											content: data.employees[i].email
										}
									},
									{
										td: {
											content: roles[data.employees[i].role]
										}
									},
											],
								onclick: function () {
									document.location.hash = 'user/' + this.getAttribute('data-id');
								}
							}
						});
						employees.appendChild(employee);
						found = true;
					}
				//
				if (!found)
					employees.a(arc.elem('tr', '<td colspan="3"><i>No records found</i></td>', {
						class: 'no-records'
					}));
				//
				sortColumn(employees.parentNode, 0);
				scrollToTop();
			}
		});
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};