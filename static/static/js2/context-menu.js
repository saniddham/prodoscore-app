var showFlagModal = true,
		doNotShow 		= '';

doNotShow += '<div class="modal-body-donotshow">';
doNotShow += '<input type="checkbox" class="chk-default" onchange="doNotShowChange(this)" /> <span>Do not show this message for this session</span>';
doNotShow += '</div>';

function contextMenu(obj, menu){
	if (this == window)
		return new contextMenu(obj, menu);
	//
	obj.oncontextmenu = function(e, addata){
		e = e || window.event;
		/*if (('which' in e && e.which != 3) || ('button' in e && e.button != 2))
			return false;
		//*/
		if (typeof addata != 'undefined')
			obj.setAttribute('data-add', JSON.stringify(addata));
		/*e.cancelBubble = true;
		e.preventDefault();
		e.stopPropagation();*/
		var menuDom = arc.elem('ul', null, {class: 'context-menu', style: 'left: '+e.clientX+'px; top: '+e.clientY+'px;', tabindex: 0});
		for (var i in menu)
			new (function(obj, menuDom, mItemData){
				menuDom.appendChild(arc.elem('li', (typeof mItemData.icon != 'undefined' && mItemData.icon != false ? '<i class="fa '+mItemData.icon+'"></i>' : '') + mItemData.title))
					.onclick = function(){
						mItemData.onclick(obj);
						menuDom.onblur();
					};
			})(obj, menuDom, menu[i]);
		obj.parentNode.appendChild(menuDom);
		menuDom.focus();
		menuDom.onblur = function(){
			if (menuDom.parentNode == null)
				return false;
			try{
				menuDom.parentNode.removeChild(menuDom);
			}catch(e){}
			obj.removeAttribute('data-add');
			delete menuDom;
			//delete this;
		}
		menuDom.oncontextmenu = function(e){
			menuDom.onblur();
			//obj.oncontextmenu();
			return false;
		}
		return false;
	}
}

// -------------------------------------------------------------------------
//	Flag this activity
// -------------------------------------------------------------------------
var flagMenu = function (id, masterData, date, employee, evtElem, swapCmd) {
	var menuObj = this;
	return {
		'flag': {
			'icon': 'fa-ban',
			'title': 'Flag this activity',
			onclick: function (context) {
				if (context.hasAttribute('data-add'))
					id = JSON.parse(context.getAttribute('data-add'));
				//
				new arc.ajax(base_url + 'set-flag', {
					method: 'POST',
					data: {
						'id': id,
						action: 'flag'
					},
					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						if (typeof data['new-score'] != 'undefined') {
							if (typeof processedData.employees[employee].days[DateToShort(date)].scr != 'undefined')
								data['new-score'][employee].scr.p = processedData.employees[employee].days[DateToShort(date)].scr.p;
							processedData.employees[employee].days[DateToShort(date)] = data['new-score'][employee];
							if (typeof selDateDetails.detail[id] != 'undefined')
								selDateDetails.detail[id].flag = 4;
							EmployeeDOMRef.EmployeeDataDrilldownRow.style.display = 'none';
							//*/
							drawEmployeeProdoscoreChart(processedData, EmployeeDOMRef.context, [LoadedEmpId], EmployeeDOMRef, startD, endD);
							loadDetails(loadDetailsLastCall[0], loadDetailsLastCall[1], loadDetailsLastCall[2], processedData, EmployeeDOMRef, true,
								function(){
									if (typeof swapCmd != 'undefined' && !swapCmd)
										showDetails(this, date, employee, evtElem, false);
								});
							EmployeeDOMRef.context.querySelector('#prodoscore-for-date').innerHTML = data['new-score'][employee].scr.l.toFixed(0);
							//*/
							context.addClass('exclude');
							context.removeClass('include');
							// Rewrite data
							// Redraw graph
							// Reload details
							if (typeof swapCmd == 'undefined' || swapCmd){
								new contextMenu(evtElem, new unflagMenu(id, masterData, date, employee, evtElem));
								delete menuObj;
							}
							else{
								selectedChkBxes = [];
							}

							if (showFlagModal) {
								var flagModal = new modal({
									title: 'Activity flagged',
									body: 'The change you made might take a few minutes to affect the Prodoscore, we thank you for your patience.' + doNotShow,
									footer: '<button class="button gray modal-ok">OK</button>',
									destroyOnHide: true
								});
								flagModal.el.q('.modal-ok')[0].onclick = function () {
									flagModal.hide();
								};
								flagModal.show();
							}


							/*/
							/*if (confirm('Do you want to immidiately reload the new score.?'))
							else{*/
							//}
						}
						if (typeof data['error'] != 'undefined' && data['error'] == 'expired'){
							var unflagModal = new modal({
								title: 'Activity NOT flagged',
								body: 'Flagging functionality is only available for records within the last 7 days.',
								footer: '<button class="button gray modal-ok">OK</button>',
								destroyOnHide: true
							});
							unflagModal.el.q('.modal-ok')[0].onclick = function () {
								unflagModal.hide();
							};
							unflagModal.show();
						}
					}
				});
			}
		}
	};
};

// -------------------------------------------------------------------------
//	Unflag this activity
// -------------------------------------------------------------------------
var unflagMenu = function (id, masterData, date, employee, evtElem, swapCmd) {
	var menuObj = this;
	return {
		'unflag': {
			'icon': 'fa-check',
			'title': 'Unflag this activity',
			onclick: function (context) {
				if (context.hasAttribute('data-add'))
					id = JSON.parse(context.getAttribute('data-add'));
				//
				new arc.ajax(base_url + 'set-flag', {
					method: 'POST',
					data: {
						'id': id,
						action: 'unflag'
					},
					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						if (typeof data['new-score'] != 'undefined') {
							if (typeof processedData.employees[employee].days[DateToShort(date)].scr != 'undefined')
								data['new-score'][employee].scr.p = processedData.employees[employee].days[DateToShort(date)].scr.p;
							processedData.employees[employee].days[DateToShort(date)] = data['new-score'][employee];
							if (typeof selDateDetails.detail[id] != 'undefined')
								selDateDetails.detail[id].flag = 0;
							EmployeeDOMRef.EmployeeDataDrilldownRow.style.display = 'none';
							//*/
							drawEmployeeProdoscoreChart(processedData, EmployeeDOMRef.context, [LoadedEmpId], EmployeeDOMRef, startD, endD);
							loadDetails(loadDetailsLastCall[0], loadDetailsLastCall[1], loadDetailsLastCall[2], processedData, EmployeeDOMRef, true,
								function(){
									if (typeof swapCmd != 'undefined' && !swapCmd)
										showDetails(this, date, employee, evtElem, false);
								});
							EmployeeDOMRef.context.querySelector('#prodoscore-for-date').innerHTML = data['new-score'][employee].scr.l.toFixed(0);
							//*/
							context.removeClass('exclude');
							context.addClass('include');
							// Rewrite data
							// Redraw graph
							// Reload details
							if (typeof swapCmd == 'undefined' || swapCmd){
								new contextMenu(evtElem, new flagMenu(id, masterData, date, employee, evtElem));
								delete menuObj;
							}
							else{
								selectedChkBxes = [];
							}

							if (showFlagModal) {
								var unflagModal = new modal({
									title: 'Activity unflagged',
									body: 'The change you made might take a few minutes to affect the Prodoscore, we thank you for your patience.' + doNotShow,
									footer: '<button class="button gray modal-ok">OK</button>',
									destroyOnHide: true
								});
								unflagModal.el.q('.modal-ok')[0].onclick = function () {
									unflagModal.hide();
								};
								unflagModal.show();
							}
							/*/
							/*if (confirm('Do you want to immidiately reload the new score.?'))
							else{*/
							//}
						}
						if (typeof data['error'] != 'undefined' && data['error'] == 'expired'){
							var unflagModal = new modal({
								title: 'Activity NOT unflagged',
								body: 'Flagging functionality is only available for records within the last 7 days.',
								footer: '<button class="button gray modal-ok">OK</button>',
								destroyOnHide: true
							});
							unflagModal.el.q('.modal-ok')[0].onclick = function () {
								unflagModal.hide();
							};
							unflagModal.show();
						}
					}
				});
			}
		}
	};
};

function doNotShowChange (e) {
	var val = e.checked;
	showFlagModal = !val;
}
