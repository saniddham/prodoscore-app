
var noRecordsRow = [];
var mgrIndex = {};
var organizationSettings;
var activeFrame, scrollTarget, scrollDest = 0;

var page = 'employees';
var loadEmployeeId = -1, employees4Mgr = -1;

var navigationAnchors = q('ul.main-navigation li a');
var pageContent = q('.page-content')[0];
var app = q('#app')[0];

var newlyActivatedEmployees = [];

// ----------------------------------------------------------------------------------------

function initializeProdoScore(){
	if (typeof usernameIcon != 'function'){
		setTimeout(initializeProdoScore, 512);
		return false;
	}
	//
	new arc.ajax(base_url+'organization-settings/', {
			callback: function(data){
				var dat = eval('('+data.responseText+')');
				dat['workingdays'] = eval('('+dat['workingdays']+')');
				organizationSettings = dat;
				roles = organizationSettings.roles;
				//
				var userName = q('.user-name-wrap p.user-name')[0];
				var picture = userName.getAttribute('data-picture');
				var icon = usernameIcon(userName.innerHTML);
				//
				setMyProfileLink(organizationSettings.myself);
				userName.parentNode.insertBefore(arc.elem('div', (picture == '' ? '<p>'+icon[0]+'</p>' : ''),
					{class: 'circle blue', style: 'background-color:#'+icon[1]+'; border: 2px solid #'+icon[1]+'; background-image: url(\''+picture+'\');'}), userName);
				//
				initializeNavigation();
				//
				if (document.location.hash == '')
					document.location.hash = '#employees';
				window.onpopstate();
				//
				//q('.inner-wrap a')[0].href = '/settings/#employees';
				//
				var userName = q('a.user-name-wrap')[0];
				userName.removeAttribute('href');
				//
				var selDomainDivision = q('div.header-left')[0];
				var selDomainDrpDwn = selDomainDivision.q('select')[0];
				var addDomainBtn = selDomainDivision.q('a.button.primary')[0];
				//*/
				if (organizationSettings.myself.level)
					new arc.ajax(base_url+'register/get-domains/', {
						callback: function(data){
							var domains = eval('('+data.responseText+')');
							//
							selDomainDrpDwn.innerHTML = '';
							var hasOnboardingDomains = false;
							for (var i = 0; i < domains.length; i++){
								if (domains[i].status == 7){
									var option = arc.elem('option', domains[i].title, {value: domains[i].title})
									if (dat['domain'] == domains[i].title)
										option.selected = true;
									selDomainDrpDwn.a(option);
								}
								else
									hasOnboardingDomains = true;
							}
							//
							if (hasOnboardingDomains){
								//selDomainDivision.style.display = 'block';
								addDomainBtn.style.display = 'block';
							}
							else
								addDomainBtn.style.display = 'none';
							//
							if (domains.length == 1)
								selDomainDrpDwn.style.display = 'none';
							else
								selDomainDivision.style.display = 'block';
							//
							selDomainDrpDwn.onchange = function(){
								new arc.ajax(base_url+'register/select-domain/?switch-only=true&domain='+this.value, {
									callback: function(data){
										document.location.href = base_url;
									}
								});
							};
						},
						fallback: function(){
							selDomainDivision.style.display = 'none';		//selDomainDrpDwn.style.display = 'none'; addDomainBtn.style.display = 'none';
						}
					});
				else{
					selDomainDivision.style.display = 'none';		//selDomainDrpDwn.style.display = 'none'; addDomainBtn.style.display = 'none';
				}
				//*/
				//
				app.style.opacity = 1;
			},
			fallback: function(xmlhttp){
				if (xmlhttp.status == 500)
					alert('There was a server error while loading your organization settings. We will be looking into this.');
				else
					alert('Couldn\'t connect to server. Please check your internet connection and retry.');
			}
		}
	);
	//
	new function(){
		if (typeof initializeFeedbackForm == 'function')
			initializeFeedbackForm();
		else
			setTimeout(arguments.callee, 500);
	}();

	window.onscroll = function(){
		scrollTarget = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
		pageContent.style.top = '-'+scrollTarget+'px';
	}
	scrollDest = 0;
	scroll();
}

var scrollingTable, fixAtBottom;
function scroll(){
	activeFrame = q('.page-content .content.active, .page-content [xclass="content"].active');
	if (activeFrame.length > 0){
		activeFrame = activeFrame[activeFrame.length-1];
		app.style.height = (activeFrame.offsetHeight + 160)+'px';
		scrollingTable = activeFrame.q('table.sortable');
		fixAtBottom = activeFrame.q('div.fix-at-bottom');
		if (scrollingTable.length > 0 && fixAtBottom.length > 0)
			if (window.innerHeight - scrollingTable[0].getBoundingClientRect().top > 100)
				fixAtBottom[0].style.marginBottom = '3px';
			else
				fixAtBottom[0].style.marginBottom = '-40px';
	}
	//
	pageContent.style.top = '-'+scrollTarget+'px';
	setTimeout(scroll, 250);
	//
	if (scrollTarget > 100 && messages.innerHTML != ''){
		var remove = messages.q('.message');
		for (var i = 0; i < remove.length; i++)
			remove[i].onclick();
	}
}

// ----------------------------------------------------------------------------------------

function setMyProfileLink(myself){
	var userName = q('a.user-name-wrap')[0];
	var myProfile = q('ul.main-navigation.lower li[data-tag="myprofile"] a')[0];
	if (myself.status == 1){
		userName.setAttribute('href', '#employee/'+myself.id+'/'+myself.email);
		userName.setAttribute('title', myself.fullname);
		if (typeof myProfile != 'undefined'){
			myProfile.setAttribute('href', '#employee/'+myself.id+'/'+myself.email);
			myProfile.removeAttribute('title');
		}
	}
	else{
		userName.removeAttribute('href');
		userName.setAttribute('title', 'Your visibility is set to "Hidden", you can change this on the Employee settings page.');
		if (typeof myProfile != 'undefined'){
			myProfile.removeAttribute('href');
			myProfile.setAttribute('title', 'Your visibility is set to "Hidden", you can change this on the Employee settings page.');
		}
	}
}

// ----------------------------------------------------------------------------------------

function initializeNavigation(){

	new arc.nav('employees', app.q('#empSettings')[0],
		arc.require(script_path+'module.settings.employees.js', template_path+'settings.employees.html')
	);

	new arc.nav('employee', app.q('#employeeSettings')[0],
		arc.require(script_path+'module.settings.employee.js', template_path+'settings.employee.html')
	);

	new arc.nav('alert', app.q('#alertSettings')[0],
		arc.require(script_path+'module.settings.alert.js', template_path+'settings.alert.html')
	);

	if (organizationSettings.myself.role == 80){
		/*var turboBridgeSettings = app.q('#turboBridgeSettings')[0];
		if (turboBridgeSettings != null)
			new arc.nav('products/turbobridge', turboBridgeSettings, showTurboBridgeSettings);

		var broadSoftSettings = app.q('#broadSoftSettings')[0];
		if (broadSoftSettings != null)
			new arc.nav('products/phone-system', broadSoftSettings, showBroadSoftSettings);

		var crmSettings = app.q('#crmSettings')[0];
		if (crmSettings != null)
			new arc.nav('products/crm-system', crmSettings, showCRMSettings);

		var mobileSettings = app.q('#mobileSettings')[0];
		if (mobileSettings != null)
			new arc.nav('products/mobile', mobileSettings, showMobileSettings);*/

		new arc.nav('organization', app.q('#timeSettings')[0],// showTimeSettings
			arc.require(script_path+'module.settings.organization.js', template_path+'settings.organization.html')
		);

		new arc.nav('users', app.q('#userAccounts')[0],
			arc.require(script_path+'module.settings.users.js', template_path+'settings.users.html')
		);

		//var userSettings = app.q('#userSettings')[0];//userSettings
		new arc.nav('user', app.q('#userSettings')[0],
			arc.require(script_path+'module.settings.user.js', template_path+'settings.user.html')
		);

		new arc.nav('employees-bulk-add', app.q('#employeesBulkAdd')[0],
			arc.require(script_path+'module.settings.employees-bulk-add.js', template_path+'settings.employees-bulk-add.html')
		);

		let scoreSettings = app.q('#scoreSettings');
		if (scoreSettings.length > 0)
			new arc.nav('score', scoreSettings[0],
				arc.require(script_path+'module.settings.score.js', template_path+'settings.score.html')
			);

		let productSettings = app.q('#productSettings');
		if (productSettings.length > 0){
			var turboBridgeSettings = app.q('#turboBridgeSettings');
			var broadSoftSettings = app.q('#broadSoftSettings');
			var crmSettings = app.q('#crmSettings');
			//
			new arc.nav('products', productSettings[0],
				function(){
					if (turboBridgeSettings.length > 0)
						setTimeout(function(){document.location.href = '#products/turbobridge';}, 510);
					else if (broadSoftSettings.length > 0)
						setTimeout(function(){document.location.href = '#products/phone-system';}, 510);
					else if (crmSettings.length > 0)
						setTimeout(function(){document.location.href = '#products/crm-system';}, 510);
				}
			);
			//
			if (turboBridgeSettings.length > 0)
				new arc.nav('products/turbobridge', turboBridgeSettings[0],
					arc.require(script_path+'module.settings.products.conf-call.js', template_path+'settings.products.conf-call.html')
				);
			//
			if (broadSoftSettings.length > 0)
				new arc.nav('products/phone-system', broadSoftSettings[0],
					arc.require(script_path+'module.settings.products.phone-system.js', template_path+'settings.products.phone-system.html')
				);
			//
			if (crmSettings.length > 0)
				new arc.nav('products/crm-system', crmSettings[0],
					arc.require(script_path+'module.settings.products.crm-system.js', template_path+'settings.products.crm-system.html')
				);
		}
		//
		let billingSettings = app.q('#billingSettings');
		if (billingSettings.length > 0)
			new arc.nav('billing', billingSettings[0],
				arc.require(script_path+'module.settings.billing.js', template_path+'settings.billing.html')
			);
		/*var mobileSettings = app.q('#mobileSettings')[0];
		if (mobileSettings != null)
			new arc.nav('mobile', mobileSettings, showMobileSettings);*/
	}
	/*else{
		new arc.nav('employees-bulk-add', arc.elem('div'),
			function(context, params, e, loaded){
				alert('You need \'Administrator\' privileges.');
				setTimeout(function(){
					document.location.href = '#employees';
				}, 512);
			}
		);
	}*/
}

// =====================================================================================

var sidebar = q('div.sidebar')[0];
var sidebarNav = sidebar.q('nav.sidebar-nav')[0];
q('.header .fa.fa-bars')[0].onclick = function(){
	if (sidebar.className.indexOf('active') == -1){
		sidebar.addClass('active');
		sidebar.focus();
	}
}
q('div.page-content')[0].onclick = function(){
	sidebar.removeClass('active');
}
var sidebarBottomHeader = q('div.sidebar .bottom h2')[0];
if (typeof sidebarBottomHeader != 'undefined'){
	sidebarBottomHeader.onclick = function(){
		if (this.q('i.fa')[0].className == 'fa fa-caret-down'){
			this.parentNode.addClass('collapsed');
			this.q('i.fa')[0].className = 'fa fa-caret-up';
			sidebarNav.addClass('bottom-collapsed');
			localStorage['legend-collapsed'] = 'true';
		}
		else{
			this.parentNode.removeClass('collapsed');
			this.q('i.fa')[0].className = 'fa fa-caret-down';
			sidebarNav.removeClass('bottom-collapsed');
			localStorage['legend-collapsed'] = 'false';
		}
	}
	if (localStorage['legend-collapsed'] == 'true')
		sidebarBottomHeader.onclick();
}

function hidePrevAlerts() {
	var alertContainer = document.getElementById('messages');
	alertContainer.innerHTML = "";
}


// ==============================================================================================
// ==============================================================================================
//													FILTER AND SEARCH ITEMS
// ==============================================================================================

var searchEmployee, searchRole, filterByManager, retrieveTerminated, showNewEmployees;

function bindFilters(){
	searchEmployee = q('input[name="searchEmployee"]');
	searchRole = q('select[name="searchRole"]');
	filterByManager = q('select[name="searchManager"]');
	retrieveTerminated = q('input[name="retrieveTerminated"]')[0];
	showNewEmployees =  q('input[name="showNewEmployees"]')[0];
	//
	/*for (var i = 0; i < 2; i++){
		//if (arrayIgnore.indexOf(i) == -1) {
		new function(i) {*/
	// Populate Role Select
	searchRole[0].a(arc.elem('option', '[ Filter by ]', {value: '*'}));
	for (var j in roles) {
		if (j > 0 && arrayIgnore.indexOf(j) == -1 && roles[j] != '') {
			searchRole[0].a(arc.elem('option', roles[j], {value: j}));
		}
	}
	sortList(searchRole[0], 0);
		/*}(i);
		//}
	}*/
	// -----------------------------------------------------------------------------
	/*searchEmployee[0].onkeyup = function() {
		var employees = contentEmployees.querySelectorAll('tr.tablerow[data-id]');
		var found = false;
		var d_style = (employees[0].parentNode.parentNode.classList.contains('table--fixed-body') ? 'block' : 'table-row');

		for (var j = 0; j < employees.length; j++) {
			if ( ( this.value == '' || employees[j].querySelectorAll('td')[0].innerHTML.toLowerCase().indexOf(this.value.toLowerCase()) > -1 )
			&& (searchRole[0].value == '*' || employees[j].getAttribute('data-role') == searchRole[0].value)
			&& (i != 1 || retrieveTerminated.checked || employees[j].getAttribute('data-role') > 0) ){
				employees[j].style.display = d_style;
				found = true;

			}
			else {
				employees[j].style.display = 'none';
			}
		}
		noRecordsRow[0].style.display = found ? 'none' : d_style;
	}
	searchRole[0].onchange = function(e){
		searchEmployee[0].value = '';
		filterByManager[0].value = '*';
		if (this.value == '*')
			document.location.hash = 'employees';
		else
			document.location.hash = 'employees/role/' + this.value + '/' + this.options[this.selectedIndex].innerHTML.toLowerCase();
	}
	filterByManager[0].onchange = function(e){
		searchEmployee[0].value = '';
		searchRole[0].value = '*';
		if (this.value == '*'){
			employees4Mgr = -1;
			document.location.hash = 'employees';
		}
		else {
			document.location.hash = 'employees/under/' + this.value + '/' + (typeof processedData.employees[this.value] !== 'undefined' ? processedData.employees[this.value].email : processedData.self.email);
		}
	}*/
	function filterEmpList(){
		let filters = {};
		let employees = app.q('#empSettings')[0].q('tr.tablerow[data-id]');
		let found = false;
		let d_style = (employees[0].parentNode.parentNode.classList.contains('table--fixed-body') ? 'block' : 'table-row');
		let availableManagers = [];
		//
		filters.search = (searchEmployee[0].value === "" ? true : searchEmployee[0].value.toLowerCase());
		filters.manager = (filterByManager[0].value === "*" ? true : parseInt(filterByManager[0].value));
		filters.retrieveTerminated = retrieveTerminated.checked;
		filters.showNew = showNewEmployees.checked;
		filters.role = ( searchRole[0].value === "*" ? true : parseInt(searchRole[0].value));
		//
		for (let opts of filterByManager[0].options){
			if (opts.value !== "*") {
				availableManagers.push(parseInt(opts.value));
			}
		}
		function filterRoles(emp){
			if (filters.retrieveTerminated){
				searchRole[0].setAttribute('disabled', true);
				return (parseInt(emp.getAttribute('data-role')) < 0);
			}
			else if (filters.showNew){
				searchRole[0].setAttribute('disabled', true);
				return (parseInt(emp.getAttribute('data-role')) === 0);
			}
			else if ( (filters.role === true && parseInt(emp.getAttribute('data-role')) > 0)  || parseInt(emp.getAttribute('data-role')) === filters.role ){
				searchRole[0].removeAttribute('disabled');
				return true;
			}
			else{
				searchRole[0].removeAttribute('disabled');
				return false;
			}
		}
		function filterManager(emp){
			let empManager = parseInt(emp.getAttribute('data-manager'));

			if (filters.manager === 52 && availableManagers.indexOf(empManager) === -1) {
				return true;
			} else if (filters.manager === true || empManager === filters.manager) {
				return true;
			} else {
				return false;
			}
		}
		for (let emp of employees){
			filterManager(emp);
			if (
				(filters.search === true || emp.querySelectorAll('td')[0].innerHTML.toLowerCase().indexOf(filters.search) > -1) &&
				filterManager(emp) && filterRoles(emp)
			){

				emp.style.display = d_style;
				found = true;

			}
			else{
				emp.style.display = 'none';
			}
		}
		noRecordsRow[0].style.display = found ? 'none' : d_style;
	}
	let filterEmpListTimeout = false;
	searchEmployee[0].onkeyup =
	searchRole[0].onchange =
	filterByManager[0].onchange = function(e){
		clearTimeout(filterEmpListTimeout);
		filterEmpListTimeout = setTimeout(filterEmpList, 512);
	}
	//
	retrieveTerminated.onchange = function(e){
		if (this.checked){
			showNewEmployees.checked = false;
		}
		filterEmpList();
	}
	//
	showNewEmployees.onchange = function(e){
		if (this.checked){
			retrieveTerminated.checked = false;
		}
		filterEmpList();
	}
}

// ==============================================================================================
// ==============================================================================================
//													LOAD RAW DATA
// ==============================================================================================

function loadRawData(context, params){
	var rawdata = context.q('table.rawdata tbody')[0];
	rawdata.innerHTML = '';
	doFetchData('dashboard',
		function(data, self, startD, endD){
			context.style.height = '88vh';
			var th = context.q('table.rawdata thead tr');
			th[0].innerHTML = '<th class="vl" width="160">Employee</th>';
			th[1].innerHTML = '<th class="vl" width="160">Date</th>';
			var prodCount, lastSubCol;
			for (var id in data.employees)
				if (data.employees[id].status > 0 && data.employees[id].role > 0 && typeof data.employees[id].scr != 'undefined' && data.employees[id].scr){
					prodCount = 0;
					for (code in products){
						lastSubCol = arc.elem('th', code);
						th[1].a(lastSubCol);
						prodCount += 1;
					}
					th[1].a(arc.elem('th', 'Score', {class: 'vl'}));
					th[0].a(arc.elem('th', '<b>'+data.employees[id].fullname+'</b><br/>'+
												roles[data.employees[id].role]+'<br/>'+
												'<span>OWS: '+data.employees[id].scr.g.toFixed(2)+
												'<br/>RBS: '+data.employees[id].scr.r.toFixed(2)+
												'<br/>LRS: '+data.employees[id].scr.l.toFixed(2)+'</span>',
										{colspan: prodCount+1, class: 'vl'}));
				}
			rawdata.innerHTML = '';
			var newRow;
			for (date in data.days){
				newRow = arc.elem('tr', '<td class="vl" width="160">'+DateFromShort(date).sqlFormatted()+'</td>');
				for (var id in data.employees)
					if (data.employees[id].status > 0 && data.employees[id].role > 0 && typeof data.employees[id].scr != 'undefined' && data.employees[id].scr)
						if (data.employees[id].days[date] == null){
							for (code in products)
								newRow.a(arc.elem('td', '-'));
							//
							newRow.a(arc.elem('td', '-', {class: 'vl'}));
						}
						else{
							for (code in products)
								newRow.a(arc.elem('td', '&nbsp;'+(typeof data.employees[id].days[date].prod[code] != 'undefined' ? data.employees[id].days[date].prod[code] : '')));
							//
							newRow.a(arc.elem('td', '<span>OWS:&nbsp;'+data.employees[id].days[date].scr.g.toFixed(2)+'<br/>RBS:&nbsp;'+data.employees[id].days[date].scr.r.toFixed(2)+'<br/>LRS:&nbsp;'+data.employees[id].days[date].scr.l.toFixed(2)+'</span>', {class: 'vl'}));
						}
				rawdata.a(newRow);
			}
			//	Allow downloading to Excel file
			new contextMenu(context.q('table')[0], {
					'download-csv': {
						icon: 'fa-table',
						title: 'Download CSV file',
						onclick: function(rawdata){
							downloadCSV(rawdata, 'Raw Data from '+document.forms[0].from_date.value+' to '+document.forms[0].to_date.value+'.csv');
						}
					},
					'download-xlsx': {
						icon: 'fa-file-excel-o',
						title: 'Download Excel file',
						onclick: function(rawdata){
							downloadExcel(rawdata, 'Raw Data from '+document.forms[0].from_date.value+' to '+document.forms[0].to_date.value+'.xlsx');
						}
					}
				});

			context.onscroll = function(e){
				var rowHeads = q('.rawdata tr td:first-child, .rawdata tr th:first-child');//
				for (var i = 0; i < rowHeads.length; i++)
					rowHeads[i].style.left = this.scrollLeft+'px';
			}
		}
	);
}

initializeProdoScore();
