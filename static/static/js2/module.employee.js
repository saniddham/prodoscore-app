
let employeeCorrelationAjaxRequests = [false, false];
module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		var timelineRow = context.q('#timeline-row')[0];
		var EmployeeDataDrilldownRow = context.q('#drilldown-row')[0];
		LoadedEmpId = params[0];
		timelineRow.style.display = 'none';
		context.q('#employee-day-stats')[0].style.display = 'none';
		EmployeeDataDrilldownRow.style.display = 'none';
		EmployeeDOMRef = {context: context,
						timelineRow: timelineRow,
						EmployeeDataDrilldownRow: EmployeeDataDrilldownRow,
						timeline: timelineRow.q('#timeline')[0],
						busy5mGraph: context.q('#busy5m')[0],
						busyNLPGraph: context.q('#busyNLP')[0],
						busy5mHighlight: context.q('#busy5m-highlight')[0],
						busy5mHighlightInner: context.q('#busy5m-highlight-inner')[0],
						DetailDrilldownTbody: context.q('.stat-details tbody')[0]};
		sorttable.makeSortable(context.q('.stat-details')[0]);
		//
		var fromDate = document.forms[0].from_date.value,
			toDate = document.forms[0].to_date.value;
		if (organizationSettings.correlations_enabled === 1) {
			var empCorrTopics 		= context.q('.correlationsOrgData tbody')[0],
				empCorrContacts 	= context.q('.correlationsPplData tbody')[0];
			//
			empCorrTopics.innerHTML = '<tr class="data-loading"><td colspan="2"><div class="loading-graph"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div></div></td></tr>';
			empCorrContacts.innerHTML = '<tr class="data-loading"><td colspan="2"><div class="loading-graph"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div></div></td></tr>';
			var empTabs = new Tab({
				el: context.q('.employee-tabs')[0],
				onSelectTab: function (tab) {
					if ( tab.index === 1 ) {
						eventLogger('start', 'correlations-ajax');
						context.q('.emp-correlation-instances-table tbody')[0].innerHTML = '<tr class="no-records"><td colspan="3"><i>No records to display</i></td></tr>';
						context.q('.emp-correlation-instances-row')[0].style.display = 'none';
						//
						if (employeeCorrelationAjaxRequests[0])
							employeeCorrelationAjaxRequests[0].abort();
						employeeCorrelationAjaxRequests[0] = new arc.ajax( base_url + 'correlations-ajax/?fromdate=' + fromDate + '&todate=' + toDate + '&limit=55&employee=' + LoadedEmpId, {
							// doCache: true,
							callback: function (data) {
								data = eval('(' + data.responseText + ')');
								renderEmpCorrelations(data, context);
							}
						});
					}
				}
			});
			//
			empTabs.selectTab(0);
			context.q('.emp-correlation-instances-table tbody')[0].innerHTML = '<tr class="no-records"><td colspan="3"><i>No records to display</i></td></tr>';
			context.q('.emp-correlation-instances-row')[0].style.display = 'none';
		}
		else{
			context.q('nav.card-tabs-nav')[0].style.display = 'none';
			EmployeeDOMRef.DetailDrilldownTbody.parentNode.q('thead th.sortable_th')[1].style.display = 'none';
		}
		//
		doFetchData('managers',
			function(data, self, startD, endD){
				//
				if (typeof data.employees[LoadedEmpId] == 'undefined' && self.id!=LoadedEmpId){
					setTimeout(function(){
						document.location.hash = 'employees';
						showAlert('warning', 'You don\'t have permission to view users outside your domain.');
					}, 240);
					return false;
				}
				else if ( ! ( organizationSettings.myself.role == 80 || (LoadedEmpId in data.employees && data.employees[LoadedEmpId].manager == uid) || LoadedEmpId == uid ||
						(typeof data.employees[ data.employees[LoadedEmpId].manager ] != 'undefined' && haveManagerRelationship(data, LoadedEmpId, uid))) ){
					setTimeout(function(){
						document.location.hash = 'employees';
						showAlert('warning', 'You don\'t have permission to view details of that user.');
					}, 240);
					return false;
				}
				//
				fillEmployeeDetails(context, data, params[0]);
				let user_data = LoadedEmpId in data.employees ? data.employees[LoadedEmpId] : organizationSettings.myself
				//
				setTimeout(function () {
					breadCrumbViewModel();
					breadCrumbViewModel.reset();
					breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
					breadCrumbViewModel.addCrumb('#employees', 'Employees', false);
					breadCrumbViewModel.addCrumb('#employees/role/' + user_data.role + '/', roles[user_data.role], false);
					breadCrumbViewModel.addCrumb(location.hash, user_data.fullname, true);
					breadCrumbViewModel.render(context);
				}, 200);
				//
				context.q('#oneEmployeeChart')[0].innerHTML = loadingIndicator;
				context.q('#employee-day-stats')[0].style.display = 'none';
				//
				user_data = LoadedEmpId in processedData.employees ? processedData.employees[LoadedEmpId] : processedData.self
				if (typeof user_data.days != 'undefined' && Object.keys(user_data.days).length > 0)
					//	If already have data, use it - draw the chart
					setTimeout(function(){
						drawEmployeeProdoscoreChart(processedData, context, params, EmployeeDOMRef, startD, endD);
					}, 10)
				else
					//	Fetch data for the chart from server
					new arc.ajax(base_url+'employee-ajax/?fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted()+'&employee='+LoadedEmpId, {
							callback: function(data){
								//	Store the data
								data = eval('('+data.responseText+')');
								if (LoadedEmpId in processedData.employees)
								    processedData.employees[LoadedEmpId].days = data.days;
								else if (parseInt(LoadedEmpId) === processedData.self.id)
								    processedData.self.days = data.days;
								//	Draw the chart
								drawEmployeeProdoscoreChart(processedData, context, params, EmployeeDOMRef, startD, endD);
							},
							fallback: function(xmlhttp){
								if (xmlhttp.status == 500)
									showAlert('error', 'Server Error.');
								else
									showAlert('warning', 'Couldn\'t connect to server. Please check your internet connection and retry.');
							}
						}
					);
				//
				if (self.role < 8 && self.email == data.employees[params[0]].email)
					context.q('button.user-settings-btn')[0].style.display = 'none';
				else{
					context.q('button.user-settings-btn')[0].style.display = 'inline-block';
					context.q('button.user-settings-btn')[0].onclick = function(){
						document.location = 'settings/#employee/'+params[0];
					}
				}
				//
				scrollToTop();
			}
		);
		//	-----------------------------------------------------------------------------------------------
		//	NLP toggle functionality
		//	-----------------------------------------------------------------------------------------------
		//context.q('#busyNLP')[0].parentNode.q('h2 input[type="checkbox"]')[0].onclick = function(){
		//	context.q('#busyNLP')[0].style.display = this.checked ? 'block' : 'none';
		//}
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};

// -------------------------------------------------------------------------
//	Employee Prodoscore - Chart
// -------------------------------------------------------------------------
function drawEmployeeProdoscoreChart(processedData, context, params, DOMRef, startD, endD) { //, callback
    let user_data = params[0] in processedData.employees ? processedData.employees[params[0]] : processedData.self;
	var icon = usernameIcon(user_data.fullname);
	var scores = context.q('.right-holder .score');
	var individualDate = context.querySelector('#individual-date');
	var prodoscoreForDate = context.querySelector('#prodoscore-for-date');
	//
	var chartData = [['Date', prevPeriodTxt(), 'Score']]; //'Previous period'
	var dataIncomplete = [];
	for (var i = startD; i <= endD; i++) {
		var date = DateFromShort(i);
		if (isWorkingDay(date.getDay())) {
			if (typeof user_data.days[i] != 'undefined') {
				chartData.push([date,
					user_data.days[i].scr.p ? user_data.days[i].scr.p.toFixed(0) : null,
					user_data.days[i].scr.l.toFixed(0)]); //
			}
			//
			if (typeof user_data.days[i] == 'undefined') // || Object.keys(processedData.employees[params[0]].days[i].prod).length == 0
				dataIncomplete.push({
					p: 'All',
					d: date.sqlFormatted()
				});
			//
			else if (typeof user_data.days[i].warning != 'undefined') {
				var tmp = user_data.days[i].warning.incomplete;
				var prodName = ((typeof products[tmp.p][1].name !== 'undefined' && products[tmp.p][1].name !== '') ? products[tmp.p][1].name : products[tmp.p][0].charAt(0).toUpperCase() + products[tmp.p][0].substring(1).replace(/_/g, ' '));
				dataIncomplete.push({
					p: prodName,
					d: tmp.d
				});
			}
		} else
			chartData.push([date]);
	}
	var warning = context.q('#oneEmployeeChart')[0].parentNode.q('h2 i.warning')[0];
	if (dataIncomplete.length > 0) {
		warning.className = 'warning warn';
		var tmp = 'Following data is missing\non the graph at the moment:';
		for (var i = 0; i < dataIncomplete.length; i++)
			tmp += '\n' + dataIncomplete[i].p + ' on ' + dataIncomplete[i].d;
		warning.setAttribute('title', tmp);
	} else {
		warning.className = 'warning';
		warning.setAttribute('title', '');
	}
	//
	//	Draw employee chart
	drawChartEmployee(context.q('#oneEmployeeChart')[0], chartData, icon, processedData.prev_offset,
		//	When a day (column) on the graph is clicked
		function (row, col) {
			//	Only allow clicking current period column
			if (col != 2)
				return false;
			//
			loadDetails(row[0], params[0], scores, processedData, DOMRef);
			individualDate.innerHTML = row[0].toString().substring(0, 15);
			prodoscoreForDate.innerHTML = row[2];
		}, function (row) {
			loadDetails(row[0], params[0], scores, processedData, DOMRef);
			individualDate.innerHTML = row[0].toString().substring(0, 15);
			prodoscoreForDate.innerHTML = row[2];
		});
	//
	for (var i = 0; i < 11; i++)
		scores[i].parentNode.parentNode.onclick = false;
}


// -------------------------------------------------------------------------
//	Employee Correlations
// -------------------------------------------------------------------------
function renderEmpCorrelations(data, context){
	eventLogger('end', 'correlations-ajax');
	let self = this;
	self.data = data;
	//
	/*self.row = function (id, className, title, significance, sortkey) {
		let row = arc.elem('tr', '<td sorttable_customkey="' + sortkey.toLowerCase() + '">' + title + '</td><td class="score">' + significance + '</td>', {class: 'corr-org '+className, 'data-id': id});
		row.onclick = function () {
			eventLogger('start', 'correlations-ajax-details');
			let topics = empCorrTopics.q('tr'),
					cods = empCorrContacts.q('tr');
			//
			for (let t of topics) {
				t.removeClass('active');
			}
			//
			for (let c of cods) {
				c.removeClass('active');
			}
			//
			row.addClass('active');
			//
			var fromDate = document.forms[0].from_date.value,
				toDate = document.forms[0].to_date.value;
			if (employeeCorrelationAjaxRequests[1])
				employeeCorrelationAjaxRequests[1].abort();
			employeeCorrelationAjaxRequests[1] = new arc.ajax( base_url + 'correlations-ajax-details/?entity=' + id + '&fromdate=' + fromDate + '&todate=' + toDate + '&employee=' + LoadedEmpId, {
				callback: function (data) {
					data = eval('(' + data.responseText + ')');
					setTimeout(function () {
						window.scrollTo( 0, window.innerHeight / 2 );
					}, 300);
					renderEmpCorrelationDetails(data, context);
				},
				fallback: function () {
					showAlert('error', 'A server error occoured');
					renderEmpCorrelationDetails([], context);
				}
			});
		}
		return row;
	}
	//
	var empCorrTopics 	= context.q('.correlationsOrgData tbody')[0],
		empCorrContacts 	= context.q('.correlationsPplData tbody')[0]; // if (typeof empCorrTopics == 'undefined')
	empCorrTopics.innerHTML = '<tr class="no-records"><td colspan="2">No records to display</td></tr>';
	empCorrContacts.innerHTML = '<tr class="no-records"><td colspan="2">No records to display</td></tr>';
	sorttable.makeSortable(empCorrTopics.parentNode);
	sorttable.makeSortable(empCorrTopics.parentNode);
	//
	if (Object.keys(self.data.organizations).length > 0) {
		empCorrTopics.innerHTML = '';
	}
	if (Object.keys(self.data.coos).length > 0) {
		empCorrContacts.innerHTML = '';
	}
	for (let org in self.data.organizations) {
		let row = self.row(org, 'topic', self.data.organizations[org].title, self.data.organizations[org].significance, self.data.organizations[org].title);
		empCorrTopics.appendChild(row);
	}
	for (let cod in self.data.coos) {
		let nameOrEmail = '';
		if (typeof self.data.coos[cod].name != 'undefined') {}
			nameOrEmail = '<span class="person-name">' + self.data.coos[cod].name + '</span>';
		if (typeof self.data.coos[cod].email != 'undefined')
			nameOrEmail += '<span class="person-email">(' + self.data.coos[cod].email + ')</span>';
		if (typeof self.data.coos[cod].phone != 'undefined')
			nameOrEmail += '<span class="person-phone">' + self.data.coos[cod].phone + '</span>';
		let row = self.row(cod, 'topic', nameOrEmail, self.data.coos[cod].significance, self.data.coos[cod].name || self.data.coos[cod].email || self.data.coos[cod].phone || '');
		empCorrContacts.appendChild(row);
	}
	sortColumn(empCorrTopics.parentNode, 1, false);
	sortColumn(empCorrContacts.parentNode, 1, false);*/
	//
	let tableEntities = context.q('.correlationsOrgData tbody')[0];
	let tableCOO = context.q('.correlationsPplData tbody')[0];
	tableEntities.innerHTML = '';
	tableCOO.innerHTML = '';
	sorttable.makeSortable(tableEntities.parentNode);
	sorttable.makeSortable(tableCOO.parentNode);
	//
	let scrollTo = [false, 0];
	for (let etype in data)
		for (eid in data[ etype ]){
			let label, row;
			if (etype == 'coos' || etype == 'persons'){
				label = (data[ etype ][ eid ].name || data[ etype ][ eid ].email || data[ etype ][ eid ].phone || data[ etype ][ eid ].title || '').trim();//'<div>'+//+'</div>'
				let line2 = (data[ etype ][ eid ].phone || data[ etype ][ eid ].email || '').trim();
				if (line2 != '' && line2 != label)
					label += '<small>('+line2+')</small>';
			}
			else
				label = data[ etype ][ eid ].title;
			//
			let isThis = document.location.href.split('#')[1].split('/')[3] == eid;
			let className = (isThis ? 'highlight' : '');
			if (etype == 'coos'){
				row = arc.elem('tr', '<td>'+label+'</td>'+
								//'<td>'+taxonomyDictionary[ etype ]+'</td>'+
								'<td class="score" sorttable_customkey="'+data[ etype ][ eid ].significance+'">'+data[ etype ][ eid ].significance+'</td>',
							{'data-id': eid, 'data-etype': etype, 'data-label': label.split('<small>')[0], 'class': className});
				tableCOO.appendChild(row);
				if (isThis)
					scrollTo = [tableCOO, row];
			}
			else{
				row = arc.elem('tr', '<td><span class="circle fa '+faDictionary[ etype ]+'" style="background-color:'+colorDictionary[ etype ]+';" title="'+taxonomyDictionary[ etype ]+'"></span>'+label+'</td>'+
								//'<td>'+taxonomyDictionary[ etype ]+'</td>'+
								'<td class="score" sorttable_customkey="'+data[ etype ][ eid ].significance+'">'+data[ etype ][ eid ].significance+'</td>',
							{'data-id': eid, 'data-etype': etype, 'data-label': label, 'class': className});
				tableEntities.appendChild(row);
				if (isThis)
					scrollTo = [tableEntities, row];
			}
			/*row.onclick = function(){
				var eid = this.getAttribute('data-id');
				var etype = this.getAttribute('data-etype');
				if (selRow)
					selRow.removeClass('highlight');
				selRow = this;
				selRow.addClass('highlight');
				//
				document.location.href = '#correlations/o/'+etype+'/'+eid+'/'+this.getAttribute('data-label').toLocaleLowerCase().replace(/ /g, '-');
			};*/
			row.onclick = function () {
				eventLogger('start', 'correlations-ajax-details');
				let topics = tableEntities.q('tr'),
					cods = tableCOO.q('tr');
				//
				for (let t of topics) {
					t.removeClass('active');
				}
				//
				for (let c of cods) {
					c.removeClass('active');
				}
				//
				this.addClass('active');
				//
				var fromDate = document.forms[0].from_date.value,
					toDate = document.forms[0].to_date.value;
				if (employeeCorrelationAjaxRequests[1])
					employeeCorrelationAjaxRequests[1].abort();
				employeeCorrelationAjaxRequests[1] = new arc.ajax( base_url + 'correlations-ajax-details/?entity=' + this.getAttribute('data-id') + '&fromdate=' + fromDate + '&todate=' + toDate + '&employee=' + LoadedEmpId, {
					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						setTimeout(function () {
							window.scrollTo( 0, window.innerHeight / 2 );
						}, 300);
						renderEmpCorrelationDetails(data, context);
					},
					fallback: function () {
						showAlert('error', 'A server error occoured');
						renderEmpCorrelationDetails([], context);
					}
				});
			};
		}
	sortColumn(tableEntities.parentNode, 1, false);
	sortColumn(tableCOO.parentNode, 1, false);
}

function renderEmpCorrelationDetails(data, context){
	eventLogger('end', 'correlations-ajax-details');
	//
	var output = context.q('.emp-correlation-instances-table tbody')[0],
		row = context.q('.emp-correlation-instances-row')[0];
	//
	if (data.length === 0) {
		output.innerHTML = '<tr class="no-records"><td colspan="3"><i>No records to display</i></td></tr>';
		eventLogger('end', 'correlations-ajax-details');
		row.style.display ='block';
		return true;
	}
	else{
		output.innerHTML = '';
	}
	for (let i of data){
		let icon = icons[i.product];
		let el = '';
		el += '<td width="20" class="icon-col" sorttable_customkey="' + i.product + '"><div class="icon" style="background-image:url(\''+ icon + '\');" title="' + products[i.product][0] + '"></div>' + (i.x > 1 ? '<span class="icon-x">x' + i.x + '</span>' : '') + '</td>';
		el += '<td title="' + i.title + '"><span class="ellipsis">' + i.title + '</span></td>';
		el += '<td>' + i.date + ' ' + mtsToHrs(i.start_time) + '</td>';
		el += '<td sorttable_customkey="' + i.sentiment.score + '">' + sentimentWidget(i.sentiment) + '</td>';
		//
		output.appendChild(arc.elem('tr', el ));
	}
	//
	sortColumn(output.parentNode, 2);
	row.style.display ='block';
}



// -------------------------------------------------------------------------
//	Load details for the selected date
// -------------------------------------------------------------------------
var loadDetailsLastCall = false;
var dayWorkTimeline = false;

function loadDetails(date, employee, scores, processedData, DOMRef, doNotScroll, callback) { //timelineRow, timeline, busy5mGraph, busy5mHighlight, busy5mHighlightInner
	eventLogger('start', 'emp-details');
	loadDetailsLastCall = [date, employee, scores];
	// -------------------------------------------------------------------------
	//	Prodoscore Inputs [DATE]
	// -------------------------------------------------------------------------
	var dat = DateToShort(date);
	var keys = {
		0: ['dc', 'n'], 1: ['gm', 'n'], 2: ['cl', 't'], 3: ['ch', 'n'],
		4: ['tb', 't'],
		5: ['zc', 't'], 6: ['ze', 't'], 7: ['zt', 'n'], 8: ['zl', 'n'], 9: ['zi', 'n'], 10: ['za', 'n'],
		11: ['bs', 't'],
		12: ['sf', 'n'],
		13: ['sms', 'n'], 14: ['cll', 't'],
		15: ['pl', 'n'], 16: ['po', 'n'],
		17: ['rcc', 't'], 18: ['rcs', 'n'],
		19: ['sfl', 'n'],
		20: ['scl', 'n'], 21: ['sco', 'n'], 22: ['scc','t'], 23: ['scm', 't'],
		24: ['chm', 'n'], 25: ['cha', 'n'],
		26: ['sfc', 'n'], 27: ['sfi', 'n'], 28: ['sf_n', 'n'], 29: ['sfl_n', 'n'], 30: ['vbc', 't'],
		31: ['otl', 'n'], 32: ['odr', 'n'], 33: ['oev', 't']};
	// First, hide all rows
	for (var i = 0; i < scores.length; i++) {
		scores[i].parentNode.parentNode.parentNode.style.display = 'none';
	}

	let employee_data = parseInt(employee) in processedData.employees ? processedData.employees[employee] : processedData.self;
	// If we have data for the given date..
	if (typeof employee_data.days[dat].prod != 'undefined')
		for (var i = 0; i < scores.length; i++)
			// If we have data for each product
			if (typeof employee_data.days[dat].prod[ keys[i][0] ] != 'undefined'){
				scores[i].parentNode.parentNode.parentNode.style.display = 'block';
				if (keys[i][1] == 't')
					scores[i].innerHTML = mtsToTime(employee_data.days[dat].prod[ keys[i][0] ]);
				else
					scores[i].innerHTML = employee_data.days[dat].prod[ keys[i][0] ];
			}

	//
	for (var i = 0; i < scores.length; i++) {
		scores[i].parentNode.parentNode.addClass('cursor-pointer');
		scores[i].parentNode.parentNode.onclick = new(function (product, obj) {
			return function () {
				showDetails(obj, date, employee, product, false);
			}
		})(Object.keys(products)[prodInputsLiMapping[i]], scores[i].parentNode.parentNode);

	}
	DOMRef.context.q('#employee-day-stats')[0].style.display = 'block';
	//employeeDayStats.style.display = 'block';
	// ------------------
	DOMRef.EmployeeDataDrilldownRow.style.display = 'none';
	selDateDetails = false;
	DOMRef.timelineRow.style.display = 'block';
	// DOMRef.timeline.innerHTML = '';
	prevCal = {
		title: '',
		from: 0,
		to: 0
	};
	DOMRef.busy5mGraph.innerHTML = loadingIndicator;
	DOMRef.busy5mHighlight.style.width = '0px';
//setTimeout(function(){
	new arc.ajax(base_url + 'dashboard-detail/?date=' + date.sqlFormatted() + '&employee=' + employee, {
		callback: function (data) {
			eventLogger('end', 'emp-details');

			var zoom = 5;
			var dayStart = parseInt(organizationSettings.daystart.replace(':', '.')),
				dayEnd = parseInt(organizationSettings.dayend.replace(':', '.')) + 1;
			selDateDetails = eval('(' + data.responseText.replace(/ï»¿/g, '') + ')');
			if (typeof callback == 'function')
				callback(selDateDetails);

			dayWorkTimeline = new WorkTimeline({
				data: selDateDetails.detail,
				date: date,
				employee: employee,
				dayStart: dayStart,
				dayEnd: dayEnd - 0.016666666666666666,
				segmentWidth: 150,
				timeline: '#dayWorkTimeline',
				eventOnClick: function eventOnClick(id, el, product) {
					showDetails(el, date, employee, product, id);
				},
				timelineOnScroll: function timelineOnScroll(timeAtScrollPos, decimaltimeAtScrollPos, sc_left, e) {
					DOMRef.busy5mHighlight.style.left = 'calc(20px + ' + (3 + 94.25 * (dayWorkTimeline.timeline.scrollLeft / (dayWorkTimeline.timeline.scrollWidth))) + '%)';
					DOMRef.busy5mHighlightInner.style.left = 'calc(20px + ' + (3 + 89.5 * ((dayWorkTimeline.timeline.scrollLeft + dayWorkTimeline.timeline.offsetWidth / 2) / (dayWorkTimeline.timeline.scrollWidth))) + '%)';
				},
				timelineMouseMove: function (e) {
					DOMRef.busy5mHighlightInner.style.left = (DOMRef.busy5mHighlight.offsetWidth * (e.offsetX + (e.srcElement || e.target).offsetLeft - 40 - ((e.srcElement || e.target) == dayWorkTimeline.timeline ? 0 : dayWorkTimeline.timeline.scrollLeft)) / dayWorkTimeline.timeline.offsetWidth) + 'px';
				},
				timelineInitialized: function (timeline) {
					DOMRef.busy5mHighlight.style.left = 'calc(20px + ' + (3 + 94.25 * (timeline.timeline.scrollLeft / (timeline.timeline.scrollWidth))) + '%)';
					DOMRef.busy5mHighlight.style.width = (74.6 * (timeline.timeline.offsetWidth / (timeline.timeline.scrollWidth - (timeline.timeline.offsetWidth / 2)))) + '%';
					DOMRef.busy5mHighlightInner.style.left = 'calc(20px + ' + (3 + 89.5 * ((timeline.timeline.scrollLeft + timeline.timeline.offsetWidth / 2) / (timeline.timeline.scrollWidth))) + '%)';
				}
			});

			var bitSpace = [],
				h, currentBin, width, max = 0;

			//	Create bins to store vertical adjustments
			for (var i = 0; i < (dayEnd - dayStart - 0.9) * 60 / zoom; i++) {// - 0.5
				bitSpace[i] = 1024;
			}
			//
			//
			// -------------------------------------------------------------------------
			//	Prodoscore throughout the day
			//	Plot score accumilation through the day
			// -------------------------------------------------------------------------
			setTimeout(function () {
				var busy5m = [['Time', 'Activity']];
				for (var i = 0; i <= bitSpace.length+10; i += 2) {
					busy5m.push([mtsToHrs(i * zoom + dayStart * 60), 0]);
				}
				for (var id in selDateDetails.detail)
					if (arrayIgnore.indexOf(id) == -1) {
						var from = Math.floor((selDateDetails.detail[id].time[0] - dayStart * 60) / 10),
							to = Math.ceil((selDateDetails.detail[id].time[1] - dayStart * 60) / 10);
						if (from == to)
							to += 1;
						for (var i = from + 1; i < to + 1; i++)
							if (typeof busy5m[i + 1] != 'undefined' &&
								selDateDetails.detail[id].flag == 0 && typeof products[selDateDetails.detail[id].product] != 'undefined') {
								busy5m[i + 1][1] += products[selDateDetails.detail[id].product][1].weight;
							}
					}
				drawChartEmployeeBusy5m(DOMRef.busy5mGraph, busy5m,
					function (row, col) {
						dayWorkTimeline.scrollToTime(dayWorkTimeline.timeToDecimalTime(row[0]));
						busy5mHighlightInnerFocus(DOMRef);
					});
			}, 5);
			//
			//DOMRef.timeline.scrollLeft = 0;
			if (max < 4)
				max = 4

			//	Close if any alerts displayed in the notification area
			for (var i = messages.childNodes.length - 1; i > -1; i--)
				messages.childNodes[i].click();
			//
		},
		fallback: function () {
			showAlert('warning', 'Couldn\'t connect to server. Please check connectivity and retry.');
			clearTimeout(connectionRetry);
			connectionRetry = setTimeout(
				function () {
					loadDetails(date, employee, scores, processedData, DOMRef);
				}, 2400);
		}
	});
	// NLP chart commented out
	// DOMRef.busyNLPGraph.innerHTML = '';
	// new arc.ajax(base_url + 'emp-day-correlations/?date=' + date.sqlFormatted() + '&employee=' + employee, {
	// 	callback: function (data){
	// 		data = eval('(' + data.responseText + ')');
	// 		var timePoints = Object.keys(data.timeline);
	// 		var dayStart = parseInt(60 * organizationSettings.daystart.replace(':', '.')),
	// 			dayEnd = parseInt(60 * organizationSettings.dayend.replace(':', '.'))
	// 		var txt, entity;
	// 		for (var i = 0; i < timePoints.length; i++){
	// 			for (var j = 0; j < data.timeline[ timePoints[i] ].length; j++){
	// 				txt = '';
	// 				entity = data.entities[ data.timeline[ timePoints[i] ][j] ];
	// 				if (entity.type == 'organization')
	// 					txt += entity.title + '<br/>';
	// 				else
	// 					txt += (entity.name || entity.email || entity.phone) + '<br/>';
	// 			}
	// 			DOMRef.busyNLPGraph.appendChild( elem('div', txt, {
	// 				class: 'entity',
	// 				style: 'left: calc(93px + '+(90 * (timePoints[i] - dayStart) / (dayEnd - dayStart))+'%); top: '+(parseInt((0.125 + Math.random()) * 90))+'px'}) );
	// 		}
	// 	}
	// });
//}, 5000);
	if (typeof doNotScroll == 'undefined' || !doNotScroll) {
		setTimeout(function () {
			scrollToMiddle(420);
		}, 200);
	}

}



// -------------------------------------------------------------------------
//	Score Details Drill Down
// -------------------------------------------------------------------------

//	Display details for a selected date for a selected product
//var DetailDrilldownTbody = contentOneEmployee.q('.stat-details tbody')[0];
var showDetailsTimeout = false;

function showDetails(obj, date, employee, product, highlight){
	EmployeeDOMRef.DetailDrilldownTbody.innerHTML = '';
	EmployeeDOMRef.EmployeeDataDrilldownRow.style.display = 'block';
	EmployeeDOMRef.DetailDrilldownTbody.innerHTML = '<div class="loading"></div>';
	var scrollTo = false;
	var showDurationFor = ['cl', 'tb', 'zc', 'ze', 'bs', 'cll', 'rcc', 'vbc', 'oev'];
	let lastWeek = new Date();
	lastWeek.setDate( lastWeek.getDate() - 8 ); // possible OB1e - Yup, fixed
	let flagExpired = date < lastWeek;
	if (selDateDetails == false){
		clearTimeout(showDetailsTimeout);
		showDetailsTimeout = setTimeout(obj.onclick, 400);
		return;
	}
	else{
		let duration = 0;
		let counter = 0;
		if (showDurationFor.indexOf(product) !== -1){
			EmployeeDOMRef.DetailDrilldownTbody.parentNode.classList.add('has-duration');
		}
		else{
			EmployeeDOMRef.DetailDrilldownTbody.parentNode.classList.remove('has-duration');
		}
		let data = selDateDetails.detail;
		if (Object.keys(data).length == 0)
			EmployeeDOMRef.DetailDrilldownTbody.innerHTML = '<tr><td colspan="2" class="no-records" style="text-align:center;"><i>No records to display</i></td></tr>';
		else{
			EmployeeDOMRef.DetailDrilldownTbody.innerHTML = '';
			let tmpRow;//, prevEndTime = 0,
				prevCal = {
					title: '',
					from: 0,
					to: 0
				},
				sfData = [];
			// var hasDuration = false;
			dataLoop:
			for (let id in data){
				if (product == data[id].product) {
					if (product == 'cl'){ //	Remove duplicate events from displaying
						if (prevCal.title == data[id].title && prevCal.from == data[id].time[0] && prevCal.to == data[id].time[1])
							continue;
						prevCal = {
							title: data[id].title,
							from: data[id].time[0],
							to: data[id].time[1]
						};
					}
					else if (product == 'sf' || product == 'sfl' || product == 'sfc' || product == 'sfi' ||  product == 'sfl_n' ||  product == 'sf_n') {
						for (let z = 0; z < sfData.length; z++) {
							if (sfData[z].title == data[id].title && sfData[z].from == data[id].time[0] && sfData[z].to == data[id].time[1]) {
								continue dataLoop;
							}
						}
						let newSF = {};
						newSF.title = data[id].title;
						newSF.from = data[id].time[0];
						newSF.to = data[id].time[1];
						sfData.push(newSF);
					}
					else if ( product == 'po' ||  product == 'pl') {
						for (let key in data[id].data) {
							if (data[id].data[key] == null || data[id].data[key] == "") {
								data[id].data[key] = '-';
							}
						}
					}
					tmpRow = new statDetailsRow(data, id, product, highlight, flagExpired);

					if (product == 'tb')
						tmpRow.setAttribute('time', data[id].time);
						/*if (data[id].time[0] <= prevEndTime) {
							tmpRow.addClass('sub');
							tmpRow.cells[0].innerHTML += '<div class="fa icon fa-user"></div>';
						}
						else
							tmpRow.cells[1].childNodes[0].nodeValue = 'Conference Call';*/
					EmployeeDOMRef.DetailDrilldownTbody.appendChild(tmpRow);
					/*if (data[id].time[1] > prevEndTime)
						prevEndTime = data[id].time[1];
					*/
					// if (data[id].time[0] != data[id].time[1]) {
					// 	hasDuration = true;
					// }

					if (showDurationFor.indexOf(product) !== -1) {
						let cur_duration = data[id].time[1] - data[id].time[0];
						if (data[id].time_range === 1 || data[id].time_range === -1) {
							cur_duration = (1440 + data[id].time[1]) - data[id].time[0];
						}

						duration += cur_duration;
						counter++;
					}

					//
					/*new (function(tmpRow){
						setTimeout(function(){
							tmpRow.cells[0].innerHTML = parseInt(tmpRow.getBoundingClientRect().top)+scrollTarget;
						}, 2000);
					})(tmpRow);*/
					//
					if (id == highlight)
						scrollTo = tmpRow;
				}
			}
			let hasMoreRow = EmployeeDOMRef.DetailDrilldownTbody.parentNode.q('tfoot tr.load-more')[0];
			if (typeof selDateDetails.limits[ products[product][0] ] == 'undefined')
				hasMoreRow.style.display = 'none';
			else{
				//var hasMoreRow = elem('tr', '<td></td><td><i>Load '+(selDateDetails.limits[ products[product][0] ]-200)+' More Records</i></td><td></td>');
				//EmployeeDOMRef.DetailDrilldownTbody.appendChild(hasMoreRow);
				hasMoreRow.q('th')[1].innerHTML = '<i>Load '+(selDateDetails.limits[ products[product][0] ]-200)+' More Records</i>';
				hasMoreRow.style.display = 'table-row';
				hasMoreRow.onclick = function(){
					//console.log(date, employee, product);
					new arc.ajax(base_url + 'dashboard-detail/?date='+date.sqlFormatted()+'&employee='+employee+'&product='+product, {
						callback: function (data){
							let details = eval('(' + data.responseText.replace(/ï»¿/g, '') + ')');
							for (let id in details.detail){
								details.detail[ id ].product = product;
								selDateDetails.detail[ id ] = details.detail[ id ];
							}
							delete selDateDetails.limits[ products[product][0] ];
							showDetails(obj, date, employee, product, highlight);
						}
					});
				}
			}

			EmployeeDOMRef.DetailDrilldownTbody.parentNode.q('tfoot tr.total-entries th')[1].innerHTML = "Total Entries: " + counter;
			//EmployeeDOMRef.DetailDrilldownTbody.parentNode.q('tfoot th')[2].innerHTML = mtsToTime(duration);

			sortColumn(EmployeeDOMRef.DetailDrilldownTbody.parentNode, 2);
			//
			if (product == 'tb'){
				let time, prevEndTime = 0, confStartTime = 0, confEndTime = 0, pTbl, rows = EmployeeDOMRef.DetailDrilldownTbody.rows;
				for (let i = 0; i < rows.length; i++){
					time = rows[i].getAttribute('time').split(',');
					if (time[0]*1 < prevEndTime) {
						rows[i].addClass('sub');
						//rows[i].removeChild(rows[i].cells[2]);
						rows[i].cells[0].innerHTML = '<div class="fa icon fa-user"></div>';
						rows[i].onclick = false;
						pTbl.appendChild(rows[i]);
						i -= 1;
						if (confEndTime < time[1]*1){
							confEndTime = time[1]*1
							pTbl.parentNode.parentNode.cells[2].innerHTML = mtsToHrs(confStartTime) + ' to ' + mtsToHrs(confEndTime);
						}
					}
					else{
						let tmp = rows[i].innerHTML;
						//rows[i].cells[1].childNodes[0].nodeValue = 'Conference Call ['+rows[i].cells[1].childNodes[0].nodeValue+']';
						rows[i].cells[1].innerHTML = 'Conference Call';
						//pTbl = rows[i].cells[1].q('table tbody')[0];
						pTbl = rows[i].cells[1].appendChild(arc.elem('table', '', {class: 'stat-details-inner', style: 'height: auto; transition: height 0.5s;'}));
						tmp = pTbl.appendChild(arc.elem('tr', tmp));
						tmp.className = 'tablerow include sub';
						//tmp.removeChild(tmp.cells[2]);
						tmp.cells[0].innerHTML = '<div class="fa icon fa-user"></div>';
						confStartTime = time[0];
					}
					if (time[1]*1 > prevEndTime)
						prevEndTime = time[1]*1;
				}
			}
			//
			if (EmployeeDOMRef.DetailDrilldownTbody.innerHTML == "") {
				EmployeeDOMRef.DetailDrilldownTbody.innerHTML = '<tr><td colspan="3" class="no-records" style="text-align:center;"><i>No records to display</i></td></tr>';
			}
		}
		setTimeout(function () {
			if (scrollTo == false)
				scrollTo = q('#drilldown-row')[0].getBoundingClientRect().top + scrollTarget;//Math.min(1386, (100 + contentOneEmployee.offsetHeight - window.innerHeight));
			else{
				if (scrollTo.className.indexOf('sub') > -1)
					scrollTo = scrollTo.parentNode.parentNode.parentNode;
				scrollTo = scrollTo.getBoundingClientRect().top + scrollTarget;
			}
			// console.log(scrollTo);
			window.scrollTo( 0, scrollTo - (window.innerHeight / 2) );
		}, 400);
	}
	// ===================================
	//	SELECT ALL AND FLAG
	//
	let selectAllChk = EmployeeDOMRef.DetailDrilldownTbody.parentNode.q('thead tr.tablerow input[type="checkbox"]')[0];
	selectAllChk.onclick = function(e){
		let rows = EmployeeDOMRef.DetailDrilldownTbody.q('tr.tablerow td.icon-col input[type="checkbox"]');
		selectedChkBxes = [];
		for (let i = 0; i < rows.length; i++){
			rows[i].checked = this.checked;
			rows[i].onclick();
		}
	}
	//
	//	Handle if flagging expired - 7 days
	selectAllChk.disabled = flagExpired;
	selectAllChk.checked = false;
	if (flagExpired)
		selectAllChk.parentNode.setAttribute('title', 'Flagging functionality is only available for records within the last 7 days.');
	else
		selectAllChk.parentNode.removeAttribute('title');
	//
	let selectAllMenuBtn = EmployeeDOMRef.DetailDrilldownTbody.parentNode.q('thead tr.tablerow button')[0];
	let flmenu = (new flagMenu(selectedChkBxes, processedData, date, employee, product, false)).flag;
	let ufmenu = (new unflagMenu(selectedChkBxes, processedData, date, employee, product, false)).unflag;
	let compMenu = {'flag': flmenu, 'unflag': ufmenu};
	flmenu.title = 'Flag';
	flmenu.icon = false;
	ufmenu.title = 'Unflag';
	ufmenu.icon = false;
	new contextMenu(selectAllMenuBtn, compMenu); //'test-flag': {icon: 'fa-ban',title: 'Test Flag',onclick: function(rawdata){console.log(Object.keys(selectedChkBxes));}}
	//
	let selectAllMenuBtn_oncontextmenu = selectAllMenuBtn.oncontextmenu;
	selectAllMenuBtn.oncontextmenu = function(){};
	selectAllMenuBtn.onclick = function(e){
		selectAllMenuBtn_oncontextmenu(e, selectedChkBxes);
	};
}
// ---------------------------------
let chkBxSelectLatch = false;
let selectedChkBxes = [];
function chkBxSelect(e){
	let checked = this.checked;
	if (this.hasAttribute('disabled') || chkBxSelectLatch)
		return false;
	//
	if (typeof e != 'undefined'){
		e.preventDefault();
		e.stopPropagation();
		e.cancelBubble = true;
	}
	//
	let id = this.name.split('-')[2];
	if (checked)
		selectedChkBxes.push(id);
	else{
		let index = selectedChkBxes.indexOf(id);
		if (index > -1)
			selectedChkBxes.splice(index, 1);//delete selectedChkBxes[ id ];
	}
	//
	//chkBxSelectLatch = true;
	new (function(chk, box, row){
		setTimeout(function(){
			box.checked = chk;
			if (chk)
				row.addClass('selected');
			else
				row.removeClass('selected');
		}, 10);
	})(checked, this, this.parentNode.parentNode);
	//
	//chkBxSelectLatch = false;
	return false;
};
//	Handles each detail row that can be expanded
var highlightedStatDetailRow = false;
function statDetailsRow(data, id, product, highlight, flagExpired){
	/*if (typeof data[id].data == 'string')
		data[id].data = eval('(' + data[id].data + ')');*/
	var icon = icons[product];
	var mime = products[product][0];
	var title = data[id].title;
	var icon_addon = '';
	var time_addon = '';
	var time_title = '';
	var tr_class = '';
	var hasDetails = (function () {
		return (Object.keys(products[product][3]).length > 0);// || Object.keys(data[id].sentiment).length > 0
	})();
	/*if (product == 'dc') {
		icon = getIcon(data[id]);
	}
	else if (product == 'ch') {
		var tmp = document.createElement("div"),
			tmpTitle = showEncrypted(data[id].data.data);
		tmp.innerHTML = tmpTitle;
		title = tmp.textContent || tmp.innerText || "<i>NO TEXT CONTENT</i>";
		if (title.length > 280) {
			title = title.substring(0,280) + '&hellip;';
		}
		try {
			title = decodeURIComponent(escape(title));
		} catch (err) {
			// some text cannot be decoded. Shouldn't cause an issue.
			// URIError: malformed URI sequence
		}
	}else */
	if (product == 'gm' || product == 'otl') {
		var re = title.indexOf('Re: ');
		var fwd = title.indexOf('Fwd: ');
		if (re == -1 && fwd == -1) {} else if (Math.min(re, fwd) == re || fwd == -1)
			//title = '<i class="fa fa-reply"></i>' + title;
			icon_addon = '<i class="fa fa-reply icon-addon"></i>';
		else
			// title = '<i class="fa fa-share"></i>' + title;
			icon_addon = '<i class="fa fa-share icon-addon"></i>';
	}
	else if ( product == 'rcc' || product == 'vbc' ){
		var type = title.split(': ');

		if ( typeof type[1] != 'undefined' && (type[1] == 'Inbound' || type[1] == 'Outbound') ) {
			data[id].type = type[1].toLowerCase();
			title = type[0];
			if ( data[id].type == 'inbound' ) {
				icon_addon = '<i title="Outbound call" class="icon-addon fa fa-arrow-left"></i>';
			} else {
				icon_addon = '<i title="Inbound call" class="icon-addon fa fa-arrow-right"></i>';
			}
		}
	}
	else if ( product == 'bs' ){
		var type = title.split('*');

		if ( typeof type[1] != 'undefined' ) {
			data[id].type = type[1].toLowerCase();
			title = type[0];
			if (type[1] == 'Inbound' || type[1] == 'Outbound') {
				if ( data[id].type == 'inbound' ) {
					icon_addon = '<i title="Outbound call" class="icon-addon fa fa-arrow-left"></i>';
				} else {
					icon_addon = '<i title="Inbound call" class="icon-addon fa fa-arrow-right"></i>';
				}
			}

		}
	}
	else if (product === 'cll' || product === 'sms') {
		var type = title.split(': ');

		if ( typeof type[1] != 'undefined' && (type[1] == 'Incoming' || type[1] == 'Outgoing') ) {
			data[id].type = type[1].toLowerCase();
			title = type[0];
			if ( data[id].type == 'incoming' ) {
				icon_addon = '<i title="Outbound call" class="icon-addon fa fa-arrow-left"></i>';
			} else if (data[id].type == 'outgoing') {
				icon_addon = '<i title="Inbound call" class="icon-addon fa fa-arrow-right"></i>';
			}
		}
	}
	else if ( product === 'sfl' || product === 'sf' || product === 'sfc' || product === 'sfi' ||  product === 'sfl_n' ||  product === 'sf_n') {
		if ( data[id].title != "" && data[id].title != null ) {
			let sfl_title = data[id].title;
			let type = sfl_title.substring(0, 2).trim();

			if (type === '&#x2192;' || type === '&#x2190;') {
				sfl_title = sfl_title.substring(2);
			}
			title = sfl_title;
		}
	}
	else if (product === 'cl' || product === 'scc' || product === 'scm') {
		if ( data[id].time_range == -1 || data[id].time_range == 1) {
			icon_addon = '<i class="fa fa-clock-o icon-addon icon-addon--right"></i>';
			time_addon = '<i class="fa fa-exclamation time-addon"></i>';
			if (data[id].time_range == 1) {
				time_title = 'Carried on to the next day';
				tr_class += ' time_range_next_day';
			} else {
				time_title = 'Carried forward from the previous day';
				tr_class += ' time_range_prev_day';
			}
		}
	}
	//
	//	if (product == 'tb')	icon_addon = '<i title="Ingoing call" class="icon-addon fa fa-arrow-up"></i>';
	if ( ( product == 'tb' || product == 'rcc' ) && typeof data[id].hasTranscript != 'undefined' ){
		icon_addon += ' <i class="fa fa-microphone icon-addon-t2 red"></i>';
	}

	if (title === "" || title === null) {
		title = "-";
	}
	/*else if (product == 'sms' || product == 'cll') {
		if (data[id].data.type == 2)
			icon_addon = '<i class="fa fa-reply icon-addon"></i>';
		else
			icon_addon = '<i class="fa fa-share icon-addon"></i>';
	}
	else if (product == 'bs')
		title = data[id].data.personality.replace('Terminator', '<i class="fa fa-sign-in"></i> from ').replace('Originator', '<i class="fa fa-sign-out"></i> to ') +
			' ' + title.replace('Group', '<small>(Internal)</small>').replace('Network', '<small>(External)</small>');*/
	//
	var dayStart = organizationSettings.daystart.split(':'),
			dayEnd = organizationSettings.dayend.split(':');

	var dayStartMins = (parseInt(dayStart[0]) * 60) + parseInt(dayStart[1]),
			dayEndMins = (parseInt(dayEnd[0]) * 60) + parseInt(dayEnd[1]);

	var time = '';
	if (data[id].time[0] == data[id].time[1]) {
		var timeClass = (data[id].time[0] > dayStartMins && data[id].time[0] < dayEndMins) ? '' : 'not-in-range';
		time = '<span class="time-0 ' + timeClass + '">' + mtsToHrs(data[id].time[0]) + '</span>';
	}
	else {
		var time0Class = (data[id].time[0] > dayStartMins && data[id].time[0] < dayEndMins) ? '' : 'not-in-range';
		var time1Class = (data[id].time[1] > dayStartMins && data[id].time[1] < dayEndMins) ? '' : 'not-in-range';
		time = '<span class="time-0 ' + time0Class + '">' + mtsToHrs(data[id].time[0]) + '</span>' + ' to ' + '<span class="time-1 ' + time1Class + '">' + mtsToHrs(data[id].time[1]) + '</span>';
	}
	//
	var duration = data[id].time[1] - data[id].time[0];
	if (data[id].time_range === 1 || data[id].time_range === -1) {
		duration = (1440 + data[id].time[1]) - data[id].time[0];
	}

	var duration = data[id].time[1] - data[id].time[0];
	if (data[id].time_range === 1 || data[id].time_range === -1) {
		duration = (1440 + data[id].time[1]) - data[id].time[0];
	}

	var row = {
		tr: {
			class: 'tablerow ' + (data[id].flag == 0 ? 'include ' : 'exclude ') + (hasDetails ? 'hasDetails' : '') + ' ' + tr_class,
			'data-id': id,
			content: [
				{
					td: {
						content: '<input type="checkbox" name="select-detail-'+id+'"'+(flagExpired ? ' disabled title="Flagging functionality is only available for records within the last 7 days."' : '')+' />' +
							'<div class="icon" style="background-image:url(\'' + icon + '\');" title="' + mime + '"></div>' + icon_addon,
						class: 'icon-col'
						//,'sorttable_customkey': data[id].data['mimeType']
					}
				},
				{
					td: {
						content: '<span class="ellipsis">'+title+'</span>' + (products[product][3] ? '<table class="stat-details-inner"><tbody></tbody></table>' : '')
					}
				},
				{
					td: {
						content: time_addon + time,
						class: 'time-col',
						title: time_title
					}
				}
			]
		}
	};
	if (typeof data[id].sentiment == 'undefined')
		row.tr.content.push({td: {style: 'display:none'}});
	else
		row.tr.content.push({td: {class: 'center', content: sentimentWidget(data[id].sentiment)}});
	row = arc.reactor(row);
	//
	//	We cannot check this [hasDetails] before hand, because we have to make the ajax call to find out if there is any data or .
	//if (products[product][3] && hasDetails){
	row.onclick = function(){
		if (!organizationSettings['show_details'])
			return false;
		//
		var detable = this.cells[1].q('table')[0];
		//	Toggle display of details of detail record
		if (this.className.indexOf('details') > -1 || this.className.indexOf('highlight') > -1){
			detable.style.height = detable.offsetHeight+'px';
			detable.style.transition = 'height 0.5s';
			setTimeout(
				function(){
					detable.style.height = '0px';
					setTimeout(
						function(){
							row.removeClass('details');
							row.removeClass('highlight');
							detable.style.height = 'auto';
							detable.style.transition = '';
						}, 500);
				}, 10);
			dayWorkTimeline.unglowEvent(id);
		}
		else{
			if (highlightedStatDetailRow)
				highlightedStatDetailRow.removeClass('highlight');
			row.addClass('details');
			row.addClass('highlight');
			highlightedStatDetailRow = row;
			loadDetailData(id, data[id], function(){
				var height = detable.offsetHeight;
				detable.style.height = '16px';
				detable.style.transition = 'height 0.5s';
				setTimeout(
					function(){
						detable.style.height = height+'px';
						setTimeout(
							function(){
								detable.style.height = 'auto';
							}, 500);
					}, 10);
			});
			dayWorkTimeline.scrollToTime(data[id].time[0] / 60);
			dayWorkTimeline.glowEvent(id);
		}
	/*}
	row.onmouseover = function(){*/
	};
	//	Select rows for flagging
	row.q('input[type="checkbox"]')[0].onclick = chkBxSelect;
	//}
	// ===============================================================
	//	Load metadata for a stat detail row - From JSON file in Cloud Storage file
	var loadDetailData = function(id, detail, callback){
		if (!organizationSettings['show_details'])
			return false;
		//
		if (row.className.indexOf('sub') > -1){
			//	Especially for broadsoft, find the mail row
			row.removeClass('details highlight');
			row = (function(row){
					if (row.tagName == 'TR' && row.className.indexOf('tablerow') > -1 && row.className.indexOf('sub') == -1)
						return row;
					else
						return arguments.callee(row.parentNode);
				})(row);
			row.addClass('details highlight');
		}
		var detable, detables = row.cells[1].q('table tbody');
		//	Especially for broadsoft, there are sub rows inside a single detail row. We need to iterate for those
		var loaded = 0;
		for (var i = 0; i < detables.length; i++){
			detable = detables[i];
			if (detable.innerHTML != '' && detable.innerHTML != '<small>Loading ...</small>'){
				callback();
				return false;
			}
			//	Recursively navigate up to the TR with data-id to  find the detail record id
			id = (function(node){
					if (node.tagName == 'TR' && node.className.indexOf('tablerow') > -1 && node.getAttribute('data-id') != null)
						return node.getAttribute('data-id');
					else
						return arguments.callee(node.parentNode);
				})(detable);
			detable.innerHTML = '<small>Loading ...</small>';
//setTimeout(function(){
			//	Need to isolate for each asynchronous callback
			//new (function(){})();
			//new arc.ajax('https://app.prodoscore.com/dashboard-detail-data-raw/?id='+id+'&filename=data', {
			new arc.ajax(base_url + 'dashboard-detail-data/?id=' + id, {
				callback: function (data, ref) {//[id, detable, callback]
					var detable = ref[1];
					/*var hasSentiment = (function () {
						return (typeof data[id].sentiment !== 'undefined'  && Object.keys(data[id].sentiment).length > 0);
					})();*/
					//
					data = eval('('+data.responseText+')');
					var entities = data.entities;
					var sentiment = data.sentiment;
					data = data.data;
					detable.innerHTML = '';

					// Replace 'null' or empty string with '-'
					for (var key in data) {
						if (data[key] == null || data[key] == "") {
							data[key] = '-';
						}
					}

					// Google Drive file type icons
					if (product == 'dc'){
						if (typeof data.mime != 'undefined')
							detable.appendChild(arc.reactor({tr: {content: [
								{td: {content: 'File Type'}},
								{td: {content: '<img src="'+getIcon(data, title)+'" height="16" width="16" title="'+data.mime+'" /> ' + data.mime.replace('application/vnd.google-apps.', '')}}
							]}}));
					}
					//
					// Display phone call type
					else if (product == 'cll'){
						data.duration /= 60;
						if (typeof data.type != 'undefined'){
							if (data.type == 3 || isNaN(data.duration))//data.duration == '-'
								delete data.duration;
							data.type = ['', 'Incoming', 'Outgoing', 'Missed'][data.type];
						}
					}
					//
					// Prepend doller sign for ProsperWorks oppertunities and leads
					else if (product == 'po' ||  product == 'pl') {
						if (data.monetary_value != null) {
							data.monetary_value = '$' + data.monetary_value;
						}
						else {
							data.monetary_value = '-';
						}
					}
					//
					//	Hide duration for missed and failed RingCentral calls
					else if ( product == 'rcc' || product == 'vbc' ){
						if (['Missed', 'Call Failed', 'Hang Up', 'Unknown', 'Rejected',
						'Receive Error', 'Blocked', 'No Answer', 'International Disabled',
						'Busy', 'Send Error', 'No Fax Machine', 'ResultEmpty', 'Suspended',
						'Call Failure', 'Internal Error', 'IP Phone Offline', 'Stopped',
						'International Restriction', 'Abandoned', 'Declined',
						'Fax Receipt Error', 'Fax Send Error'].indexOf(data.result) > -1) {
							delete data.duration;
						}
					}
					//
					else if (product === 'cl') {
						if ( typeof data.duration !== 'undefined') data.duration = Math.abs(data.duration);
					}

					if (product == 'odr'){
						if (typeof data.mime != 'undefined')
							detable.appendChild(arc.reactor({tr: {content: [
								{td: {content: 'File Type'}},
								{td: {content: '<img src="'+getIcon(data, title)+'" height="16" width="16" title="'+data.mime+'" /> ' + data.mime.replace('application/vnd.openxmlformats-officedocument.presentationml.', '').replace('application/vnd.openxmlformats-officedocument.wordprocessingml.','').replace('application/vnd.openxmlformats-officedocument.spreadsheetml.','').replace('application/vnd.','').replace('application/','').replace('text/','')}}
							]}}));
					}
					//
					/*/ Sales Force - parse email data	//	This is implemented at the server side
					else if ( product == 'sf' ){
						data.Description = data.Description.split('\n');
						for (var i = 0; i < data.Description.length; i++){
							data.Description[i] = data.Description[i].split(':');
							if (data.Description[i].length > 1){
								if (data.Description[i][0] == 'Body'){
									data.Description = data.Description.slice(i+1, 33).join('\n');//<br/>.substring(0, 500)
									break;
								}
								else
									data[ data.Description[i][0] ] = data.Description[i].slice(1).join(':').substring(1);
							}
						}
					}*/
					// ---------------------------------------------------------------------------------------------------------
					//	Actually insert rows
					// ---------------------------------------------------------------------------------------------------------
					if (data.constructor === Array){
						console.warn('Product '+product+' gives data in an array, not dict.!')
						for (var i = 0; i < data.length; i++)
							if (products[product][3].length==0 || typeof products[product][3][ data[i].name ] != 'undefined') {
								detable.appendChild(arc.reactor({tr: {content: [
									{td: {content: products[product][3][ data[i].name ]}},
									{td: {content: data[i].name == 'duration' ? mtsToTime(data[i].value) : data[i].value.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\n/g, '<br/>')}}//.replace(/ï»¿/g, '')
								]}}));
							}
					}
					else{
						var attribs = Object.keys(products[product][3]);
						for (var i = 0; i < attribs.length; i++) {
							if (typeof data[attribs[i]] == 'undefined') {
								data[attribs[i]] = '-';
							}
							else{
								var attributeValue = data[attribs[i]];
								if (product == 'zc' &&  attributeValue.indexOf("Dialled") != -1) {
									var newAttrbuteValue = attributeValue.replace("Dialled","Dialed");
									data[attribs[i]] = newAttrbuteValue;
								}
							}
							detable.appendChild(arc.reactor({
								tr: {
									content: [
										{td: {content: products[product][3][attribs[i]]}},
										{
											td: {
												content:
													(attribs[i] == 'duration' || products[product][3][attribs[i]] == 'Duration') ? // then
														mtsToTime(data[attribs[i]]) :
													// else
														(typeof data[attribs[i]] == 'object' ? // then
															JSON.stringify(data[attribs[i]]) :
														// else
															data[attribs[i]].toString().replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/ï»¿/g, '').replace(/\n/g, '<br/>')
												)
											}
										}//
									]
								}
							}));
						}
						//
						/*for (var key in data)
							if (products[product][3].length==0 || typeof products[product][3][ key ] != 'undefined') {
								detable.appendChild(arc.reactor({tr: {content: [
									{td: {content: products[product][3][ key ]}},
									{td: {content: key == 'duration' ? mtsToTime(data[key]) : (typeof data[key] == 'object' ? JSON.stringify(data[key]) : data[key].toString().replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/ï»¿/g, '')) }}//
								]}}));
							}*/
					}
					// ---------------------------------------------------------------------------------------------------------
					//	Additional Rows
					// ---------------------------------------------------------------------------------------------------------
					if (product == 'gm' && typeof data.body != 'undefined'){
						var GmailBody = data.body.replace(/ï»¿/g, '');
						if (GmailBody != 'TEXT TOO LONG'){
							/*try{	//	This is temporary, untill the memcached old email bodies are removed.
								GmailBody = atob(GmailBody.replace(/_/g, '/').replace(/-/g, '+')).replace(/â/g, '').replace(/Â/g, ''); GmailBody = elem('div', GmailBody); GmailBody = GmailBody.textContent || GmailBody.innerText || ''; }catch(e){}*/
							if (GmailBody.trim() == '')
								GmailBody = '<i>No text content</i>';//The email body contains either images or tables // THAT'S WRONG MESSAGE
						}
						detable.appendChild(arc.reactor({tr: {content: [
							{td: {content: 'Body'}},
							{td: {content: GmailBody}}
						]}}));
					}
					else if (product == 'ch'){
						detable.innerHTML = '';	//	Should We .?
						if (typeof data.from != 'undefined')
							detable.appendChild(arc.reactor({tr: {content: [
								{td: {content: 'From'}},
								{td: {content: data.from[0]+' &lt;'+data.from[1]+'&gt;'}}
							]}}));
						//
						if (typeof data.data != 'undefined'){
							var tmp = document.createElement("div"),
								tmpTitle = showEncrypted(data.data);
							tmp.innerHTML = tmpTitle;
							title = tmp.textContent || tmp.innerText || "<i>NO TEXT CONTENT</i>";
							if (title.length > 280) {
								title = title.substring(0,280) + '&hellip;';
							}
							try {
								title = decodeURIComponent(escape(title));
							} catch (err) {
								// some text cannot be decoded. Shouldn't cause an issue.
								// URIError: malformed URI sequence
							}
							//
							detable.appendChild(arc.reactor({tr: {content: [
								{td: {content: 'Message'}},
								{td: {content: title}}
							]}}));
						}
					}
					else if ( product == 'rcs' ){
						/*detable.appendChild(arc.reactor({tr: {content: [
							{td: {content: 'From'}},
							{td: {content: data.from.phoneNumber+' ('+data.from.location+')'}}
						]}}));*/
						function location_rcs (elm) {

						    if (elm != undefined){
						        return `(${elm})`
						    }else{
						        return ''
						    }
						}
						for (var j = 0; j < data.to.length; j++)
							detable.appendChild(arc.reactor({tr: {content: [
								{td: {content: 'To'}},
								{td: {content: (data.to[j].phoneNumber ||  ( "Extension: " + data.to[j].extensionNumber) )
								+ (location_rcs(data.to[j].location) || '') }}
							]}}));
					}
					else if ( product == 'sf' || product =='sfl' || product == 'sfc' || product == 'sfi' ||  product =='sfl_n' ||  product == 'sf_n'){
						if (product == 'sfc') {
						     detable.appendChild(arc.reactor({tr: {content: [
							{td: {content: 'Call Duration'}},
							{td: {content: (data.CallDurationInSeconds >= 0 ? (data.CallDurationInSeconds >=60 ? (data.CallDurationInSeconds >=3600 ? Math.floor(data.CallDurationInSeconds /3600) + "h " + Math.floor(data.CallDurationInSeconds /60) + "m" +data.CallDurationInSeconds %60 + "s" : Math.floor(data.CallDurationInSeconds /60) + "m " +data.CallDurationInSeconds %60 + "s") : data.CallDurationInSeconds +"s") :"00s" ) }}
						     ]}}));
						}
						detable.appendChild(arc.reactor({tr: {content: [
						    {td: {content: 'More Info'}},
						    {td: {content: '<a href="'+data.Link+'" target="_blank">'+data.Link+'</a>'}}
						]}}));
					}
					//
					// Show transcript of TurboBridge call
					if ( typeof data.transcript != 'undefined' ){
						if ( product == 'tb' ){
							var transcriptRow = arc.reactor(
								{	tr: {content: [
										{td: {content: 'Transcript'}},
										{td: {content: '<div class="transcript-expand">'+data.transcript.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/ï»¿/g, '').replace(/\n/g, '<br/>')+'</div>'}}
									]}
								});
							detable.parentNode.parentNode.parentNode.parentNode.appendChild(transcriptRow);
							if (data.transcript.split('\n\n').length > 2)
								new showMoreBtn( transcriptRow.q('.transcript-expand')[0] );
							//transcriptRow.style.height = '100px';
							//delete data.transcript;
						}
						//
						// Show transcript of RingCentral call
						if ( product == 'rcc' ){
							var transcriptRow = arc.reactor(
								{	tr: {content: [
										{td: {content: 'Transcript'}},
										{td: {content: '<div class="transcript-expand">'+data.transcript.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/ï»¿/g, '').replace(/\n/g, '<br/>')+'</div>'}}
									]}
								});
							detable.appendChild(transcriptRow);
							if (data.transcript.split('\n\n').length > 2)
								new showMoreBtn( transcriptRow.q('.transcript-expand')[0] );
						}
					}
					//
					//if (false)	//	TEMPORARILY HIDDEN FEATURE FOR THE MOMENT
					//	Load NLP entities related to this detail record
					if (Object.keys(entities).length > 0){
						var entitySpace = detable.appendChild(arc.reactor({tr: {content: [
							{td: {colspan: 2, class: 'entities'}}
						], style: 'width:calc(100vw - 280px);'}})).q('td')[0];
						for (entity in entities)
							if (isset(faDictionary[ entities[entity].etype+'s' ]))
								entitySpace.appendChild(arc.elem('span',
									'<i class="fa '+faDictionary[ entities[entity].etype+'s' ]+'" style="color:'+colorDictionary[ entities[entity].etype+'s' ]+';"></i> '+
									(entities[entity].title || entities[entity].name || entities[entity].email || entities[entity].phone),
									{class: 'nlpentity '+entities[entity].etype+' title'}));
/* TO DO: commenting out to enable this later since the accuracy of the data is not satisfying at the moment
							else if (entities[entity].etype == 'person'){
								if (typeof entities[entity].name != 'undefined')
									entitySpace.appendChild(elem('span', '<i class="fa fa-user"></i> '+entities[entity].name, {class: 'nlpentity person name'}));
								else if (typeof entities[entity].email != 'undefined')
									entitySpace.appendChild(elem('span', '<i class="fa fa-user"></i> '+entities[entity].email, {class: 'nlpentity person email'}));
								else if (typeof entities[entity].phone != 'undefined')
									entitySpace.appendChild(elem('span', '<i class="fa fa-user"></i> '+entities[entity].phone, {class: 'nlpentity person phone'}));
							}
//*/
					}
					ref[2]();
				}
			},
			//	Reference object to be passed to Ajax callback
			[id, detable,
				function(){
					loaded += 1;
					if (detables.length == loaded)
						callback();
				}
			]);
		}
	};
	// ===============================================================
	//
	if (highlight == id){
		if (organizationSettings['show_details']){
			highlightedStatDetailRow = row;
			row.addClass('details highlight');
			setTimeout(function(){
				loadDetailData(id, data[id], function(){});
			}, 100);
		}
		else{
			row.addClass('highlight');
		}
	}
	return row;
}

let sentimentHelpModal = new modal({
	title: 'What Is Mood (Sentiment) ?',
	body: '<table>'+
			'<tr><th>Mood (Sentiment)</th><th>Sample Values</th></tr>'+
			'<tr><td>Clearly Positive*  HAPPY Green</td><td>"score": 0.8, "magnitude": 3.0</td></tr>'+
			'<tr><td>Clearly Negative* Sad Red</td><td>"score": -0.6, "magnitude": 4.0</td></tr>'+
			'<tr><td>Neutral Face Yellow</td><td>"score": 0.1, "magnitude": 0.0</td></tr>'+
			'<tr><td>Mixed</td><td>"score": 0.0, "magnitude": 4.0</td></tr>'+
		'<table><ol class="ol">'+
			'<li><pre>score</pre> of the sentiment ranges between <pre>-1.0</pre> (negative) and <pre>1.0</pre> (positive) and corresponds to the overall emotional leaning of the text.</li>'+
			'<li><pre>magnitude</pre> indicates the overall strength of emotion (both positive and negative) within the given text, between <pre>0.0</pre> and <pre>+inf</pre>. Unlike <pre>score</pre>, <pre>magnitude</pre> is not normalized; each expression of emotion within the text (both positive and negative) contributes to the text\'s <pre>magnitude</pre> (so longer text blocks may have greater magnitudes).</li>'+
		'</ol>',
	footer: '<button class="button gray modal-ok">OK</button>'
});
sentimentHelpModal.el.q('.modal-ok')[0].onclick = function(){
	sentimentHelpModal.hide();
};

function showMoreBtn(transcriptRow){
	transcriptRow.style.position = 'relative';
	var btn = arc.elem('a', 'Show More', {class: 'button small show-more-transcript'});
	btn.onclick = function(e){
		if (this.innerHTML == 'Show More'){
			transcriptRow.style.transition = 'none';
			transcriptRow.style.height = 'auto';
			var height = transcriptRow.offsetHeight;
			transcriptRow.style.height = '52px';
			transcriptRow.style.transition = 'height 0.5s';
			setTimeout(function(){
				transcriptRow.style.height = height+'px';
			}, 10);
			this.innerHTML = 'Show Less';
			transcriptRow.className = 'transcript-expand expanded';
		}
		else{
			transcriptRow.style.transition = 'height 0.5s';
			transcriptRow.style.height = '52px';
			this.innerHTML = 'Show More';
			transcriptRow.className = 'transcript-expand';
		}
		e.stopPropagation();
		e.preventDefault();
		e.cancelBubble = true;
		return false;
	}
	transcriptRow.appendChild(btn);
}
