
//var empSettingsData = {};

var moduleSettingsEmployees = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		eventLogger('start', 'loadEmpSettings');
		hidePrevAlerts();
		//doFetchData('settings', function(data, self, startD, endD){});
		//context.q('a[href="#employees-bulk-add"] img')[0].src = static_url+'img/icons/xlsx.png';	//	Can be removed when deploying to live
		//
		var employees = context.q('.empSettings_users')[0].tBodies[0];
		sorttable.makeSortable(employees.parentNode);
		var data = null;
		var rolesDropDown, mgrDropdowns = [];
		var mgrIndex = {};
		var mgrIndexAll = {};
		if (organizationSettings.myself.role < 80){
			context.q('.employee-filters_last')[0].style.display = 'none';
			/*context.q('.employee-filters_last a.button.bulk-add')[0].onclick = function(){
				alert('You need \'Administrator\' privileges.');
				return false;
			}*/
		}

		employees.innerHTML = '<tr><td colspan="5" style="width: 100%;">Fetching employees, please wait...</td></tr>';
		// clear search
		context.q('input[name="searchEmployee"]')[0].value = '';
		new arc.ajax(base_url + 'update-employee/', {
			callback: function (resp) {
				eventLogger('end', 'loadEmpSettings');
				data = eval('(' + resp.responseText + ')');
				//empSettingsData = data;
				var self = organizationSettings.myself;//data.employees[uid];
				var icon, email, chk, checkboxes;
				var saveCancelButtons = context.q('button.primary, button.gray');
				//
				employees.innerHTML = '';
				filterByManager[0].innerHTML = '<option value="*">[ Filter by ]</option><option value="52">None</option>';
				searchRole[0].value = '*';
				if (Object.keys(mgrIndex).length == 1) {
					employees4Mgr = Object.keys(mgrIndex)[0];
					filterByManager[0].value = employees4Mgr;
				}
				//****************************************************
				//****************************************************
				var counts = employeeSettingsRows(data, data.employees, employees, employees4Mgr, mgrDropdowns, mgrIndex, mgrIndexAll, self);
				//var newCount = counts.newCount; var termCount = counts.termCount;
				//****************************************************
				//****************************************************
				//
				//context.q('.employee-filters .terminatedToggle .count')[0].innerHTML = '(' + counts.termCount + ')';
				context.q('.employee-filters .newEmpToggle .count')[0].innerHTML = '(' + counts.newCount + ')';
				//
				//	CONTROL ACTIVATED ACCOUNTS NOT EXCEEDING SUBSCRIBED SEATS
				data.setActivated = function(delta){
					data.activated += delta;
					let remaining = data.seats - data.activated;
					employees.parentNode.tFoot.q('span.remaining')[0].style.color = (remaining < 0 ? 'red' : '');
					employees.parentNode.tFoot.q('span.remaining')[0].innerHTML = remaining;
					employees.parentNode.tFoot.q('span.seats')[0].innerHTML = data.seats;
					if (remaining < 0){
						saveCancelButtons[0].setAttribute('disabled', true);
						scrollToTop();
						hidePrevAlerts();
						showAlert('error', 'You do not have sufficient licenses to activate the selected users.');
					}
					else{
						saveCancelButtons[0].removeAttribute('disabled');
					}
				};
				if (data.seats == null || data.seats == Infinity){
					data.seats = Infinity;
					employees.parentNode.q('tfoot tr td')[1].style.display = 'none';
				}
				else
					employees.parentNode.q('tfoot tr td')[1].style.display = 'block';
				data.setActivated(0);
				hidePrevAlerts();
				//
				/*if ( counts.termCount < 1) {
					context.q('.employee-filters input[name="retrieveTerminated"]')[0].setAttribute('disabled', true);
					context.q('.employee-filters input[name="retrieveTerminated"]')[0].classList.remove('has-data');
				}
				else {
					context.q('.employee-filters input[name="retrieveTerminated"]')[0].removeAttribute('disabled');
					context.q('.employee-filters input[name="retrieveTerminated"]')[0].classList.add('has-data');
				}*/
				//
				for (let i = 0; i < searchRole[0].options.length; i++)
					searchRole[0].options[i].style.display = typeof counts.roles[ searchRole[0].options[i].value ] == 'undefined' && searchRole[0].options[i].value != '*' ? 'none': '';
				//
				if (counts.newCount < 1){
					context.q('.employee-filters input[name="showNewEmployees"]')[0].setAttribute('disabled', true);
					context.q('.employee-filters input[name="showNewEmployees"]')[0].classList.remove('has-data');
				}
				else{
					context.q('.employee-filters input[name="showNewEmployees"]')[0].removeAttribute('disabled');
					context.q('.employee-filters input[name="showNewEmployees"]')[0].classList.add('has-data');
				}
				//
				retrieveTerminated.checked = false;
				sortColumn(employees.parentNode, 0);
				//
				noRecordsRow[0] = arc.elem('tr', '<td colspan="5"><i>No records found</i></td>', {
					class: 'no-records',
					style: 'display:none;'
				});
				employees.a(noRecordsRow[0]);
				//
				for (var i in mgrIndex)
					if (arrayIgnore.indexOf(i) == -1) {
						filterByManager[0].a(arc.elem('option', data.employees[i].fullname, {
							value: i
						}));
						for (var j in mgrDropdowns)
							if (arrayIgnore.indexOf(j) == -1) {
								var tmpOption = arc.elem('option', data.employees[i].fullname, {
									value: i
								});
								if (mgrDropdowns[j].getAttribute('data-value') == i)
									tmpOption.selected = true;
								if (mgrDropdowns[j].parentNode.parentNode.getAttribute('data-id') == i)
									tmpOption.disabled = true;
								mgrDropdowns[j].a(tmpOption);
							}
					}
				//	Add managers who are not yet assigned subordinates
				for (var i in mgrIndexAll)
					if (arrayIgnore.indexOf(i) == -1 && typeof mgrIndex[i] == 'undefined')
						for (var j in mgrDropdowns)
							if (arrayIgnore.indexOf(j) == -1){
								var tmpOption = arc.elem('option', data.employees[i].fullname, {
									value: i
								});
								if (mgrDropdowns[j].getAttribute('data-value') == i)
									tmpOption.selected = true;
								if (mgrDropdowns[j].parentNode.parentNode.getAttribute('data-id') == i)
									tmpOption.disabled = true;
								mgrDropdowns[j].a(tmpOption);
							}
				//
				sortList(filterByManager[0], 1);
				for (var j in mgrDropdowns)
					if (arrayIgnore.indexOf(j) == -1)
						sortList(mgrDropdowns[j], 0, false);
				//
				/*if (typeof params[0] != 'undefined')
					moduleSettingsEmployees.onload(context, data, mgrIndex, params[0]);
					//showEmployeeSettingsPage(context/*employeeSettings* /, data, mgrIndex, params[0]);*/
				//
				//	---------------------------------------------------------------------------------------------
				//	SAVE AND CANCEL FOR EMPLOYEE BULK UPDATE TABLE
				//	---------------------------------------------------------------------------------------------
				//	SAVE
				saveCancelButtons[0].onclick = function (){
					var employees = context.q('tr.tablerow[data-id]');
					var upload = [],
						hasErrors = false;
					//	Find and compose a list of changed settings (diff).
					for (var i = 0; i < employees.length; i++){
						if (employees[i].style.display != 'none'){
							var attribs = employees[i].q('select');
							var reportEmail = employees[i].q('input[type="email"]')[0];
							var reportFrequency = employees[i].q('input[type="checkbox"]');
							var empID = employees[i].getAttribute('data-id');
							var tmp = {
								id: empID
							};
							//
							if (reportEmail.className != '') {
								if (reportEmail.value == '' || /\S+@\S+/.test(reportEmail.value))	// /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
									tmp.reportEmail = reportEmail.value;
								else {
									reportEmail.className = 'warning';
									if (!hasErrors) {
										showAlert('error', 'Please enter a valid email address');
										reportEmail.focus();
										scrollToTop();
									}
									hasErrors = true;
								}
							}
							//
							if (reportFrequency[0].parentNode.className != '' || reportFrequency[1].parentNode.className != '')
								tmp.reportFrequency = '10' + (reportFrequency[1].checked ? '1' : '0') + (reportFrequency[0].checked ? '1' : '0');
							//
							if (attribs.length == 1){
								if (attribs[0].className != '')
									tmp.status = attribs[0].value;
							}
							else if (attribs.length == 2){
								if (attribs[0].className != '')
									tmp.role = attribs[0].value;
								if (attribs[1].className != '')
									tmp.status = attribs[1].value;
							}
							else if (attribs[0].className != '' || attribs[1].className != '' || attribs[2].className != '') {
								if (attribs[0].className != '')
									tmp.role = attribs[0].value;
								if (attribs[1].className != '')
									tmp.manager = attribs[1].value;
								if (attribs[2].className != '')
									tmp.status = attribs[2].value;
							}
							if (Object.keys(tmp).length > 1)
								upload.push(tmp);
						}
					}
					//	Update if any changes
					if (upload.length > 0 && !hasErrors) { // change is here
						eventLogger('start', 'saveEmpSettings');
						new arc.ajax(base_url + 'update-employee/', {
							method: 'POST',
							data: upload,
							callback: function (response){
								response = JSON.parse(response.responseText);
								if (typeof response['message'] == 'undefined' || response['message'] != 'database updated'){
									showAlert('error', 'Failed to save employee settings');
									eventLogger('end', 'saveEmpSettings');
									scrollToTop();
									return false;
								}
								else
									window.newlyActivatedEmployees = response['data-ids'];
								//
								var keys = {
									'role': 0,
									'manager': 1,
									'status': 2
								}, tmp;
								for (var i = 0; i < upload.length; i++){
									for (key in upload[i]){
										if (key == 'id'){
										}
										else if (key == 'reportFrequency'){
											checkboxes = context.q('tr.tablerow[data-id="' + upload[i].id + '"] input[type="checkbox"]');
											checkboxes[0].parentNode.className = 'inp-switch';
											checkboxes[0].setAttribute('data-value', checkboxes[0].checked ? '1' : '0');
											checkboxes[1].parentNode.className = 'inp-switch';
											checkboxes[1].setAttribute('data-value', checkboxes[1].checked ? '1' : '0');
										}
										else if (key == 'reportEmail'){
											tmp = context.q('tr.tablerow[data-id="' + upload[i].id + '"] input[type="email"]')[0];
											tmp.setAttribute('data-value', upload[i][key]);
											tmp.className = '';
										}
										else{
											tmp = context.q('tr.tablerow[data-id="' + upload[i].id + '"] select')[keys[key]];
											if (typeof tmp != 'undefined'){
												tmp.setAttribute('data-value', upload[i][key]);
												tmp.className = '';
											}
										}
										//*/
										try{
											if (key != 'id' && typeof processedData.employees != 'undefined' && typeof processedData.employees[upload[i].id] != 'undefined')
												processedData.employees[upload[i].id][key] = upload[i][key];
										}
										catch (e){
										}
										//*/
										//	If self.status is changed - Change access to myprofile link
										if (key == 'status' && upload[i].id == organizationSettings.myself.id){
											organizationSettings.myself.status = upload[i][key];
											setMyProfileLink(organizationSettings.myself);
										}
									}
								}
								//fillMgrsNTeamsLists(null, empSettingsData, upload);
								//	ALERT USER OF SUCCESFUL UPDATE
								var employees = context.q('tr.tablerow[data-id]');
								for (var i = 0; i < employees.length; i++) {
									if (employees[i].style.display != 'none') {
										var attribs = employees[i].q('select');
										var empID = employees[i].getAttribute('data-id');
										//
										attribs[0].className = '';
										attribs[0].setAttribute('data-value', attribs[0].value);
										if (attribs.length > 2){
											attribs[1].className = '';
											attribs[1].setAttribute('data-value', attribs[1].value);
											attribs[2].className = '';
											attribs[2].setAttribute('data-value', attribs[2].value);
										}
										else if (attribs.length > 1){
											attribs[1].className = '';
											attribs[1].setAttribute('data-value', attribs[1].value);
										}
									}
								}
								showAlert('success', 'Employee settings saved');
								eventLogger('end', 'saveEmpSettings');
								scrollToTop();
							},
							fallback: function (data){
								showAlert('error', 'Failed to save employee settings');
								eventLogger('end', 'saveEmpSettings');
								scrollToTop();
							}
						});
					}
					//	TO DO : Send data by Ajax, Update local storage, reset bgcolors
					//scrollToTop();
				}
				//	---------------------------------------------------------------------------------------------
				//	RESET the employee settings editable table - drop-downs
				saveCancelButtons[1].onclick = function () {
					var selects = context.q('select[data-value]');
					hasChanges = [];
					employees.parentNode.tFoot.q('tr td')[0].style.display = 'none';
					//	NEED TO REFACTOR THIS TO ITERATE ROW-BY-ROW
					for (var i = 0, j = 0, pre_role = null, cur_role = null; i < selects.length; i++){
						if ( j == 0 )
							pre_role = selects[i].value;
						//
						selects[i].value = selects[i].getAttribute('data-value');
						selects[i].className = '';
						selects[i].parentNode.setAttribute('sorttable_customkey',
							(selects[i].querySelector('[value="' + selects[i].getAttribute('data-value') + '"]') || {'innerHTML': ''}).innerHTML
						);

						// disable/enable visibility dropdown according to role
						if ( j == 0 ){	//	Role
							cur_role = selects[i].value;
							if (pre_role < 1 && cur_role > 0){
								data.setActivated(1);
							}
							else if (pre_role > 0 && cur_role < 1){
								data.setActivated(-1);
							}
							pre_role = null;
							selects[i].parentNode.parentNode.setAttribute('data-role', cur_role);
						}
						else if ( j == 1 ){	//	Manager
							selects[i].parentNode.parentNode.setAttribute('data-manager', selects[i].value);
						}
						else if ( j == 2 ){	//	Visibility
							if ( cur_role <= 0 ){
								selects[i].setAttribute('disabled', true);
							}
							else{
								selects[i].removeAttribute('disabled');
							}
							cur_role = null;
							selects[i].parentNode.parentNode.setAttribute('data-status', selects[i].value);
							j = -1;
						}
						j++;
					}
					var emails = context.q('input[type="email"][data-value]');
					for (var i = 0; i < emails.length; i++) {
						emails[i].value = emails[i].getAttribute('data-value');
						emails[i].className = '';
						emails[i].parentNode.setAttribute('sorttable_customkey', (emails[i].querySelector('[value="' + emails[i].getAttribute('data-value') + '"]') || {
							'innerHTML': ''
						}).innerHTML);
					}
					var ticks = context.q('input[type="checkbox"][data-value]');
					for (var i = 0; i < ticks.length; i++) {
						ticks[i].checked = ticks[i].getAttribute('data-value') == 1 ? true : false;
						ticks[i].parentNode.className = 'inp-switch';
					}
					scrollToTop();
				}
			}
		});

		(function () {
			bindFilters();
			// retrieve terminated employees
			var terminatedEmpData = null;
			var termRows = [];
			var self = organizationSettings.myself;

			function populateTerminated (term_data) {

				var icon, email, chk, checkboxes;
				var rolesDropDown, mgrDropdowns = [];

				/*if (Object.keys(mgrIndex).length == 1) {
					employees4Mgr = Object.keys(mgrIndex)[0];
					filterByManager[0].value = employees4Mgr;
				}*/
				filterByManager[0].value = '*';

				//*/****************************************************
				//****************************************************
				var result = employeeSettingsRows(data, term_data.employees, employees, employees4Mgr, mgrDropdowns, mgrIndex, mgrIndexAll, self);
				termRows = result.rows;
				//****************************************************
				//****************************************************

				for (var i in mgrIndex) {
					if (arrayIgnore.indexOf(i) == -1) {
						// filterByManager[0].a(arc.elem('option', data.employees[i].fullname, {
						// 	value: i
						// }));
						for (var j in mgrDropdowns) {
							if (arrayIgnore.indexOf(j) == -1) {
								var tmpOption = arc.elem('option', data.employees[i].fullname, {
									value: i
								});
								if (mgrDropdowns[j].getAttribute('data-value') == i)
									tmpOption.selected = true;
								if (mgrDropdowns[j].parentNode.parentNode.getAttribute('data-id') == i)
									tmpOption.disabled = true;
								mgrDropdowns[j].a(tmpOption);
							}
						}
					}
				}

				//	Add managers who are not yet assigned subordinates
				for (var i in mgrIndexAll) {
					if (arrayIgnore.indexOf(i) == -1 && typeof mgrIndex[i] == 'undefined') {
						for (var j in mgrDropdowns) {
							if (arrayIgnore.indexOf(j) == -1){
								var tmpOption = arc.elem('option', data.employees[i].fullname, {
									value: i
								});
								if (mgrDropdowns[j].getAttribute('data-value') == i)
									tmpOption.selected = true;
								if (mgrDropdowns[j].parentNode.parentNode.getAttribute('data-id') == i)
									tmpOption.disabled = true;
								mgrDropdowns[j].a(tmpOption);
							}
						}
					}
				}

				//
				sortList(filterByManager[0], 1);
				for (var j in mgrDropdowns)
					if (arrayIgnore.indexOf(j) == -1)
						sortList(mgrDropdowns[j], 0, false);
				//
				for (var s of mgrDropdowns){
					var existingVals = [];
					for (var o of s.options){
						if (existingVals.indexOf(o.value) === -1) {
							existingVals.push(o.value);
						}
						else{
							o.style.display = 'none';
						}
					}
				}
			}
			//
			function showTerminated(){
				var rows = employees.q('tr');
				for (var i = 0; i < rows.length; i++) {
					var row = rows[i];
					row.style.display = 'none';
				}
				for (var j = 0; j < termRows.length; j++) {
					termRows[j].style.display = 'block';
				}
				//searchEmployee[1].setAttribute('disabled', 'disabled');
				searchRole[0].setAttribute('disabled', 'disabled');
				filterByManager[0].setAttribute('disabled', 'disabled');
				showNewEmployees.setAttribute('disabled', 'disabled');
				eventLogger('end', 'fetchTerminated');
			}
			//
			function hideTerminated(){
				for (var j = 0; j < termRows.length; j++){
					/*if (window.newlyActivatedEmployees.indexOf(termRows[j].getAttribute('data-id')*1) > 0){
						termRows[j].addClass('highlight');
						termRows[j].removeClass('role-terminated');
					}
					else*/
					termRows[j].style.display = 'none';
				}
				//searchEmployee[1].removeAttribute('disabled');
				searchRole[0].removeAttribute('disabled');
				filterByManager[0].removeAttribute('disabled');
				showNewEmployees.removeAttribute('disabled');
				searchRole[0].onchange();
				eventLogger('end', 'fetchTerminated');
			}
			//
			var getTerminatedToggle = context.q('input[name="retrieveTerminated"]')[0];
			getTerminatedToggle.checked = false;
			showNewEmployees.checked = false;
			getTerminatedToggle.onchange = function () {
				eventLogger('start', 'fetchTerminated');
				if (this.checked){
					// get data if not cached
					if (terminatedEmpData === null) {
						new arc.ajax(base_url + 'update-employee/?terminated=True', {
							callback: function (resp) {
								resp = eval('(' + resp.responseText + ')');
								terminatedEmpData = resp;
								for (var i in terminatedEmpData.employees) {
									if (typeof data.employees[i] === 'undefined') {
										data.employees[i] = terminatedEmpData.employees[i];
									}
								}
								populateTerminated(terminatedEmpData);
								showTerminated();
							}
						});
					}
					else{
						showTerminated();
					}
				}
				else{
					// show other data
					hideTerminated();
				}
			}
		})();
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		if (typeof loaded == 'function')
			loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};

function employeeSettingsRows(data, employees, datatable, employees4Mgr, mgrDropdowns, mgrIndex, mgrIndexAll, self){
	var rows = [], newCount = 0, termCount = 0, employeesShortlist = [];
	var hasChanges = [], avRoles = {};
	var showHideSaveCancelButtons = function(input, status, tfoot, index){
		if (typeof index == 'undefined')
			index = 0;
		var row = input.parentNode.parentNode.rowIndex;
		var cell = input.parentNode.cellIndex+index;
		if (typeof hasChanges[row] == 'undefined')
			hasChanges[row] = [];
		hasChanges[row][cell] = status;
		//row = row.parentNode.indexOf(row);
		//console.log(JSON.stringify(hasChanges));
		tfoot.q('tr td')[0].style.display = hasChanges.toString().indexOf('true') > -1 ? 'block' : 'none';
	};
	for (var i in employees){
		if (employees4Mgr == -1 || employees[i].manager == employees4Mgr) {
			//
			if (self.role == 80){
				var tmpDropdown = makeDropdown({}, employees[i].manager, function (input) {
					input.parentNode.parentNode.setAttribute('data-manager', input.value);
					var managerIsSetInFilter = false;
					for (var i = filterByManager[0].childNodes.length-1; i > -1; i--){
						var managerOption = filterByManager[0].childNodes[i];
						if (managerOption.value == input.value){
							managerIsSetInFilter = true;
							break;
						}
					}
					//
					if (!managerIsSetInFilter){
						for (var i = input.childNodes.length-1; i > -1; i--){
							var optionManager = input.childNodes[i];
							if (optionManager.value == input.value){
								filterByManager[0].a(arc.elem('option', optionManager.innerHTML, {
									value: input.value
								}));
							}
						}
					}
					//
					showHideSaveCancelButtons(input, input.className=='updated', datatable.parentNode.tFoot);
				}, 52);
				//
				if (employees[i].role <= 0 ) {
					tmpDropdown.setAttribute('disabled', true);
				}
				mgrDropdowns.push(tmpDropdown);
			}
			else
				var tmpDropdown = typeof employees[employees[i].manager] != 'undefined' ? employees[employees[i].manager].fullname : '';
			//
			//if ((self.role == 80) || (employees[i].manager == uid || (typeof employees[employees[i].manager] != 'undefined' && employees[employees[i].manager].manager == uid))) {
			if (self.role == 80 || employees[uid].manager != i) {
				var email = employees[i].report_email;
				/*if ((employees[i].report_email == null || employees[i].report_email == '') && employees[i].is_app_user == 0 && employees[i].email.indexOf('@') > -1)
					email = employees[i].email;*/
				if (email == null)
					email = '';
				icon = usernameIcon(employees[i].fullname);
				chk = ('00' + employees[i].report_enabled).split('').reverse();
				avRoles[employees[i].role] = true;

                let rolesDict = roles;
				if (employees[i].is_app_user){
					rolesDict = {0:'Not activated', 70:'Manager', 80:'Administrator', '-1':'Terminated'};
					if (typeof rolesDict[ employees[i].role ] == 'undefined')
						rolesDict[ employees[i].role ] = roles[employees[i].role];
				}

				rolesDropDown = (self.role < 80 ? roles[employees[i].role] :
					sortList(makeDropdown(rolesDict, employees[i].role,
						function (input, e) {
							let prevVal = input.parentNode.parentNode.getAttribute('data-role');
							if (prevVal > 69 && input.value < 70){
								if (!confirm('Are you sure?\nChanging this employee\'s user role will reset the manager name to "none" for any employee who currently reports to this employee.')){
									e.cancelBubble = true;
									e.stopPropagation();
									e.preventDefault();
									input.value = prevVal;
									return false;
								}
							}

							var row = input.parentNode.parentNode;
							var mgr_id = row.getAttribute('data-id');
							var mgr_name = row.q('p.user-name')[0].innerHTML;
							var checkboxes = row.q('input[type="checkbox"]');
							var row_status_dd = row.q('select')[2];
							var row_mgr = row.q('select')[1];
							// Disable Daily Reports checkbox on admin

							/*// NOT USED
							if (input.value != 0){
								row.setAttribute('new-employee-change', true);
							}*/

							//	Admins don't have daily emails
							if (input.value == 80){
								checkboxes[1].setAttribute("disabled", true);
								checkboxes[1].checked = false;
							}
							else{
								checkboxes[1].removeAttribute("disabled");
							}

							//	ACTIVATE EMPLOYEE
							if (input.value > 0 && prevVal < 1){
								//	Toggle disabled attribute of status dropdown according to role
								row_status_dd.removeAttribute("disabled");
								row_status_dd.value = row_status_dd.getAttribute('data-value');
								row_mgr.removeAttribute('disabled');
								data.setActivated(1);
								//row_mgr.value = row_mgr.getAttribute('data-value');
							}
							//	DE-ACTIVATE EMPLOYEE
							else if (input.value < 1 && prevVal > 0){
								row_status_dd.setAttribute("disabled", true);
								row_status_dd.value = -1;
								row_mgr.setAttribute('disabled', true);
								row_mgr.value = 52;
								data.setActivated(-1);
							}
							row_status_dd.className = row_status_dd.value == row_status_dd.getAttribute('data-value') ? '' : 'updated';
							//
							//	Add manager to all dropdowns to be selected as a manager
							if (input.value > 69 && prevVal < 70){
								for (var k = 0; k < mgrDropdowns.length; k++) {
									mgrDropdowns[k].a(arc.elem('option', mgr_name, {
										'value': mgr_id
									}));
									if (mgrDropdowns[k].getAttribute('data-value') == mgr_id){
										mgrDropdowns[k].value = mgr_id;
										mgrDropdowns[k].className = '';
									}
									var parentRow = mgrDropdowns[k].parentNode.parentNode;
									var parentRowId = parentRow.getAttribute("data-id");
									if (parentRowId == mgr_id){
										for (var i = mgrDropdowns[k].childNodes.length-1; i > -1; i--){
											var optionValue = mgrDropdowns[k].childNodes[i].value;
											if (typeof optionValue != 'undefined' && optionValue == mgr_id){
												mgrDropdowns[k].childNodes[i].setAttribute('disabled', true);
											}
										}
									}
								}
							}
							//
							//	Remove manager from all dropdowns
							else if (prevVal > 69 && input.value < 70){
								for (var k = 0; k < mgrDropdowns.length; k++) {
									if (mgrDropdowns[k].value == mgr_id){
										mgrDropdowns[k].value = 52;
										mgrDropdowns[k].addClass('updated');
									}
									let tmpDel = mgrDropdowns[k].q('select[value="' + mgr_id + '"]');
									if (tmpDel.length > 0)
										mgrDropdowns[k].removeChild(tmpDel[0]);
								}
								let tmpDel = filterByManager[0].q('select[value="' + mgr_id + '"]');
								if (tmpDel > 0)
									filterByManager[0].removeChild(tmpDel[0]);
								/*//	Shortened
								// var testAttr = filterByManager[0].
								for (var i = filterByManager[0].childNodes.length-1; i > -1; i--){
									var managerOption = filterByManager[0].childNodes[i];
									if (managerOption.value == mgr_id){
										filterByManager[0].removeChild(managerOption);
									}
								}*/
							}
							//
							input.parentNode.parentNode.setAttribute('data-role', input.value);
							showHideSaveCancelButtons(input, input.className=='updated', datatable.parentNode.tFoot);
						}, -1), -1, false
					)
				);
				if (uid == i && typeof rolesDropDown != 'string') {
					rolesDropDown.disabled = true;
				}

				// set visibility status to -1 if role = system, terminated, or deactivated
				var status_dropdown = makeDropdown(
					statuses,
					(employees[i].role > 0 ? employees[i].status : -1),
					function (input) {
						/*var row = input.parentNode.parentNode;
						var mgr_id = row.getAttribute('data-id');
						//
						//	Add manager to all dropdowns to be selected as a manager
						if (input.value == 1) {
							for (var k = 0; k < mgrDropdowns.length; k++) {
								/*mgrDropdowns[k].a(arc.elem('option', mgr_name, {
									'value': mgr_id
								}));* /
								if (mgrDropdowns[k].getAttribute('data-value') == mgr_id){
									mgrDropdowns[k].value = mgr_id;
									mgrDropdowns[k].className = '';
								}
								/*var parentRow = mgrDropdowns[k].parentNode.parentNode;
								var parentRowId = parentRow.getAttribute("data-id");
								if (parentRowId == mgr_id){
									for (var i = mgrDropdowns[k].childNodes.length-1; i > -1; i--){
										var optionValue = mgrDropdowns[k].childNodes[i].value;
										if (typeof optionValue != 'undefined' && optionValue == mgr_id){
											mgrDropdowns[k].childNodes[i].setAttribute('disabled', true);
										}
									}
								}* /
							}
						}
						//
						//	Remove manager from all dropdowns
						if (input.parentNode.parentNode.getAttribute('data-status') == 1){
							for (var k = 0; k < mgrDropdowns.length; k++) {
								if (mgrDropdowns[k].value == mgr_id){
									mgrDropdowns[k].value = 52;
									mgrDropdowns[k].addClass('updated');
								}
								//mgrDropdowns[k].removeChild(mgrDropdowns[k].q('[value="' + mgr_id + '"]')[0]);
							}
						}*/
						//
						input.parentNode.parentNode.setAttribute('data-status', input.value);
						showHideSaveCancelButtons(input, input.className=='updated', datatable.parentNode.tFoot);
					}, -1);

				// disable status_dropdown if role = system, terminated, or deactivated
				if ( employees[i].role <= 0) {
					status_dropdown.setAttribute("disabled", true);
				}

				// new and terminated count
				if (employees[i].role === 0) {
					newCount++;
				} else if (employees[i].role < 0) {
					termCount++;
				}



				var employee = arc.reactor({
					tr: {
						class: 'tablerow role-' + ( employees[i].role === 0 ? 'new' : roles[employees[i].role].toLowerCase())+(window.newlyActivatedEmployees.indexOf(i*1) > -1 ? ' highlight' : ''),
						'data-id': i,
						'data-role': employees[i].role,
						'data-manager': employees[i].manager,
						'data-new': (employees[i].role === 0),
						'data-status': employees[i].status,
						content: [
							{
								td: {
									content: {
										div: {
											class: 'user-name-wrap',
											content: {
												p: {
													class: 'user-name',
													content: employees[i].fullname
												}
											}
										}
									},
									onclick: function () {
										document.location.hash = 'employee/' + this.parentNode.getAttribute('data-id');
									},
									style: 'border-left-color:#' + icon[1]
								}
							},
							{
								td: {
									content: rolesDropDown,
									class: (typeof rolesDropDown == 'string' ? 'has-gutter' : 'no-gutter'),
									'sorttable_customkey': roles[employees[i].role]
								}
							},
							{
								td: {
									content: tmpDropdown,
									class: typeof tmpDropdown == 'string' ? 'has-gutter' : 'no-gutter',
									title: (employees[employees[i].manager] == undefined ? '' : employees[employees[i].manager].fullname),
									'sorttable_customkey': (employees[employees[i].manager] == undefined ? '' : employees[employees[i].manager].fullname)
								}
							},
							{
								td: {
									content: status_dropdown,
									'sorttable_customkey': statuses[(employees[i].role > 0 ? employees[i].status : -1)],
									class: 'no-gutter'
								}
							},
							//
							//	Report Email
							{
								td: {
									content: {
										input: {
											type: 'email',
											value: email,
											'data-value': email, // placeholder: employees[i].email,
											onkeyup: function () {
												this.className = this.value != this.getAttribute('data-value') ? 'updated' : '';
												showHideSaveCancelButtons(this, this.className=='updated', datatable.parentNode.tFoot);
											}
										}
									},
									class: 'no-gutter'
								}
							},
							//
							//	Report Frequency
							{
								td: {
									content: [
										{
											label: {
												content: [
													{
														input: {
															type: 'checkbox',
															'data-value': chk[0] || 0,
															onclick: function () {
																this.parentNode.className = this.checked == this.getAttribute('data-value') == 1 ? 'inp-switch' : 'inp-switch updated';
																showHideSaveCancelButtons(this.parentNode, this.parentNode.className=='inp-switch updated', datatable.parentNode.tFoot, 0);
															}
														}
													},
													{
														span: {
															content: 'Weekly'
														}
													}
												],
												class: 'inp-switch'
											}
										},
										{
											label: {
												content: [
													{
														input: {
															type: 'checkbox',
															'data-value': chk[1] || 0,
															onclick: function () {
																this.parentNode.className = this.checked == this.getAttribute('data-value') == 1 ? 'inp-switch' : 'inp-switch updated';
																showHideSaveCancelButtons(this.parentNode, this.parentNode.className=='inp-switch updated', datatable.parentNode.tFoot, 1);
															}
														}
													},
													{
														span: {
															content: 'Daily'
														}
													}
												],
												class: 'inp-switch'
											}
										}
									],
									class: 'inp-switch-col'
								}
							}
						]
					}
				});

				if (employees[i].role < 1)
					employee.style.display = 'none';
				datatable.a(employee);
				rows.push(employee);
				checkboxes = employee.q('input[type="checkbox"]');
				checkboxes[0].checked = chk[0] == '1';
				checkboxes[1].checked = chk[1] == '1';
				// Disable daily reports if admin
				if ( employees[i].role == 80 ) {
					checkboxes[1].setAttribute("disabled", true);
					checkboxes[1].checked = false;
				}
				found = true;
				employeesShortlist[i] = [employees[i].fullname, icon[1]];
			}
		}
		//if ((self.role == 80 || employees[i].manager == uid || (typeof employees[employees[i].manager] != 'undefined' && employees[employees[i].manager].manager == uid)))
		if (typeof employees[employees[i].manager] != 'undefined' && typeof mgrIndex[employees[i].manager] == 'undefined')
			if (employees[i].role > 0 && employees[i].status == 1 && employees[employees[i].manager].role > 0)// && employees[employees[i].manager].status == 1
				mgrIndex[employees[i].manager] = true;
			//	List all managers - by role
		if (employees[i].role == 80 || (self.role == 80 || employees[i].manager == uid) && employees[i].role == 70)//employees[i].status == 1 && 
			mgrIndexAll[i] = true;
	}
	window.newlyActivatedEmployees = [];
	return {newCount: newCount, termCount: termCount, rows: rows, roles: avRoles};//Object.keys()
}