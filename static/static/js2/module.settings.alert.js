
var moduleSettingsAlert = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		hidePrevAlerts();
		eventLogger('start', 'alertSettings');
		if (organizationSettings.myself.role == 80)
			context.q('#adminOnly')[0].style.display = 'block';
		var buttons = context.q('.btngroup.btn');
		var btnsets = [[0, 1], [2, 3], [4, 5], [6, 7], [8, 9]];
		for (var j = 0; j < btnsets.length; j++)
			new(function (btnset) {
				for (var i = 0; i < btnset.length; i++)
					if (typeof buttons[btnset[i]] != 'undefined'){
						buttons[btnset[i]].onclick = function () {
							for (var j = 0; j < btnset.length; j++)
								buttons[btnset[j]].removeClass('active');
							this.addClass('active');
							return false;
						};
					}
			})(btnsets[j]);
		//	---------------------------------------------------------------------------------------------------------
		//	Save alert settings
		//	---------------------------------------------------------------------------------------------------------
		var form = context.q('form')[0];
		form.onsubmit = function () {
			eventLogger('start', 'saveAlertSettings');
			let alert_data = {
				enabled_alert_below: buttons[0].className.indexOf('active') > -1,
				enabled_alert_below_period: buttons[2].className.indexOf('active') > -1,
				enabled_alert_inactive: buttons[4].className.indexOf('active') > -1,
				enabled_alert_inactive_period: buttons[6].className.indexOf('active') > -1
			};
			if (buttons.length > 8)
				alert_data.enabled_alert_new_employee = buttons[8].className.indexOf('active') > -1;
			new arc.ajax(base_url + 'alert-settings/', {
				method: 'POST',
				data: alert_data,
				callback: function (data) {
					//data = eval('('+data.responseText+')');
					showAlert('success', 'Your alert settings are saved.', 4000);
					eventLogger('end', 'saveAlertSettings');
					/*/ update organization settings and update notifications
					organizationSettings.alert_below_period = (alert_data.enabled_alert_below_period ? 1 : 0);
					organizationSettings.alert_below_yesterday = (alert_data.enabled_alert_below ? 1 : 0);
					organizationSettings.alert_inactive_period = (alert_data.enabled_alert_inactive_period ? 1 :0);
					organizationSettings.alert_inactive_yesterday = (alert_data.enabled_alert_inactive ? 1 : 0);
					organizationSettings.alert_new_employee = (alert_data.enabled_alert_new_employee ? 1 : 0);
					if (organizationSettings.domain == organizationSettings.myself.email.split('@')[1])
						notifications.renderNotification(false);*/
					scrollToTop();
				}
			});
			return false;
		}
		//	---------------------------------------------------------------------------------------------------------
		//	Load alert settings
		//	---------------------------------------------------------------------------------------------------------
		new arc.ajax(base_url + 'alert-settings/', {
			callback: function (data) {
				eventLogger('end', 'alertSettings');
				data = eval('(' + data.responseText + ')');
				buttons[0].className = 'btngroup btn' + (data.enabled_alert_below ? ' active' : '');
				buttons[1].className = 'btngroup btn' + (data.enabled_alert_below ? '' : ' active');
				buttons[2].className = 'btngroup btn' + (data.enabled_alert_below_period ? ' active' : '');
				buttons[3].className = 'btngroup btn' + (data.enabled_alert_below_period ? '' : ' active');
				buttons[4].className = 'btngroup btn' + (data.enabled_alert_inactive ? ' active' : '');
				buttons[5].className = 'btngroup btn' + (data.enabled_alert_inactive ? '' : ' active');
				buttons[6].className = 'btngroup btn' + (data.enabled_alert_inactive_period ? ' active' : '');
				buttons[7].className = 'btngroup btn' + (data.enabled_alert_inactive_period ? '' : ' active');
				if (buttons.length > 8){
					buttons[8].className = 'btngroup btn' + (data.enabled_alert_new_employee ? ' active' : '');
					buttons[9].className = 'btngroup btn' + (data.enabled_alert_new_employee ? '' : ' active');
				}
			}
		});
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};