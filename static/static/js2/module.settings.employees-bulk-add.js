
var moduleEmpBulkAdd = module.exports = {
	module: this,
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		hidePrevAlerts();
		eventLogger('start', 'empBulkAdd');
		//	-----------------------------------------------------------------------------------------------
		//	Receive dropped or browsed file
		let fileUpload = context.q('input[type="file"]')[0];
		fileUpload.onchange = function(){
			fileDropped(this.files[0]);
			this.value = '';
		};
		let dropBox = fileUpload.parentNode.parentNode;
		dropBox.ondrop = function(e){
			var droppedFiles = (e.originalEvent || e).dataTransfer.files;
			if (droppedFiles.length > 0)
				fileDropped(droppedFiles[0]);
			e.preventDefault();
			e.stopPropagation();
			this.removeClass('hovering');
			fileUpload.value = '';
		};
		dropBox.ondragenter = dropBox.ondragover = function(e){
			e.preventDefault();
			this.addClass('hovering');
		};
		dropBox.ondragleave = function(e){
			this.removeClass('hovering');
		};
		//
		let xlsxDataPreview = context.q('#xlsx-data-preview')[0];
		let xlsxDataTable = xlsxDataPreview.q('table.users-table tbody')[0];
		sorttable.makeSortable(xlsxDataTable.parentNode);
		//
		let errorContainer = context.q('#error-container')[0];
		let errorReport = errorContainer.q('#error-report')[0];
		//
		let data;
		let submitButton = context.q('tfoot button.button.primary')[0];
		let cancelButton = context.q('tfoot button.button.gray')[0];
		let bulkAddSteps = context.q('#bulk-add-steps')[0];
		let bulkAddStsBar = arc.elem('div', '<span class="remaining">...</span> out of <span class="seats">...</span> licenses left', {class: 'center', style: 'display:none'});
		let tFootStsCntnr = xlsxDataTable.parentNode.q('tfoot tr td')[1];
		bulkAddSteps.a(bulkAddStsBar);
		//	-----------------------------------------------------------------------------------------------
		//	Fetch employees not-activated and existing managers
		let newEmployees = false,
			newEmpByEmail = {},
			extEmpByEmail = {};
		module.extManagers = {};
		new arc.ajax(base_url + 'update-employee/', {
			callback: function (resp) {
				data = eval('('+resp.responseText+')');//JSON.parse(resp.responseText);//Doesn't support/parse JavaScript.Infinity
				for (let key in data.employees){
					data.employees[ key ].id = key;
					if (data.employees[ key ].role == 0)
						newEmpByEmail[ data.employees[ key ].email.toLowerCase() ] = data.employees[ key ];
					else{
						extEmpByEmail[ data.employees[ key ].email.toLowerCase() ] = data.employees[ key ];
						if (data.employees[ key ].role > 69)
							module.extManagers[ data.employees[ key ].email.toLowerCase() ] = data.employees[ key ];
					}
				}
				data._activated = data.activated;
				newEmployees = data;
				//
				//	CONTROL ACTIVATED ACCOUNTS NOT EXCEEDING SUBSCRIBED SEATS
				data.setActivated = function(delta){
					data.activated += delta;
					let remaining = data.seats - data.activated;
					bulkAddStsBar.q('span.remaining')[0].style.color = (remaining < 0 ? 'red' : '');
					bulkAddStsBar.q('span.remaining')[0].innerHTML = remaining;
					bulkAddStsBar.q('span.seats')[0].innerHTML = data.seats;
					if (remaining < 0){
						submitButton.setAttribute('disabled', true);
						scrollToTop();
						hidePrevAlerts();
						showAlert('error', 'Number of users in the uploaded file exceeds the number of licenses available.');
					}
					else{
						submitButton.removeAttribute('disabled');
					}
				};
				if (data.seats == null || data.seats == Infinity){
					data.seats = Infinity;
					bulkAddStsBar.style.display = 'none';
				}
				else
					bulkAddStsBar.style.display = 'block';
				//
				data.setActivated(0);
				hidePrevAlerts();
			}
		});
		//	-----------------------------------------------------------------------------------------------
		//	Submit bulk-add employees to activate
		submitButton.onclick = function(){
			eventLogger('start', 'saveBulkAddEmps');
			postData = [];
			for (let i = 0; i < xlsxDataTable.rows.length; i++)
				if (xlsxDataTable.rows[i].getAttribute('data-status') == '1'){
					let chkbxs = xlsxDataTable.rows[i].q('input[type="checkbox"]');
					postData.push({
						id: xlsxDataTable.rows[i].getAttribute('data-id'),
						manager: xlsxDataTable.rows[i].getAttribute('data-manager'),
						role: xlsxDataTable.rows[i].getAttribute('data-role'),
						reportEmail: xlsxDataTable.rows[i].cells[4].innerHTML,
						reportFrequency: '10'+(chkbxs[0].checked ? '1' : '0')+(chkbxs[1].checked ? '1' : '0'),
						status: '1'
					});
				}
			//
			new arc.ajax(base_url + 'update-employee/', {
				method: POST,
				data: postData,
				callback: function (resp){
					eventLogger('end', 'saveBulkAddEmps');
					resp = eval('('+resp.responseText+')');//JSON.parse(resp.responseText);//Doesn't support/parse JavaScript.Infinity
					if (typeof resp['message'] != 'undefined' && resp['message'] == 'database updated'){
						window.newlyActivatedEmployees = resp['data-ids'];
						//document.location.href = '#employees';
						//
						data._activated = data.activated;
						cancelButton.onclick();
						//
						showAlert('success', 'Employees have been successfully activated.');
						//moduleEmpBulkAdd.onload(context, params);
					}
					else
						showAlert('error', 'There was an error activating new employees.');
					scrollToTop();
				},
				fallback: function(res){
					showAlert('error', 'There was an error activating new employees.');
					eventLogger('end', 'saveBulkAddEmps');
					scrollToTop();
				}
			});
		};
		cancelButton.onclick = function(){
			//moduleEmpBulkAdd.onload(context, params);
			hidePrevAlerts();
			scrollToTop();
			//
			data.activated = data._activated;
			data.setActivated(0);
			//
			xlsxDataPreview.style.display = 'none';
			xlsxDataTable.innerHTML = '';
			bulkAddSteps.a(bulkAddStsBar);
			//
			errorContainer.style.display = 'none';
			errorReport.innerHTML = '';
		};
		//	-----------------------------------------------------------------------------------------------
		//	Validate and load CSV data to DOM
		let validateAndLoadCSV = function(csv, file, fileData){
			//	Wait till new employees list loaded
			if (!newEmployees){
				setTimeout(function(){
					validateAndLoadCSV(csv, file, fileData);
				}, 520);
				return false;
			}
			var result = validateXLSXData(csv, file, fileData.split('base64,')[1],
				function(level, error){ // errorCallback
					errorContainer.style.display = 'block';
					errorReport.a( arc.elem('div', error+'<a class="fa fa-close" aria-hidden="true"></a>', {'class': 'message error'}) )
						.q('a.fa')[0].onclick = function(){
							errorReport.removeChild( this.parentNode );
							if (errorReport.childNodes.length == 0)
								errorContainer.style.display = 'none';
						};
				});
			if (!result){
				//	Errors cancel/clear button
				/*errorReport.a( arc.elem('div', '<button class="button gray">Cancel</button>', {'class': 'form-wrap'}) )
					.q('button')[0].onclick = function(){
						cancelButton.onclick();
					};*/
				return false;
			}
			//
			xlsxDataPreview.style.display = 'block';
			let rows = Object.keys(csv);
			let employees = {}, hasBulkAdd = false;
			for (let i = 2; i < rows.length; i++){
				let employee = csv[ rows[i] ];
				employee[2] = employee[2].toLowerCase();
				if (typeof employee[5] != 'undefined')
					employee[5] = employee[5].toLowerCase();
				employees[ employee[2] ] = employee;
			}
			for (let i = 2; i < rows.length; i++){
				let employee = csv[ rows[i] ];
				let exclude = false;
				if (typeof newEmpByEmail[ employee[2] ] == 'undefined'){
					exclude = true;
					if (typeof extEmpByEmail[ employee[2] ] == 'undefined')
						showAlert('error', 'Employee '+employee[1]+' &lt;'+employee[2]+'&gt; is not a new employee.');// already added or
				}
				else
					hasBulkAdd = true;
				//
				let icon = usernameIcon(employee[1]);
				let roleTxt = Object.values(roles);
				let roleKeys = Object.keys(roles);
				let role = roleKeys[ roleTxt.indexOf(employee[3]) ];
				let row = arc.reactor({
					tr: {
						class: 'tablerow role-'+employee[3].toLowerCase()+( exclude ? ' exclude' : '' ),
						'data-id': (typeof newEmpByEmail[ employee[2] ] != 'undefined' ? newEmpByEmail[ employee[2] ].id : (typeof module.extManagers[ employee[2] ] != 'undefined' ? module.extManagers[ employee[2] ].id : 'null')),//rows[i][0],
						'data-role': role,
						'data-manager': (typeof newEmpByEmail[ employee[5] ] != 'undefined' ? newEmpByEmail[ employee[5] ].id : (typeof module.extManagers[ employee[5] ] != 'undefined' ? module.extManagers[ employee[5] ].id : '52')),//employee[5],
						'data-status': (typeof newEmpByEmail[ employee[2] ] != 'undefined' ? 1 : 0),
						content: [
							{
								td: {
									content: {
										div: {
											class: 'user-name-wrap',
											content: {
												p: {
													class: 'user-name',
													content: employee[1]
												}
											}
										}
									},
									style: 'border-left-color:#' + icon[1]
								}
							},
							{
								td: {
									content: employee[3],
									'sorttable_customkey': employee[3]
								}
							},
							{
								td: {
									content: (employees[ employee[5] ] == undefined ? (module.extManagers[ employee[5] ] == undefined ? '' : module.extManagers[ employee[5] ].fullname) : employees[ employee[5] ][1]),
									'sorttable_customkey': (employees[ employee[5] ] == undefined ? (module.extManagers[ employee[5] ] == undefined ? '' : module.extManagers[ employee[5] ].fullname) : employees[ employee[5] ][1]),
									'data-id': (employees[ employee[5] ] == undefined ? (module.extManagers[ employee[5] ] == undefined ? '' : module.extManagers[ employee[5] ].id) : employees[ employee[5] ][1])
								}
							},
							{td: {content: ''}},
							//
							//	Report Email
							{
								td: {
									content: employee[2]
								}
							},
							//
							//	Report Frequency
							{
								td: {
									content: [
										{
											label: {
												content: [
													{
														input: {
															type: 'checkbox',
															'data-value': 0,
															onclick: function () {
																this.parentNode.className = this.checked == this.getAttribute('data-value') == 1 ? 'inp-switch' : 'inp-switch updated';
															}
														}
													},
													{span: {content: 'Weekly'}}
												], class: 'inp-switch'
											}
										},
										{
											label: {
												content: [
													{
														input: {
															type: 'checkbox',
															'data-value': 0,
															onclick: function () {
																this.parentNode.className = this.checked == this.getAttribute('data-value') == 1 ? 'inp-switch' : 'inp-switch updated';
															}
														}
													},
													{span: {content: 'Daily'}}
												], class: 'inp-switch'
											}
										}
									], class: 'inp-switch-col'
								}
							}
						]
					}
				});
				xlsxDataTable.a(row);
				if (!exclude)
					data.setActivated(1);
				if (employee[3] == 'Administrator')
					row.q('input[type="checkbox"]')[1].disabled = true;
				if (exclude)
					row.q('input[type="checkbox"]')[0].disabled = row.q('input[type="checkbox"]')[1].disabled = true;
				//
			}
			if (!hasBulkAdd){
				scrollToTop();
				hidePrevAlerts();
				showAlert('warning', 'No valid records found in the uploaded file.');
				submitButton.setAttribute('disabled', true);
			}
			else if (data.seats - data.activated >= 0)
				submitButton.removeAttribute('disabled');
			//
			tFootStsCntnr.a(bulkAddStsBar);
		};
		//	-----------------------------------------------------------------------------------------------
		//	Handle dropped file
		function fileDropped(file){
			eventLogger('start', 'empBulkAddUpload');
			cancelButton.onclick();
			//
			var reader = new FileReader();
			reader.onload = function(e){
				eventLogger('end', 'empBulkAddUpload');
				var fileData = e.target.result;
				var csv = parseFile(fileData);
				if (!csv)
					showAlert('error', 'Please upload a file of correct format.');
				else//{}
					validateAndLoadCSV(csv, file, fileData);
			}
			setTimeout(function(){
				reader.readAsDataURL(file);
			}, 1);
		}
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		eventLogger('end', 'empBulkAdd');
		//
		if (typeof loaded == 'function')
			loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};

// No need to wait till these load (no need arc.require). We need these only when the user is uploading a file.
document.head.appendChild( arc.elem('script', null, {src: script_path+'jszip.js'}) );
document.head.appendChild( arc.elem('script', null, {src: script_path+'xlsx.js'}) );

function parseFile(file){
	try{
		file = file.split('base64,')[1];//.replace(/\//g, '-')
		file = atob(file);
		//
		//	Parse the file container to extract data
		var cfb = XLSX.read(file, {type: 'binary'});
		var sheet1 = cfb.Sheets[Object.keys(cfb.Sheets)[0]];
		//
		//	Get sheet data into a matrix of cells
		var keys = Object.keys(sheet1);
		var sheet = {}, col = 0, row = 0;
		for (var i = 0; i < keys.length; i++){
			col = keys[i].charCodeAt(0) - 65;
			row = 1 * keys[i].substring(1);
			if (col > -1 && col < 10){
				if (typeof sheet[row] == 'undefined')
					sheet[row] = {};
				sheet[row][col] = sheet1[ keys[i] ].v;
			}
		}
		//
		//	Remove rows with only one column
		keys = Object.keys(sheet);
		for (var i = 0; i < keys.length; i++)
			if (Object.keys(sheet[ keys[i] ]).length == 1)
				delete sheet[ keys[i] ];
		//var wb = XLSX.parse_xlscfb(cfb);
		//console.log(wb);
		//var output = XLSX.utils.sheet_to_csv(wb);
		return sheet;
	}
	catch(e){
		//console.log(e);
		return false;
	}
}

function validateXLSXData(data, file, fileData, errorCallback){
	//	First row has to be faximile
	if (!data || JSON.stringify(data[1]) != '{"0":"Count","1":"Employee name","2":"Employee email","3":"User role","4":"Manager name","5":"Manager\'s Email Address"}'){
		showAlert('error', 'Please upload an XLSX file of correct format. Use the given template.');// The file you uploaded is corrupted.
		return false;
	}
	var emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var roles = ['Administrator', 'Manager', 'System', 'Terminated', 'Not activated', 'Business Development', 'Contractor', 'Customer Service', 'Finance & Acc', 'Human Resource', 'Inside Sales', 'IT', 'Marketing', 'Operations', 'Procurement', 'Project Management', 'Sales', 'Sales Eng', 'Sales Support', 'Support', 'Tech Support'];
	//
	var employees = {}, missingMgrs = {}, errors = [];
	var rows = Object.keys(data), row;
	for (var i = 2; i < rows.length; i++){
		row = data[ rows[i] ];
		//
		//	Validate email address
		if (typeof row[1] == 'undefined' || row[1] == '')
			errors.push([rows[i], 'name', 'NULL']);
		//
		//	Validate email address
		if (typeof row[2] == 'undefined' || row[2] == '' || ! emailRegEx.test(row[2]))
			errors.push([rows[i], 'email', typeof row[2] == 'undefined' ? 'NULL' : row[2]]);
		//
		//	Validate role is within enum
		if (typeof row[3] == 'undefined' || roles.indexOf(row[3]) == -1)
			errors.push([rows[i], 'role', typeof row[3] == 'undefined' ? 'NULL' : row[3]]);
		//
		// Index all employee roles by email
		if (typeof employees[ row[2] ] != 'undefined')
			errors.push([rows[i], 'duplicate', row[1]+' &lt;'+row[2]+'&gt;']);
		else
			employees[ row[2] ] = [ row[1]+' &lt;'+row[2]+'&gt;', row[0], row[3], rows[i] ];
	}
	//	Validate if managers within specified admins and managers
	for (var i = 2; i < rows.length; i++){
		row = data[ rows[i] ];
		if (typeof row[4] == 'undefined')
			row[4] = row[5];
		else
			row[4] += ' &lt;'+row[5]+'&gt;';
		//
		//	Manager unspecified
		if (typeof row[2] == 'undefined' || typeof row[5] == 'undefined' || row[5] == ''){
		}
		//
		//	Manager is self
		else if (row[5].toLowerCase() == row[2].toLowerCase()){
			errors.push([rows[i], 'smanager', row[4]]);
		}
		//
		//	Manager is not among given employees
		else if (typeof employees[ row[5] ] == 'undefined' && typeof module.extManagers[ row[5] ] == 'undefined'){
			errors.push([rows[i], 'manager', row[4]]);
		}
		//
		//	Manager is not specified as manager nor admin
		else if (typeof module.extManagers[ row[5] ] == 'undefined' && employees[ row[5] ][2] != 'Manager' && employees[ row[5] ][2] != 'Administrator'){
			errors.push([ rows[i], 'nmanager', employees[ row[5] ][0] ]);
		}
	}
	//
	//	There are errors found
	if (errors.length > 0){
		//console.log(errors);
		//	Display error messages
		for (var i = 0; i < errors.length; i++)
			if (errors[i][1] == 'name')
				errorCallback('error', 'Name at <b>B'+errors[i][0]+'</b> is empty.');
			else if (errors[i][1] == 'email')
				errorCallback('error', 'Email address <i>\''+errors[i][2]+'\'</i> at <b>C'+errors[i][0]+'</b> is invalid.');
			else if (errors[i][1] == 'role')
				errorCallback('error', 'User role <i>\''+errors[i][2]+'\'</i> at <b>D'+errors[i][0]+'</b> is invalid.');
			else if (errors[i][1] == 'duplicate')
				errorCallback('error', 'Employee <i>\''+errors[i][2]+'\'</i> at <b>C'+errors[i][0]+'</b> is a duplicate.');
			else if (errors[i][1] == 'manager')
				errorCallback('error', 'Manager <i>\''+errors[i][2]+'\'</i> at <b>F'+errors[i][0]+'</b> is not found.');
			else if (errors[i][1] == 'nmanager')
				errorCallback('error', 'Manager <i>\''+errors[i][2]+'\'</i> at <b>F'+errors[i][0]+'</b> is not defined as Manager.');
			else if (errors[i][1] == 'smanager')
				errorCallback('error', 'Manager <i>\''+errors[i][2]+'\'</i> at <b>F'+errors[i][0]+'</b> is themselves.');
		//
		return false;
	}
	//
	//	All validations passed
	else{
		return true;
	}
}
