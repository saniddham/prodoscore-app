
var moduleSettingsUser = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		hidePrevAlerts();
		eventLogger('start', 'loadSettingsUser');
		q('[data-tag="users"]')[0].className = 'active';
		new arc.ajax(base_url + 'update-employee/?filter=appusers', {
			callback: function (resp) {
				eventLogger('end', 'loadSettingsUser');
				data = eval('(' + resp.responseText + ')');
		/*doFetchData('user-accounts',
			function (data, self, startD, endD) {*/
				var employee = context.querySelector('form');
				var buttons = employee.querySelectorAll('button.primary, button.red, button.gray');
				//
				var id = params[0];
				if (typeof data.employees[id] == 'undefined') {
					employee.id.value = 'new';
					employee.fullname.value = '';
					employee.email.value = '';
					employee.email.removeAttribute('disabled');
					employee.role.value = '';
					employee.password.value = '';
					buttons[1].style.display = 'none';
					employee.role.options[2].style.display = 'none';
				}
				else{
					employee.id.value = id;
					employee.fullname.value = data.employees[id].fullname;
					employee.email.value = data.employees[id].email;
					employee.email.setAttribute('disabled', true);
					employee.role.value = data.employees[id].role;
					employee.password.value = '[ENCRYPTED]';
					buttons[1].style.display = 'inline-block';
					employee.role.options[2].style.display = '';
				}
				//
				employee.email.onkeydown = function(e){
					if (e.keyCode == 32){
						e.stopPropagation();
						return false;
					}
				};
				//
				//var prevAlert;//prevAlert =
				employee.onsubmit = buttons[0].onclick = function (type) {
					type = typeof type !== 'undefined' ? type : false;
					//employee.email.value = employee.email.value.replace(/ /g, '');
					/*try{
						hideAlert(prevAlert, 10);
					}catch(e){}*/
					//
					if (employee.fullname.value.trim() == '') {
						showAlert('error', 'Please enter the full name');
						employee.fullname.focus();
					}
					else if (employee.role.value.trim() == '') {
						showAlert('error', 'Please select user role');
						employee.role.focus();
					}
					else if (employee.email.value == '' || employee.email.value.indexOf(' ') > -1) { //	 || ! /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(employee.email.value) // employee.email.value.trim() == ''
						showAlert('error', 'The username must not include spaces');// / email address
						employee.email.focus();
					}
					else if (employee.password.value.trim() == '') {
						showAlert('error', 'Please enter a password');
						employee.password.focus();
					}
					else {
						eventLogger('start', 'saveUsrSettings');
						var upData = [{
							id: employee.id.value,
							fullname: employee.fullname.value,
							email: employee.email.value,
							role: employee.role.value,
							password: employee.password.value,
							appuser: 1
						}];
						new arc.ajax(base_url + 'update-employee/', {
							method: 'POST',
							data: upData,
							callback: function (response) {
								eventLogger('end', 'saveUsrSettings');
								response = eval('(' + response.responseText + ')');
								if (typeof response.errors != 'undefined') {
									scrollToTop();
									for (var email in response.errors)
										showAlert('error', 'This username is already in use');
								}
								else {
									try {
										/*if (typeof processedData.employees[response['data-ids'][0]] == 'undefined')
											processedData.employees[response['data-ids'][0]] = {};
										processedData.employees[response['data-ids'][0]].fullname = employee.fullname.value;
										processedData.employees[response['data-ids'][0]].email = employee.email.value;
										processedData.employees[response['data-ids'][0]].role = employee.role.value;
										upData[0].id = response['data-ids'][0];
										fillMgrsNTeamsLists(null, data, upData);*/
										if (employee.id.value == 'new'){
											document.location.hash = 'employee/' + response['data-ids'][0];
											showAlert('success', 'User account created');
										}
										else {
											document.location.hash = 'users';
											if (type === 'delete') {
												showAlert('success', 'User account deleted');
											}
											else {
												showAlert('success', 'User account saved');
											}
										}
									}
									catch (e) {
										showAlert('error', response.message);
									}
								}
							},
							fallback: function (response) {
								eventLogger('end', 'saveUsrSettings');
								showAlert('error', 'Error saving User account');
							}
						});
					}
					return false;
				}
				buttons[1].onclick = function () {
					if (!confirm('Are you sure you want to delete this user.?'))
						return false;
					employee.role.value = -1;
					employee.onsubmit('delete');
					return false;
				}
				buttons[2].onclick = function () {
						document.location.hash = 'users';
						return false;
					}
					//
				scrollToTop();
			}
		});
		context.q('form')[0].fullname.onkeyup = function(){
			var color = usernameIcon(this.value, false);
			this.style.borderLeft = '3px solid #' + color[1];
		}
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};
