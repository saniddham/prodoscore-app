
var moduleSettingsEmployee = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		hidePrevAlerts();
		if (prevXMLhttpRequest)
			prevXMLhttpRequest.abort();
		eventLogger('start', 'employeeSettings');
		//	var leaves = context.q('#leaves tbody')[0];
		//	leaves.innerHTML = '<tr><td><div class="loading"></div></td></tr>';
		employeeSettings.q('.nav-button.back')[0].onclick = function(){
			document.location.hash = '#employees';
		}
		q('[data-tag="employees"]')[0].className = 'active';
		//
		var employee;
		var updateUserForm = context.q('form')[0];
		var checkboxes = updateUserForm.q('input[type="checkbox"]');
		var blockUser = context.q('button.red.primary.block')[0];
		var unblockUser = context.q('button.green.primary.block')[0];
		new arc.ajax(base_url + 'update-employee/', {
			callback: function (data) {
				data = eval('(' + data.responseText + ')');
				empSettingsData = data;

				var self = organizationSettings.myself;//data.employees[uid];

				prevXMLhttpRequest = new arc.ajax(base_url + 'employee-settings/?id=' + params[0], {// + '&fromdate=' + DateFromShort(startD).sqlFormatted() + '&todate=' + DateFromShort(endD).sqlFormatted()
					callback: function (data) {
						eventLogger('end', 'employeeSettings');
						data = eval('(' + data.responseText + ')');
						employee = data.employee;
						var userRoleEnabled = true;
						//			leaves.innerHTML = '';
						//var employee = data.employees[params[0]];
						//
						var managrs = {};
						for (var i in data.managers)
							managrs[i] = data.managers[i];
						//
						var unIcon = usernameIcon(employee.fullname);
						var usericon = context.q('div.circle')[0];
						usericon.style.backgroundColor = '#' + unIcon[1];
						usericon.style.border = '2px solid #' + unIcon[1];
						if (employee.picture == '') {
							usericon.innerHTML = '<p>' + unIcon[0] + '</p>';
							usericon.style.backgroundImage = '';
						}
						else {
							usericon.innerHTML = '';
							usericon.style.backgroundImage = "url('" + employee.picture + "')";
						}
						var username = context.q('p.user-name')[0];
						username.innerHTML = employee.fullname;
						//
						if (typeof updateUserForm.userrole != 'undefined') {
							updateUserForm.userrole.innerHTML = '';
						}
						//updateUserForm.userrole.appendChild(arc.elem('option', 'Please Select', {value: 0}));

						if (self.role == 70) {
							userRoleEnabled = false;
							if (context.q('#userrole')[0]) {
								updateUserForm.userrole.parentNode.removeChild( updateUserForm.userrole );
							}
							for (var i in roles) {
								if (arrayIgnore.indexOf(i) == -1 && roles[i] != '') {
									if (i == employee.role) {
										updateUserForm.userrolestatic.value = roles[i];
									}
								}
							}
						}
						else {
							if (context.q('#userrolestatic')[0]) {
								updateUserForm.userrolestatic.parentNode.removeChild( updateUserForm.userrolestatic );
							}

							for (var emp in empSettingsData.employees) {
							    var empId  = parseInt(emp, 10)
                                if (empId == parseInt(params[0], 10) ){
                                    break;
                                }

							}
							if (empSettingsData.employees[empId].is_app_user) {
							    for (var i in roles) {
                                    if (arrayIgnore.indexOf(i) == -1 && roles[i] != '') {
                                        if (i == 70 || i == 80 || i==-1 || i==0) {
                                            var tmp = arc.elem('option', roles[i], {
                                            value: i
                                            });
                                            if   (i == employee.role)
                                                tmp.selected = true;
                                                updateUserForm.userrole.appendChild(tmp);
                                        }
                                    }
							    }
							} else {
                                for (var i in roles) {
                                    if (arrayIgnore.indexOf(i) == -1 && roles[i] != '') {
                                        var tmp = arc.elem('option', roles[i], {
                                            value: i
                                        });
                                        if (i == employee.role)
                                            tmp.selected = true;
                                        updateUserForm.userrole.appendChild(tmp);
                                    }
							    }

							}
							updateUserForm.userrole.disabled = params[0] == uid;
							sortList(updateUserForm.userrole, -1, false);
						}
						//
						if ( typeof updateUserForm.usermgr != 'undefined' ) {
							updateUserForm.usermgr.innerHTML = '';
						}
						if (self.role == 70) {
							if (context.q('#usermgr')[0]) {
								updateUserForm.usermgr.parentNode.removeChild( updateUserForm.usermgr );
							}
							for (var i in managrs) {
								if (arrayIgnore.indexOf(i) == -1) {
									if (i == employee.manager) {
										updateUserForm.usermgrstatic.value = managrs[i];
									}
								}
							}
						}
						else {
							if (context.q('#usermgrstatic')[0]) {
								updateUserForm.usermgrstatic.parentNode.removeChild( updateUserForm.usermgrstatic );
							}
							updateUserForm.usermgr.appendChild(arc.elem('option', '* Please Select', {
								value: 52
							}));
							for (var i in managrs) {
								if (arrayIgnore.indexOf(i) == -1) {
									var tmp = arc.elem('option', managrs[i], {
										value: i
									});
									if (i == employee.manager)
										tmp.selected = true;
									if (params[0] == i)
										tmp.disabled = true;
									updateUserForm.usermgr.appendChild(tmp);
								}
							}
							sortList(updateUserForm.usermgr, -1, false);
						}
						//
						updateUserForm.visibility.options[0].selected = employee.status == 1;
						updateUserForm.visibility.options[1].selected = employee.status < 1;

						// toggle visibility and disable manager according to role
						if ( employee.role <= 0 ) {
							updateUserForm.visibility.selectedIndex = 1;
							updateUserForm.visibility.setAttribute('disabled', true);

							updateUserForm.usermgr.selectedIndex = 0;
							updateUserForm.usermgr.setAttribute('disabled', true);
						}
						else{
							updateUserForm.visibility.removeAttribute('disabled');
							if (self.role == 80) {
								updateUserForm.usermgr.removeAttribute('disabled');
							}
						}
						//
						//updateUserForm.report_email.value = ((employee.report_email != null && employee.report_email != '' || employee.email.indexOf('@') == -1)) ? employee.report_email : employee.email; //updateUserForm.report_email.placeholder = employee.email;
						updateUserForm.report_email.value = employee.report_email;
						/*if (employee.email.indexOf('@') > -1 && employee.is_app_user == 1 && (employee.report_email == null || employee.report_email == ''))
							updateUserForm.report_email.value = employee.email; //updateUserForm.report_email.placeholder = employee.email;*/
						//
						var chk = ('00' + employee.report_enabled).split('').reverse();
						checkboxes[0].checked = chk[0] == '1';
						checkboxes[1].checked = chk[1] == '1';
						//
						// disable daily reporting for admins
						if (employee.role == 80) {
							checkboxes[1].setAttribute("disabled", true);
							checkboxes[1].checked = false;
						}
						else {
							checkboxes[1].removeAttribute("disabled");
							checkboxes[1].checked = chk[1] == '1';
						}
						if (userRoleEnabled) {
							updateUserForm.userrole.onchange = function(e){
								if (employee.role > 69 && updateUserForm.userrole.value < 70){
									if (!confirm('Are you sure?\nChanging this employee\'s user role will reset the manager name to "none" for any employee who currently reports to this employee.')){
										e.cancelBubble = true;
										e.stopPropagation();
										e.preventDefault();
										updateUserForm.userrole.value = employee.role;
										return false;
									}
									else
										employee.role = updateUserForm.userrole.value;
								}
								if (updateUserForm.userrole.value == 80) {
									checkboxes[1].setAttribute("disabled", true);
									checkboxes[1].checked = false;
								}
								else {
									checkboxes[1].removeAttribute("disabled");
								}

								// toggle visibility and disable manager according to role
								if ( updateUserForm.userrole.value <= 0 ) {
									updateUserForm.visibility.selectedIndex = 1;
									updateUserForm.visibility.setAttribute('disabled', true);
									updateUserForm.usermgr.selectedIndex = 0;
									updateUserForm.usermgr.setAttribute('disabled', true);
								} else {
									updateUserForm.visibility.removeAttribute('disabled');
									updateUserForm.usermgr.removeAttribute('disabled');
								}

							};
						}

						breadCrumbViewModel();
						breadCrumbViewModel.reset();
						//breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
						breadCrumbViewModel.addCrumb('#employees', 'Employee settings', false);
						breadCrumbViewModel.addCrumb(location.hash, employee.fullname, true);
						breadCrumbViewModel.render(context);
						//
						//*/	//	REMOVEING THESE TWO BEAUTIFUL AND PERFECTLY WORKING COUPLE OF BUTTONS BECAUSE SOMEONE DISLIKE THESE
						blockUser.style.display = employee.status == 1 ? 'block' : 'none';
						unblockUser.style.display = employee.status < 1 ? 'block' : 'none';
						blockUser.onclick = function(){
							setUserStatus(-1);
						}
						unblockUser.onclick = function(){
							setUserStatus(1);
						}

						// hide/show hide/unhide button on visibility dropdown change
						updateUserForm.visibility.onchange = function (){
							if (this.value == 1) {
								unblockUser.style.display = 'none';
								blockUser.style.display = 'block';
							} else {
								blockUser.style.display = 'none';
								unblockUser.style.display = 'block';
							}
						};
						//*/
						//
						//			for (var i in data.leaves)
						//				leaves.appendChild(arc.elem('tr', '<td>'+data.leaves[i].date+'</td><td>'+data.leaves[i].description+'</td>', {'data-id': 'date-'+data.leaves[i].date}));
						//			sortColumn(leaves.parentNode, 0);
						//
					}
				});
			}
		});
		//
		//var saveUser = updateUserForm.q('button.primary.button')[0];//.right
		updateUserForm.onsubmit = function () {
			if (updateUserForm.report_email.value != '' && !/\S+@\S+/.test(updateUserForm.report_email.value)) {  //	/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
				showAlert('error', 'Please enter a valid email address');
				updateUserForm.report_email.focus();
			}
			else{
				var data = {
					id: params[0],
					status: updateUserForm.visibility.value,
					reportEmail: updateUserForm.report_email.value,// == '' || updateUserForm.report_email.value == employee.email ? null : updateUserForm.report_email.value,
					reportFrequency: '10' + (checkboxes[1].checked ? '1' : '0') + (checkboxes[0].checked ? '1' : '0')
				};
				if (typeof updateUserForm.userrole != 'undefined')
					data.role = updateUserForm.userrole.value;
				if (typeof updateUserForm.usermgr != 'undefined')
					data.manager = updateUserForm.usermgr.value;
				//
				eventLogger('start', 'saveEmpSettings');
				new arc.ajax(base_url + 'update-employee/', {
					method: 'POST',
					data: [data],
					callback: function(response){
						eventLogger('end', 'saveEmpSettings');
						response = JSON.parse(response.responseText);
						if (typeof response.errors != 'undefined'){
							showAlert('error', 'You do not have sufficient licenses to activate this user.');
						}
						else{
							showAlert('success', 'Employee settings saved');
							cacheKey = '';
							/*response.employees[id].role = updateUserForm.userrole.value;
							response.employees[id].manager = updateUserForm.usermgr.value;*/
							/*if (typeof processedData.employees != 'undefined') {
								processedData.employees[params[0]].role = updateUserForm.userrole.value;
								processedData.employees[params[0]].manager = updateUserForm.usermgr.value;
								processedData.employees[params[0]].status = updateUserForm.visibility.value;
								processedData.employees[params[0]].report_email = updateUserForm.report_email.value;
								processedData.employees[params[0]].report_enabled = '10' + (checkboxes[1].checked ? '1' : '0') + (checkboxes[0].checked ? '1' : '0');
							}
							fillMgrsNTeamsLists(null, empSettingsData, [data]);*/
							//document.location = document.location;	//	This would suck
							//
							organizationSettings.myself.status = updateUserForm.visibility.value;
							setMyProfileLink(organizationSettings.myself);
						}
					},
					fallback: function (data) {
						showAlert('error', 'Failed to save employee settings');
						eventLogger('end', 'saveEmpSettings');
					}
				});
			}
			return false;
		}
		//
		//*/	//	REMOVEING THESE TWO BEAUTIFUL AND PERFECTLY WORKING COUPLE OF BUTTONS BECAUSE SOMEONE DISLIKE THESE
		var setUserStatus = function (status) {
			var data = [{
				id: params[0],
				status: status
			}];
			eventLogger('start', 'blockEmpSettings');
			new arc.ajax(base_url + 'update-employee/', {
				method: 'POST',
				data: data,
				callback: function (response) {
					eventLogger('end', 'saveEmpSettings');
					//showAlert('success', 'Employee '+(status == 1 ? 'un':'')+'blocked');
					showAlert('success', 'Employee is ' + (status == 1 ? 'made visible' : 'hidden'));
					//var user = eval('('+localStorage['user-'+params[0]]+')');
					//user[5] = status;
					//localStorage['user-'+params[0]] = JSON.stringify(user);
					//response.employees[params[0]].status = status;
					/*if (typeof processedData.employees != 'undefined')
						processedData.employees[params[0]].status = status;
					fillMgrsNTeamsLists(null, empSettingsData, data);*/
					//
					blockUser.style.display = status == 1 ? 'block' : 'none';
					unblockUser.style.display = status < 1 ? 'block' : 'none';

					// set visibility dropdown value
					for(var i=0; i < updateUserForm.visibility.options.length; i++) {
						if(updateUserForm.visibility.options[i].value == status) {
							updateUserForm.visibility.selectedIndex = i;
						}
					}
					organizationSettings.myself.status = status;
					setMyProfileLink(organizationSettings.myself);
				},
				fallback: function (data) {
					showAlert('error', 'Failed to ' + (status == 1 ? 'un' : '') + 'block employee');
					eventLogger('end', 'blockEmpSettings');
				}
			});
		}
		//*/
		//
		var regLeavesForm = context.q('form')[1];
		//var submitButton = regLeavesForm.q('button.primary')[0];
		regLeavesForm.onsubmit = regLeavesForm.onsubmit = function () {
			var opt = regLeavesForm.q('button.btngroup');
			var data = {
				employee: params[0],
				description: regLeavesForm.message.value
			};
			if (opt[0].className.indexOf('active') > -1)
				data['date'] = regLeavesForm.from_date.value;
			else {
				data['from_date'] = regLeavesForm.from_date.value;
				data['to_date'] = regLeavesForm.to_date.value;
			}
			new arc.ajax(base_url + 'register-leaves', {
				method: 'POST',
				data: data,
				callback: function (data) {
					data = eval('(' + data.responseText + ')');
					for (var i = 0; i < data.length; i++) {
						var checkExt = leaves.q('[data-id="date-' + data[i].date + '"]');
						if (checkExt.length == 0)
							leaves.appendChild(arc.elem('tr', '<td>' + data[i].date + '</td><td>' + data[i].description + '</td>', {
								'data-id': 'date-' + data[i].date
							}));
						else
							checkExt[0].q('td')[1].innerHTML = data[i].description;
						sortColumn(leaves.parentNode, 0);
					}
				}
			});
			return false;
		}
		var optionButtons = regLeavesForm.q('button.btngroup.btn');
		var endDateRow = regLeavesForm.q('div.wrap.end-date')[0];
		//new calendar(regLeavesForm.from_date);
		//new calendar(regLeavesForm.to_date);
		new textareaHandler(regLeavesForm.message);
		for (var i = 0; i < optionButtons.length; i++)
			optionButtons[i].onclick = function (i) {
				return function () {
					for (var j = 0; j < optionButtons.length; j++)
						optionButtons[j].removeClass('active');
					optionButtons[i].addClass('active');
					endDateRow.style.display = i == 0 ? 'none' : 'block';
					return false;
				}
			}(i);
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};
