
var startD, endD;
var processedData = [], dateBins = [];
var noRecordsRow = [];
var mgrIndex = {};
var organizationSettings, earliestDate, latestDate;
var activeFrame, scrollTarget, scrollDest = 0;

var page = 'dashboard';
var loadEmployeeId = -1, employees4Mgr = -1;

var navigationAnchors = q('ul.main-navigation li a');
var pageContent = q('.page-content')[0];
var app = q('#app')[0];
//var yesterdayDetails = false, yesterdayDetailsDate = false;
var myIcon;

// ----------------------------------------------------------------------------------------

function initializeProdoScore(){
	if (typeof calendar != 'function' && typeof usernameIcon != 'function'){
		setTimeout(initializeProdoScore, 512);
		return false;
	}
	new calendar(document.forms[0].from_date);
	new calendar(document.forms[0].to_date);
	//
	/*yesterdayDetailsDate = document.forms[0].to_date.value;
	new arc.ajax(base_url+'ajax-yesterday-details/?date='+yesterdayDetailsDate, {
		callback: function(data){
			yesterdayDetails = JSON.parse(data.responseText);
		}
	});*/
	//
	new arc.ajax(base_url+'organization-settings/', {
		callback: function(data){
			var dat = eval('('+data.responseText+')');
			dat['workingdays'] = eval('('+dat['workingdays']+')');
			organizationSettings = dat;
			roles = organizationSettings.roles;
			//
			var userName = q('.user-name-wrap p.user-name')[0];
			var picture = userName.getAttribute('data-picture');
			myIcon = usernameIcon(userName.innerHTML);
			//
			setMyProfileLink(organizationSettings.myself);
			userName.parentNode.insertBefore(arc.elem('div', (picture == '' ? '<p>'+myIcon[0]+'</p>' : ''),
				{class: 'circle blue', style: 'background-color:#'+myIcon[1]+'; border: 2px solid #'+myIcon[1]+'; background-image: url(\''+picture+'\');'}), userName);
			//
			earliestDate = new Date(dat.since+'  12:00');
			earliestDate.setDate(earliestDate.getDate() - 93);
			latestDate = new Date(dat.upto+'  12:00');
			//
			initializeNavigation();
			date_filter_onchange('last-7-days');
			//
			// Initialize Notifications
			notifications = new Notifications();
			notifications.renderNotification();
			//
			if (document.location.hash == '')
				document.location.hash = '#dashboard';
			window.onpopstate();
			//
			app.style.opacity = 1;
		},
		/*onprogress: function(progress){
			progressiveSubLoaded = progress;
			displayProgress();
		},*/
		fallback: function(xmlhttp){
			if (xmlhttp.status == 500)
				alert('There was a server error while loading your organization settings. We will be looking into this.');
			else
				alert('Couldn\'t connect to server. Please check your internet connection and retry.');
		}
	});
	//
	new function(){
		if (typeof initializeFeedbackForm == 'function')
			initializeFeedbackForm();
		else
			setTimeout(arguments.callee, 500);
	}();

	window.onscroll = function(){
		scrollTarget = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
		pageContent.style.top = '-'+scrollTarget+'px';
	}
	scrollDest = 0;
	scroll();
}

var scrollingTable, fixAtBottom;
function scroll(){
	activeFrame = q('.page-content .content.active, .page-content [xclass="content"].active');
	if (activeFrame.length > 0){
		activeFrame = activeFrame[activeFrame.length-1];
		app.style.height = (activeFrame.offsetHeight + 160)+'px';
		scrollingTable = activeFrame.q('table.sortable');
		fixAtBottom = activeFrame.q('div.fix-at-bottom');
		if (scrollingTable.length > 0 && fixAtBottom.length > 0)
			if (window.innerHeight - scrollingTable[0].getBoundingClientRect().top > 100)
				fixAtBottom[0].style.marginBottom = '3px';
			else
				fixAtBottom[0].style.marginBottom = '-40px';
	}
	//
	pageContent.style.top = '-'+scrollTarget+'px';
	setTimeout(scroll, 250);
	//
	if (scrollTarget > 100 && messages.innerHTML != ''){
		var remove = messages.q('.message');
		for (var i = 0; i < remove.length; i++)
			remove[i].onclick();
	}
}

// ----------------------------------------------------------------------------------------

function setMyProfileLink(myself){
	var userName = q('a.user-name-wrap')[0];
	var myProfile = q('ul.main-navigation.lower li[data-tag="myprofile"] a')[0];
	if (myself.status == 1){
		userName.setAttribute('href', '#employee/'+myself.id+'/'+myself.email);
		userName.setAttribute('title', myself.fullname);
		if (typeof myProfile != 'undefined'){
			myProfile.setAttribute('href', '#employee/'+myself.id+'/'+myself.email);
			myProfile.removeAttribute('title');
		}
	}
	else{
		userName.removeAttribute('href');
		userName.setAttribute('title', 'Your visibility is set to "Hidden", you can change this on the Employee settings page.');
		if (typeof myProfile != 'undefined'){
			myProfile.removeAttribute('href');
			myProfile.setAttribute('title', 'Your visibility is set to "Hidden", you can change this on the Employee settings page.');
		}
	}
	if (typeof myProfile != 'undefined'){
		myProfile.q('.icon.icon-profile')[0].innerHTML = (typeof myself.picture == 'undefined' || myself.picture == '' ? '' : '<img src="'+myself.picture+'" style="border:1px solid #'+myIcon[1]+'" />');
	}
}

// ----------------------------------------------------------------------------------------

var contentMain = q('#contentMain')[0];
function initializeNavigation(){

	new arc.nav('dashboard', contentMain,
		arc.require(script_path+'module.dashboard.js', template_path+'dashboard.html')
	);

	new arc.nav('dashboard/strata', q('#strataCount')[0],
		arc.require(script_path+'module.dashboard.strata.js', template_path+'dashboard.strata.html')
	);

	new arc.nav('managers', q('#contentManagers')[0],
		arc.require(script_path+'module.managers.js', template_path+'managers.html')
	);

	new arc.nav('employees', q('#contentEmployees')[0],
		arc.require(script_path+'module.employees.js', template_path+'employees.html')
	);

	new arc.nav('employee', q('#contentOneEmployee')[0],
		arc.require(script_path+'module.employee.js', template_path+'employee.html')
		/*function(context, params){
			loadEmployeeData(context, params);
			navigationAnchors[navigationAnchors.length-2].parentNode.addClass('active');
		}*/
	);

	let contentCorrelations = q('#contentCorrelations');
	if (contentCorrelations.length > 0)
		new arc.nav('correlations', contentCorrelations[0],
			arc.require(script_path+'module.correlations.js', template_path+'correlations.html')
			/*function(context, params){
				loadCorrelations(context, params);
			},
			function(context, params){
				clearTimeout(connectionRetryCr);
			}*/
		);

	/*new arc.nav('correlations/topic', document.getElementById('correlationDetailsSubFrame'),
		function(context, params){
			loadCorrelationDetails(context, 'topic', params);
		}
	);

	new arc.nav('correlations/cod', document.getElementById('correlationDetailsSubFrame'),
		function(context, params){
			loadCorrelationDetails(context, 'cod', params);
		}
	);

	new arc.nav('rawdata', document.getElementById('raw-data'),
		function(context, params){
			loadRawData(context, params);
		}
	);

	new arc.nav('alerts', arc.elem('span'),
		function(context, params){
			setTimeout("document.location.hash = 'dashboard';", 400);
		}
	);*/

}

// =====================================================================================

/*/function dateFilters(obj){//, inv
	obj.style.transition = 'all 0.5s';
	//inv.style.transition = 'all 0.5s';
	var hideTimeout, preventDisplay = false;
	this.hide = function(){
		preventDisplay = true;
		hideTimeout = setTimeout(function(){
			obj.style.opacity = 0;
			obj.style.width = '0px';
			obj.style.pointerEvents = 'none';
			preventDisplay = false;
			//
			//inv.style.display = 'block';
		}, 50);
	}
	this.show = function(){
		if (preventDisplay)
			return false;
		obj.style.opacity = 1;
		obj.style.width = 'auto';
		obj.style.pointerEvents = 'auto';
		//
		//inv.style.display = 'none';
	}
}
var dateFilters = new dateFilters(q('div.header-left')[0]);//, q('div.header-left')[1]*/

/*var backToEmployeeSettings = employeeSettings.q('.nav-button.back')[0];
backToEmployeeSettings.onclick = function(){
	history.back();
}*/

var sidebar = q('div.sidebar')[0];
var sidebarNav = sidebar.q('nav.sidebar-nav')[0];
q('.header .fa.fa-bars')[0].onclick = function(){
	if (sidebar.className.indexOf('active') == -1){
		sidebar.addClass('active');
		sidebar.focus();
	}
}
q('div.page-content')[0].onclick = function(){
	sidebar.removeClass('active');
}
var sidebarBottomHeader = q('div.sidebar .bottom h2')[0];
if (typeof sidebarBottomHeader != 'undefined'){
	sidebarBottomHeader.onclick = function(){
		if (this.q('i.fa')[0].className == 'fa fa-caret-down'){
			this.parentNode.addClass('collapsed');
			this.q('i.fa')[0].className = 'fa fa-caret-up';
			sidebarNav.addClass('bottom-collapsed');
			localStorage['legend-collapsed'] = 'true';
		}
		else{
			this.parentNode.removeClass('collapsed');
			this.q('i.fa')[0].className = 'fa fa-caret-down';
			sidebarNav.removeClass('bottom-collapsed');
			localStorage['legend-collapsed'] = 'false';
		}
	}
	if (localStorage['legend-collapsed'] == 'true')
		sidebarBottomHeader.onclick();
}


// ==============================================================================================
// ==============================================================================================
//													FILTER AND SEARCH ITEMS
// ==============================================================================================

var dateFilter = document.getElementById('date-filter');
var notifications;

dateFilter.onchange =
	function(){
		date_filter_onchange(this.value);
		//document.forms[0].onsubmit();
		if (organizationSettings.domain == organizationSettings.myself.email.split('@')[1])
			notifications.renderNotification();
		if (this.value != 'custom')
			window.onpopstate({forcePop: true});
	};
document.forms[0].from_date.onchange =
	function(){
		dateFilter.value = 'custom';
		//document.forms[0].onsubmit();
		/*if (document.forms[0].to_date.value == document.forms[0].from_date.value)
			document.location = '#dashboard/'+document.forms[0].from_date.value;
		else*/
		if (organizationSettings.domain == organizationSettings.myself.email.split('@')[1])
			notifications.renderNotification();
		window.onpopstate({forcePop: true});
	};
document.forms[0].to_date.onchange =
	function(){
		dateFilter.value = 'custom';
		//document.forms[0].onsubmit();
		/*if (document.forms[0].to_date.value == document.forms[0].from_date.value)
			document.location = '#dashboard/'+document.forms[0].to_date.value;
		else*/
		if (organizationSettings.domain == organizationSettings.myself.email.split('@')[1])
			notifications.renderNotification();
		window.onpopstate({forcePop: true});
	};

//var searchEmployee, searchRole, filterByManager, retrieveTerminated, showNewEmployees;
/*function bindFilters(){
	searchEmployee = q('input[name="searchEmployee"]');
	searchRole = q('select[name="searchRole"]');
	filterByManager = q('select[name="searchManager"]');
	retrieveTerminated = q('input[name="retrieveTerminated"]')[0];
	showNewEmployees =  q('input[name="showNewEmployees"]')[0];
	//
	for (var i = 0; i < 2; i++){
		//if (arrayIgnore.indexOf(i) == -1) {
		new function(i) {
			// Populate Role Select
			searchRole[i].a(arc.elem('option', '[ Filter by ]', {value: '*'}));
			for (var j in roles) {
				if (j > 0 && arrayIgnore.indexOf(j) == -1 && roles[j] != '') {
					searchRole[i].a(arc.elem('option', roles[j], {value: j}));
				}
			}
			sortList(searchRole[i], 0);
		}(i);
		//}
	}
	// -----------------------------------------------------------------------------
	searchEmployee[0].onkeyup = function() {

		var employees = contentEmployees.querySelectorAll('tr.tablerow[data-id]');
		var found = false;
		var d_style = (employees[0].parentNode.parentNode.classList.contains('table--fixed-body') ? 'block' : 'table-row');

		for (var j = 0; j < employees.length; j++) {
			if ( ( this.value == '' || employees[j].querySelectorAll('td')[0].innerHTML.toLowerCase().indexOf(this.value.toLowerCase()) > -1 )
			&& (searchRole[0].value == '*' || employees[j].getAttribute('data-role') == searchRole[0].value)
			&& (i != 1 || retrieveTerminated.checked || employees[j].getAttribute('data-role') > 0) ){
				employees[j].style.display = d_style;
				found = true;

			}
			else {
				employees[j].style.display = 'none';
			}
		}
		noRecordsRow[0].style.display = found ? 'none' : d_style;
	}
	searchRole[0].onchange = function(e){
		searchEmployee[0].value = '';
		filterByManager[0].value = '*';
		if (this.value == '*')
			document.location.hash = 'employees';
		else
			document.location.hash = 'employees/role/' + this.value + '/' + this.options[this.selectedIndex].innerHTML.toLowerCase();
	}
	filterByManager[0].onchange = function(e){
		searchEmployee[0].value = '';
		searchRole[0].value = '*';
		if (this.value == '*'){
			employees4Mgr = -1;
			document.location.hash = 'employees';
		}
		else {
			document.location.hash = 'employees/under/' + this.value + '/' + (typeof processedData.employees[this.value] !== 'undefined' ? processedData.employees[this.value].email : processedData.self.email);
		}
	}
}*/

// ==============================================================================================
// ==============================================================================================
//													LOAD RAW DATA
// ==============================================================================================

function loadRawData(context, params){
	var rawdata = context.q('table.rawdata tbody')[0];
	rawdata.innerHTML = '';
	doFetchData('dashboard',
		function(data, self, startD, endD){
			context.style.height = '88vh';
			var th = context.q('table.rawdata thead tr');
			th[0].innerHTML = '<th class="vl" width="160">Employee</th>';
			th[1].innerHTML = '<th class="vl" width="160">Date</th>';
			var prodCount, lastSubCol;
			for (var id in data.employees)
				if (data.employees[id].status > 0 && data.employees[id].role > 0 && typeof data.employees[id].scr != 'undefined' && data.employees[id].scr){
					prodCount = 0;
					for (code in products){
						lastSubCol = arc.elem('th', code);
						th[1].a(lastSubCol);
						prodCount += 1;
					}
					th[1].a(arc.elem('th', 'Score', {class: 'vl'}));
					th[0].a(arc.elem('th', '<b>'+data.employees[id].fullname+'</b><br/>'+
												roles[data.employees[id].role]+'<br/>'+
												'<span>OWS: '+data.employees[id].scr.g.toFixed(2)+
												'<br/>RBS: '+data.employees[id].scr.r.toFixed(2)+
												'<br/>LRS: '+data.employees[id].scr.l.toFixed(2)+'</span>',
										{colspan: prodCount+1, class: 'vl'}));
				}
			rawdata.innerHTML = '';
			var newRow;
			for (date in data.days){
				newRow = arc.elem('tr', '<td class="vl" width="160">'+DateFromShort(date).sqlFormatted()+'</td>');
				for (var id in data.employees)
					if (data.employees[id].status > 0 && data.employees[id].role > 0 && typeof data.employees[id].scr != 'undefined' && data.employees[id].scr)
						if (data.employees[id].days[date] == null){
							for (code in products)
								newRow.a(arc.elem('td', '-'));
							//
							newRow.a(arc.elem('td', '-', {class: 'vl'}));
						}
						else{
							for (code in products)
								newRow.a(arc.elem('td', '&nbsp;'+(typeof data.employees[id].days[date].prod[code] != 'undefined' ? data.employees[id].days[date].prod[code] : '')));
							//
							newRow.a(arc.elem('td', '<span>OWS:&nbsp;'+data.employees[id].days[date].scr.g.toFixed(2)+'<br/>RBS:&nbsp;'+data.employees[id].days[date].scr.r.toFixed(2)+'<br/>LRS:&nbsp;'+data.employees[id].days[date].scr.l.toFixed(2)+'</span>', {class: 'vl'}));
						}
				rawdata.a(newRow);
			}
			//	Allow downloading to Excel file
			new contextMenu(context.q('table')[0], {
					'download-csv': {
						icon: 'fa-table',
						title: 'Download CSV file',
						onclick: function(rawdata){
							downloadCSV(rawdata, 'Raw Data from '+document.forms[0].from_date.value+' to '+document.forms[0].to_date.value+'.csv');
						}
					},
					'download-xlsx': {
						icon: 'fa-file-excel-o',
						title: 'Download Excel file',
						onclick: function(rawdata){
							downloadExcel(rawdata, 'Raw Data from '+document.forms[0].from_date.value+' to '+document.forms[0].to_date.value+'.xlsx');
						}
					}
				});

			context.onscroll = function(e){
				var rowHeads = q('.rawdata tr td:first-child, .rawdata tr th:first-child');//
				for (var i = 0; i < rowHeads.length; i++)
					rowHeads[i].style.left = this.scrollLeft+'px';
			}
		}
	);
}

initializeProdoScore();
