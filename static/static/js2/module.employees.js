
var moduleEmployees = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		//	Breadcrumbs
		breadCrumbViewModel();
		breadCrumbViewModel.reset();
		breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
		breadCrumbViewModel.addCrumb('#employees', 'Employees', true);
		breadCrumbViewModel.render(context);
		//
		//	Initialize
		let employees = context.q('.employeedata')[0].tBodies[0];
		sorttable.makeSortable(employees.parentNode);
		let userProfileBtn = context.q('button.user-profile-btn')[0];
		let filterByManager = context.q('select[name="searchManager"]')[0];
		let searchRole = context.q('select[name="searchRole"]')[0];
		let searchEmployee = context.q('input[name="searchEmployee"]')[0];
		let employeesChart = context.q('#employeesChart')[0];
		let empHeader = context.q('.one-row.emp-header')[0];
		//let userSettingsBtn = context.q('button.user-settings-btn')[0];
		let employeesUnder = [];
		let employeesShortlist = [], mgrIndex = {}, employeesCumilScore = 0;
		let noRecordsRow;
		employees.innerHTML = '';
		empHeader.style.display = 'none';
		employeesChart.parentNode.parentNode.parentNode.style.display = 'none';
		//
		//	Correlations Widget
		context.q('.corrWidget-col')[0].style.display = 'none';
		if (organizationSettings.correlations_enabled === 1) {
			let correlationsWidget = new CorrelationsWidget();
			correlationsWidget.init(context.q('div.corrWidget')[0]);
		}
		employees.parentNode.classList.remove('employeedata--no-roles');
		employees.parentNode.parentNode.classList.add('col-12');
		employees.parentNode.parentNode.classList.remove('col-8');
		//
		let continueLoading = function(data, self, startD, endD){
			//moduleEmployees.data = data;
			filterByManager.innerHTML = '<option value="*">[ Filter by ]</option>';
			searchRole.value = '*';
			employees.innerHTML = '';
			employeesUnder = [];
			//	-----------------------------------------------------------------------------------------------
			//	Fill Employees List
			//	-----------------------------------------------------------------------------------------------
			for (let i in data.employees){
				if (data.employees[i].role > 0 && data.employees[i].status == 1 && typeof data.employees[i].scr != 'undefined' && data.employees[i].scr &&
						(typeof params[1] == 'undefined' || ( params[0] == 'under' && data.employees[i].manager == params[1] || params[0] == 'role' && data.employees[i].role == params[1] ) )){
					let icon = usernameIcon(data.employees[i].fullname);
					let className = strataStr(data.employees[i].strata);
					let mgrName = data.employees[data.employees[i].manager] == undefined ? 
						(organizationSettings.myself.id == data.employees[i].manager ?
							organizationSettings.myself.fullname :
							'-')
						:
						data.employees[data.employees[i].manager].fullname;
					//
					var employee = arc.reactor(
						{tr: {class: 'tablerow', 'data-id': i, 'data-role': data.employees[i].role,
							content: [
								{td: {content: {
									div: {class: 'user-name-wrap',
										content: {p: {class: 'user-name', content: data.employees[i].fullname}}
									}}, style: 'border-left:2px solid #'+icon[1]
								}},
								{td: {content: roles[data.employees[i].role]}},
								{td: {content: mgrName}},
								{td: {class: 'score '+className[0], content: data.employees[i].scr.l.toFixed(0)}},//, title: 'OWS: '+data.employees[i].scr.g.toFixed(2)+'\nRBS: '+data.employees[i].scr.r.toFixed(2)+'\nLRS: '+data.employees[i].scr.l.toFixed(2)
								{td: {class: 'text '+(!data.employees[i].scr.delta || data.employees[i].scr.delta == 0 ? 'same' : (data.employees[i].scr.delta > 0 ? 'increase' : 'decrease')),
									content: !data.employees[i].scr.delta || data.employees[i].scr.delta == 0 ? '-' : Math.abs(data.employees[i].scr.delta.toFixed(0))+'%', 'sorttable_customkey': data.employees[i].scr.delta}}
							],
							onclick: function(){
								document.location.hash = 'employee/'+this.getAttribute('data-id')+'/'+data.employees[this.getAttribute('data-id')].email;
								return false;
							}
						}}
					);
					employeesUnder.push(i);
					employees.a(employee);
					employeesShortlist[i] = [data.employees[i].fullname, icon[1]];
					employeesCumilScore += data.employees[i].scr.l;
				}
				//
				if (data.employees[i].role > 0 && data.employees[i].status == 1 &&
				    ((typeof data.employees[data.employees[i].manager] !== 'undefined' && data.employees[data.employees[i].manager].role > 0 && data.employees[data.employees[i].manager].status == 1)
				    || (typeof data.self !== 'undefined' && data.employees[i].manager==data.self.id))){
					mgrIndex[data.employees[i].manager] = true;
				}
			}
			sortColumn(employees.parentNode, 0);
			noRecordsRow = arc.elem('tr', '<td colspan="5"><i>No records found</i></td>', {class: 'no-records', style: 'display:none;'});
			employees.a(noRecordsRow);
			//	-----------------------------------------------------------------------------------------------
			//	Fill Filter Drop-Downs and Select to user-selected option
			//	-----------------------------------------------------------------------------------------------
			searchRole.appendChild(arc.elem('option', '[ Filter by ]', {value: '*'}));
			for (var j in roles)
				//if (j > 0 && arrayIgnore.indexOf(j) == -1 && roles[j] != '') {
				if (isset(RoleList[j])){
					searchRole.appendChild(arc.elem('option', roles[j], {value: j}));
				}
			sortList(searchRole, 0);
			//	-----------------------------------------------------------------------------------------------
			for (let i in mgrIndex)
				if (arrayIgnore.indexOf(i) == -1 && (typeof data.employees[i]!== 'undefined' || parseInt(i)===data.self.id)){
					filterByManager.a(arc.elem('option', typeof data.employees[i]!== 'undefined' ? data.employees[i].fullname : data.self.fullname, {value: i}));
				}
			sortList(filterByManager, 0);
			//	-----------------------------------------------------------------------------------------------
			searchRole.value = '*';
			filterByManager.value = '*';
			if (params.length > 1){
				if (params[0] == 'under')
					filterByManager.value = params[1];
				else if (params[0] == 'role')
					searchRole.value = params[1];
			}
			//	-----------------------------------------------------------------------------------------------
			//	Apply Filters
			//	-----------------------------------------------------------------------------------------------
			if (typeof params[1] != 'undefined'){
				//	-----------------------------------------------------------------------------------------------
				//	Draw chat background only
				//	-----------------------------------------------------------------------------------------------
				var chartDataBlank = [['Date']], chartIndex = [];
				var colors = [];
				for (var id in employeesShortlist)
					if (arrayIgnore.indexOf(id) == -1){
						chartDataBlank[0].push(employeesShortlist[id][0]);
						colors.push('#'+employeesShortlist[id][1]);
						chartIndex[id] = chartDataBlank[0].length-1;
					}
				for (var i  = startD; i < endD+1; i++){
					var date = DateFromShort(i);
					if (isWorkingDay(date.getDay())){
						date.setHours(0); date.setMinutes(0);
						var row = [date];
						for (var id in employeesShortlist)
							if (arrayIgnore.indexOf(id) == -1)
								row.push(0);
						chartDataBlank.push(row);
					}
					else
						chartDataBlank.push([date]);
				}
				new ArcChart(document.getElementById('employeesChart'), chartDataBlank, colors,
					{
						chartArea:{left:40, right:20, bottom:60, top:10},
						fontFamily: 'Linotte-Regular',
						additionalData: {chartIndex: chartIndex},
						//
						verticalRanges: verticalRanges,
						maxy: 100
					}).draw('column');
				//
				//	-----------------------------------------------------------------------------------------------
				//	Show employees under a selected Manager
				//	-----------------------------------------------------------------------------------------------
				if (params[0] == 'under'){
					empHeader.style.display = 'flex';
					fillEmployeeDetails(context, data, params[1], (employeesCumilScore / Object.keys(employeesShortlist).length));
					//
					/*userSettingsBtn.onclick = function(){
						document.location = 'settings/#employee/'+params[1];
					}*/
					userProfileBtn.onclick = function(){
						document.location = '/#employee/'+params[1];
					}
					let emp_name = (typeof data.employees[params[1]] !== 'undefined' ? data.employees[params[1]].fullname : data.self.fullname);
					employeesChart.parentNode.q('h2')[0].innerHTML = 'Employees under '+ emp_name;
					//
					breadCrumbViewModel();
					breadCrumbViewModel.reset();
					breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
					breadCrumbViewModel.addCrumb('#managers', 'Managers', false);
					breadCrumbViewModel.addCrumb(location.hash, emp_name, true);
					breadCrumbViewModel.render(context);
				}
				//	-----------------------------------------------------------------------------------------------
				//	Compare employees in the selected Team (Role)
				//	-----------------------------------------------------------------------------------------------
				else{
					employeesChart.parentNode.q('h2')[0].innerHTML = 'Employees in '+roles[params[1]]+' Team';
					breadCrumbViewModel();
					breadCrumbViewModel.reset();
					breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
					breadCrumbViewModel.addCrumb('#employees', 'Employees', false);
					breadCrumbViewModel.addCrumb(location.hash, roles[params[1]]+' Team', true);
					breadCrumbViewModel.render(context);
					//
					//	MOVE THIS BLOCK OF CODE BELOW THE ELSE BLOCK FOR REFACTORING AND EXTENDING TO MANAGER FILTER AS WELL
					if (organizationSettings.correlations_enabled === 1){
						employees.parentNode.classList.add('employeedata--no-roles');
						employees.parentNode.parentNode.classList.remove('col-12');
						employees.parentNode.parentNode.classList.add('col-8');
						//
						context.q('.corrWidget-col')[0].style.display = 'block';
						let correlationsWidget = new CorrelationsWidget();
						correlationsWidget.init(context.q('div.corrWidget')[0]);
						correlationsWidget.loadData(document.forms[0].from_date.value, document.forms[0].to_date.value, employeesUnder);
					}
				}
				//
				/*/	EXPERIMENTAL CODE - UNTESTED [ENABLE NLP FOR UNDER-MANAGER]
				if (organizationSettings.correlations_enabled === 1){
					employees.parentNode.classList.add('employeedata--no-roles');
					employees.parentNode.parentNode.classList.remove('col-12');
					employees.parentNode.parentNode.classList.add('col-8');
					//
					context.q('.corrWidget-col')[0].style.display = 'block';
					let correlationsWidget = new CorrelationsWidget();
					correlationsWidget.init(context.q('div.corrWidget')[0]);
					correlationsWidget.loadData(document.forms[0].from_date.value, document.forms[0].to_date.value, employeesUnder);
				}*/
				//	-----------------------------------------------------------------------------------------------
				//	Draw employee comparison chart after fetching required data
				//	-----------------------------------------------------------------------------------------------
				//	Draw chat background only
				var chartData = [['Date']], chartIndex = [];
				var colors = [];
				for (var id in employeesShortlist)
					if (arrayIgnore.indexOf(id) == -1){
						chartData[0].push(employeesShortlist[id][0]);
						colors.push('#'+employeesShortlist[id][1]);
						chartIndex[id] = chartData[0].length-1;
					}
				for (var i  = startD; i < endD+1; i++){
					var date = DateFromShort(i);
					if (isWorkingDay(date.getDay())){
						date.setHours(0); date.setMinutes(0);
						var row = [date];
						for (var id in employeesShortlist)
							if (arrayIgnore.indexOf(id) == -1)
								row.push(0);
						chartData.push(row);
					}
					else
						chartData.push([date]);
				}
				new ArcChart(document.getElementById('employeesChart'), chartData, colors,
					{
						chartArea:{left:40, right:20, bottom:60, top:10},
						fontFamily: 'Linotte-Regular',
						additionalData: {chartIndex: chartIndex},
						//
						verticalRanges: verticalRanges,
						maxy: 100
					}).draw('column');
				fillMissingEmployeeData(employeesShortlist, data, startD, endD,
					function(employeesShortlist, data){
						compareEmployeesChart(employeesShortlist, data, startD, endD);
					});
				//
				employeesChart.parentNode.parentNode.parentNode.style.display = 'block';
			}
			//	-----------------------------------------------------------------------------------------------
			//	Only one manager is present -? SHOULD WE KEEP THIS BLOCK. SEE IF THIS APPLIES ACTUALLY..
			//	-----------------------------------------------------------------------------------------------
			else if (Object.keys(mgrIndex).length == 1 && params[1] != self.manager){
				//	Draw employee comparison chart after fetching required data
				fillMissingEmployeeData(employeesShortlist, data, startD, endD,
					function(employeesShortlist, data){
						compareEmployeesChart(employeesShortlist, data, startD, endD);
					});
				//
				employeesChart.parentNode.parentNode.parentNode.style.display = 'block';
				filterByManager.value = Object.keys(mgrIndex)[0];
			}
			//	-----------------------------------------------------------------------------------------------
			//	If you came to add comparison between teams, we shouldn't.
			//	-----------------------------------------------------------------------------------------------
		};
		let normalLoading = function(){
			doFetchData('employees',
				function(data, self, startD, endD){
					continueLoading(data, self, startD, endD);
				}
			);
		};
		//
		//	ENTRY POINT
		if (document.forms[0].filter.value == 'custom')
			normalLoading();
		else
			doFetchEmployeeList(document.forms[0].filter.value, document.forms[0].to_date.value,
				function(data, startD,endD ){
					processedData.employees = data;
					continueLoading({'employees': data, 'self': organizationSettings.myself}, organizationSettings.myself, startD, endD);
				}
			);
			/*new arc.ajax(base_url+'dashboard-employee-list-ajax/?period='+document.forms[0].filter.value.replace(/-/g, '_')+'&date='+document.forms[0].to_date.value, {
				callback: function(resp){
					let data = JSON.parse(resp.responseText);
					let startD = DateToShort(document.forms[0].from_date.value);
					let endD = DateToShort(document.forms[0].to_date.value);
					processedData.employees = data;
					continueLoading({'employees': data, 'self': organizationSettings.myself}, organizationSettings.myself, startD, endD);
				}
			});*/
		//	-----------------------------------------------------------------------------------------------
		//	Search Employee by Name
		//	-----------------------------------------------------------------------------------------------
		searchEmployee.value = '';
		let searchEmployeeTimeout = false;
		searchEmployee.onkeyup = function(){
			clearTimeout(searchEmployeeTimeout);
			searchEmployeeTimeout = setTimeout(function(){
				var employees = context.querySelectorAll('tr.tablerow[data-id]');
				var found = false;
				var d_style = (employees[0].parentNode.parentNode.classList.contains('table--fixed-body') ? 'block' : 'table-row');
				for (var j = 0; j < employees.length; j++) {
					if ( ( searchEmployee.value == '' || employees[j].querySelectorAll('td')[0].innerText.toLowerCase().indexOf(searchEmployee.value.toLowerCase()) > -1 )
					&& (searchRole.value == '*' || employees[j].getAttribute('data-role') == searchRole.value)
					&& (employees[j].getAttribute('data-role') > 0) ){
						employees[j].style.display = d_style;
						found = true;
					}
					else {
						employees[j].style.display = 'none';
					}
				}
				noRecordsRow.style.display = found ? 'none' : d_style;
			}, 512);
		}
		//	-----------------------------------------------------------------------------------------------
		//	Filter Employees by Role
		//	-----------------------------------------------------------------------------------------------
		let searchRoleTimeout = false;
		searchRole.onchange = function(e){
			clearTimeout(searchRoleTimeout);
			searchRoleTimeout = setTimeout(function(){
				searchEmployee.value = '';
				filterByManager.value = '*';
				if (searchRole.value == '*')
					document.location.hash = 'employees';
				else
					document.location.hash = 'employees/role/' + searchRole.value + '/' + searchRole.options[searchRole.selectedIndex].innerHTML.toLowerCase();
			}, 512);
		}
		//	-----------------------------------------------------------------------------------------------
		//	Filter Employees by Manager
		//	-----------------------------------------------------------------------------------------------
		let filterByManagerTimeout = false;
		filterByManager.onchange = function(e){
			clearTimeout(filterByManagerTimeout);
			filterByManagerTimeout = setTimeout(function(){
				searchEmployee.value = '';
				searchRole.value = '*';
				if (filterByManager.value == '*'){
					//employees4Mgr = -1;
					document.location.hash = 'employees';
				}
				else {
					document.location.hash = 'employees/under/' + filterByManager.value + '/' + (typeof processedData.employees[filterByManager.value] !== 'undefined' ? processedData.employees[filterByManager.value].email : processedData.self.email);
				}
			}, 512);
		}
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		loaded();
		//	-----------------------------------------------------------------------------------------------
		//	Allow Downloading as an Excel or CSV File
		//	-----------------------------------------------------------------------------------------------
		new contextMenu(employees.parentNode, {
			'download-csv': {
				icon: 'fa-table',
				title: 'Download CSV file',
				onclick: function(rawdata){
					let bkp = rawdata.q('tr.no-records');
					if (bkp.length > 0){
						bkp = [bkp[0].parentNode, bkp[0]];
						bkp[0].removeChild(bkp[1]);
					}
					else
						bkp = false;
					//
					downloadCSV(rawdata, 'Employees-prodoscore-from-'+document.forms[0].from_date.value+'-to-'+document.forms[0].to_date.value+'.csv');
					//
					if (bkp)
						bkp[0].a(bkp[1]);
				}
			},
			'download-xlsx': {
				icon: 'fa-file-excel-o',
				title: 'Download Excel file',
				onclick: function(employees){
					let bkp = employees.q('tr.no-records');
					if (bkp.length > 0){
						bkp = [bkp[0].parentNode, bkp[0]];
						bkp[0].removeChild(bkp[1]);
					}
					else
						bkp = false;
					//
					downloadExcel(employees, 'Employees-prodoscore-from-'+document.forms[0].from_date.value+'-to-'+document.forms[0].to_date.value+'.xlsx');
					//
					if (bkp)
						bkp[0].a(bkp[1]);
				}
			}
		});
		//	-----------------------------------------------------------------------------------------------
	}
};

function compareEmployeesChart(employeesShortlist, data, startD, endD){
	var chartData = [['Date']], chartIndex = [];
	var colors = [];
	for (var id in employeesShortlist)
		if (arrayIgnore.indexOf(id) == -1){
			chartData[0].push(employeesShortlist[id][0]);
			colors.push('#'+employeesShortlist[id][1]);
			chartIndex[id] = chartData[0].length-1;
		}
	for (var i  = startD; i < endD+1; i++){
		var date = DateFromShort(i);//new Date((16800+(1*i))*86400000+timezoneOffset);
		if (isWorkingDay(date.getDay())){
			date.setHours(0);
			date.setMinutes(0);
			var row = [date];
			for (var id in employeesShortlist)
				if (arrayIgnore.indexOf(id) == -1)
					row.push(typeof data.employees[id].days[i] == 'undefined' ? null : data.employees[id].days[i].scr.l.toFixed(0));
			chartData.push(row);
		}
		else
			chartData.push([date]);
	}
	//setTimeout(function(){
	drawChartEmployees4Manager(document.getElementById('employeesChart'), chartData, colors,
		function(row, col, options){
			col = options.additionalData.chartIndex.indexOf(col);
			document.location.hash = 'employee/'+col+'/'+processedData.employees[col].email;
		},
		{chartIndex: chartIndex}
		/*function(chart){
			bindChartHover(chart, chartIndex, contentEmployees);
		}*/);
	//}, 250);
}

