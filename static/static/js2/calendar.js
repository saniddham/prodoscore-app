
var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var wdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

function calendar(input){
	var _self = this;
	var hoverLatch = false;
	//
	this.calBody = arc.elem('div', '<div><button class="button">&#x25C0;</button><select></select><select></select><button class="button">&#x25b6;</button></div><table><thead><tr>'+
		'</tr></thead><tbody></tbody></table>', {class: 'calendar'});
	var timezoneOffset = 0;
	new (function(tr){
			if (typeof organizationSettings == 'undefined' || typeof organizationSettings.workingdays == 'undefined'){
				var callee = arguments.callee;
				setTimeout(function(){
					callee(tr);
				}, 520);
			}
			else{
				for (var i = 0; i < 7; i++)
					tr.appendChild(arc.elem('th', wdays[i], organizationSettings.workingdays.indexOf(i) == -1 ? {class: 'non-working'} : {}));
				timezoneOffset = new Date();
				timezoneOffset = timezoneOffset.getTimezoneOffset() + organizationSettings.tzoffset;
			}
		})(this.calBody.q('thead tr')[0]);
	this.calBody.onmouseover = function(){
		hoverLatch = true;
	};
	this.calBody.onmouseout = function(){
		hoverLatch = false;
	};
	input.parentNode.insertBefore(this.calBody, input);
	var date = new Date(input.value);
	this.inputFocusLatch = false;
	//
	var button = this.calBody.querySelectorAll('button');
	this.prevMonth = button[0];
	this.nextMonth = button[1];
	var select = this.calBody.querySelectorAll('select');
	this.selYear = select[0];
	this.selMonth = select[1];
	this.calTable = this.calBody.querySelector('table tbody');
	//
	for (var i = 0; i < months.length; i++){
		tmp = arc.elem('option', months[i], {value: ('0'+(i+1)).substring((i+1).toString().length-1)});
		if (date.getMonth() == i)
			tmp.selected = true;
		this.selMonth.appendChild(tmp);
	}
	//
	var today = new Date();
	for (var i = -3; i < 1; i++){
		tmp = arc.elem('option', today.getFullYear()+i, {value: today.getFullYear()+i});
		if (i == 0)
			tmp.selected = true;
		this.selYear.appendChild(tmp);
	}
	//
	this.fillMonth = function(year, month){
		date = new Date(year, month-1, 1);
		date.setMinutes(1);
		//
		var inpDate = new Date(input.value);			//	Date in the date input box
		inpDate.setMinutes(inpDate.getTimezoneOffset()+60);
		//
		if (inpDate.getFullYear() == year && inpDate.getMonth() == month-1)
			inpDate = inpDate.getDate();
		else
			inpDate = false;
		_self.calTable.innerHTML = '';
		var row = _self.calTable.insertRow();
		for (var i = 0; i < date.getDay(); i++)
			row.appendChild(arc.elem('td', '', (organizationSettings.workingdays.indexOf(i) == -1 ? {class: 'non-working'} : {})));
		var inSettings = document.location.href.indexOf('/settings') > -1;//document.location.hash.startsWith('#settings/');
		while (date.getMonth() == month-1){
			tmp = arc.elem('td', date.getDate(), {class: inpDate==date.getDate() ? 'selected' : ''});
			row.appendChild(tmp);
			tmp.onclick = function(){
				input.focus();
			}
			if (organizationSettings.workingdays.indexOf(date.getDay()) == -1)
				tmp.addClass('non-working');
			else if (earliestDate > date || latestDate < date)
				tmp.addClass('future');
			else if (latestDate > date || inSettings)
				tmp.onclick = function(){
					input.value = _self.selYear.value+'-'+_self.selMonth.value+'-'+(('0'+this.innerHTML).substring(this.innerHTML.length-1));
					input.onchange();
					//
					_self.calBody.style.display = 'none';
				}
			else
				tmp.addClass('future');
			date.setDate(date.getDate()+1);
			if (date.getDay() == 0)
				row = _self.calTable.insertRow();
		}
		if (date.getDay() > 0)
			for (var i = date.getDay(); i < 7; i++)
				row.appendChild(arc.elem('td', ''));
		date.setMonth(date.getMonth()-1);
		_self.inputFocusLatch = true;
		input.focus();
		_self.inputFocusLatch = false;
	}
	//
	input.onfocus = function(){
		_self.calBody.style.display = 'block';
		if (_self.inputFocusLatch)
			return;
		//
		var tmp = input.value.split('-');
		_self.selYear.value = tmp[0];
		_self.selMonth.value = tmp[1];
		_self.fillMonth(tmp[0], tmp[1]);
	};
	input.onblur = function(){
		if (!hoverLatch)
			_self.calBody.style.display = 'none';
	};
	//
	this.selYear.onchange = this.selMonth.onchange = function(){
		_self.fillMonth(_self.selYear.value, _self.selMonth.value);
	}
	this.prevMonth.onclick = function(){
		date.setMonth(date.getMonth()-1);
		_self.selYear.value = date.getFullYear();
		tmp = 1*date.getMonth() + 1;
		_self.selMonth.value = ('0'+tmp).substring(tmp.toString().length-1);
		_self.fillMonth(_self.selYear.value, _self.selMonth.value);
	};
	this.nextMonth.onclick = function(){
		date.setMonth(date.getMonth()+1);
		_self.selYear.value = date.getFullYear();
		tmp = 1*date.getMonth() + 1;
		_self.selMonth.value = ('0'+tmp).substring(tmp.toString().length-1);
		_self.fillMonth(_self.selYear.value, _self.selMonth.value);
	};
	//
	this.selYear.onblur = this.selMonth.onblur = function(){
		if (!hoverLatch)
			_self.calBody.style.display = 'none';
		else{
			input.focus();
		}
	}
	//
	input.readOnly = true;
	this.calBody.style.display = 'none';
}
