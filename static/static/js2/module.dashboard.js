
var correlationDataCache = false;

module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		//	Permanant Link to a Specific Period - Unused, Undocumented, Untested feature
		if (params.length > 0){
			document.forms[0].from_date.value = params[0];
			document.forms[0].to_date.value = params.length > 1 ? params[1] : params[0];
			document.getElementById('date-filter').value = 'custom';
		}
		else
			context.style.opacity = 1;
		eventLogger('start', 'dashboard');
		//
		//	Initialize
		var employees = context.q('ul.employee-list.subs')[0];
		employees.innerHTML = '';
		//
		let continueLoading = function(data, self, startD, endD, histogram_details){
			//	-----------------------------------------------------------------------------------------------
			//	Organization Prodoscore - Top Line
			//	-----------------------------------------------------------------------------------------------
			let mainStatus = context.q('#org-score, #org-delta');
			mainStatus[0].innerHTML = data.organization.score.toFixed(0);
			mainStatus[0].parentNode.className = strataStr(data.organization.score_strata)[0];
			//
			if (!data.organization.score_delta){
				mainStatus[1].parentNode.className = 'no-change';
				mainStatus[1].parentNode.innerHTML = '<span id="org-delta">-</span>';
			}
			else if (data.organization.score_delta < 0){
				mainStatus[1].parentNode.className = 'decrease';
				mainStatus[1].parentNode.innerHTML = '<span id="org-delta">'+Math.abs(data.organization.score_delta.toFixed(0))+'</span>% Performance decrease';
			}
			else{
				mainStatus[1].parentNode.className = 'increase';
				mainStatus[1].parentNode.innerHTML = '<span id="org-delta">'+Math.abs(data.organization.score_delta.toFixed(0))+'</span>% Performance increase';
			}
			//	-----------------------------------------------------------------------------------------------
			//	Three Strata Info Boxes
			//	-----------------------------------------------------------------------------------------------
			let meta = ['below', 'baseline', 'above'], detailBox, tmp;
			for (let i = 0; i < 3; i++){
				detailBox = context.q('div.detail-box.'+meta[i])[0];
				tmp = detailBox.q('p.status')[0];
				if (data.organization.strata_delta[i] === false || data.organization.strata_delta[i] == 0){
					tmp.innerHTML = '0%<span><br/> No Change</span>';
					tmp.className = 'status same';
				}
				else{
					tmp.innerHTML = Math.abs(data.organization.strata_delta[i])+'%'+(data.organization.strata_delta[i] > 0 ? '<span><br/> Increase</span>' : '<span><br/> Decrease</span>');
					tmp.className = data.organization.strata_delta[i] > 0 ? 'status increase' : 'status decrease';
				}
				if (typeof data.organization.strata[i] == 'undefined')
					detailBox.q('p.user_number')[0].innerHTML = '...<span><br/>Employees</span>';
				else
					detailBox.q('p.user_number')[0].innerHTML = data.organization.strata[i]+'<span><br>Employee'+(data.organization.strata[i] == 1 ? '' : 's')+'</span>';
			}
			//	-----------------------------------------------------------------------------------------------
			//	Fill My Employees List
			//	-----------------------------------------------------------------------------------------------
			employees.innerHTML = '';
			//data.employees.sort(function(a, b){return a[1] - b[1];});
			for (let i in data.employees){
				if (typeof data.employees[i].scr != 'undefined' && data.employees[i].scr){//data.employees[i].role > 0 && data.employees[i].status == 1 && 
					let icon = usernameIcon(data.employees[i].fullname);
					let className = strataStr(data.employees[i].strata);
					//if ( (self.role == 80 && data.employees[i].role == 70 ) || data.employees[i].manager == uid ){
					//
					/*let hasSubordinates = false;
					for (let j in data.employees)
						if (data.employees[j].role > 0 && data.employees[j].status == 1 && data.employees[j].manager == i){
							hasSubordinates = true;
							break;
						}*/
					//
					let employee = arc.reactor(
						{li: {
							'data-sort-key': (102 - data.employees[i].scr.l.toFixed(0)),
							content: {a: {'data-id': i,// 'data-role': data.employees[i].role,
								href: (data.employees[i].hasSubordinates && data.employees[i].role > 69 ? 'employees/under/' : 'employee/')+i+'/'+data.employees[i].email,//'#dashboard/'+i+'/'+data.employees[i].email,
								style: 'border-left:2px solid #'+icon[1],
								content: [
									{
										div: {class: 'user-name-wrap',
											content: {p: {class: 'user-name', content: data.employees[i].fullname}}
										}
									},
									{
										div: {class: 'right-holder',
											content: [
												{p: {class: 'text '+className[0], content: className[1]}},
												{p: {class: 'score '+className[0], content: data.employees[i].scr.l.toFixed(0)}}

											]
										}
									}
								],
								onclick: function(){
									/*let hasSubordinates = false;
									for (let i in data.employees)
										if (data.employees[i].role > 0 && data.employees[i].status == 1 && data.employees[i].manager == this.getAttribute('data-id')){
											hasSubordinates = true;
											break;
										}*/
									if (data.employees[this.getAttribute('data-id')].hasSubordinates && data.employees[this.getAttribute('data-id')].role > 69)
										document.location.hash = 'employees/under/'+this.getAttribute('data-id')+'/'+data.employees[this.getAttribute('data-id')].email;
									else
										document.location.hash = 'employee/'+this.getAttribute('data-id')+'/'+data.employees[this.getAttribute('data-id')].email;
									return false;
								}
							}}
						}}
					);
					//*/
					employees.a(employee);
					//}
				}
			}
			sortList(employees);
			if (employees.childNodes.length > 8)
				for (let i = employees.childNodes.length - 1; i > 7; i--)
					employees.removeChild(employees.childNodes[i]);
			//	-----------------------------------------------------------------------------------------------
			//	Prepare Charts Data
			//	-----------------------------------------------------------------------------------------------
			var graphData = [['Bin', 'Employees']], bin, empCount = 0, histogramColors;//, 'list'
			let binSize = 10;
			histogramColors = [];
			//
			for (let i = 0; i < 100/binSize; i++){
				graphData.push([ (i*binSize + ' - ' + (i+1)*binSize), 0, [] ]);
				histogramColors.push((i+0.5)*binSize < 30 ? '#f1433c' : ((i+0.5)*binSize < 70 ? '#494949' : '#1e86d9'));
			}
			if (histogram_details)
				for (let i in histogram_details)
					graphData[(1*i)+1][1] += histogram_details[ i ];
			else
				for (let i in data.employees)
					if (typeof data.employees[i].scr != 'undefined' && data.employees[i].scr){//data.employees[i].role > 0 && 
						bin = Math.floor(data.employees[i].scr.l / binSize);
						//
						if (bin+1 >= graphData.length)
							bin = graphData.length-2;
						graphData[bin+1][1] += 1;
						//graphData[bin+1][2].push(data.employees[i].fullname);
					}
			//	-----------------------------------------------------------------------------------------------
			var graphData2 = [['Date', prevPeriodTxt(), 'Score']];
			//let days = Object.keys(data.days);
			//
			for (let j  = startD; j < endD+1; j++){
				let date = DateFromShort(j);
				//
				if (typeof data.days[j] == 'undefined')
					graphData2.push([date]);
				else if ( isWorkingDay(date.getDay()) )
					graphData2.push([date, data.days[j].score_prev ? Math.round(data.days[j].score_prev) : null, Math.round(data.days[j].score)]);
				else
					graphData2.push([date]);
			}
			//	-----------------------------------------------------------------------------------------------
			//	Draw Charts
			//	-----------------------------------------------------------------------------------------------
			setTimeout(function(){
				//	Organization Prodoscore Chart
				drawChartDashboardProdoScore(context.q('#chart_div_org')[0], graphData2, data.prev_offset,
					function(date, col){
						date = new Date(date[0]);
						if (col == 1) {
							//	Previous Period
							date.setDate(date.getDate() - data.prev_offset);
						}
						//
						document.forms[0].to_date.value = date.sqlFormatted();
						document.forms[0].from_date.value = date.sqlFormatted();
						document.getElementById('date-filter').value = 'custom';
						window.onpopstate({forcePop: true});
					});
				//
				//	Histogram
				drawChartDashboardHistogram(context.q('#chart_histogram')[0], graphData, histogramColors);
			}, 50);
			//	-----------------------------------------------------------------------------------------------
			//	Initialize Correlations Widget
			//	-----------------------------------------------------------------------------------------------
			if (organizationSettings.correlations_enabled == 1){
				let correlationsWidget = new CorrelationsWidget();
				correlationsWidget.init(context.q('div.corrWidget')[0]);
				if (correlationDataCache != false)
					correlationsWidget.loadData(document.forms[0].from_date.value, document.forms[0].to_date.value, '', correlationDataCache);
				else
					correlationsWidget.loadData(document.forms[0].from_date.value, document.forms[0].to_date.value);
			}
			else{
				context.q('div.corrWidget')[0].parentNode.style.display = 'none';
				context.q('div.corrWidget')[0].parentNode.nextSibling.style.display = 'none';
			}
			//	-----------------------------------------------------------------------------------------------
		};
		// ==================================================================
		// ==================================================================
		// ==================================================================
		let altLoadTimeout = false;
		let normalLoading = function(){
			doFetchData('dashboard',
				function(data, self, startD, endD){
					for (let i = 0; i < 3; i++)
						if (typeof data.organization.strata[i] == 'object')
							data.organization.strata[i] = data.organization.strata[i].length;
					//
					let keys = Object.keys(data.employees);
					for (let i = keys.length-1; i > -1; i--)
						if ( ( data.employees[ keys[i] ].role < 1 || data.employees[ keys[i] ].status != 1 ) &&  keys[i]  != uid )
							delete data.employees[ keys[i] ];
					//
					/*keys = Object.keys(data.employees);
					for (let i = keys.length-1; i > -1; i--)
						if ( (self.role == 80 && data.employees[ keys[i] ].role == 70 ) || data.employees[ keys[i] ].manager == uid ||  keys[i]  == uid ){}
						else
							delete data.employees[ keys[i] ];*/
					//
					keys = Object.keys(data.employees);
					for (let i = keys.length-1; i > -1; i--){
						data.employees[ keys[i] ].hasSubordinates = false;
						for (let j in data.employees)
							if (data.employees[j].role > 0 && data.employees[j].status == 1 && data.employees[j].manager ==  keys[i] ){
								data.employees[ keys[i] ].hasSubordinates = true;
								break;
							}
					}
					//
					continueLoading(data, self, startD, endD);
					eventLogger('end', 'dashboard');
				}
			);
		};
		//
		//	ENTRY POINT
		if (document.forms[0].filter.value == 'custom'){
			correlationDataCache = false;
			normalLoading();
		}
		else{
			new arc.ajax(base_url+'dashboard-load-details-ajax/?period='+document.forms[0].filter.value.replace(/-/g, '_')+'&date='+document.forms[0].to_date.value, {
				callback: function(resp){
					clearTimeout(altLoadTimeout);
					//
					let pData = JSON.parse(resp.responseText);
					startD = DateToShort(pData.details.fromdate);//DateToShort(document.forms[0].from_date.value);
					endD = DateToShort(pData.details.todate);//DateToShort(document.forms[0].to_date.value);
					//
					document.forms[0].from_date.value = pData.details.fromdate;
					document.forms[0].to_date.value = pData.details.todate;
					//
					// cacheKey = startD+':'+endD; cacheData = pData.ajax_dashboard;
					//
					if (typeof pData.ajax_correlations == 'string')
						correlationDataCache = JSON.parse(pData.ajax_correlations);
					else if (typeof pData.ajax_correlations == 'object')
						correlationDataCache = pData.ajax_correlations;
					//
					continueLoading(pData.ajax_dashboard, pData.ajax_dashboard.self, startD, endD,
						pData.ajax_dashboard.organization.histogram_details);
					eventLogger('end', 'dashboard');
					//
					//	Cache for other pages
					setTimeout(function(){
						doFetchData('dashboard', function(data, self, startD, endD){});
						new arc.ajax(base_url+'correlations-ajax/?fromdate='+document.forms[0].from_date.value+'&todate='+document.forms[0].to_date.value, {callback: function(data){}});
					}, 1);
				}
			});
			altLoadTimeout = setTimeout(normalLoading, 1200);
		}
		// ==================================================================
		// ==================================================================
		// ==================================================================
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		loaded();
		//	-----------------------------------------------------------------------------------------------
	}
	//	-----------------------------------------------------------------------------------------------
};

function drawChartDashboardProdoScore(context, chartData, offset, selCallback, colors, pointSize){
	context.innerHTML = loadingIndicator;
	if (typeof colors == 'undefined')
		colors = ['hsla(206, 60%, 80%, 0.75)', '#3366cc'];//
	if (typeof pointSize == 'undefined')
		pointSize = 6;
	var chart = new ArcChart(context, chartData, colors,
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnclick: selCallback,
			verticalRanges: verticalRanges,
			prevOffset: [offset, 0],
			points: pointSize,
			curve: 0
		});
		var res = chart.draw('line');//chartData.length == 2 ? 'column' : x
}

function drawChartDashboardHistogram(context, chartData, colors, columnHover){
	context.innerHTML = loadingIndicator;
	new ArcChart(context, chartData, colors,
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			//columnOnHover: columnHover, //tooltipPosition: 'to-right',
			tooltipPosition: 'above',
			setSpacing: 'histogram',
		}).draw('histogram');
}
