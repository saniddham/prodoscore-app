
var moduleSettingsProductPhoneSystem = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		eventLogger('start', 'confCallSettings');
		var formContainer = new scrollOpenClose(context.q('div.one-row'));

		var selectedPS = 'ringcentral';
		var psSelect = context.q('#phoneSystemSelect')[0];

		var form = context.q('form')[0];
		var rsForm = context.q('#RingCentralSettings')[0];
		var vbcForm = context.q('#vbcSettings')[0];

		var formFilled = new checkFilled(form);
		formFilled.disableBtn();

		var rc_auth_window = null,
			rc_auth_window_init = false,
			rc_auth_btn = rsForm.q('#rc_auth')[0];

		var vbc_auth_window = null,
			vbc_auth_window_init = false,
			vbc_auth_btn = vbcForm.q('#vbc_auth')[0];

		var rc_timer;
		var vbc_timer;

		var bridges = {},
			dropdowns = [];
		var employeedata = context.q('table.employeedata tbody')[0];
		sorttable.makeSortable(employeedata.parentNode);

		var credTable = context.q('table.phone-systems-table tbody')[0],
				credsData = context.q('table.phone-systems-table tbody tr'),
				addNewRowBtn = context.q('button.addNewRowBtn')[0],
				newRowSchema = {
					tr: {
						class: 'new-row',
						content: [
							{
								td: {
									content: [
										{
											input: {
												type: 'hidden',
												name: 'id[]',
												value: 'new'
											}
										},
										{
											input: {
												class: 'td-input',
												type: "text",
												name: 'xsi[]',
												placeholder: "Phone number",
												"data-required": true
											}
										}

									]

								}
							},
							{
								td: {
									content: {
										input: {
											class: 'td-input',
											type: "text",
											name: 'userid[]',
											placeholder: "Admin username",
											"data-required": true
										}
									}
								}
							},
							{
								td: {
									content: {
										input: {
											class: 'td-input',
											type: "password",
											name: 'password[]',
											placeholder: "Password",
											"data-required": true
										}
									}
								}
							},
							{
								td: {
									content: [
										{
											button: {
												type: 'button',
												class: 'ac-link edit',
												content: 'edit'
											}
										},
										{
											button: {
												type: 'button',
												class: 'ac-link done',
												content: 'done'
											}
										},
										{
											button: {
												type: 'button',
												class: 'ac-link delete',
												content: 'delete'
											}
										},
										{
											i: {
												class: 'fa fa-exclamation',
												title: 'Invalid credentials',
												"aria-hidden": true
											}
										}
									]
								}
							}
						]
					}
				};

		function hideShowSystem() {
			if ( selectedPS == 'broadsoft' ) {
				form.style.display = 'block';
				rsForm.style.display = 'none';
				vbcForm.style.display = 'none';
			}
			else if ( selectedPS == 'ringcentral' ) {
				form.style.display = 'none';
				rsForm.style.display = 'block';
				vbcForm.style.display = 'none';
			}
			else if ( selectedPS == 'vbc' ) {
				form.style.display = 'none';
				rsForm.style.display = 'none';
				vbcForm.style.display = 'block';
			}
			else{
				form.style.display = 'none';
				rsForm.style.display = 'none';
				vbcForm.style.display = 'none';
			}
		}
		hideShowSystem();
		psSelect.onchange = function (e) {
			selectedPS = psSelect.value;
			hideShowSystem();
		}

		//	=========================================
		//	BROADSOFT - ADD/EDIT/DELETE ADMIN
		//	=========================================
		addNewRowBtn.onclick = function (){
			var newRow = arc.reactor(newRowSchema);
			newRow.q('button.edit')[0].style.display = 'none';

			newRow.q('button.edit')[0].onclick = function () {
				editRow(newRow, this);
			};

			newRow.q('button.delete')[0].onclick = function () {
				deleteRow(newRow);
			};

			newRow.q('button.done')[0].onclick = function () {
				commitRow(newRow, this);
			};

			credTable.appendChild(newRow);

			formFilled.updateFields();
		};

		//	--------------------------------------------------------------------------------------------------
		//		(!-- There 3 functions are better moved to lib.js --)
		//	--------------------------------------------------------------------------------------------------
		//	--------------------------------------------------------------------------------------------------
		function commitRow (row, el) {
			var inputs = row.q('input'),
				actions = row.q('button.ac-link');

			if (inputs[1].value.trim() == '') {
				showAlert('error', 'Please enter the Admin Phone');//inputs[1].getAttribute('placeholder')
				inputs[1].focus();
				return false;
			}
			else if (inputs[2].value.trim() == '') {
				showAlert('error', 'Please enter the Admin Email/Username');//inputs[1].getAttribute('placeholder')
				inputs[2].focus();
				return false;
			}
			else if (inputs[3].value == 'new' && passwords[i].value == '[ENCRYPTED]') {
				showAlert('error', 'Please enter the password');//inputs[1].getAttribute('placeholder')
				inputs[3].focus();
				return false;
			}

			for ( var i = 0; i < inputs.length; i++ ) {
				var input = inputs[i];
				input.setAttribute("disabled", "disabled");
			}

			row.dataset.edit = false;
			row.removeClass('row-editing');
			row.removeClass('has-error');
			actions[0].innerHTML = "edit";
			actions[0].removeClass('discard');
			actions[0].addClass('edit');
			actions[0].style.display = 'inline-block';
			el.style.display = 'none';
		}

		function editRow (row, el) {
			var dataEdit = row.dataset.edit,
					inputs = row.q('input.td-input'),
					actions = row.q('button.ac-link');

			if ( typeof dataEdit == 'undefined' || dataEdit == 'false') {
				row.dataset.edit = true;
				for ( var i = 0; i < inputs.length; i++ ) {
					var input = inputs[i];
					input.dataset.prev = input.value;
					input.removeAttribute("disabled");
				}
				row.addClass('row-editing');
				inputs[0].focus();
				el.innerHTML = "discard";
				el.removeClass('edit');
				el.addClass('discard');
				actions[1].style.display = 'inline-block';
			}
			else {
				row.dataset.edit = false;
				for ( var i = 0; i < inputs.length; i++ ) {
					var input = inputs[i];
					input.value = input.dataset.prev;
					input.setAttribute("disabled", "disabled");
				}
				row.removeClass('row-editing');
				el.innerHTML = "edit";
				el.removeClass('discard');
				el.addClass('edit');
				actions[1].style.display = 'none';
			}

			return false;
		}

		function deleteRow (row) {
			row.parentNode.removeChild(row);
			formFilled.disableBtn();
			formFilled.updateFields();
			return false;
		}

		function initRow (row, data) {
			var cols = row.q('td'),
				inputs = row.q('input.td-input'),
				actions = cols[3].q('button.ac-link');

			if (  typeof data.error == 'undefined' || data.error == '' ) {
				// no error
				actions[1].style.display = 'none';
				for ( var i = 0; i < inputs.length; i++ ) {
					var ip = inputs[i];
					ip.setAttribute("disabled", "disabled");
				}
			}
			else {
				actions[0].style.display = 'none';
				row.addClass('has-error');
			}

			// edit
			actions[0].onclick = function () {
				editRow(row, this);
				return false;
			};

			// done
			actions[1].onclick = function () {
				commitRow(row, this);
				return false;
			};

			// delete
			actions[2].onclick = function () {
				//console.log(this);
				deleteRow(row);
				return false;
			};
		}
		//	=========================================
		//	=========================================


		if (typeof hasBroadSoftAlerts == 'undefined') {
			hidePrevAlerts();
		}
		//
		//	---------------------------------------------------------------------------------------------------------
		//	Save mapping bridges to employees
		//	---------------------------------------------------------------------------------------------------------
		var form2 = context.q('#formPSEmpData')[0];
		form2.onsubmit = function () {
			var selBridges = form2.q('tr.tablerow[data-id]'),
				bridge;
			var uploadData = {};
			for (var i = 0; i < selBridges.length; i++) {
				bridge = selBridges[i].q('.search-dropdown')[0];
				if (bridge.className != 'search-dropdown'){
					if (bridge.getAttribute('data-selected') == '')
						uploadData[selBridges[i].getAttribute('data-id')] = 'null';
					else
						uploadData[selBridges[i].getAttribute('data-id')] = bridge.getAttribute('data-selected');
				}
			}
			eventLogger('start', 'saveConfCallBridgeSettings');
			if ( selectedPS == 'ringcentral' ) {
				new arc.ajax(base_url + 'ringcentral-settings/', {
					method: 'POST',
					data: {
						accounts: uploadData
					},
					callback: function (data) {
						eventLogger('end', 'saveConfCallBridgeSettings');
						for (var i = 0; i < selBridges.length; i++) {
							bridge = selBridges[i].q('.search-dropdown')[0];
							bridge.className = 'search-dropdown';
							//bridge.setAttribute('data-value', bridge.value);
							bridge.setAttribute('data-value', bridge.getAttribute('data-selected'));
						}
						showAlert('success', 'Phone System bridges are assigned.');
						hasBroadSoftAlerts = true;
						scrollToTop();
					}
				});
			}
			else if ( selectedPS == 'vbc' ) {
				new arc.ajax(base_url + 'vbc-settings/', {
					method: 'POST',
					data: {
						accounts: uploadData
					},
					callback: function (data) {
						eventLogger('end', 'saveConfCallBridgeSettings');
						for (var i = 0; i < selBridges.length; i++) {
							bridge = selBridges[i].q('.search-dropdown')[0];
							bridge.className = 'search-dropdown';
							//bridge.setAttribute('data-value', bridge.value);
							bridge.setAttribute('data-value', bridge.getAttribute('data-selected'));
						}
						showAlert('success', 'Phone System bridges are assigned.');
						hasBroadSoftAlerts = true;
						scrollToTop();
					}
				});
			}
			else {
				new arc.ajax(base_url + 'broadsoft-settings/', {
					method: 'POST',
					data: {
						bridges: uploadData
					},
					callback: function (data) {
						eventLogger('end', 'saveConfCallBridgeSettings');
						for (var i = 0; i < selBridges.length; i++) {
							bridge = selBridges[i].q('.search-dropdown')[0];
							bridge.className = 'search-dropdown';
							//bridge.setAttribute('data-value', bridge.value);
							bridge.setAttribute('data-value', bridge.getAttribute('data-selected'));
						}
						showAlert('success', 'Phone System bridges are assigned.');
						hasBroadSoftAlerts = true;
						scrollToTop();
					}
				});
			}

			return false;
		}
		//
		//	---------------------------------------------------------------------------------------------------------
		//	Reset form
		//	---------------------------------------------------------------------------------------------------------
		form2.q('input.button.cancel')[0].onclick = function () {
			var selects = dropdowns;//form2.q('.search-dropdown');
			for (var i = 0; i < selects.length; i++) {
				/*var tmp; tmp = selects[i].DOM.getAttribute('data-value').split(':')[1]; if (tmp == '') tmp = null;*/
				selects[i].setValue( selects[i].DOM.getAttribute('data-value') );
				//selects[i].className = '';
				selects[i].DOM.parentNode.setAttribute('sorttable_customkey', bridges[ selects[i].DOM.getAttribute('data-value') ]);//.split(':')[1]
			}
			scrollToTop();
		}
		// form.q('input[type="button"]')[0].onclick = function () {
		// 	BSCredentialsContainer.appendChild( arcReact({id: 'new', xsiface: '', userid: ''}, BSCredentialsRegent) );
		// }
		//	---------------------------------------------------------------------------------------------------------
		//	Save Conf call settings
		//	---------------------------------------------------------------------------------------------------------
		form.onsubmit = function () {
			hidePrevAlerts();
			var ids = form.elements['id[]'];
			var xsis = form.elements['xsi[]'];
			var userids = form.elements['userid[]'];
			var passwords = form.elements['password[]'];
			if (typeof ids == 'undefined'){
				ids = [];
				xsis = [];
				userids = [];
				passwords = [];
			}
			else if (typeof ids.length == 'undefined'){
				ids = [ids];
				xsis = [xsis];
				userids = [userids];
				passwords = [passwords];
			}
			//
			var data = [];
			var allPassed = xsis.length > 0;
			for (var i = 0; i < xsis.length; i++){
				if (xsis[i].value.trim() == '') {
					showAlert('error', 'Please enter the Admin Phone');
					xsis[i].focus();
					allPassed = false;
				}
				else if (userids[i].value.trim() == '') {
					showAlert('error', 'Please enter the Admin Email/Username');
					userids[i].focus();
					allPassed = false;
				}
				else if (ids[i].value == 'new' && passwords[i].value == '[ENCRYPTED]') {
					showAlert('error', 'Please enter the password');
					passwords[i].focus();
					allPassed = false;
				}
				else
					data.push({
						id: ids[i].value,
						xsiface: xsis[i].value,
						userid: userids[i].value,
						password: passwords[i].value
					});
			}
			if ( allPassed || data.length == 0 ){
				eventLogger('start', 'saveConfCallSettings');
				data.system = 'broadsoft';
				new arc.ajax(base_url + 'broadsoft-settings/', {
					method: 'POST',
					data: data,
					callback: function (data) {
						eventLogger('end', 'saveConfCallSettings');
						data = eval('(' + data.responseText + ')');
						if (data.result == 'success'){
							form.q('input.button.red')[0].removeAttribute('disabled');
							showAlert('success', 'Phone System settings are saved.');
							hasBroadSoftAlerts = true
							moduleSettingsProductPhoneSystem.onload(context, params);
						}
						else if (data.result == 'error'){
							showAlert('error', 'Invalid data.');
						}
						else{
							showAlert('error', 'Please enter valid data');
						}
					}
				});
			}
			return false;
		}

		//	---------------------------------------------------------------------------------------------------------
		//	RINGCENTRAL POPUP
		//	---------------------------------------------------------------------------------------------------------

		// reset button
		rc_auth_btn.innerHTML = 'Authenticate with RingCentral';
		rc_auth_btn.removeAttribute('disabled');

		function open_rc_popup () {
			var w = 500;
			var h = 500;

			// Fixes dual-screen position
			var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
			var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

			var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
			var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

			var left = ((width / 2) - (w / 2)) + dualScreenLeft;
			var top = ((height / 2) - (h / 2)) + dualScreenTop;

			if (rc_auth_window == null || rc_auth_window.closed) {
				rc_auth_window = window.open(
					base_url + 'rc-auth/',
					'rc_popup',
					'width=' + w + ',height=' + h + ',top=' + top + 'left=' + left + ',resizable,scrollbars,status'
				);
				rc_timer = setInterval(check_rc_window, 500);
			}
			else{
				rc_auth_window.focus();
			}
		}

		// check if rc_auth_window is closed
		function check_rc_window () {
			if (rc_auth_window.closed) {
				rc_auth_btn.innerHTML = 'Authenticate with RingCentral';
				rc_auth_btn.removeAttribute('disabled');
				clearInterval(rc_timer);
			}
		}

		window.handleRCResponse = function (response) {
			rc_auth_window.close();
			if ( response == 'success' ) {
				rc_auth_btn.innerHTML = '<i class="fa fa-check fa-fw"></i> Authentication successful';
				rsForm.q('input.button.red')[0].removeAttribute('disabled');
				showAlert('success', 'RingCentral has been successfully authenticated');
				hasBroadSoftAlerts = true
				moduleSettingsProductPhoneSystem.onload(context, params);
			}
			else {
				rc_auth_btn.innerHTML = 'Authenticate with RingCentral';
				rc_auth_btn.removeAttribute('disabled');
				showAlert('error', response);
			}
		}

		rc_auth_btn.onclick = function(){
			this.setAttribute('disabled', true);
			this.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Authentication in progress';
			open_rc_popup();
		}

		//
		//	---------------------------------------------------------------------------------------------------------
		//	VBC POPUP
		//	---------------------------------------------------------------------------------------------------------
	
		// reset button
		vbc_auth_btn.innerHTML = 'Authenticate with Vonage Business Cloud';
		vbc_auth_btn.removeAttribute('disabled');
	
		function open_vbc_popup(){
			var w = 500;
			var h = 585;
	
			// Fixes dual-screen position
			var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
			var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
	
			var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
			var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
	
			var left = ((width / 2) - (w / 2)) + dualScreenLeft;
			var top = ((height / 2) - (h / 2)) + dualScreenTop;
	
			if (vbc_auth_window == null || vbc_auth_window.closed){
				vbc_auth_window = window.open(
					base_url + 'vbc-auth/',
					'vbc_popup',
					'width=' + w + ',height=' + h + ',top=' + top + 'left=' + left + ',resizable,scrollbars,status'
				);
				vbc_timer = setInterval(check_vbc_window, 500);
			}
			else{
				vbc_auth_window.focus();
			}
		}
	
		// check if vbc_auth_window is closed
		function check_vbc_window(){
			if (vbc_auth_window.closed){
				vbc_auth_btn.innerHTML = 'Authenticate with Vonage Business Cloud';
				vbc_auth_btn.removeAttribute('disabled');
				clearInterval(vbc_timer);
			}
		}
	
		window.handleVBCResponse = function (response) {
			vbc_auth_window.close();
			if ( response == 'success' ) {
				vbc_auth_btn.innerHTML = '<i class="fa fa-check fa-fw"></i> Authentication successful';
				rsForm.q('input.button.red')[0].removeAttribute('disabled');
				showAlert('success', 'Vonage Business Cloud has been successfully authenticated');
				hasBroadSoftAlerts = true
				moduleSettingsProductPhoneSystem.onload(context, params);
			}
			else {
				vbc_auth_btn.innerHTML = 'Authenticate with Vonage Business Cloud';
				vbc_auth_btn.removeAttribute('disabled');
				showAlert('error', response);
			}
		}
	
		vbc_auth_btn.onclick = function () {
			this.setAttribute('disabled', true);
			this.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Authentication in progress';
			open_vbc_popup();
		}
	
		//	//==========================================================================
		//	//==========================================================================
		//	Load Conf call details
		//	//==========================================================================
		//	//==========================================================================
		//*
		hideSaveCancelButtons(context);
		var dropdowns = [];
		new arc.ajax(base_url + 'ringcentral-settings/', {
			callback: function (data) {
				data = eval('(' + data.responseText + ')');
				employeedata.innerHTML = '<tr class="no-records"><td colspan="3"><i>No records to display</i></td></tr>';
				//==========================================================================
				// Load RingCentral settings
				//==========================================================================
				if ( data.settings.system == 'ringcentral' ) {
					eventLogger('end', 'confCallSettings');
					selectedPS = 'ringcentral';
					psSelect.value = selectedPS;
					hideShowSystem();
					//hideShowSystem();
					rsForm.removeAttribute('disabled');
					if ( typeof data.accounts.error != 'undefined' ) {
						// handle error
						hidePrevAlerts();
						showAlert('error', data.accounts.error);
						hasBroadSoftAlerts = true;
						if (data.accounts.error == 'credentials-not-set')
							rsForm.setAttribute('disabled', true);
					}
					else if ( data.accounts.length > 0 ) {
						var ringCentralAccountsAssignStatus = {};
						formContainer.hide();
						showSaveCancelButtons(context);
						accounts = {}, dropdowns = [];
						var dropdown;//, tmp;
						for (var i = 0; i < data.accounts.length; i++) {
							accounts[data.accounts[i].id] = data.accounts[i].id + ' - (' + (data.accounts[i].email != '' ? data.accounts[i].email : data.accounts[i].name) + ')';
							if (typeof ringCentralAccountsAssignStatus[data.accounts[i].id] == 'undefined'){
								ringCentralAccountsAssignStatus[data.accounts[i].id] = {};
								ringCentralAccountsAssignStatus[data.accounts[i].id]['user_set'] = false;
								ringCentralAccountsAssignStatus[data.accounts[i].id]['default_user_set'] = false;
								ringCentralAccountsAssignStatus[data.accounts[i].id]['default_user'] = {};
							}
						}
						//
						employeedata.innerHTML = '';
						for (var i in data.employees) {
							var icon = usernameIcon(data.employees[i].fullname);

							dropdown = new makeSearchDropdown(accounts, data.employees[i].ringcentral_id,
								function (sel) {
									//enforceSingleAssignment(dropdowns);
								}, 'null', dropdowns);

							dropdowns.push(dropdown);

							var employee = arc.reactor({
								tr: {
									class: 'tablerow',
									'data-id': i,
									content: [
										{
											td: {
												content: {
													div: {
														class: 'user-name-wrap',
														content: {
															p: {
																class: 'user-name',
																content: data.employees[i].fullname
															}
														}
													}
												},
												style: 'border-left:2px solid #' + icon[1]
											}
										},
										{
											td: {
												content: roles[data.employees[i].role]
											}
										},
										{
											td: {
												content: dropdown.DOM,
												'sorttable_customkey': data.employees[i].ringcentral_id
											}
										},
									]
								}
							});
							employeedata.appendChild(employee);
							// ------------------------------------------- PREVENT SPOOKY ERROR
							new (function(dropdown, accounts, ringCentralAccountsAssignStatus, i){
								setTimeout(function(){
							// -------------------------------------------
									for (var j in accounts) {
										let tmp = accounts[j].split(' (');
										if (tmp.length > 1){
											if (tmp[1].toLowerCase() == data.employees[i].email.toLowerCase() + ')') {
												 if (ringCentralAccountsAssignStatus[j]['user_set'] == false){
													if (dropdown.value == 'null' || dropdown.value == null){
														dropdown.setValue(j);
														//dropdown.className = 'updated';
														dropdown.parentNode.setAttribute('sorttable_customkey', accounts[j]);
														ringCentralAccountsAssignStatus[j]['default_user'] = dropdown;
														ringCentralAccountsAssignStatus[j]['default_user_set'] = true;
													}
												}
											} else if (dropdown.value == j){
												if (ringCentralAccountsAssignStatus[j]['default_user_set'] == true){
													ringCentralAccountsAssignStatus[j]['default_user'].removeAttribute("class");
													ringCentralAccountsAssignStatus[j]['default_user'].value = 'null';
													ringCentralAccountsAssignStatus[j]['default_user'].parentNode.setAttribute('sorttable_customkey', 'undefined');
												} else {
													ringCentralAccountsAssignStatus[j]['user_set'] = true;
												}
											}
										}
									}
							// -------------------------------------------
								}, 10);
							})(dropdown, accounts, ringCentralAccountsAssignStatus, i);
							// -------------------------------------------
						}
						//
						sortColumn(employeedata.parentNode, 0);
						enforceSingleAssignment(dropdowns);
					}
					if (typeof data.accounts.error != 'undefined' || data.accounts.length == 0) {
						setTimeout(function () {
							formContainer.show();
						}, 250);
					}
				}
				//==========================================================================
				// If the system is not RingCentral check if and load VonageBusinessCloud settings
				//==========================================================================
				else if ( data.settings.system == 'vbc' ) {
					selectedPS = 'vbc';
					psSelect.value = selectedPS;
					hideShowSystem();
					//hideShowSystem();
					new arc.ajax(base_url + 'vbc-settings/', {
						callback: function (data) {
							eventLogger('end', 'confCallSettings');
							data = eval('(' + data.responseText + ')');
							vbcForm.removeAttribute('disabled');
							if ( typeof data.accounts == 'undefined' || typeof data.accounts.error != 'undefined' ) {
								// handle error
								hidePrevAlerts();
								showAlert('error', data.accounts.error);
								hasBroadSoftAlerts = true;
								if (data.accounts.error == 'credentials-not-set')
									vbcForm.setAttribute('disabled', true);
							}
							else if ( data.accounts.length > 0 ) {
								var vonageBusinessCloudAccountsAssignStatus = {};
								formContainer.hide();
								showSaveCancelButtons(context);
								accounts = {}, dropdowns = [];
								var dropdown;//, tmp;
								for (var i = 0; i < data.accounts.length; i++) {
									accounts[data.accounts[i].id] = data.accounts[i].extensionNumber + ' - (' + (data.accounts[i].email != '' ? data.accounts[i].email : data.accounts[i].name) + ')';
									if (typeof vonageBusinessCloudAccountsAssignStatus[data.accounts[i].id] == 'undefined'){
										vonageBusinessCloudAccountsAssignStatus[data.accounts[i].id] = {};
										vonageBusinessCloudAccountsAssignStatus[data.accounts[i].id]['user_set'] = false;
										vonageBusinessCloudAccountsAssignStatus[data.accounts[i].id]['default_user_set'] = false;
										vonageBusinessCloudAccountsAssignStatus[data.accounts[i].id]['default_user'] = {};
									}
								}
								//
								employeedata.innerHTML = '';
								for (var i in data.employees) {
									var icon = usernameIcon(data.employees[i].fullname);
	
									dropdown = new makeSearchDropdown(accounts, data.employees[i].ringcentral_id,
										function (sel) {
											//enforceSingleAssignment(dropdowns);
										}, 'null', dropdowns);
	
									dropdowns.push(dropdown);
	
									var employee = arc.reactor({
										tr: {
											class: 'tablerow',
											'data-id': i,
											content: [
												{
													td: {
														content: {
															div: {
																class: 'user-name-wrap',
																content: {
																	p: {
																		class: 'user-name',
																		content: data.employees[i].fullname
																	}
																}
															}
														},
														style: 'border-left:2px solid #' + icon[1]
													}
												},
												{
													td: {
														content: roles[data.employees[i].role]
													}
												},
												{
													td: {
														content: dropdown.DOM,
														'sorttable_customkey': data.employees[i].ringcentral_id
													}
												},
											]
										}
									});
									employeedata.appendChild(employee);
									// ------------------------------------------- PREVENT SPOOKY ERROR
									new (function(dropdown, accounts, vonageBusinessCloudAccountsAssignStatus, i){
										setTimeout(function(){
									// -------------------------------------------
											for (var j in accounts) {
												let tmp = accounts[j].split(' (');
												if (tmp.length > 1){
													if (tmp[1].toLowerCase() == data.employees[i].email.toLowerCase() + ')') {
														 if (vonageBusinessCloudAccountsAssignStatus[j]['user_set'] == false){
															if (dropdown.value == 'null' || dropdown.value == null){
																dropdown.setValue(j);
																//dropdown.className = 'updated';
																dropdown.DOM.parentNode.setAttribute('sorttable_customkey', accounts[j]);
																vonageBusinessCloudAccountsAssignStatus[j]['default_user'] = dropdown;
																vonageBusinessCloudAccountsAssignStatus[j]['default_user_set'] = true;
															}
														}
													} else if (dropdown.value == j){
														if (vonageBusinessCloudAccountsAssignStatus[j]['default_user_set'] == true){
															vonageBusinessCloudAccountsAssignStatus[j]['default_user'].removeAttribute("class");
															vonageBusinessCloudAccountsAssignStatus[j]['default_user'].value = 'null';
															vonageBusinessCloudAccountsAssignStatus[j]['default_user'].parentNode.setAttribute('sorttable_customkey', 'undefined');
														} else {
															vonageBusinessCloudAccountsAssignStatus[j]['user_set'] = true;
														}
													}
												}
											}
									// -------------------------------------------
										}, 10);
									})(dropdown, accounts, vonageBusinessCloudAccountsAssignStatus, i);
									// -------------------------------------------
								}
								//
								sortColumn(employeedata.parentNode, 0);
								enforceSingleAssignment(dropdowns);
							}
							if (typeof data.accounts.error != 'undefined' || data.accounts.length == 0) {
								setTimeout(function () {
									formContainer.show();
									form.q('input.button.red')[0].setAttribute('disabled', true);
								}, 25);
							}
						}
					});
				}
				//==========================================================================
				// If the system is not RingCentral check if and load BroadSoft settings
				//==========================================================================
				else if ( data.settings.system == 'broadsoft' ){
					selectedPS = 'broadsoft';
					psSelect.value = selectedPS;
					hideShowSystem();
					new arc.ajax(base_url + 'broadsoft-settings/', {
						callback: function (data) {
							data = eval('(' + data.responseText + ')');
							data.bridges = [];
							var allPassed = true, progress = [0, 0];

							credTable.innerHTML = '';
							for (adminid in data.settings) {
								var newRow = arc.reactor(newRowSchema),
									btns = newRow.q('td button'),
									inputs = newRow.q('td input');

								if ( data.settings[adminid].error == "credentials-wrong" ) {
									allPassed = false;
								}

								data.settings[adminid].id = adminid;
								inputs[0].value = adminid;
								inputs[1].value = data.settings[adminid].xsiface;
								inputs[2].value = data.settings[adminid].userid;
								inputs[3].value = '[ENCRYPTED]';
								initRow(newRow, data.settings[adminid]);
								credTable.appendChild(newRow);
								//
								progress[0] += 1;
								new arc.ajax(base_url + 'broadsoft-bridges/?userid='+data.settings[adminid].userid, {
									callback: function (bridges) {
										if (bridges.responseText == '{"error": "credentials-wrong"}'){
											hidePrevAlerts();
											showAlert('error', 'The credentials you have entered for one or more records is invalid');
											//return false;
										}
										//
										bridges = eval('(' + bridges.responseText + ')');
										for (var i = 0; i < bridges.length; i++)
											data.bridges.push(bridges[i]);
										//
										progress[1] += 1;
										if (progress[0] == progress[1]){
											continueLoading();
											form.q('input.button.red')[0].removeAttribute('disabled');
										}
									}
								});
							}

							formFilled.updateFields();

							if ( !allPassed ) {
								hidePrevAlerts();
								showAlert('error', 'The credentials you have entered for one or more records is invalid');
								hasBroadSoftAlerts = true;
							}
							// ---------------------------------------------------------------------------
							var continueLoading = function(){
								eventLogger('end', 'confCallSettings');
								//employeedata.innerHTML = '<tr class="no-records"><td colspan="3"><i>No records to display</i></td></tr>';
								if (typeof data.bridges.error != 'undefined') {
									if (data.bridges.error == 'credentials-not-set') {
										// showAlert('warning', 'Please enter Phone System admin email and password');
									}
									else if (data.bridges.error == 'credentials-wrong') {
										showAlert('error', 'The admin email and password you have entered is wrong');
										hasBroadSoftAlerts = true;
									}
									else {
										showAlert('error', data.bridges.error.message);
										hasBroadSoftAlerts = true;
									}
								}
								else if (data.bridges.length > 0) {
									formContainer.hide();
									showSaveCancelButtons(context);
									bridges = {}, dropdowns = [];
									var tmp;
									for (var i = 0; i < data.bridges.length; i++) {
										tmp = data.bridges[i].userid.split('@')
										bridges[data.bridges[i].admin + ':' + data.bridges[i].userid] = tmp[0] + ' - ' + ' (' + (data.bridges[i].email != '' ? data.bridges[i].email : data.bridges[i].name) + ')'; // + data.bridges[i].name;
									}
									//
									var dropdown;
									employeedata.innerHTML = '';
									for (var i in data.employees) {
										var icon = usernameIcon(data.employees[i].fullname);
										dropdown = new makeSearchDropdown(bridges, data.employees[i].broadsoft.admin + ':' + data.employees[i].broadsoft.userid,
											function (sel) {
												//enforceSingleAssignment(dropdowns);
											}, 'null', dropdowns);
										dropdowns.push(dropdown);
										var employee = arc.reactor({
											tr: {
												class: 'tablerow',
												'data-id': i,
												content: [
													{
														td: {
															content: {
																div: {
																	class: 'user-name-wrap',
																	content: {
																		p: {
																			class: 'user-name',
																			content: data.employees[i].fullname
																		}
																	}
																}
															},
															style: 'border-left:2px solid #' + icon[1]
														}
													},
													{
														td: {
															content: roles[data.employees[i].role]
														}
													},
													{
														td: {
															content: dropdown.DOM,
															'sorttable_customkey': bridges[data.employees[i].broadsoft.admin + ':' + data.employees[i].broadsoft.userid]
														}
													},
												]
											}
										});
										employeedata.appendChild(employee);
										if (dropdown.value == 'null' || dropdown.value == null)
											for (var j in bridges) {
												tmp = bridges[j].split(' (');
												if (tmp.length > 1 && tmp[1].toLowerCase() == data.employees[i].email.toLowerCase() + ')') {
													dropdown.setValue(j);
													//dropdown.DOM.className = 'updated';
													//dropdown.DOM.parentNode.setAttribute('sorttable_customkey', bridges[j]);
												}
											}
									}
									//
									sortColumn(employeedata.parentNode, 0);
									enforceSingleAssignment(dropdowns);
								}
								//	Not Else-If : If there were any error or no data
								if (typeof data.bridges.error != 'undefined' || data.bridges.length == 0)
									setTimeout(function () {
										formContainer.show();
									}, 250);
								//
							}
							if (Object.keys(data.settings).length == 0){
								continueLoading();
								formContainer.show();
								form.q('input.button.red')[0].setAttribute('disabled', true);
							}
							// ---------------------------------------------------------------------------
							if (Object.keys(data.settings).length == 0)
								continueLoading();
						}
					});

				}
				//==========================================================================
				//==========================================================================
				else{
					// NO PHONE SYSTEM SELECTED
					eventLogger('end', 'confCallSettings');
					formContainer.show();
					selectedPS = 'none';
					psSelect.value = '';
					form.q('input.button.red')[0].setAttribute('disabled', true);
					rsForm.q('input.button.red')[0].setAttribute('disabled', true);
					vbcForm.q('input.button.red')[0].setAttribute('disabled', true);
					hideShowSystem();
				}
			}
		});
		//*/
		//	---------------------------------------------------------------------------------------------------------
		form.q('input.button.cancel.gray')[0].onclick = function () {
			formContainer.hide();
			return false;
		}
		rsForm.q('.button.cancel')[0].onclick = function () {
			formContainer.hide();
			return false;
		}
		vbcForm.q('.button.cancel')[0].onclick = function () {
			formContainer.hide();
			return false;
		}
		//	---------------------------------------------------------------------------------------------------------
		//	Disconnect
		//	---------------------------------------------------------------------------------------------------------
		form.q('input.button.red')[0].onclick = function () {
			new arc.ajax(base_url + 'disconnect-products/?product=broadsoft', {
				callback: function (data){
					for (var i = 0; i < credTable.rows.length; i++)
						deleteRow(credTable.rows[i]);
					formContainer.hide();
					moduleSettingsProductPhoneSystem.onload(context, params);
					showAlert('success', 'Disconnected successfully.');
				}
			});
		}
		rsForm.q('input.button.red')[0].onclick = function () {
			new arc.ajax(base_url + 'disconnect-products/?product=ringcentral', {
				callback: function (data){
					formContainer.hide();
					moduleSettingsProductPhoneSystem.onload(context, params);
					showAlert('success', 'Disconnected successfully.');
				}
			});
		}
		vbcForm.q('input.button.red')[0].onclick = function () {
			new arc.ajax(base_url + 'disconnect-products/?product=vbc', {
				callback: function (data){
					formContainer.hide();
					moduleSettingsProductPhoneSystem.onload(context, params);
					showAlert('success', 'Disconnected successfully.');
				}
			});
		}
		//	---------------------------------------------------------------------------------------------------------
		context.q('input[type="button"]')[0].onclick = function () {
			formContainer.show();
			return false;
		}
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		if (typeof loaded == 'function')
			loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};