
var moduleSettingsProductCRMSystem = module.exports = {
	//	-----------------------------------------------------------------------------------------------
	onload: function(context, params, e, loaded){
		eventLogger('start', 'CRMSettings');
		var formContainer = new scrollOpenClose(context.q('div.one-row'));
		var form = context.q('form')[0],
			formSalesforce = context.q('form')[1],
			formProsperWorks = context.q('form')[2],
			formSugarCRM = context.q('form')[3],
			formModules = context.q('.crm-modules form')[0];

		var accounts = {},
			dropdowns = [];
		var employeedata = context.q('table.employeedata tbody')[0];
		sorttable.makeSortable(employeedata.parentNode);

		var zohoFilled = new checkFilled(form),
			sfFilled = new checkFilled(formSalesforce),
			ppWFilled = new checkFilled(formProsperWorks),
			sgFilled = new checkFilled(formSugarCRM);

		var systemSelect = document.getElementById('system'),
			selectedSystemVal = systemSelect.value,
			selectedSystem = null;

		var crmHelpModal;
		var crmWarnModal;

		var availableModules = {
			zoho: [
				['leads', 0],
				['invoices', 0],
				['accounts', 0],
				['tasks', 0],
				['events', 0],
				['calls', 0]
			],
			salesforce: [
				['New Leads', 0],
				['New Opportunities', 0],
				['Chatter Messages', 0],
				['Chatter Activities', 0],
				['Calls Logged',0],
				['Activities',0]
			],
			prosperworks: [
				['leads', 0],
				['opportunities', 0]
			],
			sugarCRM: [
				['leads', 0],
				['opportunities', 0],
				['meetings', 0],
				['calls', 0]
			]
		};

		var formData = {};

		if (typeof hasCRMAlerts == 'undefined') {
			hidePrevAlerts();
		}

		context.q('.crm-modules')[0].style.display = 'none';

		// modules
		function renderModules (data) {
			let modules = availableModules[selectedSystem];
			let preVals;
			let moduleHolder = context.q('.crm-modules .module-switches')[0];
			let moduleHolderHeading = context.q('.crm-modules .card-heading')[0];
			let moduleBtn = context.q('.crm-modules input[type="submit"]')[0];
			let sysText = '';

			if ( typeof modules === 'undefined' ) return false;

			switch (selectedSystem) {
				case 'zoho':
					sysText = 'ZOHO';
					break;
				case 'salesforce':
					sysText = 'Salesforce';
					break;
				case 'prosperworks':
					sysText = 'ProsperWorks';
					break;
				case 'sugarCRM':
					sysText = 'SugarCRM';
					break;
				default:
					sysText = 'CRM System';
			}

			moduleHolderHeading.innerHTML = 'Available modules for ' + sysText;

			function updateModules (dule, value) {
				let modVal = [];

				for (var m of modules) {
					if (m[0] === dule) {
						m[1] = (value ? 1 : 0);
					}
					modVal.push(m[1]);
				}

				modVal = modVal.reverse().join('');
				modVal = parseInt(modVal, 2);

				if (preVals !== modVal){
					moduleBtn.removeAttribute('disabled');
				}
				else{
					moduleBtn.setAttribute('disabled', true);
				}

			}

			for (let i = 0; i < modules.length; i++) {
				let cb;
				let nom = modules[i][0].trim().replace('_', ' ');
				modules[i][1] = parseInt(data[i]);

				cb = arc.elem('label', '<input type="checkbox" name="modules[' + modules[i][0] + ']">' + '<span class="text">' + toTitleCase(nom) + '</span>', {
					class: 'module-switches_switch'
				});

				cb.q('input')[0].onchange = function () {
					updateModules(this.name.replace('modules[', '').replace(']',''), this.checked);
				}

				if (modules[i][1] === 1) {
					cb.q('input')[0].checked  = true;
				}

				moduleHolder.appendChild(cb);
			}

			preVals = (function () {
				let x = [];

				for (let m of modules) {
					x.push(m[1]);
				}

				x = x.reverse().join('');
				x = parseInt(x, 2);

				return x;

			})();

		}

		//	---------------------------------------------------------------------------------------------------------
		//	CRM Toggle fields
		//	---------------------------------------------------------------------------------------------------------

		var toggleSystem = function () {
			var crmSystemBlocks = document.getElementsByClassName("crm-system-block");
			for (var i = 0; i < crmSystemBlocks.length; i++) {
				crmSystemBlocks[i].style.display = 'none';
			}
			if (selectedSystemVal) {
				document.getElementById("system-" + selectedSystemVal).style.display = 'block';
			}
			fillForm(false);
		}

		systemSelect.onchange = function () {
			selectedSystemVal = systemSelect.value;
			toggleSystem();
		};

		function validateForm (form, names) {
			let result = {valid: true, invalidEl:[], errMsg:[]};
			for ( let name of names ) {
				let val = form[name].value;

				form[name].removeClass('has-error');

				if ( val.trim() == "" || (name==='token' && val.length >40)) {
					result.valid = false;
					result.invalidEl.push(form[name]);
					let msg = (name==='token' && val.length >40) ? 'Token exceeds the expected length' :'Please fill the following fields';
					result.errMsg.push(msg);
					form[name].addClass('has-error');
				}
			}
			return result;
		}

		//	---------------------------------------------------------------------------------------------------------
		//	Save CRM Modules
		//	---------------------------------------------------------------------------------------------------------
		formModules.onsubmit = function () {
			let modules = [],
				moduleEls = context.q('.module-switches input[type="checkbox"]'),
				modBtn = context.q('.crm-modules input[type="submit"]')[0];

			eventLogger('start', 'saveCRMModules');
			modBtn.setAttribute('disabled', true);

			for (let m of moduleEls) {
				modules.push(m.checked ? '1' : '0');
			}

			let modFlag = modules.reverse().join('');
			modFlag = parseInt(modFlag, 2);

			new arc.ajax(base_url + 'crm-settings/', {
				method: 'POST',
				data: {
					modules: modFlag
				},
				callback: function (data) {
					eventLogger('end', 'saveCRMModules');
					context.q('.crm-modules .module-switches')[0].innerHTML = '';
					renderModules(modules.reverse());
					//
					showAlert('success', 'CRM modules are saved.');
				},
				fallback: function (e) {
					eventLogger('end', 'saveCRMModules');
					showAlert('error', 'An error occoured while trying to save CRM modules.');
					modBtn.removeAttribute('disabled');
				}
			});
			return false;
		}

		//	---------------------------------------------------------------------------------------------------------
		//	Save CRM Settings
		//	---------------------------------------------------------------------------------------------------------
		form.onsubmit = function () {
			eventLogger('start', 'saveCRMSettings');
			var formValid = validateForm(this, ['token']);

			if ( formValid.valid ) {
				new arc.ajax(base_url + 'crm-settings/', {
					method: 'POST',
					data: {
						system: systemSelect.value,
						token: form.token.value
					},
					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						if (data.result == 'Success'){
							hasCRMAlerts = true;
							form.q('input.button.red')[0].removeAttribute('disabled');
							showAlert('success', 'CRM settings are saved.');
							moduleSettingsProductCRMSystem.onload(context, params);
						}
						else{
							eventLogger('end', 'saveCRMSettings');
							showAlert('error', 'Invalid Credentials');
						}
					}
				});
			}
			else {
				eventLogger('end', 'saveCRMSettings');
				scrollToTop();
				formValid.invalidEl[0].focus();
				showAlert('error', formValid.errMsg[0]);
			}

			return false;
		}

		//	---------------------------------------------------------------------------------------------------------
		//	Save Salesforce Settings
		//	---------------------------------------------------------------------------------------------------------
		formSalesforce.onsubmit = function () {
			eventLogger('start', 'saveSalesforceSettings');
			var formValid = validateForm(this, ['userid', 'password', 'salesforce_token']);

			if ( formValid.valid ) {
				new arc.ajax(base_url + 'crm-settings/', {
					method: 'POST',
					data: {
						system: systemSelect.value,
						admin: formSalesforce.userid.value,
						password: formSalesforce.password.value,
						token: formSalesforce.salesforce_token.value
					},
					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						if (data.result){
							hasCRMAlerts = true;
							formSalesforce.q('input.button.red')[0].removeAttribute('disabled');
							showAlert('success', 'CRM settings are saved.');
							moduleSettingsProductCRMSystem.onload(context, params);
						}
						else{
							eventLogger('end', 'saveSalesforceSettings');
							showAlert('error', 'Invalid Credentials');
						}
					}
				});
			}
			else {
				eventLogger('end', 'saveSalesforceSettings');
				scrollToTop();
				formValid.invalidEl[0].focus();
				showAlert('error', 'Please fill the following fields');
			}

			return false;
		}

		//	---------------------------------------------------------------------------------------------------------
		//	Save ProsperWorks Settings
		//	---------------------------------------------------------------------------------------------------------
		formProsperWorks.onsubmit = function () {
			eventLogger('start', 'saveProsperWorksSettings');

			var formValid = validateForm(this, ['crm_admin', 'prosperworks_token']);

			if ( formValid.valid ) {
				new arc.ajax(base_url + 'crm-settings/', {
					method: 'POST',
					data: {
						system: systemSelect.value,
						crm_admin: formProsperWorks.crm_admin.value,
						token: formProsperWorks.prosperworks_token.value
					},
					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						if (data.result){
							hasCRMAlerts = true;
							formProsperWorks.q('input.button.red')[0].removeAttribute('disabled');
							showAlert('success', 'CRM settings are saved.');
							moduleSettingsProductCRMSystem.onload(context, params);
						}
						else{
							eventLogger('end', 'saveProsperWorksSettings');
							showAlert('error', 'Invalid Credentials');
						}
					}

				});
			}
			else {
				eventLogger('end', 'saveProsperWorksSettings');
				scrollToTop();
				formValid.invalidEl[0].focus();
				showAlert('error', 'Please fill the following fields');
			}


			return false;
		}

		//	---------------------------------------------------------------------------------------------------------
		//	Save SugarCRM Settings
		//	---------------------------------------------------------------------------------------------------------
		formSugarCRM.onsubmit = function () {
			eventLogger('start', 'saveSugarCRMSettings');
			var formValid = validateForm(this, ['instance', 'sugarcrm_password', 'crm_admin',
			'sugar_key', 'sugar_key_name']);

			if ( formValid.valid ) {
				new arc.ajax(base_url + 'crm-settings/', {
					method: 'POST',
					data: {
						system: systemSelect.value,
						crm_admin: formSugarCRM.crm_admin.value,
						password: formSugarCRM.sugarcrm_password.value,
						instance: formSugarCRM.instance.value,
						key_name: formSugarCRM.sugar_key_name.value,
						key: formSugarCRM.sugar_key.value
					},

					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						if (data.result) {
							hasCRMAlerts = true;
							saveSuccess = true;
							formSugarCRM.q('input.button.red')[0].removeAttribute('disabled');
							moduleSettingsProductCRMSystem.onload(context, params);
						} else {
							eventLogger('end', 'saveSugarCRMSettings');
							scrollToTop();
							showAlert( 'error', 'The credentials you have entered are either invalid or are already in use.' );
						}
					}
				});
			}
			else {
				eventLogger('end', 'saveSugarCRMSettings');
				scrollToTop();
				formValid.invalidEl[0].focus();
				showAlert('error', 'Please fill the following fields');
			}


			return false;
		}


		//	---------------------------------------------------------------------------------------------------------
		//	CRM Mapping employees
		//	---------------------------------------------------------------------------------------------------------
		var form2 = context.q('form.crm-employees')[0];
		form2.onsubmit = function () {
			var selAccounts = form2.q('tr.tablerow[data-id]'),
				account,
				postURL = '',
				postData = {};
			var uploadData = {};

			for (var i = 0; i < selAccounts.length; i++) {
				account = selAccounts[i].q('.search-dropdown')[0];
				if (account.className.indexOf('updated') > -1)
					uploadData[selAccounts[i].getAttribute('data-id')] = account.getAttribute('data-selected');
			}

			postURL = 'crm-settings/';
			postData = {
				accounts: uploadData
			};

			eventLogger('start', 'saveCRMSettings');
			new arc.ajax(base_url + postURL, {
				method: 'POST',
				data: postData,
				callback: function (data) {
					eventLogger('end', 'saveCRMSettings');
					for (var i = 0; i < selAccounts.length; i++) {
						account = selAccounts[i].q('.search-dropdown')[0];
						account.className = 'search-dropdown';
						account.setAttribute('data-value', account.getAttribute('data-selected'));
					}
					showAlert('success', 'CRM accounts are assigned.');
					scrollToTop();
				}
			});
			return false;
		}
		//	---------------------------------------------------------------------------------------------------------
		//	Reset form
		//	---------------------------------------------------------------------------------------------------------
		form2.q('input.button.cancel')[0].onclick = function () {
			//var selects = form2.q('select[data-value]');
			for (var i = 0; i < dropdowns.length; i++) {
				dropdowns[i].setValue(dropdowns[i].DOM.getAttribute('data-value'));
				//dropdowns[i].DOM.removeClass('updated');
				dropdowns[i].DOM.parentNode.setAttribute('sorttable_customkey', accounts[dropdowns[i].DOM.getAttribute('data-value')]);
			}
			scrollToTop();
		}

		// populate employee data table
		var dropdowns;
		function populateData (data, callback) {
			let employees = data.employees,
				d_acc			= data.accounts,
				system		= data.settings.system,
				accounts 	= {},
				accountsAssignStatus = {};
			dropdowns = [];

			for ( let i = 0; i < d_acc.length; i++ ) {
				if(typeof accountsAssignStatus[d_acc[i].id] == 'undefined'){
					accountsAssignStatus[d_acc[i].id] = {};
					accountsAssignStatus[d_acc[i].id]['user_set'] = false;
					accountsAssignStatus[d_acc[i].id]['default_user_set'] = false;
					accountsAssignStatus[d_acc[i].id]['default_user'] = {};
				}
				if ( system == 'zoho' ) {
					accounts[d_acc[i].id] = d_acc[i].name + ' [' + d_acc[i].role + '] ' + d_acc[i].profile;
				} else if ( system == 'prosperworks' ) {
					accounts[d_acc[i].id] = d_acc[i].name + ' [' + d_acc[i].role + ']';
				} else if ( system == 'salesforce' ) {
					accounts[d_acc[i].id] = d_acc[i].name + ' [' + d_acc[i].email + ']';
				} else if ( system == 'sugarCRM' ) {
					accounts[d_acc[i].id] = d_acc[i].name + (d_acc[i].email ? ' [' + d_acc[i].email + ']' : '' );
				} else {
					break;
				}
			}

			employeedata.innerHTML = '';

			for ( let i in employees ) {
				let icon = usernameIcon(employees[i].fullname),
						dropdown, employee;

				dropdown = new makeSearchDropdown(accounts, employees[i].crm_id, function(sel) { /*enforceSingleAssignment(dropdowns)*/ }, 'null', dropdowns);

				dropdowns.push(dropdown);

				employee = arc.reactor({
					tr: {
						class: 'tablerow',
						'data-id': i,
						content: [{
								td: {
									content: {
										div: {
											class: 'user-name-wrap',
											content: {
												p: {
													class: 'user-name',
													content: employees[i].fullname
												}
											}
										}
									},
									style: 'border-left:2px solid #' + icon[1]
								}
							},
							{
								td: {
									content: roles[employees[i].role]
								}
							},
							{
								td: {
									content: dropdown.DOM,
									'sorttable_customkey': accounts[employees[i].crm_id]
								}
							},
						]
					}
				});

					for ( let j in accounts ) {
						if ( accounts[j].split(' [')[0].toLowerCase() == employees[i].fullname.toLowerCase()) {
							if (accountsAssignStatus[j]['user_set'] == false){
								if (dropdown.value == 'null' || dropdown.value == null){
									dropdown.setValue(j);
									//dropdown.className = 'updated';
									dropdown.DOM.parentNode.setAttribute('sorttable_customkey', accounts[j]);
									accountsAssignStatus[j]['default_user'] = dropdown;
									accountsAssignStatus[j]['default_user_set'] = true;
								}
							}
						} else if (dropdown.value == j){
							if (accountsAssignStatus[j]['default_user_set'] == true){
								//accountsAssignStatus[j]['default_user'].removeAttribute("class");
								accountsAssignStatus[j]['default_user'].setValue(null);
								accountsAssignStatus[j]['default_user'].DOM.parentNode.setAttribute('sorttable_customkey', 'undefined');
							} else {
								accountsAssignStatus[j]['user_set'] = true;
							}
						}
					}
				employeedata.appendChild(employee);
			}

			sortColumn(employeedata.parentNode, 0);
	//		enforceSingleAssignment(dropdowns);		***********************************************

			callback();
		}

		function fillForm (toggle) {
			eventLogger('end', 'CRMSettings');
			toggle = (typeof toggle !== 'undefined') ?  toggle : true;
			form.q('input.button.red')[0].setAttribute('disabled', true);
			formSalesforce.q('input.button.red')[0].setAttribute('disabled', true);
			formProsperWorks.q('input.button.red')[0].setAttribute('disabled', true);
			formSugarCRM.q('input.button.red')[0].setAttribute('disabled', true);
			switch (selectedSystem) {
				case 'zoho':
					if ( typeof formData.accounts.error === 'undefined' && typeof formData.accounts !== 'string' ) {
						form.token.value = '[ENCRYPTED]';
						form.q('input.button.red')[0].removeAttribute('disabled');
					}
					break;
				case 'salesforce':
					formSalesforce.userid.value = formData.settings.crm_admin || '';
					if ( formData.settings.crm_admin !== "" ) {
						formSalesforce.password.value = '[ENCRYPTED]';
						formSalesforce.salesforce_token.value = '[ENCRYPTED]';
						formSalesforce.q('input.button.red')[0].removeAttribute('disabled');
					}
					break;
				case 'prosperworks':
					formProsperWorks.crm_admin.value = formData.settings.crm_admin || '';
					if ( formData.settings.crm_admin !== "" ) {
						formProsperWorks.prosperworks_token.value = '[ENCRYPTED]';
						formProsperWorks.q('input.button.red')[0].removeAttribute('disabled');
					}
					break;
				case 'sugarCRM':
					formSugarCRM.instance.value = formData.settings.crm_instance || '';
					formSugarCRM.crm_admin.value = formData.settings.crm_admin || '';
					formSugarCRM.sugar_key_name.value = formData.settings.sugar_key_name || '';
					if (formData.settings.crm_admin !== "") {
						formSugarCRM.sugar_key.value = '[ENCRYPTED]';
						formSugarCRM.sugarcrm_password.value = '[ENCRYPTED]';
						formSugarCRM.q('input.button.red')[0].removeAttribute('disabled');
					}
					break;
				default:
					if (toggle) {
						formContainer.show(toggleSystem());
					}
			}
			if (typeof crmHelpModal === 'undefined') {
				let bd = '';
					bd += '<ol class="ol">';
					bd += '<li>Log in to your Sugar Instance using the Admin account</li>'
					bd += '<li>Navigate to <b>\'Administration\'</b></li>';
					bd += '<li>Select <b>\'OAuth Keys\'</b> under <b>\'System\'</b> section</li>';
					bd += '<li>On the navigation bar select the downward arrow next to \'OAuth Keys\' and select <b>\'Create OAuth Key\'</b></li>';
					bd += '<li>Create the OAuth key and select the OAuth Version as <b>OAuth 2.0</b></li>';
					bd += '<li>Save and add the details to Prodoscore</li>';
					bd += '</ol>'

		    crmHelpModal = new modal({
			title: 'Instructions',
			body: bd,
			footer: '<button class="button gray modal-ok">OK</button>'
		    });

		    crmHelpModal.el.q('.modal-ok')[0].onclick = function () {
			crmHelpModal.hide();
		    };

		    formSugarCRM.q('.help-text a')[0].onclick = function (e) {
			e.preventDefault();
			crmHelpModal.show();
		    }
			}
			if (typeof crmWarnModal === 'undefined') {

			    bd = `<ol>
				    <lh class="lh-left-margin"><b>You need to edit the following files in the Sugar CRM </b></lh>
				    <lh class="lh-left-margin"><b>directory.</b></lh>
				    <li class="with-top-margin"><b>config_override.php</b>
				<dl class="no-top-margin">
				    <dt><i>Change</i></dt>
				    <dt style="color: #6b6e71;"> $sugar_config[\'disable_unknown_platforms\'] = true</dt>
				    <dt><i>into</i></dt>
				    <dt style="color: #6b6e71;">$sugar_config[\'disable_unknown_platforms\'] = false;</dt>
				</dl>
				    </li>
			    <li><b>custom/client/platform.php</b>
				<dl class="no-top-margin">
				    <dt><i>There will be three lines in it as follows,</i></dt>
					<dd style="color: #6b6e71;">$platforms[] = \'base\';</dd>
					<dd style="color: #6b6e71;">$platforms[] = \'mobile\';</dd>
					<dd style="color: #6b6e71;">$platforms[] = \'portal\';</dd>
				    <dt><i>Add a new line as follows,</i></dt>
					<dd style="color: #6b6e71;">$platforms[] = \'prodoscore\';</dd>
				</dl>
			    </li>
				 </ol>`;

		    crmWarnModal = new modal({
			title: 'Instructions',
			body: bd,
			footer: '<button class="button gray modal-ok">OK</button>'
		    });

		    crmWarnModal.el.q('.modal-ok')[0].onclick = function () {
			crmWarnModal.hide();
		    };

		    formSugarCRM.q('.warn-text a')[0].onclick = function (e) {
			e.preventDefault();
			crmWarnModal.show();
		    }
			}
		}

		//	---------------------------------------------------------------------------------------------------------
		//	Load data
		//	---------------------------------------------------------------------------------------------------------
		hideSaveCancelButtons(context);
		new arc.ajax( base_url + 'crm-settings/', {
			callback: function (data) {
				data = eval( '(' + data.responseText + ')' );
				formData = data;
				selectedSystem = data.settings.system;
				hidePrevAlerts();

				// set selectedSystem in system drop down
				for ( var i = 0, optionsLength = systemSelect.options.length; i < optionsLength; i++ ) {
					if (systemSelect.options[i].value == data.settings.system) {
						systemSelect.selectedIndex = i;
						selectedSystemVal = systemSelect.value;
					}
				}

				if ( typeof data.accounts.error !== 'undefined' ) {
					eventLogger('end', 'CRMSettings');
					if ( typeof data.accounts.error == 'string' ) {
						showAlert( 'error', data.accounts.error );
					}
					else if ( typeof data.accounts.error.message == 'string' ) {
						showAlert( 'error', data.accounts.error.message );
					}
					formContainer.show(toggleSystem());
				}
				else if ( typeof data.accounts == 'string' ) {
					eventLogger('end', 'CRMSettings');
					showAlert( 'error', data.accounts );
					formContainer.show(toggleSystem());
				}

				if (typeof saveSuccess != 'undefined') {
					if ( saveSuccess == true ) {
						saveSuccess = false;
						showAlert('success', 'CRM settings are saved.');
					}
				}
				// token and password fields are set to '[ENCRYPTED]' to prevent autofilling
				// if the value == '[ENCRYPTED]' it's not saved

				if (data.settings.crm_modules === null) {
					data.settings.crm_modules = 0;
				}

				data.settings.crm_modules = ('000000' + data.settings.crm_modules.toString(2)).split('').reverse();

				context.q('.crm-modules .module-switches')[0].innerHTML = '';
				renderModules(data.settings.crm_modules);


				// reset the employee table
				employeedata.innerHTML = '<tr class="no-records"><td colspan="3"><i>No records to display</i></td></tr>';

				// reset forms and fill relevant form data
				form.reset();
				formSalesforce.reset();
				formProsperWorks.reset();
				formSugarCRM.reset();
				fillForm();

				zohoFilled.updateFields();
				sfFilled.updateFields();
				ppWFilled.updateFields();
				sgFilled.updateFields();

				if (typeof data.accounts.error != 'undefined' || typeof data.accounts == 'string' || data.accounts.length == 0) {
					eventLogger('end', 'CRMSettings');
					formContainer.show(toggleSystem());
				}
				else {
					context.q('.crm-modules')[0].style.display = 'block';
					showSaveCancelButtons(context);
					populateData(data, function () {
						eventLogger('end', 'CRMSettings');
						formContainer.hide();
					});
				}

			}

		});


		//	---------------------------------------------------------------------------------------------------------
		//	Disconnect
		//	---------------------------------------------------------------------------------------------------------
		form.q('input.button.red')[0].onclick = function () {
			new arc.ajax(base_url + 'disconnect-products/?product=zoho', {
				callback: function (data){
					formContainer.hide();
					moduleSettingsProductCRMSystem.onload(context, params);
					showAlert('success', 'Disconnected successfully.');
				}
			});
		}
		form.q('input.button.cancel.gray')[0].onclick = function () {
			formContainer.hide();
			return false;
		}

		//	---------------------------------------------------------------------------------------------------------
		//	Disconnect
		//	---------------------------------------------------------------------------------------------------------
		formSalesforce.q('input.button.red')[0].onclick = function () {
			new arc.ajax(base_url + 'disconnect-products/?product=salesforce', {
				callback: function (data){
					formContainer.hide();
					moduleSettingsProductCRMSystem.onload(context, params);
					showAlert('success', 'Disconnected successfully.');
				}
			});
		}
		formSalesforce.q('input.button.cancel.gray')[0].onclick = function () {
			formContainer.hide();
			return false;
		}

		//	---------------------------------------------------------------------------------------------------------
		//	Disconnect
		//	---------------------------------------------------------------------------------------------------------
		formProsperWorks.q('input.button.red')[0].onclick = function () {
			new arc.ajax(base_url + 'disconnect-products/?product=prosperworks', {
				callback: function (data){
					formContainer.hide();
					moduleSettingsProductCRMSystem.onload(context, params);
					showAlert('success', 'Disconnected successfully.');
				}
			});
		}
		formProsperWorks.q('input.button.cancel.gray')[0].onclick = function () {
			formContainer.hide();
			return false;
		}

		//	---------------------------------------------------------------------------------------------------------
		//	Disconnect
		//	---------------------------------------------------------------------------------------------------------
		formSugarCRM.q('input.button.red')[0].onclick = function () {
			new arc.ajax(base_url + 'disconnect-products/?product=sugar', {
				callback: function (data){
					formContainer.hide();
					moduleSettingsProductCRMSystem.onload(context, params);
					showAlert('success', 'Disconnected successfully.');
				}
			});
		}
		formSugarCRM.q('input.button.cancel.gray')[0].onclick = function () {
			formContainer.hide();
			return false;
		}

		context.q('input[type="button"]')[0].onclick = function () {
			formContainer.show(toggleSystem());
			return false;
		}
		//	-----------------------------------------------------------------------------------------------
		//	Everything Initialized
		//	Release renderer to re-draw DOM
		if (typeof loaded == 'function')
			loaded();
		//	-----------------------------------------------------------------------------------------------
	}
};

function toTitleCase(str){
	return str.replace(/(^\w{1}|\.\s*\w{1})/gi, function (txt) {
		return txt.toUpperCase();
	});
}
