
var pageContent, app, activeFrame, scrollTarget = 0;
pageContent = q('.page-content')[0];
app = q('#app')[0];
var latestDate = new Date();

window.onscroll = function(){
	scrollTarget = document.body.parentNode.scrollTop;
}

function scroll(){
	activeFrame = q('.page-content .content.active, .page-content [xclass="content"].active');
	if (activeFrame.length > 0)
		app.style.height = (activeFrame[activeFrame.length-1].offsetHeight + 160)+'px';
	//
	pageContent.style.top = '-'+scrollTarget+'px';
	requestAnimationFrame(scroll);
}
scroll();

var sidebar = document.querySelectorAll('div.sidebar')[0];
document.querySelectorAll('.header .fa.fa-bars')[0].onclick = function(){
	if (sidebar.className.indexOf('active') == -1){
		sidebar.addClass('active');
		sidebar.focus();
	}
}
document.querySelectorAll('div.page-content')[0].onclick = function(){
	sidebar.removeClass('active');
}
