
var loader, loadDetails, progress, app, allLoadingItems, progressiveLoaded = 0, loadProgress = 0, GoogleChartsLoaderItems = 0;
var progressiveSubLoaded = 0;
var takingTooLongP, takingTooLongTimeout = false;
var timestamp = 0;//parseInt((new Date()).getTime() / 60000).toString(32);

document.onreadystatechange = function(e){
	if (/loaded|complete/.test(document.readyState)){
		allLoadingItems = document.querySelectorAll('script[data-src], link[rel="stylesheet"][data-href], img[data-src]');
		loader = document.getElementById('loader');
		progress = document.getElementById('progress');
		app = document.getElementById('app');
		loadDetails = document.getElementById('loadDetails');
		progress.style.width = '0%';
		//
		for (var i = 0; i < allLoadingItems.length; i++){
			allLoadingItems[i].onload = progressiveScriptLoaded;
			allLoadingItems[i].onerror = progressiveScriptError;
			if (allLoadingItems[i].tagName == 'IMG'){
				allLoadingItems[i].setAttribute('src', allLoadingItems[i].getAttribute('data-src')+'?v='+timestamp);
			}
			else if (allLoadingItems[i].tagName == 'SCRIPT')
				allLoadingItems[i].setAttribute('src', allLoadingItems[i].getAttribute('data-src')+'?v='+timestamp);
			else
				allLoadingItems[i].setAttribute('href', allLoadingItems[i].getAttribute('data-href')+'?v='+timestamp);
		}
		resetTakingTooLongMsg();
	}
}

// Show mssage if taking too long to load, and recommend using Chrome if not Chrome
function resetTakingTooLongMsg(){
	if (takingTooLongP && takingTooLongP.parentNode != null)
		takingTooLongP.parentNode.removeChild(takingTooLongP);
	clearTimeout(takingTooLongTimeout);
	takingTooLongTimeout = setTimeout(function(){
		takingTooLongP = loader.appendChild(document.createElement('p'));
		takingTooLongP.innerHTML = 'It is taking too long to load Prodoscore.<br/>Please check your internet connection.'+
			(!!window.chrome ? '' :// && !!window.chrome.webstore
				'<br/><br/>Prodoscore recommends using Google Chrome browser.<br/>'+
				'You may download Chrome from link given below.<br/>'+
				'<a href="https://www.google.com/chrome/" target="_blank"><img src="'+static_url+'img/chrome.png" /><br/><span>Chrome</span></a>');
	}, 3600);
}

function progressiveScriptError(e){
	console.log(e);
	//progressiveLoaded += 1;
	loadDetails.innerHTML += '[&#x2717;] '+(this.getAttribute('src') || this.getAttribute('href'))+'<br/>';
	//alert('Error loading the following resource:\n'+(this.getAttribute('src') || this.getAttribute('href'))+'\n\nPlease check your internet connection.');
	var err = loader.appendChild(document.createElement('p'));
	err.innerHTML = 'Error loading the following resource: '+(this.getAttribute('src') || this.getAttribute('href'))+'<br/>Please check your internet connection.';
	err.className = 'error'
}
function displayProgress(){
	loadProgress = 100 * (progressiveLoaded + progressiveSubLoaded/40) / (allLoadingItems.length+10);//+9
	progress.style.width = loadProgress + '%';
	progress.innerHTML = loadProgress.toFixed() + '%';
}
//var preloadLatch = false;
function progressiveScriptLoaded(e){
	progressiveLoaded += 1;
	displayProgress();
	//if (e.path != 'undefined' && e.path.length > 0 && typeof e.path[0].src != 'undefined' && e.path[0].src.endsWith('ajax.js'))
	/*if (!preloadLatch && typeof doFetchData != 'undefined' && typeof ajax != 'undefined'){
		preloadLatch = true;
		doFetchData('preload', function(){progressiveScriptLoaded({});});
	}*/
	//*
	if (progressiveLoaded == allLoadingItems.length-1){// - 2
		clearTimeout(takingTooLongTimeout);
		initializeProdoScore();
	}
	else if (progressiveLoaded == allLoadingItems.length){
		setTimeout(
			function(){
				progressiveLoaded += 1;
				displayProgress();
				//
				loader.style.opacity = 0;
				app.style.opacity = 0;
				app.style.display = 'block';
				setTimeout(
					function(){
						loader.style.display = 'none';
						/*/	//	Moved to login.html footer
						var isChrome = !!window.chrome;// && !!window.chrome.webstore
						var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
						//
						if (isChrome == false && isSafari == false) {
							var popup = document.getElementById('supportedBrowsers'),
								dismiss = popup.q('.dismiss')[0];
							//
							popup.style.display = 'block';
							document.body.classList.add('overlay');
							//
							dismiss.onclick = function () {
								popup.style.display = 'none';
								document.body.classList.remove('overlay');
							}
						}
						//*/
					}, 600);
				setTimeout(
					function(){
						app.style.opacity = 1;
					}, 10);
			}, 3200);
	}
	else
		resetTakingTooLongMsg();
	//*/
}
