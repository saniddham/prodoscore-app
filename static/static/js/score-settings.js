
function showScoreSettings(context, params){
	dateFilters.hide();
	eventLogger('start', 'scoreSettings');
	//
	var prodWeights = {};
	var form1 = context.q('form')[0];
	var weights = context.q('.weights')[0];
	var chkCustomWgts = form1.q('input[type="checkbox"]')[0];
	var rngWeights = {};
	//
	// =========================================================================================
	ajax(base_url+'org-prod-weights/', {
		callback: function(data){
			prodWeights = JSON.parse(data.responseText);
			//
			weights.innerHTML = '';
			var tweight, hasOrgWgts = false;
			for (var pcode in products)
				if (typeof prodWeights[pcode] != 'undefined'){
					tweight = prodWeights[pcode].org;
					var li = elem('li', '<label class="label left-label ">'+products[pcode][1]['name-2']+'</label>'+
								'<input type="range" min="1" max="24" name="weight:'+pcode+'" value="'+tweight+'" />'+
								'<span class="range-label">'+tweight+'</span>',
								{'class': 'col col-6 input-wrap'});
					weights.appendChild(li);
					//
					rngWeights[pcode] = li.q('input[type="range"]')[0];
					rngWeights[pcode].onmousemove = function(){
						if (this.parentNode.q('span')[0].innerHTML != this.value){
							this.parentNode.q('span')[0].innerHTML = this.value;
							chkCustomWgts.checked = true;
						}
					}
					//
					if (prodWeights[pcode].org != prodWeights[pcode]['default'])
						hasOrgWgts = true;
				}
			chkCustomWgts.checked = hasOrgWgts;
		}
	});
	chkCustomWgts.onclick = function(){
		for (pcode in rngWeights){
			rngWeights[pcode].value = this.checked ? prodWeights[pcode].org : prodWeights[pcode]['default'] ;
			rngWeights[pcode].parentNode.q('span')[0].innerHTML = rngWeights[pcode].value;
		}
	}
	form1.onsubmit = function(){
		eventLogger('start', 'saveScoreSettings');
		if (!chkCustomWgts.checked){
			var formData = new FormData();
			formData.append('delete', 'all');
		}
		ajax(base_url+'save-org-weights/', {
			method: 'POST',
			data: chkCustomWgts.checked ? this : formData,
			callback: function(data){
				scrollToTop();
				showAlert('success', 'Organization product weights are saved.', 4000);
				eventLogger('end', 'saveScoreSettings');
				for (pcode in rngWeights)
					prodWeights[pcode].org = rngWeights[pcode].value;
			}
		});
		return false;
	};
	// =========================================================================================
	var form2 = context.q('form')[1];
	var ulRoles = context.q('.roles')[0];
	var chkOrgBaselines = form2.q('input[type="checkbox"]')[0];
	//
	var progress = [0, 0];
	var checkpoint = function(){
		progress[1] += 1;
		if (progress[0] == progress[1]){
			eventLogger('end', 'scoreSettings');
		}
	};
	ajax(base_url+'api/sub-roles/', {
		callback: function(data){
			data = JSON.parse(data.responseText);
			//
			ulRoles.innerHTML = '';
			for (var i = 0; i < data.roles.length; i++)
				if (data.roles[i].id > 0){
					progress[0] += 1;
					// -------------------------------------------------------
					new (function(role){
						var li = elem('li', '<h2 class="card-heading">'+role.title+' team</h2>'+
									'<div class="form-wrap"><ul class="baselines-box">Loading...</ul></div>',
									{'class': 'card-wrap col col-4', 'data-id': role.id});
						var ul = li.q('ul')[0];
						ulRoles.appendChild(li);
						ajax(base_url+'role-baselines/?role='+role.id, {
							callback: function(data, li){
								data = JSON.parse(data.responseText);
								ul.innerHTML = '';
								if (Object.keys(data.products) == 0){
									ul.parentNode.parentNode.style.display = 'none';
								}
								//
								else{
									for (var pcode in data.products){
										var li = elem('li', '<b>'+products[pcode][1]['name-2']+'</b>'+
													'<span title="Comparison with Gobal Average" class="f-right'+(data.products[pcode].delta == 0.0 ?
														'">0' : ' delta '+(data.products[pcode].delta > 0 ? 'increase' : 'decrease')+'">'+(data.products[pcode].delta > 0 ? '+' : '')+data.products[pcode].delta.toFixed(0))+'%</span>'+
													'<span title="Your Organization Average" class="f-right">'+data.products[pcode].org.toFixed(2)+'</span>'+
													'<input type="hidden" name="'+role.id+':'+pcode+'" value="'+data.products[pcode].org.toFixed(2)+'" />');
										ul.appendChild(li);
									}
									if (data.has_org_baseline > 0)
										chkOrgBaselines.checked = true;
								}
								checkpoint();
							}
						}, li);
					})(data.roles[i]);
					// -------------------------------------------------------
				}
		}
	});
	form2.onsubmit = function(){
		eventLogger('start', 'saveScoreSettings');
		if (!chkOrgBaselines.checked){
			var formData = new FormData();
			formData.append('delete', 'all');
		}
		ajax(base_url+'save-org-baselines/', {
			method: 'POST',
			data: chkOrgBaselines.checked ? this : formData,
			callback: function(data){
				scrollToTop();
				showAlert('success', (chkOrgBaselines.checked ? 'Organization product baselines are selected.' : 'Global baselines have been selected.'), 4000);
				eventLogger('end', 'saveScoreSettings');
			}
		});
		return false;
	};
	// =========================================================================================
}
