
/*
* Arc Reactor JavaScript library/framework
* https://github.com/vishva8kumara/arc-reactor
* Distributed Under MIT License
* See index.html for the demonstration;
* 	refer to the embedded JS there for documentation.
*
* Font-Awsome and Roboto Font is redistributed with this under
* their respective licenses, and not covered under this MIT license.
*
*/

var arrayIgnore = ['min', 'max', 'sum', 'avg'];
var navigationTable = {};
//var navigationLatch = false;
var navigationActiveStack = [];

function navFrame(hash, obj, callback, kcabllac){
	navigationTable[hash] = [obj, callback, kcabllac];
	obj.style.display = 'none';
}

window.onpopstate = function(event){
	//	Process difference of states
	var tmpStack = [];
	var onLoadCallCandidates = [];
	hash = document.location.hash.replace('#', '').split('/');
	for (var i = 0; i < hash.length; i++){
		var path = hash.slice(0, i+1).join('/');
		if (typeof navigationTable[path] != 'undefined'){
			tmpStack.push(navigationTable[path].concat([hash.slice(i+1).join('/')]));
			//	List candidate onload functions - we are calling only the last
			if (typeof navigationTable[path][1] == 'function' && (!existInActiveStack(navigationTable[path][0], hash.slice(i+1).join('/')) || (typeof event != 'undefined' && typeof event.forcePop != 'undefined')))
				onLoadCallCandidates.push([navigationTable[path], hash.slice(i+1)]);
				//navigationTable[path][1](navigationTable[path][0], hash.slice(i+1));
		}
	}
	var waitLoading = false;
	var waitCallback = function(){
		//	Show frames to be active
		navigationActiveStack = [];
		for (var i = 0; i < tmpStack.length; i++){
			tmpStack[i][0].style.display = 'block';
			new function(obj){
				setTimeout(function(){
						obj.addClass('active');
					}, 10);
			}(tmpStack[i][0]);
			navigationActiveStack.push(tmpStack[i]);
		}
	};
	//	Hide frames to be inactive
	for (var i = 0; i < navigationActiveStack.length; i++)
		if (!existInStack(tmpStack, navigationActiveStack[i])){
			//	Call onUnload function
			if (typeof navigationActiveStack[i][2] == 'function')
				navigationActiveStack[i][2](navigationActiveStack[i][0], event);
			navigationActiveStack[i][0].removeClass('active');
			new function(obj){
				setTimeout(function(){
						obj.style.display = 'none';
					}, 250);
			}(navigationActiveStack[i][0]);
		}
	//
	//	Call onload function
	if (onLoadCallCandidates.length > 0){
		onLoadCallCandidates = onLoadCallCandidates[onLoadCallCandidates.length - 1];
		/*if (typeof event == 'undefined')
			event = {};
		event.target = event.currentTarget = event.srcElement = onLoadCallCandidates[0][0];*/
		if (onLoadCallCandidates[0][1].length == 4){
			onLoadCallCandidates[0][1](onLoadCallCandidates[0][0], onLoadCallCandidates[1], event, waitCallback);
			waitLoading = true;
		}
		else{
			onLoadCallCandidates[0][1](onLoadCallCandidates[0][0], onLoadCallCandidates[1], event);
		}
		var charts = onLoadCallCandidates[0][0].querySelectorAll('.chart');
		for (var c = 0; c < charts.length; c++)
			charts[c].innerHTML = loadingIndicator;
		if (typeof ga == 'function')
			ga('send', 'pageview', document.location.pathname+document.location.hash);//document.location.href
	}
	//	Display frames right-away
	if (!waitLoading)
		waitCallback();
	//
	document.body.scrollLeft = 0;
	document.body.scrollTop = 0;
	//
	//	Set classname for the parent of active anchor
	var anchors = document.querySelectorAll('nav a[href^=\\#], ul.tabs a[href^=\\#]');
	for (var i = 0; i < anchors.length; i++){
		if (document.location.hash.substring(1).startsWith(anchors[i].href.split('#')[1]))
			anchors[i].parentNode.addClass('loading-m');
		else
			anchors[i].parentNode.removeClass('loading-m');
	}
	setTimeout(function(){
		var anchors = document.querySelectorAll('nav a[href^=\\#], ul.tabs a[href^=\\#]');
		for (var i = 0; i < anchors.length; i++){
			if (document.location.hash.substring(1).startsWith(anchors[i].href.split('#')[1]))
				anchors[i].parentNode.addClass('active');
			else
				anchors[i].parentNode.removeClass('active');
		}
	}, 125);
};
function existInActiveStack(dom, params){
	for (var i = 0; i < navigationActiveStack.length; i++)
		if (navigationActiveStack[i][0] == dom && navigationActiveStack[i][3] == params)
			return true;
	return false;
}
function existInStack(tmpStack, obj){
	for (var i = 0; i < tmpStack.length; i++)
		if (tmpStack[i][0] == obj[0])
			return true;
	return false;
}
//window.addEventListener('load', window.onpopstate);
window.addEventListener('keyup',
	function(e){
		var evt = e || window.event;
		if (evt.keyCode == 27)
			history.back();
	});
document.addEventListener('backbutton',
	function(){
		history.back();
	}, false);
//function addClassDelayed(obj, className, delay){}

//	Make Ajax requests
function ajax(url, options, ref){
	if (this == window){
		return new ajax(url, options);
		//false;
	}
	var _this = this;
	this.method = 'GET';
	this.data = null;
	this.callback = function(data){console.log(data);};
	this.failback = function(data){console.log(data);};
	this.headers = {};
	this.evalScripts = false;
	this.doCache = false;
	var opts = {'method': 'method', 'type': 'method', 'data': 'data', 'form': 'data', 'callback': 'callback', 'success': 'callback',
			'failback': 'failback', 'fallback': 'failback','error': 'failback', 'progress': 'progress', 'onprogress': 'progress', 'url': 'url', 'uri': 'url',
			'headers': 'headers', 'evalScripts': 'evalScripts', 'eval': 'evalScripts', 'doCache': 'doCache', 'cache': 'doCache'};
	if (typeof url == 'object' && (typeof url['url'] != 'undefined' || typeof url['uri'] != 'undefined')){
		ref = options;
		options = url;
		url = options['url'] || options['uri'];
	}
	for (var key in options)
		//if (arrayIgnore.indexOf(key) == -1)
		if (typeof opts[key] != 'undefined')
			this[opts[key]] = options[key];
		else
			console.log('Ajax: Unrecognized option \''+key+'\' with value: '+options[key]);
	//
	this.abort = function(){
		_this.failback = function(a, b){};
		_this.xmlhttp.abort();
	}
	this.deliverResult = function(data){
		if (typeof _this.callback == 'function')
			_this.callback(data, ref);
		else if (typeof _this.callback == 'object' && _this.callback.toString().indexOf("Element") > -1)
			_this.callback.innerHTML = data.responseText;
		else{
			var elem = document.getElementById(_this.callback);
			if (typeof elem == 'object' && elem.toString().indexOf("Element") > -1)
				elem.innerHTML = data.responseText;
			else
				console.log('Ajax: Callback \''+_this.callback+'\' is neither function, nor object or an ID of an object.');
		}
		//
		if (typeof _this.evalScripts != 'undefined'){
			var P0 = data.responseText.indexOf('<script');
			while (P0 > -1){
				var P0 = data.responseText.indexOf('>', P0) + 1;
				var P1 = data.responseText.indexOf('</script', P0);
				if (_this.evalScripts != false)
					_this.evalScripts(data.responseText.substring(P0, P1));
				else
					try{
						eval(data.responseText.substring(P0, P1));
					}
					catch (e){
						console.log(e);
					}
				P0 = data.responseText.indexOf('<script', P1);
			}
		}
	}
	//
	if (this.doCache === true && typeof localStorage[url] != 'undefined'){
		_this.deliverResult({responseText: localStorage[url], response: localStorage[url], responseURL: url, status: 200, statusText: 'From Cache'});
		return true;
	}
	else if (this.doCache == false)
		localStorage.removeItem(url);
	//
	if (window.XMLHttpRequest)
		this.xmlhttp = new XMLHttpRequest();
	else if (window.ActiveXObject)
		this.xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	//
	this.xmlhttp.open(_this.method.toUpperCase(), url, true);
	this.xmlhttp.onreadystatechange = function () {
		if (this.readyState == 4){
			if (this.status == 200){
				_this.deliverResult(this);
				if (_this.doCache != false)
					localStorage[url] = this.responseText;
			}
			else if (typeof _this.failback == 'function')
				_this.failback(this, ref);
		}
	};
	if (typeof _this.progress == 'function')
		this.xmlhttp.onprogress = function(data){_this.progress(data.total < data.loaded ? 100 : 100 * data.loaded / data.total, data, ref);};
	//
	if (_this.method.toUpperCase() == "POST"){
		var params = '';
		if (_this.data == null){}
		else if (_this.data.toString().indexOf("Form") > -1 && (typeof _this.data.tagName != 'undefined' && _this.data.tagName == "FORM")){
			for (var i = 0; i < _this.data.elements.length; i++) {
				if (_this.data.elements[i].type == "checkbox")
					params += _this.data.elements[i].checked ? (params == '' ? '' : '&') + _this.data.elements[i].name + '=on' : '';
				else if (_this.data.elements[i].type == "radio")
					params += _this.data.elements[i].checked ? (params == '' ? '' : '&') + _this.data.elements[i].name + '=' + encodeURIComponent(_this.data.elements[i].value) : '';
				else
					params += (params == '' ? '' : '&') + _this.data.elements[i].name + '=' + encodeURIComponent(_this.data.elements[i].value);
			}
			//params = _this.data;
			this.xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
		}
		else if (_this.data.constructor.name == 'FormData'){
			//this.xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded");
			params = _this.data;
		}
		else if (typeof _this.data == 'object'){
			params = JSON.stringify(_this.data);
			this.xmlhttp.setRequestHeader("content-type", "application/json");
		}
		else
			params = _this.data;
		//
		if (typeof _this.headers != 'undefined')
			for (var key in _this.headers)
				this.xmlhttp.setRequestHeader(key, _this.headers[key]);
		if (csrftoken != false)
			this.xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
		this.xmlhttp.send(params);
	}
	else
		this.xmlhttp.send(null);
	//return this.xmlhttp;//true
}
var cookies = document.cookie.split(';');
var csrftoken = false;
for (var i = 0; i < cookies.length; i++){
	var cookie = cookies[i].trim().split('=');
	if (cookie[0] == 'csrftoken')
		csrftoken = cookie[1];
}


// ------------------------------------------------------------------------------------
//	DOM GEN
// ------------------------------------------------------------------------------------

//	Create a Dom Element of given type, innerHTML and options
function elem(tagname, innerHTML, options){
	var obj = document.createElement(tagname);
	if (typeof innerHTML !== 'undefined' && innerHTML != null && innerHTML != false)
		obj.innerHTML = innerHTML;
	if (typeof options !== 'undefined')
		for (var key in options)
			obj.setAttribute(key, options[key]);
	return obj;
}

//	Read DOM tree and generate JSON
function arcRead(dom, index){
	var obj = {};
	if (typeof index == 'undefined')
		var index = {};
	for (var i = 0; i < dom.attributes.length; i++){
		obj[dom.attributes[i].name] = dom.attributes[i].value;
		if (dom.attributes[i].value.substring(0, 2) == '[[' && dom.attributes[i].value.substr(-2) == ']]')
			index[dom.attributes[i].value.replace('[[', '').replace(']]', '')] = [obj, dom.attributes[i].name];
	}
	if (dom.children.length == 0){
		obj.content = dom.innerHTML;
		if (dom.innerHTML.substring(0, 2) == '[[' && dom.innerHTML.substr(-2) == ']]')
			index[dom.innerHTML.replace('[[', '').replace(']]', '')] = [obj, 'content'];
	}
	else if (dom.children.length == 1){
		var res = arcRead(dom.children[0], index);
		obj.content = res[0];
		//index = index.concat(res[1]);
	}
	else{
		obj.content = [];
		for (var i = 0; i < dom.children.length; i++){
			var res = arcRead(dom.children[i], index);
			obj.content.push(res[0]);
			//index = index.concat(res[1]);
		}
	}
	var output = {};
	output[dom.tagName.toLowerCase()] = obj;
	return [output, index];
}

//	Generate DOM tree from JSON
function arcReact(data, schema){
	for (var key in schema[1])
		if (arrayIgnore.indexOf(key) == -1)
			schema[1][key][0][schema[1][key][1]] = data[key];
	return arcTree(schema[0]);//Object.assign({}, schema)
}

//	Generate a DOM tree from a JavaScript object or JSON Schema/Object
//	This is depricated - Use arc.js instead for new implimentations.
function arcReactor(data){
	var obj;
	for (var tagname in data){
		obj = document.createElement(tagname);
		for (var key in data[tagname]){
			var value = data[tagname][key]
			if (key == 'content'){
				if (typeof value == 'string' || typeof value == 'number')
					obj.innerHTML = value;
				else if (Array.isArray(value))
					for (var i = 0; i < value.length; i++)
						obj.appendChild(arcReactor(value[i]));			//	Recursion point
				else if (value instanceof HTMLElement)
					obj.appendChild(value);
				else if (typeof value == 'object')
					obj.appendChild(arcReactor(value));				//	Recursion point
			}
			else if (key.substring(0, 2) == 'on')
				obj[key] = value;				//	Event handler - Sugesstion: Check if a function instead
			else
				obj.setAttribute(key, value);
		}
	}
	return obj;
}

//	Generate DOM tree from JSON
function arcTree(data){
	var obj;
	for (var tagname in data){
		obj = document.createElement(tagname);
		for (var key in data[tagname]){
			var value = data[tagname][key]
			if (key == 'content'){
				if (typeof value == 'string' || typeof value == 'number')
					obj.innerHTML = value;
				else if (typeof value == 'object')
					if (typeof value.length == 'undefined')
						obj.appendChild(arcTree(value));
					else
						for (var i = 0; i < value.length; i++)
							obj.appendChild(arcTree(value[i]));
			}
			else if (typeof value == 'function' /*key.substring(0, 2) == 'on'*/)
				obj[key] = value;
			else
				obj.setAttribute(key, value);
		}
	}
	return obj;
}

//	Generate HTML Table from JSON data and a JSON schema
function arcTbl(data, schema){
	var table = elem('table', false, {class: 'table-striped', width: '100%'});
	var tr = table.appendChild(elem('tr', false, {'data-id': 'head'}));
	for (var i = 0; i < schema.length; i++){
		if (typeof schema[i].type != 'undefined'){
			if (schema[i].type == 'numeric')
				tr.appendChild(elem('th', schema[i].title, {align: 'right'}));
			else
				tr.appendChild(elem('th', schema[i].title));
		}
		else
			tr.appendChild(elem('th', schema[i].title));
	}
	var tmp;
	for (var i = 0; i < data.length; i++){
		tr = table.appendChild(elem('tr', false, {'data-id': (data[i].id != undefined ? data[i].id : '')}));
		for (var j = 0; j < schema.length; j++){
			tmp = data[i][schema[j].name];
			if (schema[j]['enum'] != undefined && typeof schema[j]['enum'][tmp] != 'undefined')
				tmp = schema[j]['enum'][tmp];
			if (typeof schema[j].type != 'undefined'){
				if (schema[j].type == 'numeric')
					tr.appendChild(elem('td', tmp+'&nbsp;', {align: 'right'}));
				else
					tr.appendChild(elem('td', tmp+'&nbsp;'));
			}
			else
				tr.appendChild(elem('td', tmp+'&nbsp;'));
		}
	}
	return table;
}

// ------------------------------------------------------------------------------------

if ( !window.requestAnimationFrame ) {
	window.requestAnimationFrame = ( function() {
		return window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame ||
		function(callback, element) {
			window.setTimeout( callback, 1000 / 60 );
		};
	} )();
}

Array.prototype.max = function() {
	return Math.max.apply(null, this);
};

Array.prototype.min = function() {
	return Math.min.apply(null, this);
};

Array.prototype.sum = function(){
	for(var total = 0,l=this.length;l--;total+=(1*this[l]));
	return total;
}

Array.prototype.avg = function(){
	for(var total = 0,l=this.length;l--;total+=(1*this[l]));
	return this.length == 0 ? '-' : total / this.length;
}

Float32Array.prototype.max = function() {
	return Math.max.apply(null, this);
};

Float32Array.prototype.min = function() {
	return Math.min.apply(null, this);
};

Date.prototype.sqlFormatted = function() {
	var yyyy = this.getFullYear().toString();
	var mm = (this.getMonth()+1).toString();
	var dd  = this.getDate().toString();
	return yyyy +'-'+ (mm[1]?mm:"0"+mm[0]) +'-'+ (dd[1]?dd:"0"+dd[0]);
};

HTMLElement.prototype.addClass = function(classname){
	this.className = this.className.replace(new RegExp(classname, 'g'), '').trim()+' '+classname;
}

HTMLElement.prototype.removeClass = function(classname){
	this.className = this.className.replace(new RegExp(classname, 'g'), '').trim();
}

HTMLElement.prototype.q = function(selector){
	return this.querySelectorAll(selector);
}

function q(selector){
	return document.querySelectorAll(selector);
}

function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}


//	In a better world - we can drop the following..

if ('ab'.substr(-1) != 'b'){
	String.prototype.substr = function(substr){
		return function(start, length){
			return substr.call(this, start < 0 ? this.length + start : start, length)
		}
	}(String.prototype.substr);
}

if (!String.prototype.trim){
	String.prototype.trim = function (){
		return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
	};
}

/*/
if (typeof HTMLElement.prototype.innerText == 'undefined'){
	HTMLElement.prototype.innerText = function (){
		return this.innerHTML.strip().stripTags().replace(/\n/g,'').replace(/\s{2}/g,'');
	};
}
//*/

// ------------------------------------------------------------------------------------

setTimeout(console.log('%c{ArcReactor.js}', 'font-weight:bold; font-size:14pt; color:#204080;'), 10);
setTimeout(console.log('Loaded and Ready...\n\n'), 10);
setTimeout(console.log('%cThis is a browser feature intended for developers. Do not paste code you receive from strangers here.', 'color:#A84040;'), 10);
