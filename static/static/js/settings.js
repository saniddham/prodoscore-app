function hidePrevAlerts() {
	var alertContainer = document.getElementById('messages');
	alertContainer.innerHTML = "";
}

var empSettingsData = {};

function employeeSettingsRows(employees, datatable, employees4Mgr, mgrDropdowns, mgrIndex, mgrIndexAll, self){
	var rows = [], newCount = 0, termCount = 0, employeesShortlist = [];
	var hasChanges = [];
	var showHideSaveCancelButtons = function(input, status, tfoot, index){
		if (typeof index == 'undefined')
			index = 0;
		var row = input.parentNode.parentNode.rowIndex;
		var cell = input.parentNode.cellIndex+index;
		if (typeof hasChanges[row] == 'undefined')
			hasChanges[row] = [];
		hasChanges[row][cell] = status;
		//row = row.parentNode.indexOf(row);
		//console.log(JSON.stringify(hasChanges));
		tfoot.style.display = hasChanges.toString().indexOf('true') > -1 ? 'block' : 'none';
	};
	for (var i in employees) {
		if (employees4Mgr == -1 || employees[i].manager == employees4Mgr) {
			//
			if (self.role == 80) {
				var tmpDropdown = makeDropdown({}, employees[i].manager, function (input) {
					input.parentNode.parentNode.setAttribute('data-manager', input.value);
					var managerIsSetInFilter = false;
					for (var i = filterByManager[1].childNodes.length-1; i > -1; i--){
						var managerOption = filterByManager[1].childNodes[i];
						if (managerOption.value == input.value){
							managerIsSetInFilter = true;
							break;
						}
					}
					//
					if (!managerIsSetInFilter){
						for (var i = input.childNodes.length-1; i > -1; i--){
							var optionManager = input.childNodes[i];
							if (optionManager.value == input.value){
								filterByManager[1].appendChild(elem('option', optionManager.innerHTML, {
									value: input.value
								}));
							}
						}
					}
					//
					showHideSaveCancelButtons(input, input.className=='updated', datatable.parentNode.tFoot);
				}, 52);

				if (employees[i].role <= 0 ) {
					tmpDropdown.setAttribute('disabled', true);
				}
				mgrDropdowns.push(tmpDropdown);
			}
			else
				var tmpDropdown = typeof employees[employees[i].manager] != 'undefined' ? employees[employees[i].manager].fullname : '';
			//
			//if ((self.role == 80) || (employees[i].manager == uid || (typeof employees[employees[i].manager] != 'undefined' && employees[employees[i].manager].manager == uid))) {
			if (self.role == 80 || employees[uid].manager != i) {
				var email = employees[i].report_email;
				/*if ((employees[i].report_email == null || employees[i].report_email == '') && employees[i].is_app_user == 0 && employees[i].email.indexOf('@') > -1)
					email = employees[i].email;*/
				if (email == null)
					email = '';
				icon = usernameIcon(employees[i].fullname);
				chk = ('00' + employees[i].report_enabled).split('').reverse();
				rolesDropDown = (self.role < 80 ? roles[employees[i].role] :
					sortList(makeDropdown(roles, employees[i].role,
						function (input, e) {
							if (input.parentNode.parentNode.getAttribute('data-role') > 69 && input.value < 70){
								if (!confirm('Are you sure?\nChanging this employee\'s user role will reset the manager name to "none" for any employee who currently reports to this employee.')){
									e.cancelBubble = true;
									e.stopPropagation();
									e.preventDefault();
									input.value = input.parentNode.parentNode.getAttribute('data-role');
									return false;
								}
							}
							//
							var row = input.parentNode.parentNode;
							var mgr_id = row.getAttribute('data-id');
							var mgr_name = row.q('p.user-name')[0].innerHTML;
							var checkboxes = row.q('input[type="checkbox"]');
							var row_status_dd = row.q('select')[2];
							var row_mgr = row.q('select')[1];
							// Disable Daily Reports checkbox on admin

							if (input.value != 0){
								row.setAttribute('new-employee-change', true);
							}

							if (input.value == 80){
								checkboxes[1].setAttribute("disabled", true);
								checkboxes[1].checked = false;
							} else{
								checkboxes[1].removeAttribute("disabled");
							}

							// toggle disabled attribute of status dropdown according to role
							if (input.value > 0) {
								row_status_dd.removeAttribute("disabled");
								row_status_dd.value = row_status_dd.getAttribute('data-value');
								row_mgr.removeAttribute('disabled');
								//row_mgr.value = row_mgr.getAttribute('data-value');
							}
							else {
								row_status_dd.setAttribute("disabled", true);
								row_status_dd.value = -1;
								row_mgr.setAttribute('disabled', true);
								row_mgr.value = 52;
							}
							row_status_dd.className = row_status_dd.value == row_status_dd.getAttribute('data-value') ? '' : 'updated';
							//
							//	Add manager to all dropdowns to be selected as a manager
							if (input.value > 69) {
								for (var k = 0; k < mgrDropdowns.length; k++) {
									mgrDropdowns[k].appendChild(elem('option', mgr_name, {
										'value': mgr_id
									}));
									if (mgrDropdowns[k].getAttribute('data-value') == mgr_id){
										mgrDropdowns[k].value = mgr_id;
										mgrDropdowns[k].className = '';
									}
									var parentRow = mgrDropdowns[k].parentNode.parentNode;
									var parentRowId = parentRow.getAttribute("data-id");
									if (parentRowId == mgr_id){
										for (var i = mgrDropdowns[k].childNodes.length-1; i > -1; i--){
											var optionValue = mgrDropdowns[k].childNodes[i].value;
											if (typeof optionValue != 'undefined' && optionValue == mgr_id){
												mgrDropdowns[k].childNodes[i].setAttribute('disabled', true);
											}
										}
									}
								}
							}
							//
							//	Remove manager from all dropdowns
							if (input.parentNode.parentNode.getAttribute('data-role') > 69){
								for (var k = 0; k < mgrDropdowns.length; k++) {
									if (mgrDropdowns[k].value == mgr_id){
										mgrDropdowns[k].value = 52;
										mgrDropdowns[k].addClass('updated');
									}
									mgrDropdowns[k].removeChild(mgrDropdowns[k].q('[value="' + mgr_id + '"]')[0]);
								}
								// var testAttr = filterByManager[1].
								for (var i = filterByManager[1].childNodes.length-1; i > -1; i--){
									var managerOption = filterByManager[1].childNodes[i];
									if (managerOption.value == mgr_id){
										filterByManager[1].removeChild(managerOption);
									}
								}
							}
							//
							input.parentNode.parentNode.setAttribute('data-role', input.value);
							showHideSaveCancelButtons(input, input.className=='updated', datatable.parentNode.tFoot);
						}, -1), -1, false));
				if (uid == i && typeof rolesDropDown != 'string') {
					rolesDropDown.disabled = true;
				}

				// set visibility status to -1 if role = system, terminated, or deactivated
				var status_dropdown = makeDropdown(
					statuses,
					(employees[i].role > 0 ? employees[i].status : -1),
					function (input) {
						var row = input.parentNode.parentNode;
						var mgr_id = row.getAttribute('data-id');
						//
						//	Add manager to all dropdowns to be selected as a manager
						if (input.value == 1) {
							for (var k = 0; k < mgrDropdowns.length; k++) {
								/*mgrDropdowns[k].appendChild(elem('option', mgr_name, {
									'value': mgr_id
								}));*/
								if (mgrDropdowns[k].getAttribute('data-value') == mgr_id){
									mgrDropdowns[k].value = mgr_id;
									mgrDropdowns[k].className = '';
								}
								/*var parentRow = mgrDropdowns[k].parentNode.parentNode;
								var parentRowId = parentRow.getAttribute("data-id");
								if (parentRowId == mgr_id){
									for (var i = mgrDropdowns[k].childNodes.length-1; i > -1; i--){
										var optionValue = mgrDropdowns[k].childNodes[i].value;
										if (typeof optionValue != 'undefined' && optionValue == mgr_id){
											mgrDropdowns[k].childNodes[i].setAttribute('disabled', true);
										}
									}
								}*/
							}
						}
						//
						//	Remove manager from all dropdowns
						if (input.parentNode.parentNode.getAttribute('data-status') == 1){
							for (var k = 0; k < mgrDropdowns.length; k++) {
								if (mgrDropdowns[k].value == mgr_id){
									mgrDropdowns[k].value = 52;
									mgrDropdowns[k].addClass('updated');
								}
								//mgrDropdowns[k].removeChild(mgrDropdowns[k].q('[value="' + mgr_id + '"]')[0]);
							}
						}
						//
						input.parentNode.parentNode.setAttribute('data-status', input.value);
						showHideSaveCancelButtons(input, input.className=='updated', datatable.parentNode.tFoot);
					}, -1);

				// disable status_dropdown if role = system, terminated, or deactivated
				if ( employees[i].role <= 0) {
					status_dropdown.setAttribute("disabled", true);
				}

				// new and terminated count
				if (employees[i].role === 0) {
					newCount++;
				} else if (employees[i].role < 0) {
					termCount++;
				}

				var employee = arcReactor({
					tr: {
						class: 'tablerow role-' + ( employees[i].role === 0 ? 'new' : roles[employees[i].role].toLowerCase()),
						'data-id': i,
						'data-role': employees[i].role,
						'data-manager': employees[i].manager,
						'data-new': (employees[i].role === 0),
						'data-status': employees[i].status,
						content: [
							{
								td: {
									content: {
										div: {
											class: 'user-name-wrap',
											content: {
												p: {
													class: 'user-name',
													content: employees[i].fullname
												}
											}
										}
									},
									onclick: function () {
										document.location.hash = 'settings/employees/o/' + this.parentNode.getAttribute('data-id');
									},
									style: 'border-left-color:#' + icon[1]
								}
							},
							{
								td: {
									content: rolesDropDown,
									class: (typeof rolesDropDown == 'string' ? 'has-gutter' : 'no-gutter'),
									'sorttable_customkey': roles[employees[i].role]
								}
							},
							{
								td: {
									content: tmpDropdown,
									class: typeof tmpDropdown == 'string' ? 'has-gutter' : 'no-gutter',
									title: (employees[employees[i].manager] == undefined ? '' : employees[employees[i].manager].fullname),
									'sorttable_customkey': (employees[employees[i].manager] == undefined ? '' : employees[employees[i].manager].fullname)
								}
							},
							{
								td: {
									content: status_dropdown,
									'sorttable_customkey': statuses[(employees[i].role > 0 ? employees[i].status : -1)],
									class: 'no-gutter'
								}
							},
							//
							//	Report Email
							{
								td: {
									content: {
										input: {
											type: 'email',
											value: email,
											'data-value': email, // placeholder: employees[i].email,
											onkeyup: function () {
												this.className = this.value != this.getAttribute('data-value') ? 'updated' : '';
												showHideSaveCancelButtons(this, this.className=='updated', datatable.parentNode.tFoot);
											}
										}
									},
									class: 'no-gutter'
								}
							},
							//
							//	Report Frequency
							{
								td: {
									content: [
										{
											label: {
												content: [
													{
														input: {
															type: 'checkbox',
															'data-value': chk[0] || 0,
															onclick: function () {
																this.parentNode.className = this.checked == this.getAttribute('data-value') == 1 ? 'inp-switch' : 'inp-switch updated';
																showHideSaveCancelButtons(this.parentNode, this.parentNode.className=='inp-switch updated', datatable.parentNode.tFoot, 0);
															}
														}
													},
													{
														span: {
															content: 'Weekly'
														}
													}
												],
												class: 'inp-switch'
											}
										},
										{
											label: {
												content: [
													{
														input: {
															type: 'checkbox',
															'data-value': chk[1] || 0,
															onclick: function () {
																this.parentNode.className = this.checked == this.getAttribute('data-value') == 1 ? 'inp-switch' : 'inp-switch updated';
																showHideSaveCancelButtons(this.parentNode, this.parentNode.className=='inp-switch updated', datatable.parentNode.tFoot, 1);
															}
														}
													},
													{
														span: {
															content: 'Daily'
														}
													}
												],
												class: 'inp-switch'
											}
										}
									],
									class: 'inp-switch-col'
								}
							}
						]
					}
				});
				if (employees[i].role < 1)
					employee.style.display = 'none';
				datatable.appendChild(employee);
				rows.push(employee);
				checkboxes = employee.q('input[type="checkbox"]');
				checkboxes[0].checked = chk[0] == '1';
				checkboxes[1].checked = chk[1] == '1';
				// Disable daily reports if admin
				if ( employees[i].role == 80 ) {
					checkboxes[1].setAttribute("disabled", true);
					checkboxes[1].checked = false;
				}
				found = true;
				employeesShortlist[i] = [employees[i].fullname, icon[1]];
			}
		}
		//if ((self.role == 80 || employees[i].manager == uid || (typeof employees[employees[i].manager] != 'undefined' && employees[employees[i].manager].manager == uid)))
		if (typeof employees[employees[i].manager] != 'undefined' && typeof mgrIndex[employees[i].manager] == 'undefined')
			if (employees[i].role > 0 && employees[i].status == 1 && employees[employees[i].manager].role > 0 && employees[employees[i].manager].status == 1)
				mgrIndex[employees[i].manager] = true;
			//	List all managers - by role
		if (employees[i].status == 1 && (employees[i].role == 80 || (self.role == 80 || employees[i].manager == uid) && employees[i].role == 70))
			mgrIndexAll[i] = true;
	}
	return {newCount: newCount, termCount: termCount, rows: rows};
}

function loadSettingsPage(context, params) {
	dateFilters.hide();
	eventLogger('start', 'loadEmpSettings');

	hidePrevAlerts();
	//
	//date_filter_onchange('yesterday');
	doFetchData('settings', function(data, self, startD, endD){});
	//
	var employees = context.q('.empSettings_users')[0].tBodies[0];
	var data = null;
	var rolesDropDown, mgrDropdowns = [];
	var mgrIndex = {};
	var mgrIndexAll = {};
	if (organizationSettings.myself.role == 70)
		context.q('.employee-filters_right')[0].style.display = 'none';

	employees.innerHTML = '<tr><td colspan="5" style="width: 100%;">Fetching employees, please wait...</td></tr>';
	// clear search
	context.q('input[name="searchEmployee"]')[0].value = '';
	new ajax(base_url + 'update-employee/', {
		callback: function (resp) {
			data = eval('(' + resp.responseText + ')');
			empSettingsData = data;
			var self = organizationSettings.myself;//data.employees[uid];
			var icon, email, chk, checkboxes;

			//
			employees.innerHTML = '';
			filterByManager[1].innerHTML = '<option value="*">[ Filter by ]</option><option value="52">None</option>';
			searchRole[1].value = '*';
			if (Object.keys(mgrIndex).length == 1) {
				employees4Mgr = Object.keys(mgrIndex)[0];
				filterByManager[1].value = employees4Mgr;
			}

			//****************************************************
			//****************************************************
			var counts = employeeSettingsRows(data.employees, employees, employees4Mgr, mgrDropdowns, mgrIndex, mgrIndexAll, self);
			//var newCount = counts.newCount; var termCount = counts.termCount;
			//****************************************************
			//****************************************************
			//
			//context.q('.employee-filters .terminatedToggle .count')[0].innerHTML = '(' + counts.termCount + ')';
			context.q('.employee-filters .newEmpToggle .count')[0].innerHTML = '(' + counts.newCount + ')';

			/*if ( counts.termCount < 1) {
				context.q('.employee-filters input[name="retrieveTerminated"]')[0].setAttribute('disabled', true);
				context.q('.employee-filters input[name="retrieveTerminated"]')[0].classList.remove('has-data');
			} else {
				context.q('.employee-filters input[name="retrieveTerminated"]')[0].removeAttribute('disabled');
				context.q('.employee-filters input[name="retrieveTerminated"]')[0].classList.add('has-data');
			}*/


			if ( counts.newCount < 1) {
				context.q('.employee-filters input[name="showNewEmployees"]')[0].setAttribute('disabled', true);
				context.q('.employee-filters input[name="showNewEmployees"]')[0].classList.remove('has-data');
			} else {
				context.q('.employee-filters input[name="showNewEmployees"]')[0].removeAttribute('disabled');
				context.q('.employee-filters input[name="showNewEmployees"]')[0].classList.add('has-data');
			}

			retrieveTerminated.checked = false;
			sortColumn(employees.parentNode, 0);
			noRecordsRow[1] = elem('tr', '<td colspan="5"><i>No records found</i></td>', {
				class: 'no-records',
				style: 'display:none;'
			});
			employees.appendChild(noRecordsRow[1]);
			//
			for (var i in mgrIndex)
				if (arrayIgnore.indexOf(i) == -1) {
					filterByManager[1].appendChild(elem('option', data.employees[i].fullname, {
						value: i
					}));
					for (var j in mgrDropdowns)
						if (arrayIgnore.indexOf(j) == -1) {
							var tmpOption = elem('option', data.employees[i].fullname, {
								value: i
							});
							if (mgrDropdowns[j].getAttribute('data-value') == i)
								tmpOption.selected = true;
							if (mgrDropdowns[j].parentNode.parentNode.getAttribute('data-id') == i)
								tmpOption.disabled = true;
							mgrDropdowns[j].appendChild(tmpOption);
						}
				}
			//	Add managers who are not yet assigned subordinates
			for (var i in mgrIndexAll)
				if (arrayIgnore.indexOf(i) == -1 && typeof mgrIndex[i] == 'undefined')
					for (var j in mgrDropdowns)
						if (arrayIgnore.indexOf(j) == -1){
							var tmpOption = elem('option', data.employees[i].fullname, {
								value: i
							});
							if (mgrDropdowns[j].getAttribute('data-value') == i)
								tmpOption.selected = true;
							if (mgrDropdowns[j].parentNode.parentNode.getAttribute('data-id') == i)
								tmpOption.disabled = true;
							mgrDropdowns[j].appendChild(tmpOption);
						}
			//
			sortList(filterByManager[1], 1);
			for (var j in mgrDropdowns)
				if (arrayIgnore.indexOf(j) == -1)
					sortList(mgrDropdowns[j], 0, false);
			//
			if (typeof params[0] != 'undefined')
				showEmployeeSettingsPage(employeeSettings, data, mgrIndex, params[0]);
			//
			//	---------------------------------------------------------------------------------------------
			//	SAVE AND CANCEL FOR EMPLOYEE BULK UPDATE TABLE
			//	---------------------------------------------------------------------------------------------
			var buttons = context.q('button.primary, button.gray');
			buttons[0].onclick = function (){
				var employees = context.q('tr.tablerow[data-id]');
				var upload = [],
					hasErrors = false;
				//	Find and compose a list of changed settings (diff).
				for (var i = 0; i < employees.length; i++){
					if (employees[i].style.display != 'none'){
						var attribs = employees[i].q('select');
						var reportEmail = employees[i].q('input[type="email"]')[0];
						var reportFrequency = employees[i].q('input[type="checkbox"]');
						var empID = employees[i].getAttribute('data-id');
						var tmp = {
							id: empID
						};
						// row.setAttribute('new-employee-change', true);
						var employeeRoleEnable = employees[i].getAttribute('new-employee-change');
						if (notifications && employeeRoleEnable != null && employeeRoleEnable == 'true'){
							var notificationContent = document.body.q('#notificationBar')[0].q('.notifications_bar_content');
							var newEmployCount = notificationContent[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0];
							var newEmployeeNotificationList = notificationContent[0].childNodes[0].childNodes[1].childNodes[0].childNodes[0];
							for (var j = newEmployeeNotificationList.childNodes.length-1; j > -1; j--){
								var newEmployeeOption = newEmployeeNotificationList.childNodes[j];
								var trimedEmployeeName = newEmployeeOption.textContent;
								trimedEmployeeName = trimedEmployeeName.substring(0, trimedEmployeeName.length -1 );
								var notificationName = employees[i].childNodes[0].innerText;
								notificationName = notificationName.substring(0, notificationName.length -2 )
								if (trimedEmployeeName == notificationName){
									var newNodeValue = newEmployCount.nodeValue;
									newEmployeeNotificationList.removeChild(newEmployeeOption);
									newNodeValue--;
									newEmployCount.nodeValue = newNodeValue;

								}
							}
						}

						//
						if (reportEmail.className != '') {
							if (reportEmail.value == '' || /\S+@\S+/.test(reportEmail.value))	// /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
								tmp.reportEmail = reportEmail.value;
							else {
								reportEmail.className = 'warning';
								if (!hasErrors) {
									showAlert('error', 'Please enter a valid email address');
									reportEmail.focus();
									scrollToTop();
								}
								hasErrors = true;
							}
						}
						//
						if (reportFrequency[0].parentNode.className != '' || reportFrequency[1].parentNode.className != '')
							tmp.reportFrequency = '10' + (reportFrequency[1].checked ? '1' : '0') + (reportFrequency[0].checked ? '1' : '0');
						//
						if (attribs.length == 1){
							if (attribs[0].className != '')
								tmp.status = attribs[0].value;
						}
						else if (attribs.length == 2){
							if (attribs[0].className != '')
								tmp.role = attribs[0].value;
							if (attribs[1].className != '')
								tmp.status = attribs[1].value;
						}
						else if (attribs[0].className != '' || attribs[1].className != '' || attribs[2].className != '') {
							if (attribs[0].className != '')
								tmp.role = attribs[0].value;
							if (attribs[1].className != '')
								tmp.manager = attribs[1].value;
							if (attribs[2].className != '')
								tmp.status = attribs[2].value;
						}
						if (Object.keys(tmp).length > 1)
							upload.push(tmp);
					}
				}
				//	Update if any changes
				if (upload.length > 0 && !hasErrors) { // change is here
					eventLogger('start', 'saveEmpSettings');
					new ajax(base_url + 'update-employee/', {
						method: 'POST',
						data: upload,
						callback: function (response) {
							var keys = {
								'role': 0,
								'manager': 1,
								'status': 2
							}, tmp;
							for (var i = 0; i < upload.length; i++){
								for (key in upload[i]){
									if (key == 'id'){
									}
									else if (key == 'reportFrequency'){
										checkboxes = context.q('tr.tablerow[data-id="' + upload[i].id + '"] input[type="checkbox"]');
										checkboxes[0].parentNode.className = 'inp-switch';
										checkboxes[0].setAttribute('data-value', checkboxes[0].checked ? '1' : '0');
										checkboxes[1].parentNode.className = 'inp-switch';
										checkboxes[1].setAttribute('data-value', checkboxes[1].checked ? '1' : '0');
									}
									else if (key == 'reportEmail'){
										tmp = context.q('tr.tablerow[data-id="' + upload[i].id + '"] input[type="email"]')[0];
										tmp.setAttribute('data-value', upload[i][key]);
										tmp.className = '';
									}
									else{
										tmp = context.q('tr.tablerow[data-id="' + upload[i].id + '"] select')[keys[key]];
										if (typeof tmp != 'undefined'){
											tmp.setAttribute('data-value', upload[i][key]);
											tmp.className = '';
										}
									}
									//*/
									try{
										if (key != 'id' && typeof processedData.employees != 'undefined' && typeof processedData.employees[upload[i].id] != 'undefined')
											processedData.employees[upload[i].id][key] = upload[i][key];
									}
									catch (e){
									}
									//*/
									//	If self.status is changed - Change access to myprofile link
									if (key == 'status' && upload[i].id == organizationSettings.myself.id){
										organizationSettings.myself.status = upload[i][key];
										setMyProfileLink(organizationSettings.myself);
									}
								}
							}
							fillMgrsNTeamsLists(null, empSettingsData, upload);
							//	ALERT USER OF SUCCESFUL UPDATE
							var employees = context.q('tr.tablerow[data-id]');
							for (var i = 0; i < employees.length; i++) {
								if (employees[i].style.display != 'none') {
									var attribs = employees[i].q('select');
									var empID = employees[i].getAttribute('data-id');
									//
									attribs[0].className = '';
									attribs[0].setAttribute('data-value', attribs[0].value);
									if (attribs.length > 2){
										attribs[1].className = '';
										attribs[1].setAttribute('data-value', attribs[1].value);
										attribs[2].className = '';
										attribs[2].setAttribute('data-value', attribs[2].value);
									}
									else if (attribs.length > 1){
										attribs[1].className = '';
										attribs[1].setAttribute('data-value', attribs[1].value);
									}
								}
							}
							showAlert('success', 'Employee settings saved');
							eventLogger('end', 'saveEmpSettings');
							scrollToTop();
						},
						fallback: function (data) {
							showAlert('error', 'Failed to save employee settings');
							eventLogger('end', 'saveEmpSettings');
							scrollToTop();
						}
					});
				}
				//	TO DO : Send data by Ajax, Update local storage, reset bgcolors
				//scrollToTop();
			}
			//	Reset the employee settings editable table - drop-downs
			buttons[1].onclick = function () {
				var selects = context.q('select[data-value]');
				hasChanges = [];
				employees.parentNode.tFoot.style.display = 'none';
				for (var i = 0, j = 0, cur_role = null; i < selects.length; i++) {
					selects[i].value = selects[i].getAttribute('data-value');
					selects[i].className = '';
					selects[i].parentNode.setAttribute('sorttable_customkey', (selects[i].querySelector('[value="' + selects[i].getAttribute('data-value') + '"]') || {
						'innerHTML': ''
					}).innerHTML);

					// disable/enable visibility dropdown according to role
					if ( j == 0 ) {
						cur_role = selects[i].value;
					}
					else if ( j == 2 ) {
						if ( cur_role <= 0 ) {
							selects[i].setAttribute('disabled', true);
						} else {
							selects[i].removeAttribute('disabled');
						}
						cur_role = null;
						j = -1;
					}
					j++;
				}
				var emails = context.q('input[type="email"][data-value]');
				for (var i = 0; i < emails.length; i++) {
					emails[i].value = emails[i].getAttribute('data-value');
					emails[i].className = '';
					emails[i].parentNode.setAttribute('sorttable_customkey', (emails[i].querySelector('[value="' + emails[i].getAttribute('data-value') + '"]') || {
						'innerHTML': ''
					}).innerHTML);
				}
				var ticks = context.q('input[type="checkbox"][data-value]');
				for (var i = 0; i < ticks.length; i++) {
					ticks[i].checked = ticks[i].getAttribute('data-value') == 1 ? true : false;
					ticks[i].parentNode.className = 'inp-switch';
				}
				scrollToTop();
			}
			eventLogger('end', 'loadEmpSettings');
		}
	});



	(function () {
		// retrieve terminated employees
		var terminatedEmpData = null;
		var termRows = [];
		var self = organizationSettings.myself;

		function populateTerminated (term_data) {

			var icon, email, chk, checkboxes;
			var rolesDropDown, mgrDropdowns = [];

			/*if (Object.keys(mgrIndex).length == 1) {
				employees4Mgr = Object.keys(mgrIndex)[0];
				filterByManager[1].value = employees4Mgr;
			}*/
			filterByManager[1].value = '*';

			//*/****************************************************
			//****************************************************
			var result = employeeSettingsRows(term_data.employees, employees, employees4Mgr, mgrDropdowns, mgrIndex, mgrIndexAll, self);
			termRows = result.rows;
			//****************************************************
			//****************************************************

			for (var i in mgrIndex) {
				if (arrayIgnore.indexOf(i) == -1) {
					// filterByManager[1].appendChild(elem('option', data.employees[i].fullname, {
					// 	value: i
					// }));
					for (var j in mgrDropdowns) {
						if (arrayIgnore.indexOf(j) == -1) {
							var tmpOption = elem('option', data.employees[i].fullname, {
								value: i
							});
							if (mgrDropdowns[j].getAttribute('data-value') == i)
								tmpOption.selected = true;
							if (mgrDropdowns[j].parentNode.parentNode.getAttribute('data-id') == i)
								tmpOption.disabled = true;
							mgrDropdowns[j].appendChild(tmpOption);
						}
					}
				}
			}

			//	Add managers who are not yet assigned subordinates
			for (var i in mgrIndexAll) {
				if (arrayIgnore.indexOf(i) == -1 && typeof mgrIndex[i] == 'undefined') {
					for (var j in mgrDropdowns) {
						if (arrayIgnore.indexOf(j) == -1){
							var tmpOption = elem('option', data.employees[i].fullname, {
								value: i
							});
							if (mgrDropdowns[j].getAttribute('data-value') == i)
								tmpOption.selected = true;
							if (mgrDropdowns[j].parentNode.parentNode.getAttribute('data-id') == i)
								tmpOption.disabled = true;
							mgrDropdowns[j].appendChild(tmpOption);
						}
					}
				}
			}

			//
			sortList(filterByManager[1], 1);
			for (var j in mgrDropdowns)
				if (arrayIgnore.indexOf(j) == -1)
					sortList(mgrDropdowns[j], 0, false);

			for (var s of mgrDropdowns) {
				var existingVals = [];
				for (var o of s.options) {
					if (existingVals.indexOf(o.value) === -1) {
						existingVals.push(o.value);
					} else {
						o.style.display = 'none';
					}
				}
			}
		}

		function showTerminated () {
			var rows = employees.q('tr');

			for (var i = 0; i < rows.length; i++) {
				var row = rows[i];
				row.style.display = 'none';
			}

			for (var j = 0; j < termRows.length; j++) {
				termRows[j].style.display = 'block';
			}

			//searchEmployee[1].setAttribute('disabled', 'disabled');
			searchRole[1].setAttribute('disabled', 'disabled');
			filterByManager[1].setAttribute('disabled', 'disabled');
			showNewEmployees.setAttribute('disabled', 'disabled');
			eventLogger('end', 'fetchTerminated');

		}

		function hideTerminated () {

			for (var j = 0; j < termRows.length; j++) {
				termRows[j].style.display = 'none';
			}

			//searchEmployee[1].removeAttribute('disabled');
			searchRole[1].removeAttribute('disabled');
			filterByManager[1].removeAttribute('disabled');
			showNewEmployees.removeAttribute('disabled');

			searchRole[1].onchange();
			eventLogger('end', 'fetchTerminated');
		}

		var getTerminatedToggle = context.q('input[name="retrieveTerminated"]')[0];

		getTerminatedToggle.checked = false;
		showNewEmployees.checked = false;

		getTerminatedToggle.onchange = function () {
			eventLogger('start', 'fetchTerminated');
			if (this.checked) {
				// get data if not cached

				if (terminatedEmpData === null) {

					new ajax(base_url + 'update-employee/?terminated=True', {
						callback: function (resp) {
							resp = eval('(' + resp.responseText + ')');
							terminatedEmpData = resp;
							for (var i in terminatedEmpData.employees) {
								if (typeof data.employees[i] === 'undefined') {
									data.employees[i] = terminatedEmpData.employees[i];
								}
							}
							populateTerminated(terminatedEmpData);
							showTerminated();

						}
					});

				} else {
					showTerminated();
				}


			} else {
				// show other data
				hideTerminated();
			}
		}
	})();


}


//	---------------------------------------------------------------------------------------------
//	FILL EMPLOYEE SETTINGS FORM
//	---------------------------------------------------------------------------------------------

function showEmployeeSettingsPage(context, params) {
	//return false;	//	For now
	//dateFilters.hide();
	hidePrevAlerts();
	startD = DateToShort(document.forms[0].from_date.value);
	endD = DateToShort(document.forms[0].to_date.value);
	if (prevXMLhttpRequest)
		prevXMLhttpRequest.abort();
	eventLogger('start', 'employeeSettings');
	//	var leaves = context.q('#leaves tbody')[0];
	//	leaves.innerHTML = '<tr><td><div class="loading"></div></td></tr>';
	var employee;
	var updateUserForm = context.q('form')[0];
	var checkboxes = updateUserForm.q('input[type="checkbox"]');
	var blockUser = context.q('button.red.primary.block')[0];
	var unblockUser = context.q('button.green.primary.block')[0];
	new ajax(base_url + 'update-employee/', {
		callback: function (data) {
			data = eval('(' + data.responseText + ')');
			empSettingsData = data;
			var self = organizationSettings.myself;//data.employees[uid];

			prevXMLhttpRequest = new ajax(base_url + 'employee-settings/?id=' + params[0] + '&fromdate=' + DateFromShort(startD).sqlFormatted() + '&todate=' + DateFromShort(endD).sqlFormatted(), {
				callback: function (data) {
					data = eval('(' + data.responseText + ')');
					employee = data.employee;
					var userRoleEnabled = true;
					//			leaves.innerHTML = '';
					//var employee = data.employees[params[0]];
					//
					var managrs = {};
					for (var i in data.managers)
						managrs[i] = data.managers[i];
					//
					var unIcon = usernameIcon(employee.fullname);
					var usericon = context.q('div.circle')[0];
					usericon.style.backgroundColor = '#' + unIcon[1];
					usericon.style.border = '2px solid #' + unIcon[1];
					if (employee.picture == '') {
						usericon.innerHTML = '<p>' + unIcon[0] + '</p>';
						usericon.style.backgroundImage = '';
					}
					else {
						usericon.innerHTML = '';
						usericon.style.backgroundImage = "url('" + employee.picture + "')";
					}
					var username = context.q('p.user-name')[0];
					username.innerHTML = employee.fullname;
					//
					if (typeof updateUserForm.userrole != 'undefined') {
						updateUserForm.userrole.innerHTML = '';
					}
					//updateUserForm.userrole.appendChild(elem('option', 'Please Select', {value: 0}));

					if (self.role == 70) {
						userRoleEnabled = false;
						if (context.q('#userrole')[0]) {
							updateUserForm.userrole.parentNode.removeChild( updateUserForm.userrole );
						}
						for (var i in roles) {
							if (arrayIgnore.indexOf(i) == -1 && roles[i] != '') {
								if (i == employee.role) {
									updateUserForm.userrolestatic.value = roles[i];
								}
							}
						}
					}
					else {
						if (context.q('#userrolestatic')[0]) {
							updateUserForm.userrolestatic.parentNode.removeChild( updateUserForm.userrolestatic );
						}
						for (var i in roles) {
							if (arrayIgnore.indexOf(i) == -1 && roles[i] != '') {
								var tmp = elem('option', roles[i], {
									value: i
								});
								if (i == employee.role)
									tmp.selected = true;
								updateUserForm.userrole.appendChild(tmp);
							}
						}
						updateUserForm.userrole.disabled = params[0] == uid;
						sortList(updateUserForm.userrole, -1, false);
					}
					//
					if ( typeof updateUserForm.usermgr != 'undefined' ) {
						updateUserForm.usermgr.innerHTML = '';
					}
					if (self.role == 70) {
						if (context.q('#usermgr')[0]) {
							updateUserForm.usermgr.parentNode.removeChild( updateUserForm.usermgr );
						}
						for (var i in managrs) {
							if (arrayIgnore.indexOf(i) == -1) {
								if (i == employee.manager) {
									updateUserForm.usermgrstatic.value = managrs[i];
								}
							}
						}
					}
					else {
						if (context.q('#usermgrstatic')[0]) {
							updateUserForm.usermgrstatic.parentNode.removeChild( updateUserForm.usermgrstatic );
						}
						updateUserForm.usermgr.appendChild(elem('option', '* Please Select', {
							value: 52
						}));
						for (var i in managrs) {
							if (arrayIgnore.indexOf(i) == -1) {
								var tmp = elem('option', managrs[i], {
									value: i
								});
								if (i == employee.manager)
									tmp.selected = true;
								if (params[0] == i)
									tmp.disabled = true;
								updateUserForm.usermgr.appendChild(tmp);
							}
						}
						sortList(updateUserForm.usermgr, -1, false);
					}
					//
					updateUserForm.visibility.options[0].selected = employee.status == 1;
					updateUserForm.visibility.options[1].selected = employee.status < 1;

					// toggle visibility and disable manager according to role
					if ( employee.role <= 0 ) {
						updateUserForm.visibility.selectedIndex = 1;
						updateUserForm.visibility.setAttribute('disabled', true);

						updateUserForm.usermgr.selectedIndex = 0;
						updateUserForm.usermgr.setAttribute('disabled', true);
					}
					else{
						updateUserForm.visibility.removeAttribute('disabled');
						if (self.role == 80) {
							updateUserForm.usermgr.removeAttribute('disabled');
						}
					}
					//
					//updateUserForm.report_email.value = ((employee.report_email != null && employee.report_email != '' || employee.email.indexOf('@') == -1)) ? employee.report_email : employee.email; //updateUserForm.report_email.placeholder = employee.email;
					updateUserForm.report_email.value = employee.report_email;
					if (employee.email.indexOf('@') > -1 && employee.is_app_user == 1 && (employee.report_email == null || employee.report_email == ''))
						updateUserForm.report_email.value = employee.email; //updateUserForm.report_email.placeholder = employee.email;
					//
					var chk = ('00' + employee.report_enabled).split('').reverse();
					checkboxes[0].checked = chk[0] == '1';
					checkboxes[1].checked = chk[1] == '1';
					//
					// disable daily reporting for admins
					if (employee.role == 80) {
						checkboxes[1].setAttribute("disabled", true);
						checkboxes[1].checked = false;
					}
					else {
						checkboxes[1].removeAttribute("disabled");
						checkboxes[1].checked = chk[1] == '1';
					}
					if (userRoleEnabled) {
						updateUserForm.userrole.onchange = function(e){
							if (employee.role > 69 && updateUserForm.userrole.value < 70){
								if (!confirm('Are you sure?\nChanging this employee\'s user role will reset the manager name to "none" for any employee who currently reports to this employee.')){
									e.cancelBubble = true;
									e.stopPropagation();
									e.preventDefault();
									updateUserForm.userrole.value = employee.role;
									return false;
								}
								else
									employee.role = updateUserForm.userrole.value;
							}
							if (updateUserForm.userrole.value == 80) {
								checkboxes[1].setAttribute("disabled", true);
								checkboxes[1].checked = false;
							}
							else {
								checkboxes[1].removeAttribute("disabled");
							}

							// toggle visibility and disable manager according to role
							if ( updateUserForm.userrole.value <= 0 ) {
								updateUserForm.visibility.selectedIndex = 1;
								updateUserForm.visibility.setAttribute('disabled', true);
								updateUserForm.usermgr.selectedIndex = 0;
								updateUserForm.usermgr.setAttribute('disabled', true);
							} else {
								updateUserForm.visibility.removeAttribute('disabled');
								updateUserForm.usermgr.removeAttribute('disabled');
							}

						};
					}

					breadCrumbViewModel();
					breadCrumbViewModel.reset();
					breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
					breadCrumbViewModel.addCrumb('#settings/employees', 'Employee settings', false);
					breadCrumbViewModel.addCrumb(location.hash, employee.fullname, true);
					breadCrumbViewModel.render(context);
					//
					//*/	//	REMOVEING THESE TWO BEAUTIFUL AND PERFECTLY WORKING COUPLE OF BUTTONS BECAUSE SOMEONE DISLIKE THESE
					blockUser.style.display = employee.status == 1 ? 'block' : 'none';
					unblockUser.style.display = employee.status < 1 ? 'block' : 'none';
					blockUser.onclick = function(){
						setUserStatus(-1);
					}
					unblockUser.onclick = function(){
						setUserStatus(1);
					}

					// hide/show hide/unhide button on visibility dropdown change
					updateUserForm.visibility.onchange = function (){
						if (this.value == 1) {
							unblockUser.style.display = 'none';
							blockUser.style.display = 'block';
						} else {
							blockUser.style.display = 'none';
							unblockUser.style.display = 'block';
						}
					};
					//*/
					//
					//			for (var i in data.leaves)
					//				leaves.appendChild(elem('tr', '<td>'+data.leaves[i].date+'</td><td>'+data.leaves[i].description+'</td>', {'data-id': 'date-'+data.leaves[i].date}));
					//			sortColumn(leaves.parentNode, 0);
					//
					eventLogger('end', 'employeeSettings');
				}
			});

		}
	});

	//
	//var saveUser = updateUserForm.q('button.primary.button')[0];//.right
	updateUserForm.onsubmit = function () {
		if (updateUserForm.report_email.value != '' && !/\S+@\S+/.test(updateUserForm.report_email.value)) {  //	/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
			showAlert('error', 'Please enter a valid email address');
			updateUserForm.report_email.focus();
		}
		else{
			var data = {
				id: params[0],
				status: updateUserForm.visibility.value,
				reportEmail: updateUserForm.report_email.value,// == '' || updateUserForm.report_email.value == employee.email ? null : updateUserForm.report_email.value,
				reportFrequency: '10' + (checkboxes[1].checked ? '1' : '0') + (checkboxes[0].checked ? '1' : '0')
			};
			if (typeof updateUserForm.userrole != 'undefined')
				data.role = updateUserForm.userrole.value;
			if (typeof updateUserForm.usermgr != 'undefined')
				data.manager = updateUserForm.usermgr.value;
			//
			eventLogger('start', 'saveEmpSettings');
			new ajax(base_url + 'update-employee/', {
				method: 'POST',
				data: [data],
				callback: function (response) {
					showAlert('success', 'Employee settings saved');
					cacheKey = '';
					/*response.employees[id].role = updateUserForm.userrole.value;
					response.employees[id].manager = updateUserForm.usermgr.value;*/
					/*if (typeof processedData.employees != 'undefined') {
						processedData.employees[params[0]].role = updateUserForm.userrole.value;
						processedData.employees[params[0]].manager = updateUserForm.usermgr.value;
						processedData.employees[params[0]].status = updateUserForm.visibility.value;
						processedData.employees[params[0]].report_email = updateUserForm.report_email.value;
						processedData.employees[params[0]].report_enabled = '10' + (checkboxes[1].checked ? '1' : '0') + (checkboxes[0].checked ? '1' : '0');
					}*/
					fillMgrsNTeamsLists(null, empSettingsData, [data]);
					eventLogger('end', 'saveEmpSettings');
					//document.location = document.location;	//	This would suck
					//
					organizationSettings.myself.status = updateUserForm.visibility.value;
					setMyProfileLink(organizationSettings.myself);
				},
				fallback: function (data) {
					showAlert('error', 'Failed to save employee settings');
					eventLogger('end', 'saveEmpSettings');
				}
			});
		}
		return false;
	}
	//
	//*/	//	REMOVEING THESE TWO BEAUTIFUL AND PERFECTLY WORKING COUPLE OF BUTTONS BECAUSE SOMEONE DISLIKE THESE
	var setUserStatus = function (status) {
		var data = [{
			id: params[0],
			status: status
		}];
		eventLogger('start', 'blockEmpSettings');
		new ajax(base_url + 'update-employee/', {
			method: 'POST',
			data: data,
			callback: function (response) {
				//showAlert('success', 'Employee '+(status == 1 ? 'un':'')+'blocked');
				showAlert('success', 'Employee is ' + (status == 1 ? 'made visible' : 'hidden'));
				//var user = eval('('+localStorage['user-'+params[0]]+')');
				//user[5] = status;
				//localStorage['user-'+params[0]] = JSON.stringify(user);
				//response.employees[params[0]].status = status;
				/*if (typeof processedData.employees != 'undefined')
					processedData.employees[params[0]].status = status;*/
				fillMgrsNTeamsLists(null, empSettingsData, data);
				eventLogger('end', 'saveEmpSettings');
				//
				blockUser.style.display = status == 1 ? 'block' : 'none';
				unblockUser.style.display = status < 1 ? 'block' : 'none';

				// set visibility dropdown value
				for(var i=0; i < updateUserForm.visibility.options.length; i++) {
					if(updateUserForm.visibility.options[i].value == status) {
						updateUserForm.visibility.selectedIndex = i;
					}
				}
				organizationSettings.myself.status = status;
				setMyProfileLink(organizationSettings.myself);
			},
			fallback: function (data) {
				showAlert('error', 'Failed to ' + (status == 1 ? 'un' : '') + 'block employee');
				eventLogger('end', 'blockEmpSettings');
			}
		});
	}
	//*/
	//
	var regLeavesForm = context.q('form')[1];
	//var submitButton = regLeavesForm.q('button.primary')[0];
	regLeavesForm.onsubmit = regLeavesForm.onsubmit = function () {
		var opt = regLeavesForm.q('button.btngroup');
		var data = {
			employee: params[0],
			description: regLeavesForm.message.value
		};
		if (opt[0].className.indexOf('active') > -1)
			data['date'] = regLeavesForm.from_date.value;
		else {
			data['from_date'] = regLeavesForm.from_date.value;
			data['to_date'] = regLeavesForm.to_date.value;
		}
		new ajax(base_url + 'register-leaves', {
			method: 'POST',
			data: data,
			callback: function (data) {
				data = eval('(' + data.responseText + ')');
				for (var i = 0; i < data.length; i++) {
					var checkExt = leaves.q('[data-id="date-' + data[i].date + '"]');
					if (checkExt.length == 0)
						leaves.appendChild(elem('tr', '<td>' + data[i].date + '</td><td>' + data[i].description + '</td>', {
							'data-id': 'date-' + data[i].date
						}));
					else
						checkExt[0].q('td')[1].innerHTML = data[i].description;
					sortColumn(leaves.parentNode, 0);
				}
			}
		});
		return false;
	}
	var optionButtons = regLeavesForm.q('button.btngroup.btn');
	var endDateRow = regLeavesForm.q('div.wrap.end-date')[0];
	//new calendar(regLeavesForm.from_date);
	//new calendar(regLeavesForm.to_date);
	new textareaHandler(regLeavesForm.message);
	for (var i = 0; i < optionButtons.length; i++)
		optionButtons[i].onclick = function (i) {
			return function () {
				for (var j = 0; j < optionButtons.length; j++)
					optionButtons[j].removeClass('active');
				optionButtons[i].addClass('active');
				endDateRow.style.display = i == 0 ? 'none' : 'block';
				return false;
			}
		}(i);
}

function showTimeSettings(context){
	dateFilters.hide();
	hidePrevAlerts();
	var settingsForm = context.q('form')[0];//employeeSettings
	var buttons = settingsForm.q('button.primary, button.gray');
	var weekdays = settingsForm.q('button.btngroup.btn');

	var preVals = ["09:00", "17:00"];

	function setFullDay () {
		settingsForm.start_time.value = '00:00';
		settingsForm.end_time.value = '23:59';

		settingsForm.start_time.setAttribute("disabled", true);
		settingsForm.end_time.setAttribute("disabled", true);
	}

	function unsetFullDay () {
		settingsForm.start_time.removeAttribute("disabled");
		settingsForm.end_time.removeAttribute("disabled");
		settingsForm.start_time.value = preVals[0];
		settingsForm.end_time.value = preVals[1];
	}

	settingsForm.full_day.onchange = function(){
		if (this.checked) {
			setFullDay();
		} else {
			unsetFullDay();
		}
	};

	settingsForm.start_time.onchange = function (e) {
		preVals[0] = e.target.value;
	};

	settingsForm.end_time.onchange = function (e) {
		preVals[1] = e.target.value;
	};


	//
	new function(){
		if (typeof organizationSettings == 'undefined'){
			setTimeout(arguments.callee, 10);
			return false;
		}
		new ajax(base_url+'timezone/', {
				method: 'GET',
				callback: function(data){
					var region = context.q('form #region')[0], timezone = context.q('form #timezone')[0], tmp, tmpi;
					region.innerHTML = '';
					timezone.innerHTML = '';
					data = eval('('+data.responseText+')');

					for (var i = 0; i < data.length; i++){
						tmp = elem('option', data[i][0], {value: i});

						if ((typeof organizationSettings['timezone'].split('/')[1] === 'undefined' && data[i][0] === 'Other')	|| data[i][0] == organizationSettings['timezone'].split('/')[0] ) {
							tmp.selected = true;
							for (var j = 0; j < data[i][1].length; j++){
								tmpi = elem('option', data[i][1][j][1], {value: data[i][1][j][0]});
								if (data[i][1][j][0] == organizationSettings['timezone']) {
									tmpi.selected = true;
								}
								timezone.appendChild(tmpi);
							}
						}
						region.appendChild(tmp);
					}

					region.onchange = function(){
						timezone.innerHTML = '';
						for (var i = 0; i < data[this.value][1].length; i++){
							tmp = elem('option', data[this.value][1][i][1], {value: data[this.value][1][i][0]});
							if (data[this.value][1][i][0] == organizationSettings['timezone'])//.substring(organizationSettings['timezone'].indexOf('/')+1)
								tmp.selected = true;
							timezone.appendChild(tmp);
						}
					}
					/*for (var i in data){
						tmp = elem('option', i, {value: i});
						if (i == organizationSettings['timezone'].split('/')[0]){
							tmp.selected = true;
							for (var j in data[i]){
								tmpi = elem('option', data[i][j], {value: j});
								if (j == organizationSettings['timezone'].substring(organizationSettings['timezone'].indexOf('/')+1))
									tmpi.selected = true;
								timezone.appendChild(tmpi);
							}
						}
						region.appendChild(tmp);
					}
					region.onchange = function(){
						timezone.innerHTML = '';
						for (var i in data[this.value]){
							tmp = elem('option', data[this.value][i], {value: i});
							if (data[this.value][i] == organizationSettings['timezone'].substring(organizationSettings['timezone'].indexOf('/')+1))
								tmp.selected = true;
							timezone.appendChild(tmp);
						}
					}*/
				}
			});
		for (var i = 0; i < weekdays.length; i++){
			weekdays[i].onclick = function(i){
				var btn = this;
				return function(){
					if (weekdays[i].className.indexOf('active') == -1)
						weekdays[i].addClass('active');
					else
						weekdays[i].removeClass('active');
					return false;
				}
			}(i);
			if (organizationSettings['workingdays'].indexOf(i) > -1)
				weekdays[i].addClass('active');
		}
		settingsForm.start_time.value = preVals[0] = organizationSettings['daystart'];
		settingsForm.end_time.value = preVals[1] = organizationSettings['dayend'];
		settingsForm.timezone.value = organizationSettings['timezone'];
		settingsForm.show_details.checked = organizationSettings['show_details'] == 1;

		if (settingsForm.start_time.value == '00:00' && settingsForm.end_time.value == '23:59') {
			settingsForm.full_day.checked = true;
			setFullDay();
		} else {
			settingsForm.full_day.checked = false;
			unsetFullDay();
		}
		eventLogger('end', 'timeSettings');
	}();
	//
	settingsForm.onsubmit = buttons[0].onclick = function(){
		var workingdays = [];
		for (var i = 0; i < weekdays.length; i++)
			if (weekdays[i].className.indexOf('active') > -1)
				workingdays.push(i);
		//
		//	Validate form data
		if (settingsForm.start_time.value == ''){
			showAlert('error', 'Please select work hours start time.');
			return false;
		}
		else if (settingsForm.end_time.value == ''){
			showAlert('error', 'Please select work hours end time.');
			return false;
		}
		else if (settingsForm.start_time.value >= settingsForm.end_time.value) {
			showAlert('error', 'Change the start time to be before the end time.');
			return false;
		}
		else if (workingdays.length == 0){
			showAlert('error', 'You have to select at least one working day.');
			return false;
		}
		else if (workingdays.length < 3){
			if (!confirm('Are you sure you want to proceed with less than 3 working days.?'))
				return false;
		}
		//
		eventLogger('start', 'orgSettings');
		new ajax(base_url+'update-organization/', {
				method: 'POST',
				data: {
					workingdays: workingdays,
					daystart: settingsForm.start_time.value,
					dayend: settingsForm.end_time.value,
					timezone: settingsForm.timezone.value,
					show_details: settingsForm.show_details.checked ? 1 : 0
				},
				callback: function(data){
					organizationSettings['workingdays'] = workingdays;
					organizationSettings['daystart'] = settingsForm.start_time.value;
					organizationSettings['dayend'] = settingsForm.end_time.value;
					organizationSettings['timezone'] = settingsForm.timezone.value;
					organizationSettings['show_details'] = settingsForm.show_details.checked ? 1 : 0;
					if (settingsForm.start_time.value == '00:00' && settingsForm.end_time.value == '23:59') {
						settingsForm.full_day.checked = true;
						setFullDay();
					} else {
						settingsForm.full_day.checked = false;
						unsetFullDay();
					}
					scrollToTop();
					showAlert('success', 'Organization settings are saved');
					eventLogger('end', 'orgSettings');
				},
				fallback: function(data){
					scrollToTop();
					showAlert('error', 'Organization settings could not be saved');
					eventLogger('end', 'orgSettings');
				}
			});
		return false;
	}
}


//	---------------------------------------------------------------------------------------------
//	FILL USER ACCOUNT SETTINGS FORM
//	---------------------------------------------------------------------------------------------

function showUserSettingsPage(userSettings, params) {
	hidePrevAlerts();
	doFetchData('user-accounts',
		function (data, self, startD, endD) {
			var employees = userSettings.querySelector('.employeedata').tBodies[0];
			employees.innerHTML = '';
			//
			var addUser = userSettings.querySelector('button.primary, button.gray');
			addUser.onclick = function () {
					document.location.hash = 'settings/users/o/new';
				}
				//
			for (var i in data.employees)
				if (data.employees[i].role > 69 && data.employees[i].is_app_user) {
					//var stats = reProcessUserStats(data, i, ['icon', 'change']);
					var icon = usernameIcon(data.employees[i].fullname);
					var employee = arcReactor({
						tr: {
							class: 'tablerow',
							'data-id': i,
							'data-role': data.employees[i].role,
							'data-manager': data.employees[i].manager,
							content: [
								{
									td: {
										content: {
											div: {
												class: 'user-name-wrap',
												content: {
													p: {
														class: 'user-name',
														content: data.employees[i].fullname
													}
												}
											}
										},
										style: 'border-left:2px solid #' + icon[1]
									}
								},
								{
									td: {
										content: data.employees[i].email
									}
								},
								{
									td: {
										content: roles[data.employees[i].role]
									}
								},
										],
							onclick: function () {
								document.location.hash = 'settings/users/o/' + this.getAttribute('data-id');
							}
						}
					});
					employees.appendChild(employee);
					found = true;
				}
			sortColumn(employees.parentNode, 0);
			scrollToTop();
		});
}

function showOneUserSettings(userSettings, params) {
	hidePrevAlerts();
	doFetchData('user-accounts',
		function (data, self, startD, endD) {
			var employee = userSettings.querySelector('form');
			var buttons = employee.querySelectorAll('button.primary, button.red, button.gray');
			//
			var id = params[0];
			if (typeof data.employees[id] == 'undefined') {
				employee.id.value = 'new';
				employee.fullname.value = '';
				employee.email.value = '';
				employee.email.removeAttribute('disabled');
				employee.role.value = '';
				employee.password.value = '';
				buttons[1].style.display = 'none';
				employee.role.options[2].style.display = 'none';
			} else {
				employee.id.value = id;
				employee.fullname.value = data.employees[id].fullname;
				employee.email.value = data.employees[id].email;
				employee.email.setAttribute('disabled', true);
				employee.role.value = data.employees[id].role;
				employee.password.value = '[ENCRYPTED]';
				buttons[1].style.display = 'inline-block';
				employee.role.options[2].style.display = '';
			}
			//
			employee.email.onkeydown = function(e){
				if (e.keyCode == 32){
					e.stopPropagation();
					return false;
				}
			};
			//
			//var prevAlert;//prevAlert =
			employee.onsubmit = buttons[0].onclick = function (type) {
				type = typeof type !== 'undefined' ? type : false;
				//employee.email.value = employee.email.value.replace(/ /g, '');
				/*try{
					hideAlert(prevAlert, 10);
				}catch(e){}*/
				//
				if (employee.fullname.value.trim() == '') {
					showAlert('error', 'Please enter the full name');
					employee.fullname.focus();
				}
				else if (employee.role.value.trim() == '') {
					showAlert('error', 'Please select user role');
					employee.role.focus();
				}
				else if (employee.email.value == '' || employee.email.value.indexOf(' ') > -1) { //	 || ! /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(employee.email.value) // employee.email.value.trim() == ''
					showAlert('error', 'The username must not include spaces');// / email address
					employee.email.focus();
				}
				else if (employee.password.value.trim() == '') {
					showAlert('error', 'Please enter a password');
					employee.password.focus();
				}
				else {
					eventLogger('start', 'saveUsrSettings');
					var upData = [{
								id: employee.id.value,
								fullname: employee.fullname.value,
								email: employee.email.value,
								role: employee.role.value,
								password: employee.password.value,
								appuser: 1
							}];
					new ajax(base_url + 'update-employee/', {
						method: 'POST',
						data: upData,
						callback: function (response) {
							response = eval('(' + response.responseText + ')');
							if (typeof response.errors != 'undefined') {
								for (var email in response.errors)
									showAlert('error', 'This username is already in use');
							}
							else {
								try {
									/*if (typeof processedData.employees[response['data-ids'][0]] == 'undefined')
										processedData.employees[response['data-ids'][0]] = {};
									processedData.employees[response['data-ids'][0]].fullname = employee.fullname.value;
									processedData.employees[response['data-ids'][0]].email = employee.email.value;
									processedData.employees[response['data-ids'][0]].role = employee.role.value;*/
									upData[0].id = response['data-ids'][0];
									fillMgrsNTeamsLists(null, empSettingsData, upData);
									if (employee.id.value == 'new'){
										document.location.hash = 'settings/employees/o/' + response['data-ids'][0];
										showAlert('success', 'User account created');
									}
									else {
										document.location.hash = 'settings/users';
										if (type === 'delete') {
											showAlert('success', 'User account deleted');
										} else {
											showAlert('success', 'User account saved');
										}
									}
								}
								catch (e) {
									showAlert('error', response.responseText);
								}
							}
							eventLogger('end', 'saveUsrSettings');
						},
						fallback: function (response) {
							showAlert('error', 'Error saving User account');
							eventLogger('end', 'saveUsrSettings');
						}
					});
				}
				return false;
			}
			buttons[1].onclick = function () {
				if (!confirm('Are you sure you want to delete this user.?'))
					return false;
				employee.role.value = -1;
				employee.onsubmit('delete');
				return false;
			}
			buttons[2].onclick = function () {
					document.location.hash = 'settings/users';
					return false;
				}
				//
			scrollToTop();
		});
	userSettings.q('form')[0].fullname.onkeyup = function () {
		var color = usernameIcon(this.value, false);
		this.style.borderLeft = '3px solid #' + color[1];
	}
}

function showAlertSettings(context, params) {
	dateFilters.hide();
	hidePrevAlerts();
	eventLogger('start', 'alertSettings');
	var buttons = context.q('.btngroup.btn');
	var btnsets = [[0, 1], [2, 3], [4, 5], [6, 7], [8, 9]];
	for (var j = 0; j < btnsets.length; j++)
		new(function (btnset) {
			for (var i = 0; i < btnset.length; i++)
				if (typeof buttons[btnset[i]] != 'undefined'){
					buttons[btnset[i]].onclick = function () {
						for (var j = 0; j < btnset.length; j++)
							buttons[btnset[j]].removeClass('active');
						this.addClass('active');
						return false;
					};
				}
		})(btnsets[j]);
	//	---------------------------------------------------------------------------------------------------------
	//	Save alert settings
	//	---------------------------------------------------------------------------------------------------------
	var form = context.q('form')[0];
	form.onsubmit = function () {
			eventLogger('start', 'saveAlertSettings');
			let alert_data = {
				enabled_alert_below: buttons[0].className.indexOf('active') > -1,
				enabled_alert_below_period: buttons[2].className.indexOf('active') > -1,
				enabled_alert_inactive: buttons[4].className.indexOf('active') > -1,
				enabled_alert_inactive_period: buttons[6].className.indexOf('active') > -1
			};
			if (buttons.length > 8)
				alert_data.enabled_alert_new_employee = buttons[8].className.indexOf('active') > -1;
			new ajax(base_url + 'alert-settings/', {
				method: 'POST',
				data: alert_data,
				callback: function (data) {
					//data = eval('('+data.responseText+')');
					showAlert('success', 'Your alert settings are saved.', 4000);
					eventLogger('end', 'saveAlertSettings');
					// update organization settings and update notifications
					organizationSettings.alert_below_period = (alert_data.enabled_alert_below_period ? 1 : 0);
					organizationSettings.alert_below_yesterday = (alert_data.enabled_alert_below ? 1 : 0);
					organizationSettings.alert_inactive_period = (alert_data.enabled_alert_inactive_period ? 1 :0);
					organizationSettings.alert_inactive_yesterday = (alert_data.enabled_alert_inactive ? 1 : 0);
					organizationSettings.alert_new_employee = (alert_data.enabled_alert_new_employee ? 1 : 0);
					if (organizationSettings.domain == organizationSettings.myself.email.split('@')[1])
						notifications.renderNotification(false);
					scrollToTop();
				}
			});
			return false;
		}
		//	---------------------------------------------------------------------------------------------------------
		//	Load alert settings
		//	---------------------------------------------------------------------------------------------------------
	new ajax(base_url + 'alert-settings/', {
		callback: function (data) {
			data = eval('(' + data.responseText + ')');
			buttons[0].className = 'btngroup btn' + (data.enabled_alert_below ? ' active' : '');
			buttons[1].className = 'btngroup btn' + (data.enabled_alert_below ? '' : ' active');
			buttons[2].className = 'btngroup btn' + (data.enabled_alert_below_period ? ' active' : '');
			buttons[3].className = 'btngroup btn' + (data.enabled_alert_below_period ? '' : ' active');
			buttons[4].className = 'btngroup btn' + (data.enabled_alert_inactive ? ' active' : '');
			buttons[5].className = 'btngroup btn' + (data.enabled_alert_inactive ? '' : ' active');
			buttons[6].className = 'btngroup btn' + (data.enabled_alert_inactive_period ? ' active' : '');
			buttons[7].className = 'btngroup btn' + (data.enabled_alert_inactive_period ? '' : ' active');
			if (buttons.length > 8){
				buttons[8].className = 'btngroup btn' + (data.enabled_alert_new_employee ? ' active' : '');
				buttons[9].className = 'btngroup btn' + (data.enabled_alert_new_employee ? '' : ' active');
			}
			eventLogger('end', 'alertSettings');
		}
	});
	//	---------------------------------------------------------------------------------------------------------
}
