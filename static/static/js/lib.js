// ==============================================================================================
// ==============================================================================================
//													LIB / FUNC
// ==============================================================================================

var timezoneOffset = (new Date()).getTimezoneOffset() * 60000;
var monthsReverse = {
	'Jan': '01',
	'Feb': '02',
	'Mar': '03',
	'Apr': '04',
	'May': '05',
	'Jun': '06',
	'Jul': '07',
	'Aug': '08',
	'Sep': '09',
	'Oct': '10',
	'Nov': '11',
	'Dec': '12'
};
var iconsMap = {
	'application/vnd.google-apps.audio': 'icon_1_audio_x32.png',
	'application/vnd.google-apps.document': 'logo_docs_64px.png',
	'application/vnd.google-apps.drawing': 'icon_1_drawing_x32.png',
	'application/vnd.google-apps.file': 'docs.svg',
	'application/vnd.google-apps.form': 'logo_forms_64px.png',
	'application/vnd.google-apps.fusiontable': 'logo_sheets_64px.png',
	'application/vnd.google-apps.map': '',
	'application/vnd.google-apps.photo': '',
	'application/vnd.google-apps.presentation': 'logo_slides_64px.png',
	'application/vnd.google-apps.script': '',
	'application/vnd.google-apps.sites': 'logo_sites_64px.png',
	'application/vnd.google-apps.spreadsheet': 'logo_sheets_64px.png',
	'application/vnd.google-apps.video': 'icon_1_video_x32.png',
};

function DateFromShort(date) {
	return new Date((16800 + (1 * date)) * 86400000 + timezoneOffset + (4*3600000) );
}

function DateToShort(date) {
	if (date instanceof Date)
		return parseInt((date.getTime() - timezoneOffset - (4*3600000)) / 86400000) - 16800;
	else
		return parseInt((new Date(date)).getTime() / 86400000) - 16800; // - timezoneOffset
}

function strataStr(strata) {
	if (strata == 2)
		return ['above', 'Above'];
	else if (strata == 1)
		return ['baseline', 'Average'];
	else
		return ['below', 'Below'];
}

function strataClassname4Score(avg) {
	return avg > 70 ? 'above' : (avg < 30 ? 'below' : 'baseline');
}


//	------------------------------------------------------

//	Find if the given day of week is a working day as defined on time settings.
//	Should be improved: to exclude organization holidays when defined in future.
//	Sugesstion: Also exclude leaves for each employee
function isWorkingDay(day) {
	if (organizationSettings['workingdays'].length == 0) {
		organizationSettings['workingdays'] = [1, 2, 3, 4, 5];
		showAlert('warning', 'Please set working days for your organization.');
		if (typeof processedData != 'undefined' && typeof processedData.employees != 'undefined' && typeof uid != 'undefined' && organizationSettings.myself.role == 80)
			document.location = '#settings/organization';
	}
	if (typeof organizationSettings == 'undefined' || organizationSettings == null) {
		console.log('Working days organization settings are not loaded, yet queried');
		//throw(error);
		if (day > 0 && day < 6)
			return true;
		return false;
	} else
		return organizationSettings['workingdays'].indexOf(day) > -1;
}

function isWithinWorkingHours(time) {
	if (time.indexOf(' - ') > -1) {
		time = time.split(' - ');
		return isWithinWorkingHours(time[0]) && isWithinWorkingHours(time[1]);
	}
	time = timeToMts(time);
	var daystart = timeToMts(organizationSettings.daystart);
	var dayend = timeToMts(organizationSettings.dayend);
	if (time < daystart || time > dayend)
		return false;
	else
		return true;
}

function timeToMts(time) {
	time = time.split(':');
	//
	if (time[1].indexOf(' ') > -1 && (time[1].indexOf('AM') > -1 || time[1].indexOf('PM') > -1)){
		time[1] = time[1].split(' ');
		if (time[1][1] == 'PM')
			time[0] = time[0]*1 + 12;
		time[1] = time[1][0];
	}
	//
	return time[0] * 60 + (time.length > 1 ? time[1] * 1 : 0) + (time.length > 2 ? time[2] / 60 : 0);
}

function mtsToTime(time) {
	var mts = parseInt(time % 60),
		hrs = Math.floor(time / 60);
	var secs = parseInt(60 * (time - Math.floor(time)));
	if (isNaN(mts) || isNaN(hrs))
		return '-';
	//
	return (hrs > 0 ? hrs + 'h ' : '') + (mts < 10 ? '0' : '') + mts + 'm' + (secs > 0 ? ' '+secs+'s' : '');//
}

function mtsToHrs(time) {
	var mts = parseInt(time % 60),
		hrs = Math.floor(time / 60),
		ampm = '';
	// if (hrs > 12) {
	// 	ampm = ' PM';
	// 	hrs = hrs - 12;
	// } else {
	// 	ampm = ' AM';
	// }
	return (hrs < 10 ? '0' : '') + hrs + ':' + (mts < 10 ? '0' : '') + mts + ampm;
}

function sortList(ul, skipAt, debug){
	// return ul;
	var value;
	if (typeof ul.value != 'undefined'){
		//if (ul.value == 80) return ul;
		value = ul.value;
	}
	if (typeof skipAt == 'undefined')
		skipAt = -1;
	var lis = [];

	// Add all lis to an array
	for (var i = ul.childNodes.length; i--;){
		if (i == skipAt)
			break;
		else
			lis.push(ul.removeChild(ul.childNodes[i]));
	}

	// Sort the lis in ascending order
	lis.sort(function(a, b){
		return b.innerText > a.innerText ? 1 : -1;
	});

	// Add them into the ul in order
	for (var i = lis.length; i--;)
		ul.appendChild(lis[i]);

	if (typeof ul.value != 'undefined')
		ul.value = value;
	return ul;
}

function sortColumn(table, col, order) {
	var thead = table.querySelectorAll('th');
	for (var i = 0; i < thead.length; i++) {
		thead[i].removeClass('sorttable_sorted');
		thead[i].removeClass('sorttable_sorted_reverse');
	}
	//if (typeof order != 'undefined' && order == false)
	//	thead[col].addClass('sorttable_sorted');
	try {
		sorttable.innerSortFunction.apply(thead[col], []);
		if (typeof order != 'undefined' && order == false)
			sorttable.innerSortFunction.apply(thead[col], []);
	} catch (e) {}
}

//	Generates DOM for a select / drop-down input
//	that changes color when changed to a different value than original
//	and keeps alatch to prevent events on parent object when clicked on select
function makeDropdown(data, selected, onchange, defVal) {
	if (typeof defVal == 'undefined')
		defVal = 0;
	var output = elem('select', null, {
		'data-value': selected
	});
	if (defVal != -1)
		output.appendChild(elem('option', 'None', {
			value: defVal
		}));
	for (var i in data)
		if (arrayIgnore.indexOf(i) == -1 && data[i] != '') {
			var tmp = elem('option', data[i], {
				value: i
			});
			if (i == selected)
				tmp.selected = true;
			output.appendChild(tmp);
		}
	/*sortList(output);
	output.value = selected;*/
	output.onchange = function (e) {
		if (this.value == this.getAttribute('data-value'))
			this.className = '';
		else
			this.className = 'updated';
		//
		var result = onchange(this, e);
		if (typeof result != 'undefined' && result === false){
			//
			if (this.value == this.getAttribute('data-value'))
				this.className = '';
			else
				this.className = 'updated';
			//
			return false;
		}
		//
		this.parentNode.setAttribute('sorttable_customkey', this.querySelector('[value="' + this.value + '"]').innerHTML);
	};
	/*setTimeout(function(){
		sortList(output);
		output.value = selected;
	}, 800);//240*/
	return output;
}

//	Generates DOM for a searchable select / drop-down input
function makeSearchDropdown(data, selected, onchange, defVal, dropdowns) {
	var noneStr = 'None';
	if (typeof defVal == 'undefined')
		defVal = 0;
	if (typeof selected == 'undefined')
		selected = '';
	var output = elem('div', '<input type="text" placeholder="Search" /><i class="fa fa-search"></i><ul class="dropdown"></ul>', {
		'data-value': selected, class: 'search-dropdown', 'data-selected': selected//'null'
	});
	var input = output.q('input')[0];
	var ul = output.q('ul.dropdown')[0];
	var _self = this;
	this.value = (typeof data[ selected ] == 'undefined') ? null : selected;
	//
	if (defVal != -1){
		ul.appendChild(elem('li', noneStr, {// - Type to Search
			'data-value': ''
		}));
		//input.value = noneStr;//data[ selected ];
	}
	input.onfocus = function(){
		search();
		//ul.style.display = 'block';
		_self.DOM.addClass('open');
		this.value = '';
		this.select();
	};
	input.onblur = function(){
		//ul.style.display = 'none';
		_self.DOM.removeClass('open');
		//ul.innerHTML = '';
		setTimeout(function(){
			if (typeof data[ _self.DOM.getAttribute('data-selected') ] == 'undefined' || _self.DOM.getAttribute('data-selected') == 'null' || _self.DOM.getAttribute('data-selected') == 'null:null' || _self.DOM.getAttribute('data-selected') == '')
				input.value = noneStr;
			else
				input.value = data[ _self.DOM.getAttribute('data-selected') ];
		}, 10);
	};
	var searchTimeout;
	input.onkeyup = function(){
		clearTimeout(searchTimeout);
		searchTimeout = setTimeout(search, 200);
	};
	var search = function(){
		var selections = {};
		for (var i = 0; i < dropdowns.length; i++)
			selections[ dropdowns[i].value ] = i;
		//
		ul.innerHTML = '';
		ul.appendChild(elem('li', noneStr, {// - Type to Search
			'data-value': ''
		}));
		//
		if (input.value.length > 1){
			for (var i in data)
				if (arrayIgnore.indexOf(i) == -1 && data[i] != '' && data[i].toLowerCase().indexOf( input.value.toLowerCase() ) > -1 && typeof selections[i] == 'undefined'){
					var tmp = elem('li', data[i], {
						'data-value': i
					});
					/*if (i == selected){
						//tmp.selected = true;
						input.value = data[ selected ];//i;
					}*/
					ul.appendChild(tmp);
				}
			sortList(ul, 0);
		}
	};
	ul.onmousedown = function(e){
		if (e.target.tagName == 'LI'){
			input.value = e.target.innerText;
			_self.value = e.target.getAttribute('data-value');
			_self.onchange(e);
		}
	};
	this.setValue = function(val){
		if (typeof data[ val ] == 'undefined'){
			input.value = noneStr;
			_self.value = null;
		}
		else{
			input.value = data[ val ];
			_self.value = val;
		}
		_self.onchange({});
		input.onkeyup();
	};
	//
	if (typeof data[ selected ] == 'undefined')
		input.value = noneStr;
	else{
		input.value = data[ selected ];
		input.onkeyup();
	}
	/*for (var i in data)
		if (arrayIgnore.indexOf(i) == -1 && data[i] != '') {
			var tmp = elem('option', data[i], {
				value: i
			});
			if (i == selected)
				tmp.selected = true;
			output.appendChild(tmp);
		}*/
	this.onchange = function() {
		if (_self.value == _self.DOM.getAttribute('data-value') || (_self.value == null && (_self.DOM.getAttribute('data-value') == '' || _self.DOM.getAttribute('data-value') == 'null:' || _self.DOM.getAttribute('data-value') == 'null:null')))
			_self.DOM.removeClass('updated');
		else
			_self.DOM.addClass('updated');
		_self.DOM.parentNode.setAttribute('sorttable_customkey', input.value);
		_self.DOM.setAttribute('data-selected', _self.value);
		onchange(this);
	};
	this.DOM = output;
	//return this;
}

function parseHeaders(data) {
	var output = {};
	for (var id in data)
		output[data[id].name] = data[id].value;
	return output;
}

function decode_base64(dom) {
	dom.parentNode.innerHTML = atob(dom.getAttribute('data-base64').replace(/_/g, '/').replace(/-/g, '+'));
}

function showEncrypted(msg) {
	try {
		return atob(msg.replace(/_/g, '/').replace(/-/g, '+')).replace(/Â/gi, '<br/>');
	} catch (e) {
		return 'Unable to Decode';
	}
}

//	------------------------------------------------------------------------------------------

function getIcon(data, title) {
	// if (typeof data.data == 'string') data.data = eval('(' + data.data + ')');
	var mime = data['mimeType'] || data['mime'];
	if (mime.startsWith('application/vnd.google-apps'))
		return static_url+'img/icons/' + iconsMap[mime];
	else if (title.lastIndexOf('.') > title.length - 6)
		return 'http://mimeicon.herokuapp.com/' + title.substring(title.lastIndexOf('.') + 1) + '?size=32';
	else
		return 'http://mimeicon.herokuapp.com/' + mime + '?size=32';
}

function sentimentWidget(sentiment){
	if (sentiment.score == null && sentiment.magnitude == null)
		return '';
	else
		return (sentiment.score < -0.1 ? '<i class="sentiment fa fa-frown-o"></i>' : (sentiment.score > 0.1 ? '<i class="sentiment fa fa-smile-o"></i>' : '<i class="sentiment fa fa-meh-o"></i>'))+
			' &nbsp; <span title="Score of the sentiment ranges from -1.0 (very negative) to 1.0 (very positive)">Score:</span> '+
				'<b style="color:'+(sentiment.score < -0.1 ? '#DC0000' : (sentiment.score > 0.1 ? '#008A00' : '#D2B200'))+'">'+sentiment.score+'</b>'+
			' &nbsp; <span title="Magnitude is the strength of sentiment regardless of score, ranges from 0 to infinity.">Magnitude:</span> <b>'+sentiment.magnitude+'</b>';
}

function scrollOpenClose(formContainer, callback) {
	var formAnimating = false;
	formContainer[1].style.height = '0px';
	this.show = function () {
		if (formAnimating)
			return false;
		formAnimating = true;
		formContainer[1].style.height = 'auto';
		var height = formContainer[1].offsetHeight;
		formContainer[1].style.height = '0px';
		formContainer[0].style.transition = formContainer[1].style.transition = 'height 0.5s';
		setTimeout(
			function () {
				formContainer[0].style.height = '0px';
				formContainer[1].style.height = height + 'px';
				setTimeout(
					function () {
						formAnimating = false;
						formContainer[1].style.height = 'auto';
						if (typeof callback == 'function') {
							callback();
						}
					}, 500);
			}, 10);
	}
	this.hide = function () {
		if (formAnimating)
			return false;
		formAnimating = true;
		formContainer[0].style.transition = formContainer[1].style.transition = 'height 0.5s';
		formContainer[1].style.height = formContainer[1].offsetHeight + 'px';
		setTimeout(
			function () {
				formContainer[0].style.height = '41px';
				formContainer[1].style.height = '0px';
				setTimeout(
					function () {
						formAnimating = false;
						if (typeof callback == 'function') {
							callback();
						}
					}, 500);
			}, 10);
	}
}

function enforceSingleAssignment(dropdowns, selections) {
	if (dropdowns[0].constructor.name == 'makeSearchDropdown'){
		/*selections = {};
		for (var i = 0; i < dropdowns.length; i++){
			if (typeof selections[ dropdowns[i].value ] != 'undefined' && selections[ dropdowns[i].value ] != i)
				dropdowns[i].setValue(null);
			if (dropdowns[i].value != null)
				selections[ dropdowns[i].value ] = i;
		}*/
	}
	else{
		for (var i = 0; i < dropdowns.length; i++)
			for (var k = 0; k < dropdowns[i].options.length; k++)
				dropdowns[i].options[k].disabled = false;
		//
		for (var i = 0; i < dropdowns.length; i++)
			for (var j = 0; j < dropdowns.length; j++)
				if (dropdowns[i] != dropdowns[j])
					for (var k = 1; k < dropdowns[j].options.length; k++)
						if (dropdowns[j].options[k].value == dropdowns[i].value)
							dropdowns[j].options[k].disabled = true;
	}
}

//	---------------------------------------------------------------------------------------------

function downloadCSV(table, name) {
	var uri = 'data:text/csvl;base64,';
	var base64 = function (s) {
		return window.btoa(unescape(encodeURIComponent(s)))
	};
	var tmp = [], ctx = '';
	/*/if (!table.nodeType)	table = document.getElementById(table);
	for (var i = 0; i < table.tHead.rows[0].cells.length; i++)
		tmp.append(table.tHead.rows[0].cells[i].innerText);
	ctx += tmp.join(',')+'\n';
	//*/
	for (var i = 0; i < table.rows.length; i++){
		tmp = [];
		for (var j = 0; j < table.rows[i].cells.length; j++)
			tmp.push((table.rows[i].cells[j].className.indexOf('decrease') > -1 ? '-' : '') +
				table.rows[i].cells[j].innerText.replace(/\n/g, ''));
		ctx += tmp.join(',')+'\n';
	}
	//var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
	var link = document.createElement("a");
	link.target = '_blank';
	link.download = name;
	link.href = uri + base64(ctx);
	link.click();
}

function downloadExcel(table, name) {
	var uri = 'data:application/vnd.ms-excel;base64,';
	var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
	var base64 = function (s) {
		return window.btoa(unescape(encodeURIComponent(s)))
	};
	var format = function (s, c) {
		return s.replace(/{(\w+)}/g, function (m, p) {
			return c[p];
		})
	};
	//if (!table.nodeType)	table = document.getElementById(table);
	var newTbl = elem('table');
	for (var i = 0; i < table.rows.length; i++){
		tmp = elem('tr');
		for (var j = 0; j < table.rows[i].cells.length; j++)
			tmp.appendChild(elem('td', (table.rows[i].cells[j].className.indexOf('decrease') > -1 ? '-' : '') +
				table.rows[i].cells[j].innerText.replace(/\n/g, '')));
		newTbl.appendChild(tmp);
	}
	var ctx = {
		worksheet: name || 'Worksheet',
		table: newTbl.innerHTML
	};
	var link = document.createElement("a");
	link.target = '_blank';
	link.download = name;
	link.href = uri + base64(format(template, ctx));
	link.click();
}

function downloadSVG(svg, name) {
	//*/
	//	SVG
	var uri = 'data:image/svg;base64,';
	var header = '<?xml version="1.0" encoding="utf-8"?><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" enable-background="new 0 0 ' + svg.parentNode.offsetHeight + ' ' + svg.parentNode.offsetWidth + '" xml:space="preserve" font-family="' + svg.getAttribute('font-family') + '" style="background-color:#FFFFFF;">';
	var base64 = function (s) {
		return window.btoa(unescape(encodeURIComponent(s)))
	};
	var link = document.createElement("a");
	link.target = '_blank';
	link.download = name + '.svg';
	link.href = uri + base64(header + svg.innerHTML + '<style>@font-face{font-family: "Linotte-Regular";src: ' +
		'url("https://www.prodoscore.com/wp-content/themes/prodoscore/fonts/307BC1_0_0.woff2") format("woff2"), ' +
		'url("https://www.prodoscore.com/wp-content/themes/prodoscore/fonts/307BC1_0_0.woff") format("woff");</style></svg>');
	link.click();
	/*/
	//	PNG
	html2canvas(svg,
		{
			height: svg.offsetHeight, width: svg.offsetWidth, background: '#FFFFFF',
			onrendered: function(canvas){
				var link = document.createElement("a");
				link.target = '_blank';
				link.download = name+'.png';
				link.href = canvas.toDataURL('image/png');
				link.click();
			}
		});
	//*/
}

function downloadPNG(canvas, name) {
	//*/
	var link = document.createElement("a");
	link.target = '_blank';
	link.download = name + '.png';
	link.href = canvas.toDataURL('image/png');
	link.click();
	//*/
}

function prevPeriodTxt() {
	var dateFilter = q('select#date-filter')[0];
	if (dateFilter.value == 'yesterday')
		return 'Prev ' + ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'CatUrDay'][(startD + 4) % 7];
	else if (dateFilter.value == 'this-week')
		return 'Prev week';
	else if (dateFilter.value == 'last-7-days')
		return 'Prev 7 days';
	else if (dateFilter.value == 'this-month')
		return 'Prev month';
	else if (dateFilter.value == 'last-30-days')
		return 'Prev 30 days';
	else if (dateFilter.value == 'custom')
		return 'Prev period';
}

//	---------------------------------------------------------------------------------------------

var messages = document.getElementById('messages');
var prevAlert = {
	level: '',
	message: '',
	timeout: false,
	obj: false
};

function showAlert(level, message, timeout) {
	if (prevAlert.level == level && prevAlert.message == message && prevAlert.obj.parentNode != null) {
		clearTimeout(prevAlert.timeout);
		prevAlert.timeout = new hideAlert(prevAlert.obj, typeof timeout != 'undefined' ? timeout : message.length * 300);
		prevAlert.obj.style.opacity = 0;
		setTimeout(
			function () {
				prevAlert.obj.style.opacity = 1;
			}, 200);
		return prevAlert.obj;
	}
	prevAlert.obj = elem('div', message, {
		class: 'message ' + level
	});
	messages.appendChild(prevAlert.obj);
	prevAlert.timeout = new hideAlert(prevAlert.obj, typeof timeout != 'undefined' ? timeout : message.length * 300);
	prevAlert.level = level;
	prevAlert.message = message;
	return prevAlert.obj;
}

function hideAlert(ref, time) {
	var tout = setTimeout(
		function () {
			if (ref.parentNode == null)
				return false;
			ref.style.opacity = 0;
			ref.style.height = 0;
			ref.style.paddingTop = 0;
			ref.style.paddingBottom = 0;
			ref.style.marginTop = 0;
			ref.style.marginBottom = 0;
			setTimeout(
				function () {
					ref.parentNode.removeChild(ref);
					ref = false;
				}, 280);
		}, time);
	ref.onclick = function () {
		clearTimeout(tout);
		ref.style.opacity = 0;
		ref.style.height = 0;
		ref.style.paddingTop = 0;
		ref.style.paddingBottom = 0;
		ref.style.marginTop = 0;
		ref.style.marginBottom = 0;
		setTimeout(
			function () {
				if (!ref)
					return false;
				ref.parentNode.removeChild(ref);
				ref = false;
			}, 280);
	}
	return tout;
}

//	PROGRESS BAR ANIMATION
var loading = document.getElementById('loading');
//let loadingAnim = false,
let smallLoadingAnim = false, smallLoadingStep = 0, loadingStarted = false;
function smallLoading(){
	smallLoadingStep += 0.5;
	if (smallLoadingStep > 25)
		return false;
	//
	loading.style.width = (smallLoadingStep+70)+'%';
	smallLoadingAnim = setTimeout(smallLoading, 500);
}
function eventLogger(state, evt){
	if (state == 'start'){
		if (loadingStarted)
			return false;
		loadingStarted = true;
		//
		loading.className = 'loading';//animate
		loading.style.opacity = 0;
		loading.style.width = '0%';
//console.log('LOADING ['+evt+'] - 1 INITIATE');
		setTimeout(function(){
			loading.className = 'loading animate';
//console.log('LOADING ['+evt+'] - 2 START opacity:1');
			setTimeout(function(){
				loading.style.display = 'block';
				loading.style.opacity = 1;
			}, 1);
			setTimeout(function(){
//console.log('LOADING ['+evt+'] - 3 ANIMATE width:30%');
				loading.style.width = '30%';
			}, 450);
		}, 1);
		setTimeout(function(){
//console.log('LOADING ['+evt+'] - 4 ANIMATE width:70%++');
			loading.style.width = '70%';
			smallLoadingStep = 0;
			smallLoadingAnim = setTimeout(smallLoading, 500);
		}, 502);
	}
	else{
		/*if (loadingAnim)
			return false;
		loadingAnim = true;*/
		//
		clearTimeout(smallLoadingAnim);
//console.log('LOADING ['+evt+'] - 5 FINALIZE width:100%');
		loading.style.width = '100%';
		setTimeout(
			function(){
				clearTimeout(smallLoadingAnim);
				loading.style.opacity = 0;
			}, 650);
		setTimeout(
			function(){
//console.log('LOADING ['+evt+'] - 6 RESET');
				loading.style.display = 'none';
				//
				loading.className = 'loading';
				loading.style.width = '0%';
				//
				//loadingAnim = false;
				loadingStarted = false;
			}, 850);
	}
}


//	Convert the name to a hex color code based on how it sounds like
//	To get a unique color code per person
function usernameIcon(username, nocache) {
	var key = 'icon-' + username.toLowerCase().replace(/ /g, '-');
	if (localStorage[key] == undefined) {
		username = username.split(/[\s,_]/);
		var un = 'NU';
		try {
			un = username[0][0];
			un = (un + (username.length == 1 ? username[0][1] : username[1][0])).toUpperCase();
		} catch (e) {}
		if (username.length == 1)
			username[0] = soundex(username[0]);
		else
			username[0] = soundex(username[0][0] + username[1]); //+username[0][1]
		//var hue = (parseInt(1.44*(username[0].charCodeAt(0)-65)) + username[0].substring(1)) % 360;
		//console.log(username[0].charCodeAt(0) + ' + ' + username[0].substring(1) + ' = ' + (username[0].charCodeAt(0) + username[0].substring(1)));
		//*/
		//var hue = ((2.5 * username[0].charCodeAt(0))) % 360;//
		var hue = (username[0].charCodeAt(0) * 1.5 + parseInt(username[0].substring(1))) % 360;
		username[0] = hslToRgb(hue / 360, 0.72, 0.58);
		//var hue = ( username[0].charCodeAt(0)*1.5 + parseInt(username[0].substring(1)) ) % 360;
		//console.log(username[0].substring(1));
		//username[0] = hslToRgb( hue / 360, 0.6+(username[0].substring(1)/1500), 0.58 );
		/*/
		var hue = username[0].substring(1) % 360;//
		username[0] = hslToRgb( hue / 360, username[0].charCodeAt(0)/100, 0.58 );
		//*/
		username[2] = [username[0][0], username[0][1], username[0][2]];
		username[0] = [username[0][0].toString(16), username[0][1].toString(16), username[0][2].toString(16)];
		username[0] = ('00' + username[0][0]).substring(username[0][0].length) + ('00' + username[0][1]).substring(username[0][1].length) + ('00' + username[0][2]).substring(username[0][2].length);
		//		if (typeof nocache == 'undefined' || nocache == false)
		//			localStorage[key] = JSON.stringify([un, username[0], username[2], hue]);
		return [un, username[0], username[2], hue];
	} else
		return eval('(' + localStorage[key] + ')');
}

//	Generates a code based on how a word sounda like.
var soundex = function (s) {
	//console.log(s);
	var a = s.toLowerCase().split(''),
		f = a.shift(),
		r = '',
		codes = {
			a: '',
			e: '',
			i: '',
			o: '',
			u: '',
			b: 1,
			f: 1,
			p: 1,
			v: 1,
			c: 2,
			g: 2,
			j: 2,
			k: 2,
			q: 2,
			s: 2,
			x: 2,
			z: 2,
			d: 3,
			t: 3,
			l: 4,
			m: 5,
			n: 5,
			r: 6
		};

	r = f +
		a
		.map(function (v, i, a) {
			return codes[v]
		})
		.filter(function (v, i, a) {
			return ((i === 0) ? v !== codes[f] : v !== a[i - 1]);
		})
		.join('');

	return (r + '000').slice(0, 4).toUpperCase();
};

function hslToRgb(h, s, l) {
	var r, g, b;

	if (s == 0) {
		r = g = b = l;
	} else {
		function hue2rgb(p, q, t) {
			if (t < 0) t += 1;
			if (t > 1) t -= 1;
			if (t < 1 / 6) return p + (q - p) * 6 * t;
			if (t < 1 / 2) return q;
			if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
			return p;
		}

		var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
		var p = 2 * l - q;
		r = hue2rgb(p, q, h + 1 / 3);
		g = hue2rgb(p, q, h);
		b = hue2rgb(p, q, h - 1 / 3);
	}

	return [parseInt(r * 255), parseInt(g * 255), parseInt(b * 255)];
}

//	------------------------------------------------------

Date.prototype.sqlFormatted = function () {
	var yyyy = this.getFullYear().toString();
	var mm = (this.getMonth() + 1).toString();
	var dd = this.getDate().toString();
	return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
};

//	------------------------------------------------------

window.navHistory = {
	current: location.hash
};

window.addEventListener("hashchange", function () {
	window.navHistory.prev = window.navHistory.current;
	window.navHistory.current = location.hash;
}, false);

var breadCrumbViewModel = function () {
	var self = this;
	self.breadCrumbs = [];

	breadCrumbViewModel.addCrumb = function (url, text, active) {
		self.breadCrumbs.push({ 'url': url, 'text': text, 'active': active });
	}

	breadCrumbViewModel.reset = function () {
		self.breadCrumbs = [];
	}

		breadCrumbViewModel.render = function (context) {
			var breadcrumbs, breadcrumbsUL;

			if (context.q('#AppBreadcrumbs')[0]) {
				context.removeChild(context.q('#AppBreadcrumbs')[0]);
			}

			breadcrumbs = document.createElement('DIV');
			breadcrumbsUL = document.createElement('UL');

			breadcrumbs.id = 'AppBreadcrumbs';
			breadcrumbs.className = 'breadcrumbs';

			for ( var i = 0; i < self.breadCrumbs.length; i++ ) {
				var crumb = self.breadCrumbs[i];
				breadcrumbsUL.innerHTML += '<li class="' + (crumb.active ? 'current' : '') + '"><a href="' + crumb.url + '">' + crumb.text + '</a></li>';
			}

			breadcrumbs.appendChild(breadcrumbsUL);
			context.insertBefore(breadcrumbs, context.children[0]);
		}
}

String.prototype.capitalizeFirstLetter = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

// fade out

function fadeOut(el, callback){
	el.style.opacity = 1;

	(function fade() {
	if ((el.style.opacity -= .1) < 0) {
		el.style.display = "none";
			if (typeof callback === "function") {
		callback(el);
		}
	} else {
		requestAnimationFrame(fade);
	}
	})();
}

// fade in

function fadeIn(el, callback, display) {
	el.style.opacity = 0;
	el.style.display = display || "block";

	(function fade() {
	var val = parseFloat(el.style.opacity);
	if (!((val += .1) > 1)) {
		el.style.opacity = val;
		requestAnimationFrame(fade);
	} else {
			if (typeof callback === "function") {
		callback(el);
		}
		}
	})();
}

// Modal model

var modal = function (options = {}) {

	let self = this;

	let defaults = {
		title: null,
		body: null,
		footer: null,
		showX: true,
		destroyOnHide: false
	};

	let opts = Object.assign({}, defaults, options);

	self.setTitle = function () {
		let _ 		= self,
				el 		= document.createElement('DIV'),
				x 		= document.createElement('BUTTON'),
				h 		= document.createElement('H3'),
				txt 	= document.createTextNode(opts.title);

		el.classList.add('modal-title');

		if (opts.title != null) {
			h.appendChild(txt);
			el.appendChild(h);
		}

		if (opts.showX) {
			x.setAttribute('type', 'button');
			x.classList.add('dismiss');
			x.appendChild(document.createTextNode('x'));
			x.addEventListener('click', function () {
				_.hide();
			});
			el.appendChild(x);
		}
		_.el.appendChild(el);
		return el;
	};

	self.setBody = function () {
		if (opts.body == null) return false;
		let _ 		= self,
				el 		= document.createElement('DIV');
		el.classList.add('modal-body');
		el.insertAdjacentHTML('beforeend', opts.body);
		_.el.appendChild(el);
		return el;
	};

	function isElement(o){
		return (
		typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
		o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
		);
	}

	self.setFooter = function () {
		if (opts.footer == null) return false;
		let _ 	= self,
				el	= document.createElement('DIV');
		el.classList.add('modal-footer');
		if ( isElement(opts.footer) ) {
			el.appendChild(opts.footer);
		} else {
			el.insertAdjacentHTML('beforeend', opts.footer);
		}
		_.el.appendChild(el);
		return el;
	};


	self.constructor = function () {
		let el = self.el = document.createElement('DIV');
		el.style.display = 'none';
		el.style.opacity = 0;
		el.classList.add('modal');
		self.setTitle();
		self.setBody();
		self.setFooter();
		document.body.appendChild(el);
	};

	self.destroy = function () {
		let el = self.el;
		el.parentNode.removeChild(el);
	};

	self.show = function () {
		fadeIn(self.el);
	};

	self.hide = function () {
		let callback = null;
		if (opts.destroyOnHide) callback = self.destroy.bind(null);
		fadeOut(self.el, callback);
	};

	self.constructor();

	return self;

};

// Tabs

var Tab = function (options = {}) {

	let self = this;

	let defaults = {
		el: null,
		nav: 'nav',
		onSelectTab: function (tab) {
			return tab;
		}
	};

	let opts = Object.assign({
		tabs: []
	}, defaults, options);

	if (opts.el == null) {
		console.log('Tab: el is required');
		return false;
	}

	self.selectTab = function (i) {
		for (let k = 0; k < opts.tabs.length; k++) {
			opts.tabs[k].trigger.classList.remove('active');
			opts.tabs[k].target.classList.remove('active');
			opts.tabs[k].active = false;
			if ( k == i) {
				opts.tabs[k].trigger.classList.add('active');
				opts.tabs[k].target.classList.add('active');
				opts.tabs[k].active = true;
				opts.onSelectTab(opts.tabs[k]);
			}
		}

	}

	self.getTabs = function () {
		return opts.tabs;
	}

	self.bindNav = function () {
		let nav 			= opts.el.querySelector(opts.nav),
			triggers	 	= nav.querySelectorAll('a');
		opts.nav = nav;
		for (let i = 0; i < triggers.length; i++) {
			let trigger = triggers[i],
				target	 	= opts.el.querySelector(trigger.hash),
				tab	 		= {};

			tab.active = false;
			tab.trigger = trigger;
			tab.target = target;
			tab.index = i;

			if (trigger.className.indexOf('active') != -1) {
				tab.active = true;
			}

			opts.tabs.push(tab);
		}

		for (let j = 0; j < opts.tabs.length; j++) {
			let tab = opts.tabs[j];

			tab.trigger.onclick = function (e) {
				e.preventDefault();
				if ( !tab.active ) {
					self.selectTab(j);
				}
			}
		}


	}

	self.constructor = function () {
		self.bindNav();
	}

	self.constructor();

	return self;


}

function checkFilled(form) {

	let self = this;

	self.reqFlds = [];

	let submitBtn = form.q('input[type="submit"]')[0],
			reqFlds = form.q('input[data-required="true"]'),
			empty = false;

	self.enableBtn = function() {
		submitBtn.removeAttribute('disabled');
	}

	self.disableBtn = function() {
		submitBtn.setAttribute('disabled', true);
	}

	self.checkFields = function () {

		for (let i = 0; i < self.reqFlds.length; i++) {
			let r = self.reqFlds[i];
			if ( r.value.trim() === "") {
				empty = true;
				self.disableBtn();
				break;
			} else {
				self.enableBtn();
			}
		}
	}

	self.fieldListeners = function () {
		for (let r of self.reqFlds) {
			r.onkeyup = function (e) {
				self.checkFields();
			}
			r.onpaste = function (e) {
				setTimeout(self.checkFields, 10);
			}
		}
	}

	self.getFields = function () {
		self.reqFlds = form.q('input[data-required="true"]');
	}

	self.updateFields = function() {
		empty = false;
		self.getFields();
		self.checkFields();
		self.fieldListeners();
	}


	self.updateFields();

}

function WorkTimeline() {
	var _this = this;
	var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	var self = this;
	var glowingEvent = false;

	var defaults = {
		data: {},
		date: null,
		employee: null,
		dayStart: 9,
		dayEnd: 18,
		timeline: '#timeline',
		timelineInnerClass: 'timeline-inner',
		segmentWidth: 200,
		unit: 20,
		maxHeight: 300,
		displayTitleFor: ['cl', 'tb', 'zc', 'ze', 'bs', 'cll', 'rcc', 'sfc', 'vbc'],
		eventOnClick: null,
		timelineOnScroll: null,
		timelineMouseMove: null,
		timelineInitialized: null
	};

	var prevCal = {
		title: '',
		from: 0,
		to: 0
	};

	this.options = Object.assign({}, defaults, options);

	this.timeline = document.querySelector(this.options.timeline);
	this.timeline.innerHTML = '';
	this.initHeight = 0;
	this.fullHeight = 0;

	this.timelineInner = function () {
		var el = document.createElement('div');
		el.className = _this.options.timelineInnerClass;
		_this.timeline.appendChild(el);
		return el;
	}();

	this.timelineBars = function () {
		var el = document.createElement('div');
		el.className = 'timeline-bars';
		_this.timelineInner.appendChild(el);

		return el;
	}();

	this.timelineEvents = function () {
		var el = document.createElement('div');
		el.className = 'timeline-events';
		_this.timelineInner.appendChild(el);

		return el;
	}();

	this.segmentsInTimeline = this.timeline.offsetWidth / this.options.segmentWidth;

	this.workHours = [];
	this.Events = [];
	this.EventIndex = {};

	function validateTimes() {
		if (self.options.dayStart >= self.options.dayEnd) {
			throw 'dayStart should be before dayEnd';
		} else if (self.options.dayStart < 0) {
			throw 'dayStart cannot be less than 0';
		} else if (self.options.dayEnd > 24) {
			throw 'dayEnd cannot be greater than 24';
		}
	}

	this.timeToDecimalTime = function timeToDecimalTime(time) {
		var t = time.split(':');
		var h = parseInt(t[0]);
		var m = parseInt(t[1]);
		var z = m / 60;
		return h + z;
	};

	this.decimalTimeToTime = function (decimalTime) {

		var hrs = Math.floor(Math.abs(decimalTime));
		var min = Math.floor(Math.abs(decimalTime) * 60 % 60);

		return (hrs < 10 ? "0" : "") + hrs + ":" + (min < 10 ? "0" : "") + min;
	};

	this.minutesToWidth = function minutesToWidth(minutes) {
		return Math.max(minutes * (self.options.segmentWidth / 30), self.options.unit);
	};

	this.timeToX = function timeToX(time) {
		return (time - self.options.dayStart) * 2 * self.options.segmentWidth;
	};

	this.xToTime = function (x) {
		return x / (2 * self.options.segmentWidth) + self.options.dayStart;
	};

	function setDimensions() {
		var height = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

		self.timelineInner.style.width = (self.workHours.length) * self.options.segmentWidth + 'px';
		self.timelineInner.style.height = (height ? height : self.options.unit * 12) + 'px';
		self.timeline.style.height = (height ? height : self.options.unit * 12) + 5 + 'px';
		self.timelineBars.style.width = self.timelineInner.style.width;
		self.timelineBars.style.height = self.timelineInner.style.height;
		self.timelineBars.style.position = 'absolute';
		self.timelineBars.style.top = 0;
		self.timelineBars.style.left = 0;

		self.timelineEvents.style.width = self.timelineInner.style.width;
		self.timelineEvents.style.height = self.timelineInner.style.height;
		self.timelineEvents.style.position = 'absolute';
		self.timelineEvents.style.top = 0;
		self.timelineEvents.style.left = 0;
	}

	function drawBars() {
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			for (var _iterator = self.workHours[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var hour = _step.value;

				var el = document.createElement('DIV');
				var label = document.createElement('SPAN');

				el.className = 'segment';
				el.style.width = self.options.segmentWidth + 'px';

				if (Number.isInteger(hour.id)) {
					el.classList.add('hour');
				} else {
					el.classList.add('hour-half');
				}

				label.appendChild(document.createTextNode(hour.label));
				el.appendChild(label);
				self.timelineBars.appendChild(el);
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}
	}

	function drawEvent(id, product, eventData) {

		var decimalStartTime = self.timeToDecimalTime(mtsToHrs(eventData.time[0]));
		var decimalEndTime = self.timeToDecimalTime(mtsToHrs(eventData.time[1]));

		eventData.relTime = [eventData.time[0], eventData.time[1]];

		/*if (eventData.time_range !== 0 && eventData.relTime[0] !== eventData.relTime[1]) {
			if (eventData.time_range === -1 && decimalEndTime > self.options.dayStart) {
				decimalStartTime = self.options.dayStart;
				eventData.relTime[0] = decimalStartTime * 60;
			} else if (eventData.time_range === 1 && decimalStartTime < self.options.dayEnd) {
				decimalEndTime = self.options.dayEnd;
				eventData.relTime[1] = decimalEndTime * 60;
			} else {
				return false;
			}
		}*/

		if (decimalEndTime < self.options.dayStart || decimalStartTime > self.options.dayEnd) {
			return false;
		}

		var el = document.createElement('DIV');
		var icon = document.createElement('I');

		var width = 0;
		var x = 0;
		var y = 0;

		var label = eventData.title;
		var flag = eventData.flag;

		var classes = product + ' ' + (flag ? 'exclude' : 'include') + (decimalStartTime < self.options.dayStart ? ' right-align' : '');

		x = self.timeToX(decimalStartTime);

		width = self.minutesToWidth(Math.min(eventData.relTime[1], self.options.dayEnd * 60) - eventData.relTime[0]);

		if (product === 'ch') {
			try {
				label = atob(eval('(' + eventData + ')')['data'].replace(/_/g, '/').replace(/-/g, '+'));
			} catch (e) {}
		}

		if (product == 'cl') {
			if (prevCal.title == label && prevCal.from == eventData.time[0] && prevCal.to == eventData.time[1]) {
				return false;
			}
			prevCal = {
				title: label,
				from: eventData.time[0],
				to: eventData.time[1]
			};
		}

		el.style.position = 'absolute';
		el.style.top = y + 22 + 'px';
		el.style.left = x + 'px';
		el.style.width = width + 'px';
		el.style.height = self.options.unit + 'px';

		el.dataset.id = id;

		el.title = label + '\n' + mtsToHrs(eventData.time[0]) + (eventData.time[0] != eventData.time[1] ? ' to ' + mtsToHrs(eventData.time[1]) : '');

		icon.className = 'icon icon-' + product;
		el.appendChild(icon);

		if (label !== '' && self.options.displayTitleFor.includes(product)) {
			var lbl = document.createElement('SPAN');
			lbl.appendChild(document.createTextNode(label));
			el.appendChild(lbl);
		}

		el.className = 'event ' + classes;

		el.onclick = function () {
			self.options.eventOnClick(id, el, product);
			el.glow();
		};
		el.glow = function(){
			if (glowingEvent)
				glowingEvent.className = glowingEvent.className.replace(' glow', '');
			//
			glowingEvent = el;
			glowingEvent.className += ' glow';
			/*setTimeout(function(){
				el.className = el.className.replace(' glow', '');
			}, 800);*/
		};
		el.unglow = function(){
			//glowingEvent = el;
			el.className = el.className.replace(' glow', '');
		};

		self.timelineEvents.appendChild(el);

		try {
			new contextMenu(el, flag == 0 ? new flagMenu(id, self.options.data, self.options.date, self.options.employee, el) : //else
			new unflagMenu(id, self.options.data, self.options.date, self.options.employee, el));
		} catch (e) {
			console.warn(e);
		}

		return el;
	}

	function checkOverlap(rect1, rect2) {
		rect1 = rect1.getBoundingClientRect();
		rect2 = rect2.getBoundingClientRect();
		var overlap = !(rect1.right <= rect2.left || rect1.left >= rect2.right || rect1.bottom <= rect2.top || rect1.top >= rect2.bottom);
		return overlap;
	}

	function populateEvents() {
		let minTime = 999;
		for (var d in self.options.data) {
			var e = self.options.data[d];

			var eventEl = drawEvent(d, e.product, e);

			if (eventEl) {
				var ev = {};
				ev.id = d;
				ev.el = eventEl;
				self.Events.push(ev);
				self.EventIndex[d] = eventEl;
				if (e.time[0] > 0 && minTime > e.time[0]){
					minTime = e.time[0];
				}
			}
		}

		if (self.Events.length === 0) {
			self.timeline.addClass('no-data');
		}
		else {
			self.scrollToTime((minTime / 60) + 1);
			//console.log('min-time: '+minTime);
			self.timeline.removeClass('no-data');
			// vertically align
			for (var z = 0; z < 2; z++) {
				for (var i = 0; i < self.Events.length; i++) {
					for (var j = 0; j < self.Events.length; j++) {
						if (i === j) {
							continue;
						}

						if (checkOverlap(self.Events[i].el, self.Events[j].el)) {
							var top = parseInt(self.Events[j].el.style.top) + self.options.unit;
							self.Events[j].el.style.top = top + 'px';
							self.fullHeight = Math.max(self.fullHeight, top);
						}
					}
				}
			}

			if (self.initHeight < self.fullHeight) {
				setDimensions(Math.min(self.fullHeight + self.options.unit, self.options.maxHeight));
			}
		}
	}

	this.glowEvent = function (id) {
		self.EventIndex[id].glow();
	};
	this.unglowEvent = function (id) {
		self.EventIndex[id].unglow();
	};

	this.scrollToTime = function (dcTime) {
		if (dcTime > _this.options.dayEnd || dcTime < _this.options.dayStart) {
			return false;
		}

		var self = _this;

		var currentScrollPos = self.timeline.scrollLeft;
		var segmentsInTimeline = self.timeline.offsetWidth / self.options.segmentWidth;
		var scrollTo = ((dcTime - self.options.dayStart) * 2 - segmentsInTimeline / 2) * self.options.segmentWidth;

		var scrollDelta = currentScrollPos - scrollTo;

		var init = null;

		var disp = scrollTo - currentScrollPos;

		function animate(timestamp) {
			timestamp = timestamp || new Date().getTime();

			var runtime = timestamp - init;
			var progress = runtime / 300;
			progress = Math.min(progress, 1);

			var h = currentScrollPos + Math.floor(disp * progress);

			self.timeline.scrollLeft = h;

			if (runtime < 300) {
				window.requestAnimationFrame(function (timestamp) {
					animate(timestamp);
				});
			} else {
				self.timeline.scrollLeft = scrollTo;
			}
		}

		window.requestAnimationFrame(function (timestamp) {
			init = timestamp || new Date().getTime();
			animate(timestamp);
		});
	};

	// init
	(function () {

		try {
			validateTimes();
		} catch (e) {
			console.error(e);
			return false;
		}

		// work hours

		var _loop = function _loop(i) {
			var hour = {},
			hourHalf = {};

			hour.id = self.options.dayStart + i;
			hour.label = function () {
				if (hour.id < 10) {
					return '0' + hour.id + ':00';
				} else {
					return hour.id + ':00';
				}
			}();

			self.workHours.push(hour);

			if (hour.id <= self.options.dayEnd) {
				hourHalf.id = hour.id + .5;
				hourHalf.label = function () {
					var l = Math.floor(hourHalf.id);
					if (l < 10) {
						return '0' + l + ':30';
					} else {
						return l + ':30';
					}
				}();

				self.workHours.push(hourHalf);
			}
		};

		for (var i = 0; i <= self.options.dayEnd - self.options.dayStart; i++) {
			_loop(i);
		}

		self.initHeight = self.options.unit * 12;

		setDimensions();
		drawBars();
		populateEvents();

		self.timeline.addEventListener('scroll', function (e) {
			var sc_left = self.timeline.scrollLeft;
			var decimaltimeAtScrollPos = [self.xToTime(sc_left), self.xToTime(sc_left + self.timeline.offsetWidth)];
			var timeAtScrollPos = [self.decimalTimeToTime(decimaltimeAtScrollPos[0]), self.decimalTimeToTime(decimaltimeAtScrollPos[1])];

			self.timelineBars.style.top = self.timeline.scrollTop + 'px';
			if (self.options.timelineOnScroll) {
				self.options.timelineOnScroll(timeAtScrollPos, decimaltimeAtScrollPos, sc_left, e);
			}
		});

		if (self.options.timelineMouseMove) {
			self.timeline.onmousemove = function (e) {
				self.options.timelineMouseMove(e);
			};
		}

		if (self.options.timelineInitialized) {
			self.options.timelineInitialized(self);
		}
	})();
}
