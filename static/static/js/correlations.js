
var delayedAutoReLoadCr = false, connectionRetryCr = false;
var correlationsContext = false;
var correlationData, nodeData, correlationDetails, correlationDateRange;

var taxonomyDictionary = {'consumer_goods': 'Item', 'events': 'Event', 'locations': 'Location', 'organizations': 'Organization', 'others': 'Other', 'persons': 'Person', 'work_of_arts': 'Work'};
var colorDictionary = {'consumer_goods': 'hsl(1, 72%, 58%)', 'events': 'hsl(40, 72%, 50%)', 'locations': 'hsl(60, 85%, 42%)', 'organizations': 'hsl(120, 72%, 58%)', 'others': 'hsl(180, 85%, 42%)', 'persons': 'hsl(220, 72%, 58%)', 'work_of_arts': 'hsl(260, 72%, 58%)'};
//var colorDictionary = {'consumer_goods': 'hsl(0, 72%, 58%)', 'events': 'hsl(20, 72%, 50%)', 'locations': 'hsl(40, 85%, 45%)', 'organizations': 'hsl(140, 72%, 58%)', 'others': 'hsl(180, 72%, 58%)', 'persons': 'hsl(220, 72%, 58%)', 'work_of_arts': 'hsl(260, 72%, 58%)'};
var correlationsToolTip = q('#contentCorrelations #entities_collections .chartTooltip')[0];
var bubbleChart = false;

var graphData = {'nodes': [], 'edges': []};
var nodesIndex = {};
var selectedNodes = [];
var graphLinks = [];
var correlationsIntExtFilter = '';
var correlationAjaxRequests = [false, false, false, false];

var correlationsSelTabIndexLatch = false, correlationsSelTabIndex = 0;

var corrilationsInitialized = false;
function initializeCorrelations(context, params, etypeFilters, connDegree){
	//if (corrilationsInitialized)	return false;
	// -----------------------------------------------------------------------------------------------------
	etypeFilters = context.q('.employee-filters_left .visibility-toggle input[type="checkbox"][name^="etype-"]');
	var filterByEtype = function(){
		selETypes = [];
		for (var i = 0; i < etypeFilters.length; i++){
			if (etypeFilters[i].checked)
				selETypes.push(etypeFilters[i].name.split('-')[1]+'s');
		}
		bubbleChart.filterEtype(selETypes);
	}
	for (var i = 0; i < etypeFilters.length; i++){
		etypeFilters[i].onclick = filterByEtype;
		etypeFilters[i].parentNode.style.color = colorDictionary[ etypeFilters[i].name.split('-')[1]+'s' ];
	}
	// -----------------------------------------------------------------------------------------------------
	var nsizeFilters = context.q('.employee-filters_left .visibility-toggle input[type="checkbox"][name^="nsize-"]');
	var filterByNSize = function(){
		this.checked = true;
		for (var i = 0; i < nsizeFilters.length; i++)
			if (this != nsizeFilters[i])
				nsizeFilters[i].checked = false;
		//
		bubbleChart.nodeResize(this.name.split('-')[1]);
	}
	for (var i = 0; i < nsizeFilters.length; i++)
		nsizeFilters[i].onclick = filterByNSize;
	// -----------------------------------------------------------------------------------------------------
	var connFilters = context.q('.employee-filters_left .visibility-toggle input[type="checkbox"][name^="conn-"]');
	var filterConnDegree = function(){
		this.checked = true;
		for (var i = 0; i < connFilters.length; i++)
			if (this != connFilters[i])
				connFilters[i].checked = false;
		//
		bubbleChart.chgConnDegree(this.name.split('-')[1]);
	}
	for (var i = 0; i < connFilters.length; i++)
		connFilters[i].onclick = filterConnDegree;
	// -----------------------------------------------------------------------------------------------------
	var entropyFilters = context.q('.employee-filters_left .visibility-toggle input[type="checkbox"][name^="top-"]');
	var filterEntropy = function(){
		this.checked = true;
		for (var i = 0; i < entropyFilters.length; i++)
			if (this != entropyFilters[i])
				entropyFilters[i].checked = false;
		//
		params.limit = this.name.split('-')[1];
		loadCorrelations(context, params);
	}
	for (var i = 0; i < entropyFilters.length; i++)
		entropyFilters[i].onclick = filterEntropy;
	// -----------------------------------------------------------------------------------------------------
	var intExtFilters = context.q('.employee-filters_right .visibility-toggle input[type="checkbox"][name^="channel-"]');
	intExtFilters[0].onclick = intExtFilters[1].onclick = function(){
		if (!intExtFilters[0].checked && !intExtFilters[1].checked){
			if (this == intExtFilters[0])
				intExtFilters[1].checked = true;
			else
				intExtFilters[0].checked = true;
		}
		loadCorrelations(context, params);
	};
	if (intExtFilters[0].checked && !intExtFilters[1].checked)
		correlationsIntExtFilter = '&intext=1';
	else if (!intExtFilters[0].checked && intExtFilters[1].checked)
		correlationsIntExtFilter = '&intext=2';
	else if (!intExtFilters[0].checked && !intExtFilters[1].checked)
		correlationsIntExtFilter = '';
	// -----------------------------------------------------------------------------------------------------
	var entitiesSpace = context.q('#entities_space')[0];
	var showTextLables = context.q('#contentCorrelations input[type="checkbox"][name="text-lables"]')[0];
	showTextLables.onclick = function(){
		if (showTextLables.checked)
			entitiesSpace.addClass('show-text');
		else
			entitiesSpace.removeClass('show-text');
	}
	// -----------------------------------------------------------------------------------------------------
	var goFullScreen = context.q('#contentCorrelations label.visibility-toggle.full-screen')[0];
	goFullScreen.onclick = function(){
		entitiesSpace.q('svg')[0].webkitRequestFullScreen();
		/*if (goFullScreen.checked){
			entitiesSpace.addClass('fullscreen');
			entitiesSpace.webkitRequestFullScreen();
		}
		else{
			entitiesSpace.removeClass('fullscreen');
			entitiesSpace.webkitExitFullscreen();
		}*/
	}
}

function loadCorrelations(context, params, loadDetailOf){
	//
	correlationsContext = context;
	clearTimeout(delayedAutoReLoadCr);
	eventLogger('start', 'correlations');
	startD = DateToShort(document.forms[0].from_date.value);
	endD = DateToShort(document.forms[0].to_date.value);
	bubbleChart = false;
	//
	var etypeFilters, connDegree;
	initializeCorrelations(context, params, etypeFilters, connDegree);
	/*if (endD - startD > 7){
		endD = startD + 7;
		document.forms[0].to_date.value = DateFromShort(endD).sqlFormatted();
		showAlert('warning', 'Please select one week duration at a time for better performance.', 2400);
	}*/
	correlationDateRange = startD+'-'+endD;
	//
	var entitiesSpace = context.q('#entities_space')[0];
	correlationsToolTip = context.q('#entities_collections .chartTooltip')[0];
	//
	var empEngagement = context.q('table#empEngagement tbody')[0];
	var prodUsage = context.q('table#prodUsage tbody')[0];
	//
	correlationDetails = context.q('#correlationDetails')[0];
	correlationDetails.style.display = 'none';
	context.q('#chart_correlations')[0].style.display = 'none';

	/*var correlationDataToggles = context.q('.correlationsOrgData-wrap a');
	for (var i = 0; i < correlationDataToggles.length; i++) {
		var el = correlationDataToggles[i];
		(function (el) {
			el.onclick = function (e) {
				var elTable = context.q(el.getAttribute('href'))[0];
				var height = elTable.offsetHeight;

				for (var j = 0; j < correlationDataToggles.length; j++) {
					correlationDataToggles[j].removeClass('active');
					var _table = context.q(correlationDataToggles[j].getAttribute('href'))[0];
					_table.removeClass('active');
				}
				el.addClass('active');
				elTable.addClass('active');
				return false;
			}
		})(el);
	}*/
	if (startD > endD){
		delayedAutoReLoadCr = setTimeout(function(){
			endD = startD;
			var tmp = DateFromShort(endD).toString().split(' ');//-1
			document.forms[0].to_date.value = tmp[3]+'-'+monthsReverse[tmp[1]]+'-'+tmp[2];
			loadCorrelations(context, params);
		}, 2400);
		showAlert('warning', 'Please set a To date after the From date.', 2400);
		return false;
	}
	//
	// ========================================================================================
	//	TABS - SIMPLE VIEW
	var tables = context.q('#entities_collections #simpleView table tbody');
	var viewTabs = new Tab({
		el: context.q('#entities_collections .card-wrap')[0],
		onSelectTab: function (tab){
			if (tab.index == 1 && !bubbleChart)
				viewTabs.selectTab(0);
			//
			var rowHighlighted = tables[0].q('tr.highlight');
			if (tab.index == 1 && rowHighlighted.length > 0){
				tables[0].scrollTop = rowHighlighted[0].offsetTop - (tables[0].offsetHeight / 2);
				//tables[0].q('tr.highlight')[0].focus();
			}
			if (!correlationsSelTabIndexLatch)
				correlationsSelTabIndex = tab.index;
		}
	});
	correlationsSelTabIndexLatch = true;
	viewTabs.selectTab(0);
	setTimeout(function(){correlationsSelTabIndexLatch = false;}, 2500);
	// ========================================================================================
	var progress = 0;
	graphData = {'nodes': [], 'edges': []};
	var checkpoint = function(){
		progress += 1;
		if (progress < 2)
			return false;
		//
		/*if (bubbleChart){
			bubbleChart.setGraphData(graphData, selectedNodes, []);
			bubbleChart.redraw();
			/*if (typeof loadDetailOf != 'undefined')
				tables[0].q('tr[data-id="'+loadDetailOf+'"]')[0].onclick();* /
		}
		else{*/
		let initializeBubbleChart = function(){
			viewTabs.selectTab(0);
			if (entitiesSpace.offsetHeight == 0){
				setTimeout(initializeBubbleChart, 250);
				return false;
			}
			bubbleChart = drawNLPBubbleChart(entitiesSpace, graphData, nodesIndex, selectedNodes, graphLinks, context.q('#searchCorrelations')[0], tables[0], tables[1], etypeFilters,
				function(){
					if (typeof loadDetailOf != 'undefined'){
						let tr = tables[0].q('tr[data-id="'+loadDetailOf+'"]');
						if (tr.length > 0)
							tr[0].onclick();
					}
					tables[1].innerHTML = '';
					setTimeout(function(){
						viewTabs.selectTab(correlationsSelTabIndex);
					}, 120);
				});
		};
		initializeBubbleChart();
		//}
	};
	// ========================================================================================
	if (correlationAjaxRequests[0]){
		correlationAjaxRequests[0].abort();
		correlationAjaxRequests[1].abort();
	}
	correlationAjaxRequests[0] = new ajax(base_url+'correlations-ajax/'+
			'?fromdate='+DateFromShort(startD).sqlFormatted()+
			'&todate='+DateFromShort(endD).sqlFormatted()+
			correlationsIntExtFilter+
			(typeof params.limit=='undefined'?'':'&limit='+params.limit), {
		callback: function(data){
			context.q('#searchCorrelations')[0].value = '';
			empEngagement.innerHTML = '<td colspan="3"><i>Select a topic/contact</i></td>';
			prodUsage.innerHTML = '<td colspan="3"><i>Select a topic/contact</i></td>';
			//
			correlationData = eval('('+data.responseText+')');
			nodeData = {};
			nodesIndex = {};
			//
			for (etype in correlationData){
				for (eid in correlationData[etype]){
					nodeData[eid] = correlationData[etype][eid];
					nodeData[eid].type = etype;
					tmp = correlationData[etype][eid].title || correlationData[etype][eid].name || correlationData[etype][eid].email || correlationData[etype][eid].phone;
					if (tmp != null){
						tmp = tmp.length;
						if (tmp < 26 && tmp > 0){
							if (etype == 'persons'){
								nodesIndex[eid] = {'id': eid, 'group': etype, 'significance': correlationData[etype][eid].significance, 'label': 
									(correlationData[etype][eid].name || correlationData[etype][eid].email || correlationData[etype][eid].phone),
									//'name': correlationData[etype][eid].name, 'email': correlationData[etype][eid].email, 'phone': correlationData[etype][eid].phone
									/*(correlationData[etype][eid].name || '')+
									(correlationData[etype][eid].email ? '<br/>&lt;'+correlationData[etype][eid].email+'&gt;':'')+
									(correlationData[etype][eid].phone ? '<br/>'+correlationData[etype][eid].phone:'')*/
								};
								graphData.nodes.push(nodesIndex[eid]);
								selectedNodes.push(eid);
							}
							else if (etype == 'others'){
								nodesIndex[eid] = {'id': eid, 'group': etype, 'significance': correlationData[etype][eid].significance, 'label': correlationData[etype][eid].title};
								graphData.nodes.push(nodesIndex[eid]);
								selectedNodes.push(eid);
							}
							else{
								nodesIndex[eid] = {'id': eid, 'group': etype, 'significance': correlationData[etype][eid].significance, 'label': correlationData[etype][eid].title};
								graphData.nodes.push(nodesIndex[eid]);
								selectedNodes.push(eid);
							}
						}
					}
				}
			}
			eventLogger('end', 'correlations');
			checkpoint();
		},
		fallback: function(xmlhttp){
			if (xmlhttp.status == 500)
				showAlert('error', 'Server Error.');
			else
				showAlert('warning', 'Couldn\'t connect to server. Please check your internet connection and retry.');
			//	Display warning, Invalidate cache and retry same in 2.4 seconds
			clearTimeout(connectionRetryCr);
			connectionRetryCr = setTimeout(
				function(){
					loadCorrelations(context, params);
				}, 1200);
		}
	});
	// ========================================================================================
	correlationAjaxRequests[1] = new ajax(base_url+'correlations-graph/?fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted()+correlationsIntExtFilter, {
		callback: function(data){
			//correlationsGraph = JSON.parse(data.responseText);
			graphData.edges = JSON.parse(data.responseText);
			checkpoint();
		}
	});
	// ========================================================================================
}

var NLPMouseoverCircle, NLPSelectedCircle = false;
function d3ChartTooltipOn(){
	var e = window.event;
	if (e.target.nodeName == 'circle' || e.target.nodeName == 'text'){
		let elem = e.target.parentNode;
		correlationsToolTip.innerHTML =  '<small> '+taxonomyDictionary[elem.getAttribute('data-type')]+'</small><br/>'+
								'<b>'+elem.getAttribute('data-title')+'</b>'+': '+elem.getAttribute('data-significance');
		//
		correlationsToolTip.style.top = (e.clientY-(correlationsToolTip.offsetHeight*1)-10)+'px';
		correlationsToolTip.style.left = (e.clientX-(correlationsToolTip.offsetWidth/2))+'px';
		//
		correlationsToolTip.style.display = 'block';
		correlationsToolTip.style.opacity = 1;
		correlationsToolTip.style.transform = 'scale(1)';
		//
		//if (NLPSelectedCircle != false && NLPSelectedCircle.group == elem.getAttribute('data-type') && NLPSelectedCircle.label == elem.getAttribute('data-title')){}
		elem.querySelector('circle').setAttribute('stroke', '#4f4f4f');
		//elem.querySelector('text').setAttribute('fill', '#ffff88');
		NLPMouseoverCircle = elem;
		//console.log(NLPSelectedCircle);
		//elem.style.stroke = '#4F4F4F';
	}
}
function d3ChartTooltipOff(){
	correlationsToolTip.style.opacity = 0;
	correlationsToolTip.style.transform = 'scale(0.1)';
	//
	if (NLPMouseoverCircle){
		NLPMouseoverCircle.querySelector('circle').setAttribute('stroke', '#fff');
		//NLPMouseoverCircle.querySelector('text').setAttribute('fill', '#ffffff');
	}
	//NLPMouseoverCircle.style.stroke = '#FFFFFF';
}

function drawNLPBubbleChart(entitiesSpace, graphData, nodesIndex, selectedNodes, graphLinks, searchCorrelations, tableEntities, tableRelatedEntities, etypeFilters, callback){
	/*if (entitiesSpace.offsetHeight == 0){
		setTimeout(function(){drawNLPBubbleChart(entitiesSpace, graphData, nodesIndex, selectedNodes, graphLinks, searchCorrelations, tableEntities, tableRelatedEntities, etypeFilters, callback)}, 250);
		return false;
	}*/
	var selRow = false;
	var sizeMode = 'same';
	var connectionDegree = 'first';
	//var selID = false;
	entitiesSpace.innerHTML = '';
	var svg = d3.select(entitiesSpace).append('svg').attr('width', entitiesSpace.offsetWidth).attr('height', entitiesSpace.offsetHeight-1)
		.call(d3.zoom().on("zoom", function () {
			//if (d3.event.sourceEvent.ctrlKey) //else return true;
			svg.attr("transform", "translate(" + d3.event.transform.x + ', ' + d3.event.transform.y + ")" + " scale(" + d3.event.transform.k + ")")
		}))
		.append("g");
	//var graphLinks = [];
	//
	var d3Chart = d3.forceSimulation(graphData.nodes)
		.force('charge', d3.forceManyBody().strength(function(d) { return sizeMode=='same' ? -240 : (-36 * Math.sqrt(d.significance / Math.PI)); }))
		.force('link', d3.forceLink(graphLinks).distance(function(d) {
			return sizeMode=='same' ? 240 : 
				(	(160 - d.strength) +
					//50 + //d.kind=='first'?25:(d.kind=='second'?50:80) +//15
					(16 * Math.sqrt(d.source.significance / Math.PI)) + (16 * Math.sqrt(d.target.significance / Math.PI))	);//2 * 
		}))
		.force('x', d3.forceX())
		.force('y', d3.forceY())
		.alphaTarget(1)
		.on('tick', ticked);
	var group = svg.append("g").attr("transform", "translate(" + entitiesSpace.offsetWidth / 2 + "," + entitiesSpace.offsetHeight / 2 + ")"),
		link = group.append("g")
			.attr('stroke-width', 2).attr('stroke', '#3366cc')
			.selectAll(".link"),
		node = group.append("g")
			.selectAll(".node");
	function restart() {
		// Apply the general update pattern to the nodes.
		node = node.data(graphData.nodes, function(d) { return d.id;});
		node.exit().remove();
		for (var i in node._groups[0]) if (arrayIgnore.indexOf(i) == -1)
			node._groups[0][i].innerHTML = '';
		//
		node = node.enter()
			.append('g')
			.attr('onmouseover', 'd3ChartTooltipOn();')
			.attr('onmouseout', 'd3ChartTooltipOff();')
			.attr('data-title', function(d) { return d.label; })
			.attr('data-significance', function(d) { return d.significance; })
			.attr('data-type', function(d) { return d.group; })
			.call(d3.drag()
				.on("start", dragstarted)
				.on("drag", dragged)
				.on("end", dragended))
			.on('click', clicked).merge(node);
		node.append('circle')
			//.attr('fill', function(d) { return '#'+usernameIcon(d.group)[1]; })
			.attr('fill', function(d) { return colorDictionary[ d.group ]; })
			.attr('stroke', '#fff').attr('stroke-width', .5)
			//.attr('class', function(d) { d.id == selID ? 'selected' : '' })
			.attr('r', function(d) { return sizeMode=='same' ? 40 : (8 * Math.sqrt(d.significance / Math.PI)); });
		node.append('text')
			.text(function(d) { return d.label; })
			.attr('fill', '#FFFFFF')
			.attr('stroke', '#808080').attr('stroke-width', .25)
			.attr('font-size', function(d){ return (sizeMode=='same' ? 12.5 : (12 + (d.significance/40))) / Math.sqrt(d.label.length / 10); })
			.attr('text-anchor', 'middle');
		node;
		//
		// Apply the general update pattern to the links.
		link = link.data(graphLinks, function(d) {
			return d.source.id + "-" + d.target.id;
		});
		link.exit().remove();
		link = link.enter().append('line').merge(link);
		// Update and restart the d3Chart.
		d3Chart.nodes(graphData.nodes);
		d3Chart.force('link').links(graphLinks);
		d3Chart.alpha(1).restart();
		// selRow = false;
	}
	function ticked(e) {
		node.attr('style', function(d) {
			if (selectedNodes.length == 0 || selectedNodes.indexOf(d.id) > -1)
				return 'opacity:1;';
			else
				return 'opacity:0.3; filter:grayscale(40%);';
		}).select('text')
			.attr('fill', function(d) {
				if (NLPSelectedCircle == d)
					return '#ffff44';
				else
					return '#ffffff';
			});
		//
		node.attr('transform', function(d) {
				return 'translate(' + d.x + ', ' + d.y + ')';
			});
		link.attr('x1', function(d) { return d.source.x; })
			.attr('y1', function(d) { return d.source.y; })
			.attr('x2', function(d) { return d.target.x; })
			.attr('y2', function(d) { return d.target.y; })
			.attr('stroke', function(d) {
				return 'rgba(51, 102, 204, '+(d.strength/100)+')';
			});
	}
	function clicked(d){
		d.tr.onclick();
	}
	function linkRelatedNodes(id){
		graphLinks = [];
		selectedNodes = [id];
		for (tid in graphData.edges[id]){
			if (graphData.edges[id][tid] > 25 && typeof nodesIndex[tid] != 'undefined'){
				if (selectedNodes.indexOf(tid) == -1)
					selectedNodes.push(tid);
				graphLinks.push({'source': nodesIndex[id], 'target': nodesIndex[tid], 'kind': 'first', 'strength': graphData.edges[id][tid]});
				if (connectionDegree == 'second'/* || connectionDegree == 'third'*/)
					for (tid2 in graphData.edges[tid]){
						if (graphData.edges[tid][tid2] > 49 && typeof nodesIndex[tid2] != 'undefined'){
							if (selectedNodes.indexOf(tid2) == -1)
								selectedNodes.push(tid2);
							graphLinks.push({'source': nodesIndex[tid], 'target': nodesIndex[tid2], 'kind': 'second', 'strength': graphData.edges[tid][tid2]});
						}
					}
			}
		}
		if (connectionDegree == 'second')
			for (tid in graphData.edges)
				if (typeof nodesIndex[tid] != 'undefined' && typeof graphData.edges[tid][id] != 'undefined' && graphData.edges[tid][id] > 49){
					if (selectedNodes.indexOf(tid) == -1)
						selectedNodes.push(tid);
					graphLinks.push({'source': nodesIndex[tid], 'target': nodesIndex[id], 'kind': 'zeroth', 'strength': graphData.edges[tid][id]});
				}
		//
		restart();
	}
	function dragstarted(d) {
		if (!d3.event.active)
			d3Chart.alphaTarget(0.3).restart();
		d.fx = d.x;
		d.fy = d.y;
	}
	function dragged(d) {
		d.fx = d3.event.x;
		d.fy = d3.event.y;
		//
		var e = window.event;
		correlationsToolTip.style.top = (e.clientY-(correlationsToolTip.offsetHeight*1)-10)+'px';
		correlationsToolTip.style.left = (e.clientX-(correlationsToolTip.offsetWidth/2))+'px';
	}
	function dragended(d) {
		if (!d3.event.active)
			d3Chart.alphaTarget(0);
		d.fx = null;
		d.fy = null;
	}
	searchCorrelations.onkeyup = function(){
		selectedNodes = [0];
		graphLinks = [];
		for (var i = 0; i < graphData.nodes.length; i++)
			if (graphData.nodes[i].label.toLowerCase().indexOf(this.value.toLowerCase()) > -1)
				selectedNodes.push(graphData.nodes[i].id);
		//
		let rows = tableEntities.q('tr');
		for (var i = 0; i < rows.length; i++)
			if (rows[i].cells[0].innerHTML.toLowerCase().indexOf(this.value.toLowerCase()) > -1)
				rows[i].style.display = 'block';
			else
				rows[i].style.display = 'none';
		//
		restart();
	}
	restart();
	//
	// ===========================================
	function fillSimpleView(){
		tableEntities.innerHTML = '';
		for (var i = 0; i < graphData.nodes.length; i++){
			var row = elem('tr', '<td>'+(/*graphData.nodes[i].group == 'persons' ?
										'<span class="person-name">'+(graphData.nodes[i].name || '')+'</span>'+'<span class="person-email">'+(graphData.nodes[i].email || '')+'</span>'+'<span class="person-phone">'+(graphData.nodes[i].phone || '')+'</span>'
									:*/
										graphData.nodes[i].label)+'</td>'+
							'<td>'+taxonomyDictionary[ graphData.nodes[i].group ]+'</td>'+
							'<td class="score">'+graphData.nodes[i].significance+'</td>',
						{'data-id': graphData.nodes[i].id, 'data-group': graphData.nodes[i].group, 'data-label': graphData.nodes[i].label});
			tableEntities.appendChild(row);
			graphData.nodes[i].tr = row;
			row.setAttribute('data-nodeindex', i);
			row.onclick = function(){
				var id = this.getAttribute('data-id');
				var nodeIndex = this.getAttribute('data-nodeindex');
				NLPSelectedCircle = graphData.nodes[nodeIndex];
				var group = this.getAttribute('data-group');
				if (selRow)
					selRow.removeClass('highlight');
				selRow = this;
				selRow.addClass('highlight');
				//
				if (document.location.hash != '#correlations/o/'+group+'/'+id+'/'+this.getAttribute('data-label').replace(/ /g, '-'))
					document.location.href = '#correlations/o/'+group+'/'+id+'/'+this.getAttribute('data-label').replace(/ /g, '-');
				linkRelatedNodes(id);
				//
				tableRelatedEntities.innerHTML = '';
				for (var i = 1; i < selectedNodes.length; i++){
					//	Display set intersection for related enttities
					let intersection = '';
					if ((typeof graphData.edges[ selectedNodes[i] ] == 'undefined' || typeof graphData.edges[ selectedNodes[i] ][ id ] == 'undefined') && 
						(typeof graphData.edges[ id ] == 'undefined' || typeof graphData.edges[ id ][ selectedNodes[i] ] == 'undefined'))
						intersection = '-';
					else if (typeof graphData.edges[ selectedNodes[i] ] != 'undefined' && typeof graphData.edges[ selectedNodes[i] ][ id ] != 'undefined')
						intersection += '&#x25C4; '+graphData.edges[ selectedNodes[i] ][ id ]+'<tiny>%</tiny> ';
					else if (typeof graphData.edges[ id ] != 'undefined' && typeof graphData.edges[ id ][ selectedNodes[i] ] != 'undefined')
						intersection += ' '+graphData.edges[ id ][ selectedNodes[i] ]+'<tiny>%</tiny> &#x25BA;';
					//
					tableRelatedEntities.appendChild(
						elem('tr', '<td>'+(/*nodesIndex[ selectedNodes[i] ].group == 'persons' ?
										'<span class="person-name">'+(nodesIndex[ selectedNodes[i] ].name || '')+'</span>'+
										'<span class="person-email">'+(nodesIndex[ selectedNodes[i] ].email || '')+'</span>'+
										'<span class="person-phone">'+(nodesIndex[ selectedNodes[i] ].phone || '')+'</span>'
									:*/
										nodesIndex[ selectedNodes[i] ].label)+'</td>'+
								//'<td>'+taxonomyDictionary[ graphData.nodes[ selectedNodes[i] ].group ]+'</td>'+
								'<td align="right">'+nodesIndex[ selectedNodes[i] ].significance+'</td>'+
								'<td align="right">'+intersection+'</td>',
							{'data-type': nodesIndex[ selectedNodes[i] ].group})
					);
				}
				sortColumn(tableRelatedEntities.parentNode, 2, false);
				//
				/*for (var i = 0; i < etypeFilters.length; i++)
					etypeFilters[i].checked = true;*/
				//
			};
		}
		sortColumn(tableEntities.parentNode, 2, false);
	}
	// ===========================================
	fillSimpleView();
	callback();
	//
	return {
		redraw: restart,
		/*setGraphData: function(data, nodes, links){//, size_mode, degree
			graphData = data;
			selectedNodes = nodes;
			//sizeMode = size_mode; connectionDegree = degree;
			if (typeof links == 'undefined')
				graphLinks = [];
			else
				graphLinks = links;
			//
			NLPSelectedCircle = false;
			restart();
			fillSimpleView();
		},*/
		filterEtype: function(etypes){
			selectedNodes = [0];
			graphLinks = [];
			for (var i = 0; i < graphData.nodes.length; i++)
				if (etypes.indexOf(graphData.nodes[i].group) > -1)
					selectedNodes.push(graphData.nodes[i].id);
			//
			for (var i = 0; i < tableEntities.rows.length; i++)
				if (etypes.indexOf(tableEntities.rows[i].getAttribute('data-group')) > -1)
					tableEntities.rows[i].style.display = 'block';
				else
					tableEntities.rows[i].style.display = 'none';
			//
			NLPSelectedCircle = false;
			restart();
		},
		nodeResize: function(mode){
			sizeMode = mode;
			restart();
		},
		chgConnDegree: function(degree){
			connectionDegree = degree;
			//restart();
			if (selRow)
				linkRelatedNodes(selRow.getAttribute('data-id'));
		}
	};
}

var prevSelected = false;
function loadCorrelationDetails(DOMcontext, ObjContext, params){
	var id = params[0].split(',')[0];
	//
	//	If correlation details not loaded, first load them and then proceed afterwards
	startD = DateToShort(document.forms[0].from_date.value);
	endD = DateToShort(document.forms[0].to_date.value);
	if (correlationDateRange != startD+'-'+endD || typeof correlationData[ObjContext] == 'undefined'){//typeof row == 'undefined' || 
		correlationDateRange = startD+'-'+endD;
		loadCorrelations(document.getElementById('contentCorrelations'), [], id);
		return false;
	}
	else{
		let tables = DOMcontext.parentNode.q('#entities_collections #simpleView table tbody');
		let tr = tables[0].q('tr[data-id="'+id+'"]');
		if (tr.length > 0)
			tr[0].onclick();
	}
	var collection = correlationData[ObjContext];
	var correlationDetailPassDownData = {collection: collection, title: (collection[id].title || collection[id].name || collection[id].email || collection[id].phone)};
	//
	eventLogger('start', 'correlations-details');
	//
	// ---------------------------------------------------------------------------------------
	//	Populate related entities to the Entities-selected frame
	/*setTimeout(function(){
		entitiesSpace = DOMcontext.parentNode.q('#entities_space')[0];
		entitiesSelected = DOMcontext.parentNode.q('#entities_selected')[0];
		for (var i = entitiesSelected.childNodes.length - 1; i > -1; i--){
			/*entitiesSelected.childNodes[i].setAttribute('href', entitiesSelected.childNodes[i].getAttribute('data-href'));
			entitiesSelected.childNodes[i].removeAttribute('data-href');
			entitiesSelected.childNodes[i].onclick = null;* /
			entitiesSpace.appendChild(entitiesSelected.childNodes[i]);
		}
		//
		sortEntityCards(entitiesSpace);
		//
		if (typeof correlationsGraph[id] != 'undefined')
			for (eid in correlationsGraph[id]){
				var sel = entitiesSpace.q('.card-wrap[data-id="'+eid+'"]');
				if (sel.length > 0){
					/*sel[0].setAttribute('data-href', sel[0].getAttribute('href'));
					sel[0].removeAttribute('href');
					sel[0].onclick = function(){
						params[0] = id+','+this.getAttribute('data-id');
						loadCorrelationDetails(DOMcontext, ObjContext, params);
					};* /
					entitiesSelected.appendChild(sel[0]);
				}
			}
		sortEntityCards(entitiesSelected);
	}, 1);*/
	// ---------------------------------------------------------------------------------------
	/*var correlationsOrgData = document.body.q('table.correlationsOrgData tbody tr.corr-org.active');
	var correlationsPplData = document.body.q('table.correlationsPplData tbody tr.corr-org.active');
	for (var i = 0; i < correlationsOrgData.length; i++)
		correlationsOrgData[i].removeClass('active');
	//
	for (var i = 0; i < correlationsPplData.length; i++)
		correlationsPplData[i].removeClass('active');*/
	//
	//row.addClass('active'); // row.addClass('details'); row.parentNode.scrollTop = row.offsetTop - 48;
	//
	//	Load aggregated percentages
	loadAggregatedPercentages(params[0], DOMcontext,
		function(dayData){
			/*/setTimeout(
				function(){
				}, 10);//*/
			empEngagement.style.height = '0px';
			setTimeout(
				function(){
				//	row.removeClass('details');
				//	row.removeClass('highlight');
					empEngagement.style.height = 'auto';
					empEngagement.style.transition = '';
				}, 500);
				//
				//	Draw chart
				var graphData = [['Date', 'Significance']];
				var days = Object.keys(dayData);
				for (var j  = days.min(); j < days.max()+1; j++){
					var date = DateFromShort(j);
					//
					if ( isWorkingDay(date.getDay()) )
						graphData.push([date, dayData[j]]);
					else
						graphData.push([date]);
				}
				drawChartCorrelationChange(correlationsContext.q('#chart_correlations .chart')[0], graphData,
					function(date, col){
						date = new Date(date[0]);
						//console.log(date);
						filterCorrelationDetails('date', date.sqlFormatted(), params[0]);
					}
				);
		});
	//
	//	Load Details
	//loadCorrelationTopicDetails(params[0]);
	//
	empEngagement.style.height = empEngagement.offsetHeight+'px';
	empEngagement.style.transition = 'height 0.5s';
	correlationsContext.q('#chart_correlations')[0].style.display = 'block';
	correlationsContext.q('#chart_correlations h2')[0].innerHTML = 'Trend for "'+correlationDetailPassDownData.title+'"';
	correlationsContext.q('#engagement-title')[0].innerHTML = 'Engagement on "'+correlationDetailPassDownData.title+'"';
}

/*/	Sort entities by significance
function sortEntityCards(entitiesSpace){
	entityCards = [];
	for (var i = entitiesSpace.childNodes.length; i--;)
		entityCards.push(entitiesSpace.removeChild(entitiesSpace.childNodes[i]));
	//
	entityCards.sort(function(a, b){
		return parseInt(b.q('b.significance')[0].innerText) > parseInt(a.q('b.significance')[0].innerText) ? 1 : -1;
	});
	//
	for (var i = 0; i < entityCards.length; i++)
		entitiesSpace.appendChild(entityCards[i]);
}*/

var pDict = {gm: 1, ch: 3, dc: 0, cl: 2, tb: 4, zc: 5, ze: 6, zt: 7, bs: 11, zl: 8, zi: 9, za: 10, sf: 12, sms: 13, cll: 14, pl: 15, po: 16, rcc: 17, rcs: 18, sfl:19, chm:20, cha:21};
function loadAggregatedPercentages (id, context, callback) {
	onScrolledToBottom = function(){};
	//var engagementEl = q('table#empEngagement tbody')[0];
	//var prodUseEl = q('table#prodUsage tbody')[0];
	empEngagement = /*context.*/q('table#empEngagement tbody')[0];
	prodUsage = /*context.*/q('table#prodUsage tbody')[0];
	correlationDetails.style.display = 'none';

	if (correlationAjaxRequests[2]){
		correlationAjaxRequests[2].abort();
	}
	correlationAjaxRequests[2] = new arc.ajax(base_url+'correlations-ajax-aggr/?fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted()+'&entity='+id+correlationsIntExtFilter, {
		callback: function(data){

			statDetails = eval('('+data.responseText+')');
			//correlationDetailPassDownData.days = statDetails.days;
			callback(statDetails.days);

			// clear the tables
			empEngagement.innerHTML = "";
			prodUsage.innerHTML = "";
			var tmpr;

			for (var i = 0; i < statDetails.emp.length; i++) {
				tmpr = elem('tr', '<td>'+statDetails.emp[i].fullname+'</td><td class="score">'+statDetails.emp[i].perc+'%</td>', {'data-id': statDetails.emp[i].id});
				tmpr.onclick = function () {
					/*if (this.cells[0].innerHTML == 'Others'){
						var tmp = [];
						for (var j = 0; j < this.parentNode.childNodes.length-1; j++) {
							tmp.push(this.parentNode.childNodes[j].getAttribute('data-id'));
							filterCorrelationDetails('employee', tmp.join(','), id);
						}
					} else {*/
						filterCorrelationDetails('employee', this.getAttribute('data-id'), id);
					//}

				}
				empEngagement.appendChild(tmpr);
			}

			for (var i = 0; i < statDetails.prod.length; i++){
				var icon = icons[statDetails.prod[i].name];
				tmpr = elem('tr', '<td class="icon-col"><div class="icon icon-' +statDetails.prod[i].name + '"  title="'+products[statDetails.prod[i].name][0]+'"></div></td>'+
							'<td>'+q('h3.app-name')[ pDict[statDetails.prod[i].name] ].innerHTML+'</td>'+//products[statDetails.prod[i].name][0]
							'<td class="score">'+(statDetails.prod[i].perc > 1 ? statDetails.prod[i].perc+'%' : '< 1%')+'</td>', {'data-id': products[statDetails.prod[i].name][0]});
				tmpr.onclick = function(){
					filterCorrelationDetails('product', this.getAttribute('data-id'), id);
				}
				prodUsage.appendChild(tmpr);
			}
			// ---------------------------

			var height = empEngagement.offsetHeight;
			empEngagement.style.height = '0px';
			empEngagement.style.transition = 'height 0.5s';

			setTimeout(function () {
				eventLogger('end', 'correlations-details');
			}, 3);
			/*setTimeout(function () {
				window.scrollTo( 0, window.innerHeight / 1.5 );
			}, 666);*/

			setTimeout(
				function(){
					empEngagement.style.height = height+'px';
					setTimeout(
						function(){
							empEngagement.style.height = 'auto';
						}, 200);
				}, 10);
			//
		}
	});
}

function filterCorrelationDetails(col, val, entity_id, page){
	if (typeof page == 'undefined')
		page = 1;
	//
	let filter = '?entity='+entity_id+correlationsIntExtFilter;
	if (col == 'date'){
		filter += '&fromdate='+val+'&todate='+val;
	}
	else if (col == 'employee'){
		filter += '&employee='+val+'&fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted();
	}
	else if (col == 'product'){
		filter += '&product='+val+'&fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted();
	}
	//
	if (page == 1)
		window.scrollTo( 0, window.innerHeight );
/*	correlationRows = correlationDetails.q('tbody tr');
	if (col == 0)
		for (var i = 0; i < correlationRows.length; i++)
			if (typeof val == 'object')
				correlationRows[i].style.display = (val.indexOf(correlationRows[i].cells[2].innerText.trim()) == -1) ? 'table-row' : 'none';
			else
				correlationRows[i].style.display = (correlationRows[i].cells[2].innerText.trim() == val) ? 'table-row' : 'none';
	else if (col == 1)
		for (var i = 0; i < correlationRows.length; i++)
			correlationRows[i].style.display = (correlationRows[i].cells[0].q('div')[0].getAttribute('title') == val) ? 'table-row' : 'none';
	else if (col == 3)
		for (var i = 0; i < correlationRows.length; i++)
			correlationRows[i].style.display = (correlationRows[i].cells[3].innerText.trim().substring(0, 10) == val) ? 'table-row' : 'none';
	//
	window.scrollTo( 0, window.innerHeight );
}

function loadCorrelationTopicDetails(id) {*/
	if (correlationAjaxRequests[3]){
		correlationAjaxRequests[3].abort();
	}
	correlationAjaxRequests[3] = new arc.ajax(base_url+'correlations-ajax-details/'+filter+'&page='+page, {
		callback: function(data){
			statDetails = eval('('+data.responseText+')');
			var table = correlationDetails.q('tbody')[0];
			correlationDetails.style.display = 'block';
			//*/
			if (page == 1)
				table.innerHTML = '';
			for (var i = 0; i < statDetails.length; i++){
				var icon = icons[statDetails[i].product];
				var row = elem('tr',
								'<td width="20" class="icon-col" sorttable_customkey="'+statDetails[i].product+'">'+
									'<div class="icon" style="background-image:url(\''+icon+'\');" title="'+products[statDetails[i].product][0]+'"></div>'+
									(statDetails[i].x > 1 ? '<span class="icon-x">x'+statDetails[i].x+'</span>' : '')+
								'</td>'+
								'<td title="' + statDetails[i].title + '">'+
									'<span class="ellipsis">'+statDetails[i].title+'</span>'+
									'<table class="stat-details-inner"><tbody></tbody></table>'+
								'</td>'+
								'<td width="120">'+statDetails[i].employee+'</td>'+
								'<td width="180">'+statDetails[i].date+' '+mtsToHrs(statDetails[i].start_time)+'</td>'+
								'<td width="210" sorttable_customkey="' + statDetails[i].sentiment.score + '">'+sentimentWidget(statDetails[i].sentiment)+'</td>',
							{'class': 'tablerow hasDetails'+(statDetails[i].flag==1?' exclude':''), 'data-id': statDetails[i].id, 'data-product': statDetails[i].product});
				table.appendChild(row);
				row.onclick = expandRow;
			}
			// ---------------------------

			var height = table.offsetHeight;
			table.style.height = '0px';
			table.style.transition = 'height 0.5s';
			setTimeout(
				function(){
					table.style.height = height+'px';
					setTimeout(
						function(){
							table.style.height = 'auto';
						}, 500);
				}, 10);
			//*/
			sortColumn(table.parentNode, 3);
			//
			if (statDetails.length > 0)
				onScrolledToBottom = function(){
					filterCorrelationDetails(col, val, entity_id, page+1);
					onScrolledToBottom = function(){};
				};
		}
	});
	var expandRow = function(){
		if (!organizationSettings['show_details'])
			return false;
		//
		var row = this;
		var detable = row.cells[1].q('table')[0];
		var id = row.getAttribute('data-id');
		var product = row.getAttribute('data-product');
		//	Toggle display of details of detail record
		if (row.className.indexOf('details') > -1 || row.className.indexOf('highlight') > -1){
			detable.style.height = detable.offsetHeight+'px';
			detable.style.transition = 'height 0.5s';
			setTimeout(
				function(){
					detable.style.height = '0px';
					setTimeout(
						function(){
							row.removeClass('details');
							row.removeClass('highlight');
							detable.style.height = 'auto';
							detable.style.transition = '';
						}, 500);
				}, 10);
		}
		else{
			row.addClass('details');
			loadDetailData(id, row, product, statDetails[id], function(){
				var height = detable.offsetHeight;
				detable.style.height = '16px';
				detable.style.transition = 'height 0.5s';
				setTimeout(
					function(){
						detable.style.height = height+'px';
						setTimeout(
							function(){
								detable.style.height = 'auto';
							}, 500);
					}, 10);
			});
		}
	};
}

/*function entityRow(id, className, collection, title, sortkey, context){
	var row;
	row = elem('tr', '<td sorttable_customkey="'+sortkey.toLowerCase()+'">'+title+'</td><td class="score">'+collection[id].significance+'</td>', {class: 'corr-org '+className, 'data-id': id, draggable: true});

	//empEngagement = context.q('table#empEngagement tbody')[0];
	//prodUsage = context.q('table#prodUsage tbody')[0];

	row.onclick = function(){
		if (this.className.indexOf('topic') > -1){
			document.location.hash = 'correlations/topic/'+id+'/' + encodeURIComponent(this.q('td')[0].getAttribute('sorttable_customkey'));
		}
		else{
			document.location.hash = 'correlations/cod/'+id+'/' + encodeURIComponent(this.q('td')[0].getAttribute('sorttable_customkey'));
		}
	};
	return row;
}*/


var CorrelationsWidget = function () {

	let self = this;

	self.el = null;
	self.entitiesEl = null;

	self.prevRequest = false;

	self.processedData = {};

	self.dataCallback = function () {
		if (organizationSettings.correlations_enabled !== 1) {
			return false;
		}

		var corrEntities = [];

		for (var type in self.processedData){
			for (var eid in self.processedData[type]){
				if (type == 'persons'){
					self.processedData[type][eid].title = '';
					if ( typeof self.processedData[type][eid].name != 'undefined' && self.processedData[type][eid].name != '' ) {
						self.processedData[type][eid].title += '<span class="person-name">' + self.processedData[type][eid].name + '</span>'
					}
					if ( typeof self.processedData[type][eid].email != 'undefined' && self.processedData[type][eid].email != '' ) {
						self.processedData[type][eid].title += '<span class="person-email">' + self.processedData[type][eid].email + '</span>'
					}
					if ( typeof self.processedData[type][eid].phone != 'undefined' && self.processedData[type][eid].phone != '' ) {
						self.processedData[type][eid].title += '<span class="person-phone">' + self.processedData[type][eid].phone + '</span>'
					}
				}
				else
					corrEntities.push([eid, type, self.processedData[type][eid]])
			}
		}

		corrEntities.sort(function (a, b) {
			return b[2].significance - a[2].significance;
		});

		if ( corrEntities.length > 0 ) {
			self.entitiesEl.innerHTML = '';
			for ( var t = 0; t < corrEntities.length; t++ ) {
				if (t > 4) {
					break;
				}
				var row = elem('tr',
					'<td sorttable_customkey="' + corrEntities[t][2].title.toLowerCase() +'" data-type="'+corrEntities[t][1]+'">'+corrEntities[t][2].title+'</td>'+
					'<td class="type">'+taxonomyDictionary[ corrEntities[t][1] ]+'</td>'+
					'<td class="score">'+corrEntities[t][2].significance+'</td>',
					{ class: 'corr-widget-row' });
				row.addEventListener('click',(function (id) {
					return function () {
						document.location.hash = 'correlations/o/'+this.q('td')[0].getAttribute('data-type')+'/' + id + '/' + encodeURIComponent(this.q('td')[0].getAttribute('sorttable_customkey'));
					};
				}(corrEntities[t][0])));
				self.entitiesEl.appendChild(row);
			}
		}
		else {
			self.entitiesEl.innerHTML = '<tr><td colspan="3">No records to display</td></tr>';
		}

	};

	self.loadData = function (from, to, emp = '') {
		if (organizationSettings.correlations_enabled !== 1) {
			return false;
		}

		let q  = '', empQ = '';
		//empQ = '&employee=' + emp.join(',');
		if (Array.isArray(emp) && emp.length > 0) {
			empQ = '&employee=' + emp.join(',');
		} else if ( emp != '' ) {
			empQ = '&employee=' + emp;
		}
		q = 'fromdate=' + from + '&todate=' + to + '&limit=55' + empQ;

		self.prevRequest = new ajax(
			base_url + 'correlations-ajax/?' + q,
			{
				callback: function (data) {
					correlationData = self.processedData = self.data = eval('('+data.responseText+')'),
					self.dataCallback();
				}
			}
		);
	}

	self.init = function (el) {

		if (organizationSettings.correlations_enabled !== 1) {
			return false;
		}

		self.el = el;
		self.entitiesEl = el.q('table.card-table tbody')[0];

		// show loading bars
		self.entitiesEl.innerHTML = '<tr class="data-loading"><td colspan="3"><div class="loading-graph"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div></div></td></tr>';

	}

	return self;
}


// ===============================================================
//	Load metadata for a stat detail row - From JSON file in Cloud Storage file
var loadDetailData = function(id, row, product, detail, callback){
	if (!organizationSettings['show_details'])
		return false;
	//
	if (row.className.indexOf('sub') > -1){
		//	Especially for broadsoft, find the mail row
		row.removeClass('details highlight');
		row = (function(row){
				if (row.tagName == 'TR' && row.className.indexOf('tablerow') > -1 && row.className.indexOf('sub') == -1)
					return row;
				else
					return arguments.callee(row.parentNode);
			})(row);
		row.addClass('details highlight');
	}
	var detable, detables = row.cells[1].q('table tbody');
	//	Especially for broadsoft, there are sub rows inside a single detail row. We need to iterate for those
	var loaded = 0;
	for (var i = 0; i < detables.length; i++){
		detable = detables[i];
		if (detable.innerHTML != '' && detable.innerHTML != '<small>Loading ...</small>'){
			callback();
			return false;
		}
		//	Recursively navigate up to the TR with data-id to  find the detail record id
		id = (function(node){
				if (node.tagName == 'TR' && node.className.indexOf('tablerow') > -1 && node.getAttribute('data-id') != null)
					return node.getAttribute('data-id');
				else
					return arguments.callee(node.parentNode);
			})(detable);
		detable.innerHTML = '<small>Loading ...</small>';
//setTimeout(function(){
		//	Need to isolate for each asynchronous callback
		//new (function(){})();
		//new ajax('https://app.prodoscore.com/dashboard-detail-data-raw/?id='+id+'&filename=data', {
		new ajax(base_url + 'dashboard-detail-data/?id=' + id, {
			callback: function (data, ref) {//[id, detable, callback]
				var detable = ref[1];
				/*var hasSentiment = (function () {
					return (typeof data[id].sentiment !== 'undefined'  && Object.keys(data[id].sentiment).length > 0);
				})();*/
				//
				data = eval('('+data.responseText+')');
				var entities = data.entities;
				var sentiment = data.sentiment;
				data = data.data;
				detable.innerHTML = '';

				// Replace 'null' or empty string with '-'
				for (var key in data) {
					if (data[key] == null || data[key] == "") {
						data[key] = '-';
					}
				}

				// Google Drive file type icons
				if (product == 'dc'){
					if (typeof data.mime != 'undefined')
						detable.appendChild(arcReactor({tr: {content: [
							{td: {content: 'File Type'}},
							{td: {content: '<img src="'+getIcon(data, title)+'" height="16" width="16" title="'+data.mime+'" /> ' + data.mime.replace('application/vnd.google-apps.', '')}}
						]}}));
				}
				//
				// Display phone call type
				else if (product == 'cll'){
					data.duration /= 60;
					if (typeof data.type != 'undefined'){
						if (data.type == 3 || isNaN(data.duration))//data.duration == '-'
							delete data.duration;
						data.type = ['', 'Incoming', 'Outgoing', 'Missed'][data.type];
					}
				}
				//
				// Prepend doller sign for ProsperWorks oppertunities and leads
				else if (product == 'po' ||  product == 'pl') {
					if (data.monetary_value != null) {
						data.monetary_value = '$' + data.monetary_value;
					}
					else {
						data.monetary_value = '-';
					}
				}
				//
				//	Hide duration for missed and failed RingCentral calls
				else if ( product == 'rcc' ){
					if (['Missed', 'Call Failed', 'Hang Up', 'Unknown', 'Rejected',
					'Receive Error', 'Blocked', 'No Answer', 'International Disabled',
					'Busy', 'Send Error', 'No Fax Machine', 'ResultEmpty', 'Suspended',
					'Call Failure', 'Internal Error', 'IP Phone Offline', 'Stopped',
					'International Restriction', 'Abandoned', 'Declined',
					'Fax Receipt Error', 'Fax Send Error'].indexOf(data.result) > -1) {
						delete data.duration;
					}
				}
				//
				// Show transcript of TurboBridge call
				else if ( product == 'tb' && typeof data.transcript != 'undefined' ){
					var transcriptRow = arcReactor(
						{	tr: {content: [
								{td: {content: 'Transcript'}},
								{td: {content: '<div class="transcript-expand">'+data.transcript.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/ï»¿/g, '').replace(/\n/g, '<br/>')+'</div>'}}
							]}
						});
					detable.parentNode.parentNode.parentNode.parentNode.appendChild(transcriptRow);
					new showMoreBtn( transcriptRow.q('.transcript-expand')[0] );
					//transcriptRow.style.height = '100px';
					delete data.transcript;
				}
				//
				else if (product === 'cl') {
					if ( typeof data.duration !== 'undefined') data.duration = Math.abs(data.duration);
				}

				//
				/*/ Sales Force - parse email data	//	This is implemented at the server side
				else if ( product == 'sf' ){
					data.Description = data.Description.split('\n');
					for (var i = 0; i < data.Description.length; i++){
						data.Description[i] = data.Description[i].split(':');
						if (data.Description[i].length > 1){
							if (data.Description[i][0] == 'Body'){
								data.Description = data.Description.slice(i+1, 33).join('\n');//<br/>.substring(0, 500)
								break;
							}
							else
								data[ data.Description[i][0] ] = data.Description[i].slice(1).join(':').substring(1);
						}
					}
				}*/
				// ---------------------------------------------------------------------------------------------------------
				//	Actually insert rows
				// ---------------------------------------------------------------------------------------------------------
				if (data.constructor === Array){
					console.warn('Product '+product+' gives data in an array, not dict.!')
					for (var i = 0; i < data.length; i++)
						if (products[product][3].length==0 || typeof products[product][3][ data[i].name ] != 'undefined') {
							detable.appendChild(arcReactor({tr: {content: [
								{td: {content: products[product][3][ data[i].name ]}},
								{td: {content: data[i].name == 'duration' ? mtsToTime(data[i].value) : data[i].value.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\n/g, '<br/>')}}//.replace(/ï»¿/g, '')
							]}}));
						}
				}
				else{
					var attribs = Object.keys(products[product][3]);
					for (var i = 0; i < attribs.length; i++) {
		    if (typeof data[attribs[i]] == 'undefined') {
			data[attribs[i]] = '-';
		    } else {
			 var attributeValue = data[attribs[i]];
			 if (product == 'zc' &&  attributeValue.indexOf("Dialled") != -1) {
			    var newAttrbuteValue = attributeValue.replace("Dialled","Dialed");
				data[attribs[i]] = newAttrbuteValue;
							}
						}

		    detable.appendChild(arcReactor({
			tr: {
			    content: [
				{td: {content: products[product][3][attribs[i]]}},
				{
				    td: {
					content:
					    attribs[i] == 'duration' ? // then
						mtsToTime(data[attribs[i]]) :
						// else
						(typeof data[attribs[i]] == 'object' ? // then
							JSON.stringify(data[attribs[i]]) :
							// else
							data[attribs[i]].toString().replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/ï»¿/g, '').replace(/\n/g, '<br/>')
						)
				    }
				}//
			    ]
			}
		    }));
		}
					//
					/*for (var key in data)
						if (products[product][3].length==0 || typeof products[product][3][ key ] != 'undefined') {
							detable.appendChild(arcReactor({tr: {content: [
								{td: {content: products[product][3][ key ]}},
								{td: {content: key == 'duration' ? mtsToTime(data[key]) : (typeof data[key] == 'object' ? JSON.stringify(data[key]) : data[key].toString().replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/ï»¿/g, '')) }}//
							]}}));
						}*/
				}
				// ---------------------------------------------------------------------------------------------------------
				//	Additional Rows
				// ---------------------------------------------------------------------------------------------------------
				if (product == 'gm' && typeof data.body != 'undefined'){
					var GmailBody = data.body.replace(/ï»¿/g, '');
					if (GmailBody != 'TEXT TOO LONG'){
						/*try{	//	This is temporary, untill the memcached old email bodies are removed.
							GmailBody = atob(GmailBody.replace(/_/g, '/').replace(/-/g, '+')).replace(/â/g, '').replace(/Â/g, ''); GmailBody = elem('div', GmailBody); GmailBody = GmailBody.textContent || GmailBody.innerText || ''; }catch(e){}*/
						if (GmailBody.trim() == '')
							GmailBody = '<i>No text content</i>';//The email body contains either images or tables // THAT'S WRONG MESSAGE
					}
					detable.appendChild(arcReactor({tr: {content: [
						{td: {content: 'Body'}},
						{td: {content: GmailBody}}
					]}}));
				}
				else if (product == 'ch'){
					detable.innerHTML = '';	//	Should We .?
					if (typeof data.from != 'undefined')
						detable.appendChild(arcReactor({tr: {content: [
							{td: {content: 'From'}},
							{td: {content: data.from[0]+' &lt;'+data.from[1]+'&gt;'}}
						]}}));
					//
					if (typeof data.data != 'undefined'){
						var tmp = document.createElement("div"),
							tmpTitle = showEncrypted(data.data);
						tmp.innerHTML = tmpTitle;
						title = tmp.textContent || tmp.innerText || "<i>NO TEXT CONTENT</i>";
						if (title.length > 280) {
							title = title.substring(0,280) + '&hellip;';
						}
						try {
							title = decodeURIComponent(escape(title));
						} catch (err) {
							// some text cannot be decoded. Shouldn't cause an issue.
							// URIError: malformed URI sequence
						}
						//
						detable.appendChild(arcReactor({tr: {content: [
							{td: {content: 'Message'}},
							{td: {content: title}}
						]}}));
					}
				}
				else if ( product == 'rcs' ){
					/*detable.appendChild(arcReactor({tr: {content: [
						{td: {content: 'From'}},
						{td: {content: data.from.phoneNumber+' ('+data.from.location+')'}}
					]}}));*/
					for (var j = 0; j < data.to.length; j++)
						detable.appendChild(arcReactor({tr: {content: [
							{td: {content: 'To'}},
							{td: {content: data.to[j].phoneNumber+' ('+data.to[j].location+')'}}
						]}}));
				}
				//
				if (typeof sentiment != 'undefined' && (sentiment.score > 0 || sentiment.magnitude > 0))
					detable.appendChild(arcReactor({tr: {content: [
						{td: {content: 'Sentiment'}},
						{td: {content: (sentiment.score < -0.1 ? '<i class="sentiment fa fa-frown-o"></i>' : (sentiment.score > 0.2 ? '<i class="sentiment fa fa-smile-o"></i>' : '<i class="sentiment fa fa-meh-o"></i>'))+
									' &nbsp; <span title="Score of the sentiment ranges from -1.0 (very negative) to 1.0 (very positive)">Score:</span> <b style="color:'+(sentiment.score < -0.1 ? '#DC0000' : (sentiment.score > 0.2 ? '#008A00' : '#E29200'))+'">'+sentiment.score+'</b>'+
									' &nbsp; <span title="Magnitude is the strength of sentiment regardless of score, ranges from 0 to infinity.">Magnitude:</span> <b>'+sentiment.magnitude+'</b>'}}
					]}}));
				//
				//if (false)	//	TEMPORARILY HIDDEN FEATURE FOR THE MOMENT
				//	Load NLP entities related to this detail record
				if (Object.keys(entities).length > 0){
					var entitySpace = detable.appendChild(arcReactor({tr: {content: [
						{td: {colspan: 2, class: 'entities'}}
					], style: 'width:calc(100vw - 280px);'}})).q('td')[0];
					for (entity in entities)
						//if (entities[entity].etype == 'organization')
						entitySpace.appendChild(elem('span', (entities[entity].title || entities[entity].name || entities[entity].email), {class: 'nlpentity '+entities[entity].etype+' title'}));//'<i class="fa fa-building"></i> '+
/* TO DO: commenting out to enable this later since the accuracy of the data is not satisfying at the moment
						else if (entities[entity].etype == 'person'){
							if (typeof entities[entity].name != 'undefined')
								entitySpace.appendChild(elem('span', '<i class="fa fa-user"></i> '+entities[entity].name, {class: 'nlpentity person name'}));
							else if (typeof entities[entity].email != 'undefined')
								entitySpace.appendChild(elem('span', '<i class="fa fa-user"></i> '+entities[entity].email, {class: 'nlpentity person email'}));
							else if (typeof entities[entity].phone != 'undefined')
								entitySpace.appendChild(elem('span', '<i class="fa fa-user"></i> '+entities[entity].phone, {class: 'nlpentity person phone'}));
						}
//*/
				}
				ref[2]();//callback//data
			}//, cache: true
		},
		//	Reference object to be passed to Ajax callback
		[id, detable, function(){
						loaded += 1;
						if (detables.length == loaded)
							callback();
					}]);
	}
//}, 300);
};
