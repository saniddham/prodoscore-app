
var form = q('form label')[0];
var uploadedIcon = q('div.uploaded-icon')[0];
var uploadCtrls = q('div.upload-ctrls')[0];
uploadedIcon.style.display = 'none';

form.ondragenter = form.ondragover = function(e){
	e.preventDefault();
	//e.stopPropagation();
	this.addClass('hovering');
};

form.ondragleave = function(e){
	this.removeClass('hovering');
};

form.ondrop = function(e){
	var droppedFiles = (e.originalEvent || e).dataTransfer.files;
	//console.log(droppedFiles);
	//form.q('input[type="file"]')[0].value = droppedFiles[0];
	fileDropped(droppedFiles[0]);
	e.preventDefault();
	e.stopPropagation();
	this.removeClass('hovering');
};

form.q('input[type="file"]')[0].onchange = function(){
	fileDropped(this.files[0]);
	this.value = '';
};


function fileDropped(file){
	parent.fileDropped(file);
	var reader = new FileReader();
	reader.onload = function(e){
		var fileData = e.target.result;
		var csv = parseFile(fileData);
		var result = parent.validateXLSXData(csv, file, fileData.split('base64,')[1]);
		if (result){
			uploadCtrls.style.display = 'none';
			uploadedIcon.style.display = 'block';
			uploadedIcon.q('label')[0].innerHTML = file.name;
		}
		else{
			uploadCtrls.style.display = 'block';
			uploadedIcon.style.display = 'none';
		}
	}
	reader.readAsDataURL(file);
	//uploadFile(file);
}


function parseFile(file){
	try{
		file = file.split('base64,')[1];//.replace(/\//g, '-')
		file = atob(file);
		//
		//	Parse the file container to extract data
		var cfb = XLSX.read(file, {type: 'binary'});
		var sheet1 = cfb.Sheets[Object.keys(cfb.Sheets)[0]];
		//
		//	Get sheet data into a matrix of cells
		var keys = Object.keys(sheet1);
		var sheet = {}, col = 0, row = 0;
		for (var i = 0; i < keys.length; i++){
			col = keys[i].charCodeAt(0) - 65;
			row = 1 * keys[i].substring(1);
			if (col > -1 && col < 10){
				if (typeof sheet[row] == 'undefined')
					sheet[row] = {};
				sheet[row][col] = sheet1[ keys[i] ].v;
			}
		}
		//
		//	Remove rows with only one column
		keys = Object.keys(sheet);
		for (var i = 0; i < keys.length; i++)
			if (Object.keys(sheet[ keys[i] ]).length == 1)
				delete sheet[ keys[i] ];
		//var wb = XLSX.parse_xlscfb(cfb);
		//console.log(wb);
		//var output = XLSX.utils.sheet_to_csv(wb);
		return sheet;
	}
	catch(e){
		console.log(e);
		return false;
	}
}

