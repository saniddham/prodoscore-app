
var page = 'dashboard';
var loadEmployeeId = -1, employees4Mgr = -1;

var contentMain = document.getElementById('contentMain');
var contentSettings = document.getElementById('contentSettings');
var navigationAnchors = document.querySelectorAll('ul.main-navigation li a');
var settingsTabs = contentSettings.querySelectorAll('ul.tabs li');

function initializeNavigation(){

	new navFrame('dashboard', contentMain,
		function(context, params){
			contentMain.style.opacity = 1;
			loadDashboard(context, params);
		});

	new navFrame('dashboard/strata', document.getElementById('strataCount'),
		function(context, params){
			contentMain.style.opacity = 0;
			loadStrataPage(context, ['above', 'baseline', 'below'].indexOf(params[0]));
			var currentText = '';
			if (params == 'above') {
				currentText = 'Prodoscore above average score';
			} else if (params == 'below') {
				currentText = 'Prodoscore below average score';
			} else if (params == 'baseline') {
				currentText = 'Prodoscore within average score';
			}
			breadCrumbViewModel();
			breadCrumbViewModel.reset();
			breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
			breadCrumbViewModel.addCrumb('#dashboard/strata/' + params, currentText, true);
			breadCrumbViewModel.render(context);
		});

	new navFrame('managers', document.getElementById('contentManagers'), loadManagersPage);

	new navFrame('employees', document.getElementById('contentEmployees'),
		function(context, params){
			//context.querySelector('.one-row.topspace').style.display = 'none';
			//context.querySelector('#employeesChart').parentNode.parentNode.parentNode.style.display = typeof params[1] != 'undefined' ? 'block' : 'none';
			loadEmployeesPage(context, params);
			// clear search
			context.q('input[name="searchEmployee"]')[0].value = '';
			//filterByManager[0].value = params[1];
		});

	new navFrame('employee', document.getElementById('contentOneEmployee'),
		function(context, params){
			loadEmployeeData(context, params);
			navigationAnchors[navigationAnchors.length-2].parentNode.addClass('active');
		});

	new navFrame('correlations', document.getElementById('contentCorrelations'),
		function(context, params){
			loadCorrelations(context, params);
		},
		function(context, params){
			clearTimeout(connectionRetryCr);
		});
	new navFrame('correlations/o', document.getElementById('correlationDetailsSubFrame'),
		function(context, params){
			loadCorrelationDetails(context, params[0], params.splice(1));
		});

	new navFrame('rawdata', document.getElementById('raw-data'),
		function(context, params){
			loadRawData(context, params);
		});

	new navFrame('alerts', elem('span'),
		function(context, params){
			//loadAlerts(context, params);
			setTimeout("document.location.hash = 'dashboard';", 400);
		});

	new navFrame('settings', contentSettings,
		function(context, params){
			dateFilters.hide();
			if (document.location.hash == '#settings')
				setTimeout("document.location.hash = 'settings/employees';", 20);	//	Redirect
			return false;
		},
		function(context, params){
			setTimeout(function(){
				dateFilters.show();
			}, 80);
		});

	new navFrame('settings/employees', contentSettings.querySelector('#empSettings'), loadSettingsPage, dateFilters.show);

	new navFrame('settings/employees/o', contentSettings.querySelector('#employeeSettings'),
		function(context, params){
			dateFilters.show();
			if (typeof params[0] == 'undefined')
				document.location.hash = 'settings/employees';
			else
				showEmployeeSettingsPage(context, params);
			settingsTabs[0].addClass('active');
			//
			setTimeout(function(){
				var empSettings = contentSettings.querySelector('#empSettings');
				empSettings.removeClass('active');
				empSettings.style.display = 'none';
			}, 210);
		}, dateFilters.hide);
		//*function(){ dateFilters.hide(); //contentSettings.querySelector('#empSettings').style.display = 'block'; });*/

	//new navFrame('settings/email', contentSettings.querySelector('#emailSettings'), showEmailSettings, dateFilters.show);

	new navFrame('settings/alert', contentSettings.querySelector('#alertSettings'), showAlertSettings, dateFilters.show);

	var userSettings = contentSettings.querySelector('#userSettings');
	if (userSettings != null){

		var productSettings = contentSettings.querySelector('#productSettings');
		if (productSettings != null)
			new navFrame('settings/products', productSettings,
				function(){
					dateFilters.hide();
					setTimeout(function(){
						document.location.href = '#settings/products/turbobridge';
					}, 500);
				}, dateFilters.show);

		var turboBridgeSettings = contentSettings.querySelector('#turboBridgeSettings');
		if (turboBridgeSettings != null)
			new navFrame('settings/products/turbobridge', turboBridgeSettings, showTurboBridgeSettings, dateFilters.show);

		var broadSoftSettings = contentSettings.querySelector('#broadSoftSettings');
		if (broadSoftSettings != null)
			new navFrame('settings/products/phone-system', broadSoftSettings, showBroadSoftSettings, dateFilters.show);

		var crmSettings = contentSettings.querySelector('#crmSettings');
		if (crmSettings != null)
			new navFrame('settings/products/crm-system', crmSettings, showCRMSettings, dateFilters.show);

		var mobileSettings = contentSettings.querySelector('#mobileSettings');
		if (mobileSettings != null)
			new navFrame('settings/products/mobile', mobileSettings, showMobileSettings, dateFilters.show);

		new navFrame('settings/organization', contentSettings.querySelector('#timeSettings'), showTimeSettings, dateFilters.show);

		var scoreSettings = contentSettings.querySelector('#scoreSettings');
		if (scoreSettings != null)
			new navFrame('settings/score', scoreSettings, showScoreSettings, dateFilters.show);

		new navFrame('settings/users', contentSettings.querySelector('#userAccounts'),
			function(context, params){
				dateFilters.hide();
				showUserSettingsPage(context, params);					//		<<<<<<<<<<<<<<<<<<<<<<
			},
			dateFilters.show);

		new navFrame('settings/users/o', userSettings,
			function(context, params){
				dateFilters.hide();
				showOneUserSettings(context, params);						//		<<<<<<<<<<<<<<<<<<<<<<
				settingsTabs[settingsTabs.length-2].addClass('active');
				//
				setTimeout(function(){
					contentSettings.querySelector('#userAccounts').removeClass('active');//.style.display = 'none';
				}, 210);
			},
			dateFilters.show);

		var mobileSettings = contentSettings.querySelector('#mobileSettings');
		if (mobileSettings != null)
			new navFrame('settings/mobile', mobileSettings, showMobileSettings, dateFilters.hide);
	}

//	progressiveScriptLoaded({});

}

function dateFilters(obj, inv){
	obj.style.transition = 'all 0.5s';
	inv.style.transition = 'all 0.5s';
	var hideTimeout, preventDisplay = false;
	this.hide = function(){
		preventDisplay = true;
		hideTimeout = setTimeout(function(){
			obj.style.opacity = 0;
			obj.style.width = '0px';
			obj.style.pointerEvents = 'none';
			preventDisplay = false;
			//
			//if (inv.q('select option').length > 1)
			inv.style.display = 'block';
			//inv.style.pointerEvents = 'auto';
		}, 50);
	}
	this.show = function(){
		if (preventDisplay)
			return false;
		obj.style.opacity = 1;
		obj.style.width = 'auto';
		obj.style.pointerEvents = 'auto';
		//
		inv.style.display = 'none';
		//inv.style.pointerEvents = 'none';
	}
}
var dateFilters = new dateFilters(q('div.header-left')[0], q('div.header-left')[1]);
//var selectDomain = ;

var backToEmployeeSettings = employeeSettings.q('.nav-button.back')[0];
backToEmployeeSettings.onclick = function(){
	history.back();
}

var sidebar = q('div.sidebar')[0];
var sidebarNav = sidebar.q('nav.sidebar-nav')[0];
q('.header .fa.fa-bars')[0].onclick = function(){
	if (sidebar.className.indexOf('active') == -1){
		sidebar.addClass('active');
		sidebar.focus();
	}
	/*else
		sidebar.removeClass('active');*/
}
q('div.page-content')[0].onclick = function(){//sidebar.onblur =
	sidebar.removeClass('active');
}
var sidebarBottomHeader = q('div.sidebar .bottom h2')[0];
if (typeof sidebarBottomHeader != 'undefined'){
	sidebarBottomHeader.onclick = function(){
		if (this.q('i.fa')[0].className == 'fa fa-caret-down'){
			this.parentNode.addClass('collapsed');
			this.q('i.fa')[0].className = 'fa fa-caret-up';
			sidebarNav.addClass('bottom-collapsed');
			localStorage['legend-collapsed'] = 'true';
		}
		else{
			this.parentNode.removeClass('collapsed');
			this.q('i.fa')[0].className = 'fa fa-caret-down';
			sidebarNav.removeClass('bottom-collapsed');
			localStorage['legend-collapsed'] = 'false';
		}
	}
	if (localStorage['legend-collapsed'] == 'true')
		sidebarBottomHeader.onclick();
}


// ==============================================================================================
// ==============================================================================================
//													FILTER AND SEARCH ITEMS
// ==============================================================================================

var dateFilter = document.getElementById('date-filter');
var notifications;

dateFilter.onchange =
	function(){
		date_filter_onchange(this.value);
		//document.forms[0].onsubmit();
		if (organizationSettings.domain == organizationSettings.myself.email.split('@')[1])
			notifications.renderNotification();
		window.onpopstate({forcePop: true});
	};
document.forms[0].from_date.onchange =
	function(){
		dateFilter.value = 'custom';
		//document.forms[0].onsubmit();
		/*if (document.forms[0].to_date.value == document.forms[0].from_date.value)
			document.location = '#dashboard/'+document.forms[0].from_date.value;
		else*/
		if (organizationSettings.domain == organizationSettings.myself.email.split('@')[1])
			notifications.renderNotification();
		window.onpopstate({forcePop: true});
	};
document.forms[0].to_date.onchange =
	function(){
		dateFilter.value = 'custom';
		//document.forms[0].onsubmit();
		/*if (document.forms[0].to_date.value == document.forms[0].from_date.value)
			document.location = '#dashboard/'+document.forms[0].to_date.value;
		else*/
		if (organizationSettings.domain == organizationSettings.myself.email.split('@')[1])
			notifications.renderNotification();
		window.onpopstate({forcePop: true});
	};

var searchEmployee, searchRole, filterByManager, retrieveTerminated, showNewEmployees;

function bindFilters(){
	searchEmployee = document.querySelectorAll('input[name="searchEmployee"]');
	searchRole = document.querySelectorAll('select[name="searchRole"]');
	filterByManager = document.querySelectorAll('select[name="searchManager"]');
	retrieveTerminated = document.querySelectorAll('input[name="retrieveTerminated"]')[0];
	showNewEmployees =  document.querySelectorAll('input[name="showNewEmployees"]')[0];

	for (var i in [0, 1]) {
		if (arrayIgnore.indexOf(i) == -1) {
			new function(i) {
				// populate role select
				searchRole[i].appendChild(elem('option', '[ Filter by ]', {value: '*'}));
				for (var j in roles) {
					if (j > 0 && arrayIgnore.indexOf(j) == -1 && roles[j] != '') {
						searchRole[i].appendChild(elem('option', roles[j], {value: j}));
					}
				}
				sortList(searchRole[i], 0);
			}(i);
		}
	}
	// -----------------------------------------------------------------------------

	searchEmployee[0].onkeyup = function() {

		var employees = contentEmployees.querySelectorAll('tr.tablerow[data-id]');
		var found = false;
		var d_style = (employees[0].parentNode.parentNode.classList.contains('table--fixed-body') ? 'block' : 'table-row');

		for (var j = 0; j < employees.length; j++) {
			if ( ( this.value == '' || employees[j].querySelectorAll('td')[0].innerHTML.toLowerCase().indexOf(this.value.toLowerCase()) > -1 )
			&& (searchRole[0].value == '*' || employees[j].getAttribute('data-role') == searchRole[0].value)
			&& (i != 1 || retrieveTerminated.checked || employees[j].getAttribute('data-role') > 0) ){
				employees[j].style.display = d_style;
				found = true;

			}
			else {
				employees[j].style.display = 'none';
			}
		}
		noRecordsRow[0].style.display = found ? 'none' : d_style;
	}

	searchRole[0].onchange = function(e){
		searchEmployee[0].value = '';
		filterByManager[0].value = '*';
		if (this.value == '*')
			document.location.hash = 'employees';
		else
			document.location.hash = 'employees/role/' + this.value + '/' + this.options[this.selectedIndex].innerHTML.toLowerCase();
	}

	filterByManager[0].onchange = function(e){
		searchEmployee[0].value = '';
		searchRole[0].value = '*';
		if (this.value == '*'){
			employees4Mgr = -1;
			document.location.hash = 'employees';
		}
		else {
			document.location.hash = 'employees/under/' + this.value + '/' + (typeof processedData.employees[this.value] !== 'undefined' ? processedData.employees[this.value].email : processedData.self.email);
		}
	}

	// -----------------------------------------------------------------------------

	function filterEmpList () {
		let filters = {};
		let employees = contentSettings.querySelectorAll('tr.tablerow[data-id]');
		let found = false;
		let d_style = (employees[0].parentNode.parentNode.classList.contains('table--fixed-body') ? 'block' : 'table-row');
		let availableManagers = [];

		filters.search = (searchEmployee[1].value === "" ? true : searchEmployee[1].value.toLowerCase());
		filters.manager = (filterByManager[1].value === "*" ? true : parseInt(filterByManager[1].value));
		filters.retrieveTerminated = retrieveTerminated.checked;
		filters.showNew = showNewEmployees.checked;
		filters.role = ( searchRole[1].value === "*" ? true : parseInt(searchRole[1].value));


		for (let opts of filterByManager[1].options) {
			if (opts.value !== "*") {
				availableManagers.push(parseInt(opts.value));
			}
		}

		function filterRoles (emp) {
			if (filters.retrieveTerminated) {
				searchRole[1].setAttribute('disabled', true);
				return (parseInt(emp.getAttribute('data-role')) < 0);
			} else if (filters.showNew) {
				searchRole[1].setAttribute('disabled', true);
				return (parseInt(emp.getAttribute('data-role')) === 0);
			} else if ( (filters.role === true && parseInt(emp.getAttribute('data-role')) > 0)  || parseInt(emp.getAttribute('data-role')) === filters.role ) {
				searchRole[1].removeAttribute('disabled');
				return true;
			} else {
				searchRole[1].removeAttribute('disabled');
				return false;
			}
		}

		function filterManager (emp) {
			let empManager = parseInt(emp.getAttribute('data-manager'));

			if (filters.manager === 52 && availableManagers.indexOf(empManager) === -1) {
				return true;
			} else if (filters.manager === true || empManager === filters.manager) {
				return true;
			} else {
				return false;
			}
		}



		for (let emp of employees) {
			filterManager(emp);
			if (
				(filters.search === true || emp.querySelectorAll('td')[0].innerHTML.toLowerCase().indexOf(filters.search) > -1) &&
				filterManager(emp) &&
				filterRoles(emp)
			) {

				emp.style.display = d_style;
				found = true;

			} else {
				emp.style.display = 'none';
			}
		}
		noRecordsRow[1].style.display = found ? 'none' : d_style;
	}


	searchEmployee[1].onkeyup = function() {
		filterEmpList();
	}

	searchRole[1].onchange = function(e){
		filterEmpList();
	}

	filterByManager[1].onchange = function (e) {
		filterEmpList();
	}

	retrieveTerminated.onchange = function(e) {
		if (this.checked) {
			showNewEmployees.checked = false;
		}
		filterEmpList();
	}

	showNewEmployees.onchange = function (e) {
		if (this.checked) {
			retrieveTerminated.checked = false;
		}
		filterEmpList();
	}

}
