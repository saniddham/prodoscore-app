
function hideSaveCancelButtons(context){
	context.q('div.fix-at-bottom')[0].style.opacity = 0;
}

function showSaveCancelButtons(context){
	context.q('div.fix-at-bottom')[0].style.opacity = 1;
}

function showTurboBridgeSettings(context, params) {
	dateFilters.hide();
	eventLogger('start', 'confCallSettings');
	var formContainer = new scrollOpenClose(context.q('div.one-row'));
	var form = context.q('form')[0];
	var bridges = {},
		dropdowns = [];
	var employeedata = context.q('table.employeedata tbody')[0];

	//	---------------------------------------------------------------------------------------------------------
	//	Save Conf call settings
	//	---------------------------------------------------------------------------------------------------------
	form.onsubmit = function () {
		if (form.account_id.value.trim() == '') {
			showAlert('error', 'Please enter the Account ID');
			form.account_id.focus();
		}
		else if (form.admin.value.trim() == '') {
			showAlert('error', 'Please enter the Admin Email');
			form.admin.focus();
		}
		else if (form.password.value.trim() == ''){
		    showAlert('error', 'Please enter the Password');
		    form.password.focus();
		}
		else {
			eventLogger('start', 'saveConfCallSettings');
			new ajax(base_url + 'turbobridge-settings/', {
				method: 'POST',
				data: {
					//system: form.system.value,
					partner: form.partner_id.value,
					account: form.account_id.value,
					admin: form.admin.value,
					password: form.password.value
				},
				callback: function (data) {
					data = eval('(' + data.responseText + ')');
					if (data.result == 'success'){
						showAlert('success', 'Conference Call settings are saved.');
						form.q('input.button.red')[0].removeAttribute('disabled');
						showTurboBridgeSettings(context, params);
					}
					else{
						showAlert('error', 'Invalid Credentials.');
						eventLogger('end', 'saveConfCallSettings');
					}

				}
			});
		}
		return false;
	}
	//	---------------------------------------------------------------------------------------------------------
	//	Disconnect
	//	---------------------------------------------------------------------------------------------------------
	form.q('input.button.red')[0].onclick = function () {
		new ajax(base_url + 'disconnect-products/?product=turbobridge', {
			callback: function (data){
				formContainer.hide();
				showTurboBridgeSettings(context, params);
				showAlert('success', 'Disconnected successfully.');
			}
		});
	}
	//	---------------------------------------------------------------------------------------------------------
	//	Cancel credential form
	//	---------------------------------------------------------------------------------------------------------
	form.q('input.button.cancel.gray')[0].onclick = function () {
		formContainer.hide();
		return false;
	}
	//	---------------------------------------------------------------------------------------------------------
	//	Save mapping bridges to employees
	//	---------------------------------------------------------------------------------------------------------
	var form2 = context.q('form')[1];
	form2.onsubmit = function () {
		var selBridges = form2.q('tr.tablerow[data-id]'),
			bridge;
		var uploadData = {};
		for (var i = 0; i < selBridges.length; i++) {
			bridge = selBridges[i].q('select[data-value]')[0];
			if (bridge.className != '')
				uploadData[selBridges[i].getAttribute('data-id')] = bridge.value;
		}
		eventLogger('start', 'saveConfCallBridgeSettings');
		new ajax(base_url + 'turbobridge-settings/', {
			method: 'POST',
			data: {
				bridges: uploadData
			},
			callback: function (data) {
				for (var i = 0; i < selBridges.length; i++) {
					bridge = selBridges[i].q('select[data-value]')[0];
					bridge.className = '';
					bridge.setAttribute('data-value', bridge.value);
				}
				showAlert('success', 'Conference Call bridges are assigned.');
				eventLogger('end', 'saveConfCallBridgeSettings');
				scrollToTop();
			}
		});
		return false;
	}

	//	---------------------------------------------------------------------------------------------------------
	//	Reset mappings
	//	---------------------------------------------------------------------------------------------------------
	form2.q('input.button.cancel')[0].onclick = function () {
		var selects = form2.q('select[data-value]');
		for (var i = 0; i < selects.length; i++) {
			selects[i].value = selects[i].getAttribute('data-value');
			selects[i].className = '';
			selects[i].parentNode.setAttribute('sorttable_customkey', bridges[selects[i].getAttribute('data-value')]);
		}
		scrollToTop();
	}
	//	---------------------------------------------------------------------------------------------------------
	//	Load Conf call details
	//	---------------------------------------------------------------------------------------------------------
	hideSaveCancelButtons(context);
	new ajax(base_url + 'turbobridge-settings/', {
		callback: function (data) {
			data = eval('(' + data.responseText + ')');
			//form.system.value = data.settings.system;
			form.partner_id.value = data.settings.partner;
			form.account_id.value = data.settings.account;
			form.admin.value = data.settings.admin;

			if ( data.settings.account !== "" || data.settings.admin !== "") {
				form.password.value = '[ENCRYPTED]';
			}

			var formFilled = new checkFilled(form);
			//
			employeedata.innerHTML = '<tr class="no-records"><td colspan="2"><i>No records to display</i></td></tr>';
			if (typeof data.bridges.error != 'undefined') {
				form.q('input.button.red')[0].removeAttribute('disabled');
				if (data.bridges.error == 'credentials-not-set') {
					// showAlert('warning', 'Please enter Conference Call admin email and password');
					form.q('input.button.red')[0].setAttribute('disabled', true);
				}
				else if (data.bridges.error == 'credentials-wrong') {
					showAlert('error', 'The admin email and password you have entered is wrong');
				}
				else {
					showAlert('error', data.bridges.error.message);
				}
			}
			else if (data.bridges.length > 0) {
				formContainer.hide();
				showSaveCancelButtons(context);
				bridges = {}, dropdowns = [];
				for (var i = 0; i < data.bridges.length; i++)
					bridges[data.bridges[i].conferenceID] = (data.bridges[i].name == '' ? 'Unnamed - ' : data.bridges[i].name + ' - ') + data.bridges[i].conferenceID + ' - ' + (new Date(data.bridges[i].createdDate * 1000)).toString().substring(4, 15); // ['+data.bridges[i].tollNumber+']
				//
				employeedata.innerHTML = '';
				var dropdown;
				for (var i in data.employees) {
					var icon = usernameIcon(data.employees[i].fullname);
					dropdown = makeDropdown(bridges, data.employees[i].conf_id,
						function (sel) {
							enforceSingleAssignment(dropdowns);
						}, 'null')
					dropdowns.push(dropdown);
					var employee = arcReactor({
						tr: {
							class: 'tablerow',
							'data-id': i,
							content: [
								{
									td: {
										content: {
											div: {
												class: 'user-name-wrap',
												content: {
													p: {
														class: 'user-name',
														content: data.employees[i].fullname
													}
												}
											}
										},
										style: 'border-left:2px solid #' + icon[1]
									}
								},
								{
									td: {
										content: roles[data.employees[i].role]
									}
								},
								{
									td: {
										content: dropdown,
										'sorttable_customkey': bridges[data.employees[i].conf_id]
									}
								},
								]
						}
					});
					employeedata.appendChild(employee);
				}
				//
				sortColumn(employeedata.parentNode, 0);
				enforceSingleAssignment(dropdowns);
			}
			//	Not Else-If : If there were any error or no data
			if (typeof data.bridges.error != 'undefined' || data.bridges.length == 0)
				setTimeout(function () {
					formContainer.show();
				}, 250);
			//
			eventLogger('end', 'confCallSettings');
		}
	});
	// -----------------------
	form.elements[form.elements.length - 1].onclick = function () {
		formContainer.hide();
		return false;
	}
	context.q('input[type="button"]')[0].onclick = function () {
		formContainer.show();
		return false;
	}
	//	---------------------------------------------------------------------------------------------------------
}

function showBroadSoftSettings(context, params) {
	dateFilters.hide();
	eventLogger('start', 'confCallSettings');
	var formContainer = new scrollOpenClose(context.q('div.one-row'));

	var selectedPS = 'ringcentral';
	var psSelect = context.q('#phoneSystemSelect')[0];

	var form = context.q('form')[0];
	var rsForm = context.q('#RingCentralSettings')[0];
	var vbcForm = context.q('#vbcSettings')[0];

	var formFilled = new checkFilled(form);
	formFilled.disableBtn()

	var rc_auth_window = null,
		rc_auth_window_init = false,
		rc_auth_btn = rsForm.q('#rc_auth')[0];

	var vbc_auth_window = null,
		vbc_auth_window_init = false,
		vbc_auth_btn = vbcForm.q('#vbc_auth')[0];

	var rc_timer;
	var vbc_timer;

	var bridges = {},
		dropdowns = [];
	var employeedata = context.q('table.employeedata tbody')[0];

	var credTable = context.q('table.phone-systems-table tbody')[0],
			credsData = context.q('table.phone-systems-table tbody tr'),
			addNewRowBtn = context.q('button.addNewRowBtn')[0],
			newRowSchema = {
				tr: {
					class: 'new-row',
					content: [
						{
							td: {
								content: [
									{
										input: {
											type: 'hidden',
											name: 'id[]',
											value: 'new'
										}
									},
									{
										input: {
											class: 'td-input',
											type: "text",
											name: 'xsi[]',
											placeholder: "Phone number",
											"data-required": true
										}
									}

								]

							}
						},
						{
							td: {
								content: {
									input: {
										class: 'td-input',
										type: "text",
										name: 'userid[]',
										placeholder: "Admin username",
										"data-required": true
									}
								}
							}
						},
						{
							td: {
								content: {
									input: {
										class: 'td-input',
										type: "password",
										name: 'password[]',
										placeholder: "Password",
										"data-required": true
									}
								}
							}
						},
						{
							td: {
								content: [
									{
										button: {
											type: 'button',
											class: 'ac-link edit',
											content: 'edit'
										}
									},
									{
										button: {
											type: 'button',
											class: 'ac-link done',
											content: 'done'
										}
									},
									{
										button: {
											type: 'button',
											class: 'ac-link delete',
											content: 'delete'
										}
									},
									{
										i: {
											class: 'fa fa-exclamation',
											title: 'Invalid credentials',
											"aria-hidden": true
										}
									}
								]
							}
						}
					]
				}
			};

	function hideShowSystem() {
		if ( selectedPS == 'broadsoft' ) {
			form.style.display = 'block';
			rsForm.style.display = 'none';
			vbcForm.style.display = 'none';
		}
		else if ( selectedPS == 'ringcentral' ) {
			form.style.display = 'none';
			rsForm.style.display = 'block';
			vbcForm.style.display = 'none';
		}
		else if ( selectedPS == 'vbc' ) {
			form.style.display = 'none';
			rsForm.style.display = 'none';
			vbcForm.style.display = 'block';
		}
		else{
			form.style.display = 'none';
			rsForm.style.display = 'none';
			vbcForm.style.display = 'none';
		}
	}
	hideShowSystem();
	psSelect.onchange = function (e) {
		selectedPS = psSelect.value;
		hideShowSystem();
	}

	//	=========================================
	//	BROADSOFT - ADD/EDIT/DELETE ADMIN
	//	=========================================
	addNewRowBtn.onclick = function (){
		var newRow = arcReactor(newRowSchema);
		newRow.q('button.edit')[0].style.display = 'none';

		newRow.q('button.edit')[0].onclick = function () {
			editRow(newRow, this);
		};

		newRow.q('button.delete')[0].onclick = function () {
			deleteRow(newRow);
		};

		newRow.q('button.done')[0].onclick = function () {
			commitRow(newRow, this);
		};

		credTable.appendChild(newRow);

		formFilled.updateFields();
	};

	//	--------------------------------------------------------------------------------------------------
	//		There 3 functions are better moved to lib.js
	//	--------------------------------------------------------------------------------------------------
	//	--------------------------------------------------------------------------------------------------
	function commitRow (row, el) {
		var inputs = row.q('input'),
			actions = row.q('button.ac-link');

		if (inputs[1].value.trim() == '') {
			showAlert('error', 'Please enter the Admin Phone');//inputs[1].getAttribute('placeholder')
			inputs[1].focus();
			return false;
		}
		else if (inputs[2].value.trim() == '') {
			showAlert('error', 'Please enter the Admin Email/Username');//inputs[1].getAttribute('placeholder')
			inputs[2].focus();
			return false;
		}
		else if (inputs[3].value == 'new' && passwords[i].value == '[ENCRYPTED]') {
			showAlert('error', 'Please enter the password');//inputs[1].getAttribute('placeholder')
			inputs[3].focus();
			return false;
		}

		for ( var i = 0; i < inputs.length; i++ ) {
			var input = inputs[i];
			input.setAttribute("disabled", "disabled");
		}

		row.dataset.edit = false;
		row.removeClass('row-editing');
		row.removeClass('has-error');
		actions[0].innerHTML = "edit";
		actions[0].removeClass('discard');
		actions[0].addClass('edit');
		actions[0].style.display = 'inline-block';
		el.style.display = 'none';
	}

	function editRow (row, el) {
		var dataEdit = row.dataset.edit,
				inputs = row.q('input.td-input'),
				actions = row.q('button.ac-link');

		if ( typeof dataEdit == 'undefined' || dataEdit == 'false') {
			row.dataset.edit = true;
			for ( var i = 0; i < inputs.length; i++ ) {
				var input = inputs[i];
				input.dataset.prev = input.value;
				input.removeAttribute("disabled");
			}
			row.addClass('row-editing');
			inputs[0].focus();
			el.innerHTML = "discard";
			el.removeClass('edit');
			el.addClass('discard');
			actions[1].style.display = 'inline-block';
		}
		else {
			row.dataset.edit = false;
			for ( var i = 0; i < inputs.length; i++ ) {
				var input = inputs[i];
				input.value = input.dataset.prev;
				input.setAttribute("disabled", "disabled");
			}
			row.removeClass('row-editing');
			el.innerHTML = "edit";
			el.removeClass('discard');
			el.addClass('edit');
			actions[1].style.display = 'none';
		}

		return false;
	}

	function deleteRow (row) {
		row.parentNode.removeChild(row);
		formFilled.disableBtn();
		formFilled.updateFields();
		return false;
	}

	function initRow (row, data) {
		var cols = row.q('td'),
			inputs = row.q('input.td-input'),
			actions = cols[3].q('button.ac-link');

		if (  typeof data.error == 'undefined' || data.error == '' ) {
			// no error
			actions[1].style.display = 'none';
			for ( var i = 0; i < inputs.length; i++ ) {
				var ip = inputs[i];
				ip.setAttribute("disabled", "disabled");
			}
		}
		else {
			actions[0].style.display = 'none';
			row.addClass('has-error');
		}

		// edit
		actions[0].onclick = function () {
			editRow(row, this);
			return false;
		};

		// done
		actions[1].onclick = function () {
			commitRow(row, this);
			return false;
		};

		// delete
		actions[2].onclick = function () {
			//console.log(this);
			deleteRow(row);
			return false;
		};
	}
	//	=========================================
	//	=========================================


	if (typeof hasBroadSoftAlerts == 'undefined') {
		hidePrevAlerts();
	}
	//
	//	---------------------------------------------------------------------------------------------------------
	//	Save mapping bridges to employees
	//	---------------------------------------------------------------------------------------------------------
	var form2 = context.q('#formPSEmpData')[0];
	form2.onsubmit = function () {
		var selBridges = form2.q('tr.tablerow[data-id]'),
			bridge;
		var uploadData = {};
		for (var i = 0; i < selBridges.length; i++) {
			bridge = selBridges[i].q('.search-dropdown')[0];
			if (bridge.className != 'search-dropdown'){
				if (bridge.getAttribute('data-selected') == '')
					uploadData[selBridges[i].getAttribute('data-id')] = 'null';
				else
					uploadData[selBridges[i].getAttribute('data-id')] = bridge.getAttribute('data-selected');
			}
		}
		eventLogger('start', 'saveConfCallBridgeSettings');
		if ( selectedPS == 'ringcentral' ) {
			new ajax(base_url + 'ringcentral-settings/', {
				method: 'POST',
				data: {
					accounts: uploadData
				},
				callback: function (data) {
					for (var i = 0; i < selBridges.length; i++) {
						bridge = selBridges[i].q('.search-dropdown')[0];
						bridge.className = 'search-dropdown';
						//bridge.setAttribute('data-value', bridge.value);
						bridge.setAttribute('data-value', bridge.getAttribute('data-selected'));
					}
					showAlert('success', 'Phone System bridges are assigned.');
					hasBroadSoftAlerts = true;
					eventLogger('end', 'saveConfCallBridgeSettings');
					scrollToTop();
				}
			});
		}
		else if ( selectedPS == 'vbc' ) {
			new ajax(base_url + 'vbc-settings/', {
				method: 'POST',
				data: {
					accounts: uploadData
				},
				callback: function (data) {
					for (var i = 0; i < selBridges.length; i++) {
						bridge = selBridges[i].q('.search-dropdown')[0];
						bridge.className = 'search-dropdown';
						//bridge.setAttribute('data-value', bridge.value);
						bridge.setAttribute('data-value', bridge.getAttribute('data-selected'));
					}
					showAlert('success', 'Phone System bridges are assigned.');
					hasBroadSoftAlerts = true;
					eventLogger('end', 'saveConfCallBridgeSettings');
					scrollToTop();
				}
			});
		}
		else {
			new ajax(base_url + 'broadsoft-settings/', {
				method: 'POST',
				data: {
					bridges: uploadData
				},
				callback: function (data) {
					for (var i = 0; i < selBridges.length; i++) {
						bridge = selBridges[i].q('.search-dropdown')[0];
						bridge.className = 'search-dropdown';
						//bridge.setAttribute('data-value', bridge.value);
						bridge.setAttribute('data-value', bridge.getAttribute('data-selected'));
					}
					showAlert('success', 'Phone System bridges are assigned.');
					hasBroadSoftAlerts = true;
					eventLogger('end', 'saveConfCallBridgeSettings');
					scrollToTop();
				}
			});
		}

		return false;
	}
	//
	//	---------------------------------------------------------------------------------------------------------
	//	Reset form
	//	---------------------------------------------------------------------------------------------------------
	form2.q('input.button.cancel')[0].onclick = function () {
		var selects = dropdowns;//form2.q('.search-dropdown');
		for (var i = 0; i < selects.length; i++) {
			/*var tmp; tmp = selects[i].DOM.getAttribute('data-value').split(':')[1]; if (tmp == '') tmp = null;*/
			selects[i].setValue( selects[i].DOM.getAttribute('data-value') );
			//selects[i].className = '';
			selects[i].DOM.parentNode.setAttribute('sorttable_customkey', bridges[ selects[i].DOM.getAttribute('data-value') ]);//.split(':')[1]
		}
		scrollToTop();
	}
	// form.q('input[type="button"]')[0].onclick = function () {
	// 	BSCredentialsContainer.appendChild( arcReact({id: 'new', xsiface: '', userid: ''}, BSCredentialsRegent) );
	// }
	//	---------------------------------------------------------------------------------------------------------
	//	Save Conf call settings
	//	---------------------------------------------------------------------------------------------------------
	form.onsubmit = function () {
		hidePrevAlerts();
		var ids = form.elements['id[]'];
		var xsis = form.elements['xsi[]'];
		var userids = form.elements['userid[]'];
		var passwords = form.elements['password[]'];
		if (typeof ids == 'undefined'){
			ids = [];
			xsis = [];
			userids = [];
			passwords = [];
		}
		else if (typeof ids.length == 'undefined'){
			ids = [ids];
			xsis = [xsis];
			userids = [userids];
			passwords = [passwords];
		}
		//
		var data = [];
		var allPassed = xsis.length > 0;
		for (var i = 0; i < xsis.length; i++){
			if (xsis[i].value.trim() == '') {
				showAlert('error', 'Please enter the Admin Phone');
				xsis[i].focus();
				allPassed = false;
			}
			else if (userids[i].value.trim() == '') {
				showAlert('error', 'Please enter the Admin Email/Username');
				userids[i].focus();
				allPassed = false;
			}
			else if (ids[i].value == 'new' && passwords[i].value == '[ENCRYPTED]') {
				showAlert('error', 'Please enter the password');
				passwords[i].focus();
				allPassed = false;
			}
			else
				data.push({
					id: ids[i].value,
					xsiface: xsis[i].value,
					userid: userids[i].value,
					password: passwords[i].value
				});
		}
		if ( allPassed || data.length == 0 ){
			eventLogger('start', 'saveConfCallSettings');
			data.system = 'broadsoft';
			new ajax(base_url + 'broadsoft-settings/', {
				method: 'POST',
				data: data,
				callback: function (data) {
					data = eval('(' + data.responseText + ')');
					if (data.result == 'success'){
						form.q('input.button.red')[0].removeAttribute('disabled');
						showAlert('success', 'Phone System settings are saved.');
						hasBroadSoftAlerts = true
						showBroadSoftSettings(context, params);
					}
					else if (data.result == 'error'){
						showAlert('error', 'Invalid data.');
					}
					else{
						showAlert('error', 'Please enter valid data');
					}
					eventLogger('end', 'saveConfCallSettings');
				}
			});
		}
		return false;
	}

	//	---------------------------------------------------------------------------------------------------------
	//	RINGCENTRAL POPUP
	//	---------------------------------------------------------------------------------------------------------

	// reset button
	rc_auth_btn.innerHTML = 'Authenticate with RingCentral';
	rc_auth_btn.removeAttribute('disabled');

	function open_rc_popup () {
		var w = 500;
		var h = 500;

		// Fixes dual-screen position
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;

		if (rc_auth_window == null || rc_auth_window.closed) {
			rc_auth_window = window.open(
				base_url + 'rc-auth/',
				'rc_popup',
				'width=' + w + ',height=' + h + ',top=' + top + 'left=' + left + ',resizable,scrollbars,status'
			);
			rc_timer = setInterval(check_rc_window, 500);
		}
		else{
			rc_auth_window.focus();
		}
	}

	// check if rc_auth_window is closed
	function check_rc_window () {
		if (rc_auth_window.closed) {
			rc_auth_btn.innerHTML = 'Authenticate with RingCentral';
			rc_auth_btn.removeAttribute('disabled');
			clearInterval(rc_timer);
		}
	}

	window.handleRCResponse = function (response) {
		rc_auth_window.close();
		if ( response == 'success' ) {
			rc_auth_btn.innerHTML = '<i class="fa fa-check fa-fw"></i> Authentication successful';
			rsForm.q('input.button.red')[0].removeAttribute('disabled');
			showAlert('success', 'RingCentral has been successfully authenticated');
			hasBroadSoftAlerts = true
			showBroadSoftSettings(context, params);
		}
		else {
			rc_auth_btn.innerHTML = 'Authenticate with RingCentral';
			rc_auth_btn.removeAttribute('disabled');
			showAlert('error', response);
		}
	}

	rc_auth_btn.onclick = function(){
		this.setAttribute('disabled', true);
		this.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Authentication in progress';
		open_rc_popup();
	}

	//	---------------------------------------------------------------------------------------------------------
	//	RINGCENTRAL POPUP
	//	---------------------------------------------------------------------------------------------------------

	// reset button
	vbc_auth_btn.innerHTML = 'Authenticate with Vonage Business Cloud';
	vbc_auth_btn.removeAttribute('disabled');

	function open_vbc_popup(){
		var w = 500;
		var h = 585;

		// Fixes dual-screen position
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;

		if (vbc_auth_window == null || vbc_auth_window.closed){
			vbc_auth_window = window.open(
				base_url + 'vbc-auth/',
				'vbc_popup',
				'width=' + w + ',height=' + h + ',top=' + top + 'left=' + left + ',resizable,scrollbars,status'
			);
			vbc_timer = setInterval(check_vbc_window, 500);
		}
		else{
			vbc_auth_window.focus();
		}
	}

	// check if vbc_auth_window is closed
	function check_vbc_window(){
		if (vbc_auth_window.closed){
			vbc_auth_btn.innerHTML = 'Authenticate with Vonage Business Cloud';
			vbc_auth_btn.removeAttribute('disabled');
			clearInterval(vbc_timer);
		}
	}

	window.handleVBCResponse = function (response) {
		vbc_auth_window.close();
		if ( response == 'success' ) {
			vbc_auth_btn.innerHTML = '<i class="fa fa-check fa-fw"></i> Authentication successful';
			rsForm.q('input.button.red')[0].removeAttribute('disabled');
			showAlert('success', 'Vonage Business Cloud has been successfully authenticated');
			hasBroadSoftAlerts = true
			showBroadSoftSettings(context, params);
		}
		else {
			vbc_auth_btn.innerHTML = 'Authenticate with Vonage Business Cloud';
			vbc_auth_btn.removeAttribute('disabled');
			showAlert('error', response);
		}
	}

	vbc_auth_btn.onclick = function () {
		this.setAttribute('disabled', true);
		this.innerHTML = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Authentication in progress';
		open_vbc_popup();
	}

	//	//==========================================================================
	//	//==========================================================================
	//	Load Conf call details
	//	//==========================================================================
	//	//==========================================================================
	//*
	hideSaveCancelButtons(context);
	var dropdowns = [];
	new ajax(base_url + 'ringcentral-settings/', {
		callback: function (data) {
			data = eval('(' + data.responseText + ')');
			employeedata.innerHTML = '<tr class="no-records"><td colspan="3"><i>No records to display</i></td></tr>';
			//==========================================================================
			// Load RingCentral settings
			//==========================================================================
			if ( data.settings.system == 'ringcentral' ) {
				selectedPS = 'ringcentral';
				psSelect.value = selectedPS;
				hideShowSystem();
				//hideShowSystem();
				rsForm.removeAttribute('disabled');
				if ( typeof data.accounts.error != 'undefined' ) {
					// handle error
					hidePrevAlerts();
					showAlert('error', data.accounts.error);
					hasBroadSoftAlerts = true;
					if (data.accounts.error == 'credentials-not-set')
						rsForm.setAttribute('disabled', true);
				}
				else if ( data.accounts.length > 0 ) {
					var ringCentralAccountsAssignStatus = {};
					formContainer.hide();
					showSaveCancelButtons(context);
					accounts = {}, dropdowns = [];
					var dropdown;//, tmp;
					for (var i = 0; i < data.accounts.length; i++) {
						accounts[data.accounts[i].id] = data.accounts[i].id + ' - (' + (data.accounts[i].email != '' ? data.accounts[i].email : data.accounts[i].name) + ')';
						if (typeof ringCentralAccountsAssignStatus[data.accounts[i].id] == 'undefined'){
							ringCentralAccountsAssignStatus[data.accounts[i].id] = {};
							ringCentralAccountsAssignStatus[data.accounts[i].id]['user_set'] = false;
							ringCentralAccountsAssignStatus[data.accounts[i].id]['default_user_set'] = false;
							ringCentralAccountsAssignStatus[data.accounts[i].id]['default_user'] = {};
						}
					}
					//
					employeedata.innerHTML = '';
					for (var i in data.employees) {
						var icon = usernameIcon(data.employees[i].fullname);

						dropdown = new makeSearchDropdown(accounts, data.employees[i].ringcentral_id,
							function (sel) {
								//enforceSingleAssignment(dropdowns);
							}, 'null', dropdowns);

						dropdowns.push(dropdown);

						var employee = arcReactor({
							tr: {
								class: 'tablerow',
								'data-id': i,
								content: [
									{
										td: {
											content: {
												div: {
													class: 'user-name-wrap',
													content: {
														p: {
															class: 'user-name',
															content: data.employees[i].fullname
														}
													}
												}
											},
											style: 'border-left:2px solid #' + icon[1]
										}
									},
									{
										td: {
											content: roles[data.employees[i].role]
										}
									},
									{
										td: {
											content: dropdown.DOM,
											'sorttable_customkey': data.employees[i].ringcentral_id
										}
									},
								]
							}
						});
						employeedata.appendChild(employee);
						// ------------------------------------------- PREVENT SPOOKY ERROR
						new (function(dropdown, accounts, ringCentralAccountsAssignStatus, i){
							setTimeout(function(){
						// -------------------------------------------
								for (var j in accounts) {
									let tmp = accounts[j].split(' (');
									if (tmp.length > 1){
										if (tmp[1].toLowerCase() == data.employees[i].email.toLowerCase() + ')') {
											 if (ringCentralAccountsAssignStatus[j]['user_set'] == false){
												if (dropdown.value == 'null' || dropdown.value == null){
													dropdown.setValue(j);
													//dropdown.className = 'updated';
													dropdown.parentNode.setAttribute('sorttable_customkey', accounts[j]);
													ringCentralAccountsAssignStatus[j]['default_user'] = dropdown;
													ringCentralAccountsAssignStatus[j]['default_user_set'] = true;
												}
											}
										} else if (dropdown.value == j){
											if (ringCentralAccountsAssignStatus[j]['default_user_set'] == true){
												ringCentralAccountsAssignStatus[j]['default_user'].removeAttribute("class");
												ringCentralAccountsAssignStatus[j]['default_user'].value = 'null';
												ringCentralAccountsAssignStatus[j]['default_user'].parentNode.setAttribute('sorttable_customkey', 'undefined');
											} else {
												ringCentralAccountsAssignStatus[j]['user_set'] = true;
											}
										}
									}
								}
						// -------------------------------------------
							}, 10);
						})(dropdown, accounts, ringCentralAccountsAssignStatus, i);
						// -------------------------------------------
					}
					//
					sortColumn(employeedata.parentNode, 0);
					enforceSingleAssignment(dropdowns);
				}
				if (typeof data.accounts.error != 'undefined' || data.accounts.length == 0) {
					setTimeout(function () {
						formContainer.show();
					}, 250);
				}
				eventLogger('end', 'confCallSettings');
			}
			//==========================================================================
			// If the system is not RingCentral check if and load VonageBusinessCloud settings
			//==========================================================================
			else if ( data.settings.system == 'vbc' ) {
				selectedPS = 'vbc';
				psSelect.value = selectedPS;
				hideShowSystem();
				//hideShowSystem();
				new ajax(base_url + 'vbc-settings/', {
					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						vbcForm.removeAttribute('disabled');
						if ( typeof data.accounts == 'undefined' || typeof data.accounts.error != 'undefined' ) {
							// handle error
							hidePrevAlerts();
							showAlert('error', data.accounts.error);
							hasBroadSoftAlerts = true;
							if (data.accounts.error == 'credentials-not-set')
								vbcForm.setAttribute('disabled', true);
						}
						else if ( data.accounts.length > 0 ) {
							var vonageBusinessCloudAccountsAssignStatus = {};
							formContainer.hide();
							showSaveCancelButtons(context);
							accounts = {}, dropdowns = [];
							var dropdown;//, tmp;
							for (var i = 0; i < data.accounts.length; i++) {
								accounts[data.accounts[i].id] = data.accounts[i].extensionNumber + ' - (' + (data.accounts[i].email != '' ? data.accounts[i].email : data.accounts[i].name) + ')';
								if (typeof vonageBusinessCloudAccountsAssignStatus[data.accounts[i].id] == 'undefined'){
									vonageBusinessCloudAccountsAssignStatus[data.accounts[i].id] = {};
									vonageBusinessCloudAccountsAssignStatus[data.accounts[i].id]['user_set'] = false;
									vonageBusinessCloudAccountsAssignStatus[data.accounts[i].id]['default_user_set'] = false;
									vonageBusinessCloudAccountsAssignStatus[data.accounts[i].id]['default_user'] = {};
								}
							}
							//
							employeedata.innerHTML = '';
							for (var i in data.employees) {
								var icon = usernameIcon(data.employees[i].fullname);

								dropdown = new makeSearchDropdown(accounts, data.employees[i].ringcentral_id,
									function (sel) {
										//enforceSingleAssignment(dropdowns);
									}, 'null', dropdowns);

								dropdowns.push(dropdown);

								var employee = arcReactor({
									tr: {
										class: 'tablerow',
										'data-id': i,
										content: [
											{
												td: {
													content: {
														div: {
															class: 'user-name-wrap',
															content: {
																p: {
																	class: 'user-name',
																	content: data.employees[i].fullname
																}
															}
														}
													},
													style: 'border-left:2px solid #' + icon[1]
												}
											},
											{
												td: {
													content: roles[data.employees[i].role]
												}
											},
											{
												td: {
													content: dropdown.DOM,
													'sorttable_customkey': data.employees[i].ringcentral_id
												}
											},
										]
									}
								});
								employeedata.appendChild(employee);
								// ------------------------------------------- PREVENT SPOOKY ERROR
								new (function(dropdown, accounts, vonageBusinessCloudAccountsAssignStatus, i){
									setTimeout(function(){
								// -------------------------------------------
										for (var j in accounts) {
											let tmp = accounts[j].split(' (');
											if (tmp.length > 1){
												if (tmp[1].toLowerCase() == data.employees[i].email.toLowerCase() + ')') {
													 if (vonageBusinessCloudAccountsAssignStatus[j]['user_set'] == false){
														if (dropdown.value == 'null' || dropdown.value == null){
															dropdown.setValue(j);
															//dropdown.className = 'updated';
															dropdown.DOM.parentNode.setAttribute('sorttable_customkey', accounts[j]);
															vonageBusinessCloudAccountsAssignStatus[j]['default_user'] = dropdown;
															vonageBusinessCloudAccountsAssignStatus[j]['default_user_set'] = true;
														}
													}
												} else if (dropdown.value == j){
													if (vonageBusinessCloudAccountsAssignStatus[j]['default_user_set'] == true){
														vonageBusinessCloudAccountsAssignStatus[j]['default_user'].removeAttribute("class");
														vonageBusinessCloudAccountsAssignStatus[j]['default_user'].value = 'null';
														vonageBusinessCloudAccountsAssignStatus[j]['default_user'].parentNode.setAttribute('sorttable_customkey', 'undefined');
													} else {
														vonageBusinessCloudAccountsAssignStatus[j]['user_set'] = true;
													}
												}
											}
										}
								// -------------------------------------------
									}, 10);
								})(dropdown, accounts, vonageBusinessCloudAccountsAssignStatus, i);
								// -------------------------------------------
							}
							//
							sortColumn(employeedata.parentNode, 0);
							enforceSingleAssignment(dropdowns);
						}
						if (typeof data.accounts.error != 'undefined' || data.accounts.length == 0) {
							setTimeout(function () {
								formContainer.show();
							}, 250);
						}
						eventLogger('end', 'confCallSettings');
					}
				});
			}
			//==========================================================================
			// If the system is not RingCentral check if and load BroadSoft settings
			//==========================================================================
			else if ( data.settings.system == 'broadsoft' ){
				selectedPS = 'broadsoft';
				psSelect.value = selectedPS;
				hideShowSystem();
				new ajax(base_url + 'broadsoft-settings/', {
					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						data.bridges = [];
						var allPassed = true, progress = [0, 0];

						credTable.innerHTML = '';
						for (adminid in data.settings) {
							var newRow = arcReactor(newRowSchema),
								btns = newRow.q('td button'),
								inputs = newRow.q('td input');

							if ( data.settings[adminid].error == "credentials-wrong" ) {
								allPassed = false;
							}

							data.settings[adminid].id = adminid;
							inputs[0].value = adminid;
							inputs[1].value = data.settings[adminid].xsiface;
							inputs[2].value = data.settings[adminid].userid;
							inputs[3].value = '[ENCRYPTED]';
							initRow(newRow, data.settings[adminid]);
							credTable.appendChild(newRow);
							//
							progress[0] += 1;
							new ajax(base_url + 'broadsoft-bridges/?userid='+data.settings[adminid].userid, {
								callback: function (bridges) {
									if (bridges.responseText == '{"error": "credentials-wrong"}'){
										hidePrevAlerts();
										showAlert('error', 'The credentials you have entered for one or more records is invalid');
										//return false;
									}
									//
									bridges = eval('(' + bridges.responseText + ')');
									for (var i = 0; i < bridges.length; i++)
										data.bridges.push(bridges[i]);
									//
									progress[1] += 1;
									if (progress[0] == progress[1]){
										continueLoading();
										form.q('input.button.red')[0].removeAttribute('disabled');
									}
								}
							});
						}

						formFilled.updateFields();

						if ( !allPassed ) {
							hidePrevAlerts();
							showAlert('error', 'The credentials you have entered for one or more records is invalid');
							hasBroadSoftAlerts = true;
						}
						// ---------------------------------------------------------------------------
						var continueLoading = function(){
							//employeedata.innerHTML = '<tr class="no-records"><td colspan="3"><i>No records to display</i></td></tr>';
							if (typeof data.bridges.error != 'undefined') {
								if (data.bridges.error == 'credentials-not-set') {
									// showAlert('warning', 'Please enter Phone System admin email and password');
								}
								else if (data.bridges.error == 'credentials-wrong') {
									showAlert('error', 'The admin email and password you have entered is wrong');
									hasBroadSoftAlerts = true;
								}
								else {
									showAlert('error', data.bridges.error.message);
									hasBroadSoftAlerts = true;
								}
							}
							else if (data.bridges.length > 0) {
								formContainer.hide();
								showSaveCancelButtons(context);
								bridges = {}, dropdowns = [];
								var tmp;
								for (var i = 0; i < data.bridges.length; i++) {
									tmp = data.bridges[i].userid.split('@')
									bridges[data.bridges[i].admin + ':' + data.bridges[i].userid] = tmp[0] + ' - ' + ' (' + (data.bridges[i].email != '' ? data.bridges[i].email : data.bridges[i].name) + ')'; // + data.bridges[i].name;
								}
								//
								var dropdown;
								employeedata.innerHTML = '';
								for (var i in data.employees) {
									var icon = usernameIcon(data.employees[i].fullname);
									dropdown = new makeSearchDropdown(bridges, data.employees[i].broadsoft.admin + ':' + data.employees[i].broadsoft.userid,
										function (sel) {
											//enforceSingleAssignment(dropdowns);
										}, 'null', dropdowns);
									dropdowns.push(dropdown);
									var employee = arcReactor({
										tr: {
											class: 'tablerow',
											'data-id': i,
											content: [
												{
													td: {
														content: {
															div: {
																class: 'user-name-wrap',
																content: {
																	p: {
																		class: 'user-name',
																		content: data.employees[i].fullname
																	}
																}
															}
														},
														style: 'border-left:2px solid #' + icon[1]
													}
												},
												{
													td: {
														content: roles[data.employees[i].role]
													}
												},
												{
													td: {
														content: dropdown.DOM,
														'sorttable_customkey': bridges[data.employees[i].broadsoft.admin + ':' + data.employees[i].broadsoft.userid]
													}
												},
											]
										}
									});
									employeedata.appendChild(employee);
									if (dropdown.value == 'null' || dropdown.value == null)
										for (var j in bridges) {
											tmp = bridges[j].split(' (');
											if (tmp.length > 1 && tmp[1].toLowerCase() == data.employees[i].email.toLowerCase() + ')') {
												dropdown.setValue(j);
												//dropdown.DOM.className = 'updated';
												//dropdown.DOM.parentNode.setAttribute('sorttable_customkey', bridges[j]);
											}
										}
								}
								//
								sortColumn(employeedata.parentNode, 0);
								enforceSingleAssignment(dropdowns);
							}
							//	Not Else-If : If there were any error or no data
							if (typeof data.bridges.error != 'undefined' || data.bridges.length == 0)
								setTimeout(function () {
									formContainer.show();
								}, 250);
							//
							eventLogger('end', 'confCallSettings');
						}
						if (Object.keys(data.settings).length == 0){
							continueLoading();
							formContainer.show();
							form.q('input.button.red')[0].setAttribute('disabled', true);
						}
						// ---------------------------------------------------------------------------
						if (Object.keys(data.settings).length == 0)
							continueLoading();
					}
				});

			}
			//==========================================================================
			//==========================================================================
			else{
				// NO PHONE SYSTEM SELECTED
				eventLogger('end', 'confCallSettings');
				formContainer.show();
				selectedPS = 'none';
				psSelect.value = '';
				form.q('input.button.red')[0].setAttribute('disabled', true);
				rsForm.q('input.button.red')[0].setAttribute('disabled', true);
				hideShowSystem();
			}
		}
	});
	//*/
	//	---------------------------------------------------------------------------------------------------------
	form.q('input.button.cancel.gray')[0].onclick = function () {
		formContainer.hide();
		return false;
	}
	rsForm.q('.button.cancel')[0].onclick = function () {
		formContainer.hide();
		return false;
	}
	vbcForm.q('.button.cancel')[0].onclick = function () {
		formContainer.hide();
		return false;
	}
	//	---------------------------------------------------------------------------------------------------------
	//	Disconnect
	//	---------------------------------------------------------------------------------------------------------
	form.q('input.button.red')[0].onclick = function () {
		new ajax(base_url + 'disconnect-products/?product=broadsoft', {
			callback: function (data){
				for (var i = 0; i < credTable.rows.length; i++)
					deleteRow(credTable.rows[i]);
				formContainer.hide();
				showBroadSoftSettings(context, params);
				showAlert('success', 'Disconnected successfully.');
			}
		});
	}
	rsForm.q('input.button.red')[0].onclick = function () {
		new ajax(base_url + 'disconnect-products/?product=ringcentral', {
			callback: function (data){
				formContainer.hide();
				showBroadSoftSettings(context, params);
				showAlert('success', 'Disconnected successfully.');
			}
		});
	}
	vbcForm.q('input.button.red')[0].onclick = function () {
		new ajax(base_url + 'disconnect-products/?product=vbc', {
			callback: function (data){
				formContainer.hide();
				showBroadSoftSettings(context, params);
				showAlert('success', 'Disconnected successfully.');
			}
		});
	}
	//	---------------------------------------------------------------------------------------------------------
	context.q('input[type="button"]')[0].onclick = function () {
		formContainer.show();
		return false;
	}
	//	---------------------------------------------------------------------------------------------------------
}

function showCRMSettings(context, params) {
	dateFilters.hide();

	eventLogger('start', 'CRMSettings');
	var formContainer = new scrollOpenClose(context.q('div.one-row'));
	var form = context.q('form')[0],
		formSalesforce = context.q('form')[1],
		formProsperWorks = context.q('form')[2],
		formSugarCRM = context.q('form')[3],
		formModules = context.q('.crm-modules form')[0];

	var accounts = {},
		dropdowns = [];
	var employeedata = context.q('table.employeedata tbody')[0];

	var zohoFilled = new checkFilled(form),
		sfFilled = new checkFilled(formSalesforce),
		ppWFilled = new checkFilled(formProsperWorks),
		sgFilled = new checkFilled(formSugarCRM);

	var systemSelect = document.getElementById('system'),
		selectedSystemVal = systemSelect.value,
		selectedSystem = null;

	var crmHelpModal;
	var crmWarnModal;

	var availableModules = {
		zoho: [
			['leads', 0],
			['invoices', 0],
			['accounts', 0],
			['tasks', 0],
			['events', 0],
			['calls', 0]
		],
		salesforce: [
			['New Leads', 0],
			['New Opportunities', 0],
			['Chatter Messages', 0],
			['Chatter Activities', 0],
			['Calls Logged',0],
			['Activities',0]
		],
		prosperworks: [
			['leads', 0],
			['opportunities', 0]
		],
		sugarCRM: [
			['leads', 0],
			['opportunities', 0],
			['meetings', 0],
			['calls', 0]
		]
	};

	var formData = {};

	if (typeof hasCRMAlerts == 'undefined') {
		hidePrevAlerts();
	}

	context.q('.crm-modules')[0].style.display = 'none';

	// modules
	function renderModules (data) {
		let modules = availableModules[selectedSystem];
		let preVals;
		let moduleHolder = context.q('.crm-modules .module-switches')[0];
		let moduleHolderHeading = context.q('.crm-modules .card-heading')[0];
		let moduleBtn = context.q('.crm-modules input[type="submit"]')[0];
		let sysText = '';

		if ( typeof modules === 'undefined' ) return false;

		switch (selectedSystem) {
			case 'zoho':
				sysText = 'ZOHO';
				break;
			case 'salesforce':
				sysText = 'Salesforce';
				break;
			case 'prosperworks':
				sysText = 'ProsperWorks';
				break;
			case 'sugarCRM':
				sysText = 'SugarCRM';
				break;
			default:
				sysText = 'CRM System';
		}

		moduleHolderHeading.innerHTML = 'Available modules for ' + sysText;

		function updateModules (dule, value) {
			let modVal = [];

			for (var m of modules) {
				if (m[0] === dule) {
					m[1] = (value ? 1 : 0);
				}
				modVal.push(m[1]);
			}

			modVal = modVal.reverse().join('');
			modVal = parseInt(modVal, 2);

			if (preVals !== modVal) {
				moduleBtn.removeAttribute('disabled');
			} else {
				moduleBtn.setAttribute('disabled', true);
			}

		}

		for (let i = 0; i < modules.length; i++) {
			let cb;
			let nom = modules[i][0].trim().replace('_', ' ');
			modules[i][1] = parseInt(data[i]);

			cb = elem('label', '<input type="checkbox" name="modules[' + modules[i][0] + ']">' + '<span class="text">' + toTitleCase(nom) + '</span>', {
				class: 'module-switches_switch'
			});

			cb.q('input')[0].onchange = function () {
				updateModules(this.name.replace('modules[', '').replace(']',''), this.checked);
			}

			if (modules[i][1] === 1) {
				cb.q('input')[0].checked  = true;
			}

			moduleHolder.appendChild(cb);
		}

		preVals = (function () {
			let x = [];

			for (let m of modules) {
				x.push(m[1]);
			}

			x = x.reverse().join('');
			x = parseInt(x, 2);

			return x;

		})();

	}

	//	---------------------------------------------------------------------------------------------------------
	//	CRM Toggle fields
	//	---------------------------------------------------------------------------------------------------------

	var toggleSystem = function () {
		var crmSystemBlocks = document.getElementsByClassName("crm-system-block");
		for (var i = 0; i < crmSystemBlocks.length; i++) {
			crmSystemBlocks[i].style.display = 'none';
		}
		if (selectedSystemVal) {
			document.getElementById("system-" + selectedSystemVal).style.display = 'block';
		}
		fillForm(false);
	}

	systemSelect.onchange = function () {
		selectedSystemVal = systemSelect.value;
		toggleSystem();
	};

	function validateForm (form, names) {
		let result = {valid: true, invalidEl:[], errMsg:[]};
		for ( let name of names ) {
			let val = form[name].value;

			form[name].removeClass('has-error');

			if ( val.trim() == "" || (name==='token' && val.length >40)) {
				result.valid = false;
				result.invalidEl.push(form[name]);
				let msg = (name==='token' && val.length >40) ? 'Token exceeds the expected length' :'Please fill the following fields';
				result.errMsg.push(msg);
				form[name].addClass('has-error');
			}
		}
		return result;
	}

	//	---------------------------------------------------------------------------------------------------------
	//	Save CRM Modules
	//	---------------------------------------------------------------------------------------------------------
	formModules.onsubmit = function () {
		let modules = [],
			moduleEls = context.q('.module-switches input[type="checkbox"]'),
			modBtn = context.q('.crm-modules input[type="submit"]')[0];

		eventLogger('start', 'saveCRMModules');
		modBtn.setAttribute('disabled', true);

		for (let m of moduleEls) {
			modules.push(m.checked ? '1' : '0');
		}

		modules = modules.reverse().join('');
		modules = parseInt(modules, 2);

		new ajax(base_url + 'crm-settings/', {
			method: 'POST',
			data: {
				modules: modules
			},
			callback: function (data) {
				showAlert('success', 'CRM modules are saved.');
				eventLogger('end', 'saveCRMModules');
			},
			fallback: function (e) {
				showAlert('error', 'An error occoured while trying to save CRM modules.');
				modBtn.removeAttribute('disabled');
				eventLogger('end', 'saveCRMModules');
			}
		});
		return false;
	}

	//	---------------------------------------------------------------------------------------------------------
	//	Save CRM Settings
	//	---------------------------------------------------------------------------------------------------------
	form.onsubmit = function () {
		eventLogger('start', 'saveCRMSettings');
		var formValid = validateForm(this, ['token']);

		if ( formValid.valid ) {
			new ajax(base_url + 'crm-settings/', {
				method: 'POST',
				data: {
					system: systemSelect.value,
					token: form.token.value
				},
				callback: function (data) {
					data = eval('(' + data.responseText + ')');
					if (data.result == 'Success'){
						hasCRMAlerts = true;
						form.q('input.button.red')[0].removeAttribute('disabled');
						showAlert('success', 'CRM settings are saved.');
						showCRMSettings(context, params);
					}
					else{
						showAlert('error', 'Invalid Credentials');
						eventLogger('end', 'saveCRMSettings');
					}
				}
			});
		}
		else {
			scrollToTop();
			formValid.invalidEl[0].focus();
			showAlert('error', formValid.errMsg[0]);
			eventLogger('end', 'saveCRMSettings');
		}

		return false;
	}

	//	---------------------------------------------------------------------------------------------------------
	//	Save Salesforce Settings
	//	---------------------------------------------------------------------------------------------------------
	formSalesforce.onsubmit = function () {
		eventLogger('start', 'saveSalesforceSettings');
		var formValid = validateForm(this, ['userid', 'password', 'salesforce_token']);

		if ( formValid.valid ) {
			new ajax(base_url + 'crm-settings/', {
				method: 'POST',
				data: {
					system: systemSelect.value,
					admin: formSalesforce.userid.value,
					password: formSalesforce.password.value,
					token: formSalesforce.salesforce_token.value
				},
				callback: function (data) {
					data = eval('(' + data.responseText + ')');
					if (data.result){
						hasCRMAlerts = true;
						formSalesforce.q('input.button.red')[0].removeAttribute('disabled');
						showAlert('success', 'CRM settings are saved.');
						showCRMSettings(context, params);
					}
					else{
						showAlert('error', 'Invalid Credentials');
						eventLogger('end', 'saveSalesforceSettings');
					}
				}
			});
		}
		else {
			scrollToTop();
			formValid.invalidEl[0].focus();
			showAlert('error', 'Please fill the following fields');
			eventLogger('end', 'saveSalesforceSettings');
		}

		return false;
	}

	//	---------------------------------------------------------------------------------------------------------
	//	Save ProsperWorks Settings
	//	---------------------------------------------------------------------------------------------------------
	formProsperWorks.onsubmit = function () {
		eventLogger('start', 'saveProsperWorksSettings');

		var formValid = validateForm(this, ['crm_admin', 'prosperworks_token']);

		if ( formValid.valid ) {
			new ajax(base_url + 'crm-settings/', {
				method: 'POST',
				data: {
					system: systemSelect.value,
					crm_admin: formProsperWorks.crm_admin.value,
					token: formProsperWorks.prosperworks_token.value
				},
				callback: function (data) {
					data = eval('(' + data.responseText + ')');
					if (data.result){
						hasCRMAlerts = true;
						formProsperWorks.q('input.button.red')[0].removeAttribute('disabled');
						showAlert('success', 'CRM settings are saved.');
						showCRMSettings(context, params);
					}
					else{
						showAlert('error', 'Invalid Credentials');
						eventLogger('end', 'saveProsperWorksSettings');
					}
				}

			});
		}
		else {
			scrollToTop();
			formValid.invalidEl[0].focus();
			showAlert('error', 'Please fill the following fields');
			eventLogger('end', 'saveProsperWorksSettings');
		}


		return false;
	}

	//	---------------------------------------------------------------------------------------------------------
	//	Save SugarCRM Settings
	//	---------------------------------------------------------------------------------------------------------
	formSugarCRM.onsubmit = function () {
		eventLogger('start', 'saveSugarCRMSettings');
		var formValid = validateForm(this, ['instance', 'sugarcrm_password', 'crm_admin',
		'sugar_key', 'sugar_key_name']);

		if ( formValid.valid ) {
			new ajax(base_url + 'crm-settings/', {
				method: 'POST',
				data: {
					system: systemSelect.value,
					crm_admin: formSugarCRM.crm_admin.value,
					password: formSugarCRM.sugarcrm_password.value,
					instance: formSugarCRM.instance.value,
					key_name: formSugarCRM.sugar_key_name.value,
					key: formSugarCRM.sugar_key.value
				},

				callback: function (data) {
					data = eval('(' + data.responseText + ')');
					if (data.result) {
						hasCRMAlerts = true;
						saveSuccess = true;
						formSugarCRM.q('input.button.red')[0].removeAttribute('disabled');
						showCRMSettings(context, params);
					} else {
						scrollToTop();
						showAlert( 'error', 'The credentials you have entered are either invalid or are already in use.' );
						eventLogger('end', 'saveSugarCRMSettings');
					}
				}
			});
		}
		else {
			scrollToTop();
			formValid.invalidEl[0].focus();
			showAlert('error', 'Please fill the following fields');
			eventLogger('end', 'saveSugarCRMSettings');
		}


		return false;
	}


	//	---------------------------------------------------------------------------------------------------------
	//	CRM Mapping employees
	//	---------------------------------------------------------------------------------------------------------
	var form2 = context.q('form.crm-employees')[0];
	form2.onsubmit = function () {
		var selAccounts = form2.q('tr.tablerow[data-id]'),
			account,
			postURL = '',
			postData = {};
		var uploadData = {};

		for (var i = 0; i < selAccounts.length; i++) {
			account = selAccounts[i].q('.search-dropdown')[0];
			if (account.className.indexOf('updated') > -1)
				uploadData[selAccounts[i].getAttribute('data-id')] = account.getAttribute('data-selected');
		}

		postURL = 'crm-settings/';
		postData = {
			accounts: uploadData
		};

		eventLogger('start', 'saveCRMSettings');
		new ajax(base_url + postURL, {
			method: 'POST',
			data: postData,
			callback: function (data) {
				for (var i = 0; i < selAccounts.length; i++) {
					account = selAccounts[i].q('.search-dropdown')[0];
					account.className = 'search-dropdown';
					account.setAttribute('data-value', account.getAttribute('data-selected'));
				}
				showAlert('success', 'CRM accounts are assigned.');
				eventLogger('end', 'saveCRMSettings');
				scrollToTop();
			}
		});
		return false;
	}
	//	---------------------------------------------------------------------------------------------------------
	//	Reset form
	//	---------------------------------------------------------------------------------------------------------
	form2.q('input.button.cancel')[0].onclick = function () {
		//var selects = form2.q('select[data-value]');
		for (var i = 0; i < dropdowns.length; i++) {
			dropdowns[i].setValue(dropdowns[i].DOM.getAttribute('data-value'));
			//dropdowns[i].DOM.removeClass('updated');
			dropdowns[i].DOM.parentNode.setAttribute('sorttable_customkey', accounts[dropdowns[i].DOM.getAttribute('data-value')]);
		}
		scrollToTop();
	}

	// populate employee data table
	var dropdowns;
	function populateData (data, callback) {
		let employees = data.employees,
			d_acc			= data.accounts,
			system		= data.settings.system,
			accounts 	= {},
			accountsAssignStatus = {};
		dropdowns = [];

		for ( let i = 0; i < d_acc.length; i++ ) {
			if(typeof accountsAssignStatus[d_acc[i].id] == 'undefined'){
				accountsAssignStatus[d_acc[i].id] = {};
				accountsAssignStatus[d_acc[i].id]['user_set'] = false;
				accountsAssignStatus[d_acc[i].id]['default_user_set'] = false;
				accountsAssignStatus[d_acc[i].id]['default_user'] = {};
			}
			if ( system == 'zoho' ) {
				accounts[d_acc[i].id] = d_acc[i].name + ' [' + d_acc[i].role + '] ' + d_acc[i].profile;
			} else if ( system == 'prosperworks' ) {
				accounts[d_acc[i].id] = d_acc[i].name + ' [' + d_acc[i].role + ']';
			} else if ( system == 'salesforce' ) {
				accounts[d_acc[i].id] = d_acc[i].name + ' [' + d_acc[i].email + ']';
			} else if ( system == 'sugarCRM' ) {
				accounts[d_acc[i].id] = d_acc[i].name + (d_acc[i].email ? ' [' + d_acc[i].email + ']' : '' );
			} else {
				break;
			}
		}

		employeedata.innerHTML = '';

		for ( let i in employees ) {
			let icon = usernameIcon(employees[i].fullname),
					dropdown, employee;

			dropdown = new makeSearchDropdown(accounts, employees[i].crm_id, function(sel) { /*enforceSingleAssignment(dropdowns)*/ }, 'null', dropdowns);

			dropdowns.push(dropdown);

			employee = arcReactor({
				tr: {
					class: 'tablerow',
					'data-id': i,
					content: [{
							td: {
								content: {
									div: {
										class: 'user-name-wrap',
										content: {
											p: {
												class: 'user-name',
												content: employees[i].fullname
											}
										}
									}
								},
								style: 'border-left:2px solid #' + icon[1]
							}
						},
						{
							td: {
								content: roles[employees[i].role]
							}
						},
						{
							td: {
								content: dropdown.DOM,
								'sorttable_customkey': accounts[employees[i].crm_id]
							}
						},
					]
				}
			});

				for ( let j in accounts ) {
					if ( accounts[j].split(' [')[0].toLowerCase() == employees[i].fullname.toLowerCase()) {
						if (accountsAssignStatus[j]['user_set'] == false){
							if (dropdown.value == 'null' || dropdown.value == null){
								dropdown.setValue(j);
								//dropdown.className = 'updated';
								dropdown.DOM.parentNode.setAttribute('sorttable_customkey', accounts[j]);
								accountsAssignStatus[j]['default_user'] = dropdown;
								accountsAssignStatus[j]['default_user_set'] = true;
							}
						}
					} else if (dropdown.value == j){
						if (accountsAssignStatus[j]['default_user_set'] == true){
							//accountsAssignStatus[j]['default_user'].removeAttribute("class");
							accountsAssignStatus[j]['default_user'].setValue(null);
							accountsAssignStatus[j]['default_user'].DOM.parentNode.setAttribute('sorttable_customkey', 'undefined');
						} else {
							accountsAssignStatus[j]['user_set'] = true;
						}
					}
				}
			employeedata.appendChild(employee);
		}

		sortColumn(employeedata.parentNode, 0);
//		enforceSingleAssignment(dropdowns);		***********************************************

		callback();
	}

	function fillForm (toggle) {
		toggle = (typeof toggle !== 'undefined') ?  toggle : true;
		form.q('input.button.red')[0].setAttribute('disabled', true);
		formSalesforce.q('input.button.red')[0].setAttribute('disabled', true);
		formProsperWorks.q('input.button.red')[0].setAttribute('disabled', true);
		formSugarCRM.q('input.button.red')[0].setAttribute('disabled', true);
		switch (selectedSystem) {
			case 'zoho':
				if ( typeof formData.accounts.error === 'undefined' && typeof formData.accounts !== 'string' ) {
					form.token.value = '[ENCRYPTED]';
					form.q('input.button.red')[0].removeAttribute('disabled');
				}
				break;
			case 'salesforce':
				formSalesforce.userid.value = formData.settings.crm_admin || '';
				if ( formData.settings.crm_admin !== "" ) {
					formSalesforce.password.value = '[ENCRYPTED]';
					formSalesforce.salesforce_token.value = '[ENCRYPTED]';
					formSalesforce.q('input.button.red')[0].removeAttribute('disabled');
				}
				break;
			case 'prosperworks':
				formProsperWorks.crm_admin.value = formData.settings.crm_admin || '';
				if ( formData.settings.crm_admin !== "" ) {
					formProsperWorks.prosperworks_token.value = '[ENCRYPTED]';
					formProsperWorks.q('input.button.red')[0].removeAttribute('disabled');
				}
				break;
			case 'sugarCRM':
				formSugarCRM.instance.value = formData.settings.crm_instance || '';
				formSugarCRM.crm_admin.value = formData.settings.crm_admin || '';
				formSugarCRM.sugar_key_name.value = formData.settings.sugar_key_name || '';
				if (formData.settings.crm_admin !== "") {
					formSugarCRM.sugar_key.value = '[ENCRYPTED]';
					formSugarCRM.sugarcrm_password.value = '[ENCRYPTED]';
					formSugarCRM.q('input.button.red')[0].removeAttribute('disabled');
				}
				break;
			default:
				if (toggle) {
					formContainer.show(toggleSystem());
				}
				eventLogger('end', 'CRMSettings');
		}
		if (typeof crmHelpModal === 'undefined') {
			let bd = '';
				bd += '<ol class="ol">';
				bd += '<li>Log in to your Sugar Instance using the Admin account</li>'
				bd += '<li>Navigate to <b>\'Administration\'</b></li>';
				bd += '<li>Select <b>\'OAuth Keys\'</b> under <b>\'System\'</b> section</li>';
				bd += '<li>On the navigation bar select the downward arrow next to \'OAuth Keys\' and select <b>\'Create OAuth Key\'</b></li>';
				bd += '<li>Create the OAuth key and select the OAuth Version as <b>OAuth 2.0</b></li>';
				bd += '<li>Save and add the details to Prodoscore</li>';
				bd += '</ol>'

            crmHelpModal = new modal({
                title: 'Instructions',
                body: bd,
                footer: '<button class="button gray modal-ok">OK</button>'
            });

            crmHelpModal.el.q('.modal-ok')[0].onclick = function () {
                crmHelpModal.hide();
            };

            formSugarCRM.q('.help-text a')[0].onclick = function (e) {
                e.preventDefault();
                crmHelpModal.show();
            }
		}
		if (typeof crmWarnModal === 'undefined') {

		    bd = `<ol>
		            <lh class="lh-left-margin"><b>You need to edit the following files in the Sugar CRM </b></lh>
		            <lh class="lh-left-margin"><b>directory.</b></lh>
		            <li class="with-top-margin"><b>config_override.php</b>
                        <dl class="no-top-margin">
                            <dt><i>Change</i></dt>
                            <dt style="color: #6b6e71;"> $sugar_config[\'disable_unknown_platforms\'] = true</dt>
                            <dt><i>into</i></dt>
                            <dt style="color: #6b6e71;">$sugar_config[\'disable_unknown_platforms\'] = false;</dt>
                        </dl>
		            </li>
                    <li><b>custom/client/platform.php</b>
                        <dl class="no-top-margin">
                            <dt><i>There will be three lines in it as follows,</i></dt>
                                <dd style="color: #6b6e71;">$platforms[] = \'base\';</dd>
                                <dd style="color: #6b6e71;">$platforms[] = \'mobile\';</dd>
                                <dd style="color: #6b6e71;">$platforms[] = \'portal\';</dd>
                            <dt><i>Add a new line as follows,</i></dt>
                                <dd style="color: #6b6e71;">$platforms[] = \'prodoscore\';</dd>
                        </dl>
                    </li>
		         </ol>`;

            crmWarnModal = new modal({
                title: 'Instructions',
                body: bd,
                footer: '<button class="button gray modal-ok">OK</button>'
            });

            crmWarnModal.el.q('.modal-ok')[0].onclick = function () {
                crmWarnModal.hide();
            };

            formSugarCRM.q('.warn-text a')[0].onclick = function (e) {
                e.preventDefault();
                crmWarnModal.show();
            }
		}
	}

	//	---------------------------------------------------------------------------------------------------------
	//	Load data
	//	---------------------------------------------------------------------------------------------------------
	hideSaveCancelButtons(context);
	new ajax( base_url + 'crm-settings/', {
		callback: function (data) {
			data = eval( '(' + data.responseText + ')' );
			formData = data;
			selectedSystem = data.settings.system;
			hidePrevAlerts();

			// set selectedSystem in system drop down
			for ( var i = 0, optionsLength = systemSelect.options.length; i < optionsLength; i++ ) {
				if (systemSelect.options[i].value == data.settings.system) {
					systemSelect.selectedIndex = i;
					selectedSystemVal = systemSelect.value;
				}
			}

			if ( typeof data.accounts.error !== 'undefined' ) {
				if ( typeof data.accounts.error == 'string' ) {
					showAlert( 'error', data.accounts.error );
				}
				else if ( typeof data.accounts.error.message == 'string' ) {
					showAlert( 'error', data.accounts.error.message );
				}
				formContainer.show(toggleSystem());
				eventLogger('end', 'CRMSettings');
			}
			else if ( typeof data.accounts == 'string' ) {
				showAlert( 'error', data.accounts );
				formContainer.show(toggleSystem());
				eventLogger('end', 'CRMSettings');
			}

			if (typeof saveSuccess != 'undefined') {
				if ( saveSuccess == true ) {
					saveSuccess = false;
					showAlert('success', 'CRM settings are saved.');
				}
			}
			// token and password fields are set to '[ENCRYPTED]' to prevent autofilling
			// if the value == '[ENCRYPTED]' it's not saved

			if (data.settings.crm_modules === null) {
				data.settings.crm_modules = 0;
			}

			data.settings.crm_modules = ('000000' + data.settings.crm_modules.toString(2)).split('').reverse();

			context.q('.crm-modules .module-switches')[0].innerHTML = '';
			renderModules(data.settings.crm_modules);


			// reset the employee table
			employeedata.innerHTML = '<tr class="no-records"><td colspan="3"><i>No records to display</i></td></tr>';

			// reset forms and fill relevant form data
			form.reset();
			formSalesforce.reset();
			formProsperWorks.reset();
			formSugarCRM.reset();
			fillForm();

			zohoFilled.updateFields();
			sfFilled.updateFields();
			ppWFilled.updateFields();
			sgFilled.updateFields();

			if (typeof data.accounts.error != 'undefined' || typeof data.accounts == 'string' || data.accounts.length == 0) {
				formContainer.show(toggleSystem());
				eventLogger('end', 'CRMSettings');
			}
			else {
				context.q('.crm-modules')[0].style.display = 'block';
				showSaveCancelButtons(context);
				populateData(data, function () {
					formContainer.hide();
					eventLogger('end', 'CRMSettings');
				});
			}

		}

	});


	//	---------------------------------------------------------------------------------------------------------
	//	Disconnect
	//	---------------------------------------------------------------------------------------------------------
	form.q('input.button.red')[0].onclick = function () {
		new ajax(base_url + 'disconnect-products/?product=zoho', {
			callback: function (data){
				formContainer.hide();
				showCRMSettings(context, params);
				showAlert('success', 'Disconnected successfully.');
			}
		});
	}
	form.q('input.button.cancel.gray')[0].onclick = function () {
		formContainer.hide();
		return false;
	}

	//	---------------------------------------------------------------------------------------------------------
	//	Disconnect
	//	---------------------------------------------------------------------------------------------------------
	formSalesforce.q('input.button.red')[0].onclick = function () {
		new ajax(base_url + 'disconnect-products/?product=salesforce', {
			callback: function (data){
				formContainer.hide();
				showCRMSettings(context, params);
				showAlert('success', 'Disconnected successfully.');
			}
		});
	}
	formSalesforce.q('input.button.cancel.gray')[0].onclick = function () {
		formContainer.hide();
		return false;
	}

	//	---------------------------------------------------------------------------------------------------------
	//	Disconnect
	//	---------------------------------------------------------------------------------------------------------
	formProsperWorks.q('input.button.red')[0].onclick = function () {
		new ajax(base_url + 'disconnect-products/?product=prosperworks', {
			callback: function (data){
				formContainer.hide();
				showCRMSettings(context, params);
				showAlert('success', 'Disconnected successfully.');
			}
		});
	}
	formProsperWorks.q('input.button.cancel.gray')[0].onclick = function () {
		formContainer.hide();
		return false;
	}

	//	---------------------------------------------------------------------------------------------------------
	//	Disconnect
	//	---------------------------------------------------------------------------------------------------------
	formSugarCRM.q('input.button.red')[0].onclick = function () {
		new ajax(base_url + 'disconnect-products/?product=sugar', {
			callback: function (data){
				formContainer.hide();
				showCRMSettings(context, params);
				showAlert('success', 'Disconnected successfully.');
			}
		});
	}
	formSugarCRM.q('input.button.cancel.gray')[0].onclick = function () {
		formContainer.hide();
		return false;
	}

	context.q('input[type="button"]')[0].onclick = function () {
		formContainer.show(toggleSystem());
		return false;
	}
	//	---------------------------------------------------------------------------------------------------------
}

function showMobileSettings(context, params) {
	dateFilters.hide();
	eventLogger('start', 'MobileSettings');

	eventLogger('end', 'MobileSettings');
}

function toTitleCase(str){
	return str.replace(/(^\w{1}|\.\s*\w{1})/gi, function (txt) {
		return txt.toUpperCase();
	});
}
