
var startD, endD;

var processedData = [], dateBins = [];

//var strataCount = [0, 0, 0], dailyStrataCount = [{}, {}, {}], dailyStrataDetail = [{}, {}, {}], dailyAverages = {}, totalUsers = 0;// = new Date()
var noRecordsRow = [];
var mgrIndex = {};
var templateSchema = {};
var organizationSettings, earliestDate, latestDate;

var pageContent, app, activeFrame, scrollTarget, scrollDest = 0;

function setMyProfileLink(myself){
	var userName = q('a.user-name-wrap')[0];
	var myProfile = q('ul.main-navigation.lower li[data-tag="myprofile"] a')[0];
	if (myself.status == 1){
		//href="#employee/{{ user.id }}/{{ user.email }}"
		userName.setAttribute('href', '#employee/'+myself.id+'/'+myself.email);
		userName.setAttribute('title', myself.fullname);
		if (typeof myProfile != 'undefined'){
			myProfile.setAttribute('href', '#employee/'+myself.id+'/'+myself.email);
			myProfile.removeAttribute('title');
		}
	}
	else{
		userName.removeAttribute('href');
		userName.setAttribute('title', 'Your visibility is set to "Hidden", you can change this on the Employee settings page.');
		if (typeof myProfile != 'undefined'){
			myProfile.removeAttribute('href');
			myProfile.setAttribute('title', 'Your visibility is set to "Hidden", you can change this on the Employee settings page.');
		}
	}
}

function initializeProdoScore(){
	if (typeof calendar != 'function' && typeof usernameIcon != 'function'){
		setTimeout(initializeProdoScore, 512);
		return false;
	}
	new calendar(document.forms[0].from_date);
	new calendar(document.forms[0].to_date);
	//
//	progressiveScriptLoaded({});
	//
	new ajax(base_url+'organization-settings/', {
			callback: function(data){
				var dat = eval('('+data.responseText+')');
				dat['workingdays'] = eval('('+dat['workingdays']+')');
				organizationSettings = dat;
				roles = organizationSettings.roles;
				//delete roles[0];
				var userName = document.body.q('.user-name-wrap p.user-name')[0];
				var picture = userName.getAttribute('data-picture');
				var icon = usernameIcon(userName.innerHTML);
				/*if (!userName.parentNode.hasAttribute('title'))
					userName.parentNode.setAttribute('title', userName.innerHTML);*/
				setMyProfileLink(organizationSettings.myself);
				userName.parentNode.insertBefore(elem('div', (picture == '' ? '<p>'+icon[0]+'</p>' : ''),
					{class: 'circle blue', style: 'background-color:#'+icon[1]+'; border: 2px solid #'+icon[1]+'; background-image: url(\''+picture+'\');'}), userName);
				//userName = organizationSettings.myself.email;//userName.parentNode.getAttribute('href').split('#')[1].split('/')[1];
				//
				earliestDate = new Date(dat.since+'  12:00');
				earliestDate.setDate(earliestDate.getDate() - 93);
				latestDate = new Date(dat.upto+'  12:00');
				//latestDate = latestDate.setDate(latestDate.getDate()+1);
				bindFilters();
				//
				//progressiveScriptLoaded({});
				initializeNavigation();
				date_filter_onchange('last-7-days');
				//
				if (organizationSettings.domain != organizationSettings.myself.email.split('@')[1]){
					q('.inner-wrap a')[0].href = '#settings/employees';
					dateFilters.hide();
					//
					var userName = q('a.user-name-wrap')[0];
					userName.removeAttribute('href');
					document.body.q('#notificationTrigger')[0].style.cursor = 'default';
					//
					if (document.location.hash.split('/')[0] != '#settings')
						document.location.hash = '#settings/employees';
					window.onpopstate();
				}
				else{
					//
					// initialize notifications
					notifications = new Notifications();
					notifications.renderNotification();
					//
					if (document.location.hash == '')
						document.location.hash = '#dashboard';
					window.onpopstate();
				}
				//
				var selDomainDrpDwn = q('div.header-left')[1].q('select')[0];
				var addDomainBtn = q('div.header-left')[1].q('a.button.primary')[0];
				if (organizationSettings.myself.level)
					new ajax(base_url+'register/get-domains/', {
						callback: function(data){
							var domains = eval('('+data.responseText+')');
							//
							selDomainDrpDwn.innerHTML = '';
							var hasOnboardingDomains = false;
							for (var i = 0; i < domains.length; i++){
								if (domains[i].status == 7){
									var option = elem('option', domains[i].title, {value: domains[i].title})
									if (dat['domain'] == domains[i].title)
										option.selected = true;
									selDomainDrpDwn.appendChild(option);
								}
								else
									hasOnboardingDomains = true;
							}
							//
							if (hasOnboardingDomains)
								addDomainBtn.style.display = 'block';
							else
								addDomainBtn.style.display = 'none';
							//
							if (domains.length == 1){
								selDomainDrpDwn.style.display = 'none';
								//q('div.header-left')[1].style.display = 'none';
								//selDomainDrpDwn.innerHTML = '<option>Loading</option>';
							}
							//
							selDomainDrpDwn.onchange = function(){
								new ajax(base_url+'register/select-domain/?switch-only=true&domain='+this.value, {
									callback: function(data){
										document.location.href = base_url;
									}
								});
							};
						},
						fallback: function(){
							selDomainDrpDwn.style.display = 'none';
							addDomainBtn.style.display = 'none';
							//q('div.header-left')[1].style.opacity = 0;
							//selDomainDrpDwn.innerHTML = '<option>Loading</option>';
						}
					});
				else{
					selDomainDrpDwn.style.display = 'none';
					addDomainBtn.style.display = 'none';
					//q('div.header-left')[1].style.display = 'none';
					//selDomainDrpDwn.innerHTML = '<option>Loading</option>';
				}
			},
			onprogress: function(progress){
				progressiveSubLoaded = progress;
				displayProgress();
			},
			fallback: function(xmlhttp){
				if (xmlhttp.status == 500)
					alert('There was a server error while loading your organization settings. We will be looking into this.');
				else
					alert('Couldn\'t connect to server. Please check your internet connection and retry.');
			}
		}
	);
	//
	new function(){
		if (typeof initializeFeedbackForm == 'function')
			initializeFeedbackForm();
		else
			setTimeout(arguments.callee, 500);
	}();

	pageContent = q('.page-content')[0];
	app = q('#app')[0];
	window.onscroll = function(){
		scrollTarget = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
		pageContent.style.top = '-'+scrollTarget+'px';
	}
	scrollDest = 0;
	scroll();
}

var scrollingTable, fixAtBottom;
function scroll(){
	activeFrame = q('.page-content .content.active, .page-content [xclass="content"].active');
	if (activeFrame.length > 0){
		activeFrame = activeFrame[activeFrame.length-1];
		app.style.height = (activeFrame.offsetHeight + 160)+'px';
		scrollingTable = activeFrame.q('table.sortable');
		fixAtBottom = activeFrame.q('div.fix-at-bottom');
		if (scrollingTable.length > 0 && fixAtBottom.length > 0)
			if (window.innerHeight - scrollingTable[0].getBoundingClientRect().top > 100)
				fixAtBottom[0].style.marginBottom = '3px';
			else
				fixAtBottom[0].style.marginBottom = '-40px';
	}
	//
	pageContent.style.top = '-'+scrollTarget+'px';
	setTimeout(scroll, 250);
	//
	if (scrollTarget > 100 && messages.innerHTML != ''){
		var remove = messages.q('.message');
		for (var i = 0; i < remove.length; i++)
			remove[i].onclick();
	}
}

// ==============================================================================================
// ==============================================================================================
//													LOAD DATA TO THE UI
// ==============================================================================================

function loadRawData(context, params){
	var rawdata = context.q('table.rawdata tbody')[0];
	rawdata.innerHTML = '';
	doFetchData('dashboard',
		function(data, self, startD, endD){
			context.style.height = '88vh';
			var th = context.q('table.rawdata thead tr');
			th[0].innerHTML = '<th class="vl" width="160">Employee</th>';
			th[1].innerHTML = '<th class="vl" width="160">Date</th>';
			var prodCount, lastSubCol;
			for (var id in data.employees)
				if (data.employees[id].status > 0 && data.employees[id].role > 0 && typeof data.employees[id].scr != 'undefined' && data.employees[id].scr){
					prodCount = 0;
					for (code in products){
						lastSubCol = elem('th', code);
						th[1].appendChild(lastSubCol);
						prodCount += 1;
					}
					th[1].appendChild(elem('th', 'Score', {class: 'vl'}));
					th[0].appendChild(elem('th', '<b>'+data.employees[id].fullname+'</b><br/>'+
												roles[data.employees[id].role]+'<br/>'+
												'<span>OWS: '+data.employees[id].scr.g.toFixed(2)+
												'<br/>RBS: '+data.employees[id].scr.r.toFixed(2)+
												'<br/>LRS: '+data.employees[id].scr.l.toFixed(2)+'</span>',
										{colspan: prodCount+1, class: 'vl'}));
				}
			rawdata.innerHTML = '';
			var newRow;
			for (date in data.days){
				newRow = elem('tr', '<td class="vl" width="160">'+DateFromShort(date).sqlFormatted()+'</td>');
				for (var id in data.employees)
					if (data.employees[id].status > 0 && data.employees[id].role > 0 && typeof data.employees[id].scr != 'undefined' && data.employees[id].scr)
						if (data.employees[id].days[date] == null){
							for (code in products)
								newRow.appendChild(elem('td', '-'));
							//
							newRow.appendChild(elem('td', '-', {class: 'vl'}));
						}
						else{
							for (code in products)
								newRow.appendChild(elem('td', '&nbsp;'+(typeof data.employees[id].days[date].prod[code] != 'undefined' ? data.employees[id].days[date].prod[code] : '')));
							//
							newRow.appendChild(elem('td', '<span>OWS:&nbsp;'+data.employees[id].days[date].scr.g.toFixed(2)+'<br/>RBS:&nbsp;'+data.employees[id].days[date].scr.r.toFixed(2)+'<br/>LRS:&nbsp;'+data.employees[id].days[date].scr.l.toFixed(2)+'</span>', {class: 'vl'}));
						}
				rawdata.appendChild(newRow);
			}
			//	Allow downloading to Excel file
			new contextMenu(context.q('table')[0], {
					'download-csv': {
						icon: 'fa-table',
						title: 'Download CSV file',
						onclick: function(rawdata){
							downloadCSV(rawdata, 'Raw Data from '+document.forms[0].from_date.value+' to '+document.forms[0].to_date.value+'.csv');
						}
					},
					'download-xlsx': {
						icon: 'fa-file-excel-o',
						title: 'Download Excel file',
						onclick: function(rawdata){
							downloadExcel(rawdata, 'Raw Data from '+document.forms[0].from_date.value+' to '+document.forms[0].to_date.value+'.xlsx');
						}
					}
				});

			context.onscroll = function(e){
				var rowHeads = q('.rawdata tr td:first-child, .rawdata tr th:first-child');//
				for (var i = 0; i < rowHeads.length; i++)
					rowHeads[i].style.left = this.scrollLeft+'px';
			}
		});
}

function loadManagersPage(context, params){
	var employees = context.q('ul.employee-list');
	employees[0].innerHTML = employees[1].innerHTML = employees[2].innerHTML = '';
	breadCrumbViewModel();
	breadCrumbViewModel.reset();
	breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
	breadCrumbViewModel.addCrumb('#managers', 'Managers', true);
	breadCrumbViewModel.render(context);
	doFetchData('managers',
		function(data, self, startD, endD){
			var managers = [];
			employees[0].innerHTML = employees[1].innerHTML = employees[2].innerHTML = '';
			//	Calculate subordinate scores
			for (var i in data.employees)
				if (data.employees[i].status > 0 && typeof data.employees[i].scr != 'undefined' && data.employees[i].scr
					&& typeof data.employees[data.employees[i].manager] != 'undefined' && (organizationSettings.myself.role == 80 || data.employees[data.employees[i].manager].manager == uid)
				){
					if (typeof data.employees[data.employees[i].manager].subScore == 'undefined')
						data.employees[data.employees[i].manager].subScore = [data.employees[i].scr.l];
					else
						data.employees[data.employees[i].manager].subScore.push(data.employees[i].scr.l);
				}
			//
			for (var i in data.employees){
					if (typeof data.employees[i].subScore != 'undefined' && typeof data.employees[i].scr != 'undefined' && data.employees[i].scr){
						var icon = usernameIcon(data.employees[i].fullname);
						var className = strataStr(data.employees[i].strata);
						var employee = arcReactor({
										li: {content:
											{a: {'data-id': i, 'data-role': data.employees[i].role, href: '#employees/under/'+i+'/'+data.employees[i].email, style: 'border-left:2px solid #'+icon[1],
												content: [{
														div: {class: 'user-name-wrap',
															content: {p: {class: 'user-name', content: data.employees[i].fullname}}
														}
													},
													{
														div: {class: 'right-holder',
															content: [
																{p: {class: 'score '+className[0], content: data.employees[i].scr.l.toFixed(0)}},//, title: 'OWS: '+data.employees[i].scr.g.toFixed(2)+'\nRBS: '+data.employees[i].scr.r.toFixed(2)+'\nLRS: '+data.employees[i].scr.l.toFixed(2)
																{p: {}}
															]
														}
													}
												]
											}}
										}});
						employees[2-data.employees[i].strata].appendChild(employee);
						managers[i] = [data.employees[i].fullname, icon[1]];
					}
			}
			//
			for (var i in data.employees){
				var user = context.q('[data-id="'+i+'"]')[0];
				if (user != undefined){
					user = user.q('p')[2];
					user.className = 'score ';
					if (data.employees[i].subScore.length == 0)
						user.innerHTML = '-';
					else{
						var avg = data.employees[i].subScore.avg().toFixed(0);
						user.innerHTML = avg;
						user.className += strataClassname4Score(avg);
					}
				}
			}
			//
			//	-----------------------------------------------------------------------------------------------
			fillMissingEmployeeData(managers, data, startD, endD,
				function(managers, data){
					var chartData = [['Date']], chartIndex = [];
					var colors = [];
					for (var id in managers)
						if (arrayIgnore.indexOf(id) == -1){
							chartData[0].push(managers[id][0]);
							colors.push('#'+managers[id][1]);
							chartIndex[id] = chartData[0].length-1;
						}
					for (var i  = startD; i < endD+1; i++){
						var date = DateFromShort(i);
						if (isWorkingDay(date.getDay())){
							//
							var row = [date];
							for (var id in managers)
								if (arrayIgnore.indexOf(id) == -1)
									row.push(typeof data.employees[id].days[i] == 'undefined' ? 0 : data.employees[id].days[i].scr.l.toFixed(0));
							chartData.push(row);
						}
						else
							chartData.push([date]);
					}
					//setTimeout(function(){
						drawChartManagers(context.q('#managerChart')[0], chartData, colors,
							function(row, col, options){
								var col = options.additionalData.chartIndex.indexOf(col);
								document.location.hash = 'employees/under/'+col+'/'+processedData.employees[col].email;
							},
							{chartIndex: chartIndex}
							);
					//}, 5);
				});
		});
}

function loadEmployeesPage(context, params){

	var employees = context.q('.employeedata')[0].tBodies[0];
	var employeesUnder = [];

	employees.innerHTML = '';
	context.q('.one-row.emp-header')[0].style.display = 'none';
	context.q('#employeesChart')[0].parentNode.parentNode.parentNode.style.display = 'none';

	if (organizationSettings.correlations_enabled === 1) {
		var correlationsWidget = new CorrelationsWidget();
		context.q('.corrWidget-col')[0].style.display = 'none';
		correlationsWidget.init(context.q('div.corrWidget')[0]);
	}


	breadCrumbViewModel();
	breadCrumbViewModel.reset();
	breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
	breadCrumbViewModel.addCrumb('#employees', 'Employees', true);
	breadCrumbViewModel.render(context);

	doFetchData('employees',
		function(data, self, startD, endD){
			var employeesShortlist = [], mgrIndex = {}, employeesCumilScore = 0;
			employees.innerHTML = '';
			filterByManager[0].innerHTML = '<option value="*">[ Filter by ]</option>';
			searchRole[0].value = '*';
			for (var i in data.employees){
				if (data.employees[i].role > 0 && data.employees[i].status == 1 && typeof data.employees[i].scr != 'undefined' && data.employees[i].scr &&
						(typeof params[1] == 'undefined' || ( params[0] == 'under' && data.employees[i].manager == params[1] || params[0] == 'role' && data.employees[i].role == params[1] ) )){
					var icon = usernameIcon(data.employees[i].fullname);
					var className = strataStr(data.employees[i].strata);
					var mgrName = data.employees[data.employees[i].manager] == undefined ? 
						(organizationSettings.myself.id == data.employees[i].manager ?
							organizationSettings.myself.fullname :
							'-')
						:
						data.employees[data.employees[i].manager].fullname;
					//
					//if ( (self.role == 80) || (( data.employees[i].manager == uid || (typeof data.employees[ data.employees[i].manager ] != 'undefined' && data.employees[ data.employees[i].manager ].manager == uid) )) ){
						var employee = arcReactor(
										{tr: {class: 'tablerow', 'data-id': i, 'data-role': data.employees[i].role,
											content: [
												{td: {content: {
													div: {class: 'user-name-wrap',
														content: {p: {class: 'user-name', content: data.employees[i].fullname}}
													}}, style: 'border-left:2px solid #'+icon[1]
												}},
												{td: {content: roles[data.employees[i].role]}},
												{td: {content: mgrName}},
												{td: {class: 'score '+className[0], content: data.employees[i].scr.l.toFixed(0)}},//, title: 'OWS: '+data.employees[i].scr.g.toFixed(2)+'\nRBS: '+data.employees[i].scr.r.toFixed(2)+'\nLRS: '+data.employees[i].scr.l.toFixed(2)
												{td: {class: 'text '+(!data.employees[i].scr.delta || data.employees[i].scr.delta == 0 ? 'same' : (data.employees[i].scr.delta > 0 ? 'increase' : 'decrease')),
													content: !data.employees[i].scr.delta || data.employees[i].scr.delta == 0 ? '-' : Math.abs(data.employees[i].scr.delta.toFixed(0))+'%', 'sorttable_customkey': data.employees[i].scr.delta}}
											],
											onclick: function(){
												document.location.hash = 'employee/'+this.getAttribute('data-id')+'/'+data.employees[this.getAttribute('data-id')].email;
												return false;
											}
										}});
						employeesUnder.push(i);
						employees.appendChild(employee);
						employeesShortlist[i] = [data.employees[i].fullname, icon[1]];
						employeesCumilScore += data.employees[i].scr.l;
					//}
				}
				//if ((self.role == 80 || data.employees[i].manager == uid || (typeof data.employees[data.employees[i].manager] != 'undefined' && data.employees[data.employees[i].manager].manager == uid)) &&
					//typeof data.employees[[data.employees[i].manager]] != 'undefined' && typeof mgrIndex[data.employees[i].manager] == 'undefined')
						if (data.employees[i].role > 0 && data.employees[i].status == 1 &&
						    ((typeof data.employees[data.employees[i].manager] !== 'undefined' && data.employees[data.employees[i].manager].role > 0 && data.employees[data.employees[i].manager].status == 1)
						    || (typeof data.self !== 'undefined' && data.employees[i].manager==data.self.id))){
							mgrIndex[data.employees[i].manager] = true;
						}
			}
			sortColumn(employees.parentNode, 0);
			noRecordsRow[0] = elem('tr', '<td colspan="5"><i>No records found</i></td>', {class: 'no-records', style: 'display:none;'});
			employees.appendChild(noRecordsRow[0]);
			//
			for (var i in mgrIndex)
				if (arrayIgnore.indexOf(i) == -1 && (typeof data.employees[i]!== 'undefined' || parseInt(i)===data.self.id)){
					filterByManager[0].appendChild(elem('option', typeof data.employees[i]!== 'undefined' ? data.employees[i].fullname : data.self.fullname, {value: i}));
				}
			sortList(filterByManager[0], 0);
			//
			filterByManager[0].value = '*';
			searchRole[0].value = '*';
			if (params.length > 1){
				if (params[0] == 'under')
					filterByManager[0].value = params[1];
				else if (params[0] == 'role')
					searchRole[0].value = params[1];
			}
			//
			//	Allow downloading to Excel file
			new contextMenu(employees.parentNode, {
					'download-csv': {
						icon: 'fa-table',
						title: 'Download CSV file',
						onclick: function(rawdata){
							var bkp = rawdata.q('tr.no-records');
							if (bkp.length > 0){
								bkp = [bkp[0].parentNode, bkp[0]];
								bkp[0].removeChild(bkp[1]);
							}
							else
								bkp = false;
							//
							downloadCSV(rawdata, 'Employees-prodoscore-from-'+document.forms[0].from_date.value+'-to-'+document.forms[0].to_date.value+'.csv');
							//
							if (bkp)
								bkp[0].appendChild(bkp[1]);
						}
					},
					'download-xlsx': {
						icon: 'fa-file-excel-o',
						title: 'Download Excel file',
						onclick: function(employees){
							var bkp = employees.q('tr.no-records');
							if (bkp.length > 0){
								bkp = [bkp[0].parentNode, bkp[0]];
								bkp[0].removeChild(bkp[1]);
							}
							else
								bkp = false;
							//
							downloadExcel(employees, 'Employees-prodoscore-from-'+document.forms[0].from_date.value+'-to-'+document.forms[0].to_date.value+'.xlsx');
							//
							if (bkp)
								bkp[0].appendChild(bkp[1]);
						}
					}
				});
			//	-----------------------------------------------------------------------------------------------
			//*/
			contentEmployees.q('button.user-settings-btn')[0].style.display = 'none';
			if (organizationSettings.correlations_enabled === 1) {
				employees.parentNode.classList.remove('employeedata--no-roles');
				employees.parentNode.parentNode.classList.add('col-12');
				employees.parentNode.parentNode.classList.remove('col-8');
			}

			//*/
			if (typeof params[1] != 'undefined'){
				if (params[0] == 'under'){
					//	Show employees under a selected manager
					context.q('.one-row.emp-header')[0].style.display = 'flex';
					fillEmployeeDetails(contentEmployees, data, params[1], (employeesCumilScore / Object.keys(employeesShortlist).length));
					//
					contentEmployees.q('button.user-settings-btn')[0].onclick = function(){
						document.location.hash = 'settings/employees/o/'+params[1];
					}
					contentEmployees.q('button.user-profile-btn')[0].onclick = function(){
						document.location.hash = 'employee/'+params[1];
					}
					let emp_name = (typeof data.employees[params[1]] !== 'undefined' ? data.employees[params[1]].fullname : data.self.fullname);
					context.q('#employeesChart')[0].parentNode.q('h2')[0].innerHTML = 'Employees under '+ emp_name;

					breadCrumbViewModel();
					breadCrumbViewModel.reset();
					breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
					breadCrumbViewModel.addCrumb('#managers', 'Managers', false);
					breadCrumbViewModel.addCrumb(location.hash, emp_name, true);
					breadCrumbViewModel.render(context);

				}
				else {
					//	Compare employees in the selected team
					context.q('#employeesChart')[0].parentNode.q('h2')[0].innerHTML = 'Employees in '+roles[params[1]]+' Team';
					breadCrumbViewModel();
					breadCrumbViewModel.reset();
					breadCrumbViewModel.addCrumb('#dashboard', 'Dashboard', false);
					breadCrumbViewModel.addCrumb('#employees', 'Employees', false);
					breadCrumbViewModel.addCrumb(location.hash, roles[params[1]]+' Team', true);
					breadCrumbViewModel.render(context);

					if (organizationSettings.correlations_enabled === 1) {
						employees.parentNode.classList.add('employeedata--no-roles');
						employees.parentNode.parentNode.classList.remove('col-12');
						employees.parentNode.parentNode.classList.add('col-8');

						context.q('.corrWidget-col')[0].style.display = 'block';
						correlationsWidget.loadData(document.forms[0].from_date.value, document.forms[0].to_date.value, employeesUnder);
					}

				}
				//	Draw employee comparison chart after fetching required data
				fillMissingEmployeeData(employeesShortlist, data, startD, endD,
					function(employeesShortlist, data){
						compareEmployeesChart(employeesShortlist, data);
					});
				//
				context.q('#employeesChart')[0].parentNode.parentNode.parentNode.style.display = 'block';
			}
			//	Only one manager is present -? SHOULD WE KEEP THIS BLOCK. SEE IF THIS APPLIES ACTUALLY..
			else if (Object.keys(mgrIndex).length == 1 && params[1] != self.manager){
				//	Draw employee comparison chart after fetching required data
				fillMissingEmployeeData(employeesShortlist, data, startD, endD,
					function(employeesShortlist, data){
						compareEmployeesChart(employeesShortlist, data);
					});
				//
				context.q('#employeesChart')[0].parentNode.parentNode.parentNode.style.display = 'block';
				filterByManager[0].value = Object.keys(mgrIndex)[0];
			}
			//	If you came to add comparison between teams, we shouldn't.
		});

}

// ==============================================================================================
//													END OF JAVASCRIPT FILE
// ==============================================================================================
