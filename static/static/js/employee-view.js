
var organizationSettings, earliestDate, latestDate;
var roles = {
	/*0: '', 1: 'Contractors', 2: 'Sales team', 3: 'Sales engineers', 4: 'Operations', 5: 'Human resources', 6: 'Tech support',
	7: '[Managers]', 8: '[Administrators]',
	10: 'Human resource', 11: 'Finance &amp; Accounting', 12: 'Legal', 13: 'Marketers', 14: 'IT', 15: 'Support',
	70: 'Managers', 80: 'Administrators'*/
};
var navigationActiveStack = [[{id: 'employeeView', removeClass: function(classname){}, style: {display: 'block'}}]];

function initializeProdoScore(){
	new calendar(document.forms[0].from_date);
	new calendar(document.forms[0].to_date);
	//document.forms[0].submitBtn.focus();
	//
	var userName = document.body.q('.user-name-wrap p.user-name')[0];
	var picture = userName.getAttribute('data-picture');
	var icon = usernameIcon(userName.innerHTML);
	userName.parentNode.insertBefore(elem('div', (picture == '' ? '<p>'+icon[0]+'</p>' : ''),
		{class: 'circle blue', style: 'background-color:#'+icon[1]+'; border: 2px solid #'+icon[1]+'; background-image: url(\''+picture+'\');'}), userName);
	userName = userName.parentNode.getAttribute('data-href').split('#')[1].split('/')[1];
	//
	new ajax(base_url+'organization-settings/', {
			callback: function(data){
				dat = eval('('+data.responseText+')');
				dat['workingdays'] = eval('('+dat['workingdays']+')');
				organizationSettings = dat;
				roles = organizationSettings.roles;
				//delete roles[0];
				roles[0] = '* Please Select';
				//
				earliestDate = new Date(dat.since+'  12:00');
				earliestDate.setDate(earliestDate.getDate() - 93);
				latestDate = new Date(dat.upto+'  12:00');
				//
				date_filter_onchange('last-7-days');
				loadView();
				//
				if (document.location.hash == '')
					document.location.hash = '#dashboard';
				window.onpopstate();
			},
			fallback: function(xmlhttp){
				if (xmlhttp.status == 500)
					alert('There was a server error while loading your organization settings. We will be looking into this.');
				else
					alert('Couldn\'t connect to server. Please check your internet connection and retry.');
			}
		});
	//initializeFeedbackForm();
}
document.body.onload = function(){
	initializeProdoScore();
}

var mgrName = q('#mgr-name')[0];
//
var empScore = q('#emp-score')[0];
var empDelta = q('#emp-delta')[0];
var empDeltaDir = q('#emp-delta-dir')[0];
//
var teamScore = q('#team-score')[0];
var teamDelta = q('#team-delta')[0];
var teamDeltaDir = q('#team-delta-dir')[0];
//
var orgScore = q('#org-score')[0];
var orgDelta = q('#org-delta')[0];
var orgDeltaDir = q('#org-delta-dir')[0];
//
var employeeList = q('.employee-list')[0];
var employeeListItemTemplate = arcRead(employeeList.removeChild(employeeList.q('li')[0]));
//
var employeeDayStats = q('#employee-day-stats')[0];
var recommendation = q('#recommendation')[0];
var recDetail = q('#rec-detail')[0];
//
function randomName(self, real){
	if (self == real)
		return (real.fullname != '' ? real.fullname : 'You');//real.fullname + ' ()'
	else
		return '';//'Colleague ' + real.fullname[0];
	/*{
		var x = real.fullname.split(' ');
		return x.length == 1 ? x[0][0] +' - '+ x[0][1] : x[0][0] +' - '+ x[1][0];
	}*/
}
//
var chartDrawTimeout = false;
var recommendationTimeout = false;
function loadView(){
	clearTimeout(chartDrawTimeout);
	clearTimeout(recommendationTimeout);
	/*recommendation.innerHTML = 'Recommendation line';
	recDetail.innerHTML = 'Recommendation details';*/
	doFetchData('dashboard',
		function(data, self, startD, endD){
			if (typeof data.employees[self.id] != 'undefined')
				self = data.employees[self.id];
			mgrName.innerHTML = self.manager == 52 || typeof data.employees[self.manager] == 'undefined' ? '' : 	data.employees[self.manager].fullname;
			employeeList.innerHTML = '';
			// ----------------------------------------------------------------------------------------------------
			//	Find team score and delta
			var teamScr = [0, 0, 0], team = {}, employeesShortlist = [], icon;
			var graphData = [['Date']], graphColors = [];
			for (var i in data.employees){
				if (data.employees[i].role == self.role && typeof data.employees[i].scr != 'undefined' && data.employees[i].scr){
					teamScr[0] += data.employees[i].scr.l;
					//if (typeof data.employees[i].scr.delta != 'undefined')
					teamScr[1] += data.employees[i].scr.delta;
					teamScr[2] += 1;
					team[data.employees[i].scr.l.toFixed(0)+'-'+i] = data.employees[i];
				}
			}
			// ----------------------------------------------------------------------------------------------------
			//	Populate leaderboard
			q('#leaderboard-heading')[0].innerHTML = roles[self.role]+' leaderboard';
			q('#chart_div_org-heading')[0].innerHTML = roles[self.role]+' Prodoscore chart';
			var sorted = Object.keys(team).sort(function(a, b){return b.split('-')[0] - a.split('-')[0]}),	//	function(a, b){return b-a}
				sStr, id, prev = Infinity, num = 0, tmp, color, myIndex;	//	It would be logically appropriate to shot #0 when everybody had 0 score..
			var isTop = false;
			for (var i = 0; i < sorted.length; i++){
				icon = usernameIcon(team[ sorted[i] ].fullname);
				if (prev > team[ sorted[i] ].scr.l)
					num = num+1;
				//id = sorted[i].split('-')[1];
				sStr = strataStr(team[ sorted[i] ].strata);
				if (team[ sorted[i] ].email == self.email || i < 5){
					employeesShortlist[ sorted[i].split('-')[1] ] = [team[ sorted[i] ].fullname];
					//
					icon = usernameIcon(team[ sorted[i] ].fullname);
					graphData[0][ 5 - i ] = '#'+num+' '+randomName(self, team[ sorted[i] ]);
					if (self.email == team[ sorted[i] ].email){
						graphColors[ 4 - i ] = 'rgba('+icon[2]+',1)';
						myIndex = i;
					}
					else
						graphColors[ 4 - i ] = 'hsl('+icon[3]+', 25%, 75%)';
					//
					color = 'hsl('+icon[3]+', 28%, 80%)';
					if (self.email == team[ sorted[i] ].email)
					//	If it it self - show bright color
						color = '#'+icon[1];
					//
					if (num == 1 && self.email == team[ sorted[i] ].email)
						isTop = true;
					tmp = employeeList.appendChild(arcReact({
							id: sorted[i].split('-')[1],
							class: self.email == team[ sorted[i] ].email ? 'self' : '',
							number: '#'+num, role: team[ sorted[i] ].role,
							email: team[ sorted[i] ].email,
							fullName: (num < 5 && self.email == team[ sorted[i] ].email ? '<strong>Congratulations ' + randomName(self, team[sorted[i]]) + '!</strong>' : randomName(self, team[sorted[i]])),
							score: team[ sorted[i] ].scr.l.toFixed(0),
							style: 'border-left:2px solid '+color,
							style2: 'background-color:'+color,
							scoreDesc: sStr[1], scoreClass1: 'score '+sStr[0], scoreClass2: 'text '+sStr[0]
						}, employeeListItemTemplate));
					if (self.email == team[ sorted[i] ].email){
						var picture = document.body.q('.user-name-wrap p.user-name')[0].getAttribute('data-picture');
						if (picture != ''){
							tmp = tmp.q('i.fa')[0];
							tmp.innerHTML = '<img src="'+picture+'" />';
							tmp.className = 'self-icon';
						}
					}
				}
				prev = team[ sorted[i] ].scr.l;
			}
			// ----------------------------------------------------------------------------------------------------
			//	Draw team graph
			/*sorted = sorted.reverse();
			num = Math.min(sorted.length+1, 5), prev = Infinity;
			for (var i = Math.max(sorted.length - num - 1, 0); i < sorted.length; i++)
				if (i < 5 || team[ sorted[i] ].email == self.email){
					if (prev > team[ sorted[i] ].scr.l)
						num = num-1;
					icon = usernameIcon(team[ sorted[i] ].fullname);
					graphData[0].push('#'+num+' '+randomName(self, team[ sorted[i] ]));
					if (self.email == team[ sorted[i] ].email){
						graphColors.push('rgba('+icon[2]+',1)');
						myIndex = i;
					}
					else
						//graphColors.push('hsl('+icon[3]+', 20%, 85%)');
						graphColors.push('hsl('+icon[3]+', 25%, 75%)');
						//graphColors.push('rgba('+icon[2]+',0.35)');
				}*/
			var tmp, foundAny;//days = Object.keys(data.days),
			//for (var i = 0; i < days.length; i++){
			fillMissingEmployeeData(employeesShortlist, data, startD, endD,
				function(employeesShortlist, data){
					for (var i  = startD; i < endD+1; i++){
						var dateObj = DateFromShort(i);
						tmp = [dateObj];
						foundAny = false;
						for (var j = 0; j < sorted.length; j++)
							if (j < 5 || team[ sorted[j] ].email == self.email)
								if (!isWorkingDay(dateObj.getDay()) || typeof team[ sorted[j] ].days[i] == 'undefined')
									tmp[ 5 - j ] = null;
								else{
									tmp[ 5 - j ] = team[ sorted[j] ].days[i].scr.l.toFixed(0);
									foundAny = true;
								}
						if (!foundAny)
							tmp = [dateObj];
						graphData.push(tmp);
					}
					// ----------------------------------------------------------------------------------------------------
					//console.log(graphData);
					clearTimeout(chartDrawTimeout);
					chartDrawTimeout = setTimeout(
						function(){
							drawChartEmployeeDashboard(q('#chart_div_org')[0], graphData,
								function(row, col){
									if (col != myIndex+1)
										return false;
									//
									employeeDayStats.style.display = 'block';
									var prods = self.days[ DateToShort(row[0]) ].prod;
									var lis = employeeDayStats.q('p.score'), prod;
									employeeDayStats.q('#individual-date')[0].innerHTML = row[0].toString().substring(0, 15);
									for (var i = 0; i < lis.length; i++){
										prod = lis[i].className.split(' ')[1];
										if (typeof prods[prod] == 'undefined')
											lis[i].parentNode.parentNode.parentNode.style.display = 'none';
										else{
											if (['cl', 'tb', 'zc', 'ze'].indexOf(prod) > -1)
												lis[i].innerHTML = mtsToTime(prods[prod]);
											else
												lis[i].innerHTML = prods[prod];
											lis[i].parentNode.parentNode.parentNode.style.display = 'block';
										}
									}
									//for (var prod in prods)
									//	employeeDayStats.q('.right-holder p.score.'+prod)[0].innerHTML = prods[prod];
									scrollToMiddle(document.body.scrollHeight);
								}, graphColors, 8
							);
							q('#chart_div_org_legend circle')[0].style.backgroundColor = graphColors[4];
						}, 5);
					// ----------------------------------------------------------------------------------------------------
					//	Write recommendation line
					clearTimeout(recommendationTimeout);
					recommendationTimeout = setTimeout(
						function(){
							var recommendations = processRecommendations(sorted);
							recDetail.innerHTML = '';
							if (self.strata == 2){
								recommendation.innerHTML = 'Congratulations! You\'re working hard and it\'s paying off!<br/>';
								if (self.scr.delta > 5)
									recDetail.innerHTML = 'Wow! You\'ve stepped up your game. Keep it up!';
								else if (self.scr.delta < -10)
									recDetail.innerHTML = 'You\'re still doing great, but something seems off recently. Everything alright?';
								else
									recDetail.innerHTML = 'You\'re at the top level, and you\'re staying there. Fantastic.';
								//
								if (isTop)
									recDetail.innerHTML += '<br/><br/>Winner, winner! You have the top score.';
							}
							else if (self.strata == 1){
								recommendation.innerHTML = 'You\'re doing a good job, but you could take to the next level.<br/>';
								if (self.scr.delta > 5)
									recDetail.innerHTML = 'You\'re making big improvements. Keep moving up!';
								else if (self.scr.delta < -10)
									recDetail.innerHTML = 'Your performance has been dropping off. Let\'s get it back up!';
								else
									recDetail.innerHTML = 'You\'re doing well. But don\'t forget to push yourself!';
								//
								if (isTop)
									recDetail.innerHTML += '<br/><br/>You have the top score! Keep aiming higher!';
							}
							else if (self.strata == 0){
								recommendation.innerHTML = 'Time to step it up! Let\'s see where we can improve.<br/>';
								if (self.scr.delta > 5)
									recDetail.innerHTML = 'You\'re doing better, but still need some work. Keep at it!';
								else if (self.scr.delta < -10)
									recDetail.innerHTML = 'Oh no! Your performance has significantly dropped down. Let\'s turn back around.';
								else
									recDetail.innerHTML = 'You seem like you\'re stuck in a rut. Time to make some big changes.';
								//
								if (isTop)
									recDetail.innerHTML += '<br/><br/>You have the top score, but that\'s not saying much this week.';
							}
							//
							var prods = Object.keys(recommendations);
							if (!isTop && prods.length > 0){
								recDetail.innerHTML += '<br/><br/>Aiming for one of the top spots? The current top scorers are using ';
								var usages = [];
								for (var i = 0; i < prods.length; i++)
									usages.push('<b>' + (parseInt(recommendations[ prods[i] ] * 20)/20) + '</b> times more ' + products[ prods[i] ][1].name);
									//usages.push('<b>' + (recommendations[ prods[i] ] > 1.5 ? parseInt(recommendations[ prods[i] ]) : recommendations[ prods[i] ]) + '</b> times more ' + products[ prods[i] ][1].name);
								//
								usages = usages.slice(0, usages.length - 1).join(', ') + (usages.length > 1 ? ' and ' : '') + usages[usages.length - 1];
								recDetail.innerHTML += usages+' than you daily. Good luck!';
							}
						}, 5);
					// ----------------------------------------------------------------------------------------------------
				});
			// ----------------------------------------------------------------------------------------------------
			//	Fill employee scores
			if (typeof self.scr == 'undefined')
				self.scr = {l: 0, delta: 0};
			if (self.scr){
				empScore.innerHTML = self.scr.l.toFixed(0);
				empScore.parentNode.className = 'score main '+strataStr(self.strata)[0];
				empScore.parentNode.parentNode.className = 'card-wrap '+strataStr(self.strata)[0];
				//
				if (typeof self.scr.delta == 'undefined')
					empDelta.innerHTML = '-';
				else
					empDelta.innerHTML = Math.abs(self.scr.delta).toFixed(0);
				empDelta.parentNode.className = 'score main '+(self.scr.delta < 0 ? 'below' : 'above');
				//
				empDeltaDir.innerHTML = self.scr.delta > 0 ? 'increase' : 'decrease';
			}
			else{
				empScore.innerHTML = '-';
				empScore.parentNode.className = 'score main';
				empScore.parentNode.parentNode.className = 'card-wrap';
				//
				empDelta.innerHTML = '-';
				empDelta.parentNode.className = 'score main';
				//
				empDeltaDir.innerHTML = 'same';
			}
			// ----------------------------------------------------------------------------------------------------
			//	Fill team scores
			if (teamScr[2] == 0)
				teamScr = [0, 0, 1];
			var score = teamScr[0] / teamScr[2],
				delta = teamScr[1] / teamScr[2];
			//*if (isNaN(score)) teamScore.innerHTML = '-'; else*/
			teamScore.innerHTML = Math.abs(score).toFixed(0);
			teamScore.parentNode.className = 'score main '+strataClassname4Score(score);
			teamScore.parentNode.parentNode.className = 'card-wrap '+strataClassname4Score(score);
			//
			if (isNaN(delta))
				teamDelta.innerHTML = '-';
			else
				teamDelta.innerHTML = Math.abs(delta).toFixed(0);
			teamDelta.parentNode.className = 'score main '+(delta < 0 ? 'below' : 'above');
			//
			teamDeltaDir.innerHTML = delta > 0 ? 'increase' : 'decrease';
			// ----------------------------------------------------------------------------------------------------
			//	Fill org scores
			orgScore.innerHTML = data.organization.score.toFixed(0);
			orgScore.parentNode.className = 'score main '+strataStr(data.organization.score_strata)[0];
			orgScore.parentNode.parentNode.className = 'card-wrap '+strataStr(data.organization.score_strata)[0];
			//
			if (!data.organization.score_delta)
				orgDelta.innerHTML = '-';
			else
				orgDelta.innerHTML = Math.abs(data.organization.score_delta).toFixed(0);
			orgDelta.parentNode.className = 'score main '+(data.organization.score_delta < 0 ? 'below' : 'above');
			//
			orgDeltaDir.innerHTML = data.organization.score_delta > 0 ? 'increase' : 'decrease';
			//orgDeltaDir.parentNode.className = 'score main '+(data.organization.score_delta > 0 ? 'above' : 'below');
		});
}

function processRecommendations(sorted){
	//employeesShortlist
	var eid, days, prods;
	var wProds = {}, mProds = {}, compProds = {};
	//
	//	index data to a data structure
	var fillData = function(empdays, sProds){//, comp
		//
		//	For their working days
		days = Object.keys(empdays);
		for (var did = 0; did < days.length; did++){
			//
			//	For each product usage on each day
			prods = Object.keys(empdays[ days[did] ].prod);
			for (var pid = 0; pid < prods.length; pid++){
				if (typeof sProds[ prods[pid] ] == 'undefined')
					sProds[ prods[pid] ] = {'c': 0, 's': 0};
				//
				sProds[ prods[pid] ].c += 1;
				sProds[ prods[pid] ].s += empdays[ days[did] ].prod[ prods[pid] ];
			}
		}
		//
		prods = Object.keys(sProds);
		for (var pid = 0; pid < prods.length; pid++)
			sProds[ prods[pid] ] = sProds[ prods[pid] ].s / sProds[ prods[pid] ].c;
	};
	//
	fillData(processedData.employees[uid].days, mProds);
	prods = Object.keys(mProds);
	/*for (var pid = 0; pid < prods.length; pid++){
		if (mProds[ prods[pid] ] == 0)
			mProds[ prods[pid] ] = 1;
	}*/
	//
	//	Consider peers above in score to this employee
	for (var i = 0; i < sorted.length; i++){
		eid = sorted[i].split('-')[1];
		//
		//	Stop at this employee
		if (eid == uid)
			break;
		//
		wProds[eid] = {};
		fillData(processedData.employees[eid].days, wProds[eid]);//, mProds
		//
		prods = Object.keys(wProds[eid]);
		for (var pid = 0; pid < prods.length; pid++){
			if (typeof mProds[ prods[pid] ] == 'undefined' || wProds[eid][ prods[pid] ] > mProds[ prods[pid] ]){
				if (typeof compProds[ prods[pid] ] == 'undefined' || compProds[ prods[pid] ] < wProds[eid][ prods[pid] ])
					compProds[ prods[pid] ] = wProds[eid][ prods[pid] ];
				//mProds[ prods[pid] ] = wProds[eid][ prods[pid] ];
			}
		}
	}
	/*console.log(mProds);
	console.log(wProds);*/
	//
	prods = Object.keys(compProds);
	for (var pid = 0; pid < prods.length; pid++){
		if (typeof mProds[ prods[pid] ] == 'undefined' || mProds[ prods[pid] ] == 0)
			mProds[ prods[pid] ] = 1;
		else if (compProds[ prods[pid] ] == mProds[ prods[pid] ])
			delete compProds[ prods[pid] ];
		if (typeof compProds[ prods[pid] ] != 'undefined')
			compProds[ prods[pid] ] = compProds[ prods[pid] ] / mProds[ prods[pid] ];
	}
	//
	//	Remove any less than or equal to 1
	prods = Object.keys(compProds);
	for (var pid = 0; pid < prods.length; pid++)
		if (compProds[ prods[pid] ] <= 1)
			delete compProds[ prods[pid] ];
	/*/	Calculate averages
	prods = Object.keys(wProds);
	for (var pid = 0; pid < prods.length; pid++){
		//wProds[ prods[pid] ] = wProds[ prods[pid] ].total / wProds[ prods[pid] ].count;
		if (typeof mProds[ prods[pid] ] == 'undefined' || mProds[ prods[pid] ] == 0)
			mProds[ prods[pid] ] = 1;
		if (wProds[ prods[pid] ] > mProds[ prods[pid] ]){
			var tmp = parseInt( 10 * wProds[ prods[pid] ] / mProds[ prods[pid] ] ) / 10;
			if (tmp > 1)
			compProds[ prods[pid] ] = tmp;
		}
	}*/
	//
	// console.log(wProds); // console.log(mProds);
	return compProds;
}

var dateFilter = document.getElementById('date-filter');
dateFilter.onchange =
	function(){
		date_filter_onchange(this.value);
		loadView();
	};
document.forms[0].from_date.onchange =
	function(){
		dateFilter.value = 'custom';
		loadView();
	};
document.forms[0].to_date.onchange =
	function(){
		dateFilter.value = 'custom';
		loadView();
	};


function displayProgress(){}
function progressiveScriptLoaded(){}