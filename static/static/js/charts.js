
var dateFormat = 'EE\nd/M', fontName = 'Linotte-Regular';
var loadingIndicator = '<div class="loading-graph"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div></div>';//'<div class="loading"></div>';

var verticalRanges = {
	areaBelow: [0, 29, 'rgba(255, 140, 140, 0.1)'],
	below: [29, 30, 'rgba(240, 50, 50, 0.4)'],
	above: [70, 71, 'rgba(50, 140, 240, 0.4)']
}
var redrawCallbackIndex = {objects: [], functions: [], arccharts: []};

function compareEmployeesChart(employeesShortlist, data){
	var chartData = [['Date']], chartIndex = [];
	var colors = [];
	for (var id in employeesShortlist)
		if (arrayIgnore.indexOf(id) == -1){
			chartData[0].push(employeesShortlist[id][0]);
			colors.push('#'+employeesShortlist[id][1]);
			chartIndex[id] = chartData[0].length-1;
		}
	for (var i  = startD; i < endD+1; i++){
		var date = DateFromShort(i);//new Date((16800+(1*i))*86400000+timezoneOffset);
		if (isWorkingDay(date.getDay())){
			date.setHours(0);
			date.setMinutes(0);
			var row = [date];
			for (var id in employeesShortlist)
				if (arrayIgnore.indexOf(id) == -1)
					row.push(typeof data.employees[id].days[i] == 'undefined' ? null : data.employees[id].days[i].scr.l.toFixed(0));
			chartData.push(row);
		}
		else
			chartData.push([date]);
	}
	setTimeout(function(){
		drawChartEmployees4Manager(document.getElementById('employeesChart'), chartData, colors,
			function(row, col, options){
				var col = options.additionalData.chartIndex.indexOf(col);
				document.location.hash = 'employee/'+col+'/'+processedData.employees[col].email;
			},
			{chartIndex: chartIndex}
			/*function(chart){
				bindChartHover(chart, chartIndex, contentEmployees);
			}*/);
	}, 250);
}

// ==============================================================================================
// ==============================================================================================
//													Arc.JS Charts
// ==============================================================================================

function ArcChart(context, data, colors, options, callback){
	var _self = this;
	this.data = data;
	//	Default options to use when not provided by programmer
	var defaultOptions = {
		chartArea:{left:40, right:10, bottom:60, top:5},
		backgroundColor: '#FFFFFF',
		//grayDays: 'rgba(160, 160, 160, 0.2)',
		grayDays: 'rgba(164, 180, 193, 0.25)',
		fontColor: '#303030',
		tooltipPosition: 'above',
		points: false,
		curve: 0,
		fillColors: ['none'],
	};
	for (var key in defaultOptions)
		if (typeof options[key] == 'object'){
			for (var skey in defaultOptions[key])
				if (typeof options[key][skey] == 'undefined')
					options[key][skey] = defaultOptions[key][skey];
		}
		else if (typeof options[key] == 'undefined')
			options[key] = defaultOptions[key];
	if (typeof colors == 'undefined'){
		colors = ['skyblue', 'yellow', 'lightgreen', 'red', 'purple', 'blue', 'orange', 'green'];
	}
	//
	//	Create a division to display tooltip
	var tooltip = elem('div', null, {class: 'chartTooltip '+options.tooltipPosition, style: 'opacity:0;'});
	//
	//	Calculate minimum and maximum values so we can contain the chart withing these bounds - scaling
	var colMax = 0, rowMin = Math.min(), rowMax = 0;
	for (var i = 1; i < _self.data.length; i++){
		if (getVal(_self.data[i][0]) > rowMax)
			rowMax = getVal(_self.data[i][0]);
		if (getVal(_self.data[i][0]) < rowMin)
			rowMin = getVal(_self.data[i][0]);
		for (var j = 1; j < _self.data[i].length; j++)
			if (_self.data[i][j]-colMax > 0)
				colMax = _self.data[i][j];
	}
	//	Default step in X axis - Microseconds per day
	var xGap = 24*3600000;
	if (_self.data.length > 1 && !(_self.data[1][0] instanceof Date) && _self.data[1][0].indexOf(':') > -1)
		xGap = 60;
	if (typeof options.setSpacing != 'undefined' && options.setSpacing == 'histogram'){
		xGap = 1;
		rowMin = 0;
		rowMax = _self.data.length - 1;
	}
	var graph = elemSVG('svg');
	this.chartType = false;
	this.chartAreaBottomOriginal = options.chartArea.bottom;
	//
	this.draw = function(chartType, isRedraw){
		_self.chartType = chartType;
		options.chartArea.bottom = _self.chartAreaBottomOriginal;
		//	Wait till chart draw area is render-ready
		if (context.offsetWidth == 0 || context.offsetHeight == 0){
			setTimeout(function(){_self.draw(chartType);}, 250);
			return;
		}
		if (colMax == 0){
			context.innerHTML = '<i class="no-chart">No data to display on the chart.</i>';
			//return {error: 'No data to display on the chart.'};
		}
		else{
			//	Create chart SVG element
			graph = elemSVG('svg', null, {
					width: context.offsetWidth,
					height: context.offsetHeight,
					xmlns: 'http://www.w3.org/2000/svg',
					'font-family': options.fontFamily,
					//'shape-rendering': (chartType=='line'?'':'crispEdges'),
					style: 'background-color:'+options.backgroundColor+';'});
			var chartGrid = elemSVG('g', null, {id: 'grid'});
			var chartArea = elemSVG('g', null, {id: 'chart', transform: 'translate('+options.chartArea.left+', '+options.chartArea.top+')'});
			var chartBG = elemSVG('g', null, {id: 'background', transform: 'translate('+options.chartArea.left+', '+options.chartArea.top+')'});
			//
			//	Calculate chart bounding rectangle
			var areaHeight = context.offsetHeight - options.chartArea.top - options.chartArea.bottom;
			var areaWidth = context.offsetWidth - options.chartArea.left - options.chartArea.right;
			var setWidth = (xGap * areaWidth) / (xGap + rowMax - rowMin);
			var colWidth = (setWidth / (_self.data[0].length))-2, colOffset = 0;
			if (_self.data[0].length == 2)
				colWidth = (setWidth / 1.45)-1;
			if (colWidth > 50){
				//colOffset = (setWidth - (colWidth - 51) * (_self.data[0].length == 2 ? 1.75 : _self.data[0].length)) / 2;
				colOffset = (setWidth - 50 * _self.data[0].length) / 2;
				colWidth = 48;
			}
			else if (colWidth < 3){
				colWidth = 3;
				setWidth = 3 * _self.data[0].length * 1.5;
				//colOffset = 3 * 2;
				areaWidth = setWidth * (xGap + rowMax - rowMin) / xGap;
				graph.setAttribute('width', areaWidth + options.chartArea.left + options.chartArea.right);
				graph.setAttribute('height', context.offsetHeight-9);
				areaHeight -= 9;
				options.chartArea.bottom = _self.chartAreaBottomOriginal + 9;
			}
			//if (chartType != 'histogram')
			colOffset += colWidth / 2;
			//
			//	Draw horizontal scale lines in the background
			drawHorizontalLine(chartGrid, areaWidth + options.chartArea.left, options, topForVal(context, options, areaHeight, colMax, 0), '', '#808080');
			//
			if (colMax < 5)
				var segments = chartType == 'histogram' ? Math.ceil(colMax / 4) * 4 : Math.ceil(colMax / 2) * 2;
			else if (colMax < 25)
				var segments = Math.ceil(colMax / 20) * 20;
			else
				var segments = Math.ceil(colMax / 50) * 50;
			for (var i = 3; i > 0; i--){
				drawHorizontalLine(chartGrid, areaWidth + options.chartArea.left, options, topForVal(context, options, areaHeight, colMax, i*(segments/4)), i*(segments/4), '#D0D0D0');
			}
			//
			//	Draw vertical ranges if specified
			if (typeof options.verticalRanges != 'undefined')
				for (range in options.verticalRanges)
					chartBG.appendChild(elemSVG('rect', null, {
										id: 'range-'+range,
										x: -5,
										y: 10 + (areaHeight * (1 - (options.verticalRanges[range][1] / colMax))),
										width: areaWidth+5,
										height: areaHeight * (options.verticalRanges[range][1]-options.verticalRanges[range][0]) / colMax,
										style: 'fill:'+options.verticalRanges[range][2]+';'
									}));
			//
			//	Determine X Axis label rotate angle if X axis is dense
			var barHeight;
			if (setWidth < 50)
				options.rotateXAxisLabels = 45;
			else
				options.rotateXAxisLabels = undefined;
			//
			//	Prepare lines if line chart
			if (chartType == 'line'){
				var lines = [], line, d, circle, circleHover;
				for (var i = 1; i < _self.data[0].length; i++){
					line = elemSVG('path', null, {'stroke-width': 2, stroke: colors[i-1], d: '', fill: options.fillColors[i-1] || 'none'});
					chartArea.appendChild(line);
					lines.push(line);
				}
			}
			//
			var lastLblX = -100, curve = '';
			//	Draw the chart
			for (var i = 1; i < _self.data.length; i++){
				if (_self.data[i].length == 1){
					//	A Gray Day - Date with no data (weekends)
					var rect = elemSVG('rect', null, {
									id: 'date-gray-'+getVal(_self.data[i][0]),
									x: (setWidth * (getVal(_self.data[i][0]) - rowMin) / xGap) + 1 - (_self.data[0].length == 2 ? colWidth/4 : colWidth/2),
									y: 0-options.chartArea.top,
									width: setWidth-2,
									height: areaHeight+(2*options.chartArea.top),
									style: 'fill:'+options.grayDays+';'
								});
					chartBG.appendChild(rect);
					//chartArea.appendChild(rect);
				}
				else{
					var group = elemSVG('g', null, {id: 'date-'+getVal(_self.data[i][0]), class: 'colgroup',
										//	'clip-path': 'url(#clip)', width: setWidth,
											transform: 'translate('+(setWidth * (getVal(_self.data[i][0], i-0.5) - rowMin) / xGap)/*+colOffset*/+', 0)'});
					var offset = 0;
					// Draw Column Chart
					if (chartType == 'column'){
						var rect = elemSVG('rect', null, {
							x: (colOffset / 4),
							width: setWidth - (colWidth/1.5),
							y: areaHeight + options.chartArea.top - 1,
							height: 3,
							class: 'col-group-underline',
							style: 'fill:rgb(25, 25, 25);'
						});
						group.appendChild(rect);
						//
						//	Draw columns
						for (var j = 1; j < _self.data[i].length; j++){
							if (typeof _self.data[i][j] != 'undefined' && _self.data[i][j] != null){
								barHeight = _self.data[i][j] * areaHeight / colMax;
								if (typeof options.prevOffset != 'undefined' && typeof options.prevOffset[j-1] != 'undefined')
									offset = options.prevOffset[j-1];
								else
									offset = 0;
								var rect = elemSVG('rect', null, {
												id: 'value-'+_self.data[0][j].toLowerCase().replace(/ /g, '-')+'-'+getVal(_self.data[i][0]),
												x: _self.data[i].length == 2 ?  (setWidth / 2) - (colWidth / 2) - 8  :  ((colWidth+1)*(j-1)) + colOffset,
												y: areaHeight - barHeight + options.chartArea.top,
												width: colWidth,
												height: barHeight,
												style: 'fill:'+colors[j-1]+';'
											});
								new chartBar(tooltip, rect, '<b>'+formatXAxis(_self.data[i][0], offset).join(' ')+'</b><br>'+_self.data[0][j]+': '+_self.data[i][j], _self.data[i], j, options);
								group.appendChild(rect);
							}
						}
					}
					// Draw Histogram
					else if (chartType == 'histogram'){
						//	Draw column for bins
						barHeight = _self.data[i][1] * areaHeight / colMax;
						var rect = elemSVG('rect', null, {
										id: 'value-'+_self.data[i][0],
										x: 0,//setWidth * (i-0.5)
										y: areaHeight - barHeight + options.chartArea.top,
										width: setWidth - 1,
										height: barHeight,
										style: 'fill:'+colors[i-1]+';'
									});
						new chartBar(tooltip, rect, '<b>'+formatXAxis(_self.data[i][0], offset).join(' ')+'</b><br>'+_self.data[0][j]+': '+_self.data[i][j], _self.data[i], j, options);
						group.appendChild(rect);
					}
					//	Draw Line Chart
					else if (chartType == 'line'){
						for (var j = 1; j < _self.data[i].length; j++){
							if (typeof _self.data[i][j] != 'undefined' && _self.data[i][j] != null && !isNaN(_self.data[i][j])){//_self.data[i][j] != null
								barHeight = _self.data[i][j] * areaHeight / colMax;
								if (typeof options.prevOffset != 'undefined' && typeof options.prevOffset[j-1] != 'undefined')
									offset = options.prevOffset[j-1];
								else
									offset = 0;
								d = lines[j-1].getAttribute('d');
								lines[j-1].setAttribute('d', d+(d == '' ? 'M ' : (' S '+
															(((setWidth-(setWidth/12)-(colWidth/2))/2) + (setWidth * (getVal(_self.data[i][0]) - rowMin) / xGap))+','+(areaHeight - barHeight + options.chartArea.top)+' ' ) )+//(0.8*barHeight + 0.2*(_self.data[i-1][j] * areaHeight / colMax))
															(((setWidth+(setWidth/12)-(colWidth/2))/2) + (setWidth * (getVal(_self.data[i][0]) - rowMin) / xGap))+','+(areaHeight - barHeight + options.chartArea.top));
								if (options.points){
									circleHover = elemSVG('g');
									circle = elemSVG('circle', null, {cx: ((setWidth+(setWidth/16)-(colWidth/2))/2), cy: (areaHeight - barHeight + options.chartArea.top), r: (options.points / 2), stroke: 'none', 'stroke-width': 0, fill: colors[j-1]});
									circleHover.appendChild(circle);
									circle2 = elemSVG('circle', null, {cx: ((setWidth+(setWidth/16)-(colWidth/2))/2), cy: (areaHeight - barHeight + options.chartArea.top), r: (1 * options.points)+2, fill: 'rgba(255, 255, 255, 0)', class: 'hover'});
									circleHover.appendChild(circle2);
									new chartBar(tooltip, circleHover, '<b>'+formatXAxis(_self.data[i][0], offset).join(' ')+'</b><br>'+_self.data[0][j]+': '+_self.data[i][j], _self.data[i], j, options);
									group.appendChild(circleHover);
								}
							}
						}
					}
					//
					//	Draw txt labels
					if (30 + lastLblX < setWidth * (getVal(_self.data[i][0]) - rowMin) / xGap){
						var txtLbl = elemSVG('g');
						if (typeof options.rotateXAxisLabels != 'undefined')
							txtLbl.setAttribute('transform', 'rotate(-'+options.rotateXAxisLabels+' '+(10+(setWidth/5))+' '+(10+context.offsetHeight-options.chartArea.bottom+options.chartArea.top)+')');
						txtLbl.appendChild(elemSVG('text', formatXAxis(_self.data[i][0])[0], {
							'text-anchor': 'middle', 'font-size': 12, 'font-family': options.fontFamily,
							fill: options.fontColor,
							transform: 'translate('+((setWidth/2)-10)+', '+(areaHeight+25)+')',
							//y: 0, //y: areaHeight+20
							}));
						txtLbl.appendChild(elemSVG('text', formatXAxis(_self.data[i][0])[1], {
							'text-anchor': 'middle', 'font-size': 12, 'font-family': options.fontFamily,
							fill: options.fontColor,
							transform: 'translate('+((setWidth/2)-10)+', '+(areaHeight+35)+')',
							//y: 0, //y: areaHeight+30
							}));
						group.appendChild(txtLbl);
						//
						lastLblX = setWidth * (getVal(_self.data[i][0]) - rowMin) / xGap;
					}
					else if (chartType == 'histogram'){
						var txtLbl = elemSVG('g', null, {
								transform: (typeof options.rotateXAxisLabels != 'undefined' ?
											'rotate(-'+options.rotateXAxisLabels+' '+(10+(setWidth/5))+' '+(10+context.offsetHeight-options.chartArea.bottom+options.chartArea.top)+')' : '')
							});
						txtLbl.appendChild(elemSVG('text', _self.data[i][0], {
							'text-anchor': 'middle',
							'font-size': 12,
							//y: 0, //y: areaHeight+20,
							fill: options.fontColor,
							transform: 'translate('+(setWidth/2)+', '+(areaHeight+25)+')'}));
						group.appendChild(txtLbl);
						//
						lastLblX = setWidth * (getVal(_self.data[i][0]) - rowMin) / xGap;
					}
					// click handler for labels
					if (typeof txtLbl !== 'undefined'&& typeof options.labelOnclick != 'undefined' && options.labelOnclick != false ) {
						(function (data) {
							txtLbl.onclick = function () {
								options.labelOnclick(data);
							}
						})(_self.data[i]);

						txtLbl.style.cursor = 'pointer';
					}
					chartArea.appendChild(group);
				}
			}
			//
			graph.appendChild(chartGrid);
			graph.appendChild(chartBG);
			graph.appendChild(chartArea);
			context.innerHTML = '';
			context.appendChild(graph);
			context.appendChild(tooltip);
			//graph.appendChild(elemSVG('style', '@font-face{font-family: "Linotte-Regular";src: '+
			//			'url("https://www.prodoscore.com/wp-content/themes/prodoscore/fonts/307BC1_0_0.woff2") format("woff2"), '+
			//			'url("https://www.prodoscore.com/wp-content/themes/prodoscore/fonts/307BC1_0_0.woff") format("woff");'));
			//graph.appendChild(elemSVG('clipPath', '<use xlink:href="#rect"/>', {id:'clip'}));
			new contextMenu(graph, {
					'download-svg': {
						icon: 'fa-line-chart',
						title: 'Download as SVG',
						onclick: function(graph){
							//console.log(graph.innerHTML);
							downloadSVG(graph, graph.parentNode.parentNode.q('h2')[0].innerHTML.replace(/<\/?[^>]+(>|$)/g , ''));
						}
					},
					'download-png': {
						icon: 'fa-line-chart',
						title: 'Download as PNG',
						onclick: function(graph){
							//downloadSVG(graph, graph.parentNode.parentNode.q('h2')[0].innerHTML.replace(/<\/?[^>]+(>|$)/g , ''));
							context.addClass('say-cheese');
							html2canvas(graph,
								{
									height: graph.offsetHeight, width: graph.offsetWidth, top: 0, background: '#FFFFFF',
									onrendered: function(canvas){
										downloadPNG(canvas, graph.parentNode.parentNode.q('h2')[0].innerHTML.replace(/<\/?[^>]+(>|$)/g , ''));
										context.removeClass('say-cheese');
									}
								});
						}
					}
				});
		}
		//
		if (typeof callback != 'undefined')
			callback(_self);
		//
		//	Redraw this graph on window resize
		if (typeof isRedraw == 'undefined' || !isRedraw){
			if (redrawCallbackIndex.objects.indexOf(context) > -1){
				var i = redrawCallbackIndex.objects.indexOf(context);
				window.removeEventListener('resize', redrawCallbackIndex.functions[i]);
				delete redrawCallbackIndex.arccharts[i];
				redrawCallbackIndex.objects.splice(i, 1);
				redrawCallbackIndex.functions.splice(i, 1);
				redrawCallbackIndex.arccharts.splice(i, 1);
			}
			redrawCallbackIndex.functions.push(redrawOnResize);
			redrawCallbackIndex.objects.push(context);
			redrawCallbackIndex.arccharts.push(_self);
			window.addEventListener('resize', redrawOnResize, false);
		}
		//
		return graph;
	};
	//
	var redrawTimeout,
	redrawOnResize = function(){
		var parentNode = context.parentNode;
		while (parentNode != document.body){
			if (parentNode.style.display == 'none')
				return false;
			parentNode = parentNode.parentNode;
		}
		//console.log(context);
		clearTimeout(redrawTimeout);
		redrawTimeout = setTimeout(function(){
				graph.innerHTML = '';
				_self.draw(_self.chartType, true);
			}, 250);
	};
	//
	//	Draw a horizontal line in the chart background
	function drawHorizontalLine(graph, areaWidth, options, height, value, color){
		graph.appendChild(elemSVG('path', null, {
			d: 'M'+(options.chartArea.left-5)+','+height+
			' L'+(areaWidth)+','+height, stroke: color}));//-options.chartArea.right
		graph.appendChild(elemSVG('text', value,
			{'text-anchor': 'end',
			'font-size': 12,
			y: height + 5,
			x: options.chartArea.left - 8,
			fill: options.fontColor}));
	}
	//
	//	Calculate the pixel Y value for a given chart data
	function topForVal(context, options, areaHeight, colMax, val){
		return context.offsetHeight - options.chartArea.bottom + options.chartArea.top - (areaHeight * val / colMax);
	}
	//
	//	Format string for X axis labels
	function formatXAxis(x, offset){
		if (x instanceof Date){
			//*/
			if (typeof offset == 'number' && offset > 0){
				x = new Date(x)
				x.setDate(x.getDate()-offset);
			}
			//*/
			return [x.toString().substring(0, 3), x.toString().substring(4, 10), x.toString().substring(11, 15)];
		}
		else
			return [x, ''];
	}
	//
	//	Get X axis absolute value for a DateTime object
	function getVal(x, alt){
		if (x instanceof Date)
			return x.getTime();
		else if (x.indexOf(':') > -1)
			return timeToMts(x);
		else if (typeof x == 'string' && typeof alt != 'undefined')
			return alt;
		else
			return x;
	}
	//
	var hoverLatch = false;
	//	Handle tooltip for chart columns
	var tooltipRepositionTimeout = [false, false];
	context.onmouseover = function(e){
		var rect = {top: 0, left: 0};//context.getBoundingClientRect();
		//console.log(rect.left+':'+rect.top);
		clearTimeout(tooltipRepositionTimeout[0]);
		clearTimeout(tooltipRepositionTimeout[1]);
		tooltipRepositionTimeout[0] = setTimeout(function(){
			if (typeof options.tooltipPosition != 'undefined'){
				if (options.tooltipPosition == 'to-right'){
					tooltip.style.top = (e.clientY-rect.top-(tooltip.offsetHeight/1.5)+context.offsetTop)+'px';
					tooltip.style.left = (e.clientX-rect.left+8+context.offsetLeft)+'px';
				}
				else{
					tooltip.style.top = (e.clientY-rect.top-(tooltip.offsetHeight*1)+context.offsetTop-50)+'px';
					tooltip.style.left = (e.clientX-rect.left-(tooltip.offsetWidth/2)+context.offsetLeft)+'px';
				}
			}
			else{
				tooltip.style.top = (e.clientY-rect.top-(tooltip.offsetHeight*1)+context.offsetTop-50)+'px';
				tooltip.style.left = (e.clientX-rect.left-(tooltip.offsetWidth/2)+context.offsetLeft)+'px';
			}
			if (rect.left+tooltip.offsetLeft < 0)//1*tooltip.style.left.replace('px', '')
				tooltip.style.left = (0-rect.left)+'px';
			//
			if (rect.top+tooltip.offsetTop < 0){//1*tooltip.style.top.replace('px', '')
				if (options.tooltipPosition == 'to-right')
					tooltip.style.top = (0-rect.top)+'px';
				else
					tooltip.style.top = (e.clientY-rect.top+context.offsetTop-20)+'px';
			}
		}, 20);
		tooltipRepositionTimeout[1] = setTimeout(function(){
			if (tooltip.offsetWidth + tooltip.offsetLeft > window.innerWidth)
				tooltip.style.left = (window.innerWidth - tooltip.offsetWidth - 16)+'px';
			if (tooltip.offsetHeight + tooltip.offsetTop > window.innerHeight)
				tooltip.style.top = (window.innerHeight - tooltip.offsetHeight - 4)+'px';
		}, 280);
	}
	/*tooltip.onmouseover = function(){
		hoverLatch = true;
		tooltip.style.opacity = 1;
		tooltip.style.transform = 'scale(1)';
	}
	tooltip.onmouseout = function(){
		if (!hoverLatch)
			return false
		tooltip.style.opacity = 0;
		tooltip.style.transform = 'scale(0.1)';
		hoverLatch = false;
	}*/
	function chartBar(tooltip, obj, tooltipText, row, col, options){
		if (typeof options.columnOnclick != 'undefined' && options.columnOnclick != false){
			obj.onclick = function(){
				options.columnOnclick(row, col, options);
			}
			obj.style.cursor = 'pointer';
		}
		obj.onmouseover = function(e){
			tooltip.innerHTML = tooltipText;
			if (typeof options.columnOnHover != 'undefined' && options.columnOnHover != false)
				options.columnOnHover(row, tooltip, e);
			//
			tooltip.style.display = 'block';
			tooltip.style.opacity = 1;
			tooltip.style.transform = 'scale(1)';
		}
		obj.onmouseout = function(){
			if (!hoverLatch){
				tooltip.style.opacity = 0;
				tooltip.style.transform = 'scale(0.1)';
			}
		}
	}
}

function elemSVG(tagname, innerHTML, options){
	var obj = document.createElementNS('http://www.w3.org/2000/svg', tagname);
	if (typeof innerHTML !== 'undefined' && innerHTML != null && innerHTML != false)
		obj.innerHTML = innerHTML;
	if (typeof options !== 'undefined')
		for (var key in options)
			obj.setAttribute(key, options[key]);
	return obj;
}

// ==============================================================================================
// ==============================================================================================
//													Arc Charts
// ==============================================================================================

function drawChartEmployeeDashboard(context, chartData, selCallback, colors, pointSize){
	context.innerHTML = loadingIndicator;
	if (typeof colors == 'undefined')
		colors = ['#3366cc'];
	if (typeof pointSize == 'undefined')
		pointSize = 6;
	var chart = new ArcChart(context, chartData, colors,
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnclick: selCallback,
			verticalRanges: verticalRanges,
			points: pointSize,
			curve: 0
		});
		var res = chart.draw('line');
}

function drawChartDashboardProdoScore(context, chartData, offset, selCallback, colors, pointSize){
	context.innerHTML = loadingIndicator;
	if (typeof colors == 'undefined')
		colors = ['hsla(206, 60%, 80%, 0.75)', '#3366cc'];//
	if (typeof pointSize == 'undefined')
		pointSize = 6;
	var chart = new ArcChart(context, chartData, colors,
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnclick: selCallback,
			verticalRanges: verticalRanges,
			prevOffset: [offset, 0],
			points: pointSize,
			curve: 0
		});
		var res = chart.draw('line');
}

function drawChartDashboardHistogram(context, chartData, colors, columnHover){
	context.innerHTML = loadingIndicator;
	new ArcChart(context, chartData, colors,
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnHover: columnHover,
			//tooltipPosition: 'to-right',
			tooltipPosition: 'above',
			setSpacing: 'histogram',
		}).draw('histogram');
}

function drawChartStrata(context, chartData, color, columnHover){
	context.innerHTML = loadingIndicator;
	new ArcChart(context, chartData, [color],
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnHover: columnHover,
			tooltipPosition: 'to-right',
		}).draw('column');
}

//	-------------------------------------------------------------------------------------------------------------

function drawChartManagers(context, chartData, colors, callback, additionalData){
	context.innerHTML = loadingIndicator;
	new ArcChart(context, chartData, colors,
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnclick: callback,
			verticalRanges: verticalRanges,
			additionalData: additionalData
		}).draw('column');
}

function drawChartEmployees4Manager(context, chartData, colors, callback, additionalData){
	context.innerHTML = loadingIndicator;
	new ArcChart(context, chartData, colors,
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnclick: callback,
			additionalData: additionalData,
			//
			verticalRanges: verticalRanges
		}).draw('column');
}

//	-------------------------------------------------------------------------------------------------------------

function drawChartEmployee(context, chartData, colors, offset, selCallback, labelCallback){
	context.innerHTML = loadingIndicator;
	new ArcChart(context, chartData, ['hsla('+colors[3]+', 60%, 80%, 0.75)', '#'+colors[1]],//
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnclick: selCallback,
			labelOnclick: labelCallback,
			prevOffset: [offset, 0],
			verticalRanges: verticalRanges
		}).draw('column');
}

function drawChartEmployeeBusy5m(context, chartData, selCallback){
	context.innerHTML = loadingIndicator;
	var chart = new ArcChart(context, chartData, ['#3366cc'],
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			backgroundColor: 'transparent',
			verticalRanges: {
				below: [0, 2, 'rgba(240, 50, 50, 0.2)'],
				average: [2, 6, 'rgba(240, 240, 50, 0.2)'],
				above: [6, 6.5, 'rgba(50, 240, 140, 0.2)'],
				wayabove: [6.5, 7, 'rgba(50, 240, 140, 0.1)']
			},
			columnOnclick: selCallback,
			points: 5,
			curve: 4
		});
		var res = chart.draw('line');
}

function drawChartEmployeeOneDay(context, chartData, colors){
	context.innerHTML = loadingIndicator;
	google.charts.setOnLoadCallback(
		function(){
			var data = google.visualization.arrayToDataTable(chartData);
			var options = {
				fontName: fontName,
				slices: colors
			};
			setTimeout(
				function(){
					var chart = new google.visualization.PieChart(context);
					chart.draw(data, options);
				}, 10);
		});
}

//	-------------------------------------------------------------------------------------------------------------

function drawChartCorrelationChange(context, chartData, selCallback){
	context.innerHTML = loadingIndicator;
	if (typeof colors == 'undefined')
		colors = ['#3366cc'];//, 'hsla(206, 60%, 80%, 0.75)'
	if (typeof pointSize == 'undefined')
		pointSize = 6;
	var chart = new ArcChart(context, chartData, colors,
		{
			chartArea:{left:40, right:20, bottom:60, top:10},
			fontFamily: 'Linotte-Regular',
			columnOnclick: selCallback,
			//verticalRanges: verticalRanges,
			//prevOffset: [0, 0],
			points: pointSize,
			curve: 0
		});
		var res = chart.draw('line');
}

//	-------------------------------------------------------------------------------------------------------------

function drawOnChart(chart, data){
	var layout = chart.getChartLayoutInterface();
	var chartArea = layout.getChartAreaBoundingBox();
	var svg = chart.getContainer().getElementsByTagName('svg')[0];
	var xLoc = layout.getXLocation(value);
	//
	var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
	line.setAttribute('x1', x1);
	line.setAttribute('y1', y1);
	line.setAttribute('x2', x2);
	line.setAttribute('y2', y2);
	line.setAttribute('stroke', color);
	line.setAttribute('stroke-width', w);
	//
	svg.appendChild(createLine(xLoc,chartArea.top + chartArea.height,xLoc,chartArea.top,'#00cccc',4));
}

function bindChartHover(chart, index, content){
	for (var i in index){
		var user = content.querySelectorAll('[data-id="'+i+'"]')[0];
		if (user != undefined){
			user.onmouseover = function(){
				chart.setSelection([{'column': index[this.getAttribute('data-id')]}]);
			};
			user.onmouseout = function(){
				chart.setSelection([]);
			};
		}
	}
}

//	-------------------------------------------------------------------------------------------------------------
