
var trialSeats = 25;
function loadTrialSeats(callback){
	//let bkpcsrftoken = csrftoken;
	//csrftoken = false;
	new ajax('https://gcpmarketplace.prodoscore.com/domain-billing-info', {
		method: 'POST',
		data: {'domain': domain},
		callback: function (data){
			data = JSON.parse(data.responseText);
			if (data.plan == 'cancelled'){
				trialSeats = 25;
				callback(data.plan, 25);
				return false;
			}
			else{
				trialSeats = parseInt(data.plan.replace(/\D/g, ''));
				callback(data.plan, trialSeats);
			}
			//
			let planSeatStrs = q('span.var-plan-seats');
			for (let i = 0; i < planSeatStrs.length; i++)
				planSeatStrs[i].innerHTML = trialSeats;
			//
			let planNameStrs = q('span.var-plan-name');
			for (let i = 0; i < planNameStrs.length; i++)
				planNameStrs[i].innerHTML = data.plan;
		}
	});
	//
	new ajax('https://gcpmarketplace.prodoscore.com/update-master', {method: 'POST', data: {'domain': domain}, callback: function (data){}});
	//csrftoken = bkpcsrftoken;
}

var setLayoutTimeout = false;
function setLayout(context, repeat) {
	var headerBar = document.querySelector('#header_bar'),
		footerBar = context.q('.content-footer')[0],
		siteContent = document.querySelector('#site_content'),
		viewPortHeight = 0,
		hbHeight = 0,
		fbHeight = 0;

	// Get a cross browser viewport height
	// the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
	if (typeof window.innerWidth != 'undefined') {
		viewPortHeight = window.innerHeight;
	}

	// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
	else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientHeight != 'undefined' && document.documentElement.clientHeight != 0){
		viewPortHeight = document.documentElement.clientHeight;
	}

	// older versions of IE
	else{
		viewPortHeight = document.getElementsByTagName('body')[0].clientHeight;
	}

	hbHeight = headerBar.offsetHeight;

	if (typeof footerBar != 'undefined') {
		fbHeight = footerBar.offsetHeight;//69;
	}

	document.getElementsByTagName('body')[0].style.height = viewPortHeight + 'px';
	siteContent.style.height = (viewPortHeight - (hbHeight + fbHeight)) + 'px';
	siteContent.style.top = hbHeight + 'px';

	window.addEventListener("resize", function () {
		clearTimeout(setLayoutTimeout);
		setLayoutTimeout = setTimeout(function(){
			setLayout(context);
		}, 200);
	});
	if (typeof repeat == 'undefined')
		setTimeout(function(){
			setLayout(context, false);
		}, 200);
}

var steps = q('.steps .step');
var instep = 0;

function setStep(step) {
	for (var i = 0; i < steps.length; i++)
		steps[i].removeClass('current');
	//
	if (step <= steps.length)
		steps[step - 1].addClass('current');
	//
	instep = step;
}

function exitWarning() {
	window.onbeforeunload = function(){
		return "You haven't completed the onboarding process yet. Are you sure you want to exit?";
	}
}

function showMessage(text, type, holder, keep) {
	var message = document.createElement("DIV"),
			timer = null;

	clearInterval(timer);
	message.classList.add("alert", "alert-"+type);
	message.innerHTML = text;

	if (typeof keep == 'undefined' || !keep){
		holder.innerHTML = "";
		timer = setTimeout(function () {
			holder.innerHTML = "";
		}, 6000);
	}
	holder.appendChild(message);
}

//	Let's Get Started.!
new navFrame('step-a', q('#install-api-client')[0],
	function(context, params){
		steps[0].parentNode.parentNode.style.height = '0px';
		//setStep(1);
		setLayout(context);
		//	Check if API client already installed
		/*var checkAndGotoStep2 = function(callback){
			getUsersData(context,
				function(users, self){
					//if (typeof users == 'undefined' || users.length > 0)	//	Are you bonkers.?
					if (typeof users != 'undefined' && users.length > 0)
						//	Send user to step 2
						setTimeout(function(){
							document.location.hash = '#step-2';
						}, 240);
					if (typeof callback != 'undefined')
						callback(users);
				}, params);
		}
		checkAndGotoStep2();*/
		//gapi.additnow.render('install-button', { 'applicationid': '500114080071' });//, 'authuser': ''
		//
		//
		//	Open Marketplace listing in Popup
		var nextStep2 = context.q('#next-step-2')[0];
		var nextStep2Btn = nextStep2.q('button[type="submit"]')[0];
		/*nextStep2Btn.disabled = true;
		context.q('#popup-marketplace')[0].onclick = function(){
			//window.open('https://chrome.google.com/webstore/detail/prodoscore/agbflipnplijfghpaapdcpfcacbfphpc', 'marketplace');
			window.open('https://apps.google.com/marketplace/u/0/app/agbflipnplijfghpaapdcpfcacbfphpc', '_self');
			nextStep2Btn.disabled = false;
		}*/
		nextStep2Btn.onclick = function(){
			/*new ajax(base_url + 'register/get-domains/', {
				callback: function (data) {
					data = eval('(' + data.responseText + ')');
					if (data.length == 1)
						document.location.hash = '#step-2';
					else
						document.location.hash = '#step-b';
				}
			});*/
			document.location.hash = '#step-b';
			/*checkAndGotoStep2(function(users){
				if (typeof users == 'undefined' || users.length == 0){
					alert('Please install Prodoscore from Google Apps Marketplace to continue.');
				}
			});*/
		};
	});

new navFrame('step-b', q('#select-domain')[0],
	function(context, params, e, loaded){
		/*context.style.display = 'none';
		context.removeClass('active');*/
		steps[0].parentNode.parentNode.style.height = '0px';
		//setStep(1);
		setLayout(context);
		//
		var form = context.q('form')[0];
		var ul = form.q('ul.fa-ul')[0];
		var searchq = context.q('input[name="search"]')[0];
		var clsq = searchq.parentNode.q('i.fa')[0];
		var noResults = elem('i', 'No domains matching your search.');
		new ajax(base_url + 'register/get-domains/', {
			callback: function (data) {
				data = eval('(' + data.responseText + ')');
				ul.innerHTML = '';
				noResults.style.display = 'none';
				if (typeof data.error != 'undefined' && data.error == 'unauthorized_client'){
					//document.location.hash = '#step-1';
					context.q('div.inner-wrapper.search')[0].style.display = 'none';
					form.style.display = 'none';
					context.q('h2')[0].innerHTML = 'Error - insufficient permissions';
					context.q('p')[0].className = 'text-left';
					context.q('p')[0].innerHTML = 'Prodoscore does not have sufficient access to your G Suite account.<br/>'+
						'Here are some troubleshooting to fix this:<ul>'+
						'<li>Ensure G Suite Super admin account (Contact your G Suite Super Admin)</li>'+
						'<li>Application is to be installed domain-wide</li>'+
						'</ul>If you are still unable to troubleshoot, please contact support for further assistance.';
				}
				else if (data.length == 1){
					document.location.hash = '#step-1';
					return false;
				}
				else
					for (var i = 0; i < data.length; i++)
						if (data[i].status < 7)
							ul.appendChild(elem('li', '<label><input type="radio" name="domain" value="'+data[i].title+'">'+data[i].title+'</label>', {class: 'col-xs-6'}));
				//
				ul.appendChild(noResults);
				/*context.style.display = 'block';
				context.addClass('active');*/
				loaded();
			},
			fallback: function(){
				document.location.hash = '#step-1';
			}
		});
		//
		form.onsubmit = function(){
			if (this.domain.value == '')
				alert('Please select a domain below.');
			else{
				domain = this.domain.value;
				new ajax(base_url + 'register/select-domain/?domain='+this.domain.value, {//+'/'.replace('.' , '-dot-')
					callback: function (data) {
						data = eval('(' + data.responseText + ')');
						document.location.hash = '#step-1';
					}
				});
			}
			return false;
		};
		//
		searchq.onkeyup = function(){
			var status, allHidden = true;
			var childNodes = ul.q('li');
			for (var i = 0; i < childNodes.length; i++){
				status = this.value == '' || childNodes[i].q('input')[0].value.indexOf(this.value) > -1 ? true : false;
				childNodes[i].style.display = status ? 'block' : 'none';
				if (status)
					allHidden = false;
			}
			noResults.style.display = allHidden ? 'block' : 'none';
		};
		clsq.onclick = function(){
			searchq.value = '';
			searchq.onkeyup();
		};
	});

new navFrame('step-1', q('#time-settings')[0],
	function(context, params, e, loaded){
		steps[0].parentNode.parentNode.style.height = 'auto';
		setStep(1);
		setLayout(context);
		var form = context.q('form')[0];
		var weekdays = form.q('.working-days input[name="day"]');
		var skipBtn = form.skip,
		skip = false;
		exitWarning();
		skipBtn.onclick = function (){
			skip = true;
		}
		//
		new ajax(base_url+'register/time-settings/',
			{method: 'GET',
			callback: function(data){
				timeSettings = eval('('+data.responseText+')');
				//
				if (typeof timeSettings.workingdays == 'undefined' && typeof timeSettings.message != 'undefined'){
					//document.location.hash = '#step-1';
					document.location = 'https://apps.google.com/marketplace/u/0/app/agbflipnplijfghpaapdcpfcacbfphpc';
					//document.location = 'https://chrome.google.com/webstore/detail/prodoscore/agbflipnplijfghpaapdcpfcacbfphpc';
					//alert('Please install the API client first (Install Prodoscore from Marketplace)');
					return false;
				}
				//
				//	Load the Timezone list to select options
				new ajax(base_url+'timezone/', {
					callback: function(data){
						var region = context.q('form #region')[0], timezone = context.q('form #timezone')[0], tmp, tmpi;
						region.innerHTML = '';
						timezone.innerHTML = '';
						data = eval('('+data.responseText+')');
						var timezoneregion = ['US', 'Pacific'];
						if (typeof timeSettings != 'undefined'){
							timezoneregion = timeSettings.timezone.split('/');
						}
						for (var i = 0; i < data.length; i++){
							tmp = elem('option', data[i][0], {value: i});
							if (data[i][0] == timezoneregion[0]){
								tmp.selected = true;
								for (var j = 0; j < data[i][1].length; j++){
									tmpi = elem('option', data[i][1][j][1], {value: data[i][1][j][0]});
									if (data[i][1][j][0] == timeSettings.timezone)
										tmpi.selected = true;
									timezone.appendChild(tmpi);
								}
							}
							region.appendChild(tmp);
						}
						region.onchange = function(){
							timezone.innerHTML = '';
							for (var i = 0; i < data[this.value][1].length; i++){
								tmp = elem('option', data[this.value][1][i][1], {value: data[this.value][1][i][0]});
								if (data[this.value][1][i][0] == timeSettings.timezone)
									tmp.selected = true;
								timezone.appendChild(tmp);
							}
						}
						loaded();
					},
					cache: true});
				//
				//	Set default working days
				var workingdays = [1,2,3,4,5];
				if (typeof timeSettings != 'undefined'){
					workingdays = timeSettings.workingdays;
					//
					if (typeof timeSettings.daystart != 'undefined' && typeof timeSettings.dayend != 'undefined'){
						form.daystart.value = timeSettings.daystart;
						form.dayend.value = timeSettings.dayend;
					}
					if (typeof timeSettings.report_enabled != 'undefined'){
						timeSettings.report_enabled = ((32 + 1*timeSettings.report_enabled) >>> 0).toString(2).split('').reverse();
					}
				}
				for (var i = 0; i < weekdays.length; i++)
					if (workingdays.indexOf(i) > -1)
						weekdays[i].checked = true;
				}
			});
		//	Save time settings
		form.onsubmit = function(){
			var postData = null;
			if (skip){
				// if step is skipped set default values and submit, ignoring any entered values
				postData = {
					workingdays: [1,2,3,4,5],
					daystart: "09:00",
					dayend: "18:00",
					timezone: "America/Los_Angeles"
				}
			}
			else{
				var workingdays = [];
				for (var i = 0; i < weekdays.length; i++) {
					if (weekdays[i].checked) {
						workingdays.push(i);
					}
				}
				//
				//	Validate form data
				if (form.daystart.value == ''){
					//alert('Please select work hours start time.');
					showMessage('Please select work hours start time.', 'danger', context.q('.error-holder')[0]);
					return false;
				}
				else if (form.dayend.value == ''){
					//alert('Please select work hours end time.');
					showMessage('Please select work hours end time.', 'danger', context.q('.error-holder')[0]);
					return false;
				}
				else if (form.daystart.value >= form.dayend.value) {
					showMessage('Change the start time to be before the end time.', 'danger', context.q('.error-holder')[0]);
					return false;
				}
				else if (workingdays.length == 0){
					//alert('You have to select at least one working day.');
					showMessage('You have to select at least one working day.', 'danger', context.q('.error-holder')[0]);
					return false;
				}
				else if (workingdays.length < 3){
					if (!confirm('Are you sure you want to proceed with less than 3 working days?'))
						return false;
				}
				postData = {
					workingdays: workingdays,
					daystart: form.daystart.value,
					dayend: form.dayend.value,
					timezone: form.timezone.value
				}
			}
			//
			//	Submit time settings
			new ajax(base_url+'register/time-settings/', {
				method: 'POST',
				data: postData,
				callback: function(data){
					context.q('.error-holder')[0].innerHTML = "";
					document.location.hash = '#step-2';
				}
			});
			return false;
		}
	});

var validateXLSXData = function(data, file, fileData){};
var fileDropped = function(file){};
new navFrame('step-2', q('#upload-user-list')[0],
	function(context, params, e, loaded){
		setStep(2);
		setLayout(context);
		var submitBtns = context.q('button[type="submit"]');
		var errorHolder = context.q('div.error-holder')[0];
		var errorMessagePlus = context.q('div.error-message-plus')[0];
		var popup = context.q('div.popup-bg')[0];
		errorHolder.innerHTML = '';
		errorMessagePlus.style.display = 'none';
		popup.style.display = 'none';
		fileDropped = function(file){
			errorHolder.innerHTML = '';
			errorMessagePlus.style.display = 'none';
		};
		loadTrialSeats(function(plan, seats){});
		validateXLSXData = function(data, file, fileData){
			errorHolder.innerHTML = '';
			errorMessagePlus.style.display = 'none';
			submitBtns[0].setAttribute('disabled', true);
			submitBtns[1].setAttribute('disabled', true);
			//	First row has to be faximile
			if (!data || JSON.stringify(data[1]) != '{"0":"Count","1":"Employee name","2":"Employee email","3":"User role","4":"Manager name","5":"Manager\'s Email Address"}'){
				alert('Please upload an XLSX file of correct format. Use the given template.');// The file you uploaded is corrupted.
				submitBtns[0].removeAttribute('disabled');
				return false;
			}
			var emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var roles = ['Administrator', 'Manager', 'System', 'Terminated', 'Not activated', 'Business Development', 'Contractor', 'Customer Service', 'Finance & Acc', 'Human Resource', 'Inside Sales', 'IT', 'Marketing', 'Operations', 'Procurement', 'Project Management', 'Sales', 'Sales Eng', 'Sales Support', 'Support', 'Tech Support', 'Channel Sales'];
			//
			var employees = {}, missingMgrs = {}, errors = [];
			var rows = Object.keys(data), row, remaining = trialSeats;
			for (var i = 2; i < rows.length; i++){
				row = data[ rows[i] ];
				//
				//	Validate email address
				if (typeof row[2] == 'undefined' || row[2] == '' || ! emailRegEx.test(row[2]))
					errors.push([rows[i], 'email', row[2]]);
				//
				//	Validate role is within enum
				if (typeof row[3] == 'undefined' || roles.indexOf(row[3]) == -1)
					errors.push([rows[i], 'role', row[3]]);
				//
				// Index all employee roles by email
				//if (row[3] == 'Manager' || row[3] == 'Administrator')
				if (typeof employees[ row[2] ] != 'undefined')
					errors.push([rows[i], 'duplicate', row[1]+' &lt;'+row[2]+'&gt;']);
				else{
					remaining -= 1;
					if (remaining == -1){
						errors.push([rows[i], 'license-exceeded', row[1]+' &lt;'+row[2]+'&gt;']);
						break;
					}
					else if (remaining > -1)
						employees[ row[2] ] = [ row[1]+' &lt;'+row[2]+'&gt;', row[0], row[3], rows[i] ];
				}
			}
			//	Validate if managers within specified admins and managers
			for (var i = 2; i < rows.length; i++){
				row = data[ rows[i] ];
				if (typeof row[4] == 'undefined')
					row[4] = row[5];
				else
					row[4] += ' &lt;'+row[5]+'&gt;';
				//
				//	Manager unspecified
				if (typeof row[5] == 'undefined' || row[5] == ''){}
				//
				//	Manager is self
				else if (row[5].toLowerCase() == row[2].toLowerCase()){
					errors.push([rows[i], 'smanager', row[4]]);
				}
				//
				//	Manager is not among given employees
				else if (typeof employees[ row[5] ] == 'undefined'){//row[4] != '' || 
					/*if (typeof row[4] == 'undefined' && typeof row[5] == 'undefined')
						missingMgrs[ row[5] ] = [rows[i], 'at row '+rows[i]];
					else*/
					errors.push([rows[i], 'manager', row[4]]);
				}
				//
				//	Manager is not specified as manager nor admin
				else if (employees[ row[5] ][2] != 'Manager' && employees[ row[5] ][2] != 'Administrator'){
					//missingMgrs[ row[5] ] = employees[ row[5] ];
					errors.push([ rows[i]/*employees[ row[5] ][3]*/, 'nmanager', employees[ row[5] ][0] ]);
					//errors.push([rows[i], 'manager']);
				}
			}
			/*var keys = Object.keys(missingMgrs);
			for (var i = 0; i < keys.length; i++)
				errors.push([ missingMgrs[ keys[i] ][3], 'nmanager', missingMgrs[ keys[i] ][0] ]);*/
			//
			//	There are errors found
			if (errors.length > 0){
				console.log(errors);
				//	Display error messages
				for (var i = 0; i < errors.length; i++)
					if (errors[i][1] == 'email')
						showMessage('Email address <i>\''+errors[i][2]+'\'</i> at <b>C'+errors[i][0]+'</b> is invalid.', 'danger', errorHolder, true);
					else if (errors[i][1] == 'role')
						showMessage('User role <i>\''+errors[i][2]+'\'</i> at <b>D'+errors[i][0]+'</b> is invalid.', 'danger', errorHolder, true);
					else if (errors[i][1] == 'duplicate')
						showMessage('Employee <i>\''+errors[i][2]+'\'</i> at <b>C'+errors[i][0]+'</b> is a duplicate.', 'danger', errorHolder, true);
					else if (errors[i][1] == 'manager')
						showMessage('Manager <i>\''+errors[i][2]+'\'</i> at <b>F'+errors[i][0]+'</b> is not found.', 'danger', errorHolder, true);
					else if (errors[i][1] == 'nmanager')
						showMessage('Manager <i>\''+errors[i][2]+'\'</i> at <b>F'+errors[i][0]+'</b> is not defined as Manager.', 'danger', errorHolder, true);
					else if (errors[i][1] == 'smanager')
						showMessage('Manager <i>\''+errors[i][2]+'\'</i> at <b>F'+errors[i][0]+'</b> is themselves.', 'danger', errorHolder, true);
					else if (errors[i][1] == 'license-exceeded')
						showMessage('Number of users in the uploaded file exceeds the number of licenses ('+trialSeats+') available.', 'danger', errorHolder, true);
				//
				errorMessagePlus.style.display = 'block';	//	Additional error text
				submitBtns[0].removeAttribute('disabled');
				//
				return false;
			}
			//
			//	All validations passed
			else{
				uploadFile(file, fileData, function(data){
					popup.style.display = 'block';
					setTimeout(function(){
						popup.addClass('active');
					}, 10);
					//
					setTimeout(function(){
						popup.removeClass('active');
						setTimeout(function(){
							popup.style.display = 'none';
						}, 410);
					}, 3210);
					//alert('Your XLSX file is uploaded and being processed.\nPress NEXT to continue.');
					submitBtns[1].removeAttribute('disabled');
				});
				//
				return true;
			}
		};
		//
		var uploadFile = function(file, fileData, callback){
			var ajaxData = new FormData(elem('form', null));//, {enctype: 'multipart/form-data'}
			//ajaxData.append('cmd', 'upload');
			ajaxData.append('file', fileData);//, file.name
			new ajax(base_url + 'register/upload-xlsx/', {
				method: 'POST',
				data: ajaxData,
				callback: function (data){
					callback(JSON.parse(data.responseText));
				}
			});
		}
		//
		//	Skip
		submitBtns[0].onclick = function(){
			/*new ajax(base_url + 'register/add-employees/', {
				callback: function (data){
					document.location.hash = '#step-3';
				}
			});*/
			document.location.hash = '#step-3';
		};
		//
		//	Next
		submitBtns[1].onclick = function(){
			/*new ajax(base_url + 'register/add-employees/', {
				callback: function (data){
					document.location.hash = '#step-4';
				}
			});*/
			document.location.hash = '#completed';
		};
		//
		//	Next
		submitBtns[2].onclick = function(){
			popup.removeClass('active');
			setTimeout(function(){
				popup.style.display = 'none';
			}, 410);
		};
		//
		/*var form = context.q('form')[0];
		var ul = form.q('ul.fa-ul')[0];*/
		loaded();
	});

/*/
var rowRegentS3 = q('#select-domain-users table.progress-list tbody tr')[0];
rowRegentS3 = arcRead(rowRegentS3.parentNode.removeChild(rowRegentS3));
//*/
new navFrame('step-3', q('#select-domain-users')[0],
	function (context, params, e, loaded) {
		setStep(3);
		setLayout(context);
		exitWarning();
		document.body.className = 'initializing';
		q('div.loading')[0].style.display = 'block';
		//
		let remaining = q('span.remaining')[0];
		let seats = q('span.seats')[0];
		let submitBtns = q('input[type="submit"].btn-default.btn-blue');
		let activated = 0, remain = trialSeats;
		function deltaLicences(delta){
			activated += delta;
			remain = trialSeats - activated;
			remaining.innerHTML = remain;
			if (remain < 0){
				remaining.style.color = 'red';
				submitBtns[0].setAttribute('disabled', true);
				submitBtns[1].setAttribute('disabled', true);
			}
			else{
				remaining.style.color = '';
				submitBtns[0].removeAttribute('disabled');
				submitBtns[1].removeAttribute('disabled');
			}
		}
		seats.innerHTML = trialSeats;
		loadTrialSeats(function(plan, seats){
			seats.innerHTML = trialSeats;
			deltaLicences(0);
		});
		//
		// Reset search input
		context.q('input[name="search"]')[0].value = '';
		getUsersData(context,
			function (users, self) {
				var table = context.q('table.progress-list tbody')[0];
				var employees = [];
				var noResults = elem('tr', '<td colspan="4">No results found for the search</td>', {
					style: 'display:none; text-align:center;',
					class: 'no-results'
				});
				table.innerHTML = '';
				for (var i = 0; i < users.length; i++)
					if (users[i].status > 0 && users[i].role >= 0){
						selectRole.select.name = 'employee[' + users[i].id + ']';
						delete selectRole.select.content[0].option.selected;
						delete selectRole.select.content[1].option.selected;
						delete selectRole.select.content[2].option.selected;
						delete selectRole.select.content[3].option.selected;
						if (users[i].email == self) {
							selectRole.select.content[2].option.selected = true;
							selectRole.select.disabled = true;
							deltaLicences(1);
						}
						else {
							delete selectRole.select.disabled;
							//
							//	Limit user accounts after quota limit
							if (remain < 1){
								users[i].status = 0;
								users[i].role < -1;
							}
							//
							if (users[i].status == 0 || users[i].role < 0){
								selectRole.select.content[3].option.selected = true;
							}
							else{
								if (users[i].role == 70){
									selectRole.select.content[1].option.selected = true;
									deltaLicences(1);
								}
								else if (users[i].role == 80){
									selectRole.select.content[2].option.selected = true;
									deltaLicences(1);
								}
								else{
									selectRole.select.content[0].option.selected = true;
									deltaLicences(1);
								}
							}
							//
							selectRole.select.onchange = function(){
								let selectedOption = this.selectedOptions[0].innerHTML;
								let wasDisabled = this.parentNode.parentNode.className.indexOf('Disabled') > -1;
								if (wasDisabled && selectedOption != 'Disabled')
									deltaLicences(1);
								else if (!wasDisabled && selectedOption == 'Disabled')
									deltaLicences(-1);
								this.parentNode.parentNode.className = 'icon ' + selectedOption;
							};
						}
						//
						//*/
						var employee = arcReactor({
							tr: {
								'data-id': users[i].id,
								class: (users[i].status == 0 || users[i].role < 0 ? 'Disabled' : ''),
								content: [
									{
										td: {
											class: 'icon-col',
											content: typeof users[i].picture == 'undefined' || users[i].picture == null || users[i].picture == '' || users[i].picture.indexOf('/private/') > -1 ?
												'<i class="fa fa-user"></i>' : '<img src="' + users[i].picture + '" width="28" height="28" />'
										}
									},
									{
										td: {
											class: 'name-col',
											content: users[i].fullname
										}
									},
									{
										td: {
											class: 'email-col',
											content: users[i].email
										}
									},
									{
										td: {
											class: 'select-col',
											content: selectRole
										}
									},
								]
							}
						});
						/*/
						var employee = arcReactor({li: {
							class: 'icon'+(users[i].status == 0 ? ' Disabled' : ''), 'data-id': users[i].id,
							content: [
								{span: {class: 'icon', content: typeof users[i].picture == 'undefined' || users[i].picture.indexOf('/private/') > -1 || users[i].picture == '' ?
									'<i class="fa fa-user"></i>' : '<img src="'+users[i].picture+'" width="28" height="28" />'}},
								{span: {content: users[i].fullname}},
								selectRole,
								{small: {content: '['+users[i].email+']'}}
							]
						}});
						employee = arcReact(users[i], rowRegentS3);
						//*/
						table.appendChild(employee);
						employees.push(employee);
					}
				table.appendChild(noResults);
				new enableSearchList(context, employees, noResults);
				//
				var form = context.q('form')[0];
				var submitLatch = false;
				form.onsubmit = function () {
					if (submitLatch)
						return false;
					submitLatch = true;
					scrollToMiddle(0);
					new ajax(base_url + 'register/update-employees/', {
						method: 'POST',
						data: this,
						callback: function (data) {
							document.location.hash = '#step-4';
						},
						fallback: function () {
							alert('There was a server error. Please retry in few minutes.');
							submitLatch = false;
						}
					});
					return false;
				}
				loaded();
				q('div.loading')[0].style.display = 'none';
				//setLayout(context);
			}, params);
	});

new navFrame('step-4', q('#assign-to-managers')[0],
	function (context, params, e, loaded) {
		setStep(4);
		setLayout(context);
		exitWarning();
		document.body.className = 'initializing';
		var setMultiple = context.q('#set-multiple')[0];
		var setMultipleConfirm = context.q('#select-all-confirm')[0];
		var selectedCount = context.q('#selected-count')[0];
		//
		// Reset search input
		context.q('input[name="search"]')[0].value = '';

		getUsersData(context,
			function (users, self) {
				var table = context.q('table.progress-list tbody')[0];
				var employees = [];
				var noResults = elem('tr', '<td colspan="4">No results found for the search</td>', {style: 'display:none; text-align:center;', class: 'no-results'});
				table.innerHTML = '';
				//
				//	First, find out the managers
				//	Now, create dropdown with managers listed on them
				var managers = {
					select: {
						class: 'form-control',
						content: [{
							option: {
								value: 52,
								content: 'Select manager'
							}
						}]
					}
				};
				setMultiple.innerHTML = '<option value="*">Select manager</option>';
				for (var i = 0; i < users.length; i++)
					if (users[i].role > 69) {
						managers.select.content.push({
							option: {
								value: users[i].id,
								content: users[i].fullname
							}
						});
						setMultiple.appendChild(elem('option', users[i].fullname, {
							value: users[i].id
						}));
					}
				//
				//	Generate UI
				for (var i = 0; i < users.length; i++)
					if (users[i].role > 0) {
						managers.select['data-id'] = users[i].id;
						/*/
						var employee = arcReactor({li: {
							class: 'icon', 'data-id': users[i].id,
							content: [
								{label: {content: [
										{input: {type: 'checkbox',
											onclick: function(){
												this.parentNode.parentNode.className = 'icon'+(this.checked ? ' selected' : '');
												countSelected();
											}}},
										{span: {class: 'icon', content: typeof users[i].picture == 'undefined' || users[i].picture.indexOf('/private/') > -1 || users[i].picture == '' ? '<i class="fa fa-user"></i>' : '<img src="'+users[i].picture+'" width="28" height="28" />'}},
										{span: {content: ' ' + users[i].fullname}}
									]}},
								managers,
								{small: {content: '['+users[i].email+']'}}
							]
						}});
						//*/
						var selectCheckbox = function (checkbox) {
							checkbox.parentNode.parentNode.className = 'icon' + (checkbox.checked ? ' selected' : '');
							countSelected();
						}
						var employee = arcReactor({
							tr: {
								'data-id': users[i].id,
								content: [
									{
										td: {
											class: 'chk-col',
											content: {
												input: {
													class: 'emp-check',
													type: 'checkbox',
													onclick: function () {
														selectCheckbox(this);
													}
												}
											}
										}
									},
									{
										td: {
											class: 'icon-col',
											content: typeof users[i].picture == 'undefined' || users[i].picture == null || users[i].picture == '' || users[i].picture.indexOf('/private/') > -1 ?
												'<i class="fa fa-user"></i>' : '<img src="' + users[i].picture + '" width="28" height="28" />'
										}
									},
									{
										td: {
											class: 'name-col',
											content: users[i].fullname
										}
									},
									{
										td: {
											class: 'email-col',
											content: users[i].email
										}
									},
									{
										td: {
											class: 'select-col',
											content: managers
										}
									}
								]
							}
						});
						var opts = employee.q('.select-col option');
						for (var j = 0; j < opts.length; j++) {
							if (opts[j].value == users[i].id) {
								opts[j].disabled = true;
								opts[j].setAttribute("disabled", "disabled");
							} else {
								opts[j].disabled = false;
								opts[j].removeAttribute("disabled");
							}
						}
						table.appendChild(employee);
						employees.push(employee);
					}
				table.appendChild(noResults);
				// Toggle checkbox on username/icon click
				var empRows = table.q('tr');
				for (var r = 0; r < empRows.length; r++) {
					if (typeof empRows[r].dataset.id != 'undefined') {
						(function () {
							var rChkBoxCol = empRows[r].q('td')[0],
								rChkBox = rChkBoxCol.q('input')[0];
							empRows[r].q('td')[2].onclick = empRows[r].q('td')[1].onclick = function () {
								rChkBox.checked = !rChkBox.checked;
								selectCheckbox(rChkBox);
							}
						}());
					}
				}
				new enableSearchList(context, employees, noResults);
				countSelected();
				loaded();
			}, params);
		//
		var form = context.q('form')[0];
		//
		//	Select all checkbox
		context.q('#select-all')[0].onclick = function () {
			var chkBxs = form.q('input[type="checkbox"].emp-check');
			for (var i = 0; i < chkBxs.length; i++)
				if (chkBxs[i].parentNode.parentNode.style.display != 'none') {
					chkBxs[i].checked = this.checked;
					chkBxs[i].parentNode.parentNode.className = 'icon' + (this.checked ? ' selected' : '');
				}
			countSelected();
		};
		//
		//	Confirm button to select manager for multiple selected employees
		setMultipleConfirm.onclick = function(){
			if (setMultiple.value != '*'){
				var selects = form.q('select');
				for (var i = 0; i < selects.length; i++)
					if (selects[i].parentNode.parentNode.q('input[type="checkbox"]')[0].checked) {
						if (selects[i].parentNode.parentNode.style.display != 'none' && selects[i].parentNode.parentNode.getAttribute('data-id') != setMultiple.value)
							selects[i].value = setMultiple.value;
						selects[i].parentNode.parentNode.q('input[type="checkbox"]')[0].checked = false;
						selects[i].parentNode.parentNode.className = 'icon';
					}
				selectedCount.innerHTML = '0';
				setMultiple.value = '*';
				this.disabled = true;
				context.q('#select-all')[0].checked = false;
			}
			else{
				setMultiple.focus();
				return false;
			}
		};
		//
		//	Display the number of selected employees
		var countSelected = function(){
			var chkBxs = form.q('input[type="checkbox"].emp-check');
			var count = 0;
			for (var i = 0; i < chkBxs.length; i++)
				if (chkBxs[i].checked)
					count += 1;
			selectedCount.innerHTML = count;
			if (count > 0) {
				setMultipleConfirm.disabled = false;
			}
			else {
				setMultipleConfirm.disabled = true;
			}
		};
		//
		//	Lock form submitting once submitted
		var submitLatch = false;
		form.onsubmit = function(){
			if (submitLatch)
				return false;
			submitLatch = true;
			//
			var data = {};
			var elements = this.q('#assign-to-managers form select');
			for (var i = 0; i < elements.length; i++)
				data[elements[i].getAttribute('data-id')] = elements[i].value;
			//
			new ajax(base_url + 'register/assign-managers/', {
				method: 'POST',
				data: data,
				callback: function (data) {
					document.location.hash = '#completed';
				},
				fallback: function () {
					alert('There was an error while assigning employees to managers. However you can do this later from the settings tab.');
					submitLatch = false;
					document.location.hash = '#completed';
				}
			});
			return false;
		};
	}
);

new navFrame('completed', q('#completed')[0],
	function (context, params) {
		steps[0].parentNode.parentNode.style.height = '0px';
	}
);

document.location.hash = '#step-' + step;
window.onpopstate();



function navigateIfNotAlreadyThere(check, navTo) {
	if (document.location.hash != check) {
		document.location.hash = navTo;
		setTimeout(function () {
			window.onpopstate();
		}, 250);
		return false;
	}
	return true;
}

var useSameResponse = false,
	useSameResponseData = false;

function getUsersData(context, callback, params) {
	var callbackInner = function (data) {
		//
		//	If right data is not there, retry in 2.5 seconds
		if (typeof data.users == 'undefined'){
			new (function(context, callback, params){
				setTimeout(function(){
					getUsersData(context, callback, params);
				}, 2500);
			})(context, callback, params);
			return false;
		}
		//
		//	Shadow the same callback response for 250 milliseconds
		useSameResponseData = data;
		useSameResponse = true;
		setTimeout('useSameResponse = false;', 250);
		//
		callback(data.users, data.self, data);
		setLayout(context);
	}
	if (useSameResponse)
		callbackInner(useSameResponseData);
	else
		new ajax(base_url + 'register/get-accounts/', {
			callback: function (data) {
				callbackInner(eval('(' + data.responseText + ')'));
			}
		});
}

var selectRole = {
	select: {
		class: 'form-control',
		content: [
			{
				option: {
					value: 2,
					content: 'Employee'
				}
			},
			//{option: {value: 1, content: 'Contractor'}},
			//{option: {value: 6, content: 'Tech Support'}},
			{
				option: {
					value: 70,
					content: 'Manager'
				}
			},
			{
				option: {
					value: 80,
					content: 'Administrator'
				}
			},
			//{option: {value: -2, content: 'System'}},
			{
				option: {
					value: -1,
					content: 'Disabled'
				}
			}
		],
		onchange: function () {
			this.parentNode.parentNode.className = 'icon ' + this.selectedOptions[0].innerHTML;
		}
	}
};

function enableSearchList(context, employees, noResults) {
	var resetSearch = context.q('i.fa-times-circle')[0];
	var search = context.q('input[name="search"]')[0];
	search.onkeyup = function () {
		noResults.style.display = 'table-row';
		for (var i = 0; i < employees.length; i++)
			if (employees[i].q('.name-col')[0].innerHTML.toLowerCase().indexOf(this.value.toLowerCase()) > -1 ||
					employees[i].q('.email-col')[0].innerHTML.toLowerCase().indexOf(this.value.toLowerCase()) > -1) { //.replace(/(<([^>]+)>)/ig, '')
				employees[i].style.display = 'table-row';
				noResults.style.display = 'none';
			}
			else
				employees[i].style.display = 'none';
	}
	resetSearch.onclick = function(){
		search.value = '';
		search.onkeyup();
	}
}

var lastScrollPos = 0,
	scrollTimeout;

function scrollToMiddle(top) {
	document.body.scrollTop = (top + 4 * document.body.scrollTop) / 5;
	if (Math.abs(document.body.scrollTop - top) < 3 || lastScrollPos == document.body.scrollTop) {
		document.body.scrollTop = top;
		return true;
	}
	lastScrollPos = document.body.scrollTop;
	clearTimeout(scrollTimeout);
	scrollTimeout = setTimeout(
		function () {
			scrollToMiddle(top);
		},
		20
	);
	//requestAnimationFrame(scrollToMiddle);
}
