
function loadDashboard(context, params){
	var correlationsWidget = new CorrelationsWidget();
	var employees = context.q('ul.employee-list.subs')[0];
	employees.innerHTML = '';
	if (params.length > 0){
		document.forms[0].to_date.value = params[0];
		document.forms[0].from_date.value = params[0];
		document.getElementById('date-filter').value = 'custom';
	}
	//	Show Prodoscore Alerts
	correlationsWidget.init(context.q('div.corrWidget')[0]);
	//setTimeout(function(){calcAlerts(processedData, context);}, 10);	//	Asynchronous
	//
	doFetchData('dashboard',
		function(data, self, startD, endD){
			employees.innerHTML = '';
			for (var i in data.employees){
				if (data.employees[i].role > 0 && data.employees[i].status == 1 && typeof data.employees[i].scr != 'undefined' && data.employees[i].scr){
					var icon = usernameIcon(data.employees[i].fullname);
					var className = strataStr(data.employees[i].strata);//data.employees[i].status > 0 &&
					//if ( (self.role == 80 && data.employees[i].role == 70 ) || (data.employees[i].manager == uid) ){
					if ( (self.role == 80 && data.employees[i].role == 70 ) || data.employees[i].manager == uid ){// || (typeof data.employees[ data.employees[i].manager ] != 'undefined' && data.employees[ data.employees[i].manager ].manager == uid)
						/*/
						var employee = arcReact(data[i], templateSchema['dashboard']);
						/*/
						var employee = arcReactor(
	{li: {content:
		{a: {'data-id': i, 'data-role': data.employees[i].role, href: '#dashboard/'+i+'/'+data.employees[i].email, style: 'border-left:2px solid #'+icon[1],
			content: [
				{
					div: {class: 'user-name-wrap',
						content: {p: {class: 'user-name', content: data.employees[i].fullname}}
					}
				},
				{
					div: {class: 'right-holder',
						content: [
							{p: {class: 'text '+className[0], content: className[1]}},
							{p: {class: 'score '+className[0], content: data.employees[i].scr.l.toFixed(0)}}//, title: 'OWS: '+data.employees[i].scr.g.toFixed(2)+'\nRBS: '+data.employees[i].scr.r.toFixed(2)+'\nLRS: '+data.employees[i].scr.l.toFixed(2)

						]
					}
				}
			],
			onclick: function(){
				var hasSubordinates = false;
				for (var i in data.employees)
					if (data.employees[i].role > 0 && data.employees[i].status == 1 && data.employees[i].manager == this.getAttribute('data-id')){
						hasSubordinates = true;
						break;
					}
				if (hasSubordinates)//self.role == 80
					document.location.hash = 'employees/under/'+this.getAttribute('data-id')+'/'+data.employees[this.getAttribute('data-id')].email;
				else
					document.location.hash = 'employee/'+this.getAttribute('data-id')+'/'+data.employees[this.getAttribute('data-id')].email;
				return false;
			}
		}}
	}});
						//*/
						employees.appendChild(employee);
					}
				}
			}
			//
			//	-----------------------------------------------------------------------------------------------
			var graphData = [['Bin', 'Employees', 'list']], bin, empCount = 0, histogramColors;
			var binSize = 10;
			histogramColors = [];
			//
			for (var i = 0; i < 100/binSize; i++){
				graphData.push([ (i*binSize + ' - ' + (i+1)*binSize), 0, [] ]);
				histogramColors.push((i+0.5)*binSize < 30 ? '#f1433c' : ((i+0.5)*binSize < 70 ? '#494949' : '#1e86d9'));
			}
			for (var i in data.employees)
				if (data.employees[i].role > 0 && typeof data.employees[i].scr != 'undefined' && data.employees[i].scr){
					bin = Math.floor(data.employees[i].scr.l / binSize);
					//
					if (bin+1 >= graphData.length)
						bin = graphData.length-2;
					graphData[bin+1][1] += 1;
					graphData[bin+1][2].push(data.employees[i].fullname);
				}
			//
			var graphData2 = [['Date', prevPeriodTxt(), 'Score']];//'Previous period'
			var days = Object.keys(data.days);
			//
			for (var j  = startD; j < endD+1; j++){
				var date = DateFromShort(j);
				//
				if (typeof data.days[j] == 'undefined')
					graphData2.push([date]);
				else if ( isWorkingDay(date.getDay()) )
					graphData2.push([date, data.days[j].score_prev ? Math.round(data.days[j].score_prev) : null, Math.round(data.days[j].score)]);
				else
					graphData2.push([date]);
			}
			//
			var mainStatus = context.q('#org-score, #org-delta');
			mainStatus[0].innerHTML = data.organization.score.toFixed(0);
			mainStatus[0].parentNode.className = strataStr(data.organization.score_strata)[0];
			//
			if (!data.organization.score_delta){
				mainStatus[1].parentNode.className = 'no-change';
				mainStatus[1].parentNode.innerHTML = '<span id="org-delta">-</span>';
			}
			else if (data.organization.score_delta < 0){
				mainStatus[1].parentNode.className = 'decrease';
				mainStatus[1].parentNode.innerHTML = '<span id="org-delta">'+Math.abs(data.organization.score_delta.toFixed(0))+'</span>% Performance decrease';
			}
			else{
				mainStatus[1].parentNode.className = 'increase';
				mainStatus[1].parentNode.innerHTML = '<span id="org-delta">'+Math.abs(data.organization.score_delta.toFixed(0))+'</span>% Performance increase';
			}
			//
			setTimeout(
				function(){
					drawChartDashboardProdoScore(context.q('#chart_div_org')[0], graphData2, data.prev_offset,
						function(date, col){
							date = new Date(date[0]);
							if (col == 1) {
								//	Previous Period
								date.setDate(date.getDate() - data.prev_offset);
							}

							//
							document.forms[0].to_date.value = date.sqlFormatted();
							document.forms[0].from_date.value = date.sqlFormatted();
							document.getElementById('date-filter').value = 'custom';
							window.onpopstate({forcePop: true});
						});
					//
					//	Histogram
					drawChartDashboardHistogram(context.q('#chart_histogram')[0], graphData, histogramColors,
						function(row, tooltip, e){
							var items = [], employees = row[2];
							for (var k = 0; k < employees.length; k++){
								if (k == 10){
									items.push('<li><b>+'+(employees.length-10)+' more</b></li>');
									break;
								}
								var icon = usernameIcon(employees[k]);
								items.push('<li><a style="border-left:2px solid #'+icon[1]+';">'+employees[k]+'</a></li>');
							}
							tooltip.innerHTML = '<h2 class="card-heading">Prodoscore '+row[0]+'<br/>'+employees.length+' employee'+(employees.length == 1 ? '' : 's')+'</h2>'+
								'<ul class="employee-list tooltip">'+items.join('')+'</ul>';
						});
				}, 50);
			//	-----------------------------------------------------------------------------------------------
			//
			var meta = ['below', 'baseline', 'above'], detailBox, tmp;
			for (var i = 0; i < 3; i++){
				detailBox = contentMain.q('div.detail-box.'+meta[i])[0];
				tmp = detailBox.q('p.status')[0];
				if (data.organization.strata_delta[i] === false){
					tmp.innerHTML = '0%<span><br/> No Change</span>';//'-<span></span>'
					tmp.className = 'status same';
				}
				else if (data.organization.strata_delta[i] == 0){
					tmp.innerHTML = '0%<span><br/> No Change</span>';
					tmp.className = 'status same';
				}
				else{
					tmp.innerHTML = Math.abs(data.organization.strata_delta[i])+'%'+(data.organization.strata_delta[i] > 0 ? '<span><br/> Increase</span>' : '<span><br/> Decrease</span>');
					tmp.className = data.organization.strata_delta[i] > 0 ? 'status increase' : 'status decrease';
				}
				detailBox.q('p.user_number')[0].innerHTML = data.organization.strata[i].length+'<span><br>Employee'+(data.organization.strata[i].length == 1 ? '' : 's')+'</span>';
			}


			// correlation widget
			correlationsWidget.loadData(document.forms[0].from_date.value, document.forms[0].to_date.value);

		});
}

function loadStrataPage(context, params){
	var employees = context.q('ul.employee-list.strata')[0];
	employees.innerHTML = '';
	doFetchData('strata',
		function(data, self, startD, endD){
			var description = contentMain.q('h3.heading')[params].innerHTML.replace('Productivity is ', '');
			/*{var head_txt = context.q('h2.card-heading')[0].innerHTML;
			context.q('h2.card-heading')[0].innerHTML = head_txt.replace('with a', 'with an');}*/
			var headings = context.q('h2.card-heading span');
			headings[0].innerHTML = (description[0] == 'a' ? 'n ' : ' ') + description.toLowerCase();
			headings[1].innerHTML = description.capitalizeFirstLetter();
			//
			employees.innerHTML = '';
			//
			for (var i in data.employees){
				if (data.employees[i].status > 0 && data.employees[i].role > 0){
					var icon = usernameIcon(data.employees[i].fullname);
					var className = strataStr(data.employees[i].strata);
					if (data.employees[i].strata == 2-params){
						var employee = arcReactor(
	{li: {content:
		{a: {'data-id': i,
			'data-role': data.employees[i].role,
			href: '#employee/'+i+'/'+data.employees[i].email,//((self.role == 80) || (data.employees[i].manager == uid)) ?  : '#'
			style: 'border-left:2px solid #'+icon[1],
			content: [
				{
					div: {class: 'user-name-wrap',
						content: {p: {class: 'user-name', content: data.employees[i].fullname}}
					}
				},
				{
					div: {class: 'right-holder',
						content: [
							{p: {class: 'score '+className[0], content: data.employees[i].scr.l.toFixed(0)}},//, title: 'OWS: '+data.employees[i].scr.g.toFixed(2)+'\nRBS: '+data.employees[i].scr.r.toFixed(2)+'\nLRS: '+data.employees[i].scr.l.toFixed(2)
							{p: {class: 'text '+className[0], content: className[1]}}
						]
					}
				}
			]
			/*,onclick: function(){
				if ((self.role == 80) || (data.employees[this.getAttribute('data-id')].manager == uid)){
					// Proceed
				}else
					return false;
			}*/
		}}
	}});
						employees.appendChild(employee);
					}
				}
			}
			//	-----------------------------------------------------------------------------------------------
			var graphData = [['Date', '# Employees']];
			for (var i  = startD; i < endD+1; i++){
				var date = DateFromShort(i);
				if (isWorkingDay(date.getDay()) && typeof data.days[i] != 'undefined'){
					graphData.push([date, data.days[i].strata[2-params].length]);
				}
				else
					graphData.push([date]);
			}
			drawChartStrata(context.q('#chart_strata')[0], graphData, ['#1e86d9', '#494949', '#f1433c'][params],
				function(row, tooltip, e){
					var tmp = data.days[DateToShort(row[0])].strata[2-params];
					var employees = [],
							limit = 10,
							extra = 0;
					for (var k = 0, l=0; k < tmp.length; k++) {
						if (data.employees[tmp[k]].status > 0 && data.employees[tmp[k]].role > 0){
							if (l >= limit) {
								extra++;
							} else {
								var icon = usernameIcon(data.employees[tmp[k]].fullname);
								employees.push('<li><a style="border-left:2px solid #'+icon[1]+';">'+data.employees[tmp[k]].fullname+'</a></li>');
							}
							l++;
						}
					}
					if ( extra > 0 ) {
						employees.push('<li><a>+'+extra+' more</a></li>');
					}
					tooltip.innerHTML = '<h2 class="card-heading">'+row[0].toString().substring(0, 15)+'<br/>'+tmp.length+' Employee'+(tmp.length == 1 ? '' : 's')+'</h2><ul class="employee-list tooltip">'+employees.join('')+'</ul>';
				});
		});
}

/*/
var alertsCached = ['', false];
function calcAlerts(data, context){
	var alerts = context.q('div#alerts')[0];
	var today = DateToShort(new Date());
	//
	var makeAlertsList = function(data, name, list, title){
		var dom = elem('div', '<h4 class="card-heading"><x class="fax-times">&#xd7;</x>'+title+'</h4><ul class="employee-list alerts"></ul>', {class: 'card-wrap', style: 'margin-bottom:12px;'});
		var h2 = dom.q('.card-heading')[0];
		var close = dom.q('x.fax-times')[0];
		var alert = dom.q('ul')[0];
		for (var i = 0; i < list.length; i++){
			var icon = usernameIcon(data.employees[ list[i][0] ].fullname);
			alert.appendChild(elem('li', '<a href="#'+(name == 'newEmployees' ? 'settings/employees/o/' : 'employee/')+list[i][0]+'" style="border-left:2px solid #'+icon[1]+'"><div class="user-name-wrap ellipsis"><p class="user-name">'+data.employees[ list[i][0] ].fullname+'</p></div>'+
								'<div class="right-holder"><p class="score below">'+
								( list[i][1] == '-' ? '-' : list[i][1].toFixed(0) )+
								'</p></div></a>'));
		}
		var closeHover = false;
		close.onmouseover = function(){
			closeHover = true;
		};
		close.onmouseout = function(){
			closeHover = false;
		};
		close.onclick = function(){
			dom.style.transition = 'all 0.5s';
			localStorage['alerts-'+name+'-closed-on'] = today;
			setTimeout(function(){
				dom.style.opacity = 0;
				dom.style.height = '0px';
				dom.style.marginBottom = '0px';
			}, 10);
			setTimeout(function(){
				dom.parentNode.removeChild(dom);
			}, 510);
		};
		h2.onclick = function(){
			if (closeHover)
				return false;
			if (dom.style.height == '47px'){
				dom.style.transition = '';
				dom.style.height = 'auto';
				var height = dom.offsetHeight;
				dom.style.height = '47px';
				dom.style.transition = 'height 0.5s';
				setTimeout(function(){
					dom.style.height = height+'px';
				}, 10);
			}
			else
				dom.style.height = '47px';
		};
		dom.style.height = '47px';
		return dom;
	}
	var checkIfNotClosed = function(name){
		if (typeof localStorage['alerts-'+name+'-closed-on'] == 'undefined')
			return true;
		if (localStorage['alerts-'+name+'-closed-on'] - today == 0)
			return false;
	}
	var period = dateFilter.options[dateFilter.selectedIndex].innerHTML.toLowerCase();
	startD = DateToShort(document.forms[0].from_date.value);
	endD = DateToShort(document.forms[0].to_date.value);
	//
	if (alertsCached[0] != startD+'-'+endD){
		new ajax(base_url+'dashboard-alerts-ajax/?fromdate='+DateFromShort(startD).sqlFormatted()+'&todate='+DateFromShort(endD).sqlFormatted(), {
			callback: function(data){
				data = eval('('+data.responseText+')');
				alertsCached = [startD+'-'+endD, {'responseText': data.responseText}];
				alerts.innerHTML = '';
				//
				if (checkIfNotClosed('newEmployees') && data.new_emp.length > 0 && organizationSettings.alert_new_employee == 1)
					alerts.appendChild(new makeAlertsList(data, 'newEmployees', data.new_emp, '<span class="triangle">'+data.new_emp.length+'</span> new employee'+(data.new_emp.length == 1 ? '' : 's')+'.'));
				//
				if (checkIfNotClosed('inactives') && data.inact.length > 0 && organizationSettings.alert_inactive_period == 1)
					alerts.appendChild(new makeAlertsList(data, 'inactives', data.inact, '<span class="triangle">'+data.inact.length+'</span> inactive for '+(period=='custom' ? 'period' : period)+'.'));
				//
				if (checkIfNotClosed('yesterdayInactives') && data.yester_inact.length > 0 && organizationSettings.alert_inactive_yesterday == 1)
					alerts.appendChild(new makeAlertsList(data, 'yesterdayInactives', data.yester_inact, '<span class="triangle">'+data.yester_inact.length+'</span> inactive yesterday.'));
				//
				if (checkIfNotClosed('belows') && data.below.length > 0 && organizationSettings.alert_below_period == 1)
					alerts.appendChild(new makeAlertsList(data, 'belows', data.below, '<span class="triangle">'+data.below.length+'</span> below average for '+(period=='custom' ? 'period' : period)+'.'));//the
				//
				if (checkIfNotClosed('yesterdayBelows') && data.yester_below.length > 0 && organizationSettings.alert_below_yesterday == 1)
					alerts.appendChild(new makeAlertsList(data, 'yesterdayBelows', data.yester_below, '<span class="triangle">'+data.yester_below.length+'</span> below average for yesterday.'));
			}
		});
	}
}
//*/
