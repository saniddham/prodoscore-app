import proapp.cron
import proapp.products.salesforce

from simple_salesforce.exceptions import SalesforceRefusedRequest, SalesforceMalformedRequest
import requests, datetime, json, dateutil.parser
from datetime import timedelta
from simple_salesforce import Salesforce

from json import loads, dumps
from collections import OrderedDict


# ========================================================================
# Gets Salesforce Interactions stats for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		employee			Employee object to get conference ID
# @params		date				Date to limit the data to
# @params		working_hours	Working hours for the organization
# @params		timezone_offset	Timezone offset for that date for the organization
# ========================================================================
def get_stats(employee, date, working_hours, timezoneOffset):
    details = {}
    score = 0
    date_str = date.strftime('%Y-%m-%d')

    interactions_check_date = (date - timedelta(days=1)).strftime('%Y-%m-%d')
    LastModifiedDate = interactions_check_date + 'T00:00:00.000+0000'
    try:
        response_data = proapp.products.salesforce.slaesforce_auth(employee.domain)
        sf = Salesforce(instance_url=response_data['instance_url'], session_id=response_data['access_token'])
    except KeyError:
        proapp.cron.log('[' + employee.email + ' slaesforce authentication failure', 'info')
        return {'score': -4, 'details': details}

    record_tasks = get_tasks(employee, sf, LastModifiedDate)
    if record_tasks == -1:
        return {'score': -4, 'details': details}
    elif len(record_tasks) > 0:
        res = cal_score(record_tasks, employee, date_str, working_hours, timezoneOffset, details, score, response_data, 'Task', sf)
        score = res['score']

    record_lead_modifications = get_lead_modifications(employee, sf, LastModifiedDate)
    if record_lead_modifications == -1:
        return {'score': -4, 'details': details}
    elif len(record_lead_modifications) > 0:
        res = cal_score(record_lead_modifications, employee, date_str,  working_hours, timezoneOffset, details, score, response_data,  'Lead', sf)
        score = res['score']

    records_opportunity_modifications = get_opportunity_modifications (employee, sf, LastModifiedDate)
    if records_opportunity_modifications == -1:
        return {'score': -4, 'details': details}
    elif len(records_opportunity_modifications) > 0:
        res = cal_score(records_opportunity_modifications, employee, date_str,  working_hours, timezoneOffset, details, score, response_data, 'Opportunity', sf)
        score = res['score']

    try:
        records_events = sf.query("SELECT id, EventSubType, LastModifiedById, Subject, Type, Location, Account.Name, Description, isRecurrence, RecurrenceType,  What.Name, What.Type, Who.Type,StartDateTime, EndDateTime, ActivityDate, CreatedDate, LastModifiedDate FROM Event WHERE LastModifiedDate > " + LastModifiedDate + " AND (isRecurrence=true OR RecurrenceActivityId='')  AND LastModifiedById = '%s'" % employee.crm_id)
        records_events=proapp.products.salesforce.get_more_records(sf, records_events)
        if len(records_events) > 0:
            res = cal_score(records_events, employee, date_str,  working_hours, timezoneOffset, details, score, response_data, 'Event', sf)
            score = res['score']
    except SalesforceMalformedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceMalformedRequest', 'info')
        return {'score': -4, 'details': details}
        pass
    except SalesforceRefusedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceRefusedRequest', 'info')
        return {'score': -4, 'details': details}
        pass

    return {'score': score, 'details': details}


def get_tasks(employee, sf, LastModifiedDate):
    records_interactions = []
    try:
        records_tasks = sf.query("SELECT id,  TaskSubtype, LastModifiedById, Subject, Status, Account.Name, Description, isRecurrence, RecurrenceType, CallType,  What.Name, What.Type, Who.Type, ActivityDate, CreatedDate, LastModifiedDate FROM Task WHERE LastModifiedDate > " + LastModifiedDate + " AND (isRecurrence=true OR RecurrenceActivityId='') AND LastModifiedById = '%s'" % employee.crm_id)
        records_interactions = proapp.products.salesforce.get_more_records(sf, records_tasks)
    except SalesforceMalformedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceMalformedRequest', 'info')
        return -1
    except SalesforceRefusedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceRefusedRequest', 'info')
        return -1
        pass

    return records_interactions


def get_lead_modifications(employee, sf, LastModifiedDate):
    records_interactions = []
    try:
        records_leads_modifications = sf.query("SELECT id, Email, LastModifiedDate, Status, LastModifiedById, Company, NumberOfEmployees, Title, LeadSource, CreatedDate, CreatedById, Name  FROM Lead WHERE LastModifiedDate > " + LastModifiedDate + " AND LastModifiedById = '%s'" % employee.crm_id)
        records_interactions = proapp.products.salesforce.get_more_records(sf, records_leads_modifications)
    except SalesforceMalformedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceMalformedRequest', 'info')
        return -1
        pass
    except SalesforceRefusedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceRefusedRequest', 'info')
        return -1
        pass

    return records_interactions


def get_opportunity_modifications(employee, sf, LastModifiedDate):
    records_interactions = []
    try:
        records_opportunity_modifications = sf.query("SELECT id, LastModifiedDate, LastModifiedById, LeadSource, Description, Probability, Amount, CreatedById, CreatedDate, Name,Type, Account.Name FROM Opportunity WHERE LastModifiedDate > " + LastModifiedDate + " AND LastModifiedById = '%s'" % employee.crm_id)
        records_interactions = proapp.products.salesforce.get_more_records(sf, records_opportunity_modifications)
    except SalesforceMalformedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceMalformedRequest', 'info')
        return -1
        pass
    except SalesforceRefusedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceRefusedRequest', 'info')
        return -1
        pass

    return records_interactions


def cal_score(records, employee, date_str,  working_hours, timezoneOffset, details, score , response_data, module, sf):
    for record in records:
        record = proapp.products.salesforce.to_dict(record)
        Activity_datetime = proapp.products.salesforce.get_datetime(record['LastModifiedDate'], timezoneOffset)
        Activity_time = Activity_datetime['activity_datetime']
        check_date = Activity_datetime['activity_date'][:10]
        detail_time = (Activity_time - datetime.datetime.strptime('00:00', '%H:%M')).seconds / 60
        record['Link'] = response_data['instance_url'] + '/' + record['Id']
        if module == 'Task':
            record['Type'] = record['What']['Type'] if record['What'] is not None else record['Who']['Type'] if record['Who'] is not None else ''
            if record['TaskSubtype'] == 'Call' or record['CallType'] is not None:
                dt_add_3_sec = dateutil.parser.parse(record['CreatedDate']) + datetime.timedelta(seconds=3)
                if dateutil.parser.parse(record['LastModifiedDate']) < dt_add_3_sec:
                    continue
            if record['TaskSubtype'] == 'Email':
                try:
                    records_mail = sf.query("SELECT Id , FromAddress, ActivityId, TextBody,Subject,MessageDate FROM EmailMessage WHERE ActivityId = '%s'" % record['Id'])
                    record_mail = records_mail['records']
                    if len(record_mail) > 0:
                        if record_mail[0]['FromAddress'] == employee.email:
                            if check_date == date_str and record['LastModifiedById'] == employee.crm_id:
                                details[str(record['Id'].encode('ascii', 'ignore'))] = [record['Subject'], record, 1, detail_time, detail_time]
                            continue
                except SalesforceMalformedRequest:
                    proapp.cron.log('[' + employee.email + ' SalesforceMalformedRequest', 'info')
                    return {'score': -4, 'details': details}
                    pass
                except SalesforceRefusedRequest:
                    proapp.cron.log('[' + employee.email + ' SalesforceRefusedRequest', 'info')
                    return {'score': -4, 'details': details}
                    pass

        elif module == 'Event':
            record['Type'] = record['What']['Type'] if record['What'] is not None else record['Who']['Type'] if record['Who'] is not None else ''
            record['Description'] = record['Description'] if record['Description'] is not None else ''
            record['Description'] = record['Description'] + " @ " + record['Location'] if record['Location'] is not None else record['Description']
        else:
            record['Type'] = module
            record['Subject'] = record['Name']
            if module == 'Lead':
                record['Description'] = record['Title'] if record['Title'] is not None else ''
            if record['LastModifiedDate'] <= record['CreatedDate']:
                continue
        if check_date == date_str and record['LastModifiedById'] == employee.crm_id:
            if proapp.cron.within(Activity_time, working_hours):
                details[str(record['Id'].encode('ascii', 'ignore'))] = [record['Subject'], record, 0, detail_time, detail_time]
                score = score + 1
            else:
                details[str(record['Id'].encode('ascii', 'ignore'))] = [record['Subject'], record, 1, detail_time, detail_time]

    return {'score': score, 'details': details}