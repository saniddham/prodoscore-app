from simple_salesforce import Salesforce

import proapp.cron
import proapp.products.salesforce
from simple_salesforce.exceptions import SalesforceRefusedRequest, SalesforceMalformedRequest
import requests,datetime, json, dateutil.parser
from datetime import timedelta

from json import loads, dumps
from collections import OrderedDict

# ========================================================================
# Gets Salesforce Calls stats for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		employee			Employee object to get conference ID
# @params		date				Date to limit the data to
# @params		working_hours	Working hours for the organization
# @params		timezone_offset	Timezone offset for that date for the organization
# ========================================================================
def get_stats(employee, date, working_hours, timezoneOffset):
	details = {}
	score = 0
	date_str = date.strftime('%Y-%m-%d')
	calls_check_date = (date - timedelta(days=1)).strftime('%Y-%m-%d')
	try:
		response_data = proapp.products.salesforce.slaesforce_auth(employee.domain)
		sf = Salesforce(instance_url=response_data['instance_url'], session_id=response_data['access_token'])
	except KeyError:
		proapp.cron.log('[' + employee.email + ' slaesforce authentication failure', 'info')
		return {'score': -4, 'details': details}
	try:
		##################
		CreatedDate = calls_check_date+ 'T00:00:00.000+0000'
		records_calls = sf.query("SELECT id,  TaskSubtype, LastModifiedById, Subject, Status, Priority, Account.Name, Description, CallDurationInSeconds,CallType, isRecurrence, RecurrenceType,  What.Name, What.Type, Who.Type, ActivityDate, CreatedDate, CreatedById, LastModifiedDate FROM Task WHERE (TaskSubtype = 'Call' OR CallType != null) AND (isRecurrence = true OR RecurrenceActivityId = '') AND CreatedDate > " + CreatedDate + " AND CreatedById ='%s'" % employee.crm_id)
		records_calls = proapp.products.salesforce.get_more_records(sf, records_calls)
		if len(records_calls) > 0:
			for record in records_calls:
				record['Type'] = record['What']['Type'] if record['What'] is not None else record['Who']['Type'] if record['Who'] is not None else ''
				record['Link'] = response_data['instance_url'] + '/' + record['Id']
				record = proapp.products.salesforce.to_dict(record)
				Activity_datetime = proapp.products.salesforce.get_datetime(record['CreatedDate'], timezoneOffset)
				Activity_time = Activity_datetime['activity_datetime']
				check_date = Activity_datetime['activity_date'][:10]
				detail_time = ((Activity_time - datetime.datetime.strptime('00:00', '%H:%M')).seconds) / 60
				if record['CallDurationInSeconds'] is None:
					record['CallDurationInSeconds'] = 0
				if (check_date == date_str and record['CreatedById'] == employee.crm_id):
					if proapp.cron.within(Activity_time, working_hours) and record['CallDurationInSeconds'] >= 0:
						details[str(record['Id'].encode('ascii', 'ignore'))] = [record['Subject'], record, 0, detail_time,  round(detail_time + record['CallDurationInSeconds']/60, 0)]
						score = score + 1
					else:
						details[str(record['Id'].encode('ascii', 'ignore'))] = [record['Subject'], record, 1, detail_time,  round(detail_time + record['CallDurationInSeconds']/60, 0)]

	except SalesforceMalformedRequest:
		proapp.cron.log('[' + employee.email + ' SalesforceMalformedRequest', 'info')
		score = -4
		pass
	except SalesforceRefusedRequest:
		proapp.cron.log('[' + employee.email + ' SalesforceRefusedRequest', 'info')
		score = -4
		pass

	return {'score': score, 'details': details}

