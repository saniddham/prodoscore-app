import requests, proapp.cron
from ..models import Domain, Employee

import datetime, json, dateutil.parser, base64, time
from datetime import timedelta
from django.db import transaction
from django.conf import settings

token_url_rc = settings.VERSIONS['token_host'][settings.VERSIONS['this']] + "token/ringcentral/"


def get_stats(employee, date, working_hours, timezone_offset):
	domain = employee.domain

	details = {}
	conferences = {}
	score = 0


	# Request the token from the token service
	token_request = requests.get(token_url_rc + str(domain.id))
	rc_data = json.loads(token_request.text)
	admin_id = rc_data['owner_id']
	access_token = rc_data['access_token']
	# ##-# Uncoment Above and Coment Bellow
	# admin_id = 2082623011
	# access_token = "U0pDMDFQMDRQQVMwMHxBQUROSHM5TEhtMzJCMVVwZUFYbTN1RzNiNzRzLU81aU9Xd1ZLUGg4WjJVRjdkeGJ2RHlRZk4xSXpVbWhYM2ZaOEtFa2E1bGFHaXdKTkNlTDFsQV9wXzNXQzdVTG1BZ0o0QmNMZGk0VXhvbTZ5Z3Eyd0lPM1l4MmUxYThaR0cwTVdocTFRQ3lZOHd4eThoLUpQWmpESE9GSjZVUmFad3c1TVZXUEtKc3JpM0I4OUg1Z0s1YlBfZXhqMU1aMERwbDlGcC1aNVBuS3NnQnZ0Vzd6dVZ4cU42TDl8OEdSOXNRfHZiMVR0V3lxM2tja08wUVRjU3N4aUF8QUE"

	url = 'https://platform.ringcentral.com/restapi/v1.0/account/~/extension/'+employee.phone_system_id+'/call-log?view=Detailed&dateFrom='+date+'T00:00:00.000Z'
	##-# Uncoment Above and Coment Bellow
	# url = 'https://platform.ringcentral.com/restapi/v1.0/account/~/extension/'+str(2082623011)+'/call-log?view=Detailed&&withRecording=true&dateFrom='+'2019-01-01'+'T00:00:00.000Z'
	
	headers = {'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access_token}
	k = requests.get(url, headers=headers)
	response_data = k.json()

	##-# ##########################################
	# print('------------------------------------')
	# print(response_data)
	# print('------------------------------------')
	##-# ##########################################

	try:
		records = response_data['records']
		##-# ##########################################
		# print('------------------------------------')
		# print('len: ' + str(len(records)))
		# print('------------------------------------')
		##-# ##########################################
		if len(records) > 0:
			for record in records:
				##-# ##########################################
				# print('------------------------------------')
				# print(record)
				# print('------------------------------------')
				##-# ##########################################
				call_datetime = get_datetime(record['startTime'],timezone_offset)
				call_time = call_datetime['activity_datetime']
				check_date = call_datetime['activity_date'][:10]
				detail_time = ((call_time - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
				if record['direction'] == 'Inbound':
					call_title = get_title(record['from']) + ': Inbound'
				else:
					call_title = get_title(record['to']) + ': Outbound'
				record['duration'] /= 60.0
				if check_date == date:
					if proapp.cron.within(call_time, working_hours):
						if record['result'] in ['Missed', 'Call Failed', 'Hang Up']:
							details[record['id']] = [call_title, record, 1, detail_time, detail_time]
						else:
							details[record['id']] = [call_title, record, 0, detail_time, round(detail_time + record['duration'], 0)]
							score = score + record['duration']
					else:
						details[record['id']] = [call_title, record, 1, detail_time, round(detail_time + record['duration'], 0)]

				# if 'recording' in record:
				# 	recordingURL = record['recording']['contentUri'].split('/')
				# 	account_id = recordingURL[6]
				# 	recording_id = recordingURL[8]
				# 	conferences[record['id']] = {
				# 		'domain_id': domain.id,
				# 		'account_id': account_id,
				# 		'recording_id': recording_id
				# 	}
					##-# ##########################################
					# print('------------------------------------')
					# print(conferences)
					# print('------------------------------------')
					##-# ##########################################
	except KeyError:
		return {'score': round(score, 0), 'details': details}

	return {'score': round(score, 0), 'details': details, 'conferences': conferences}


def get_datetime(d,timezone_offset):
	format_code = "%Y-%m-%d %H:%M:%S"
	g_datetime = dateutil.parser.parse(d).strftime(format_code)
	dt = dateutil.parser.parse(g_datetime) + datetime.timedelta(hours=timezone_offset)
	activity_date = dt.strftime(format_code)

	return {'activity_date':  activity_date, 'activity_datetime':dt}


def get_title(record):
	if 'phoneNumber' in record:
		a = record['phoneNumber']
	else:
		a = record['extensionNumber']
	if 'name' in record:
		a = record['name'] + ' ('+a+')'

	return a
