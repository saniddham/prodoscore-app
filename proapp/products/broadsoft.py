
from ..models import Broadsoft_admin, Employee, Broadsoft_user, Broadsoft_event, Broadsoft_raw

import datetime, os, requests, json, proapp.cron
from datetime import timedelta

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

import math

# ========================================================================
# Gets BroadSoft stats for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		employee			Employee object to get conference ID
# @params		date				Date to limit the data to
# @params		working_hours		Working hours for the organization
# @params		timezone_offset		Timezone offset for that date for the organization
# ========================================================================
def get_stats(request, employee, date, working_hours, timezone_offset):
	details = {}
	score = 0
	missed_call = 'Missed call'
	#print('coming')
	#
	bsUser = Broadsoft_user.objects.filter(userid=employee.broadsoft_userid)
	#print (bsUser)
	if len(bsUser) == 0:
		return {'score': score, 'details': details}
	#
	fromdate = date - datetime.timedelta(days=1)
	todate = date + datetime.timedelta(days=2)
	bsEvents = Broadsoft_event.objects.filter(broadsoft_user=bsUser[0], received_on__range=(fromdate, todate))
	#print(bsEvents)
	for bsEvent in bsEvents:
		data = eval(bsEvent.data);
		try:
			if data['type'] == 'CallReleasedEvent':
				startTime = datetime.datetime.utcfromtimestamp(int(data['call']['startTime']) / 1000) + datetime.timedelta(hours=timezone_offset)
				start_detail = ((startTime - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds)/60.0
				releaseTime = datetime.datetime.utcfromtimestamp(int(data['call']['releaseTime']) / 1000) + datetime.timedelta(hours=timezone_offset)
				end_detail = ((releaseTime - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds)/60.0
				if 'callType' in data['call']['remoteParty']:
					if data['call']['remoteParty']['callType'] == 'Group':
						data['network'] = 'Internal'
					elif data['call']['remoteParty']['callType'] == 'Network':
						data['network'] = 'External'
					else:
						data['network'] = data['call']['remoteParty']['callType']
				if 'personality' in data['call']:
					if data['call']['personality'] == 'Originator':
						data['Direction'] = 'Outbound'
					elif data['call']['personality'] == 'Terminator':
						data['Direction'] = 'Inbound'
					else:
						data['Direction'] = data['call']['personality']

				if "answerTime" in data['call'] and "detachedTime" in data['call']:
					data['Status'] = 'voice mail'
				elif "answerTime" in data['call']:
					data['Status'] = 'Answered call'
				else:
					data['Status'] = missed_call

				if 'address' in data['call']['remoteParty']:
					address = data['call']['remoteParty']['address'] if type(data['call']['remoteParty']['address']) == type('String') else data['call']['remoteParty']['address']['$t']
				if 'address' in data['call']['remoteParty'] and 'name' in data['call']['remoteParty'] and isinstance(data['call']['remoteParty']['name'], basestring):
					data['call']['remoteParty']['name'] = data['call']['remoteParty']['name'] + ' ('+address+') - '+data['call']['remoteParty']['callType']
				elif 'address' in data['call']['remoteParty']:
					data['call']['remoteParty']['name'] = address+' - '+data['call']['remoteParty']['callType']
				elif 'name' not in data['remoteParty']:
					data['call']['remoteParty']['name'] = json.dumps(data['call']['remoteParty'])

				if startTime.date() == date.date() and data['call']['extTrackingId'] not in details:
					#
					if data['Status'] != missed_call:
						flag = 1
						duration = 0
						if proapp.cron.within(startTime, working_hours) and proapp.cron.within(releaseTime, working_hours):
							duration = (releaseTime - startTime).seconds
							flag = 0
							score += duration
						elif proapp.cron.within(startTime, working_hours) and not proapp.cron.within(releaseTime, working_hours):
							releaseTime = datetime.datetime.strptime(
								(str(working_hours[1][0]) + ":" + str(working_hours[1][1])), '%H:%M')
							duration = (releaseTime - startTime).seconds
							flag = 0
							score += duration
						elif proapp.cron.within(releaseTime, working_hours) and not proapp.cron.within(startTime, working_hours):
							startTime = datetime.datetime.strptime((str(working_hours[0][0]) + ":" + str(working_hours[0][1])), '%H:%M')
							duration = (releaseTime - startTime).seconds
							flag = 0
							score += duration
						else:
							duration = (releaseTime - startTime).seconds
							flag = 1
						data['duration'] = duration / 60.0
						details[ data['call']['extTrackingId'] ] = [data['call']['remoteParty']['name'].split("-")[0] + "*" + data['Direction'], data, flag, math.floor(start_detail), math.floor(end_detail)]
					else:
						details[ data['call']['extTrackingId'] ] = [data['call']['remoteParty']['name'].split("-")[0] + "*" + data['Direction'], data, 1, math.floor(start_detail), math.floor(start_detail)]
		except KeyError:
			print ("this is a KeyError", data)
			pass
	#
	subscribe(request, employee.broadsoft_admin, employee.broadsoft_userid)
	return {'score': round((score / 60.0), 0), 'details': details}

# ========================================================================
# Subscribe an endpoint to receive BroadSoft events
# ========================================================================
def subscribe(request, grpAdmin, userId):

	import base64, xml.etree.ElementTree
	from xml.etree.ElementTree import ParseError
	# Get the timestamp of the last midnight and midnight of seven days ahead
	utcNow = datetime.datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0) - datetime.datetime(1970, 1, 1)
	utc6Plus = utcNow + timedelta(days=6)
	utcNow = int(str(utcNow.total_seconds()).split('.')[0])
	utc6Plus = int(str(utc6Plus.total_seconds()).split('.')[0])
	#
	# Find BroadSoft userid record
	bsUser = Broadsoft_user.objects.filter(userid=userId)
	if len(bsUser) == 0:
		# Create a new BroadSoft userid record
		bsUser = Broadsoft_user(userid=userId, subscriptionexpire=utc6Plus, admin=grpAdmin.id)
		doSubscribe = True
	else:
		bsUser = bsUser[0]
		bsUser.admin = grpAdmin.id
		# Check if there is more time till current subscription expires
		if bsUser.subscriptionexpire > utcNow:# - 86399
			doSubscribe = False
		else:
			bsUser.subscriptionexpire = utc6Plus
			doSubscribe = True
	#
	# Check if we are actually making the subscription
	if doSubscribe and grpAdmin.is_valid > -1:
		from django.conf import settings
		# Actually do subscribe if we are on production, not dev or QA
		#selfUrl = #request.build_absolute_uri('/').replace('https://', 'http://')
		result = ('<?xml version="1.0" encoding="UTF-8"?>'+
				'<Subscription xmlns="http://schema.broadsoft.com/xsi">'+
					'<event>Advanced Call</event>'+
					'<expires>'+str(utc6Plus+(86400/2))+'</expires>'+
					'<httpContact><uri>'+settings.VERSIONS['bs_cloudfunc'][ settings.VERSIONS['this'] ]+'</uri></httpContact>'+
					'<applicationId>Any</applicationId>'+
				'</Subscription>')
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			result = requests.post('https://xsp2.telesphere.com/com.broadsoft.xsi-events/v2.0/user/'+
				userId, result, headers={'Authorization': 'Basic '+base64.b64encode(grpAdmin.broadsoft_userid+':'+grpAdmin.broadsoft_password)})
			#
			# Parse subscription response and update record
			try:
				data = xml.etree.ElementTree.fromstring(result.content)
				result = {'userid': userId}#result.content
				for c1 in data:
					if c1.tag == '{http://schema.broadsoft.com/xsi}subscriptionId':
						bsUser.subscriptionid = c1.text
						result['subscriptionid'] = c1.text
					if c1.tag == '{http://schema.broadsoft.com/xsi}expires':
						bsUser.subscriptionexpire = utcNow + 86399 #utcNow + int(c1.text) - 86400
						result['expires'] = bsUser.subscriptionexpire
				#
				grpAdmin.is_valid = 1
				grpAdmin.save()
			#
			except ParseError:
				import logging
				logger = logging.getLogger(__name__)
				logger.warning('ParseError on: '+result.content)
				print('ParseError on: '+result.content)
				#
				grpAdmin.is_valid = -1
				grpAdmin.save()
				#
				# Send email
				from django.shortcuts import render
				res = render(request, 'email/broadsoft-credential-expire.html', {'xsi': grpAdmin.broadsoft_xsi, 'userid': grpAdmin.broadsoft_userid})
				postdata = {"from": settings.MAILGUN['FROM'], 'to': [],
						"subject": "Your Broadsoft admin credentials on Prodoscore seems to be expired.",
						"html": res}
				admins = Employee.objects.filter(domain=grpAdmin.domain, role=80, status=1)
				for admin in admins:
					if not (admin.report_email is None) and '@' in admin.report_email:
						postdata['to'].append(str(admin.report_email))
					elif '@' in admin.email:
						postdata['to'].append(str(admin.email))
				#
				postdata['bcc'] = ['support@prodoscore.com']
				requests.post(settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']), data=postdata)
				#
		bsUser.save()
	else:
		result = False
	#
	return result

# ========================================================================
# Endpoint to receive BroadSoft events as they happen
# ========================================================================
@csrf_exempt
def rx_events(request):
	import xml.etree.ElementTree
	#
	raw = Broadsoft_raw(data=request.body)
	raw.save()
	#headers = json.loads(request.META)
	print(request.META)
	# TO DO: Authenticate origin server from whatever found on the headers
	# No need - it seems; since we can verify by subscriptionID and userID to be consistant with an existing record in Broadsoft_user table
	#
	data = xml_to_json(xml.etree.ElementTree.fromstring(request.body))
	#return HttpResponse(json.dumps(data), content_type = 'application/json')
	#
	userId = data['userId']
	if 'targetId' in data:
		userId = data['targetId']
	bsUser = Broadsoft_user.objects.filter(userid=userId)#, subscriptionid=data['subscriptionId']
	if len(bsUser) == 0:
		print('subscription info inconsistant/mismatch - Received: '+userId+' - '+data['subscriptionId'])
	#	return HttpResponse('{"err": "subscription info inconsistant/mismatch"}', content_type = 'application/json')#, status = 500
	#
	# Check if we need to re subscribe
	if 'eventData' in data and 'type' in data['eventData'] and data['eventData']['type'] == 'xsi:SubscriptionTerminatedEvent':
		employee = Employee.objects.filter(broadsoft_userid=userId)
		if len(employee) > 0:
			subscribe(request, employee[0].broadsoft_admin, userId)
	#
	if 'call' in data['eventData']:
		try:
			data = Broadsoft_event(broadsoft_user=bsUser[0], received_on=datetime.datetime.now(), eventid=data['eventID'], data=data['eventData']['call'])
			data.save()
		except IndexError:
			print('IndexError :-(')
		return HttpResponse('{"ack": "data received and recorded. thanks!"}', content_type = 'application/json')#json.dumps(data)
	else:
		return HttpResponse('{"ack": "data received but unused. thanks!"}', content_type = 'application/json')


def xml_to_json(xmldata):
	output = {}
	if xmldata is None:
		return True
	elif type(xmldata) == type(True):
		return xmldata
	else:
		for c in xmldata:
			if c.text is None or c.text.strip() == '':
				output[c.tag.replace('{http://schema.broadsoft.com/xsi}', '')] = xml_to_json(c)
			else:
				output[c.tag.replace('{http://schema.broadsoft.com/xsi}', '')] = c.text
		for c in xmldata.attrib:
			output[c.replace('{http://www.w3.org/2001/XMLSchema-instance}', '')] = xmldata.attrib[c]
	return output
