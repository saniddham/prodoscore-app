import json
import uuid
from json import dumps

import requests

graph_endpoint = 'https://graph.microsoft.com/v1.0{0}'

# Generic API Sending
def make_api_call(method, url, token, payload = None, parameters = None):
    """
    Generic API Sending
        :param method: 
        :param url: 
        :param token: 
        :param payload=None: 
        :param parameters=None: 
    """
    # Send these headers with all API calls
    headers = { 'Authorization' : 'Bearer {0}'.format(token),
                'Accept' : 'application/json' }
    # Use these headers to instrument calls. Makes it easier
    # to correlate requests and responses in case of problems
    # and is a recommended best practice.
    request_id = str(uuid.uuid4())
    instrumentation = { 'client-request-id' : request_id,
                        'return-client-request-id' : 'true' }
    headers.update(instrumentation)
    response = None
    if (method.upper() == 'GET'):
        response = requests.get(url, headers = headers, params = parameters)
    elif (method.upper() == 'DELETE'):
        response = requests.delete(url, headers = headers, params = parameters)
    elif (method.upper() == 'PATCH'):
        headers.update({ 'Content-Type' : 'application/json' })
        response = requests.patch(url, headers = headers, data = json.dumps(payload), params = parameters)
    elif (method.upper() == 'POST'):
        headers.update({ 'Content-Type' : 'application/json' })
        response = requests.post(url, headers = headers, data = json.dumps(payload), params = parameters)
    return response

def get_me(access_token):
    """
    docstring here
        :param access_token: 
    """
    get_me_url = graph_endpoint.format('/me')
    # Use OData query parameters to control the results
    #  - Only return the displayName and mail fields
    query_parameters = {'$select': 'displayName,mail, onPremisesDomainName'}
    r = make_api_call('GET', get_me_url, access_token, "", parameters=query_parameters)
    if (r.status_code == requests.codes.ok):
        return r.json()
    else:
        return { 'failed': True, 'status_code' : r.status_code, 'text' : r.text }
    
    
def get_domain_access_token(domain):
    """
    docstring here
        :param domain: 
    """
    post_data = {
        'grant_type': 'client_credentials',
        'client_id': '72db758c-898e-44e1-9737-70c15626e430',
        'client_secret': 'v18WLu3_M0=md.+0C_oWJOD2p6wdE]uO',
        'scope': 'https://graph.microsoft.com/.default'
    }
    token_url = 'https://login.microsoftonline.com/{0}/oauth2/v2.0/token'
    return requests.post(token_url.format(domain), data=post_data)

def get_domain_users(domain):
    """
    docstring here
        :param domain: 
    """
    token = get_domain_access_token(domain)
    access_token = token.json()['access_token']
    get_org_users_url = graph_endpoint.format('/users')
    query_parameters = {
        '$select': 'businessPhones, displayName, givenName, id, jobTitle, mail, mobilePhone, officeLocation, preferredLanguage, surname, userPrincipalName, userType',
        '$orderby': 'displayName ASC',
        # '$top' : 2
        }
    result = make_api_call('GET', get_org_users_url, access_token, parameters = query_parameters).json()
    users = result.get('value', [])
    while '@odata.nextLink' in result:
        result = make_api_call('GET', result.get('@odata.nextLink'), access_token, parameters = {})
        users += result.get('value', [])
    print('No of Fetched items : \t',len(users))
    return users