
import proapp.cron, proapp.products.calendar
from ..models import Domain, Employee

import datetime, json, requests, dateutil.parser, time
from datetime import timedelta


def get_leads(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	details = {}
	score = 0
	#
	url = ('https://crm.zoho.com/crm/private/json/Leads/searchRecords?'+
		'authtoken='+domain.crm_token+'&scope=crmapi&criteria=(Created%20By:'+employee.crm_id+')&lastModifiedTime='+date+'%2000:00:00&fromIndex=1&toIndex=200')
	leads = requests.post(url).content
	#	If the response is empty, API limits for the day is probably exeeded. We have to 'Die another day'
	if leads == '':
		return {'score': -4, 'details': details}
	#
	leads = json.loads(leads)
	# =====================================================================
	# Check if we have a positive response
	# =====================================================================
	if 'response' in leads and 'result' in leads['response']:
		leads = leads['response']['result']['Leads']['row']
		# ==================================================================
		# Iterate through the leads
		# ==================================================================
		if 'FL' in leads:		# SOMETIMES the idiots at Zoho send just one row object instead an array of rows
			lead_data = parse_zrecord(leads)
			timeMts = parse_ztasks_time(lead_data, date, timezone_offset, working_hours)
			if timeMts != False:		# Within the given date
				if 'Company' not in lead_data:
					# Not B2B
					if 'First Name' in lead_data and 'Last Name' in lead_data:
						lead_data['Company'] = lead_data['First Name'] + ' ' + lead_data['Last Name']
					elif 'First Name' in lead_data:
						lead_data['Company'] = lead_data['First Name']
					elif 'Last Name' in lead_data:
						lead_data['Company'] = lead_data['Last Name']
					if 'Email' in lead_data:
						lead_data['Company'] += ' (' + lead_data['Email'] + ')'
				if timeMts['count']:
					details[lead_data['LEADID']] = [lead_data['Company'], lead_data, 0, timeMts['time'], timeMts['time']]
					score += 1
				else:
					details[lead_data['LEADID']] = [lead_data['Company'], lead_data, 1, timeMts['time'], timeMts['time']]
		else:
			for lead in leads:	# So we handle grapes and grapefruits like this - separately
				lead_data = parse_zrecord(lead)
				timeMts = parse_ztasks_time(lead_data, date, timezone_offset, working_hours)
				if timeMts != False:	# Within the given date
					if 'Company' not in lead_data:
						# Not B2B
						if 'First Name' in lead_data and 'Last Name' in lead_data:
							lead_data['Company'] = lead_data['First Name'] + ' ' + lead_data['Last Name']
						elif 'First Name' in lead_data:
							lead_data['Company'] = lead_data['First Name']
						elif 'Last Name' in lead_data:
							lead_data['Company'] = lead_data['Last Name']
						if 'Email' in lead_data:
							lead_data['Company'] += ' (' + lead_data['Email'] + ')'
					if timeMts['count']:
						details[lead_data['LEADID']] = [lead_data['Company'], lead_data, 0, timeMts['time'], timeMts['time']]
						score += 1
					else:
						details[lead_data['LEADID']] = [lead_data['Company'], lead_data, 1, timeMts['time'], timeMts['time']]

	elif 'response' in leads and 'nodata' in leads['response']:
		score = 0

	else:
		proapp.cron.log(url, 'error')
		proapp.cron.log(json.dumps(leads), 'error')
		return {'score': -4, 'details': {}, 'error': json.dumps(leads)}

	return {'score': score, 'details': details}


def get_invoices(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	details = {}
	score = 0
	#
	url = ('https://crm.zoho.com/crm/private/json/Invoices/searchRecords?'+
		'authtoken='+domain.crm_token+'&scope=crmapi&criteria=(Created%20By:'+employee.crm_id+')&lastModifiedTime='+date+'%2000:00:00&fromIndex=1&toIndex=200')
	invoices = requests.post(url).content
	#	If the response is empty, API limits for the day is probably exeeded. We have to 'Die another day'
	if invoices == '':
		return {'score': -4, 'details': details}
	#
	invoices = json.loads(invoices)
	# =====================================================================
	# Check if we have a positive response
	# =====================================================================
	if 'response' in invoices and 'result' in invoices['response']:
		invoices = invoices['response']['result']['Invoices']['row']
		# ==================================================================
		# Iterate through the calls
		# ==================================================================
		if 'FL' in invoices:		# SOMETIMES the idiots at Zoho send just one row object instead an array of rows
			invoice_data = parse_zrecord(invoices)
			timeMts = parse_ztasks_time(invoice_data, date, timezone_offset, working_hours)
			if timeMts != False:		# Within the given date
				if timeMts['count']:
					details[invoice_data['INVOICEID']] = [invoice_data['Subject'], invoice_data, 0, timeMts['time'], timeMts['time']]
					score += 1
				else:
					details[invoice_data['INVOICEID']] = [invoice_data['Subject'], invoice_data, 1, timeMts['time'], timeMts['time']]
		else:
			for invoice in invoices:	# So we handle grapes and grapefruits like this - separately
				invoice_data = parse_zrecord(invoice)
				timeMts = parse_ztasks_time(invoice_data, date, timezone_offset, working_hours)
				if timeMts != False:	# Within the given date
					if timeMts['count']:
						details[invoice_data['INVOICEID']] = [invoice_data['Subject'], invoice_data, 0, timeMts['time'], timeMts['time']]
						score += 1
					else:
						details[invoice_data['INVOICEID']] = [invoice_data['Subject'], invoice_data, 1, timeMts['time'], timeMts['time']]

	elif 'response' in invoices and 'nodata' in invoices['response']:
		score = 0

	else:
		proapp.cron.log(url, 'error')
		proapp.cron.log(json.dumps(invoices), 'error')
		return {'score': -4, 'details': {}, 'error': json.dumps(invoices)}

	return {'score': score, 'details': details}


def get_accounts(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	details = {}
	score = 0
	#
	url = ('https://crm.zoho.com/crm/private/json/Accounts/searchRecords?'+
		'authtoken='+domain.crm_token+'&scope=crmapi&criteria=(Modified%20By:'+employee.crm_id+')&lastModifiedTime='+date+'%2000:00:00&fromIndex=1&toIndex=200')
	accounts = requests.post(url).content
	#	If the response is empty, API limits for the day is probably exeeded. We have to 'Die another day'
	if accounts == '':
		return {'score': -4, 'details': details}
	#
	accounts = json.loads(accounts)
	# =====================================================================
	# Check if we have a positive response
	# =====================================================================
	if 'response' in accounts and 'result' in accounts['response']:
		accounts = accounts['response']['result']['Accounts']['row']
		# ==================================================================
		# Iterate through the calls
		# ==================================================================
		if 'FL' in accounts:		# SOMETIMES the idiots at Zoho send just one row object instead an array of rows
			account_data = parse_zrecord(accounts)
			timeMts = parse_ztasks_time(account_data, date, timezone_offset, working_hours)
			if timeMts != False:		# Within the given date
				if timeMts['count']:
					details[account_data['ACCOUNTID']] = [account_data['Account Name'], account_data, 0, timeMts['time'], timeMts['time']]
					score += 1
				else:
					details[account_data['ACCOUNTID']] = [account_data['Account Name'], account_data, 1, timeMts['time'], timeMts['time']]
		else:
			for account in accounts:	# So we handle grapes and grapefruits like this - separately
				account_data = parse_zrecord(account)
				timeMts = parse_ztasks_time(account_data, date, timezone_offset, working_hours)
				if timeMts != False:	# Within the given date
					if timeMts['count']:
						details[account_data['ACCOUNTID']] = [account_data['Account Name'], account_data, 0, timeMts['time'], timeMts['time']]
						score += 1
					else:
						details[account_data['ACCOUNTID']] = [account_data['Account Name'], account_data, 1, timeMts['time'], timeMts['time']]

	elif 'response' in accounts and 'nodata' in accounts['response']:
		score = 0

	else:
		proapp.cron.log(url, 'error')
		proapp.cron.log(json.dumps(accounts), 'error')
		return {'score': -4, 'details': {}, 'error': json.dumps(accounts)}

	return {'score': score, 'details': details}


# ========================================================================
# Gets Zoho stats for a given user for a given date - First Calls
# 	Returns score and details on which score is based on
#
# @param		employee			Employee object to get conference ID
# @params		date				Date to limit the data to
# @params		working_hours	Working hours for the organization
# @params		timezone_offset	Timezone offset for that date for the organization
# ========================================================================
def get_calls(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	details = {}
	all_calls = {}
	score = 0
	check_date = dateutil.parser.parse(date) - timedelta(days=1)
	check_date = check_date.strftime('%Y-%m-%d')
	#
	url = ('https://crm.zoho.com/crm/private/json/Calls/searchRecords?'+
		'authtoken='+domain.crm_token+'&scope=crmapi&criteria=(Call%20Owner:'+employee.crm_id+')&lastModifiedTime='+check_date+'%2000:00:00&fromIndex=1&toIndex=200')
	calls = requests.post(url).content
	#	If the response is empty, API limits for the day is probably exeeded. We have to 'Die another day'
	if calls == '':
		return {'score': -4, 'details': details}
	#
	calls = json.loads(calls)
	# =====================================================================
	# Check if we have a positive response
	# =====================================================================
	if 'response' in calls and 'result' in calls['response']:
		calls = calls['response']['result']['Calls']['row']
		# ==================================================================
		# Iterate through the calls
		# ==================================================================
		if 'FL' in calls:		# SOMETIMES the idiots at Zoho send just one row object instead an array of rows
			call_data = parse_zrecord(calls)
			timeMts = parse_zcalls_time(call_data, date, timezone_offset, working_hours)
			if timeMts != False:		# Within the given date
				if timeMts['count']:
					event_s = timeMts['event_s']
					event_e = timeMts['event_e']
					all_calls[event_s.strftime('%H:%M') + '-' + event_e.strftime('%H:%M')] = [event_s.time(),event_e.time()]
					details[call_data['ACTIVITYID']] = [call_data['Subject'], call_data, 0, timeMts['starttime'], timeMts['endtime']]
				else:
					details[call_data['ACTIVITYID']] = [call_data['Subject'], call_data, 1, timeMts['starttime'], timeMts['endtime']]
		else:
			for call in calls:	# So we handle grapes and grapefruits like this - separately
				call_data = parse_zrecord(call)
				timeMts = parse_zcalls_time(call_data, date, timezone_offset, working_hours)
				if timeMts != False:	# Within the given date
					if timeMts['count']:
						event_s = timeMts['event_s']
						event_e = timeMts['event_e']
						all_calls[event_s.strftime('%H:%M') + '-' + event_e.strftime('%H:%M')] = [event_s.time(),event_e.time()]
						details[call_data['ACTIVITYID']] = [call_data['Subject'], call_data, 0, timeMts['starttime'], timeMts['endtime']]
					else:
						details[call_data['ACTIVITYID']] = [call_data['Subject'], call_data, 1, timeMts['starttime'], timeMts['endtime']]

	elif 'response' in calls and 'nodata' in calls['response']:
		score = 0

	else:
		proapp.cron.log(url, 'error')
		proapp.cron.log(json.dumps(calls), 'error')
		return {'score': -4, 'details': {}, 'error': json.dumps(calls)}
	count_stats = proapp.products.calendar.get_NonOverlapping_duration(all_calls)

	return {'score': count_stats, 'details': details}

# ========================================================================
# Check if it is on the specified date and within working hours
# ========================================================================
def parse_zcalls_time(task_data, date, timezone_offset, working_hours):
	if 'Call Start Time' in task_data:
		startTime = dateutil.parser.parse(task_data['Call Start Time'])
		if 'Call Duration (in seconds)' in task_data:
			duration = int(task_data['Call Duration (in seconds)'])
		elif 'Call Duration' in task_data:	# Just in case 'in seconds' field is not found
			duration = task_data['Call Duration'].split(':')
			duration = int(duration[0]) + int(duration[1])
		else:
			duration = 0
		endtime = startTime + timedelta(seconds=duration)
		time_dic = check_time_range(startTime, date, endtime, working_hours)
		call_start_time = (startTime - datetime.datetime.strptime('00:00', '%H:%M')).seconds / 60.0
		call_end_time = (endtime - datetime.datetime.strptime('00:00', '%H:%M')).seconds / 60.0
		if time_dic != False:
			duration = round(((time_dic['event_e'] - time_dic['event_s']).seconds / 60.0), 0)
			return {"count": time_dic['count'],'event_s': time_dic['event_s'],'event_e': time_dic['event_e'], "starttime": call_start_time, "endtime": call_end_time, "duration": duration}
		else:
			return False
	else:
		# Not on the given date - ignore
		return False

# ========================================================================
# Zoho Events stats
# ========================================================================
def get_events(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	details = {}
	all_events = {}
	date_check= dateutil.parser.parse(date) - datetime.timedelta(days=14)
	date_check = date_check.strftime("%Y-%m-%d")
	score = 0
	#
	url = ('https://crm.zoho.com/crm/private/json/Events/searchRecords?'+
		'authtoken='+domain.crm_token+'&scope=crmapi&criteria=(Event%20Owner:'+employee.crm_id+')&lastModifiedTime='+date_check+'%2000:00:00&fromIndex=1&toIndex=200')
	events = requests.post(url).content
	#	If the response is empty, API limits for the day is probably exeeded. We have to 'Die another day'
	if events == '':
		return {'score': 0, 'details': details}
	#
	events = json.loads(events)
	if 'response' in events and 'result' in events['response']:
		events = events['response']['result']['Events']['row']
	else:
		proapp.cron.log(url, 'error')
		proapp.cron.log(json.dumps(events), 'error')
		return {'score': 0, 'details': {}, 'error': json.dumps(events)}
	from_index = 200
	count = 200
	# =====================================================================
	# Get record from all the pages
	# =====================================================================
	while (count == 200 ):
		events_array = []
		to_index = from_index + 200
		url = ('https://crm.zoho.com/crm/private/json/Events/searchRecords?' +
			   'authtoken=' + domain.crm_token + '&scope=crmapi&criteria=(Event%20Owner:'+employee.crm_id + ')&lastModifiedTime=' + date + '%2000:00:00&fromIndex='+str(from_index)+'&toIndex='+str(to_index))
		page_events = requests.post(url).content
		page_events = json.loads(page_events)
		if 'response' in page_events:
			if 'result' in page_events['response']:
				events_array = events_array + page_events['response']['result']['Events']['row']
				count = len(page_events['response']['result']['Events']['row'])
				from_index = from_index + count
			else:
				break
		else:
			break

	# =====================================================================
	# Check if we have a positive response
	# =====================================================================
	if 'FL' in events:		# SOMETIMES the idiots at Zoho send just one row object instead an array of rows
		event_data = parse_zrecord(events)
		timeMts = parse_zevents_time(event_data, date, timezone_offset, working_hours)
		if timeMts != False:		# Within the given date
			if timeMts['count']:
				details[event_data['ACTIVITYID']] = [event_data['Subject'], event_data, 0, timeMts['details_start_time'], timeMts['details_end_time']]
				score += (timeMts['details_end_time'] - timeMts['details_start_time'])
			#
			else:
				details[event_data['ACTIVITYID']] = [event_data['Subject'], event_data, 1, timeMts['details_start_time'], timeMts['details_end_time']]
	else:
		events = events + events_array
		# ==================================================================
		# Iterate through the events
		# ==================================================================
		for event in events:	# So we handle grapes and grapefruits like this - separately
			event_data = parse_zrecord(event)
			timeMts = parse_zevents_time(event_data, date, timezone_offset, working_hours)
			if timeMts != False:	# Within the given date
				if timeMts['count']:
					event_s = timeMts['starttime']
					event_e = timeMts['endtime']
					all_events[event_s.strftime('%H:%M') + '-' + event_e.strftime('%H:%M')] = [event_s.time(), event_e.time()]
					details[event_data['ACTIVITYID']] = [event_data['Subject'], event_data, 0, timeMts['details_start_time'], timeMts['details_end_time']]
				#
				else:
					details[event_data['ACTIVITYID']] = [event_data['Subject'], event_data, 1, timeMts['details_start_time'], timeMts['details_end_time']]

	count_stats = proapp.products.calendar.get_NonOverlapping_duration(all_events)
	return {'score': count_stats, 'details': details}

def get_working_hours_events(event_s, event_e, working_hours):
	if event_e.time() == event_s.time():
		return {'count': False, 'event_s': datetime.datetime.strptime('00:00','%H:%M'), 'event_e': datetime.datetime.strptime('23:59','%H:%M')}
	if proapp.cron.within(event_s, working_hours) and proapp.cron.within(event_e, working_hours):
		return {'count':True, 'event_s':event_s, 'event_e':event_e}
	elif proapp.cron.within(event_s, working_hours):
		event_e = datetime.datetime.strptime((str(working_hours[1][0])+':'+ str(working_hours[1][1])),'%H:%M')
		return {'count':True, 'event_s':event_s, 'event_e':event_e}
	elif proapp.cron.within(event_e, working_hours):
		event_s = datetime.datetime.strptime((str(working_hours[0][0])+':'+ str(working_hours[0][1])),'%H:%M')
		return {'count':True, 'event_s':event_s, 'event_e':event_e}
	else:
		return {'count':False, 'event_s':event_s, 'event_e': event_e}

# ========================================================================
# Check if it is on the specified date and within working hours
# ========================================================================
def parse_zevents_time(task_data, date, timezone_offset, working_hours):
	if 'Start DateTime' in task_data and 'End DateTime' in task_data:
		startDate =  dateutil.parser.parse(task_data['Start DateTime'])
		startDate_timestamp =  time.mktime(startDate.timetuple())
		endDate =  dateutil.parser.parse(task_data['End DateTime'])
		endDate_timestamp = time.mktime(endDate.timetuple())
		event_range = int(endDate_timestamp - startDate_timestamp)/60
		details_start_time = (startDate - datetime.datetime.strptime('00:00', '%H:%M')).seconds / 60.0
		details_end_time = (endDate - datetime.datetime.strptime('00:00', '%H:%M')).seconds / 60.0
		if event_range < 180:
			time_dic = check_time_range(startDate, date, endDate, working_hours)
			if time_dic != False:
				return {"count": time_dic['count'], "starttime": time_dic['event_s'], "endtime": time_dic['event_e'], 'details_start_time': details_start_time, 'details_end_time': details_end_time}
		else:
			return {"count": False, "starttime": startDate, "endtime": endDate,'details_start_time': details_start_time, 'details_end_time': details_end_time}
		# Outside working hours or longer than 3 hours span
	return False

def check_time_range(startDate,date,endDate,working_hours):
	today = dateutil.parser.parse(date)
	if startDate.date() == today.date() and endDate.date() == today.date():
		time_dic = get_working_hours_events(startDate, endDate, working_hours)
	elif startDate.date() == today.date():
		endDate = today.strftime('%Y-%m-%d') + ' ' + str(working_hours[1][0]) + ':' + str(working_hours[1][1]) + ':00'
		endDate = dateutil.parser.parse(endDate)
		time_dic = get_working_hours_events(startDate, endDate, working_hours)
	elif endDate.date() == today.date():
		# print (startDate, endDate)
		startDate = today.strftime('%Y-%m-%d') + ' ' + str(working_hours[0][0]) + ':' + str(working_hours[0][1]) + ':00'
		startDate = dateutil.parser.parse(startDate)
		time_dic = get_working_hours_events(startDate, endDate, working_hours)
	else:
		return False
	return time_dic

# ========================================================================
# Zoho Tasks stats
# ========================================================================
def get_tasks(employee, date, working_hours, timezone_offset):
	details = {}
	score = 0
	#	We need to call the inner function for Created, Modified and Closed tasks seperately
	ids = []
	for context in ['Modified']:#, 'Closed'
		res = get_ztasks_inner(context, employee, date, working_hours, timezone_offset, ids)
		if res['score'] == -4:		# API Quota exceeded - Retry another day
			return res
		elif res['score'] != -4:	# Iteration with non-negative response - merge details, add to score
			details.update(res['details'])
			score += res['score']
	return {'score': score, 'details': details}

def get_ztasks_inner(context, employee, date, working_hours, timezone_offset, ids):
	domain = employee.domain
	details = {}
	score = 0
	#
	url = ('https://crm.zoho.com/crm/private/json/Tasks/searchRecords?'+
		'authtoken='+domain.crm_token+'&scope=crmapi&criteria=('+context+'%20By:'+employee.crm_id+')&lastModifiedTime='+date+'%2000:00:00&fromIndex=1&toIndex=200')
	tasks = requests.post(url).content
	#	If the response is empty, API limits for the day is probably exeeded. We have to 'Die another day'
	if tasks == '':
		return {'score': -4, 'details': details}
	#
	tasks = json.loads(tasks)
	# =====================================================================
	# Check if we have a positive response
	# =====================================================================
	if 'response' in tasks and 'result' in tasks['response']:
		tasks = tasks['response']['result']['Tasks']['row']
		# ==================================================================
		# Iterate through the tasks
		# ==================================================================
		if 'FL' in tasks:		# SOMETIMES the idiots at Zoho send just one row object instead an array of rows
			task_data = parse_zrecord(tasks)
			timeMts = parse_ztasks_time(task_data, date, timezone_offset, working_hours)
			recuring_flag = 4 if 'Recurring Activity' in task_data and task_data['Recurring Activity'].split("*")[0] == 'daily' else 0 #if there is any reccuring task flag is 4
			if timeMts != False and task_data['ACTIVITYID'] not in ids:		# Within the given date and not counted before
				if timeMts['count'] and task_data['Status'] == 'Completed':
					details[task_data['ACTIVITYID']] = [task_data['Subject']+(' ('+task_data['Related To']+')' if 'Related To' in task_data else ''), task_data, 0, timeMts['time'], timeMts['time'], recuring_flag]
					score += 1
				else:
					details[task_data['ACTIVITYID']] = [task_data['Subject'] + (' (' + task_data['Related To'] + ')' if 'Related To' in task_data else ''), task_data, 1,timeMts['time'], timeMts['time'], recuring_flag]
				ids.append(task_data['ACTIVITYID'])
		else:
			for task in tasks:	# So we handle grapes and grapefruits like this - separately
				task_data = parse_zrecord(task)
				timeMts = parse_ztasks_time(task_data, date, timezone_offset, working_hours)
				recuring_flag = 4 if 'Recurring Activity' in task_data and task_data['Recurring Activity'].split("*")[0] == 'daily' else 0
				if timeMts != False and task_data['ACTIVITYID'] not in ids:	# Within the given date and not counted before
					if timeMts['count'] and task_data['Status'] == 'Completed':
						details[task_data['ACTIVITYID']] = [task_data['Subject'] + (' (' + task_data['Related To'] + ')' if 'Related To' in task_data else ''), task_data, 0,timeMts['time'], timeMts['time'], recuring_flag]
						score += 1
					else:
						details[task_data['ACTIVITYID']] = [task_data['Subject'] + (' (' + task_data['Related To'] + ')' if 'Related To' in task_data else ''), task_data, 1,timeMts['time'], timeMts['time'], recuring_flag]
					ids.append(task_data['ACTIVITYID'])

	elif 'response' in tasks and 'nodata' in tasks['response']:
		score = 0

	else:
		proapp.cron.log(url, 'error')
		proapp.cron.log(json.dumps(tasks), 'error')
		return {'score': -4, 'details': {}, 'error': json.dumps(tasks)}

	return {'score': score, 'details': details}

# ========================================================================
# Check if it is on the specified date and within working hours
# ========================================================================
def parse_ztasks_time(task_data, date, timezone_offset, working_hours):
	if 'Modified Time' in task_data:
		evtTime = task_data['Modified Time']
		evtTime = dateutil.parser.parse(evtTime)
		date = dateutil.parser.parse(date)
		if evtTime.date() == date.date():
			evtTimeMts = (evtTime - evtTime.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds() / 60
			if proapp.cron.within(evtTime, working_hours):
				return {"count": True, "time": evtTimeMts}  # Within working hours
			else:
				return {"count": False, "time": evtTimeMts}
		else:
			return False
	else:
		return False			# Not on the given date - ignore
															# Zoho time is always +8 hours from UT


# ========================================================================
# Iterate key value pairs and build a proper data structure / dict
# ========================================================================
def parse_zrecord(task):
	task_data = {}
	for i in range(0, len(task['FL'])):
		task_data[task['FL'][i]['val'].encode('ascii', 'ignore')] = task['FL'][i]['content'].encode('ascii', 'ignore')
	#
	return task_data
