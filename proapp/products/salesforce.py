
import proapp.cron
from ..models import Domain, Employee
import simple_salesforce
from simple_salesforce import Salesforce
from simple_salesforce.exceptions import SalesforceRefusedRequest, SalesforceMalformedRequest

import datetime
import requests,datetime, json, dateutil.parser
from datetime import timedelta

from json import loads, dumps
from collections import OrderedDict

# ========================================================================
# Gets Salesforce stats for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		employee			Employee object to get conference ID
# @params		date				Date to limit the data to
# @params		working_hours	Working hours for the organization
# @params		timezone_offset	Timezone offset for that date for the organization
# ========================================================================
def get_stats(employee, date, working_hours, timezoneOffset):
	details = {}
	score = 0
	date_str = date.strftime('%Y-%m-%d')
	opps_check_date = (date - timedelta(days=1)).strftime('%Y-%m-%d')
	try:
		response_data = slaesforce_auth(employee.domain)
		sf = Salesforce(instance_url=response_data['instance_url'], session_id=response_data['access_token'])
	except KeyError:
		proapp.cron.log('[' + employee.email + ' slaesforce authentication failure', 'info')
		return {'score': -4, 'details': details}
	try:
		CreatedDate = opps_check_date + 'T00:00:00.000+0000'
		records_opps = sf.query("SELECT id, LastModifiedDate, LastModifiedById, LeadSource, Description, Probability, Amount, LastViewedDate, CloseDate, StageName, CreatedById, CreatedDate, Name,Type, Account.Name FROM Opportunity WHERE CreatedDate > "+CreatedDate+ " AND CreatedById='%s'"%employee.crm_id)
		records_opps = get_more_records(sf,records_opps)
		if len(records_opps) > 0:
			for record in records_opps:
				record['AccountName'] = record['Account']['Name']
				record['Link'] = response_data['instance_url'] + '/' + record['Id']
				record = to_dict(record)
				Activity_datetime = get_datetime(record['CreatedDate'], timezoneOffset)
				Activity_time = Activity_datetime['activity_datetime']
				check_date = Activity_datetime['activity_date'][:10]
				detail_time = ((Activity_time - datetime.datetime.strptime('00:00', '%H:%M')).seconds) / 60
				if (check_date == date_str and record['CreatedById'] == employee.crm_id):
					if (proapp.cron.within(Activity_time, working_hours)):
						details[str(record['Id'].encode('ascii', 'ignore'))] = [record['Name'], record, 0, detail_time, detail_time]
						score = score + 1
					else:
						details[str(record['Id'].encode('ascii', 'ignore'))] = [record['Name'], record, 1, detail_time, detail_time]

	except SalesforceMalformedRequest:
		proapp.cron.log('[' + employee.email + ' SalesforceMalformedRequest', 'info')
		score = -4
		pass
	except SalesforceRefusedRequest:
		proapp.cron.log('[' + employee.email + ' SalesforceRefusedRequest', 'info')
		score = -4
		pass

	return {'score': score, 'details': details}



# ============================================================
# Get Salesforce SF
# ============================================================
def slaesforce_auth(domain):
	url = 'https://login.salesforce.com/services/oauth2/token'
	token_id = domain.crm_token
	payload = {'grant_type': 'password',
			   'client_id': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
			   'client_secret': 'xxxxxxxxxxxxxxxxxxxxxxxxx',
			   'username': domain.salesforce_userid,
			   'password': domain.salesforce_password + '{}'.format(token_id)
			   }

	headers = {'Accept-Charset': 'UTF-8'}
	r = requests.post(url, data=payload, headers=headers)
	response_data = r.json()

	return response_data

# ============================================================
# conver OrderedDict to normal dictionary
# ============================================================
def to_dict(input_ordered_dict):
    return loads(dumps(input_ordered_dict))

def get_datetime(d,timezone_offset):
	format_code = "%Y-%m-%d %H:%M:%S"
	g_datetime = dateutil.parser.parse(d).strftime(format_code)
	dt = dateutil.parser.parse(g_datetime) + datetime.timedelta(hours=timezone_offset)
	activity_date = dt.strftime(format_code)

	return {'activity_date':  activity_date, 'activity_datetime':dt}

def get_more_records(sf,all_records):
	records = []
	while True:
		if 'nextRecordsUrl' in all_records:
			next_page_url = all_records['nextRecordsUrl']
			records = records + all_records['records']
			all_records = sf.query_more(next_page_url, True)
		else:
			records = records + all_records['records']
			break
	return records