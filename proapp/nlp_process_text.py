import re
# import spacy



dummy_text = (
u"""
Linda wrote to me about Apple Inc based in San Fransisco.
And here is some text about Samsung Corp. Now, here is 
some more text about Apple and its products for customers in Norway
""")

SPECIFIC_WORDS = [
	'dd', 'mm', 'yyyy'
]

def romove_special_patterns(text_context):
	# remove special charactors and split
	text_context = re.sub(re.compile('[^A-Za-z0-9]+'), ' ', text_context)

	#remove words starting with digits and longer unusual words
	unusual_words = [ word for word in text_context.split() if (word[0].isdigit() or len(word) > 20)]
	for dig_word in unusual_words:
		text_context = text_context.replace(dig_word, '')

	return ' '.join(text_context.split())


def remove_specific_words(text_context):
	if text_context in SPECIFIC_WORDS:
		return ''
	else:
		return text_context


#
# def remove_stopwords(text_context):
# 	spacy_nlp = spacy.load('en_core_web_sm')
# 	doc = spacy_nlp(text_context)
# 	tokens = [token.text for token in doc if not token.is_stop]
#
# 	return ' '.join(tokens)
#
#
# def recognize_entities(text_context):
# 	text_context = remove_stopwords(text_context)
# 	spacy_nlp = spacy.load('en_core_web_sm')
# 	doc = spacy_nlp(u"%s" % text_context)
#
# 	for ent in doc.ents:
# 		entitiy_data = dict(
# 			type = ent.label_,
# 			type_id = ent.label,
# 			content = ent.text
# 		)
#
# 		print('ID:{}\t{}\t"{}"\t'.format(ent.text, ent.label_, ent.label, ))
#
# 	return entitiy_data
#


#
#
#
#
# ee = romove_special_patterns(dummy_text)
# text = recognize_entities(ee)
# rr = remove_specific_words(text)
# print "sw: %s" % rr
#
#

# def employee