
# Register your models here.
from collections import namedtuple

import os, logging, urllib, json, datetime, time
import base64, hashlib, uuid, requests

from django.conf import settings
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from googleapiclient.errors import HttpError

from httplib2 import Http
from datetime import timedelta

from .models import Employee, Onboard, Domain, Click, Feedback, Feedback_comment, Statistic
from django.db.models import Count

TaskFailure = namedtuple("TaskFailure", ['date', 'count'])

# Authorize the user to access or redirect to relevent page
def _authorize(request):
	# User not authenticated - authenticate
	if 'id' not in request.session.keys():
		return {'redirect': '/sign-in/', 'error': 'not logged in'}
	#
	# Employee not recognized
	employee_check = Employee.objects.filter(id=request.session['id'])
	#
	# Check if role => admin
	if employee_check[0].role < 80 or (employee_check[0].domain.title != 'prodoscore.com'):
		return {'redirect': '/limbo/', 'error': 'user underprevillaged'}
	#
	return {'user': employee_check[0]}


# Admin Dashboard - List domains
def index(request):
	from django.db.models import Count
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	apage = 'a'
	if 'page' in request.GET.keys():
		apage = request.GET['page']
	#
	pages = 'SELECT 1 AS id, substring(title, 1, 1) as firstl, COUNT(*) AS count FROM proapp_domain WHERE id IN (SELECT domain_id FROM proapp_onboard WHERE status > 6) GROUP BY firstl'
	pages = Domain.objects.raw(pages)
	#
	domainsWhere = ['id IN (SELECT domain_id FROM proapp_onboard WHERE status > 6)']
	if apage != '*':
		domainsWhere.append('SUBSTRING(title, 1, 1) = \''+apage+'\'')
	domains = Domain.objects.filter().extra(select={'last_login':'SELECT MAX(last_login) FROM proapp_employee WHERE domain_id = proapp_domain.id'}).extra(where=domainsWhere).order_by('title')
	#
	epoch = datetime.datetime.strptime('1970-01-02', "%Y-%m-%d")
	return render(request, 'admin/index.html', {'domains': domains, 'epoch': epoch, 'user': auth['user'],
			'page': 'domains', 'pages': pages, 'base_url': request.build_absolute_uri('/')[:-1]})


# Admin Dashboard - Impersonate
def impersonate(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	if 'domain_id' in request.GET:
		admins = Employee.objects.filter().extra(where=['role = 80 AND domain_id = '+request.GET['domain_id']])
	if len(admins) == 0:
		return HttpResponse('{"error": "no-admin-for-domain"}', content_type = 'application/json')
	#
	request.session['origin'] = admins[0].domain.origin
	request.session['domain'] = admins[0].domain.title
	request.session['email'] = admins[0].email
	return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+'/#dashboard')


# Admin Dashboard - List on-progress domains
def on_progress(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	onboards = Onboard.objects.filter(status__lt=7, status__gt=0)
	fromdate = datetime.datetime.utcnow() - timedelta(days = 100)
	return render(request, 'admin/on-progress.html', {'onboards': onboards, 'fromdate': fromdate.strftime('%Y-%m-%d'), 'user': auth['user'], 'page': 'on-progress', 'base_url': request.build_absolute_uri('/')[:-1]})


subscription_types = {0: 'Trial', 1: 'Paid', 2: 'Expired', 3: 'Suspended'}
# Admin Dashboard - List domains
def domain_details(request):
	import proapp.views
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	domain = Domain.objects.filter(id=request.GET['id'])[0]
	has_changes = False
	#
	if 'setsubscription' in request.GET:
		domain.subscription_type = int(request.GET['setsubscription'])
		has_changes = True
	#
	if 'setsubscriptionvol' in request.GET:
		domain.subscription_volume = None if request.GET['setsubscriptionvol'] == 'Infinity' else int(request.GET['setsubscriptionvol'])
		has_changes = True
	#
	if 'setnlpprocess' in request.GET:
		domain.nlp_enabled = int(request.GET['setnlpprocess'])
		has_changes = True
	#
	if 'setcrmmoduless' in request.GET:
		if domain.crm_enabled == 1 and request.GET['setcrmmoduless'] == '0':
			proapp.views.product_disconnect(domain, domain.crm_system, request, auth['user'])
		#
		domain.crm_enabled = int(request.GET['setcrmmoduless'])
		has_changes = True
	#
	if 'settelcoenabled' in request.GET:
		if domain.telco_enabled == 1 and request.GET['settelcoenabled'] == '0':
			proapp.views.product_disconnect(domain, domain.conf_system, request, auth['user'])
			proapp.views.product_disconnect(domain, domain.phone_system, request, auth['user'])
		#
		domain.telco_enabled = int(request.GET['settelcoenabled'])
		has_changes = True
	#
	if 'setorgbaselineenabled' in request.GET:
		if domain.orgbaseline_enabled == 1 and int(request.GET['setorgbaselineenabled']) == 0:
			from .models import Org_weight, Baseline
			import proapp.calc, proapp.cron
			#
			# Delete org baselines and weights
			recs = Org_weight.objects.filter(domain=domain)
			for rec in recs:
				cacheKey = 'prod-weight-'+str(domain.id)+'-'+proapp.calc.dorp_dic[rec.pcode]
				proapp.cache.delete(cacheKey)
				rec.delete()
			#
			# Delete org baselines
			recs = Baseline.objects.filter(domain=domain)
			for rec in recs:
				cacheKey = 'baseline-'+str(domain.id)+'-'+str(rec.role)+'-'+proapp.calc.dorp_dic[rec.pcode]
				proapp.cache.delete(cacheKey)
				rec.delete()
		#
		domain.orgbaseline_enabled = int(request.GET['setorgbaselineenabled'])
		has_changes = True
	#
	if has_changes:
		domain.save()
		return HttpResponseRedirect(request.build_absolute_uri('/')+'admin/domain-details/?id='+request.GET['id'])
	#
	onboard = Onboard.objects.filter(domain=domain)[0]
	managers = Employee.objects.filter(domain=domain, role__gt=69).count()
	employees = Employee.objects.filter(domain=domain, role__lt=70, role__gt=0).count()
	terminated = Employee.objects.filter(domain=domain, role__lt=0).count()
	notactivated = Employee.objects.filter(domain=domain, role=0).count()
	allactivated = Employee.objects.filter(domain=domain, role__gt=0).count()
	return render(request, 'admin/domain.html', {'domain': domain, 'onboard': onboard,
		'managers': managers, 'employees': employees, 'terminated': terminated, 'notactivated': notactivated, 'allactivated': allactivated, 'user': auth['user'],
		'page': 'domains' if onboard.status > 6 else 'on-progress', 'base_url': request.build_absolute_uri('/')[:-1]})


# Admin Dashboard - List users in domains
def domain_users(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	domain = Domain.objects.filter(id=request.GET['id'])[0]
	admins = Employee.objects.filter(level=1, email__contains='@', domain_id=request.GET['id'])#role = 80 AND 
	if len(admins) == 0:
		return HttpResponse('{"error": "no-admin-for-domain"}', content_type = 'application/json')

	from googleapiclient.discovery import build
	from oauth2client.client import HttpAccessTokenRefreshError
	from oauth2client.service_account import ServiceAccountCredentials
	# ========================================================================
	# Load Client Secret file for the service account authorization
	# ========================================================================
	CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secret.json')
	credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRETS, scopes=['https://www.googleapis.com/auth/admin.directory.user.readonly'])

	# =====================================================================
	# Establish authorization for the domain admin
	# =====================================================================
	delegated_credentials = credentials.create_delegated(admins[0].email)
	http_auth = delegated_credentials.authorize(Http())

	employees = Employee.objects.filter(domain_id=request.GET['id'])
	empDict = {}
	for employee in employees:
		empDict[employee.profileId] = employee.id
	# =====================================================================
	# Get the list of employees from the Google Directory API
	# =====================================================================
	users = []
	if domain.origin == 'office365':
		import proapp.l1_interface
		res = proapp.l1_interface.call(method = 'GET', path = '/office365/{0}/users'.format(domain.title))
		if res.status_code == 200 and 'error' in res.json() and res.json()['error'] == False:
			users = res.json()['users']
			output = []
			for user in users:
				output.append({
					'name': {'fullName': user['displayName']},
					'primaryEmail': user['mail'],
					'suspended': user['suspended'],
					'thumbnailPhotoUrl': ''
				})
			users = output
	else:
		nextPageToken = False
		try:
			# =====================================================================
			# Use the HTTP authentication and open Directory API service entry to the domain
			# =====================================================================
			service = build('admin', 'directory_v1', http=http_auth)

			while True:
				user100s = service.users().list(domain=domain.title, fields='users(id,isAdmin,name/fullName,primaryEmail,suspended,thumbnailPhotoUrl),nextPageToken',
					**({'pageToken': nextPageToken} if nextPageToken else {})).execute()
				nextPageToken = user100s.get('nextPageToken', False)
				user100s = user100s.get('users', [])
				for user in user100s:
					if user['id'] in empDict:
						user['id'] = empDict[ user['id'] ]
					#
					users.append(user)
				if not nextPageToken:
					break
		except HttpAccessTokenRefreshError:
			return HttpResponse('{"error": "no-access-to-api", "details": "HttpAccessTokenRefreshError"}', content_type = 'application/json')
		except HttpError:
			return HttpResponse('{"error": "no-access-to-api", "details": "HttpError"}', content_type = 'application/json')

	return render(request, 'admin/domain_users.html', {'domain': domain, 'user': auth['user'], 'users': users,
		'base_url': request.build_absolute_uri('/')[:-1], 'subscription_type': subscription_types[domain.subscription_type]})


# Admin Dashboard - Test email delivery
def test_emails(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	import proapp.emails
	domain = Domain.objects.filter(id=request.GET['domain'])
	proapp.emails.schedule_email_domain(domain[0], 'qa')
	return HttpResponseRedirect(request.build_absolute_uri('/')+'admin/domain-details/?id='+request.GET['domain'])


# Admin Dashboard - Recalculate Global Averages
def average_calc(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	if request.method == 'POST':
		from .models import Baseline
		for key in request.POST:
			tmp = key.split(':')
			if len(tmp) == 3:
				prod = tmp[0]
				role = tmp[1]
				id = tmp[2]
				if id == 'new':
					rec = Baseline.objects.filter(role=role, pcode=prod, domain_id=6)
					if len(rec) == 0:
						rec = Baseline(role=role, pcode=prod, baseline=request.POST[key], domain_id=6)
					else:
						rec = rec[0]
						rec.baseline = request.POST[key]
					rec.save()
				else:
					rec = Baseline.objects.filter(id=id)[0]
					rec.baseline = request.POST[key]
					rec.save()
	#
	import proapp.calc, proapp.cron
	averages = {}
	for role in proapp.cron.role_dic:
		if role > 0:
			averages[role] = {'title': proapp.cron.role_dic[role], 'products': {}}
			for pcode in proapp.calc.products:
				if role in proapp.calc.products[pcode][1]:
					averages[role]['products'][pcode] = proapp.calc.products[pcode][1][role] # {'title': proapp.calc.products[pcode][0], 'avg': proapp.calc.products[pcode][1][role]}
				else:
					averages[role]['products'][pcode] = 999
	#
	prods = {}
	for pcode in proapp.calc.products:
		prods[pcode] = proapp.calc.products[pcode][0]
	#
	todate = datetime.datetime.now()
	emp_scores_this = Statistic.objects.using('replica').exclude(score__lt=0).order_by('-id')[:1]
	if len(emp_scores_this) > 0:
		todate = datetime.datetime.strptime(emp_scores_this[0].date, '%Y-%m-%d')
	#
	fourmonths = todate - timedelta(days=180)
	calcAvgs = Statistic.objects.using('replica').raw(
		'SELECT pb.id, pe.role, code AS pcode, AVG(score) AS baseline '+
		'FROM proapp_statistic ps '+
		'LEFT JOIN proapp_employee pe ON pe.id = ps.employee_id '+
		'LEFT JOIN proapp_product pp ON pp.slug = product '+
		'LEFT JOIN proapp_baseline pb ON pb.role = pe.role AND pb.pcode = pp.code '+
		'WHERE score > 0 AND pe.role > 0 AND pb.domain_id = 6 AND date > \''+fourmonths.strftime('%Y-%m-%d')+'\' '+
		'GROUP BY pb.id, pe.role, code')
	calculated_avgs = {}
	for calcAvg in calcAvgs:
		if calcAvg.role not in calculated_avgs:
			calculated_avgs[ calcAvg.role ] = {}
		#
		calculated_avgs[ calcAvg.role ][ calcAvg.pcode ] = [calcAvg.id, float(str(calcAvg.baseline))]
	#
	return render(request, 'admin/average-calc.html', {
		'page': 'average-calc',
		'avgs': base64.b64encode(json.dumps({
			'averages': averages,
			'products': prods,
			'calc_avgs': calculated_avgs}))
	})


# Admin Dashboard - Employee Day Score calculation breakdown
def emp_day_score(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	import proapp.calc
	#	Fetch employee record
	emp = Employee.objects.using('replica').values('role', 'domain_id').get(id=request.GET['employee'])
	strdate = request.GET['date']
	date = datetime.datetime.strptime(strdate, '%Y-%m-%d')

	data = {}
	stats = Statistic.objects.using('replica').filter(date=strdate, employee_id=request.GET['employee']).values('product', 'score')
	weightTotal = 0
	wspTotal = 0
	for stat in stats:
		if stat['score'] < 0:
			pass
		else:
			data[ stat['product'] ] = {
				'score': stat['score'],
				'baseline': proapp.calc.get_baseline(emp['domain_id'], emp['role'], stat['product']),
				'weight': proapp.calc.get_prod_weight(emp['domain_id'], stat['product'])
			}
			data[ stat['product'] ]['Sp'] = 1 - (data[ stat['product'] ]['baseline'] / (data[ stat['product'] ]['baseline'] + (stat['score']*5)))
			data[ stat['product'] ]['WSp'] = data[ stat['product'] ]['Sp'] * data[ stat['product'] ]['weight']
			weightTotal += data[ stat['product'] ]['weight']
			wspTotal += data[ stat['product'] ]['WSp']

	return render(request, 'admin/emp-day-score.html', {
		'page': 'emp-day-score',
		'stats': stats,
		'data': data,
		'weightTotal': weightTotal,
		'wspTotal': wspTotal,
		'Prodoscore': 100 * wspTotal / weightTotal
	})


# Admin Dashboard - Explore memcached
def memcached(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		from google.appengine.api import memcache
	else:
		from django.core.cache import cache
	#
	if 'delete' in request.GET:
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			data = memcache.delete(request.GET['delete'])
		return render(request, 'admin/memcached.html', {'key': request.GET['delete'], 'shortdate': '', 'identifier': '', 'keytype': ''})
	#
	if 'key' in request.GET:
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			data = memcache.get(request.GET['key'])
			return render(request, 'admin/memcached.html', {'key': request.GET['key'], 'shortdate': request.GET['shortdate'], 'identifier': request.GET['identifier'], 'keytype': request.GET['keytype'], 'data': json.dumps(data)})
		else:
			data = cache.get(request.GET['key'])
			return render(request, 'admin/memcached.html', {'key': request.GET['key'], 'shortdate': request.GET['shortdate'], 'identifier': request.GET['identifier'], 'keytype': request.GET['keytype'], 'data': json.dumps(data)})
	else:
		return render(request, 'admin/memcached.html', {'key': '', 'shortdate': '', 'identifier': '', 'keytype': ''})


# Admin Dashboard - User feedback and sugesstions
def feedback(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	if 'status' in request.GET:
		status = request.GET['status']
	else:
		status = 'new'
	#
	pages = 'SELECT 1 AS id, substring(received_on, 1, 7) as month, COUNT(*) AS count FROM proapp_feedback GROUP BY month'
	pages = Domain.objects.raw(pages)
	#
	apage = pages[len(list(pages))-1].month
	if 'page' in request.GET.keys():
		apage = request.GET['page']
	#
	feedbacks = Feedback.objects.filter(status=status).order_by('-received_on')
	if apage != '*':
		feedbacks = feedbacks.extra(where=['substring(received_on, 1, 7) = \''+apage+'\''])
	#
	return render(request, 'admin/feedback.html', {'feedbacks': feedbacks, 'user': auth['user'], 'status': status,
				'page': 'feedback', 'pages': pages, 'base_url': request.build_absolute_uri('/')[:-1]})


# Admin Dashboard - User feedback details
def feedback_details(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	feedback = Feedback.objects.filter(id=request.GET['id'])
	if len(feedback) == 0:
		return HttpResponseRedirect(request.build_absolute_uri('/')+'admin/feedback/')
	else:
		feedback = feedback[0]
	#
	# If a comment is posted, add it
	if 'comment' in request.POST:
		comment = request.POST['comment']
		#
		# If status changed, record that on the original feedback
		if request.POST['status'] != feedback.status:
			comment = feedback.status+' -> '+request.POST['status']+'<br/>'+comment
			feedback.status = request.POST['status']
			feedback.save()
		#
		comment = Feedback_comment(feedback=feedback, employee=auth['user'], comment=comment)
		comment.save()
		#
		# Send email follow-up to originator	-	RECONSIDER IMPLEMENTATION !!!
		#res = render(request, 'email/feedback-followup.html', {'comment': comment, 'employee': feedback.employee, 'description': comment.comment})
		#requests.post(
		#	settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']),
		#	data={"from": settings.MAILGUN['FROM'],
		#		"to": [feedback.employee.email],
		#		"subject": "We have received your feedback. Thank you.!~",
		#		"html": res})
		#
	comments = Feedback_comment.objects.filter(feedback=feedback)
	return render(request, 'admin/feedback-details.html', {'feedback': feedback, 'comments': comments, 'user': auth['user'], 'page': 'feedback', 'base_url': request.build_absolute_uri('/')[:-1]})


# Admin Dashboard - Usage statistics
def usage(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	utcnow = datetime.datetime.utcnow()
	todate = int((utcnow - datetime.datetime(1970, 1, 1)).total_seconds())
	fromdate = todate - (7*24*3600)
	#
	stats = Click.objects.filter(date__range=(fromdate*1000, todate*1000)).values('page').annotate(count=Count('page')).order_by('-count')
	return render(request, 'admin/usage.html', {'stats': stats, 'user': auth['user'], 'page': 'usage', 'base_url': request.build_absolute_uri('/')[:-1]})


# Admin Dashboard - Usage statistics
def usage_details(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	utcnow = datetime.datetime.utcnow()
	todate = int((utcnow - datetime.datetime(1970, 1, 1)).total_seconds())
	fromdate = todate - (7*24*3600)
	#
	innerPage = request.GET['page']
	stats = Click.objects.filter(date__range=(fromdate*1000, todate*1000), page=innerPage)
	return render(request, 'admin/usage-details.html', {'stats': stats, 'user': auth['user'], 'innerPage': innerPage, 'page': 'usage', 'base_url': request.build_absolute_uri('/')[:-1]})


# Upload modified static files to cloud storage
def synccdn(request):
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	from googleapiclient.discovery import build
	from oauth2client.service_account import ServiceAccountCredentials

	CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secret.json')
	credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRETS, scopes=[
			'https://www.googleapis.com/auth/cloud-platform',
			'https://www.googleapis.com/auth/devstorage.full_control',
			'https://www.googleapis.com/auth/devstorage.read_write'])
	http_auth = credentials.authorize(Http())
	storage = build('storage', 'v1', http=http_auth)

	data = []
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		processDir(os.path.dirname(os.path.dirname(__file__))+'/static/static/', '', data, 0, storage)
	else:
		lastSyncTimestamp = 0
		try:
			timestampFile = open('./static/sync.timestamp', 'r')
			lastSyncTimestamp = float(timestampFile.read())
			timestampFile.close()
		except:
			pass
		#
		processDir(os.path.dirname(os.path.dirname(__file__))+'/static/static/', '', data, lastSyncTimestamp, storage)
		#
		timestampFile = open('./static/sync.timestamp', 'w')
		timestampFile.write(str(time.time()))
		timestampFile.close()
	#
	return render(request, 'admin/synccdn.html', {'data': data})

def processDir(dir, path, data, lastSyncTimestamp, storage):
	for file in os.listdir(os.path.join(dir, path)):
		if os.path.isdir(os.path.join(dir, path, file)):
			processDir(dir, path+file+'/', data, lastSyncTimestamp, storage)
		else:
			fileTimestamp = os.path.getmtime(os.path.join(dir, path, file))
			if fileTimestamp > lastSyncTimestamp:
				syncFile(dir, path+file, storage)
				data.append(path+file)

def syncFile(dir, file, storage):
	import mimetypes
	mime = mimetypes.guess_type(dir+file)[0]
	#
	if mime is None:
		print(' Unknown mime type --> '+dir+' : '+file)
		return ''
	else:
		filePtr = open(dir+file, 'r')
		filedata = filePtr.read()
		filePtr.close()
		#
		resp = storage.objects().insert(bucket=settings.VERSIONS['static_buckets'][ settings.VERSIONS['this'] ], name=file, body='', predefinedAcl='publicRead')
		resp.uri = resp.uri.replace('/storage/v1/', '/upload/storage/v1/')
		resp.uri = resp.uri.replace('?alt=json&', '?alt=json&uploadType=media&')
		resp.headers['content-type'] = mime
		resp.headers['content-length'] = len(filedata)
		resp.body = filedata
		#
		resp = resp.execute()
	#
	return resp


# Admin Dashboard - Task Queue errors
def get_failed_tasks(request):
	auth = _authorize(request)

	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	not_processed_tasks = Statistic.objects.filter(score=-1).order_by('date').values('date').annotate(count=Count('score'))

	results = [TaskFailure(date=result['date'], count=result['count']) for result in not_processed_tasks]
	return render(request, 'admin/taskq.html', {'page' : 'taskq','results':results, 'user': auth['user']})

# Admin Dashboard - Task Queue error details
def failed_task_details(request):
	auth = _authorize(request)

	if 'user' not in auth and 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	today = datetime.datetime.now()
	days30ago = (today - timedelta(days=31)).strftime('%Y-%m-%d')
	tasks = Statistic.objects.using('replica').filter(score__in=[-2, -5], date__gt=days30ago).values('date', 'product').annotate(count=Count('*'))
	output = {}
	products = []
	for task in tasks:
		if task['date'] not in output:
			output[ task['date'] ] = {}
		if task['product'] not in products:
			products.append(task['product'])
		output[ task['date'] ][ task['product'] ] = task['count']

	output = sorted(output.items(), reverse=True)#, key=operator.itemgetter(0)

	return HttpResponse(json.dumps({'stats': output, 'prods': sorted(products)}), content_type = 'application/json')

