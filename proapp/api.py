
import os, requests, json, datetime, time, base64, operator, math, hashlib

from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Q

from httplib2 import Http
from datetime import timedelta

from .models import Onboard, Domain, Employee, Statistic, Detail, Notification, Feedback, Click
from .nlpmodels import Nlp_entity2detail, Nlp_entity, Nlp_entity_attribs
import proapp.cron, proapp.calc, proapp.cache

perpage = 15

# ========================================================================
# Authorize given user request to access the system or redirect to relevent page
# ========================================================================
def _authorize(request):

	# =====================================================================
	# SESSION - User not authenticated - authenticate
	# =====================================================================
	if 'email' not in request.session.keys():
		return {'error': 'not logged in'}

	# =====================================================================
	# USER RECORD - Employee not recognized
	# =====================================================================
	employee_check = Employee.objects.filter(email=request.session['email'])
	if len(employee_check) == 0:
		return {'error': 'user not recognized'}
	else:
		# ONBOARD - RECORD
		onboard = Onboard.objects.filter(domain=employee_check[0].domain)
		if len(onboard) == 0 or onboard[0].status < 7:
			return {'error': 'onboard incomplete'}

	# =====================================================================
	# SUBORDINATES - Check if role => manager or has subordinates
	# =====================================================================
	manager_check = Employee.objects.filter(manager_id=employee_check[0].id)
	if len(manager_check) == 0 and employee_check[0].role < 70:
		return {'error': 'not a manager'}

	proapp.cron.log('User: '+employee_check[0].email+' ['+str(employee_check[0].id)+']', 'info')
	return {'user': employee_check[0]}


# ========================================================================
# Dashboard UI - Set initial report time frame and render the UI HTML
# ========================================================================
def template(request):
	return render(request, request.build_absolute_uri().split('/template/')[1])


# ========================================================================
# Returns organization settings in JSON
# 	to be used in initializing Prodoscore front-end
# 	requested in an Ajax call
# ========================================================================
def organization_settings(request):
	import proapp.calc

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	# =====================================================================
	# Read the domain record for the current user
	# =====================================================================
	domain = Domain.objects.get(id=auth['user'].domain_id)
	onboard = Onboard.objects.get(domain=domain)
	alerts = Employee.objects.get(id=auth['user'].id)
	latest = Statistic.objects.using('replica').filter().extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE domain_id = '+str(auth['user'].domain_id)+')']).exclude(score__lt=0)

	# =====================================================================
	# Get timezone offset
	# =====================================================================
	tzoffset = 60 * proapp.calc.get_timezone_offset(datetime.datetime.utcnow(), domain.timezone)

	# =====================================================================
	# Write JSON to the response
	# =====================================================================
	return HttpResponse(json.dumps({'daystart': domain.daystart, 'dayend': domain.dayend, 'workingdays': domain.workingdays,
								'timezone': domain.timezone, 'tzoffset': tzoffset, 'since': onboard.started_on.strftime('%Y-%m-%d'),
								'upto': latest.latest().date, 'show_details':domain.show_details,
								'myself': {
									'id':auth['user'].id,
									'role': auth['user'].role,
									'email': auth['user'].email,
									'status': auth['user'].status,
									'picture': auth['user'].picture,
									'fullname': auth['user'].fullname,
									'alert_below_yesterday':alerts.alert_below_yesterday,
									'alert_below_period':alerts.alert_below_period,
									'alert_inactive_yesterday':alerts.alert_inactive_yesterday,
									'alert_inactive_period':alerts.alert_inactive_period,
									'alert_new_employee':alerts.alert_new_employee
								},
								'correlations_enabled':domain.nlp_enabled
							}), content_type = 'application/json')	#'earliest': (onboard.started_on - timedelta(days = 90)).strftime('%Y-%m-%d')


# ========================================================================
# List of all user roles.
# ========================================================================
def roles(request):
	import proapp.cron
	return HttpResponse(json.dumps(proapp.cron.role_dic), content_type = 'application/json')


# ========================================================================
# List of Managers in the domain (or of subordinates) at the given time.
# ========================================================================
def managers(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)

	if 'user' not in auth or 'error' in auth:
		return HttpResponse(json.dumps({'error': 'unauthorized'}), content_type='application/json', status=403)

	output = []
	if auth['user'].role == 70:
		output.append({
			'id': auth['user'].id, 				'role': auth['user'].role,
			'fullname': auth['user'].fullname, 	'manager_id': auth['user'].manager_id#, 'picture': auth['user'].picture
		})
		output.append({
			'id': auth['user'].manager.id, 				'role': auth['user'].manager.role,
			'fullname': auth['user'].manager.fullname, 	'manager_id': auth['user'].manager.manager_id#, 'picture': auth['user'].manager.picture
		})
	output = _managers(output, auth['user'], auth['user'].id)

	return HttpResponse(json.dumps({'managers': output}), content_type = 'application/json')

def _managers(output, user, user_id):
	managers = Employee.objects.using('replica').filter(domain_id=user.domain_id, role__gte=70, status=1)
	if user.role == 70:
		managers = managers.filter(manager_id=user_id)
	#
	for manager in managers:
		# Iterate
		if user.role == 70:
			_managers(output, user, manager.id)
		output.append({
			'id': manager.id, 				'role': manager.role,
			'fullname': manager.fullname, 	'manager_id': manager.manager_id#, 'picture': manager.picture
		})
	return output


# ========================================================================
# List of roles with employees assigned in the domain (or of subordinates) at the given time.
# ========================================================================
def subroles(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)

	if 'user' not in auth or 'error' in auth:
		return HttpResponse(json.dumps({'error': 'unauthorized'}), content_type='application/json', status=403)

	roles = Employee.objects.using('replica').filter(domain_id=auth['user'].domain_id).values('role').distinct()
	#
	# Limit to Subordinates
	if auth['user'].role == 70:
		roles = roles.filter(id__in=_subordinates([auth['user'].id], auth['user'].id))
	#
	output = []
	for role in roles:
		output.append({
			'id': role['role'],
			'title': proapp.cron.role_dic[ role['role'] ]
		})

	return HttpResponse(json.dumps({'roles': output}), content_type = 'application/json')


# ========================================================================
# List of Employees in the domain (or of subordinates) at the given time.
# ========================================================================
def employees(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)

	if 'user' not in auth or 'error' in auth:
		return HttpResponse(json.dumps({'error': 'unauthorized'}), content_type='application/json', status=403)

	employees = Employee.objects.using('replica').filter(
		domain_id=auth['user'].domain_id
	)
	#
	# By Manager
	if 'manager' in request.GET:
		employees = employees.filter(
			manager_id=request.GET['manager']
		)
	#
	# By Role
	if 'role' in request.GET:
		employees = employees.filter(
			role=request.GET['role']
		)
	elif 'role__gt' in request.GET:
		employees = employees.filter(
			role__gt=request.GET['role__gt']
		)
	elif 'role__lt' in request.GET:
		employees = employees.filter(
			role__lt=request.GET['role__lt']
		)
	#
	# Search by name or email
	if 'q' in request.GET:
		employees = employees.filter(
			Q(email__contains=request.GET['q']) | Q(fullname__contains=request.GET['q'])
		)
	#
	# Limit to Subordinates
	if auth['user'].role == 70:
		employees = employees.filter(
			id__in=_subordinates([auth['user'].id], auth['user'].id)
		)
	#
	# Sort
	if 'sort' in request.GET:
		employees = employees.order_by(_subordinates
			('-' if 'order' in request.GET and request.GET['order'] == 'desc' else '') + request.GET['sort']
		)
	#
	# Pagination
	page = 1
	if 'p' in request.GET:
		page = int(request.GET['p'])
	pages = int(math.ceil(1.0 * employees.count() / perpage))
	employees.query.set_limits(
		low=perpage*(page-1),
		high=perpage*page
	)
	#print(employees.query)
	#
	output = []
	for employee in employees:
		output.append({
			'id': employee.id,
			'email': employee.email,
			'manager_id': employee.manager_id,
			'role': employee.role,
			'status': employee.status,
			'report_enabled': employee.report_enabled,
			'report_email': employee.report_email,
			#'profileId': employee.profileId,
			'fullname': employee.fullname,
			#'picture': employee.picture
		})

	return HttpResponse(json.dumps({'employees': output, 'pages': pages}), content_type = 'application/json')

def _subordinates(subordinates, mgr_id):
	employees = Employee.objects.using('replica').filter(manager_id=mgr_id)
	#
	for employee in employees:
		subordinates.append(employee.id)
		_subordinates(subordinates, employee.id)
	return subordinates


# ========================================================================
# Organization Prodoscore
# ========================================================================
def organization_prodoscore(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)

	if 'user' not in auth or 'error' in auth:
		return HttpResponse(json.dumps({'error': 'unauthorized'}), content_type='application/json', status=403)

	domain = Domain.objects.filter(id=auth['user'].domain_id)[0]
	workingdays = json.loads(domain.workingdays)

	# Fetch data
	if 'date' in request.GET:
		fromdate = datetime.datetime.strptime(request.GET['date'], '%Y-%m-%d')
		todate = datetime.datetime.strptime(request.GET['date'], '%Y-%m-%d')
	#
	elif 'fromdate' in request.GET and 'todate' in request.GET:
		fromdate = datetime.datetime.strptime(request.GET['fromdate'], '%Y-%m-%d')
		todate = datetime.datetime.strptime(request.GET['todate'], '%Y-%m-%d')
	#
	else:
		return HttpResponse(json.dumps({'error': 'Specify date or fromdate & todate'}), content_type='application/json', status=401)
	#
	prev_period = fromdate - timedelta(days=(todate - fromdate).days)
	prev_period_short = (int(prev_period.strftime('%s')) / 86400) - 16801
	fromdate_short = (int(fromdate.strftime('%s')) / 86400) - 16800
	todate_short = (int(todate.strftime('%s')) / 86400) - 16800
	#
	# Match week-over-week week days to same week days
	wdayOffset = ((fromdate_short+4) % 7) - ((prev_period_short+4) % 7)
	# Minimize overlap to 3
	if 3 < wdayOffset:
		wdayOffset -= 7
	# If only one day, then goto previous week
	if prev_period_short+wdayOffset == fromdate_short:
		wdayOffset -= 7
	#
	array_prev = _org_scores(domain, auth['user'], workingdays, prev_period_short+wdayOffset, fromdate_short+wdayOffset+1)
	array_this = _org_scores(domain, auth['user'], workingdays, fromdate_short, todate_short)

	# Process
	output = array_this
	#
	for sdate in array_this['days']:
		pdate = sdate - (fromdate_short - prev_period_short - wdayOffset)
		if pdate in array_prev['days']:
			output['days'][sdate]['score_prev'] = array_prev['days'][pdate]['score']
		else:
			output['days'][sdate]['score_prev'] = -1

	output['delta'] = delta(array_this['average'], array_prev['average'])
	output['score_strata'] = strata_for_score(array_this['average'])
	#
	return HttpResponse(json.dumps(array_this), content_type = 'application/json')

# =======================================
# Organization Prodoscore for a given period
# =======================================
def _org_scores(domain, user, workingdays, from_sdate, to_sdate):
	output = {'days': {}}
	total = 0
	count = 0
	data_incomplete = False
	for sdate in range(from_sdate, to_sdate+1):
		date = datetime.datetime.fromtimestamp(86400*(sdate+16800))
		day_of_week = (sdate+4) % 7
		#
		org_score = Organization_prodoscore.objects.filter(domain=domain, date=date)
		if len(org_score) == 1 and org_score[0].score > -1:
			if org_score[0].finalized == 0:
				data_incomplete = True
			#
			if day_of_week in workingdays:
				output['days'][sdate] = {'score': org_score[0].score, 'wday': day_of_week}
				total += org_score[0].score
				count += 1
		#
		else:
			if len(org_score) == 0:
				org_score = Organization_prodoscore(domain=domain, date=date)
			else:
				org_score = org_score[0]

			day_calc_score = proapp.calc.scores(domain.id, user, 'domain_id = ' + str(domain.id) + ' AND role > 0 AND status > 0',
								datetime.datetime.strftime(date, '%Y-%m-%d'), datetime.datetime.strftime(date, '%Y-%m-%d'), {'cache': True})
			org_score.score = day_calc_score['organization']['score']
			try:
				org_score.save()
			except:	# Could be another thread writing the same record
				pass
			#
			if day_of_week in workingdays:
				output['days'][sdate] = {'score': org_score.score, 'wday': day_of_week}
				total += org_score.score
				count += 1
	#
	output['average'] = 0 if count == 0 else total / count
	output['incomplete'] = data_incomplete
	#
	return output


def delta(val1, val2):
	if val1 + val2 == 0:
		return 0
	else:
		return 100 * (val1 - val2) / (val1 + val2)


def strata_for_score(score):
	if score > 70:
		return 2
	elif score < 30:
		return 0
	else:
		return 1


# ========================================================================
# Employee Prodoscore for a time period for the relevent domain
# 	requested in an Ajax call when user changes the report time frame
# ========================================================================
def employees_prodoscore(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)

	if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	if auth['user'].role == 70: # for manager users
		subordinates = _subordinates([ auth['user'].id ], auth['user'].id)
		query = 'domain_id = '+str(auth['user'].domain_id)+' AND id in ('+( ','.join([str(sub) for sub in subordinates]) )+')'

		cache_key = 'dashboard_user_id-{}'.format(auth['user'].id)
		sum_array = scores(auth['user'].domain_id, auth['user'], query,
							request.GET['fromdate'], request.GET['todate'],
							{'cache': True, 'cache_key':cache_key});
	else:
		query = 'domain_id = ' + str(auth['user'].domain_id) + ' AND role > 0 AND status > 0'
		sum_array = scores(auth['user'].domain_id, auth['user'], query,
						   request.GET['fromdate'], request.GET['todate'],
						   {'cache': True})

	return HttpResponse(json.dumps(sum_array), content_type = 'application/json')


# ========================================================================
# Employees in each strata for a time period for the relevent domain
# 	requested in an Ajax call when user changes the report time frame
# ========================================================================
def employees_strata(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)

	if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	if auth['user'].role == 70: # for manager users
		subordinates = _subordinates([ auth['user'].id ], auth['user'].id)
		query = 'domain_id = '+str(auth['user'].domain_id)+' AND id in ('+( ','.join([str(sub) for sub in subordinates]) )+')'

		cache_key = 'dashboard_user_id-{}'.format(auth['user'].id)
		sum_array = scores(auth['user'].domain_id, auth['user'], query,
							request.GET['fromdate'], request.GET['todate'],
							{'day-strata': True, 'cache': True, 'cache_key':cache_key});
	else:
		query = 'domain_id = ' + str(auth['user'].domain_id) + ' AND role > 0 AND status > 0'
		sum_array = scores(auth['user'].domain_id, auth['user'], query,
							request.GET['fromdate'], request.GET['todate'],
							{'day-strata': True, 'cache': True})

	strata = [[], [], []]
	strataPrev = [[], [], []]
	strataDelta = [0, 0, 0]
	employees = {}
	for employee in sum_array['employees']:
		employees[employee] = {'email': sum_array['employees'][ employee ]['email'],
							'fullname': sum_array['employees'][ employee ]['fullname'],
							'role': sum_array['employees'][ employee ]['role'],
							'scr': sum_array['employees'][ employee ]['scr']
							}
		if sum_array['employees'][ employee ]['scr']:
			sp = strata_for_score(sum_array['employees'][ employee ]['scr']['p'])
			if sp:
				strataPrev[sp].append( employee )
		if 'strata' in sum_array['employees'][ employee ]:
			strata[ sum_array['employees'][ employee ]['strata'] ].append( employee )
			employees[employee]['strata'] = sum_array['employees'][ employee ]['strata']

	for i in [0, 1, 2]:
		if len(strata[i]) == 0 or len(strataPrev[i]) == 0:
			strataDelta[i] = False
		else:
			strataDelta[i] = delta(len(strata[i]), len(strataPrev[i]))

	return HttpResponse(json.dumps({'organization': {'strata': strata, 'strata_delta': strataDelta}, 'days': sum_array['days'], 'employees': employees}), content_type = 'application/json')


# ========================================================================
# Calculates scores for employees matching given query for the given period
# ========================================================================
def scores(domain_id, user, query, fromdate, todate, options):
	workingdays = json.loads(Domain.objects.get(id=domain_id).workingdays)
	#
	# =====================================================================
	# Fetch users matching the query
	# =====================================================================
	users = Employee.objects.using('replica').all().extra(where=[query]).order_by('fullname')
	#
	# =====================================================================
	# Determine from and to dates that has data for
	# =====================================================================
	fromdate = datetime.datetime.strptime(fromdate, '%Y-%m-%d')
	todate = datetime.datetime.strptime(todate, '%Y-%m-%d')
	emp_scores_this = ( Statistic.objects.using('replica')
						.filter(date__range=(fromdate, todate))
						.extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE '+query+')'])
						.exclude(score__lt=0) )
	if len(emp_scores_this) > 0:
		todate = datetime.datetime.strptime(emp_scores_this.latest().date, '%Y-%m-%d')
	#
	# =====================================================================
	# Fetch data for the previous same number of days period
	# =====================================================================
	prev_period = fromdate - timedelta(days=(todate - fromdate).days)
	prev_period_short = (int(prev_period.strftime('%s')) / 86400) - 16801
	fromdate_short = (int(fromdate.strftime('%s')) / 86400) - 16800
	todate_short = (int(todate.strftime('%s')) / 86400) - 16800
	#
	# Match week-over-week week days to same week days
	prevSWday = (prev_period_short+4) % 7
	thsSWday = (fromdate_short+4) % 7
	wdayOffset = thsSWday - prevSWday
	#
	# Minimize overlap to 3
	if 3 < wdayOffset:
		wdayOffset -= 7
	#
	# If only one day, then goto previous week
	if prev_period_short+wdayOffset == fromdate_short:
		wdayOffset -= 7
	#
	sum_array_prev =process_scores(domain_id, users, prev_period_short+wdayOffset, fromdate_short+wdayOffset+1, workingdays, options)
	sum_array_this = process_scores(domain_id, users, fromdate_short, todate_short, workingdays, options)
	#
	#	Fill employee details to score details
	for _user in users:
		if '/photos/private/' in _user.picture:
			_user.picture = ''
		if _user.id in sum_array_this['employees']:
			sum_array_this['employees'][_user.id]['fullname'] = _user.fullname
			sum_array_this['employees'][_user.id]['email'] = _user.email
			sum_array_this['employees'][_user.id]['role'] = _user.role
			sum_array_this['employees'][_user.id]['manager'] = _user.manager_id
			sum_array_this['employees'][_user.id]['picture'] = _user.picture
			if sum_array_prev['employees'][_user.id]['scr'] and sum_array_this['employees'][_user.id]['scr']:
				sum_array_this['employees'][_user.id]['scr']['delta'] = proapp.calc.delta(sum_array_this['employees'][_user.id]['scr']['l'], sum_array_prev['employees'][_user.id]['scr']['l'])
				sum_array_this['employees'][_user.id]['scr']['p'] = sum_array_prev['employees'][_user.id]['scr']['l']
			#
		else:
			sum_array_this['employees'][_user.id] = {'fullname': _user.fullname, 'email': _user.email,
				'status': _user.status, 'role': _user.role, 'manager': _user.manager_id, 'picture': _user.picture, 'is_app_user': _user.is_app_user }

	return sum_array_this


# ========================================================================
#	Fetch, Calculate and Populate data into a data structure
# ========================================================================
def process_scores(domain_id, users, fromdate, todate, workingdays, options):
	doCache = options['cache'] if 'cache' in options else False
	getProds = options['prods'] if 'prods' in options else False
	dailyStrata = options['day-strata'] if 'day-strata' in options else False
	sum_array = {'employees': {}, 'days': {}}
	emp_score_cumil = {}
	#
	for user in users:
		if user.status > 0 and user.role > -1:
			sum_array['employees'][user.id] = {}
			emp_score_cumil[user.id] = {'lcumil': 0, 'count': 0}
	#
	for vardate in range(fromdate, todate+1):
		day_of_week = (vardate+4) % 7
		if day_of_week in workingdays:
			sum_array['days'][vardate] = {'wday': day_of_week}
			if dailyStrata:
				sum_array['days'][vardate]['strata'] = [[], [], []]
			employees = None
			# =====================================================================
			# Fist check on memcached
			# =====================================================================
			if 'cache_key' in options:
				cacheKey = '{}-on{}'.format(options['cache_key'], str(vardate))
			else:
				cacheKey = 'domain-'+str(domain_id)+'-on-'+str(vardate)

			data = proapp.cache.get(cacheKey)
			if data is not None:
				employees = data

			# ================================================================
			# Get the data calculated - if not found in cache
			# ================================================================
			if employees is None:
				employees = {}
				dataIncomplete = False
				for employee in sum_array['employees']:
					# =============================================================
					# Get employee score for the day
					# =============================================================
					employees[employee] =  proapp.calc.emp_day_score(employee, vardate, getProds)
					if employees[employee] == False or 'warning' in employees[employee]:
						dataIncomplete = True

					emp_score = employees[employee]
					if emp_score != False and employee in sum_array['employees']:
						emp_score_cumil[employee]['lcumil'] += emp_score['scr']['l']
						emp_score_cumil[employee]['count'] += 1
						#
						# ==========================================================
						# Add employee to daily strata
						# ==========================================================
						if dailyStrata:
							sum_array['days'][vardate]['strata'][strata_for_score(emp_score['scr']['l'])].append(
								employee)

					if emp_score_cumil[employee]['count'] == 0:
							sum_array['employees'][employee]['scr'] = False
					else:
						sum_array['employees'][employee]['scr'] = {
							'l': round(emp_score_cumil[employee]['lcumil'] / emp_score_cumil[employee]['count'], 3)}
						sum_array['employees'][employee]['strata'] = strata_for_score(
							sum_array['employees'][employee]['scr']['l'])

				#
				# Store in cache for future use
				if doCache and not dataIncomplete:
					proapp.cache.add(cacheKey, employees, 12*3600)

			# ================================================================
			# Each employee working day
			# ================================================================
			else :
				for employee in employees:
					emp_score = employees[employee]
					if emp_score != False and employee in sum_array['employees']:
						emp_score_cumil[employee]['lcumil'] += emp_score['scr']['l']
						emp_score_cumil[employee]['count'] += 1
						#
						# ==========================================================
						# Add employee to daily strata
						# ==========================================================
						if dailyStrata:
							sum_array['days'][vardate]['strata'][ strata_for_score(emp_score['scr']['l']) ].append(employee)

					if emp_score_cumil[employee]['count'] == 0:
						sum_array['employees'][employee]['scr'] = False
					else:
						sum_array['employees'][employee]['scr'] = {
							'l': round(emp_score_cumil[employee]['lcumil'] / emp_score_cumil[employee]['count'], 3)}
						sum_array['employees'][employee]['strata'] = strata_for_score(
							sum_array['employees'][employee]['scr']['l'])

	return sum_array


# ========================================================================
# Save employee/user settings for a single employee/user or multiple employees/users
# 	Receives JSON data in an Ajax POST request
# ========================================================================
def update_employee(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	#
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	output = []
	errors = {}
	hasErrors = False
	# ===============================================================
	# BLOCK COMMENTS ARE CAUSING INDENTATION MISTAKES MAKING IT HARDER TO TRACK BLOCKS OF CODE
	# ===============================================================
	if request.method=='POST':
		# Parse JSON request body
		received_json_data=json.loads(request.body)

		# Iterate through user objects in request
		for item in received_json_data:
			emp_id = item.get('id')

			# Find if the email/username is a duplicate
			if 'email' in item:
				if emp_id == 'new':
					chk_duplicate = Employee.objects.filter(email=item.get('email'))
				else:
					chk_duplicate = Employee.objects.filter(email=item.get('email')).exclude(id=emp_id)
				if len(chk_duplicate) != 0:
					errors[item.get('email')] = "Username is duplicate"
					hasErrors = True
					continue

			# A new user account to be created
			if (emp_id == 'new'):
				employee = Employee(
					domain_id=auth['user'].domain_id,
					role=item.get('role'),
					fullname=item.get('fullname'),
					email=item.get('email'),
					password=hashlib.sha512(item.get('password')).hexdigest(),
					status=-1,
					profileId=0,
					level=0)#,admin=1

			# Open an existing user account to edit
			else:
				employee = Employee.objects.get(id=emp_id)

				# Copy attributes from JSON to database object //update_organization
				if 'role' in item and int(emp_id) != int(auth['user'].id):
					employee.role = int(item.get('role'))
					if int(item.get('role')) in [-1, -2]:
						employee.conf_id = None
						employee.crm_id = None
						employee.broadsoft_admin_id = None
						employee.broadsoft_userid = None
						employee.salesforce_userid = None
						employee.salesforce_updated = None
						employee.phone_system_id = None
						employee.chatter_id = None
						#employee.admin = 1
				if 'fullname' in item:
					employee.fullname = item.get('fullname')
				if 'email' in item:
					employee.email = item.get('email')
				if 'password' in item and item.get('password') != '[ENCRYPTED]':
					employee.password = hashlib.sha512(item.get('password')).hexdigest()

			if 'status'in item:
				employee.status = int(item.get('status'))
			if 'manager' in item:
				employee.manager_id = item.get('manager')
			if 'reportEmail'in item:
				employee.report_email = item.get('reportEmail')
			if 'reportFrequency'in item:
				employee.report_enabled = int(item.get('reportFrequency'))

			# Write (commit) the database object
			employee.save()
			output.append(employee.id)

		# Acknowledge the client
		if len(output) > 0:
			data = {'message': 'database updated', 'data-ids': output}
		elif not hasErrors:
			data = {'message': 'nothing to do'}
		else:
			data = {}
		#
		if hasErrors:
			data['errors'] = errors

	else:
		if "terminated" in request.GET and eval(request.GET['terminated']) == True:
			users = Employee.objects.filter(domain=auth['user'].domain, role__lt=0).order_by('fullname')
		else:
			users = Employee.objects.filter(domain=auth['user'].domain,role__gte=0).order_by('fullname')

		data = {'employees': {}}

		if len(users) > 0:
			for user in users:
					data['employees'][user.id] = {'fullname': user.fullname, 'email': user.email,'report_enabled': user.report_enabled, 'report_email': user.report_email,
												  'level': user.level, 'role': user.role, 'manager': user.manager_id,'status': 1 if user.status == 1 else -1,#, 'admin': user.admin
												  'picture': user.picture,'is_subordinate': user.manager_id == auth['user'].id}

	return HttpResponse(json.dumps(data), content_type = 'application/json')


# ========================================================================
#		EOF
# ========================================================================
