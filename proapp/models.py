from __future__ import unicode_literals
import pickle, base64, datetime
from django.db import models

models.EncryptedCharField = models.CharField
#	http://stackoverflow.com/questions/13077109/how-can-i-create-an-encrypted-django-field-that-converts-data-when-its-retrieve
#	https://github.com/django-extensions/django-extensions/blob/master/django_extensions/db/fields/encrypted.py

#from django.contrib import admin
#from django.contrib.auth.models import User
#from oauth2client.contrib.django_orm import FlowField
#from oauth2client.contrib.django_orm import CredentialsField

import proapp.storage

class Domain(models.Model):
	title = models.CharField(max_length=200)
	origin = models.CharField(max_length=10, default='gsuite')
	daystart = models.CharField(max_length=20, default='00:00')
	dayend = models.CharField(max_length=20, default='00:00')
	workingdays = models.CharField(max_length=24, default='[1, 2, 3, 4, 5]')
	timezone = models.CharField(max_length=50, default='USA')
	cronzone = models.IntegerField(default=0)
	#
	default_report_flag = models.IntegerField(default=1)
	subscription_type = models.IntegerField(default=0)
	subscription_volume = models.IntegerField(default=25)
	nlp_enabled = models.IntegerField(default=0)
	crm_enabled = models.IntegerField(default=0)
	telco_enabled = models.IntegerField(default=0)
	orgbaseline_enabled = models.IntegerField(default=0)
	show_details = models.IntegerField(default=1)
	#
	conf_system = models.CharField(max_length=12)
	phone_system = models.CharField(max_length=12)
	conf_partner = models.CharField(max_length=32)
	conf_account = models.CharField(max_length=32)
	phone_system_token = models.EncryptedCharField(max_length=50, default=None)
	phone_system_id = models.EncryptedCharField(max_length=32, default=None)
	phone_system_password = models.EncryptedCharField(max_length=32, default=None)
	phone_system_admin = models.IntegerField(default=None)
	conf_admin = models.EncryptedCharField(max_length=32)
	conf_password = models.EncryptedCharField(max_length=32)
	crm_system = models.CharField(max_length=12, default=None)
	crm_token = models.EncryptedCharField(max_length=40)
	crm_modules = models.IntegerField(default=32)
	crm_admin = models.EncryptedCharField(max_length=32)
	salesforce_userid = models.CharField(max_length=42)
	salesforce_password = models.EncryptedCharField(max_length=42)
	salesforce_id = models.CharField(max_length=40)
	#phone_system_score_based = models.CharField(max_length=12)

	#	Override superClass - Compute and Save cronzone
	def save(self, *args, **kwargs):
		import pytz
		try:
			tz = pytz.timezone(self.timezone)
			dt = datetime.datetime.strptime('2016-01-01', '%Y-%m-%d')
			self.cronzone = int(tz.utcoffset(dt).seconds / 7200)
		except:
			pass
		#
		super(Domain, self).save(*args, **kwargs)

	def __str__(self):
		return self.title

class Onboard(models.Model):
	domain = models.ForeignKey(Domain)
	status = models.IntegerField(default=0)
	started_on = models.DateTimeField(default='1970-01-01 00:00:00')
	last_login = models.DateTimeField(default='1970-01-01 00:00:00')
	email = models.CharField(max_length=80)

class Broadsoft_admin(models.Model):
	domain = models.ForeignKey(Domain)
	broadsoft_xsi = models.CharField(max_length=120)
	broadsoft_userid = models.CharField(max_length=42)
	broadsoft_password = models.EncryptedCharField(max_length=42)
	is_valid = models.IntegerField(default=0)

class RingCentral(models.Model):
	domain_id = models.IntegerField()
	# SHOULD BE !?
	#domain = models.ForeignKey(Domain)
	access_token = models.TextField
	refresh_token = models.TextField
	access_expire = models.DateTimeField(default='1970-01-01')
	refresh_expire = models.DateTimeField(default='1970-01-01')
	endpoint_id = models.CharField(max_length=25)
	owner_id = models.IntegerField()
	timestamp = models.FloatField()
	data = models.TextField

class Vbc(models.Model):
	domain = models.ForeignKey(Domain)
	access_token = models.TextField
	refresh_token = models.TextField
	access_expire = models.DateTimeField(default='1970-01-01')
	refresh_expire = models.DateTimeField(default='1970-01-01')
	endpoint_id = models.CharField(max_length=25)
	timestamp = models.FloatField()
	data = models.TextField

class Sugar_settings(models.Model):
	domain = models.ForeignKey(Domain)
	sugar_instance = models.CharField(max_length=256)
	sugar_admin = models.EncryptedCharField(max_length=40)
	sugar_password = models.EncryptedCharField(max_length=32)
	sugar_client_key_name = models.EncryptedCharField(max_length=32)
	sugar_client_key = models.EncryptedCharField(max_length=32)
	access_token = models.EncryptedCharField(max_length=40)
	access_expire = models.DateTimeField(default='1970-01-01')
	refresh_token = models.EncryptedCharField(max_length=40)
	refresh_expire = models.DateTimeField(default='1970-01-01')
	insert_ts = models.DateTimeField(default='1970-01-01')
	update_ts = models.DateTimeField(default='1970-01-01')

class Employee(models.Model):
	domain = models.ForeignKey(Domain)
	manager = models.ForeignKey('self', default=None)
	email = models.CharField(max_length=80)
	password = models.CharField(max_length=200, default='[NOPASSWORD]')		# This is hashed by the application - so no need to DB level encryption
	profileId = models.CharField(max_length=200, default='000000000000000000000')
	fullname = models.CharField(max_length=100)
	picture = models.CharField(max_length=200)
	level = models.BooleanField()
	admin = models.BooleanField()
	role = models.IntegerField(default=0)
	status = models.IntegerField(default=-1)
	report_enabled = models.IntegerField(default=1000)
	report_email = models.EmailField(max_length=200)
	conf_id = models.CharField(max_length=20)
	crm_id = models.CharField(max_length=40)
	last_login = models.DateTimeField(default='1970-01-01 00:00:00')
	alert_below_yesterday = models.IntegerField(default=1)
	alert_below_period = models.IntegerField(default=1)
	alert_inactive_yesterday = models.IntegerField(default=1)
	alert_inactive_period = models.IntegerField(default=1)
	alert_new_employee = models.IntegerField(default=1)
	broadsoft_admin = models.ForeignKey(Broadsoft_admin)
	broadsoft_userid = models.CharField(max_length=200, default=None)
	phone_system_id = models.CharField(max_length=200, default=None)
	salesforce_userid = models.CharField(max_length=200, default=None)
	salesforce_updated = models.IntegerField(default=0)
	is_app_user = models.BooleanField(default=0)
	last_login_domain_name = models.CharField(max_length=200)

	def __str__(self):
		return self.fullname

# Not Used nor Implemented
class Leave(models.Model):
	employee = models.ForeignKey(Employee)
	date = models.CharField(max_length=12)
	description = models.CharField(max_length=200)

class Baseline(models.Model):
	domain = models.ForeignKey(Domain)
	role = models.IntegerField(default=0)
	pcode = models.CharField(max_length=4)
	baseline = models.FloatField()

class Product(models.Model):
	code = models.CharField(max_length=4)
	slug = models.CharField(max_length=24)
	title = models.CharField(max_length=40)
	metric_desc = models.CharField(max_length=80)
	stdweight = models.IntegerField()
	hexccode = models.CharField(max_length=8)
	fields = models.TextField()

class Org_weight(models.Model):
	domain = models.ForeignKey(Domain)
	pcode = models.CharField(max_length=4)
	weight = models.IntegerField()

class Statistic(models.Model):
	employee = models.ForeignKey(Employee)
	date = models.CharField(max_length=10)
	product = models.CharField(max_length=12)
	nwh_score = models.IntegerField()
	nlp_analized = models.IntegerField()
	retry_count = models.IntegerField(default=0)
	last_updated = models.DateTimeField(default=datetime.datetime.now)
	score = models.IntegerField()	#	-1: not known yet, -2: cron failed, -3: onboard initial cron
	class Meta:
		get_latest_by = 'date'

class Employee_prodoscore(models.Model):
	employee = models.ForeignKey(Employee)
	domain = models.ForeignKey(Domain)
	date = models.CharField(max_length=10)
	role = models.IntegerField(default=0)
	score = models.FloatField()

class Organization_prodoscore(models.Model):
	domain = models.ForeignKey(Domain)
	date = models.CharField(max_length=10)
	score = models.FloatField()
	finalized = models.IntegerField(default=0)
	expire = models.DateTimeField(default='1970-01-01')

class Detail(models.Model):
	statistic = models.ForeignKey(Statistic, default=0)
	resource_id = models.CharField(max_length=100)
	title = models.CharField(max_length=200)
	counted = models.IntegerField(default=0)		# Counted into stats
	flag = models.IntegerField(default=0)			# Need to store in two forms since we allow user change one field
	start_time = models.IntegerField(default=0)
	end_time = models.IntegerField(default=0)
	nlp_pversion = models.IntegerField(default=0)	#	0: not processed yet, 666: waiting for transcript, 999: nlp not enabled for domain
	time_range = models.IntegerField(default=0)
	sentiment_magnitude = models.FloatField()
	sentiment_score = models.FloatField()

	#~ data = models.EncryptedCharField(max_length=10000)
	#~ nlp_presynaptic = models.TextField()
	#~ nlp_entities = models.TextField()

	#	-------------------------------------------------------------------------------------------------------
	#	Temporary variable for field value - not written to storage
	_data = False
	_nlp_presynaptic = False
	_nlp_entities = False

	#	-------------------------------------------------------------------------------------------------------
	#	Read file from Cloud Storage
	#	Return data field to application logic
	@property
	def data(self):
		if self._data == False:
			self._data = proapp.storage.read_file(self, '.data.json')
		#if self._data == False:
		#	self._data = '{"file": "'+proapp.storage.gcs_path(self)+'.data.json", "error": "not-found"}'
		return self._data
	#
	@property
	def nlp_presynaptic(self):
		if self._nlp_presynaptic == False:
			self._nlp_presynaptic = proapp.storage.read_file(self, '.presynaptic.json')
		return self._nlp_presynaptic
	#
	@property
	def nlp_entities(self):
		if self._nlp_entities == False:
			self._nlp_entities = proapp.storage.read_file(self, '.entities.json')
		return self._nlp_entities

	#	-------------------------------------------------------------------------------------------------------
	#	Receive data field from application logic
	#	Store field value in temporary variable
	@data.setter
	def data(self, value):
		self._data = value
	#
	@nlp_presynaptic.setter
	def nlp_presynaptic(self, value):
		self._nlp_presynaptic = value
	#
	@nlp_entities.setter
	def nlp_entities(self, value):
		self._nlp_entities = value

	#	-------------------------------------------------------------------------------------------------------
	#	Override superClass - Save data object to database and large fields Cloud Storage
	def save(self, *args, **kwargs):
		#	Write other fields to database
		super(Detail, self).save(*args, **kwargs)
		#
		#	Write temporary variable to Google Storage
		if self._data != False:
			proapp.storage.write_file(self, '.data.json', self._data)
		#	--------------------------------------------------
		if self._nlp_presynaptic != False:
			proapp.storage.write_file(self, '.presynaptic.json', self._nlp_presynaptic)
		#	--------------------------------------------------
		if self._nlp_entities != False:
			proapp.storage.write_file(self, '.entities.json', self._nlp_entities)

	def __str__(self):
		return proapp.storage.gcs_path(self)

class Doc(models.Model):
	resource_id = models.CharField(max_length=160)
	revision_id = models.CharField(max_length=80)
	migrated = models.IntegerField(default=0)

	#	-------------------------------------------------------------------------------------------------------
	#	Temporary variable for field value - not written to storage
	_content = False
	_diff = False

	#	-------------------------------------------------------------------------------------------------------
	#	Read file from Cloud Storage
	#	Return data field to application logic
	@property
	def content(self):
		if self._content == False:
			self._content = proapp.storage.read_file(self, '.content.txt')
		return self._content
	#
	@property
	def diff(self):
		if self._diff == False:
			self._diff = proapp.storage.read_file(self, '.diff.txt')
		return self._diff

	#	-------------------------------------------------------------------------------------------------------
	#	Receive data field from application logic
	#	Store field value in temporary variable
	@content.setter
	def content(self, value):
		self._content = value
	#
	@diff.setter
	def diff(self, value):
		self._diff = value

	#	-------------------------------------------------------------------------------------------------------
	#	Override superClass - Save data object to database and large fields Cloud Storage
	def save(self, *args, **kwargs):
		self.migrated = 1
		#	Write other fields to database
		super(Doc, self).save(*args, **kwargs)
		#
		#	Write temporary variable to Google Storage
		if self.content != False and self.content != '':
			proapp.storage.write_file(self, '.content.txt', self.content)
		#	--------------------------------------------------
		if self.diff != False and self.diff != '':
			proapp.storage.write_file(self, '.diff.txt', self.diff)

class Transcript_job(models.Model):
	employee = models.ForeignKey(Employee)
	detail = models.ForeignKey(Detail)
	params = models.TextField()
	status = models.IntegerField()
	percentage = models.IntegerField()

class Notification(models.Model):
	domain = models.ForeignKey(Domain)
	date = models.CharField(max_length=12)
	time = models.CharField(max_length=12)
	level = models.CharField(max_length=12)
	message = models.CharField(max_length=120)

# Depricated
class Chart(models.Model):
	code = models.CharField(max_length=50, default='00000000000000')
	data = models.TextField()

class Feedback(models.Model):
	status = models.CharField(max_length=16, default='new')
	employee = models.ForeignKey(Employee)
	received_on = models.DateTimeField(default=datetime.datetime.now, blank=True)
	feedback = models.TextField()
	screenshot = models.TextField()
	useragent = models.TextField()
	filter_fromdate = models.CharField(max_length=10)
	filter_todate = models.CharField(max_length=10)
	url_hash = models.CharField(max_length=40)
	jira_key = models.CharField(max_length=16)

# Depricated
class Feedback_comment(models.Model):
	feedback = models.ForeignKey(Feedback)
	employee = models.ForeignKey(Employee)
	commented_on = models.DateTimeField(default=datetime.datetime.now, blank=True)
	comment = models.TextField()

class Click(models.Model):
	employee = models.ForeignKey(Employee)
	date = models.IntegerField()
	page = models.CharField(max_length=80)
	dompath = models.CharField(max_length=10000)
	x = models.IntegerField()
	y = models.IntegerField()
	screenwidth = models.IntegerField()
	scrolltop = models.IntegerField()

class Broadsoft_user(models.Model):
	admin = models.IntegerField()
	userid = models.CharField(max_length=50)
	subscriptionid = models.CharField(max_length=50)
	subscriptionexpire = models.IntegerField()

class Broadsoft_event(models.Model):
	broadsoft_user = models.ForeignKey(Broadsoft_user)
	received_on = models.CharField(max_length=20)
	eventid = models.CharField(max_length=50)
	data = models.TextField()

class Broadsoft_raw(models.Model):
	data = models.TextField()

