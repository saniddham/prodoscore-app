
import base64
import datetime
import hashlib
import json
import logging
import os
import time
import urllib
import urllib2
import uuid
from collections import OrderedDict
from datetime import timedelta
from json import dumps, loads
from ssl import SSLError
from urllib import urlencode

import httplib2
import requests
from django.conf import settings
from django.core import serializers
from django.db import DatabaseError, transaction
from django.http import (HttpResponse, HttpResponseBadRequest,
                         HttpResponseRedirect)
from django.shortcuts import redirect, render
from googleapiclient import discovery
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from httplib2 import Http
from oauth2client import client, tools
from oauth2client.client import GoogleCredentials, HttpAccessTokenRefreshError
from oauth2client.service_account import ServiceAccountCredentials

import proapp.cron
import proapp.views

import proapp.products.office_365
import proapp.l1_interface

from .models import Domain, Employee, Onboard, Statistic

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import taskqueue
	from google.appengine.api.taskqueue import TaskAlreadyExistsError
	import cloudstorage as gcs


token_url_rc = settings.VERSIONS['token_host'][settings.VERSIONS['this']] + "/token/ringcentral/auth"
token_url_vbc = settings.VERSIONS['token_host'][settings.VERSIONS['this']] + "/token/vbc/auth"
#from proapp.products.ringcentral_calls import token_url_rc
#from proapp.products.vbc import token_url_vbc

scopes_dom = ['https://www.googleapis.com/auth/admin.directory.domain.readonly',
			#'https://www.googleapis.com/auth/directory.domain.readonly',
			'https://www.googleapis.com/auth/admin.directory.user.readonly',
			'https://www.googleapis.com/auth/userinfo.email']

CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secret.json')
credentials_dom = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRETS, scopes=scopes_dom)

trialSeats = 25	#	TRIAL SEATS - HARD-CODED FOR NOW

# Sign In page
def sign_in(request):
	if request.get_host() == 'app.prodoscore.com' and not request.is_secure():
		return HttpResponseRedirect('https://app.prodoscore.com/sign-in/')
	#
	data = {}
	if 'error' in request.GET:
		data['error'] = request.GET['error']
	elif 'username' in request.POST and 'password' in request.POST:
		password_check = Employee.objects.filter(email=request.POST['username'], role__gt=0)
		if len(password_check) == 0:
			data['error'] = 'Your username and password didn\'t match. Please try again.'
		else:
			if hashlib.sha512(request.POST['password']).hexdigest() == password_check[0].password:
				request.session['id'] = password_check[0].id
				request.session['email'] = password_check[0].email
				try:
					primary_domain = password_check[0].email.split('@')[1]
				except Exception as e:
					primary_domain = 'Admin'
				if password_check[0].last_login_domain_name:
					request.session['domain'] = password_check[0].last_login_domain_name
				else:
					request.session['domain'] = password_check[0].domain.title
				password_check[0].last_login = datetime.datetime.now()
				password_check[0].save()
				mgr_check = Employee.objects.filter(manager_id=password_check[0].id)
				if len(mgr_check) > 0 or password_check[0].role == 80:
					if primary_domain == request.session['domain'] or primary_domain == 'Admin':
						return HttpResponseRedirect(request.build_absolute_uri('/') + '#dashboard')
					else:
						return HttpResponseRedirect(request.build_absolute_uri('/') + '#settings/employees')
				elif password_check[0].role > 0:
					return HttpResponseRedirect(request.build_absolute_uri('/')+'dashboard')
				else:
					data['error'] = 'Your user account is terminated.'
			else:
				data['error'] = 'Your username and password didn\'t match. Please try again.'
	return render(request, 'login.html', data)


# ========================================================================
# Get domains for a user account - returns JSON object
# ========================================================================
def get_domains(request):
	if 'email' not in request.session.keys() or 'domain' not in request.session.keys():
		return HttpResponse('Access Denied')
	proapp.cron.log('User: '+request.session['email'] , 'info')
	#
	if '@' not in request.session['email']:
		return HttpResponse('[{"status": 7, "title": "'+request.session['domain']+'"}]', content_type = 'application/json')
	#
	try:
		if 'origin' in request.session.keys() and request.session['origin'] == 'office365' and 'domain' in request.session.keys():
			token = proapp.products.office_365.get_domain_access_token(request.session['domain'])
			if token.status_code == 200:
				domains = [request.session['domain']]
			else:
				return HttpResponse(json.dumps({'error': 'unauthorized_client'}), content_type = 'application/json')
		else:
			domains = get_domain_list(request.session['email'])
		output = []
		for domain in domains:
			domain_rec = Domain.objects.filter(title=domain)
			if len(domain_rec) == 0:
				output.append({'title': domain, 'status': 0})
			else:
				onboard = Onboard.objects.filter(domain=domain_rec[0])
				output.append({'title': domain_rec[0].title, 'status': onboard[0].status})
		#
		return HttpResponse(json.dumps(output), content_type = 'application/json')
	#
	except Exception, e:
		print(str(e))
		#domains = [request.session['domain']]
		return HttpResponse(json.dumps({'error': 'unauthorized_client'}), content_type = 'application/json')


# ========================================================================
# Select a domain to install Prodoscore for
# ========================================================================
def select_domain(request):
	if 'email' not in request.session.keys() or 'domain' not in request.session.keys():
		return HttpResponse('Access Denied')
	proapp.cron.log('User: '+request.session['email'], 'info')
	#
	request.session['domain'] = request.GET['domain']
	#
	switchOnly = request.GET.get('switch-only')
	if switchOnly is None:
		# Get or Create the domain record
		onboard_check = _get_or_create_domain(request, request.session['domain'], request.session['email'])
		onboard_check.status = 2
		onboard_check.save()
		employee_check = Employee.objects.filter(email=request.session['email'])
		if len(employee_check) > 0:
			employee_check = employee_check[0]
			try:
				employeeDomain = request.session['email'].split('@')[1]
				if employeeDomain == request.session['domain'] and employee_check.role == 80:
					employee_check.domain_id = onboard_check.domain_id
					employee_check.save()
			except Exception:
				print('onboarded by non emal address user')
	else :
		employee_check = Employee.objects.filter(email=request.session['email'])[0]
		if employee_check.role == 80:
			employee_check.last_login_domain_name = request.session['domain']
			employee_check.save()
	#
	return HttpResponse(json.dumps(request.session['domain']), content_type = 'application/json')


# Get or Create the domain record
def _get_or_create_domain(request, domain, emailAddress, domain_origin = None):
	with transaction.atomic():
		domain_check = Domain.objects.filter(title=domain)
		if len(domain_check) == 0:
			if domain_origin == None:
				domain_check = Domain(title=domain, origin='gsuite', default_report_flag=1000, daystart='09:00', dayend='18:00', timezone='America/Los_Angeles')
			else:
				domain_check = Domain(title=domain, origin=domain_origin, default_report_flag=1000, daystart='09:00', dayend='18:00', timezone='America/Los_Angeles')
			domain_check.save()
		else:
			domain_check = domain_check[0]
		#
		# Get or Create the onboarding record
		onboard_check = Onboard.objects.filter(domain=domain_check)
		if len(onboard_check) == 0:
			onboard_check = Onboard(domain=domain_check, status=0, started_on=datetime.datetime.now(), last_login=datetime.datetime(1970, 1, 1), email=emailAddress)
			onboard_check.save()
			#step = 1 #Select admins and managers
			#
			try:
				# Send email to Prodoscore team
				res = render(request, 'email/new-onboard.html', {'domain': domain, 'email': emailAddress, 'domain_origin': domain_origin})
				requests.post(
					settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']),
					data={"from": settings.MAILGUN['FROM'],
						"to": ["support@prodoscore.com", "qa-team@prodoscore.com"],
						"subject": "A new domain ["+domain+"] has started registering on Prodoscore from "+ domain_origin +"",
						"html": res})
			except Exception as e:
				logging.error("Error sending 'new domain' email " + str(e.message))
			#
			try:
				add_to_salesforce(onboard_check)
			except Exception as e:
				logging.error('Encountered error while adding on-boarding users to salesforce.')
				logging.error(e.message)
			#
			return onboard_check
		else:
			return onboard_check[0]


# Register page
def register(request):
	if 'email' not in request.session.keys() or 'domain' not in request.session.keys():
		# First we need to authenticate the user
		return HttpResponseRedirect(request.build_absolute_uri('/')+'sign-in/')
	#
	# If not a GApps domain, show error
	if request.session['domain'] is None:
		return render(request, 'login.html', {'error': 'You must be an administrator of a G Suite domain to setup Prodoscore.'})
	#
	# Now we know who is knocking on the door
	#
	step = 'a'
	onboard_check = False
	multiple_domain = request.GET.get('mdomains')
	with transaction.atomic():
		# Check at-least one of domains in Prodoscore
		try:
			domains = get_domain_list(request.session['email'])
		except Exception, e:
			print(str(e))
			domains = [request.session['domain']]
		domain_check = Domain.objects.filter(title__in=domains)
		#
		try:
			# Domain Found
			if multiple_domain:
				step = 'a'
			elif len(domain_check) > 0:
				found = False
				for domain_chk in domain_check:
					if domain_chk.title == request.session['domain']:
						found = True
						domain_check = domain_chk
						break;
				if not found:
					domain_check = domain_check[0]
					request.session['domain'] = domain_check.title
				#
				onboard_check = Onboard.objects.filter(domain=domain_check)
				onboard_check = onboard_check[0]
			#
			# Domain not found
			elif len(domains) == 1:
				# Get or Create the domain record
				if 'origin' in request.session.keys():
					onboard_check = _get_or_create_domain(request, request.session['domain'], request.session['email'], request.session['origin'])
				else:
					onboard_check = _get_or_create_domain(request, request.session['domain'], request.session['email'])
			#
			#else:
				# Multiple domains - allow select one domain
			#
			# Let's write everything (we know so-far) into database
			#domain_users = proapp.cron.get_domain_users(request.session['domain'], request.session['email'])
			#print(domain_users)	# Still have to check if current user is an admin - because this could still return data from Memcached
		except HttpError as error:
			return render(request, 'login.html', {'error': _translate_error(error._get_reason())})
		#
		except HttpAccessTokenRefreshError, error:
			print(str(error))
			return render(request, 'login.html', {'error': 'There was an error in authenticating your account.'})
		#
		if onboard_check:
			#elif onboard_check.status == 3:
			#	# Create employee records (if not exists)
			#	_get_or_create_employees(domain_check, domain_users)
			if onboard_check.status > 6:
				# Already onboarded. Send to dashboard
				return HttpResponseRedirect(request.build_absolute_uri('/'))
			elif onboard_check.status > 4:
				return HttpResponseRedirect(request.build_absolute_uri('/')+'register/progress')
			else:
				# Will send to approriate step based on wherever last left
				step = onboard_check.status
	#
	if step == 0:
		step = 'a'
	elif step == 3:
		step = 2
	return render(request, 'register.html', {'step': step, 'domain': request.session['domain'], 'email': request.session['email'], 'origin': request.session['origin']})#, 'task': task

#
#add onboarding details to salesforce
#
def add_to_salesforce(onboardObject):
	import simple_salesforce
	from simple_salesforce import Salesforce
	from simple_salesforce.exceptions import SalesforceMalformedRequest, SalesforceRefusedRequest
	from .models import Domain, Onboard, Employee

	OrgID = '000000ProdoScore'
	#
	#Obataining Salesforce prodoscore account credentials
	role_dic = proapp.cron.role_dic
	domain = Domain.objects.filter(id=9)[0]
	url = 'https://login.salesforce.com/services/oauth2/token'
	token_id = domain.crm_token
	payload = {'grant_type': 'password',
		   'client_id':'3MVG9szVa2RxsqBZcaYqIR5nZljlWeftvhfUfdS.9S9xsc81cBSxDTR2WfNQMKzN3bhZoCT.ZzWY1D61FihRT',
		   'client_secret':'7790752435074856812',
		   'username':domain.salesforce_userid,
		   'password':domain.salesforce_password+'{}'.format(token_id)
		   }

	headers = {'Accept-Charset': 'UTF-8'}
	r = requests.post(url, data=payload, headers=headers)
	response_data = r.json()
	salesforce_exist = False
	try:
		sf = Salesforce(instance_url=response_data['instance_url'], session_id=response_data['access_token'])
		dObject = onboardObject.domain
		employees = Employee.objects.filter(domain_id = dObject.id)
		eObject = Employee.objects.filter(domain_id=dObject.id, role=80).extra(where=["email LIKE '%%@%%'"])[0]

		#Avoid Data duplication
		if dObject.salesforce_id is None or dObject.salesforce_id == '':
			a = sf.Account.create({'Name':dObject.title, 'Org_ID__c':dObject.id, 'Primary_Contact_Email__c':eObject.email, 'Timezone__c':dObject.timezone, 'NumberOfEmployees': len(employees), 'Trial_Start_Date__c':(onboardObject.started_on).strftime('%Y-%m-%d %H:%M:%S')})
			dObject.salesforce_id = a["id"]
			dObject.save()
			employees_objects = Employee.objects.filter(domain_id=dObject.id)
			#
			for employees_object in employees_objects:
				try:
					report_enabled = str(employees_object.report_enabled)
					if ( len(report_enabled) < 2 and report_enabled == '1' ) or ( len(report_enabled) > 2 and report_enabled[3] == '1' ) or ( len(report_enabled) > 2 and report_enabled[2] == '1' ):
						email_check = True
					else:
						email_check = False
					sf.Contact.create({'AccountId':a["id"],
									   'LastName':employees_object.fullname,
									   'User_ID__c':employees_object.id,
									   'Email':employees_object.email,
									   'Role__c':role_dic[employees_object.role]})
				except SalesforceMalformedRequest:
					print(dObject.title +"SalesforceMalformedRequest")
				except IndexError:
					print(dObject.title +'this IndexError for innerloop')

	except KeyError:
		print("authentication failure")
	except IndexError:
		print(dObject.title +"this IndexError for outerloop")
	except SalesforceMalformedRequest:
		# Check whether the domain is already in salesforce
		try:
			records_accounts = sf.query("SELECT Id, Name FROM Account WHERE Org_ID__c = '" + str(dObject.id) + "'")
			records_accounts = records_accounts['records']
			if not len(records_accounts) == 0:
				records = records_accounts[0]
				dObject.salesforce_id = records['Id']
				dObject.save()
				salesforce_exist = True
		except SalesforceMalformedRequest:
			print(dObject.title + "SalesforceMalformedRequest")

	except SalesforceRefusedRequest:
		print(dObject.title +"SalesforceRefusedRequest")

	return salesforce_exist


# ============================================================
# conver OrderedDict to normal dictionary
# ============================================================
def to_dict(input_ordered_dict):
	return loads(dumps(input_ordered_dict))

# Translate error message from API to UI
def _translate_error(err):
	if err == 'Domain cannot use apis.':
		return 'Admin API access is not enabled for your domain. Please <a target="_blank" href="http://support.google.com/a/bin/answer.py?hl=en&answer=60757">enable admin API</a>.'# in Google Apps Control Panel.
	#
	elif err == 'Not Authorized to access this resource/api':
		return 'You are not authorized to install Prodoscore on your domain. Please contact your domain admin'
	#
	else:
		return err


# ========================================================================
# Get user accounts in the domain - returns JSON object
# ========================================================================
def get_accounts(request):
	if 'email' not in request.session.keys() or 'domain' not in request.session.keys():
		return HttpResponse('Access Denied')
	proapp.cron.log('User: '+request.session['email'], 'info')
	# Needs to verify if admin
	#
	domain_name = request.session['domain']#'dedemed.com'#
	domain_admin = request.session['email']#'denise@dedemed.com'#
	data_output = {'self': domain_admin}
	#
	# Check if users are already imported
	domain_check = Domain.objects.filter(title=domain_name)
	if len(domain_check) > 0:
		onboard_check = Onboard.objects.get(domain_id=domain_check[0].id)
		if onboard_check.status >= 3:
			emp_check = Employee.objects.filter(domain=domain_check[0])
			data_output['users'] = []
			for employee in emp_check:
				data_output['users'].append({'id': employee.profileId, 'email': employee.email, 'fullname': employee.fullname, 'picture': employee.picture, 'status': employee.status, 'role': employee.role})
	#
			return HttpResponse(json.dumps(data_output), content_type = 'application/json')
		else:
			return HttpResponse(json.dumps({'error': 'ERROR'}), content_type='application/json')


# ========================================================================
# Receive organization time setings - store in session
# ========================================================================
def time_settings(request):
	if 'email' not in request.session.keys() or 'domain' not in request.session.keys():
		return HttpResponse('Access Denied')
	proapp.cron.log('User: '+request.session['email'], 'info')
	# Needs to verify if admin
	#
	domain_check = Domain.objects.filter(title=request.session['domain'])
	if 'origin' in request.session.keys():
		domain_origin = request.session['origin']
	else:
		domain_origin = 'gsuite'
	if len(domain_check) > 0:
		domain_check = domain_check[0]
		if request.method=='POST':
			received_json_data = json.loads(request.body)
			domain_check.daystart = received_json_data['daystart']
			domain_check.dayend = received_json_data['dayend']
			domain_check.workingdays = received_json_data['workingdays']
			domain_check.timezone = received_json_data['timezone']
			#
			domain_check.save()
			#
			# Anchor to the next step
			onboard_check = Onboard.objects.filter(domain=domain_check)
			if len(onboard_check) > 0:
				onboard_check[0].status = 2
				onboard_check[0].save()
			# Start adding employee records to the db
			url = '/register/add-employees/?domain={0}&email={1}&origin={2}'.format(request.session['domain'], request.session['email'], domain_origin)
			if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
				# Add a taskque to process the file
				utcnow = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
				taskqueue.add(url=url, method='GET', queue_name='onboarding',
							  name='add_employee_db_records_for_' + request.session['domain'].split('.')[0] + '_' + utcnow.replace(' ', '-').replace(':', '-'))
				logging.info("Taskque added to add employee records to the db")
			else:
				req = proapp.cron.GetReq({'domain': request.session['domain'], 'email': request.session['email'], 'origin': domain_origin, 'taskqueue': 'false'})
				add_employees(req)
			return HttpResponse(json.dumps({'message': 'Record Updated'}), content_type = 'application/json')
		else:
			# in case user logs out during this stage we still have to add user records
			req = proapp.cron.GetReq({'domain': request.session['domain'], 'email': request.session['email'], 'origin': domain_origin, 'taskqueue': 'false'})
			add_employees(req)
			return HttpResponse(json.dumps({'daystart': domain_check.daystart, 'dayend': domain_check.dayend, 'workingdays': domain_check.workingdays, 'timezone': domain_check.timezone, }), content_type = 'application/json')
	else:
		return HttpResponse(json.dumps({'message': 'Domain not found. Please goto Step 1'}), content_type = 'application/json')


# ========================================================================
# BEFORE STEP (-3-) 2 LOADING - Create employee records from Google domain users (if not exists)
# ========================================================================
def add_employees(request):
	logging.debug("executing add_employees request")
	if 'email' not in request.GET or 'domain' not in request.GET:
		logging.ERROR("add_employees called without either email or domain name")
		return HttpResponse('Access Denied')
	#proapp.cron.log('User: '+request.session['email'], 'info')
	# Needs to verify if admin
	#
	logging.info("running add_employees for domain {} with email {}".format(request.GET['domain'], request.GET['email']))
	admin_email = request.GET['email']
	domain_users = []
	if 'origin' in request.GET and request.GET['origin'] == 'office365':
		res = proapp.l1_interface.call(method = 'GET', path = '/office365/{0}/users'.format(request.GET['domain']))
		if res.status_code == 200 and 'error' in res.json() and res.json()['error'] == False:
			domain_users = res.json()['users']
	else:
		domain_users = proapp.cron.get_domain_users(request.GET['domain'], request.GET['email'])
	logging.info("user list length : {}".format(len(domain_users)))
	domain_check = Domain.objects.filter(title=request.GET['domain'])
	onboard_check = Onboard.objects.get(domain_id=domain_check[0].id)
	if not onboard_check.status == 2:
		logging.error("onboard_check.status != 2")
		return HttpResponse('Not in the correct state')
	if len(domain_check) > 0:
		domain_check = domain_check[0]
		with transaction.atomic():
			# Adding the admin record
			admin = Employee.objects.filter(email=admin_email)
			if len(admin) == 0:
				logging.info("adding admin record")
				admin_details = get_user_detail(admin_email, admin_email)
				if admin_details:
					try:
						admin = Employee(
							domain=domain_check, email=admin_email, last_login_domain_name=domain_check.title,
							report_email=admin_email,
							manager_id=52, report_enabled=1000,
							status=(0 if admin_details['suspended'] else 1),
							level=(1 if admin_details['isAdmin'] else 0),
							admin=(1 if 'isAdmin' in admin_details and admin_details['isAdmin'] else 0),
							profileId=admin_details['id'],
							password='[NOPASSWORD]',
							role=80,
							fullname=admin_details['name']['fullName'],
							picture=admin_details['thumbnailPhotoUrl'] if 'thumbnailPhotoUrl' in admin_details else ' ')
						admin.save()
						logging.info("admin record added to the db")
					except DatabaseError as e:
						logging.error("Error while adding the admin record " + str(e.message))
				else:
					logging.error(" Error while adding the admin details")

			logging.info("processing domain user list")
			# adding employee records
			if 'origin' in request.GET and request.GET['origin'] == 'office365':
				for account_user in domain_users:
					if account_user['mail'] is not None:
						logging.debug("checking for user : {}".format(account_user['mail']))
						employee = Employee.objects.filter(email=account_user['mail'])
						if len(employee) == 0:
							employee = Employee(
								domain = domain_check,
								email = account_user['mail'],
								report_email = account_user['mail'],
								manager_id = 52,
								report_enabled = 1000,
								status = 1,
								level = (1 if account_user['mail'] == request.GET['email'] else 0),
								admin = 0,#(1 if account_user['isAdmin'] else 0),
								profileId = account_user['id'], password = '[NOPASSWORD]',
								fullname = account_user['displayName'],
								picture =  '')
							#
							if account_user['mail'] == request.GET['email']:
								employee.role = 80
							else:
								employee.role = 0
							#
							employee.save()
							logging.debug("added new user : {}".format(account_user['mail']))
			else:
				for account_user in domain_users:
					logging.debug("checking for user : {}".format(account_user['primaryEmail']))
					employee = Employee.objects.filter(email=account_user['primaryEmail'])
					if len(employee) == 0:
						employee = Employee(
							domain = domain_check, email = account_user['primaryEmail'],
							report_email = account_user['primaryEmail'],
							manager_id = 52, report_enabled = 1000,
							status = (0 if account_user['suspended'] else 1),
							level = (1 if account_user['isAdmin'] else 0),
							admin = (1 if account_user['isAdmin'] else 0),
							profileId = account_user['id'], password = '[NOPASSWORD]',
							fullname = account_user['name']['fullName'],
							picture = account_user['thumbnailPhotoUrl'] if 'thumbnailPhotoUrl' in account_user else '')
						#
						if account_user['primaryEmail'] == request.GET['email']:
							employee.role = 80
						else:
							if account_user['suspended']:
								employee.role = -1
							elif account_user['isAdmin']:
								employee.role = 80
							else:
								employee.role = 0
						#
						employee.save()
						logging.debug("added new user : {}".format(account_user['primaryEmail']))
					#
			onboard_check = Onboard.objects.get(domain_id=domain_check.id)
			onboard_check.status = 3  # To make sure that all the employee records are added to the db
			onboard_check.save()
	#
	return HttpResponse(json.dumps({'message': 'Accounts Created', 'status': 3}), content_type = 'application/json')


# =============================================
# Get user detail given the user email
# returns the user detail got from the request
# =============================================
def get_user_detail(user_email, admin_email):
	delegated_credentials = credentials_dom.create_delegated(admin_email)
	http_auth = delegated_credentials.authorize(Http())
	try:
		service = build('admin', 'directory_v1', http=http_auth)
		user_detail = service.users().get(userKey=user_email).execute()
	except HttpAccessTokenRefreshError as e:
		user_detail = None
		logging.error("HttpAccessTokenRefreshError " + str(e.message))
	except Exception as e:
		if e.resp.status in [404, 403]:
			logging.error("HttpError " + str(e.content))
		user_detail = None
	return user_detail


# ========================================================================
# Update Prodoscore user accounts for the domain
# 	Returns JSON object
# ========================================================================
def update_employees(request):
	if 'email' not in request.session.keys() or 'domain' not in request.session.keys():# or 'domain_users' not in request.session.keys()
		return HttpResponseRedirect(request.build_absolute_uri('/')+'register/')
	# Needs to verify if admin

	current_domain = Domain.objects.filter(title=request.session['domain'])
	if len(current_domain) > 0:
		current_domain = current_domain[0]

		# ==================================================================
		# Iterate through domain employees list
		# ==================================================================
		employees = Employee.objects.filter(domain=current_domain)
		activated = Employee.objects.filter(domain=current_domain, role__gt=0).count()
		remaining = trialSeats - activated
		for employee in employees:
			# ===============================================================
			# Update employee roles according to selected
			# ===============================================================
			if 'employee['+employee.profileId+']' in request.POST:
				newRole = request.POST['employee['+employee.profileId+']']
				if employee.role < 0 and newRole > 0:
					remaining -= 1
				#
				if remaining > -1:
					employee.role = newRole
					employee.save()
				else:
					pass	#	OVER PROVISION ATTEMPT OVERRIDING CLIENT-SIDE VALIDATION
		#
		# Anchor to the next step
		onboard_check = Onboard.objects.filter(domain=current_domain)
		if len(onboard_check) > 0:
			onboard_check[0].status = 4
			onboard_check[0].save()
		#
		cron_stats = len(Statistic.objects.filter().extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE domain_id = '+str(current_domain.id)+')']))
		# SCHEDULE CRON TASKS
		return HttpResponse(json.dumps({'message': 'User accounts updated', 'cron_stats': cron_stats}), content_type = 'application/json')
	else:
		return HttpResponse(json.dumps({'message': 'Domain not found. Please goto Step 1'}), content_type = 'application/json')


# ========================================================================
# Assign employees to managers
# 	Returns JSON object
# ========================================================================
def assign_managers(request):
	if 'email' not in request.session.keys() or 'domain' not in request.session.keys():# or 'domain_users' not in request.session.keys()
		return HttpResponseRedirect(request.build_absolute_uri('/')+'register/')
	# Needs to verify if admin

	current_domain = Domain.objects.filter(title=request.session['domain'])
	if len(current_domain) > 0:
		current_domain = current_domain[0]

	# =====================================================================
	# Iterate through received employees list
	# =====================================================================
	received_json_data = json.loads(request.body)
	for user in received_json_data:
		if received_json_data[user] != '52':
			employee = Employee.objects.filter(profileId=user)[0]
			manager = Employee.objects.filter(profileId=received_json_data[user])
			if len(manager) > 0:
				employee.manager = manager[0]
			else:
				employee.manager_id = 52
			#
			employee.save()
	#
	# Anchor to the next step
	onboard_check = Onboard.objects.filter(domain=current_domain)#, status=4
	if len(onboard_check) > 0:
		if onboard_check[0].status == 4:
			onboard_check[0].status = 5
			onboard_check[0].save()
			#
			proapp.cron.schedule_onboard_task(current_domain.id)
			#
			# Send email
			admin = Employee.objects.filter(email=request.session['email'])[0]
			try:
				res = render(request, 'email/welcome-aboard-1.html', {'fullname': admin.fullname})
				requests.post(
					settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']),
					data={"from": settings.MAILGUN['FROM'],
						"to": [admin.email],
						"bcc": ["support@prodoscore.com"],
						"subject": "Welcome Aboard",
						"html": res})
			except Exception as e:
				logging.error("Error sending welcome email" + str(e.message))
			#
			# Release session data & Log-out the user
			for key in request.session.keys():
				del request.session[key]
			request.session.flush()
			#
			return HttpResponse(json.dumps({'message': 'Managers are assigned'}), content_type = 'application/json')
		else:
			return HttpResponse(json.dumps({'message': 'Domain not found. Please goto Step 1'}), content_type = 'application/json')
	else:
		return HttpResponse(json.dumps({'message': 'Domain not found. Please goto Step 1'}), content_type = 'application/json')


# Show progress of onboarding process
def register_progress(request):
	if 'email' not in request.session.keys() or 'domain' not in request.session.keys():
		# First we need to authenticate the user
		return HttpResponseRedirect(request.build_absolute_uri('/')+'sign-in/')
	#
	employee = Employee.objects.filter(email=request.session['email'])
	if len(employee) == 0:
		return HttpResponseRedirect(request.build_absolute_uri('/')+'sign-in/')
	#
	domain = Domain.objects.filter(title=request.session['domain'])
	onboard = Onboard.objects.filter(domain=domain)
	if len(onboard) == 0:
		return HttpResponseRedirect(request.build_absolute_uri('/')+'sign-in/')
	elif onboard[0].status > 4 and onboard[0].status < 7:
		fromdate = onboard[0].started_on - timedelta(days = 93)
		return render(request, 'onboard-progress.html', {'fromdate': fromdate.strftime('%Y-%m-%d'), 'domain_id': domain[0].id, 'email': request.session['email']})
	else:
		return HttpResponseRedirect(request.build_absolute_uri('/')+'#dashboard')



def upload_user_file(request):
	"""
	Save the file uploaded by the user once verified for data validation at the front
	end  and  add a taskque to process the data in the file once it is saved
	"""
	# If domain name available in request
	if 'domain' in request.session.keys() and 'email' in request.session.keys():
		if request.method == 'POST':
			# If file available in request
			if 'file' in request.POST:
				logging.info("File available in the request")
				uploaded_file = request.POST['file']  # Get the file from the request
				#
				uploaded_file = base64.b64decode(uploaded_file)
				# Set the url of the path to where the file is to be added
				bucket_name = settings.VERSIONS['file_bucket'][settings.VERSIONS['this']]
				file_path = '/{0}/{1}'.format(bucket_name, request.session['domain'])
				file_name_start = request.session['domain'].split('.')[0]
				if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
					latest_file = '{}-latest.xlsx'.format(file_name_start)
					try:
						# A file already exists in the bucket
						gcs.stat('{}/{}'.format(file_path, latest_file))
						# rename the existing file
						gcs.copy2('{}/{}'.format(file_path, latest_file), '{}/{}{}.xlsx'.format(file_path, file_name_start, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))

					except gcs.NotFoundError:
						pass
					gcs_file = gcs.open('{}/{}'.format(file_path, latest_file), 'w',
										content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
				else:
					directory = os.path.dirname(os.path.dirname(os.path.dirname(__file__))) + file_path
					if not os.path.exists(directory):
						logging.info("directory created" + directory)
						os.makedirs(directory)  # Create folder
					gcs_file = open('{0}/{1}-latest.xlsx'.format(directory, file_name_start), 'w')
				gcs_file.write(uploaded_file)
				gcs_file.close()
				logging.info("File saved successfully")
				url = '/process-file/?domain={0}&email={1}&retry=0'.format(request.session['domain'], request.session['email'])
				if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
					# Add a taskque to process the file
					utcnow = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
					taskqueue.add(url=url, method='GET', queue_name='add-user-records', name='user_records_for_' + file_name_start + '_' + utcnow.replace(' ', '-').replace(':', '-'))
					# Return SUCCESS to the front end
				else:
					req = proapp.cron.GetReq({'domain': request.session['domain'], 'email': request.session['email'], 'retry': 0, 'taskqueue': 'false', 'scheme': request.is_secure(), 'host': request.get_host()})
					process_user_file(req)

				return HttpResponse(json.dumps({'message': 'File uploaded'}), status=200, content_type='application/json')  # Return Upload SUCCESS
			else:
				logging.error("File not found in the request")
				return HttpResponse(json.dumps({'error': 'File Not Found'}), status=404, content_type='application/json')

		else:
			logging.info("redirecting to the file upload page")
			return render(request, 'includes/upload_xlsx.html', {})
	else:
		logging.error("Domain information and admin email not sent in the request")
		return HttpResponse(json.dumps({'error': 'Bad Request'}), status=400, content_type='application/json')


def process_user_file(request):
	""" Process the uploaded file and verify data in the file"""
	import xlrd
	# change to session
	if 'domain' in request.GET and 'email' in request.GET:
		domain = Domain.objects.get(title=request.GET['domain'])
		onboarded = Onboard.objects.get(domain_id=domain.id)
		if onboarded.status == 3:  # Employee records have successfully added to the db
			# Set the URL to read the file
			file_path = '/' + settings.VERSIONS['file_bucket'][settings.VERSIONS['this']] + '/' + request.GET['domain']
			# Open the file from the Google Storage Bucket
			error_dic, save_to_db = {}, {}
			# get the saved data from the db
			user_list = Employee.objects.filter(domain_id=domain.id)
			db_emails = [user.email.lower() for user in user_list]  # list down the emails of users. convert to lowercase for comparison
			error_dic['file_validity'] = True  # validation of the file content
			try:
				if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
					file = gcs.open(file_path + '/' + request.GET['domain'].split('.')[0] + '-latest.xlsx', 'r')
					file_read = xlrd.open_workbook(file_contents=file.read())
				else:
					directory = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))+os.path.dirname(file_path)
					fcode = file_path[len(os.path.dirname(file_path)):]
					file_read = xlrd.open_workbook(filename=directory+fcode+'/'+fcode.split('.')[0]+'-latest.xlsx')
				file_data = file_read._sheet_list[0]._cell_values
			except Exception as e:
				logging.error(e.strerror)
				return HttpResponse(json.dumps({'error': 'File Not Found'}), status=404, content_type='application/json')

			if not file_data:  # empty sheet
				error_dic['file_validity'] = False
				error_dic['no_data'] = True
				logging.error("File data not available")
			else:
				template_set = {'count', 'employee name', 'employee email', 'user role', 'manager name', "manager's email address"}  # standard template
				column_list = file_data[0]  # first row of cells should contain table heading
				file_columns = [column.lower() for column in column_list] if column_list else [] # convert all the column names to lowercase for comparison

				# compare with the standard template columns
				if template_set - set(file_columns):
					error_dic['file_validity'] = False  # A difference in file columns. file INVALID
					# Add the set of missing columns to the error dictionary
					error_dic['missing_columns'] = [x.title() for x in list(template_set - set(file_columns))]

				# ------user role verification------ #
				# get the position of the user role column
				user_role_pos = file_columns.index('user role') if 'user role' in file_columns else None
				if user_role_pos:
					# get the lower cased user roles from the role_dic
					prod_roles = [role.lower() for role in proapp.cron.role_dic.values()]  # get the role dictionary from cron.py
					# compare the user roles of the file against the prodoscore roles
					compare_lists(prod_roles, file_data, file_columns, 'user role', error_dic, 'role_errors')
					if error_dic['role_errors']['blanks']:  # user roles cannot be blank
						error_dic['file_validity'] = False

				# ------employee data verification------ #
				# get the position of the employee email column
				employee_email_pos = file_columns.index('employee email') if 'employee email' in file_columns else None
				if employee_email_pos:
					# check with the users already in the db
					user_emails = compare_lists(db_emails, file_data, file_columns, 'employee email', error_dic, 'employee_email_errors')
					# check for duplicates
					error_dic['employee_duplicates'] = []
					if len(user_emails.values()) > len(set(user_emails.values())):  # duplicates present
						for key, value in user_emails.iteritems():
							if user_emails.values().count(value) != 1:
								error_dic['employee_duplicates'].append([key, value])
					save_to_db['employees'] = user_emails.values()  # non-blank unique fields under employee emails
					# validate the email format
					validate_emails(error_dic, 'employee_email_format', user_emails)
					# check for mandatory fields like employee name, email and user role
					check_blanks(error_dic, 'user_email_role_absent', 'employeename', 'employeeemail', file_columns, file_data)

				# ------manager data verification------ #
				# get the column position of "manager's email address"
				manager_pos = file_columns.index("manager's email address") if "manager's email address" in file_columns else None
				if manager_pos:
					# check with available users
					manager_emails = compare_lists(db_emails, file_data, file_columns, "manager's email address", error_dic, 'manager_email_errors')
					save_to_db['manager_emails'] = list(set(manager_emails.values()))  # non-blank unique fields under manager emails
					# validate the email format
					validate_emails(error_dic, 'manager_email_format', manager_emails)
					check_blanks(error_dic, 'manager_detail_absent', "managername", "managersemailaddress", file_columns, file_data)
					error_dic['manager_role_errors'] = []
					error_dic['manager_assigned_manager'] = []
					# check whether each manager employee is added with a role 'manager'
					for manager in save_to_db['manager_emails']:
						# availability of both user role and employee email columns in the file
						if user_role_pos and employee_email_pos:
							for row in file_data[2:]:  # traverse file data exclude the first two rows
								# manager in the employee column but their role is not 'Manager'/'Administrator'
								if manager == row[employee_email_pos] and row[user_role_pos].lower() not in ['manager', 'administrator']:
									error_dic['manager_role_errors'].append([num2alpha(employee_email_pos) + str(file_data.index(row) + 1), manager])
								# if the employee is assigned manager of themselves
								if manager == row[manager_pos] and manager == row[employee_email_pos]:
									error_dic['manager_assigned_manager'].append(
										[num2alpha(employee_email_pos) + str(file_data.index(row) + 1), row[employee_email_pos]])

					# if at least one manager is assigned an incorrect role
					if error_dic['manager_role_errors'] or error_dic['manager_assigned_manager'] or error_dic['employee_duplicates']:
						error_dic['file_validity'] = False

			error_dic['admin'] = Employee.objects.get(email=request.GET['email'])
			# file content INVALID
			if not error_dic['file_validity']:
				# revert the process of onboarding
				onboarded.status = 3
				onboarded.save()
				# send an email containing the errors

				try:
					res = render(request, 'email/file-data-error.html', error_dic)
					requests.post(settings.MAILGUN['API_ENDPOINT'],
							auth=("api", settings.MAILGUN['API_KEY']),
							data={"from": settings.MAILGUN['FROM'],
									"to": [request.GET['email']],
									"bcc": ["support@prodoscore.com"],
									"subject": "Errors in user data file",
									"html": res})
				except Exception as e:
					logging.error("Error sending email " + str(e.message))
				logging.error('Unprocessable File Content')
				return HttpResponse(json.dumps({'error': 'Unprocessable File Content'}), status=200, content_type='application/json')

			else:
				activated = Employee.objects.filter(domain=domain, role__gt=0).count()
				remaining = trialSeats - activated
				#
				manager_ids = {}  # to note down the manager ids
				manager_for_employee = {}  # to note down the manager for each employee
				employee_user_role = {}  # to note down the user role for each employee
				for row in file_data[2:]:
					employee_user_role[row[employee_email_pos].lower()] = row[user_role_pos].lower()
					for manager in save_to_db['manager_emails']:
						if row[manager_pos].lower() == manager:
							manager_for_employee[row[employee_email_pos].lower()] = manager
				# get the employee objects for the managers
				managers = Employee.objects.filter(email__in=save_to_db['manager_emails'])
				for manager in managers:  # assign manager roles for managers
					if manager.role < 0:
						remaining -= 1
					#
					if remaining > -1:
						manager.role = 70
						manager_ids[manager.email.lower()] = manager.id
						manager.save()
					else:
						pass	#	OVER PROVISION ATTEMPT OVERRIDING CLIENT-SIDE VALIDATION
				# get the employee objects for the employees
				employees = Employee.objects.filter(email__in=save_to_db['employees'])
				for employee in employees:  # assign managers and the user roles
					employee.manager_id = manager_ids[manager_for_employee[employee.email.lower()]] if employee.email.lower() in manager_for_employee.keys() else 52
					if employee.role < 0:
						remaining -= 1
					#
					if remaining > -1:
						for key, value in proapp.cron.role_dic.iteritems():
							if value.lower() == employee_user_role[employee.email.lower()]:
								employee.role = key
						employee.save()
					else:
						pass	#	OVER PROVISION ATTEMPT OVERRIDING CLIENT-SIDE VALIDATION
				# Change the onboard status that it has been successfully assigned roles and managers
				onboarded.status = 5
				onboarded.save()
				proapp.cron.schedule_onboard_task(domain.id)
				logging.info("database records updated")

				try:
					res = render(request, 'email/welcome-aboard-1.html', {'fullname': error_dic['admin']})
					requests.post(
						settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']),
						data={"from": settings.MAILGUN['FROM'],
							"to": [request.GET['email']],
							"bcc": ["support@prodoscore.com"],
							"subject": "Welcome Aboard",
							"html": res})
				except Exception as e:
					logging.error("Welcome email not sent " + str(e.message))
				logging.info("Welcome email sent")

				return HttpResponse(json.dumps({'message': 'Database Records Updated'}), status=200, content_type='application/json')

		elif onboarded.status > 3:
			pass

		else:  # Records are still not created. Have to add a new task

			count = int(request.GET['retry']) + 1  # times the same task was added
			url = '/process-file/?domain={0}&email={1}&retry={2}'.format(request.GET['domain'], request.GET['email'], count)
			if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
				# Add a taskque to process the file
				utcnow = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
				taskqueue.add(url=url, method='GET', queue_name='add-user-records', name='user_records_for_' + request.GET['domain'].split('.')[0] + '_' + utcnow.replace(' ', '-').replace(':', '-'))
			else:
				req = proapp.cron.GetReq({'domain': request.GET['domain'], 'email': request.GET['email'], 'retry': count,
					 'taskqueue': 'false'})
				process_user_file(req)
			return HttpResponse(json.dumps({'message': 'Added a new queue'}), status=200, content_type='application/json')
	else:
		logging.error("Domain information and admin email not sent in the request")
		return HttpResponse(json.dumps({'error': 'Bad Request'}), status=400, content_type='application/json')


def validate_emails(error_dic, main_key, user_emails):
	"""
	Validate the format of emails in a given list and add to a dictionary under two keys as '@' and '.'
	:param error_dic: A dictionary to add errors
	:param main_key: key of the error_dic
	:param user_emails:  the list with emails
	:return:
	"""
	error_dic[main_key] = []
	errors = {'at_sign': [], 'dot_sign': []}
	for key, email in user_emails.iteritems():
		if email.lower() != 'none':
			if len(email.split('@')) != 2:  # an email contain one '@' sign hence splitting an email to two
				errors['at_sign'].append([key, email.lower()])
				continue
			# The domain part may contain one or two '.' e.g 123@abc.co.uk vs 123@xyz.com
			if len(email.split('@')[1].split('.')) not in [2, 3]:
				errors['dot_sign'].append([key, email.lower()])
	error_dic[main_key] = errors['dot_sign'] + errors['at_sign']
	if error_dic[main_key]:
		error_dic['file_validity'] = False


def num2alpha(number):
	"""
	Returns the corresponding Engish letter to a number given. can be improved to return two or more letters
	when the input is greater than 26
	:param number:
	:return:
	"""
	import string
	if number < 27:
		number_to__letter = dict(zip(range(0, 26), string.ascii_uppercase))
		return number_to__letter[number]


def check_blanks(error_dic, error_key, key1, key2, file_columns, file_data):
	"""
	Check blanks in mandatory fields or check whether there are blanks in corresponding data columns
	e.g: Managers email cannot be Null given the Manager name
	:param error_dic: A dictionary to hold the errors
	:param error_key: Specific name for the type of error
	:param key1:
	:param key2:
	:param file_columns:
	:param file_data:
	:param kwargs: Used in case we have to compare three fields
	:return:
	"""
	file_cols = [column.replace("'", '').replace(" ", "") for column in file_columns]
	error_dic[error_key] = {key1: [], key2: []}
	key1_pos = file_cols.index(key1) if key1 in file_cols else None
	key2_pos = file_cols.index(key2) if key2 in file_cols else None
	user_role_pos = file_cols.index('userrole')
	# if columns [key1 and key2] available in file_columns
	if key1_pos and key2_pos:
		for row in file_data[2:]:
			# Track the position of the cells where mandatory data fields are missing
			if (row[key1_pos] != '' and not row[key2_pos] != '') or (not row[key1_pos] != '' and row[key2_pos] != ''):
				if row[key1_pos] == '':
					error_dic[error_key][key1].append(num2alpha(key1_pos) + str(file_data.index(row)+1))
				if row[key2_pos] == '':
					error_dic[error_key][key2].append(num2alpha(key2_pos) + str(file_data.index(row)+1))
			elif row[user_role_pos] != '' and row[key1_pos] == '' and row[key2_pos] == '' and error_key == 'user_email_role_absent':
				error_dic[error_key][key1].append(num2alpha(key1_pos) + str(file_data.index(row)+1))
				error_dic[error_key][key2].append(num2alpha(key2_pos) + str(file_data.index(row)+1))
		if error_dic[error_key][key2]:  # employee email or manager email absent
			error_dic['file_validity'] = False


def compare_lists(prod_values, file_data, file_columns, key, error_dic, key2):
	"""
	Compare two lists with strings
	:param prod_values: List one. This is the correct/ value set
	:param file_data:
	:param file_columns:
	:param key: the original column name
	:param error_dic: the dictionary that holds the errors
	:param key2: The appropriate key passed by the user to add into the error dic
	:return:
	"""
	user_data = {}
	error_dic[key2] = {'blanks': [], 'extra': []}
	key_position = file_columns.index(key)
	count = 0  # Number of empty rows
	x = file_data[2:]
	for row in file_data[2:]:
		if len(set(row)) <= 3:  # First/Empty rows
			count += 1
			if count > 3:  # To stop the loop traversing through a file with so many empty lines
				break
			continue
		if row[key_position] == '':
			error_dic[key2]['blanks'].append(num2alpha(key_position) + str(file_data.index(row)+1))
		elif row[key_position].lower() != 'none':
			y = str(file_data.index(row)+1)
			user_data[num2alpha(key_position) + str(file_data.index(row)+1)] = row[key_position].lower()
	for key, value in user_data.iteritems():
			if value not in prod_values:
				error_dic['file_validity'] = False
				error_dic[key2]['extra'].append([key, value])
	#
	return user_data


# =================
# Get domain list
# =================
def get_domain_list(email):
	# The process of checking whether secondary domains are present within the G Suite acc.
	# Use the HTTP authentication and open Directory API service entry to the domain
	delegated_credentials = credentials_dom.create_delegated(email)
	http_auth = delegated_credentials.authorize(Http())
	service = build('admin', 'directory_v1', http=http_auth)
	#
	customer_id = service.users().get(userKey=email).execute()
	customer_id = customer_id['customerId']
	sec_domains = service.domains().list(customer=customer_id).execute()
	domains = []
	if 'domains' in sec_domains:
		for domain in sec_domains['domains']:
			domains.append(domain['domainName'])
	#
	return domains


# Authenticate against Microsoft SSO
def microsoft_auth(request):
	"""
	Authenticate against Microsoft SSO
		:param request: 
	"""
	import re
	client_id = '72db758c-898e-44e1-9737-70c15626e430'
	client_secret = 'v18WLu3_M0=md.+0C_oWJOD2p6wdE]uO'
	authority = 'https://login.microsoftonline.com'
	authorize_url = '{0}{1}'.format(authority, '/common/oauth2/v2.0/authorize?{0}')
	token_url = '{0}{1}'.format(authority, '/common/oauth2/v2.0/token')
	scopes = [ 'openid','offline_access','User.Read' ]

	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		redirect_uri = 'https://{0}/microsoft-auth/'.format(request.get_host())
	else:
		redirect_uri = 'http://{0}/microsoft-auth/'.format(request.get_host())
	if 'code' in request.GET:
		#If there is already running onboarding for Gsuite
		if 'origin' in request.session.keys() and request.session['origin'] == 'gsuite' and 'domain' in request.session.keys():
			return render(request, 'login.html' , {'error' : 'There is a different domain onboarding. Complete that task or Sign out'})
		auth_code = request.GET['code']
		post_data = { 'grant_type': 'authorization_code',
			'code': auth_code,
			'redirect_uri': redirect_uri,
			'scope': ' '.join(str(i) for i in scopes),
			'client_id': client_id,
			'client_secret': client_secret
		}
		r = requests.post(token_url, data = post_data)
		if r.status_code == 200:
			office_365_tokens = r.json()
			request.session['office_365_tokens'] = office_365_tokens
			user_res = proapp.products.office_365.get_me(office_365_tokens['access_token'])
			if 'failed' in user_res:
				return  render(request, 'login.html', {'error': user_res['text']})
			if not ('mail' in user_res) or user_res['mail'] == None :
				return render(request, 'login.html' , {'error' : 'You must be an administrator of a office 365 domain to setup Prodoscore.'})
			#
			# Check if employee is registered
			domain = re.search('@[\w.]+', user_res['mail']).group()[1:]
			employee_check = Employee.objects.filter(email=user_res['mail'], role__gt=0)
			domain_check = Domain.objects.filter(title=domain)
			request.session['origin'] = 'office365'
			if len(employee_check) == 0:
				# No record - Check if at least domain is registered
				request.session['domain'] = domain
				request.session['email'] = user_res['mail']
				if len(domain_check) == 0:
				# User from a new domain - Let's get them on-board.!
					return HttpResponseRedirect(request.build_absolute_uri('/')+'register/')
				else:
					onboard_check = Onboard.objects.filter(domain=domain_check[0])
					if onboard_check[0].status == 0:  # the user has logged out during the first two steps
						return HttpResponseRedirect(request.build_absolute_uri('/') + 'register/#step-' + str(onboard_check[0].status))

					# User not recognized - have to notify admin to get them in. ## Is this possible.?
					# return HttpResponseRedirect(request.build_absolute_uri('/')+'sign-in/?error=Account not found. We will contact your domain admin and get back to you by email.')
					else:
						return render(request, 'login.html', {'error': 'Account not found.'})# We will contact your domain admin and get back to you by email. # THERE IS NO SUCH MECHANISM NOW..
					##	SEND EMAIL TO ADMIN ASKING PERMISSION FOR ACCESS
			else:
				#if len(domain_check) == 0:
				# get domain list of the user
					#user_domains = get_domain_list(user_res['mail'])
					# get user's domain from the db
					#user_db_domain = employee_check[0].domain
					#if user_db_domain.title in user_domains:
						# obtain the new Queryset (domain_check)
						#domain_check = [user_db_domain]
				if len(domain_check) > 0:
					onboard_check = Onboard.objects.filter(domain=domain_check[0])
					if len(onboard_check) > 0 and onboard_check[0].status < 7:
						request.session['domain'] = domain
						request.session['email'] = user_res['mail']
						if onboard_check[0].status < 5:
							return HttpResponseRedirect(request.build_absolute_uri('/')+'register/#step-'+str(onboard_check[0].status))
						else:
							return HttpResponseRedirect(request.build_absolute_uri('/')+'register/progress')
					request.session['id'] = employee_check[0].id
					request.session['domain'] = domain
					request.session['email'] = user_res['mail']
					employee_check[0].last_login = datetime.datetime.now()
					employee_check[0].save()
					mgr_check = Employee.objects.filter(manager_id=employee_check[0].id)
					if len(mgr_check) > 0 or employee_check[0].role == 80:
						return HttpResponseRedirect(request.build_absolute_uri('/')+'#dashboard')
					elif employee_check[0].role > 0:
						return HttpResponseRedirect(request.build_absolute_uri('/')+'dashboard')
					else:
						return render(request, 'login.html', {'error': 'Your user account is terminated.'})
				else:	# Impossible - but possible
					return render(request, 'login.html', {'error': 'Domain not found.'})
		else:
			return  render(request, 'login.html', {'error': 'Account not found.'}) 
	else:
		params = { 'client_id': client_id,
				'redirect_uri': redirect_uri,
				'response_type': 'code',
				'scope': ' '.join(str(i) for i in scopes)
				}
		office_365_sign_in_url = authorize_url.format(urlencode(params))
		return HttpResponseRedirect(office_365_sign_in_url)
	return render(request, 'login.html', {'error': 'TO DO'})


# Authenticate against Google SSO
def google_auth(request):
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		CLIENT_ID = '500114080071-i1rkhl4kfuir8q1r8qeojo51n7p4mn54.apps.googleusercontent.com'
		CLIENT_SECRET = 'BN5160v-3mVZZiZg36s-BLzt'
		REDIRECT_URI = 'https://'+request.get_host()+'/google-auth/'
	else:
		CLIENT_ID = '500114080071-f2o6jt04d1a90disra7a8862jinsru6h.apps.googleusercontent.com'
		CLIENT_SECRET = 'QnxxCiK2TetwmYZzZOzsSenK'
		REDIRECT_URI = 'http://'+request.get_host()+'/google-auth/'
	#print(request.GET)
	#
	if 'error' in request.GET:
		# Consent declined - no authentication
		return HttpResponseRedirect(request.build_absolute_uri('/')+'sign-in/?error='+request.GET['error'])
	elif 'code' in request.GET:
		#If there is already running onboarding for office365
		if 'origin' in request.session.keys() and request.session['origin'] == 'office365' and 'domain' in request.session.keys():
			return render(request, 'login.html' , {'error' : 'There is a different domain onboarding. Complete that task or Sign out'})
		# Consent accepted - proceed
		data = {	'code': request.GET['code'],
				'client_id': CLIENT_ID,
				'client_secret': CLIENT_SECRET,
				'redirect_uri': REDIRECT_URI,
				'grant_type': 'authorization_code'}
		#
		req = urllib2.urlopen('https://www.googleapis.com/oauth2/v4/token', urllib.urlencode(data))
		req = json.loads(req.read()).get('id_token').split('.')[1].encode('ascii', 'ignore')
		req = json.loads(base64.b64decode(req + '='*(-len(req)%4)))
		#
		# Check if employee is registered
		employee_check = Employee.objects.filter(email=req.get('email'), role__gt=0)
		domain_check = Domain.objects.filter(title=req.get('hd'))
		request.session['origin'] = 'gsuite'
		if len(employee_check) == 0:
			# No record - Check if at least domain is registered
			request.session['domain'] = req.get('hd')
			request.session['email'] = req.get('email')
			if len(domain_check) == 0:
				# User from a new domain - Let's get them on-board.!
				return HttpResponseRedirect(request.build_absolute_uri('/')+'register/')
			else:
				onboard_check = Onboard.objects.filter(domain=domain_check[0])
				if onboard_check[0].status == 0:  # the user has logged out during the first two steps
					return HttpResponseRedirect(request.build_absolute_uri('/') + 'register/#step-' + str(onboard_check[0].status))

				# User not recognized - have to notify admin to get them in. ## Is this possible.?
				#return HttpResponseRedirect(request.build_absolute_uri('/')+'sign-in/?error=Account not found. We will contact your domain admin and get back to you by email.')
				else:
					return render(request, 'login.html', {'error': 'Account not found.'})# We will contact your domain admin and get back to you by email. # THERE IS NO SUCH MECHANISM NOW..
				##	SEND EMAIL TO ADMIN ASKING PERMISSION FOR ACCESS
		else:
			if len(domain_check) == 0:
				# get domain list of the user
				user_domains = get_domain_list(req.get('email'))
				# get user's domain from the db
				user_db_domain = employee_check[0].domain
				if user_db_domain.title in user_domains:
					# obtain the new Queryset (domain_check)
					domain_check = [user_db_domain]
			if len(domain_check) > 0:
				onboard_check = Onboard.objects.filter(domain=domain_check[0])
				if len(onboard_check) > 0 and onboard_check[0].status < 7:
					request.session['domain'] = req.get('hd')
					request.session['email'] = req.get('email')
					if onboard_check[0].status < 5:
						return HttpResponseRedirect(request.build_absolute_uri('/')+'register/#step-'+str(onboard_check[0].status))
					else:
						return HttpResponseRedirect(request.build_absolute_uri('/')+'register/progress')
						#return render(request, 'onboard-progress.html', {'error': 'Initial data analysys for your domain is still running. We will get back to you by email once it is complete. Your patience is appriciated.'})
				request.session['id'] = employee_check[0].id
				request.session['domain'] = req.get('hd')
				request.session['email'] = req.get('email')
				employee_check[0].last_login = datetime.datetime.now()
				employee_check[0].save()
				mgr_check = Employee.objects.filter(manager_id=employee_check[0].id)
				if len(mgr_check) > 0 or employee_check[0].role == 80:
					return HttpResponseRedirect(request.build_absolute_uri('/')+'#dashboard')
				elif employee_check[0].role > 0:
					return HttpResponseRedirect(request.build_absolute_uri('/')+'dashboard')
				else:
					return render(request, 'login.html', {'error': 'Your user account is terminated.'})
			else:	# Impossible - but possible
				return render(request, 'login.html', {'error': 'Domain not found.'})
	else:
		# Release session data & Log-out the user
		for key in request.session.keys():
			del request.session[key]
		request.session.flush()
		#
		# Redirect to Google OAuth consent screen
		return HttpResponseRedirect('https://accounts.google.com/o/oauth2/v2/auth?response_type=code&'+
			'client_id='+CLIENT_ID+
			'&redirect_uri='+REDIRECT_URI+
			'&scope=https://www.googleapis.com/auth/userinfo.email&prompt=select_account')

# Delete all session data and cookies
def sign_out(request):
	for key in request.session.keys():
		del request.session[key]
	request.session.flush()
	return HttpResponseRedirect(request.build_absolute_uri('/')+'sign-in/')

# No access previllages
def limbo(request):
	return render(request, 'login.html', {'error': 'You do not have employees under you to view ProdoScore. Please contact your domain admin.'})


# Authenticate  Ringcentral
def rc_auth(request):
	CLIENT_ID = 'sK8jW2NjTaiECPsssU5GRw'
	CLIENT_SECRET = 'H2YxzaFnRhS_ZPKS_X6RTw14Akw7xbT2iNn2Q6Op4Q-A'
	REDIRECT_URI = request.build_absolute_uri('/')+'rc-auth/'
	employee = proapp.views._authorize(request)
	domain_id = employee['user'].domain_id
	if 'code' in request.GET.keys():
		# Consent accepted - proceed
		data = {	'code': request.GET['code'],
				'client_id': CLIENT_ID,
				'client_secret': CLIENT_SECRET,
				'redirect_uri': REDIRECT_URI,
				'grant_type': 'authorization_code'}
		#
		req = urllib2.urlopen('https://platform.ringcentral.com/restapi/oauth/token', urllib.urlencode(data))
		response = json.loads(req.read())
		response['domain_id'] = domain_id
		# send the data to the token service to save
		req = requests.post(token_url_rc, data=response)
		Domain.objects.filter(id=domain_id).update(phone_system='ringcentral')

		return render(request, 'rc_popup.html', {'result': 'success'})

	elif 'error' in request.GET.keys():
		# Consent declined - no authentication
		return render(request, 'rc_popup.html', {'result': request.GET['error']})
	else:
		# Send user to SSO endpoint
		return HttpResponseRedirect('https://platform.ringcentral.com/restapi/oauth/authorize?response_type=code'+
			'&client_id='+CLIENT_ID+'&redirect_uri='+REDIRECT_URI)


# Authenticate  VBC
def vbc_auth(request):
	CLIENT_ID = settings.VERSIONS['vbc_app'][settings.VERSIONS['this']][0]
	CLIENT_SECRET = settings.VERSIONS['vbc_app'][settings.VERSIONS['this']][1]
	REDIRECT_URI = request.build_absolute_uri('/')+'vbc-auth'
	employee = proapp.views._authorize(request)
	domain_id = employee['user'].domain_id
	if 'code' in request.GET.keys():
		# Consent accepted - proceed
		data = {'code': request.GET['code'],
				'client_id': CLIENT_ID,
				'client_secret': CLIENT_SECRET,
				'redirect_uri': REDIRECT_URI,
				'grant_type': 'authorization_code'}
		#
		# req = urllib2.urlopen('https://api.vonage.com/token', urllib.urlencode(data)) ##-# OLD END
		req = urllib2.urlopen('https://sso.vonage.com/oauth2/token', urllib.urlencode(data))
		response = json.loads(req.read())
		response['domain_id'] = domain_id

		# send the data to the token service to save
		req = requests.post(token_url_vbc, data=response)
		Domain.objects.filter(id=domain_id).update(phone_system='vbc')

		return render(request, 'vbc_popup.html', {'result': 'success'})

	elif 'error' in request.GET.keys():
		# Consent declined - no authentication
		return render(request, 'vbc_popup.html', {'result': request.GET['error']})
	else:
		# Send user to SSO endpoint
		return HttpResponseRedirect('https://api.vonage.com/authorize?scope=openid&response_type=code'+
			'&client_id='+CLIENT_ID+
			'&redirect_uri='+REDIRECT_URI)
