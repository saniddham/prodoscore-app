
import os, datetime, json, math, pytz
from pytz import timezone
from datetime import timedelta

from .models import Domain, Employee, Statistic, Product, Baseline, Employee_prodoscore, Org_weight, Organization_prodoscore

import proapp.cache

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import memcache
else:
	from django.core.cache import cache

prod_dic = {}
dorp_dic = {}

# NEW RBAs
products = {

	# ATTENDING EVENTS
	#'cl': ['calendar', {}, '#4466DD', 3],
	#'ze': ['zevents', {}, '#44FF66', 3],

	#  WRITING / DOCUMENTATION
	#'dc': ['docs', {}, '#66DDFF', 2],

	# WRITTEN COMMUNICATION / REACH-OUT-TO
	#'gm': ['gmail', {}, '#F04444', 5],

	# REAL-TIME-COMMUNICATION / TEXT
	#'ch': ['hangouts', {}, '#44D044', 2],
	#'sms': ['smss', {}, '#44FF66', 2],
	#'rcs': ['ringc_sms', {}, '#44FF66', 2],

	# VOICE COMMUNICATION
	#'tb': ['turbobridge', {}, '#44FF66', 4],
	#'bs': ['broadsoft', {}, '#FF6644', 4],
	#'zc': ['zcalls', {}, '#44FF66', 4],
	#'cll': ['calls', {}, '#44FF66', 4],
	#'rcc': ['ringc_calls', {}, '#44FF66', 4],

	# CRM TASKS
	#'zt': ['ztasks', {}, '#44FF66', 2],

	# PROSPECTING DEALS
	#'zl': ['zleads', {}, '#44FF66', 4],
	#'sf': ['salesforce', {}, '#44FF66', 4],
	#'sfl': ['sfleads', {}, '#44FF66', 4],
	#'po': ['pwoprtunts', {}, '#44FF66', 4],
	#'pl': ['pwleads', {}, '#44FF66', 4],

	# ACCOUNTING
	#'za': ['zaccounts', {}, '#44FF66', 3],
	#'zi': ['zinvoices', {}, '#44FF66', 3]

}

# Load baselines from database
dbProducts = Product.objects.using('replica').all().extra(where=['cat > 0'])
for product in dbProducts:
	products[product.code] = [product.slug, {}, '#'+product.hexccode, product.stdweight]
	prod_dic[product.slug] = product.code
	dorp_dic[product.code] = product.slug
	
baselines = Baseline.objects.using('replica').filter(domain_id=6)
for baseline in baselines:
	products[baseline.pcode][1][baseline.role] = baseline.baseline


# ========================================================================
# Calculates scores for employees matching given query for the given period
# ========================================================================
def scores(domain_id, user, query, fromdate, todate, options):
	workingdays = json.loads(Domain.objects.using('replica').values('workingdays').get(id=domain_id)['workingdays'])
	#
	# =====================================================================
	# Fetch users matching the query
	# =====================================================================
	users = Employee.objects.using('replica').all().extra(where=[query]).order_by('fullname')
	#
	# =====================================================================
	# Determine from and to dates that has data for
	# =====================================================================
	fromdate = datetime.datetime.strptime(fromdate, '%Y-%m-%d')
	todate = datetime.datetime.strptime(todate, '%Y-%m-%d')
	emp_scores_this = ( Statistic.objects.using('replica')
						.filter(date__range=(fromdate, todate))
						.extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE '+query+')'])
						.exclude(score__lt=0)).values('date').order_by('-date')
	if len(emp_scores_this) > 0:
		todate = datetime.datetime.strptime(emp_scores_this.latest()['date'], '%Y-%m-%d')
	#
	# =====================================================================
	# Fetch data for the previous same number of days period
	# =====================================================================
	prev = fromdate - timedelta(days=(todate - fromdate).days)
	prev_short = (int(prev.strftime('%s')) / 86400) - 16801
	fromdate_short = (int(fromdate.strftime('%s')) / 86400) - 16800
	todate_short = (int(todate.strftime('%s')) / 86400) - 16800
	#
	# Match week-over-week week days to same week days
	thsSWday = (fromdate_short+4) % 7
	while (prev_short+4) % 7 != thsSWday:
		prev_short -= 1;
	prevSWday = (prev_short+4) % 7
	wdayOffset = fromdate_short - prev_short
	#
	sum_array_prev = process_scores(domain_id, users, prev_short, prev_short+(todate_short-fromdate_short), workingdays, options)#doCache
	sum_array_this = process_scores(domain_id, users, fromdate_short, todate_short, workingdays, options)#doCache
	sum_array_this['prev_offset'] = wdayOffset
	#
	#	Fill employee details to score details
	for _user in users:
		if '/photos/private/' in _user.picture:
			_user.picture = ''
		if _user.id in sum_array_this['employees']:
			sum_array_this['employees'][_user.id]['fullname'] = _user.fullname
			sum_array_this['employees'][_user.id]['email'] = _user.email
			sum_array_this['employees'][_user.id]['role'] = _user.role
			sum_array_this['employees'][_user.id]['manager'] = _user.manager_id
			sum_array_this['employees'][_user.id]['status'] = 1 if _user.status == 1 else -1
			sum_array_this['employees'][_user.id]['picture'] = _user.picture
			if sum_array_prev['employees'][_user.id]['scr'] and sum_array_this['employees'][_user.id]['scr']:
				sum_array_this['employees'][_user.id]['scr']['delta'] = delta(sum_array_this['employees'][_user.id]['scr']['l'], sum_array_prev['employees'][_user.id]['scr']['l'])
			#
			days = sum_array_this['employees'][_user.id]['days'].keys()
			days.sort()
			#
			for i in range(0, len(days)):
				if days[i] - wdayOffset in sum_array_prev['employees'][_user.id]['days']:
					sum_array_this['employees'][_user.id]['days'][days[i]]['scr']['p'] = sum_array_prev['employees'][_user.id]['days'][days[i] - wdayOffset]['scr']['l']
		else:
			sum_array_this['employees'][_user.id] = {'fullname': _user.fullname, 'email': _user.email,#'days': {},
				'status': _user.status, 'role': _user.role, 'manager': _user.manager_id, 'picture': _user.picture }
	#
	if 'filter' in options and options['filter'] == 'appusers':
		for _user in users:
			sum_array_this['employees'][_user.id]['is_app_user'] = _user.is_app_user
	#
	sum_array_this['organization']['strata_delta'] = [0, 0, 0]
	for i in [0, 1, 2]:
		if len(sum_array_this['organization']['strata'][i]) == 0 or len(sum_array_prev['organization']['strata'][i]) == 0:
			sum_array_this['organization']['strata_delta'][i] = False
		else:
			sum_array_this['organization']['strata_delta'][i] = delta(len(sum_array_this['organization']['strata'][i]), len(sum_array_prev['organization']['strata'][i]))
	#
	if sum_array_this['organization']['score'] and sum_array_prev['organization']['score']:
		sum_array_this['organization']['score_delta'] = delta(sum_array_this['organization']['score'], sum_array_prev['organization']['score'])
	else:
		sum_array_this['organization']['score_delta'] = False
	#
	i = 0
	pdays = sum_array_prev['days'].keys()
	days = sum_array_this['days'].keys()
	days.sort()
	for i in range(0, len(days)):
		if days[i] - wdayOffset in sum_array_prev['days']:
			sum_array_this['days'][days[i]]['score_prev'] = sum_array_prev['days'][days[i] - wdayOffset]['score']
		i += 1

	return sum_array_this


# ========================================================================
#	Fetch, Calculate and Populate data into a data structure
# ========================================================================
def process_scores(domain_id, users, fromdate, todate, workingdays, options):#doCache
	doCache = options['cache'] if 'cache' in options else False
	getProds = options['prods'] if 'prods' in options else False
	sum_array = {'employees': {}, 'days': {}, 'organization': {'strata': [[], [], []]}}
	day_gscore_cumil = {}
	emp_score_cumil = {}
	#
	for user in users:
		if user.status > 0 and user.role > -1:
			sum_array['employees'][user.id] = {'days': {}}
			emp_score_cumil[user.id] = {'lcumil': 0, 'count': 0}
	#
	for vardate in range(fromdate, todate+1):
		day_of_week = (vardate+4) % 7
		if day_of_week in workingdays:
			#
			# To-Consider : Find optimal substructure and cache domain-per-day
			# CACHE PER DOMAIN - LARGE MEMCACHED OBJECTS
			#
			sum_array['days'][vardate] = {'wday': day_of_week, 'strata': [[], [], []]}
			day_gscore_cumil[vardate] = {'cumil': 0, 'count': 0}

			employees = None
			# =====================================================================
			# Fist check on memcached
			# =====================================================================
			if doCache:
				if 'cache_key' in options:
					cacheKey = '{}-on{}'.format(options['cache_key'], str(vardate))
				else:
					cacheKey = 'domain-'+str(domain_id)+'-on-'+str(vardate)

			if doCache and os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
				data = memcache.get(cacheKey)
				if data is not None:
					employees = data

			# ================================================================
			# Get the data calculated - if not found in cache
			# ================================================================
			if employees is None:
				employees = {}
				dataIncomplete = False
				#
				# Store in cache for future use
				tempEmployees = emps_day_score(sum_array['employees'], vardate, getProds)

				if tempEmployees == False:
					dataIncomplete = True
				else :
					employees = tempEmployees
				if doCache and not dataIncomplete and os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
					memcache.add(cacheKey, employees, 12*3600)

			# ================================================================
			# Each employee working day
			# ================================================================
			for employee in employees:
				emp_score = employees[employee]
				if emp_score != False and employee in sum_array['employees']:
					if getProds:
						sum_array['employees'][employee]['days'][vardate] = emp_score
					#
					# ==========================================================
					# Add employee to daily strata
					# ==========================================================
					sum_array['days'][vardate]['strata'][ emp_score['strata'] ].append(employee)
					#
					day_gscore_cumil[vardate]['cumil'] += emp_score['scr']['l']
					day_gscore_cumil[vardate]['count'] += 1
					#
					emp_score_cumil[employee]['lcumil'] += emp_score['scr']['l']
					emp_score_cumil[employee]['count'] += 1

	# =====================================================================
	# Calculate average score for the period for each employee
	# =====================================================================
	for employee in emp_score_cumil:
		if emp_score_cumil[employee]['count'] == 0:
			sum_array['employees'][employee]['scr'] = False
		else:
			sum_array['employees'][employee]['scr'] = {'l': round( emp_score_cumil[employee]['lcumil'] / emp_score_cumil[employee]['count'] , 3 )}
			sum_array['employees'][employee]['strata'] = strata_for_score(sum_array['employees'][employee]['scr']['l'])
			sum_array['organization']['strata'][ sum_array['employees'][employee]['strata'] ].append(employee)

	# =====================================================================
	# Calculate average organization prodoscore for each day
	# =====================================================================
	org_score = {'cumil': 0, 'count': 0}
	for vardate in sorted(sum_array['days'].keys()):
		if day_gscore_cumil[vardate]['count'] == 0:
			sum_array['days'][vardate]['score'] = False
		else:
			sum_array['days'][vardate]['score'] = day_gscore_cumil[vardate]['cumil'] / day_gscore_cumil[vardate]['count']
		org_score['cumil'] += sum_array['days'][vardate]['score']
		org_score['count'] += 1

	# =====================================================================
	# Get average organization score for the whole period
	# =====================================================================
	if org_score['count'] == 0:
		sum_array['organization']['score'] = False
	else:
		sum_array['organization']['score'] = org_score['cumil'] / org_score['count']

	sum_array['organization']['score_strata'] = strata_for_score(sum_array['organization']['score'])

	return sum_array


def get_baseline(domain_id, role, product):
	cacheKey = 'baseline-'+str(domain_id)+'-'+str(role)+'-'+product
	#
	# Try to get from cache
	cached = proapp.cache.get(cacheKey)
	if cached is not None:
		return cached
	#
	# Read from database
	rec = Baseline.objects.using('replica').filter(role=role, pcode=prod_dic[product], domain_id=domain_id).values('baseline')
	if len(rec) > 0:
		proapp.cache.add(cacheKey, rec[0]['baseline'], 48*3600)
		return rec[0]['baseline']
	#
	# Return the global / default
	if role in products[ prod_dic[product] ][1]:
		return products[ prod_dic[product] ][1][role]
	else:
		proapp.cron.log('Global Baseline Not Found. {Product: '+product+', Role: '+str(role)+'} Assumed 999(->infinity).', 'warning')
		return 999


def get_prod_weight(domain_id, product):
	cacheKey = 'prod-weight-'+str(domain_id)+'-'+product
	#
	# Try to get from cache
	cached = proapp.cache.get(cacheKey)
	if cached is not None:
		return cached
	#
	# Read from database
	rec = Org_weight.objects.using('replica').filter(pcode=prod_dic[product], domain_id=domain_id).values('weight')
	if len(rec) > 0:
		proapp.cache.add(cacheKey, rec[0]['weight'], 48*3600)
		return rec[0]['weight']
	#
	# Return the global / default
	else:
		return products[ prod_dic[product] ][3]


def emps_day_score(employee, date, get_prods):
	employees = {}
	# =====================================================================
	# Check on the cache
	# =====================================================================
	#cacheKey = 'score--on-'+str(date)+('-withprods' if get_prods else '')
	#if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):#get_prods and
	# 	data = proapp.cache.get(cacheKey)
	# 	if data is not None:
	# 		return data   Enable this after test
	#
	#	Fetch employee record
	emp = Employee.objects.values('role', 'id', 'domain_id').using('replica').filter(id__in=employee)  #
	date = datetime.datetime.fromtimestamp(86400*(date+16800))
	strdate = datetime.datetime.strftime(date, '%Y-%m-%d')
	#
	#	Fetch stats of employee for the day
	stats = Statistic.objects.using('replica').filter(date=strdate, employee_id__in=employee).values('product', 'score', 'employee_id')
	if len(stats) == 0:
		return False
	#
	#	Iterate for all products
	formatted_stats = {}
	for stat in stats:
		if stat['employee_id'] not in formatted_stats:
			formatted_stats[stat['employee_id']] = []
		formatted_stats[stat['employee_id']].append(stat)

	for employee in emp:
		Rscore = 0
		prod_count = 0
		prod = {}
		dataIncomplete = False
		if employee['id'] in formatted_stats:
			store_score = Employee_prodoscore.objects.filter(employee_id=employee['id'], date=strdate)
			if get_prods or len(store_score) == 0 or store_score[0].score == -1:
				for stat in formatted_stats[employee['id']]:
					if stat['product'] in prod_dic:	# Ignore unknown products from database
						if stat['score'] < 0:
							dataIncomplete = {'p': prod_dic[stat['product']], 'd': strdate}#True
						else:
							if get_prods:
								prod[prod_dic[stat['product']]] = stat['score']
							#
							if len(store_score) == 0 or store_score[0].score == -1:
								score = stat['score'] * 50
								average = get_baseline(employee['domain_id'], employee['role'], stat['product'])
								weight = get_prod_weight(employee['domain_id'], stat['product'])
								#
								Rscore += weight * score / average
								prod_count += weight
			#
			# Role based average score and General average score to employee day elements

			if len(store_score) == 1 and store_score[0].score > -1:
				Rscore = store_score[0].score
			elif prod_count > 0:
				Rscore = Rscore / prod_count
				if Rscore > 1:
					Rscore = 100 * (math.log(Rscore, 200))
				else:
					Rscore = 0
				if Rscore > 100:
					Rscore = 100

			if len(store_score) == 0:
				store_score = Employee_prodoscore(domain_id=employee['domain_id'], employee_id=employee['id'], date=strdate, role=employee['role'], score=Rscore)
			else:
				store_score = store_score[0]
				if store_score.score < 0:
					store_score.score = Rscore
			try:
				store_score.save()
			except:	# Could be another thread writing the same record
				#print('Duplicate employee_prodoscore record - Could be another thread writing the same record')
				pass

			data = {'scr': {'l': round(Rscore, 3)}, 'strata': strata_for_score(Rscore)}  # , 'prod': prod
			if get_prods:
				data['prod'] = prod
				#
			if dataIncomplete:
				data['warning'] = {'incomplete': dataIncomplete} if get_prods else 'incomplete';
			#
			employees[employee['id']] = data
	return employees


def get_timezone_offset(date, timezone_name):
	datetime_obj_pacific = timezone(timezone_name).localize(date)
	timezone_offset = 	datetime_obj_pacific.utcoffset().total_seconds() / 3600
	#
	return timezone_offset


def delta(val1, val2):
	if val1 + val2 == 0:
		return 0
	else:
		return 100 * (val1 - val2) / (val1 + val2)


def strata_for_score(score):
	if score > 70:
		return 2
	elif score < 30:
		return 0
	else:
		return 1


##-#
# ========================================================================
#  Returns the Organization Prodosocre for the current and previous
#  periods. Calculating using organization_prodoscore if records exists.
# ========================================================================
def get_org_prodosocre_for_current_n_previous_periods(auth_user, this_fromdate, this_todate):
	##-# Inner function for calculate prodoscore for a given period
	def get_org_prod_for_period(domain_id, fromdate, todate):

		org_prod_recs = Organization_prodoscore.objects.using('replica').filter(domain_id=auth_user.domain_id, date__range=(fromdate, todate)).values('domain_id', 'date', 'score')
		query = 'domain_id = ' + str(auth_user.domain_id) + ' AND role > 0 AND status > 0'

		# number_of_days = (this_todate - this_fromdate).days + 1
		# if number_of_days == len(org_prod_recs):
		# 	org_prod_for_period_sum = 0
		# 	for rec in org_prod_recs:
		# 		if rec['score'] > -1:
		# 			org_prod_for_period_sum += rec['score']
		# 		elif rec['score'] == -2:
		# 			org_prod_for_period_sum += 0
		# 			number_of_days = number_of_days - 1
		# 		else:
		# 			org_prod_for_period_sum += proapp.calc.scores(auth_user.domain_id, auth_user, query, datetime.datetime.strftime(rec['date'], '%Y-%m-%d'), datetime.datetime.strftime(rec['date'], '%Y-%m-%d'), {'cache': True})['organization']['score']

		# 	org_prod_for_period = org_prod_for_period_sum / number_of_days
		# else:
		# 	org_prod_for_period = proapp.calc.scores(auth_user.domain_id, auth_user, query, datetime.datetime.strftime(this_fromdate, '%Y-%m-%d'), datetime.datetime.strftime(this_todate, '%Y-%m-%d'), {'cache': True})['organization']['score']
		org_prod_for_period = proapp.calc.scores(auth_user.domain_id, auth_user, query, datetime.datetime.strftime(this_fromdate, '%Y-%m-%d'), datetime.datetime.strftime(this_todate, '%Y-%m-%d'), {'cache': True})['organization']['score']
		
		
		# org_prod_for_period = proapp.calc.scores(auth_user.domain_id, auth_user, query, datetime.datetime.strftime(this_fromdate, '%Y-%m-%d'), datetime.datetime.strftime(this_todate, '%Y-%m-%d'), {'cache': True})['organization']['score']

		return org_prod_for_period

	##-# End of the Method

	this_fromdate = datetime.datetime.strptime(this_fromdate, '%Y-%m-%d')
	this_todate = datetime.datetime.strptime(this_todate, '%Y-%m-%d')

	##-# getting corresponding previous date range
	prev = this_fromdate - timedelta(days=(this_todate - this_fromdate).days)
	prev_short = (int(prev.strftime('%s')) / 86400) - 16801
	fromdate_short = (int(this_fromdate.strftime('%s')) / 86400) - 16800
	todate_short = (int(this_todate.strftime('%s')) / 86400) - 16800

	# Match week-over-week week days to same week days
	thsSWday = (fromdate_short + 4) % 7
	while (prev_short + 4) % 7 != thsSWday:
		prev_short -= 1;

	prev_fromdate = prev_short
	prev_todate = prev_short + (todate_short - fromdate_short)
	prev_fromdate = datetime.datetime.fromtimestamp(86400 * (prev_fromdate + 16800))
	prev_fromdate = prev_fromdate.strftime("%Y-%m-%d")
	prev_todate = datetime.datetime.fromtimestamp(86400 * (prev_todate + 16800))
	prev_todate = prev_todate.strftime("%Y-%m-%d")

	this_org_prodoscore = get_org_prod_for_period(auth_user, this_fromdate, this_todate)
	previous_org_prodoscore = get_org_prod_for_period(auth_user, prev_fromdate, prev_todate)

	score = this_org_prodoscore
	score_delta = this_org_prodoscore - previous_org_prodoscore
	score_strata = strata_for_score(score)

	return score, score_delta, score_strata
##-#
