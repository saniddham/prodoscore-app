
# Interoperable storage helper for long database fields mapped to file storage
# Uses Google cloud storage on App Engine and file storage on Dev environment

import json, os, hashlib
import requests
from django.conf import settings

#if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
import cloudstorage as gcs
#else:
#	from django.core.files import File

#	-------------------------------------------------------------------------------------------------------
#	Store this field in Google Storage as a file
def gcs_path(detail):
	if detail.__class__.__name__ == 'Detail':
		statistic = detail.statistic
		domain = statistic.employee.domain
		date = statistic.date.split('-')
		product = statistic.product
		#
		return '/'+settings.VERSIONS['buckets'][ settings.VERSIONS['this'] ]+'/'+date[0]+'/'+date[1]+'/'+date[2]+'/'+domain.title+'/'+product+'/'+str(statistic.id)+'-'+str(detail.id)+'-'+detail.resource_id#.replace(':', '_') # Windows Hack
	#
	elif detail.__class__.__name__ == 'Doc':
		return '/'+settings.VERSIONS['doc_buckets'][ settings.VERSIONS['this'] ]+'/'+detail.resource_id+'/'+detail.revision_id

#	-------------------------------------------------------------------------------------------------------
#	If field value not loaded from Google Storage to temporary variable - do so
def read_file(detail, filename):
	gcspath = gcs_path(detail)
	filedata = ''
	#if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	try:
		data = gcs.open(gcspath+filename, 'r')
		filedata = data.read()
		data.close()
	except:
		if settings.VERSIONS['this'] == 'prod':
			filedata = False
		elif detail.__class__.__name__ == 'Detail':
			column = filename.split('.')
			column = column[len(column)-2]
			filedata = requests.get('https://app.prodoscore.com/dashboard-detail-data-raw/?id='+str(detail.id)+'&filename='+column, verify=False).content # SSL here causes SSLError: EOF occurred in violation of protocol
			#write_file(detail, filename, filedata)
	#
	# Return to application logic
	return filedata

#	-------------------------------------------------------------------------------------------------------
#	Write file to cloud storage
def write_file(detail, filename, data):
	if type(data) is dict:
		data = json.dumps(data)
	#try:
	gcspath = gcs_path(detail)
	#if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	gcs_file = gcs.open(gcspath+filename, 'w', content_type='application/json', retry_params=gcs.RetryParams(backoff_factor=1.1))
	#
	try:
		data = data.decode().encode('utf-8', 'ignore')
	except:
		data = data.decode('latin1').encode('utf-8', 'ignore')
	#
	try:
		gcs_file.write(data)
		gcs_file.close()
	except:
		print('cannot put: '+gcspath+filename)
#
#	-------------------------------------------------------------------------------------------------------
#

##-# ######################################################################################################
##-# New Methods to Read and Write files with Custom Path
##-# ######################################################################################################

def write_file_with_path(employee, domain, period, tag, data):
	gcspath = '/'+settings.VERSIONS['dashboard_cache'][ settings.VERSIONS['this'] ] + '/' + str(domain) + '/' + str(period) + '/' + str(tag) + '/' + str(employee)
#	print('WRITE TO FILE: ' + gcspath)

	if type(data) is dict:
		data = json.dumps(data)
	# #try:
	# if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	gcs_file = gcs.open(gcspath, 'w', content_type='application/json', retry_params=gcs.RetryParams(backoff_factor=1.1))
	#
	try:
		data = data.decode().encode('utf-8', 'ignore')
	except:
		data = data.decode('latin1').encode('utf-8', 'ignore')
	#
	try:
		gcs_file.write(data)
		gcs_file.close()
	except:
		print('cannot put: '+gcspath)


def read_file_with_path(employee, domain, period, tag):
	gcspath = '/'+settings.VERSIONS['dashboard_cache'][ settings.VERSIONS['this'] ] + '/' + str(domain) + '/' + str(period) + '/' + str(tag) + '/' + str(employee)
	filedata = ''
#	print('READ FROM FILE: ' + gcspath)
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		try:
			data = gcs.open(gcspath, 'r')
			filedata = data.read()
			data.close()
			return filedata
		except:
			pass


