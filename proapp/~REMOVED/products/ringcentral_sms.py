import requests, proapp.cron, proapp.products.ringcentral_calls
import datetime, json
from proapp.products.ringcentral_calls import token_url_rc


def get_stats(employee, date, working_hours, timezone_offset):
    domain = employee.domain
    details = {}
    score = 0

    # Get the access token from the token service
    token_request = requests.get(token_url_rc + str(domain.id))
    rc_data = json.loads(token_request.text)

    admin_id = rc_data['owner_id']
    access_token = rc_data['access_token']
    url = 'https://platform.ringcentral.com/restapi/v1.0/account/~/extension/'+employee.phone_system_id+'/message-store?view=Detailed&dateFrom='+date+'T00:00:00.000Z'
    headers = {'Accept': 'application/json', 'Content-Type': 'application/json',
               'Authorization': 'Bearer ' + access_token}
    k = requests.get(url, headers=headers)
    response_data = k.json()
    try:
        records = response_data['records']
        if len(records) > 0:
            for record in records:
                call_datetime = proapp.products.ringcentral_calls.get_datetime(record['creationTime'],timezone_offset)
                call_time = call_datetime['activity_datetime']
                check_date = call_datetime['activity_date'][:10]
                detail_time = ((call_time - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
                if record['direction'] == 'Outbound' and record['type'] == 'SMS' and check_date == date:
                    call_title = get_title(record['to'])
                    if proapp.cron.within(call_time, working_hours):
                        details[str(record['id'])] = [call_title, record, 0, detail_time, detail_time]
                        score = score + 1

                    else:
                        details[str(record['id'])] = [call_title, record, 1, detail_time, detail_time]
    except KeyError:
        return {'score': score, 'details': details}

    return {'score': score, 'details': details}


def get_title(records):
    for record in records:
        if 'phoneNumber' in record:
            a = record['phoneNumber']
        else:
            a = record['extensionNumber']
        if 'name' in record:
            a = record['name'] + ' ('+a+')'
    return a
