
import proapp.cron
import proapp.products.calendar
from ..models import Domain, Employee, Detail, Transcript_job

import datetime, os
import requests, json, base64
from django.conf import settings

#from google.appengine.api import app_identity
from httplib2 import Http
from django.http import HttpResponse
from googleapiclient.errors import HttpError
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import taskqueue
	import cloudstorage as gcs

# ========================================================================
# Gets TurboBridge stats for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		employee			Employee object to get conference ID
# @params		date				Date to limit the data to
# @params		working_hours	Working hours for the organization
# @params		timezone_offset	Timezone offset for that date for the organization
# ========================================================================
def get_stats(employee, date, working_hours, timezone_offset):
	#print(date)
	domain = employee.domain
	#before_date = (date + timedelta(days=1)).strftime('%Y/%m/%d')
	cCalls = requests.post(
		'https://api.turbobridge.com/4.1/CDR',
		json={'request': {
				'authAccount': {
					'email': domain.conf_admin,
					'password': domain.conf_password,
					'partnerID': domain.conf_partner,
					'accountID': domain.conf_account
				},
				'requestList': [{'getConferenceCallCDR': {'conferenceID': employee.conf_id}}],
				#'outputFormat': 'json'
			}}).content
	cCalls = json.loads(cCalls)
	if 'responseList' in cCalls and 'result' in cCalls['responseList']['requestItem'][0]:
		#score = 0
		details = {}
		conferences = {}
		#
		if cCalls['responseList']['requestItem'][0]['result']['totalResults'] == 0:
			return {'score': 0, 'details': details, 'conferences': conferences}
			#
		elif 'conferenceCallCDR' in cCalls['responseList']['requestItem'][0]['result']:
			cCalls = cCalls['responseList']['requestItem'][0]['result']['conferenceCallCDR']
			time_dic = {}
			for cCall in cCalls:
				if cCall['callStartedDate'][:10] == date:# or True
					callStarted = datetime.datetime.strptime(cCall['callStartedDate'], '%Y-%m-%d %H:%M:%S')
					callEnded = datetime.datetime.strptime(cCall['callEndedDate'], '%Y-%m-%d %H:%M:%S')
					callStartedMts = (callStarted - callStarted.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds() / 60
					callEndedMts = (callEnded - callEnded.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds() / 60
					data = json.dumps(cCall)
					if cCall['fromNumber'] == 'Blocked':
						title = employee.fullname+' Started a call'
					else:
						title = cCall['fromName']+' ('+cCall['fromNumber']+')'
					if proapp.cron.within(callStarted, working_hours) and proapp.cron.within(callEnded, working_hours):
						#good
						details[cCall['callID'].encode('ascii', 'ignore')] = [title, data, 0, callStartedMts, callEndedMts]
						#score += 1	# To Do : Remove overlapping minutes and get the diff minutes - similar to calendar
						time_dic[callStarted.strftime('%H:%M')+ '-' +callEnded.strftime('%H:%M')] = [callStarted.time(), callEnded.time()]

					else:
						#no bueno
						details[cCall['callID'].encode('ascii', 'ignore')] = [title, data, 1, callStartedMts, callEndedMts]
					#
					if cCall['cdrID'] in conferences:
						conferences[ cCall['cdrID'] ].append(cCall['callID'])
					else:
						conferences[ cCall['cdrID'] ] = [ cCall['callID'] ]
			#
			return {'score': proapp.products.calendar.get_NonOverlapping_duration(time_dic), 'details': details, 'conferences': conferences}
	else:
		print(cCalls)
		return {'score': -2, 'details': {}, 'error': cCalls['error']}


def fetch_transcripts(request):
	# Fetch if any completed jobs
	jobs = Transcript_job.objects.filter(status=8, percentage=100).order_by('-id')
	jobs.query.set_limits(low=0, high=10)

	# TB Bucket Path
	path = '/'+settings.VERSIONS['tb_buckets'][ settings.VERSIONS['this'] ]+'/'

	# Iterate completed jobs
	output = {}
	for job in jobs:
		data = gcs.open(path+str(job.id)+'.json', 'r')
		transcription = json.loads(data.read())
		data.close()
		#
		transcript = ''
		if 'results' in transcription:
			for result in transcription['results']:
				transcript += result['alternatives'][0]['transcript']+'\n\n'
		#
		detail_rec = Detail.objects.filter(id=job.detail_id)[0]
		detail_data = json.loads(detail_rec.data)
		detail_data['transcript'] = transcript
		detail_rec.data = json.dumps(detail_data)
		#
		detail_rec.nlp_pversion = 1
		detail_rec.save()
		output[job.detail_id] = {"detail_id": detail_rec.id, "statistic_id": detail_rec.statistic_id, "nlp_pversion": detail_rec.nlp_pversion}
		#
		job.status = 10
		job.save()

	return HttpResponse(json.dumps(output), content_type = 'application/json')#"{'status': 'done'}"


# Depricated
# Download TurboBridge call recording and save to Google Cloud Storage bucket
def download_recording(request):
	#bucket_name = os.environ.get('turbobridge', app_identity.get_default_gcs_bucket_name())
	#
	employee = Employee.objects.filter(id=request.GET['emp'])[0]
	domain = employee.domain
	#
	# First get the URL to download cqall recording mp3 from TurboBridge
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		confData = requests.post(
			'https://api.turbobridge.com/4.1/CDR',
			json={'request': {
					'authAccount': {
						'email': domain.conf_admin,
						'password': domain.conf_password,
						'partnerID': domain.conf_partner,
						'accountID': domain.conf_account
					},
					'requestList': [{'getConferenceCDR': {'conferenceID': employee.conf_id, 'cdrID': request.GET['cdrID']}}],
					#'outputFormat': 'json'
				}}).content
	else:
		confData = '{"responseList":{"requestItem":[{"result":{"totalResults":1,"conferenceCDR":[{"cdrID":3432,"conferenceID":"9494300506","conferenceDbID":438,"accountID":1143819398,"bridgeName":"","started":1489074764,"startedDate":"2017-03-09 10:52:44","ended":1489078597,"endedDate":"2017-03-09 11:56:37","participants":5,"peakParticipants":5,"recordingURL":"https:\/\/panel.turbobridge.com\/GetRecording.php?confID=3432&accountID=1143819398&s=va03Je3PdoSqCgQD%2BpgTru8dGnc%3D","comment":null,"totalSeconds":15420}]}}]},"authToken":"AIBYxjv+RC1Mht6tvu8AAAAL3q2+796tvu\/erb7v3q2+796tvu\/erb7v3q2+796tvu\/erb7v3q2+796tvu\/erb7vCenIh3R1cmJvYnJpZGdl18+2L9Aid8GWcG2dQr1a0EyIOCY="}'
	confData = json.loads(confData)
	#
	# Do we have a valid response.?
	if 'responseList' in confData and 'conferenceCDR' in confData['responseList']['requestItem'][0]['result']:
		#
		# Get relevent stat detail records
		resources = request.GET['resources'].split(',')
		detail = Detail.objects.filter(resource_id__in=resources).order_by('start_time')
		#
		# Except for the first one, let go of the rest of records to be processed without transcript
		for i in range(1, len(detail)):
			detail[i].nlp_pversion = 1
			detail[i].save()
		#
		# If there is no recording, let go of the first record too
		recordingURL = confData['responseList']['requestItem'][0]['result']['conferenceCDR'][0]['recordingURL']
		if recordingURL == '':
			detail[0].nlp_pversion = 1
			detail[0].save()
		else:
			# Store recording URL into stat details metadata
			data = json.loads(detail[0].data)
			data['recordingURL'] = recordingURL
			#
			try:
				# Check if file already there
				gcs_file = gcs.open('/'+settings.VERSIONS['tb_buckets'][ settings.VERSIONS['this'] ]+'/'+str(detail[0].id)+'-'+request.GET['cdrID']+'.mp3', 'r')
				gcs_file.close()
			except:
				# Download recording mp3 file and store in Google cloud storage
				if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
					mp3 = requests.get(recordingURL).content
				else:
					mp3 = 'binary<test file contents>'
				#
				#gcs_file = gcs.open('/'+settings.VERSIONS['tb_buckets'][ settings.VERSIONS['this'] ]+'/testring.txt', 'w',
				gcs_file = gcs.open('/'+settings.VERSIONS['tb_buckets'][ settings.VERSIONS['this'] ]+'/'+str(detail[0].id)+'-'+request.GET['cdrID']+'.mp3', 'w',
					content_type='audio/mp3',
					#options={'x-goog-meta-foo': 'foo', 'x-goog-meta-bar': 'bar'},
					retry_params=gcs.RetryParams(backoff_factor=1.1))
				gcs_file.write(mp3)
				gcs_file.close()
			#
			# Load Client Secret file for the service account authorization
			CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), '../client_secret.json')
			#
			# Establish authorization for NLP API service account - Use the HTTP authentication and open Cloud NLP API
			credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRETS, scopes=['https://www.googleapis.com/auth/cloud-platform'])
			http_auth = credentials.authorize(Http())
			#
			postData = json.dumps({
								#'audio': {'content': base64.b64encode(mp3)}
								'audio': {'uri': 'gs://'+settings.VERSIONS['tb_buckets'][ settings.VERSIONS['this'] ]+'/'+(str(detail[0].id)+'-'+request.GET['cdrID'])+'.mp3'},
								'config': {'languageCode': 'en', 'encoding': 'LINEAR16', 'sampleRateHertz': 8000}
							})
			print(postData)
			res = http_auth.request('https://speech.googleapis.com/v1/speech:longrunningrecognize', 'POST', headers={'Content-Type': 'application/json; charset=UTF-8'}, body=postData)[1]#content-
			#res = http_auth.request('https://speech.googleapis.com/v1/speech:recognize', 'POST', headers={'Content-Type': 'application/json; charset=UTF-8'}, body=postData)[1]#content-
			res = json.loads(res)
			print(res)
			if 'name' in res:
				url = '/turbobridge/get-transcript/?detail_id='+str(detail[0].id)+'&async_id='+res['name']
				if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
					taskqueue.add(url=url, method='GET', queue_name='turbobridge-transcript', name='get_turbobridge_transcript_'+str(detail[0].id), countdown=10)
				else:
					print(url)
			#data['transcript'] = res
			#
			#,'sampleRate': 16000,'languageCode': 'en-US'
			#
			#audio = requests.get(recordingURL)
			detail[0].data = json.dumps(data)
			detail[0].save()
			return HttpResponse(res, content_type = 'application/json')
		#data[]
	#
	return HttpResponse(json.dumps(confData), content_type = 'application/json')


# Depricated
def get_transcript(request):
	detail = Detail.objects.filter(id=request.GET['detail_id'])
	#
	# Load Client Secret file for the service account authorization
	CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), '../client_secret.json')
	#
	# Establish authorization for NLP API service account - Use the HTTP authentication and open Cloud NLP API
	credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRETS, scopes=['https://www.googleapis.com/auth/cloud-platform'])
	http_auth = credentials.authorize(Http())
	#
	#print('https://speech.googleapis.com/v1beta1/operations/'+request.GET['async_id'])
	res = http_auth.request('https://speech.googleapis.com/v1/operations/'+request.GET['async_id'], 'GET')[1]#content-
	resJson = json.loads(res)
	if 'done' in resJson and resJson['done'] == True:
		if 'response' in resJson and 'results' in resJson['response']:
			data = json.loads(detail[0].data)
			data['transcript'] = resJson['response']['results'][0]['alternatives'][0]['transcript']
			detail[0].data = json.dumps(data)
		detail[0].nlp_pversion = 1
		detail[0].save()
	else:
		# Re schedule
		utcnow = datetime.datetime.utcnow()
		utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
		url = '/turbobridge/get-transcript/?detail_id='+request.GET['detail_id']+'&async_id='+request.GET['async_id']
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			taskqueue.add(url=url, method='GET', queue_name='turbobridge-transcript', name='get_turbobridge_transcript_'+utcnow+'_'+request.GET['detail_id'], countdown=60)
		else:
			print(url)

	return HttpResponse(json.dumps(resJson), content_type = 'application/json')







