
import proapp.cron
from ..models import Domain, Employee

import datetime, base64
from datetime import timedelta

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# ========================================================================
# Gets Hangouts count for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		http_auth		HTTP authentication
# @params		userId			User to whom stats to be fetched
# @params		today			Date to limit the data to
# @params		timezoneOffset	Timezone offset for that date for the organization
# ========================================================================
def get_stats(http_auth, userId, today, working_hours, timezoneOffset):
	chat_stats = 0
	chat_sent_time = 0
	details = {}
	userId = userId.lower()

	# =====================================================================
	# Prepare query to filter documents
	# =====================================================================
	before_date = (today + timedelta(days=2)).strftime('%Y/%m/%d')
	after_date = (today - timedelta(days=2)).strftime('%Y/%m/%d')
	q_filter = 'in:chat after:'+ after_date +' before:' + before_date
	try:
		# ==================================================================
		# Fetch Hangouts messages for the date
		# ==================================================================
		service = build('gmail', 'v1', http=http_auth)
		results = proapp.cron.exec_req(service.users().messages().list(userId= userId, q=q_filter, fields='nextPageToken,messages/id'), userId)
		chat_mesgId_array = results.get('messages',[])
		while True:
			try:
				nextPageToken = results.get('nextPageToken')
				if not (nextPageToken is None):
					results = proapp.cron.exec_req(service.users().messages().list(userId=userId, q=q_filter, pageToken=nextPageToken, fields='nextPageToken, messages/id',maxResults=500), userId)
					chat_mesgId_array = chat_mesgId_array + results.get('messages', [])
				else:
					break
			except KeyError:
				break

		# ==================================================================
		# Iterate through Hangouts messages list
		# ==================================================================
		for chat_mesg in chat_mesgId_array:
			chat_id = chat_mesg['id']
			results_mesgs = proapp.cron.exec_req(service.users().messages().get(userId= userId, id=chat_id, fields='internalDate,payload(headers,body/data)'), userId)
	#		print('-------------------------------')
	#		print(results_mesgs)
	#		print('-------------------------------')
			chat_mesgs_array = results_mesgs.get('payload', [])
			chat_mesgs_timestamp = results_mesgs.get('internalDate')
			if chat_mesgs_timestamp is None:
				chat_mesgs_timestamp = 0
			format_code = "%Y-%m-%d %H:%M:%S"
			chat_mesgs_datetime = datetime.datetime.fromtimestamp(int(chat_mesgs_timestamp)/1000).strftime(format_code)
			chat_mesgs_time = datetime.datetime.strptime(chat_mesgs_datetime, format_code) + datetime.timedelta(hours=timezoneOffset)
			chat_sent_time = ((chat_mesgs_time - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
			if 'body' not in chat_mesgs_array:
				chat_mesgs_array['body'] = {}
			chat_mesgs_array['body']['dateTime'] = chat_mesgs_datetime
			try:

				# =============================================================
				# Iterate through headers to find the from username
				# =============================================================
				for j in range(0, len(chat_mesgs_array['headers'])):
					if chat_mesgs_array['headers'][j]['name'] == 'From':
						fromUser = get_chat_username(chat_mesgs_array['headers'][j]['value'])
						chat_mesgs_array['body']['from'] = fromUser
						try:
							title = base64.b64decode(
								chat_mesgs_array['body']['data'].replace('_', '/').replace('-', '+')).encode('utf-8')
							if len(title) > 128:
								title = title[:125] + '..'
						except:
							title = '[ chat-message ]'
						if chat_mesgs_time.date() == today.date():
							flag = 1
							if userId == fromUser[1].lower():
								if proapp.cron.within(chat_mesgs_time, working_hours):#chat_mesgs_time.time() > datetime.time(working_hours[0][0], working_hours[0][1]) and chat_mesgs_time.time() < datetime.time(working_hours[1][0], working_hours[1][1]):
									chat_stats = chat_stats + 1
									flag = 0
							details[chat_id] = [title, chat_mesgs_array['body'], flag, chat_sent_time, chat_sent_time]
			except IndexError, StringError:
				proapp.cron.log('['+userId+' Hangouts]$ Error in Sender ID', 'error')

	except HttpError:
		proapp.cron.log('['+userId+' Hangouts]$ Hangouts is not provisioned for this user', 'info')
		chat_stats = -2

	return {'score': chat_stats, 'details': details}

def get_chat_username(FromName):
	FromName = FromName.split('<')
	return [ FromName[0].strip(), FromName[1].split('>')[0] ]
