import requests, proapp.cron
from ..models import Domain, Employee

import datetime, json, dateutil.parser, base64, time
from datetime import timedelta
from django.db import transaction
from django.conf import settings

token_url_vbc = settings.VERSIONS['token_host'][settings.VERSIONS['this']] + "token/vbc/"


def get_stats(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	details = {}
	score = 0
	date = date - timedelta(minutes = 60*timezone_offset)
	dayafter = date + timedelta(days = 1)
	date = date - timedelta(hours = 2)
	print('['+date.strftime('%Y-%m-%d %H:%M:%S')+' to '+dayafter.strftime('%Y-%m-%d %H:%M:%S')+']')

	# Request the token from the token service
	token_request = requests.get(token_url_vbc + str(domain.id))
	rc_data = json.loads(token_request.text)
#	rc_data = eval("{u'access_token': u'e8d41e84-0b7e-38ab-a5b3-1886efcae9ce'}")

	access_token = rc_data['access_token']
	headers = {'accept': 'application/json',
			   'authorization': 'Bearer '+access_token}
	#
	url = 'https://api.vonage.com/t/vbc.prod/general/v1/appserver/rest/user/null'
	#res = proapp.cron.exec_req( requests.get(url, headers=headers), '' )
	res = requests.get(url, headers=headers)
	user = json.loads(res.text)
	#
	def process_dir(qparam, page):
		score = 0
		url = ('https://api.vonage.com/t/vbc.prod/reports/v1/accounts/'+str(user['accountId'])+'/call-logs'+
			'?start%3Agte='+date.strftime('%Y-%m-%d %H:%M:%S')+'&start%3Alte='+dayafter.strftime('%Y-%m-%d %H:%M:%S')+
			'&page_size=100&page='+str(page)+'&'+qparam+'='+str(employee.phone_system_id))
		k = requests.get(url, headers=headers)
		response_data = k.json()
		#try:
		if '_embedded' in response_data:
			records = response_data['_embedded']['call_logs']
			#if len(records) > 0:
			for record in records:
				call_datetime = get_datetime(record['start'], timezone_offset)
				call_time = call_datetime['activity_datetime']
				check_date = call_datetime['activity_date'][:10]
				detail_time = ((call_time - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
				#
				# REWRITE DIRECTION AND ACTIVITY TITLE
				if record['direction'] == 'Extension':
					if qparam == 'source_user':
						record['direction'] = 'Outbound'
						if record['destination_user'] is None or record['to'] is None:
							call_title = '(self): Loopback'
						else:
							call_title = record['destination_user']+' ('+record['to'] + '): Outbound'
					#
					elif qparam == 'destination_user':
						record['direction'] = 'Inbound'
						'''if record['source_user'] is None or record['from'] is None:
							call_title = '(self): Inbound'
						else:'''
						call_title = record['source_user']+' ('+record['from'] + '): Inbound'
				#
				else:
					if record['direction'] == 'Outbound':
						call_title = record['to'] + ': Outbound'
					#
					elif record['direction'] == 'Inbound':
						call_title = record['from'] + ': Inbound'
					#
					else:
						call_title = record['id']+': Undetermined'
				#
				record['length'] /= 60.0
				#
				# Adjustment for calls spanning across two days (midnight)
				if check_date == date.strftime('%Y-%m-%d'):
					if detail_time + record['length'] > 1440:
						record['length'] = 1440 - detail_time - 1
				elif detail_time + record['length'] > 1440:
					detail_time -= 1440
					record['length'] += detail_time
					detail_time = 0
				else:
					continue
				#
				# Check if within working hours
				if proapp.cron.within(call_time, working_hours):
					if record['result'] in ['Missed', 'Call Failed', 'Hang Up']:
						details[record['id']] = [call_title, record, 1, detail_time, detail_time]
					else:
						details[record['id']] = [call_title, record, 0, detail_time, round(detail_time + record['length'], 0)]
						score += record['length']
				else:
					details[record['id']] = [call_title, record, 1, detail_time, round(detail_time + record['length'], 0)]
		#
		# Fetch all pages
		if 'page' in response_data and response_data['page'] < response_data['total_page']:
			score += process_dir(qparam, page+1)
		#except KeyError:
		#	return false
		return score
	#
	if 'accountId' in user:
		score += process_dir('source_user', 1)
		score += process_dir('destination_user', 1)
	else:
		print('VBC Token Expired/Invalid')
		cause(error)
	#
	return {'score': round(score, 0), 'details': details}



def get_datetime(d,timezone_offset):
	format_code = "%Y-%m-%d %H:%M:%S"
	g_datetime = dateutil.parser.parse(d).strftime(format_code)
	dt = dateutil.parser.parse(g_datetime) + datetime.timedelta(hours=timezone_offset)
	activity_date = dt.strftime(format_code)

	return {'activity_date':  activity_date, 'activity_datetime': dt}

