import proapp.cron
import proapp.products.salesforce
from simple_salesforce import Salesforce
from simple_salesforce.exceptions import SalesforceRefusedRequest, SalesforceMalformedRequest
import datetime
import re
from datetime import timedelta

# ========================================================================
# Gets Salesforce chatter activities stats for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		employee			Employee object to get conference ID
# @params		date				Date to limit the data to
# @params		working_hours	Working hours for the organization
# @params		timezone_offset	Timezone offset for that date for the organization
# ========================================================================
def get_stats(employee, date, working_hours, timezoneOffset):
    details = {}
    score = 0
    date_str = date.strftime('%Y-%m-%d')
    try:
        response_data = proapp.products.salesforce.slaesforce_auth(employee.domain)
        sf = Salesforce(instance_url=response_data['instance_url'], session_id=response_data['access_token'])
    except KeyError:
        return {'score': -2, 'details': details}

    crm_id = employee.crm_id
    check_date = date.strftime('%Y-%m-%d') + 'T00:00:00.000+0000'
    try:
        #-----------------------------------
        # Get feed post score
        #-----------------------------------
        records = sf.query("SELECT  Id, Body, Title, CreatedDate, CreatedById  FROM FeedItem WHERE CreatedById = \'" + crm_id + "\' AND CreatedDate >" + check_date)
        records = records['records']
        feed_post = get_score(sf,employee,records,timezoneOffset,working_hours,date_str)
        post_score = feed_post['score']
        post_details = feed_post['details']
    except SalesforceMalformedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceMalformedRequest', 'info')
        return {'score': -2, 'details': details}
        pass
    except SalesforceRefusedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceRefusedRequest', 'info')
        return {'score': -2, 'details': details}
        pass
    #-----------------------------------
    #Get Feed Comments score
    #-----------------------------------
    try:
        records = sf.query("SELECT  Id, CommentBody, CreatedDate, CreatedById  FROM FeedComment WHERE CreatedById = \'" + crm_id + "\' AND CreatedDate >" + check_date)
        records = records['records']
        feed_comments =get_score(sf,employee,records,timezoneOffset,working_hours,date_str)
        comments_score = feed_comments['score']
        comments_details= feed_comments['details']

    except SalesforceMalformedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceMalformedRequest', 'info')
        return {'score': -2, 'details': details}
        pass
    except SalesforceRefusedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceRefusedRequest', 'info')
        return {'score': -2, 'details': details}
        pass

    # -----------------------------------
    # Add final score and Details
    # -----------------------------------
    score = post_score + comments_score
    details = dict(post_details, **comments_details)

    return {'score': score, 'details': details}

def get_score(sf,employee,records,timezoneOffset,working_hours,date_str):
    details = {}
    score = 0
    if len(records) > 0:
        for record in records:
            record = proapp.products.salesforce.to_dict(record)
            if ('Body' in record and record['Body'] is None) or ('CommentBody' in record and record['CommentBody'] is None):
                continue
            sent_datetime = proapp.products.salesforce.get_datetime(record['CreatedDate'], timezoneOffset)
            sent_time = sent_datetime['activity_datetime']
            check_date = sent_datetime['activity_date'][:10]
            detail_time = ((sent_time - datetime.datetime.strptime('00:00', '%H:%M')).seconds) / 60
            if (check_date == date_str and record['CreatedById'] == employee.crm_id):
                feedt_title = get_title(sf,record)
                p = re.compile(r'<.*?>')
                feedt_title_r = p.sub('', feedt_title)
                record['Body'] = feedt_title_r
                if (proapp.cron.within(sent_time, working_hours)):
                    # print (record)
                    details[str(record['Id'].encode('ascii', 'ignore'))] = [feedt_title_r, record, 0, detail_time, detail_time]
                    score = score + 1
                else:
                    details[str(record['Id'].encode('ascii', 'ignore'))] = [feedt_title_r, record, 1, detail_time, detail_time]

    else:
        return {'score': 0, 'details': details}
    return {'score': score, 'details': details}

def get_title(sf,record):
    body = 'title'
    id = record["Id"]
    if 'Body' in record:
        body = record['Body'].encode('ascii', 'ignore')
    else:
        body = record['CommentBody'].encode('ascii', 'ignore')
    return body
