
import proapp.cron

import datetime, time, json, operator, dateutil.parser, re

from datetime import timedelta

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# ========================================================================
# Gets Calendar count for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		http_auth		HTTP authentication
# @params		user_id			User to whom stats to be fetched
# @params		today			Date for whence stats to be fetched
# ========================================================================
def get_stats(http_auth, timezone_offset, user_id, today, working_hours):
	user_id = user_id.lower()
	igonre_events = ['break', 'lunch', 'out of office', 'ooo']#, 'untitled'
	confirmed_status = 'confirmed'
	confirmed_status = proapp.cron.convert_ascii(confirmed_status.encode('ascii', 'ignore'))#convert_ascii("cancelled")
	cancel_status = 'cancelled'
	cancel_status = proapp.cron.convert_ascii(cancel_status.encode('ascii', 'ignore'))
	accept_status = 'accepted'
	accept_status = proapp.cron.convert_ascii(accept_status.encode('ascii', 'ignore'))
	count_stats = 0
	details = {}
	time_dic = {}
	try:

		# ==================================================================
		# Prepare query to filter calendar events
		# ==================================================================
		a = list(str(timezone_offset))
		a.remove('.')
		if len(a) < 5:
			if a[0] != '-' :
				a.insert(0, '+')
			a.insert(1,0)
			a.insert(4, 0)
		timeMin = today.strftime('%Y-%m-%d')+'T00:00:00' + str(a[0]) + str(a[1]) + str(a[2]) +str(a[3]) + str(a[4])
		timeMax = (today + timedelta(days=1)).strftime('%Y-%m-%d')+'T00:00:00' + str(a[0]) + str(a[1]) + str(a[2]) +str(a[3]) + str(a[4])

		# ==================================================================
		# Fetch list of calendar events for the date
		# ==================================================================
		service = build('calendar', 'v3', http=http_auth)
		calendar_results = proapp.cron.exec_req(service.events().list(calendarId=user_id, timeMax=timeMax, timeMin=timeMin, fields='items(id,status,summary,description,location,attendees,end(dateTime,date),start(dateTime,date))'), user_id)
		calendar_items = calendar_results.get('items', [])

		i = 0
		# ==================================================================
		# TO-DO: Break-down to ID list first and detail requests later and see if caching can be advantageous
		# ==================================================================
		# Iterate through the events list
		# ==================================================================
		while (i < len(calendar_items)):
			#
			calendar_event_start = 0
			calendar_event_end = 0
			detail = ['UNTITLED', json.dumps(calendar_items[i]), True, 00, 00, 00]
			event_status = 'default'
			if 'attendees' in calendar_items[i]:
				for attendee in calendar_items[i]['attendees']:
					if attendee['email'].lower() == user_id:
						event_status = attendee['responseStatus']
						break
			else:
				event_status = calendar_items[i]['status']
			if event_status == 'needsAction':
				calendar_items[i]['attendees_status'] = 'Not Responded'
			else:
				calendar_items[i]['attendees_status'] = event_status.title()
			detail[1] = json.dumps(calendar_items[i])
			check_attedees_status = proapp.cron.convert_ascii(event_status.encode('ascii', 'ignore'))
			if 'start' in calendar_items[i] and calendar_items[i]['id'].rfind('_') < 2:	# Exclude recurring event recurrent
				if check_attedees_status != cancel_status:
					if 'summary' in calendar_items[i]:
						detail[0] = calendar_items[i]['summary'].encode('ascii', 'ignore')
					else:
						detail[0] = 'UNTITLED'
					#When processing full day event we are getting only Date instead of Datetime
					try:
						event_s = get_calendar_time(calendar_items[i]['start']['dateTime'], timezone_offset)
						calendar_event_start = ((event_s - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
						detail[3] = calendar_event_start
						#
						event_e = get_calendar_time(calendar_items[i]['end']['dateTime'], timezone_offset)
						calendar_event_end = ((event_e - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
						detail[4] = calendar_event_end
					#This Key Error raise because of the Date field
					except KeyError:
						detail[3] = 0
						detail[4] = ((datetime.datetime.strptime('23:59:59', '%H:%M:%S') - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
						detail[2] = 1

				if check_attedees_status == confirmed_status or check_attedees_status == accept_status :
					try:
						ignore = False
						#
						if calendar_event_start == 0 and calendar_event_end == 0:
							pass
						else:
							#	Events that are flagged because has words such as 'break', 'lunch' and 'out'
							#	To Do: IMPLEMENT MEACHINE LEARNING TO DO THIS
							summary_words = re.sub('[^0-9a-zA-Z]+', ' ', calendar_items[i]['summary'].encode('ascii', 'ignore').lower()).split()
							wordP1 = ''
							wordP2 = ''
							for word in summary_words:
								if word in igonre_events or (wordP2+' '+wordP1+' '+word) in igonre_events:
									ignore = True
								wordP2 = wordP1
								wordP1 = word
							#
							if not ignore:
								#d_1 = get_calendar_time(calendar_items[i]['start']['dateTime'])
								#d_2 = get_calendar_time(calendar_items[i]['end']['dateTime'])
								event_time_range = (time.mktime(event_e.timetuple()) - time.mktime(event_s.timetuple())) / 60
								# Rectify if the event spans over the midnight
								if event_time_range < 0:
									event_time_range += 1440
								# =======================================================
								# Check if the event is sheduled for within the working hours and less than 8 hours - If so, add to the list to prepare later
								# =======================================================
								if event_time_range < 180:
									# =======================================================
									# Check if the event is sheduled for two days event that start from yesterday
									# =======================================================
									if (today - timedelta(days=1)).date() == event_s.date() and event_e.date() == today.date():
										detail[5] = -1
										if proapp.cron.within(event_e, working_hours):
											detail[2] = 0
											event_s = datetime.time(working_hours[0][0], working_hours[0][1])
											time_dic[event_s.strftime('%H:%M') + '-' + event_e.strftime('%H:%M')] = [event_s,event_e.time()]
									# =======================================================
									# Check if the event is sheduled for two days event that start from today to tomorrow
									# =======================================================
									elif event_s.date() == today.date() and event_e.date() == (today + timedelta(days=1)).date():
										detail[5] = 1
										if proapp.cron.within(event_s, working_hours):
											detail[2] = 0
											event_e = datetime.time(working_hours[1][0], working_hours[1][1])
											time_dic[event_s.strftime('%H:%M') + '-' + event_e.strftime('%H:%M')] = [event_s.time(),event_e]
									# =======================================================
									# Check if the event is sheduled for event that start and end same day
									# =======================================================
									elif event_e.date() < (today + timedelta(days=1)).date():
										detail[5] = 0
										if proapp.cron.within(event_s, working_hours) and proapp.cron.within(event_e,working_hours):  # (datetime.time(working_hours[0][0], working_hours[0][1]) < d_1.time() < datetime.time(working_hours[1][0], working_hours[1][1])) and (datetime.time(working_hours[0][0], working_hours[0][1]) < d_2.time() < datetime.time(working_hours[1][0], working_hours[1][1])):
											detail[2] = 0
											time_dic[event_s.strftime('%H:%M') + '-' + event_e.strftime('%H:%M')] = [event_s.time(), event_e.time()]
										# check_calendar_event_id = convert_ascii(calendar_items[i]['id'])
										elif proapp.cron.within(event_s, working_hours) and not proapp.cron.within(event_e,working_hours):
											detail[2] = 0
											event_e = datetime.time(working_hours[1][0], working_hours[1][1])
											time_dic[event_s.strftime('%H:%M') + '-' + event_e.strftime('%H:%M')] = [event_s.time(), event_e]
										# check_calendar_event_id = convert_ascii(calendar_items[i]['id'])
										elif proapp.cron.within(event_e, working_hours) and not proapp.cron.within(event_s,working_hours):
											detail[2] = 0
											event_s = datetime.time(working_hours[0][0], working_hours[0][1])
											time_dic[event_s.strftime('%H:%M') + '-' + event_e.strftime('%H:%M')] = [event_s, event_e.time()]

								elif (180 < event_time_range < 480):
									detail[2] = 2
								else:
									detail[2] = 3
							else: # Flag calendar item
								detail[2] = 1

					except KeyError:
						proapp.cron.log('['+user_id+' Calendar]$ Full day event - ignored', 'info')
						#detail[2] = 3
					except HttpError:	# NOTE: This is improbable
						log('['+user_id+' Calendar]$ Invalid request data', 'error')
						#detail[2] = 1
				else:
					detail[2] = 1
					proapp.cron.log('['+user_id+' Calendar]$ Event '+calendar_items[i]['id']+'Not confirmed', 'info')
				# =============================================================
				# Store details of the stat - even for events not counted for stat
				# =============================================================
				details[calendar_items[i]['id'].encode('ascii', 'ignore')] = detail

			#else:
			#	pass
			i = i + 1

	except HttpError:
		proapp.cron.log('['+user_id+' Calendar]$ Calendar is not provisioned for this user', 'info')
		count_stats = -2

	# =====================================================================
	# Let's sort the events in chronological order - we are going to weed out overlapping events
	# =====================================================================
	count_stats = get_NonOverlapping_duration(time_dic)
	return {'score': count_stats, 'details': details}


def get_NonOverlapping_duration(time_dic):
	time_object = sorted(time_dic.items(), key=operator.itemgetter(0))
	count_stats = 0
	try:
		k = 1
		s_time = time_object[0][1][0]
		e_time = time_object[0][1][1]
		# ==================================================================
		# Start with the time of first event as score
		# ==================================================================
		count_stats = ((e_time.hour * 60 + e_time.minute) - (s_time.hour*60 + s_time.minute))

		# ==================================================================
		# Iterate through the sorted list of events
		# ==================================================================
		while (k < len(time_object)):
			if e_time <= time_object[k][1][0]:
				# ============================================================
				# Event starts after the end of previous event - Good
				# ============================================================
				s_time = time_object[k][1][0]
				e_time = time_object[k][1][1]
				count_stats = count_stats + ((e_time.hour * 60 + e_time.minute) - (s_time.hour*60 + s_time.minute))
			elif e_time < time_object[k][1][1]:
				# ============================================================
				# Event ends after the previous event ends - not an event inside previous event
				# ============================================================
				s_time = time_object[k - 1][1][1]
				e_time = time_object[k][1][1]
				count_stats = count_stats + ((e_time.hour * 60 + e_time.minute) - (s_time.hour*60 + s_time.minute))
			k = k + 1
	except IndexError:
		proapp.cron.log('[ Calendar]$ No Events', 'info')

	return count_stats

def get_calendar_time(setDate,timezone_offset):
	dt = dateutil.parser.parse(setDate)
	user_offset = dt.utcoffset().total_seconds() / 3600
	dt = dt - datetime.timedelta(hours=user_offset) + datetime.timedelta(hours=timezone_offset)
	return dt.replace(tzinfo=None)

def calendar_setdate(dateObject):
	a = dateObject[:19]
	return datetime.datetime.strptime(a,'%Y-%m-%dT%H:%M:%S')
