import proapp.cron, proapp.products.calendar
import requests, dateutil.parser
import datetime, json, re
import pytz
import os
from ..models import Sugar_settings
from datetime import timedelta
from django.conf import settings

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import memcache
else:
	from django.core.cache import cache

token_url = settings.VERSIONS['token_host'][settings.VERSIONS['this']] + "token/sugar/"


# ===============================================
# Get Sugar LEADS - Creation only
# ===============================================
def get_leads(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	sugar = Sugar_settings.objects.get(domain_id=domain.id)
	if timezone_offset > 0.0:
		url_date = date - timedelta(days=1)
	else:
		url_date = date
	url = "https://" + sugar.sugar_instance + "/rest/v10/Leads/filter"
	payload = {"filter": [{"created_by": {"$equals": employee.crm_id}},
						  {"date_entered": {"$gte": url_date.strftime("%Y-%m-%d")}}], "max_num": -1,
			   "fields": ["full_name", "date_entered", "lead_source", "id", "status", "account_name", "date_modified",
						  "email1", "modified_user_id", "created_by"]}
	res = process_leads_opps(url, payload, employee, date, working_hours, timezone_offset, 'full_name')
	return res


# ===============================================
# Get Sugar OPPORTUNITIES - Creation only
# ===============================================
def get_opportunities(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	sugar = Sugar_settings.objects.filter(domain_id=domain.id)[0]
	if timezone_offset > 0.0:
		url_date = date - timedelta(days=1)
	else:
		url_date = date
	url = "https://" + sugar.sugar_instance + "/rest/v10/Opportunities/filter"
	payload = {"filter": [{"created_by": {"$equals": employee.crm_id}},
						  {"date_entered": {"$gte": url_date.strftime("%Y-%m-%d")}}], "max_num": -1,
			   "fields": ["name", "date_entered", "id", "status", "account_name", "opportunity_type",
						  "amount_usdollar", "created_by", "lead_source"], 'max_num': -1}
	res = process_leads_opps(url, payload, employee, date, working_hours, timezone_offset, 'name')
	return res


# ==========================================================
# Common code block to both Leads and Opportunities
# ==========================================================
def process_leads_opps(url, payload, employee, date, working_hours, timezone_offset, inner_name):
	details = {}
	score = 0
	# request the sugar access token from the token service
	token_request = requests.get(token_url + str(employee.domain.id))
	token_data = json.loads(token_request.text)
	access_token = token_data['token']
	headers = {"Content-Type": "application/json", "OAuth-Token": access_token}
	r = requests.post(url, data=json.dumps(payload), headers=headers)
	response = json.loads(r.text)

	if 'records' in response:
		records = response['records']
		for inner_record in records:
			date_created = dateutil.parser.parse(inner_record['date_entered']).astimezone(
				pytz.utc) + datetime.timedelta(hours=timezone_offset)
			time_entered = date_created.time()

			# Calculate the time the opportunity/leads was created
			detail_time = time_entered.hour * 60 + time_entered.minute

			# ================================================================================
			# Proceed iff the date of creation is the cron date and was created by the employee
			# ================================================================================
			if (date_created.date() == date.date() and inner_record['created_by'] == employee.crm_id):
				# if created within working hours
				if (proapp.cron.within(date_created, working_hours)):
					details[str(inner_record['id'].encode('ascii', 'ignore'))] = [inner_record[inner_name],
																				  inner_record,
																				  0, detail_time, detail_time, 0]
					score = score + 1
				else:
					details[str(inner_record['id'].encode('ascii', 'ignore'))] = [inner_record[inner_name],
																				  inner_record,
																				  1, detail_time, detail_time, 0]
	return {'score': score, 'details': details}


# ========================================================================
# Get Sugar CALL details
# Only the calls that the employee will attend is taken into account
# ========================================================================
def get_calls(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	sugar = Sugar_settings.objects.get(domain_id=domain.id)
	details = {}
	xstart = datetime.time(working_hours[0][0], working_hours[0][1])
	xend = datetime.time(working_hours[1][0], working_hours[1][1])
	if timezone_offset > 0.0:
		url_date = date - timedelta(days=2)
		url_end_date = date + timedelta(days=1)
	else:
		url_date = date - timedelta(days=1)
		url_end_date = date + timedelta(days=2)
	token_request = requests.get(token_url + str(employee.domain.id))
	token_data = json.loads(token_request.text)
	access_token = token_data['token']
	time_dic = {}
	# =====================================================================
	# Fist check on memcached
	# =====================================================================
	url = "https://" + sugar.sugar_instance + "/rest/v10/Calls/filter"
	headers = {"Content-Type": "application/json", "OAuth-Token": access_token}
	payload = {"filter": [{"date_start": {"$gte": url_date.strftime("%Y-%m-%d")}}, {"date_start": {"$lt": url_end_date.strftime("%Y-%m-%d")}}, {"assigned_user_id": {"$equals": employee.crm_id}}], "max_num": -1,
				"fields": ["description", "duration_minutes", "date_end", "invitees", "name", "date_entered", "direction", "id", "date_start", "status", "account_name", "created_by", "assigned_user_id"]}
	r = requests.post(url, data=json.dumps(payload), headers=headers)
	response = json.loads(r.text)
	if 'records' in response:
		records = response['records']
		for inner_record in records:
			start_date = dateutil.parser.parse(inner_record['date_start']).astimezone(pytz.utc) + datetime.timedelta(
				hours=timezone_offset)
			end_date = dateutil.parser.parse(inner_record['date_end']).astimezone(pytz.utc) + datetime.timedelta(
				hours=timezone_offset)
			url2 = "https://" + sugar.sugar_instance + "/rest/v10/Calls/" + inner_record["id"] + "/link/users/filter"
			headers2 = {"Content-Type": "application/json", "OAuth-Token": access_token}
			r2 = requests.get(url2, headers=headers2)
			response2 = json.loads(r2.text)

			if 'records' in response2:
				records2 = response2['records']
				for b in range(0, len(records2)):
					if records2[b]['id'] == employee.crm_id:
						x = scevents_time(date, working_hours, start_date, end_date, xstart, xend)
						if x['count'] and inner_record['status'] == 'Held' and records2[b]['accept_status_calls'] == 'accept':
							inner_record['accept_status_calls'] = 'Accept'
							details[str(inner_record['id'].encode('ascii', 'ignore'))] = [inner_record['name'],
																						  inner_record,
																						  0, x['timestart'],
																						  x['timeend'], x['two_day']]
							time_dic[x['time_dic_key']] = x['time_dic']
						elif not x['not']:
							inner_record['accept_status_calls'] = records2[b]['accept_status_calls'].capitalize()
							details[str(inner_record['id'].encode('ascii', 'ignore'))] = [inner_record['name'],
																						  inner_record,
																						  1, x['timestart'],
																						  x['timeend'], x['two_day']]
	return {'score': proapp.products.calendar.get_NonOverlapping_duration(time_dic), 'details': details}


# ========================================================================
# Get Sugar MEETING details
# Only the meetings that the employee will attend is taken into account
# ========================================================================
def get_calender(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	sugar = Sugar_settings.objects.get(domain_id=domain.id)
	details = {}
	xstart = datetime.time(working_hours[0][0], working_hours[0][1])
	xend = datetime.time(working_hours[1][0], working_hours[1][1])
	if timezone_offset > 0.0:
		url_date = date - timedelta(days=2)
		url_end_date = date + timedelta(days=1)
	else:
		url_date = date - timedelta(days=1)
		url_end_date = date + timedelta(days=2)

	token_request = requests.get(token_url + str(employee.domain.id))
	token_data = json.loads(token_request.text)
	access_token = token_data['token']
	time_dic = {}
	ignore_events = ['break', 'lunch', 'out']
	# =====================================================================
	# Fist check on memcached
	# =====================================================================
	url = "https://" + sugar.sugar_instance + "/rest/v10/Meetings/filter"
	headers = {"Content-Type": "application/json", "OAuth-Token": access_token}
	payload = {"filter": [{"date_start": {"$gte": url_date.strftime("%Y-%m-%d")}}, {"date_start": {"$lt": url_end_date.strftime("%Y-%m-%d")}},  {"assigned_user_id": {"$equals": employee.crm_id}}], "max_num": -1,
				"fields": ["date_end", "name", "description", "date_start", "date_entered", "id", "status", "invitees", "account_name", "created_by", "assigned_user_id"]}
	r = requests.get(url, headers=headers, data=json.dumps(payload))
	response = json.loads(r.text)
	if 'records' in response:
		records = response['records']
		for inner_record in records:
			start_date = dateutil.parser.parse(inner_record['date_start']).astimezone(pytz.utc) + datetime.timedelta(
				hours=timezone_offset)
			end_date = dateutil.parser.parse(inner_record['date_end']).astimezone(pytz.utc) + datetime.timedelta(
				hours=timezone_offset)
			url2 = "https://" + sugar.sugar_instance + "/rest/v10/Meetings/" + inner_record["id"] + "/link/users/filter"
			headers2 = {"Content-Type": "application/json", "OAuth-Token": access_token}
			r2 = requests.get(url2, headers=headers2)
			response2 = json.loads(r2.text)

			if 'records' in response2:
				records2 = response2['records']
				for b in range(0, len(records2)):
					if records2[b]['id'] == employee.crm_id:
						summary_words = re.sub('[^0-9a-zA-Z]+', ' ', inner_record['name'].encode('ascii', 'ignore').lower()).split()
						ignore = False
						for ignored in ignore_events:
							if ignored in summary_words:
								ignore = True

						x = scevents_time(date, working_hours, start_date, end_date, xstart, xend)
						if not ignore and x['count'] and inner_record['status'] == 'Held' and records2[b]['accept_status_meetings'] == 'accept':
							inner_record['accept_status_meetings'] = 'Accept'
							details[str(inner_record['id'].encode('ascii', 'ignore'))] = [inner_record['name'], inner_record, 0, x['timestart'], x['timeend'], x["two_day"]]
							time_dic[x['time_dic_key']] = x['time_dic']

						elif not x['not'] or (x['count'] and ignore):
							inner_record['accept_status_meetings'] = records2[b]['accept_status_meetings'].capitalize()
							details[str(inner_record['id'].encode('ascii', 'ignore'))] = [inner_record['name'], inner_record, 1, x['timestart'], x['timeend'], x["two_day"]]
						break
	return {'score': proapp.products.calendar.get_NonOverlapping_duration(time_dic), 'details': details}


# ========================================================================
# Get Sugar Tasks details
# ========================================================================
def get_tasks(employee, date, working_hours, timezone_offset):
	domain = employee.domain
	sugar = Sugar_settings.objects.get(domain_id=domain.id)
	if timezone_offset > 0.0:
		url_date = date - timedelta(days=1)
	else:
		url_date = date
	details = {}
	score = 0
	token_request = requests.get(token_url + str(employee.domain.id))
	token_data = json.loads(token_request.text)
	access_token = token_data['token']
	url = "https://" + sugar.sugar_instance + "/rest/v10/Tasks/filter"
	headers = {"Content-Type": "application/json", "OAuth-Token": access_token}
	payload = {"filter": [{"created_by": {"$equals": employee.crm_id}}, {"status": "Completed"},
						  {"date_due": {"$gte": url_date.strftime("%Y-%m-%d")}}],
			   "max_num": -1}
	r = requests.get(url, data=json.dumps(payload), headers=headers)
	response = json.loads(r.text)
	if 'records' in response:
		records = response['records']
		for inner_record in records:
			date_created = dateutil.parser.parse(inner_record['date_due']).astimezone(pytz.utc) + datetime.timedelta(
				hours=timezone_offset)
			time_entered = date_created.time()
			detail_time = time_entered.hour * 60 + time_entered.minute
			if (date_created.date() == date.date() and inner_record[
				'created_by'] == employee.crm_id):  # or the assigned??
				if (proapp.cron.within(time_entered, working_hours)):
					details[str(inner_record['id'].encode('ascii', 'ignore'))] = [inner_record['name'],
																				  inner_record,
																				  0, detail_time, detail_time]
					score = score + 1
				else:
					details[str(inner_record['id'].encode('ascii', 'ignore'))] = [inner_record['name'],
																				  inner_record,
																				  0, detail_time, detail_time]
	return {'score': score, 'details': details}


# ========================================================================
# Check if it is on the specified date and within working hours
# ========================================================================
# xstart, xend : dates with start and end of working hours
# min_s, min_e : start and end of working hours represented in minutes
# ========================================================================
def scevents_time(date, working_hours, start_date, end_date, xstart, xend):
	start_time = start_date.time()
	end_time = end_date.time()
	timestart = start_time.hour * 60 + start_time.minute
	timeend = end_time.hour * 60 + end_time.minute
	time_duration = end_date - start_date
	if time_duration < timedelta(hours=3):
		start_within = proapp.cron.within(start_date, working_hours)
		end_within = proapp.cron.within(end_date, working_hours)
		if start_date.date() == date.date():  # event starts on the cron date
			if end_date.date() == start_date.date():  # event ends on the cron date
				if start_within and end_within:
					time_dic_key = start_date.strftime('%H:%M') + '-' + end_date.strftime('%H:%M')
					return {"not": False, "count": True, "timestart": timestart, "timeend": timeend, "time_dic": [start_time, end_time], "two_day": 0, "time_dic_key":time_dic_key}
				elif start_within and not end_within:
					time_dic_key = start_date.strftime('%H:%M') + '-' + xend.strftime('%H:%M')
					return {"not": False, "count": True, "timestart": timestart, "timeend": timeend, "time_dic": [start_time, xend], "two_day": 0, "time_dic_key":time_dic_key}
				elif not start_within and end_within:
					time_dic_key = xstart.strftime('%H:%M') + '-' + end_date.strftime('%H:%M')
					return {"not": False, "count": True, "timestart": timestart, "timeend": timeend, "time_dic": [xstart, end_time], "two_day": 0, "time_dic_key":time_dic_key}
				else:
					return {"not": False, "count": False, "timestart": timestart, "timeend": timeend, "time_dic": [start_time, end_time], "two_day": 0}
			else:  # end falls on another day
				if start_within:
					time_dic_key = start_date.strftime('%H:%M') + '-' + xend.strftime('%H:%M')
					return {"not": False, "count": True, "timestart": timestart, "timeend": timeend, "time_dic": [start_time, xend], "two_day": 1, "time_dic_key":time_dic_key}
				else:
					return {"not": False, "count": False, "timestart": timestart, "timeend": timeend, "two_day": 1}

		elif end_date.date() == date.date():  # event ends on the cron date
			if end_within:
				time_dic_key = xstart.strftime('%H:%M') + '-' + end_date.strftime('%H:%M')
				return {"not": False, "count": True, "timestart": timestart, "timeend": timeend, "time_dic": [xstart, end_time], "two_day": -1, "time_dic_key":time_dic_key}
			else:
				return {"not": False, "count": False, "timestart": timestart, "timeend": timeend, "two_day": -1}

		else:  # the event neither starts on cron day nor ends on the cron day
			return {"not": True, "count": False, "timestart": timestart, "timeend": timeend}
	elif start_date.date() == date.date() or end_date.date() == date.date():
		if end_date.date() == date.date() and start_date.date() == date.date():
			two_day = 0
		elif end_date.date() == date.date():
			two_day = -1
		else:
			two_day = 1
		return {"not": False, "count": False, "timestart": timestart, "timeend": timeend, "two_day": two_day}
	else:
		return {"not": True, "count": False, "timestart": timestart, "timeend": timeend}


# =============================================
# Load sugar employee data to settings page
# =============================================
def load_settings(domain_id):
	import operator
	data = []
	try:
		token_request = requests.get(token_url + str(domain_id))
		token_data = json.loads(token_request.text)
		access_token = token_data['token']
		sugar = Sugar_settings.objects.filter(domain_id=domain_id)[0]
		url = "https://" + sugar.sugar_instance + "/rest/v10/Users?&max_num=500"
		headers = {"Content-Type": "application/json", "OAuth-Token": access_token}
		response = requests.get(url, headers=headers)
		response = json.loads(response.text)
		#
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine') and response['next_offset'] != -1:
			from google.appengine.api import taskqueue
			utcnow = datetime.datetime.utcnow()
			utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
			task_name = 'fetch-sugarcrm-users-page-into-memcache'+str(domain_id)+'-on-'+utcnow
			taskqueue.add(url = '/get-sugarcrm-users-page/?domain='+str(domain_id)+'&y=1&next_offset=500', method='GET', queue_name='sugarcrm-users-cron', name=task_name, countdown=0)
		#

		if 'records' in response and response['records'] != '':
			records = response['records']
			for i in range(len(records)):
				if not records[i]['email']:
					email = ''
				else:
					email = records[i]['email'][0]['email_address']
				data.append({
					'name': records[i]['name'],
					'email': email,
					'id': records[i]['id']})
			data = sorted(data, key=operator.itemgetter('name'))
		elif 'error' in response and response['error'] != '':
			data = response['error_message']
		if not access_token:
			data = 'Invalid Access Token'
	except KeyError as e:
		print ("ERROR!! "+str(e.message))
		data = 'Invalid Credentials'
	sugar_data = {'data': data, 'sugar_instance': sugar.sugar_instance,'sugar_client_key_name': sugar.sugar_client_key_name}
	return sugar_data
