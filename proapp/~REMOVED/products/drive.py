
import proapp.cron
from ..models import Domain, Employee, Doc

import difflib, datetime, json, io
from datetime import timedelta

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

#~ import sys
#~ import thread
#~ import threading
#~ from time import sleep

# ========================================================================
# Gets Docs count for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		http_auth		HTTP authentication
# @params		userId			User to whom stats to be fetched
# @params		today_date		Date for whence stats to be fetched
# @params		timezoneOffset	Timezone offset for that date for the organization
# ========================================================================
def get_stats(http_auth, userId, today_date, timezoneOffset, working_hours):
	import dateutil.parser
	today_date_obj = dateutil.parser.parse(today_date).date()
	count_stats = 0
	details = {}
	docs_time = 0
	docs_sent_time = 0
	file_id = 'hgguuu'
	user_email = userId.email
	check_docs_type = 'application/vnd.google-apps.folder'#convert_ascii("application/vnd.google-apps.folder")

	# =====================================================================
	# Prepare query to filter documents
	# =====================================================================
	startdate = today_date +'T00:00:00.000Z'
	docs_date = today_date +'T00:00:00'
	q_filter = 'modifiedTime > \''+str(docs_date)+'\''

	try:
		# =====================================================================
		# Fetch list of documents edited or created on the date
		# =====================================================================
		service = build('drive', 'v3', http=http_auth)
		results = proapp.cron.exec_req(service.files().list(q = q_filter, fields='files(id,mimeType,name,modifiedTime), kind, nextPageToken'), user_email)
		docs_array = results.get('files', [])
		while True:
			try:
				nextPageToken = results.get('nextPageToken')
				if not (nextPageToken is None):
					results = proapp.cron.exec_req(service.files().list(q=q_filter, pageToken=nextPageToken,  fields='files(id,mimeType,name,modifiedTime),kind, nextPageToken'),user_email)
					docs_array = docs_array + results.get('files', [])
				else:
					break
			except KeyError:
				break

		i = 0
		j = 0
		# =====================================================================
		# Iterate through the documents list
		# =====================================================================
		while (i < len(docs_array)):
			try:
				file_id = docs_array[i]['id'].encode('ascii', 'ignore')
				file_name = docs_array[i]['name'].encode('ascii', 'ignore')
				file_mime = docs_array[i]['mimeType'].encode('ascii', 'ignore')
				#print('Processing :'+file_name + ' [' + file_mime + ']')

				# ================================================================
				# Get comments for each document
				# ================================================================
				results_comments = service.comments()
				results_comments = proapp.cron.exec_req(results_comments.list(fileId=file_id, startModifiedTime=startdate, fields='comments'), 'drive')#user_email
				comments_array = results_comments.get('comments', [])
				#	Iterate comments list
				for c in range(0, len(comments_array)):
					comments_modifiedTime = comments_array[c]['modifiedTime']
					comments_contetnt = comments_array[c]['content']
					t = get_doc_date(comments_modifiedTime, timezoneOffset)
					if comments_modifiedTime[:10] == today_date and proapp.cron.within(t, working_hours):
						t = ((t - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
						#
						if 'author' in comments_array[c]:
							author = comments_array[c]['author']
						elif '\n-' in comments_contetnt:
							author = comments_contetnt.split('\n-')
							author = {'displayName': author[len(author)-1], 'me': False}
						else:
							author = {'displayName': '', 'me': False}
						details['c:'+(file_id+'-' if len(comments_array[c]['id']) < 5 else '')+comments_array[c]['id']] = [file_name, json.dumps({
							'name': file_name, 'mime': file_mime, 'modified': comments_modifiedTime, 'comment': comments_contetnt,
							'author': author['displayName']}), 0 if 'me' in author and author['me'] else 1, t, t]
						if 'me' in author and author['me']:
							count_stats = count_stats + 1

				# ================================================================
				# Get revisions for each document
				# ================================================================
				results_files = proapp.cron.exec_req(service.revisions().list(fileId=file_id, fields='revisions(id,modifiedTime,lastModifyingUser(emailAddress,displayName))'), 'drive')#user_email
				revisions_array =  results_files.get('revisions', [])
				#
				#print('Number of revisions = '+str(len(revisions_array)))
				#	Go ahead to process revisions for this user for this date
				# if recentlyChanged:
				try:
					#	Get text content of last document revision
					docContent = proapp.cron.exec_req(service.files().export_media(fileId=file_id, mimeType='text/plain'), 'drive-revision')
				except:
					docContent = ''
					#print('Cannot fetch text content')
				if docContent is None or not docContent:
					docContent = ''
				docContent = docContent.decode('utf-8').encode('ascii', 'ignore')
				#
				lastRevision = Doc.objects.filter(resource_id=file_id).order_by('-id')
				lastRevision.query.set_limits(low=0, high=1)
				#
				addedLines = ''
				if len(lastRevision) > 0:#j < 16 and 
					if lastRevision[0].revision_id == revisions_array[len(revisions_array)-1]['id']:
						addedLines = lastRevision[0].diff
					else:
						if lastRevision[0].content is None or lastRevision[0].content == False:
							lastRevisionContent = ''
						else:
							try:
								lastRevisionContent = lastRevision[0].content.decode('utf-8').encode('ascii', 'ignore')
							except:
								lastRevisionContent = lastRevision[0].content.encode('ascii', 'ignore').decode('utf-8')
						#
						addedLines = getDiff(lastRevisionContent, docContent).strip()
						#docContent = addedLines
						#j += 1
				else:
					addedLines = docContent
				#content = addedLines
				#
				if len(lastRevision) == 0 or lastRevision[0].revision_id != revisions_array[len(revisions_array)-1]['id']:
					lastRevision = Doc(resource_id=file_id, revision_id=revisions_array[len(revisions_array)-1]['id'], content=docContent, diff=addedLines)
					lastRevision.save()
				#
				for k in range(0, len(revisions_array)):
					# =============================================================
					# Iterate through the revisions list
					# =============================================================
					try:
						# ==========================================================
						# Check if the user is self and the date and time within the working date and time
						# ==========================================================
						modifiedTime = revisions_array[k]['modifiedTime']
						t = get_doc_date(modifiedTime, timezoneOffset)
						if 'lastModifyingUser' in revisions_array[k]:
							docs_modify_user = revisions_array[k]['lastModifyingUser']
							if 'emailAddress' in docs_modify_user:
								user_check = docs_modify_user['emailAddress']
							else:
								user_check = docs_modify_user['displayName']

							if (user_check == user_email or user_check == userId.fullname ) and t.date().__eq__(today_date_obj):
								flag = 1
								if  proapp.cron.within(t, working_hours):
									count_stats = count_stats + 1
									flag = 0
								l = ((t - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
								details['r:'+(file_id+'-' if len(revisions_array[k]['id']) < 5 else '')+revisions_array[k]['id']] = [file_name, json.dumps({'name': file_name, 'content': addedLines, 'mime': file_mime, 'modified': modifiedTime}, encoding='latin1'), flag, l, l]
								#content = ''
							# ========================================================
							# Store details of the stat - even for revisions not counted for stat
							# ========================================================
							# details[revisions_array[k]['id']] = [file_name, json.dumps({'name': file_name, 'mime': file_mime, 'modified': modifiedTime}), 0, t, t]

					except KeyError:
						proapp.cron.log('['+user_email+' Drive]$ Revision Error: '+file_name, 'error')
			#	else:
			#		print('Not counting '+file_name)

			except IndexError:
				proapp.cron.log('['+user_email+' Drive]$ This user does not have Docs', 'info')
			except HttpError as e:
				pass#proapp.cron.log('['+user_email+' Drive]$ '+e.content+': '+file_name, 'warning')

			i = i + 1

	except HttpError:
		proapp.cron.log('['+user_email+' Drive]$ Drive is not provisioned for this user', 'info')
		count_stats = -2

	return {'score': count_stats, 'details': details}


#~ def quit_function(fn_name):
	#~ # print to stderr, unbuffered in Python 2.
	#~ print('Diff took too long')
	#~ raise Exception('Diff took too long')

#~ def exit_after(s):
	#~ ''' use as decorator to exit process if 
	#~ function takes longer than s seconds '''
	#~ def outer(fn):
		#~ def inner(*args, **kwargs):
			#~ timer = threading.Timer(s, quit_function, args=[fn.__name__])
			#~ timer.start()
			#~ try:
				#~ result = fn(*args, **kwargs)
			#~ finally:
				#~ timer.cancel()
			#~ return result
		#~ return inner
	#~ return outer

#@exit_after(8)
def getDiff(prev, cur):
	lenPrev = len(prev)
	lenCur = len(cur)
	if lenPrev == 0:
		if lenCur == 0:
			return ''
		else:
			return cur
	elif lenCur == 0:
		return ''
	#
	try:
		#print('comparing diff '+str(lenPrev)+':'+str(lenCur))
		d = difflib.Differ()
		diff = d.compare(prev, cur)
		diff = list(diff)
		addedLines = ''
		simLatch = True
		for line in diff:
			if line[0] == '+':
				addedLines += line[2:]
				simLatch = False
			elif simLatch == False:
				addedLines += "\n\n"
				simLatch = True
		return addedLines
	except:
		return ''


def get_doc_date(setDate, timezoneOffset):
	format_date = setDate[:19]
	docDate = datetime.datetime.strptime(format_date, '%Y-%m-%dT%H:%M:%S') + datetime.timedelta(hours=timezoneOffset)

	return docDate

