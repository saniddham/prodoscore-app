import proapp.cron
from ..models import Domain, Employee

import datetime
import requests,datetime, json, dateutil.parser
from datetime import timedelta

from json import loads, dumps
from collections import OrderedDict

# ========================================================================
# Gets Prosperworks stats for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		employee			Employee object to get conference ID
# @params		date				Date to limit the data to
# @params		working_hours	Working hours for the organization
# @params		timezone_offset	Timezone offset for that date for the organization
# ========================================================================

def get_opportunities(employee, date, working_hours, timezoneOffset):
	domain = employee.domain
	details = {}
	opps_details = {}
	sub_prod = "opportunity"
	score = 0
	url = 'https://api.prosperworks.com/developer_api/v1/opportunities/search'
	headers = {'Content-Type': 'application/json','X-PW-UserEmail': domain.crm_admin,'X-PW-Application':'developer_api',
			   'X-PW-AccessToken': domain.crm_token}
	r = requests.post(url, headers=headers)
	records = r.json()
	if len(records) > 0:
		for record in records:
			date_dic = get_date(record['date_created'], timezoneOffset)
			check_date = date_dic['activity_date']
			activity_datetime = date_dic['activity_datetime']
			detail_time = ((activity_datetime - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
			opps_details[record['id']] = record['name']
			if employee.crm_id == str(record['assignee_id']) and check_date[:10] == date[:10]:
				if proapp.cron.within(activity_datetime, working_hours):
					details['o:'+str(record['id'])] = [record['name'] + ' [#'+str(record['id'])+']', record, 0, detail_time, detail_time]
					score = score + 1

				else:
					details['o:'+str(record['id'])] = [record['name'] + ' [#'+str(record['id'])+']', record, 1, detail_time, detail_time]

	opp_array = get_records(employee, date, timezoneOffset, sub_prod)
	if len(opp_array) > 0:
		for record in opp_array:
			date_dic = get_date(record['activity_date'], timezoneOffset)
			activity_datetime = date_dic['activity_datetime']
			detail_time = ((activity_datetime - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
			if proapp.cron.within(activity_datetime, working_hours):
				details['a:'+str(record['id'])] = [opps_details[record['parent']['id']] +' [#'+str(record['id'])+']', record, 0, detail_time, detail_time]
				score = score + 1

			else:
				details['a:'+str(record['id'])] = [opps_details[record['parent']['id']] + ' [#'+str(record['id'])+']', record, 1, detail_time, detail_time]


	else:
		return {'score': score, 'details': details}

	return {'score': score, 'details': details}


def get_leads(employee, date, working_hours, timezoneOffset):
	domain = employee.domain
	details = {}
	leads_details = {}
	sub_prod = "leads"
	score = 0
	url = 'https://api.prosperworks.com/developer_api/v1/leads/search'
	headers = {'Content-Type': 'application/json','X-PW-UserEmail': domain.crm_admin,'X-PW-Application':'developer_api',
			   'X-PW-AccessToken': domain.crm_token}
	r = requests.post(url, headers=headers)
	records = r.json()
	if len(records) > 0:
		for record in records:
			date_dic = get_date(record['date_created'], timezoneOffset)
			check_date = date_dic['activity_date']
			activity_datetime = date_dic['activity_datetime']
			detail_time = ((activity_datetime - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
			leads_details[record['id']] = record['name']
			if employee.crm_id == str(record['assignee_id']) and check_date[:10] == date[:10]:
				if proapp.cron.within(activity_datetime, working_hours):
					details['l:'+str(record['id'])] = [record['name'] + ' [#'+str(record['id'])+']', record, 0, detail_time, detail_time]
					score = score + 1


				else:
					details['l:'+str(record['id'])] = [record['name'] + ' [#'+str(record['id'])+']', record, 1, detail_time, detail_time]

	leads_array = get_records(employee, date, timezoneOffset, sub_prod)
	if len(leads_array) > 0:
		for record in leads_array:
			date_dic = get_date(record['activity_date'], timezoneOffset)
			activity_datetime = date_dic['activity_datetime']
			detail_time = ((activity_datetime - datetime.datetime.strptime('00:00:00', '%H:%M:%S')).seconds) / 60
			if proapp.cron.within(activity_datetime, working_hours):
				details['a:'+str(record['id'])] = [leads_details[record['parent']['id']] + ' [#'+str(record['id'])+']', record, 0, detail_time, detail_time]
				score = score + 1

			else:
				details['a:'+str(record['id'])] = [leads_details[record['parent']['id']] + ' [#'+str(record['id'])+']', record, 1, detail_time, detail_time]

	else:
		return {'score': score, 'details': details}

	return {'score': score, 'details': details}


def get_records(employee, date, timezoneOffset, sub_prod):
	domain = employee.domain
	details = {}
	score = 0
	opp_array  = []
	leads_array =  []
	url = 'https://api.prosperworks.com/developer_api/v1/activities/search'
	headers = {'Content-Type': 'application/json','X-PW-UserEmail': domain.crm_admin,'X-PW-Application':'developer_api',
			   'X-PW-AccessToken': domain.crm_token}
	r = requests.post(url, headers=headers)
	records = r.json()
	if len(records) > 0:
		for record in records:
			date_dic = get_date(record['activity_date'], timezoneOffset)
			activity_date = date_dic['activity_date']
			check_id = employee.crm_id
			if check_id == str(record['user_id']) and activity_date[:10] == date[:10]:
				if record['parent']['type'] == 'opportunity':
					opp_array.append(record)

				elif record['parent']['type'] == 'lead':
					leads_array.append(record)

		if sub_prod == "opportunity":
			return opp_array

		else:
			return leads_array


	else:
		return {'product':opp_array, activity_datetime:'0000-00-00 00:00:00'}

def get_date(timestamp, timezoneOffset):
	activity_timestamp = timestamp
	format_code = "%Y-%m-%d %H:%M:%S"
	activity_date = datetime.datetime.fromtimestamp(int(activity_timestamp)).strftime(format_code)
	activity_datetime = datetime.datetime.strptime(activity_date, format_code) + datetime.timedelta(hours=timezoneOffset)
	activity_date = activity_datetime.strftime(format_code)

	return {'activity_date':  activity_date, 'activity_datetime':activity_datetime}
