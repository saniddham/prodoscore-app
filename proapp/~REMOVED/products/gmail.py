
import proapp.cron
from ..models import Domain, Employee

import datetime, json, dateutil.parser, base64, re
from datetime import timedelta

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from difflib import SequenceMatcher

gmail_split_points = ['gmail_extra', 'gmail_quote', 'gmail_signature']
parseAddresses = re.compile(r'<(.+?)>')

# ========================================================================
# Gets Gmail count for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		http_auth		HTTP authentication
# @params		userId			User to whom stats to be fetched
# @params		today			Date for whence stats to be fetched
# @params		timezoneOffset	Timezone offset for that date for the organization
# ========================================================================
def get_stats(http_auth, userId, today, working_hours, timezoneOffset):
	try:
		emails_stats = 0
		email_sent_time = 0
		details = {}

		# ==================================================================
		# Prepare query to filter inbox
		# ==================================================================
		before_date = (today + timedelta(days=2)).strftime('%Y/%m/%d')
		after_date = (today - timedelta(days=2)).strftime('%Y/%m/%d')# - timedelta(days=1)
		datestr = today.strftime('%Y-%m-%d')
		q_filter = 'in:sent after:' + after_date + ' before:' + before_date
		# ==================================================================
		# Fetch list of sent Gmail messages for the date
		# ==================================================================
		service = build('gmail', 'v1', http=http_auth)
		results = proapp.cron.exec_req(service.users().messages().list(userId=userId, q=q_filter, fields='nextPageToken, messages/id', maxResults=500), userId)
		email_mesgId_array = results.get('messages', [])
		while True:
			nextPageToken = results.get('nextPageToken')
			if not (nextPageToken is None):
				results = proapp.cron.exec_req(service.users().messages().list(userId=userId, q=q_filter, pageToken=nextPageToken, fields='nextPageToken, messages/id',maxResults=500), userId)
				email_mesgId_array = email_mesgId_array + results.get('messages', [])
			else:
				break

		# ==================================================================
		# Iterate through the email messages list
		# ==================================================================
		prev_email_time = today - timedelta(days=1)
		email_time = today - timedelta(days=1)
		prev_subject = ''
		subject = ''
		mailTo = ''
		#mailFrom = ''
		prev_mailTo = ''
		for email_mesg in email_mesgId_array:

			# ===============================================================
			# Fetch details for each email message
			# ===============================================================
			results_mesgs = proapp.cron.exec_req(service.users().messages().get(userId=userId, id=email_mesg['id'], fields='internalDate,payload(headers,parts,body)'), userId)#,filename,mimeType
			email_mesgs_array = results_mesgs.get('payload', [])
			# ============================================================
			# Iterate through the headers of email
			# ============================================================
			autoreply = False
			for j in range(0, len(email_mesgs_array['headers'])):
				# =========================================================
				# Check if the email was sent within the working hours - If so, add to the stats
				# =========================================================
				if email_mesgs_array['headers'][j]['name'] == 'Date':
					email_time = get_gmail_time(email_mesgs_array['headers'][j]['value'], timezoneOffset)
					email_sent_time = (( email_time  - datetime.datetime.strptime('00:00:00', '%H:%M:%S') ).seconds) / 60

				# =========================================================
				# Fetch the email subject from the headers
				# =========================================================
				elif email_mesgs_array['headers'][j]['name'] == 'Subject':
					subject = email_mesgs_array['headers'][j]['value']

				#https://github.com/jpmckinney/multi_mail/wiki/Detecting-autoresponders
				# =========================================================
				# Check if this email is an autoreply
				# =========================================================
				elif email_mesgs_array['headers'][j]['name'] == 'X-Autoreply' and email_mesgs_array['headers'][j]['value'] == 'yes':
					autoreply = True

				# =========================================================
				# Fetch the email recipients from the headers
				# =========================================================
				elif email_mesgs_array['headers'][j]['name'] == 'To':
					mailTo = re.findall(parseAddresses, email_mesgs_array['headers'][j]['value'])#','.join(  )#email_mesgs_array['headers'][j]['value'].split(',')
				#elif email_mesgs_array['headers'][j]['name'] == 'From':
				#	mailFrom = re.findall(parseAddresses, email_mesgs_array['headers'][j]['value'])
				#	if len(mailFrom) == 0:
				#		mailFrom = ''
				#	else:
				#		mailFrom = mailFrom[0]
			#
			format_code = "%Y-%m-%d %H:%M:%S"
			email_sent_date = email_time.strftime(format_code)
			#
			if email_sent_date[:10] == datestr:
				try:
					# ============================================================
					# Store the email body on details record - strip and removeextra (signature) and quoted parts
					# ============================================================
					if 'parts' in email_mesgs_array:
						body = get_gmail_body(email_mesgs_array['parts'])
					elif 'body' in email_mesgs_array and 'data' in email_mesgs_array['body']:
						body = base64.b64decode(email_mesgs_array['body']['data'].replace('_', '/').replace('-', '+'))
						#
						msp = len(body)
						for split_point in gmail_split_points:
							sp = body.find('<div class="'+split_point)
							if sp > -1 and sp < msp:
								msp = sp
						body = body[:msp]
						body = base64.b64encode(body)
					else:
						body = ''
					email_mesgs_array['headers'].append({'name': 'body', 'value': body})

					# ============================================================
					# Store details of the stat - even for messages not counted for stat
					# ============================================================
					if proapp.cron.within(email_time, working_hours):
						if autoreply:
							flag = 3
						#elif ((prev_email_time - email_time).total_seconds() > 120 and SequenceMatcher(None, subject, prev_subject).ratio() > 30) or not set(prev_mailTo).isdisjoint(mailTo):
						# Email blasts
						elif ((prev_email_time - email_time).total_seconds() < 10 and set(prev_mailTo).isdisjoint(mailTo) and SequenceMatcher(None, subject, prev_subject).ratio()*100 > 30
								and not ('Re:' in subject or 'Fwd:' in subject)):
							flag = 2
						else:
							emails_stats = emails_stats + 1
							flag = 0
					else:
						flag = 1
					details[email_mesg['id'].encode('ascii', 'ignore')] = [subject.encode('ascii', 'ignore'), json.dumps(email_mesgs_array['headers']), flag, email_sent_time, email_sent_time]
					#
					prev_email_time = email_time
					prev_subject = subject
					prev_mailTo = mailTo

				except IndexError:
					proapp.cron.log('['+userId+' Gmail]$ Error in Gmail', 'error')

	except HttpError:
		proapp.cron.log('['+userId+' Gmail]$ Gmail is not provisioned for this user', 'info')
		emails_stats = -2	# Retry later. Gmail API is known to throw HTTP errors randomly.
		details = {}

	#return {}
	return {'score': emails_stats, 'details': details}

# ============================================================
# Iterate through multipart body parts of email to find the cleartext version
# ============================================================
def get_gmail_body(parts):
	for j in range(0, len(parts)):
		if 'headers' in parts[j]:
			for k in parts[j]['headers']:
				if k['name'] == 'Content-Type':
					if k['value'][:9] == 'text/html':
						if 'data' in parts[j]['body']:
							body = base64.b64decode(parts[j]['body']['data'].replace('_', '/').replace('-', '+'))
						else:
							return ''
						#
						msp = len(body)
						for split_point in gmail_split_points:
							sp = body.find('<div class="'+split_point)
							if sp > -1 and sp < msp:
								msp = sp
						body = body[:msp]
						#gmail_quote = body.find('<div class="gmail_quote">')
						#body = body.split('<div class="gmail_extra">')[0]
						#body = body.split('<div class="gmail_quote">')[0]
						#body = body.split('<div class="gmail_signature">')[0]
						return base64.b64encode(body)
					elif k['value'][:21] == 'multipart/alternative' or k['value'][:17] == 'multipart/related':
						return get_gmail_body(parts[j]['parts'])
		else:
			print('No headers in:')
			print(parts[j])
			return ''

	return ''#{'data': '', 'size': 0}

def get_gmail_time(d, timezoneOffset):
	format_code = "%Y-%m-%d %H:%M:%S"
	try: 
		g_datetime = dateutil.parser.parse(d)
	except ValueError: #gmail api sending different date string format sometimes
		g_datetime = dateutil.parser.parse(d[:31])
	# Apply given timezone offset - if given - [Find time in UTC]
	dparts = d.split(' ')
	if len(dparts) == 6:
		if dparts[5] == 'GMT':
			pass
		else:
			dpart = float(dparts[5][1:3]) + float(dparts[5][3:])/60
			emailTOffset = datetime.timedelta(hours=dpart)

			if dparts[5][0:1] == '-':
				g_datetime += emailTOffset
			else:
				g_datetime -= emailTOffset

	# Apply organization timezone to get email time in org-time
	g_datetime += datetime.timedelta(hours=timezoneOffset)
	#
	# Make timezone naive dateTome obj #return g_datetime
	g_datetime = g_datetime.strftime(format_code)
	dt = dateutil.parser.parse(g_datetime)
	return dt
