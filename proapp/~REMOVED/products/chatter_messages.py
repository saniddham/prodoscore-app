import proapp.cron
import proapp.products.salesforce
from simple_salesforce import Salesforce
from simple_salesforce.exceptions import SalesforceRefusedRequest, SalesforceMalformedRequest

import datetime
from datetime import timedelta

# ========================================================================
# Gets Salesforce Chatter messages stats for a given user for a given date
# 	Returns score and details on which score is based on
#
# @param		employee			Employee object to get conference ID
# @params		date				Date to limit the data to
# @params		working_hours	Working hours for the organization
# @params		timezone_offset	Timezone offset for that date for the organization
# ========================================================================
def get_stats(employee, date, working_hours, timezoneOffset):
    details = {}
    score = 0
    date_str = date.strftime('%Y-%m-%d')
    check_date = (date - timedelta(days=2)).strftime('%Y-%m-%d')
    try:
        response_data = proapp.products.salesforce.slaesforce_auth(employee.domain)
        sf = Salesforce(instance_url=response_data['instance_url'], session_id=response_data['access_token'])
    except KeyError:
        return {'score': -2, 'details': details}

    crm_id = employee.crm_id
    check_date = check_date + 'T00:00:00.000+0000'
    try:
        records = sf.query("SELECT Id, Body, SentDate, SenderId  FROM ChatterMessage WHERE SenderId = \'" + crm_id + "\' AND SentDate >" + check_date)
        records = records['records']
        if len(records) > 0:
            for record in records:
                record = proapp.products.salesforce.to_dict(record)
                sent_datetime = proapp.products.salesforce.get_datetime(record['SentDate'], timezoneOffset)
                sent_time = sent_datetime['activity_datetime']
                check_date = sent_datetime['activity_date'][:10]
                detail_time = ((sent_time - datetime.datetime.strptime('00:00', '%H:%M')).seconds) / 60
                if (check_date == date_str):
                    if (proapp.cron.within(sent_time, working_hours)):
                        details[str(record['Id'].encode('ascii', 'ignore'))] = [record['Body'], record, 0, detail_time, detail_time]
                        score = score + 1
                    else:
                        details[str(record['Id'].encode('ascii', 'ignore'))] = [record['Body'], record, 1, detail_time, detail_time]

        else:
            return {'score': 0, 'details': details}
    except SalesforceMalformedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceMalformedRequest', 'info')
        score = -2
        pass
    except SalesforceRefusedRequest:
        proapp.cron.log('[' + employee.email + ' SalesforceRefusedRequest', 'info')
        score = -2
        pass

    return {'score': score, 'details': details}