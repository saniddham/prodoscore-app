
import os, re, json, base64, hashlib, logging
from difflib import SequenceMatcher

from django.db import transaction
from django.db.utils import OperationalError
from googleapiclient.errors import HttpError
from httplib2 import Http
from django.http import HttpResponse
from googleapiclient.discovery import build

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import memcache, taskqueue
else:
	from django.core.cache import cache

from .models import Employee, Statistic, Detail
from .nlpmodels import Nlp_entity, Nlp_entity_attribs, Nlp_entity2detail
import proapp.cron, proapp.cache

from oauth2client.service_account import ServiceAccountCredentials


# ========================================================================
# Load Client Secret file for the service account authorization
# ========================================================================
CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secret.json')

# ========================================================================
# Analyze text on details for each stat
# ========================================================================
flattenFields = {
	#'gmail': ['Subject', 'To'],#, 'From'
	#'docs': ['Subject', 'To'],
	'turbobridge': ['name', 'location', 'fromName', 'fromNumber', 'toNumber', 'transcript'],
	'ztasks': ['Subject', 'Description', 'Related To'],
	'zleads': ['Lead Source Description'],#, 'First Name', 'Last Name', 'Name', 'Email', 'Company', 'Phone', 'Lead ID'
	'zaccounts': ['Industry', 'Website'],#'Email Address', 'Phone',
	'zevents': ['Subject'],
	'zcalls': ['Subject', 'Call Owner', 'Related To'],
	'sugar_calls': ["name", "description",  "status",  "direction"],
	'sugar_leads': ["full_name",  "lead_source", "status",  "account_name", "email1"],
	'sugar_opps': ["name", "opportunity_type", "lead_source", "account_name"],
	'sugar_meets': ["name",  "description", "status"]
}
pNumPattern = re.compile(r'''			# don't match beginning of string, number can start anywhere
						(\d{3})	# area code is 3 digits (e.g. '800')
						\D*		# optional separator is any number of non-digits
						(\d{3})	# trunk is 3 digits (e.g. '555')
						\D*		# optional separator
						(\d{4})	# rest of number is 4 digits (e.g. '1212')
						\D*		# optional separator
						(\d*)		# extension is optional and can be any number of digits
						$		# end of string
						''', re.VERBOSE)

# ==============================================
# Schedule other functions -> Main cron entry point
# ==============================================
def analyze_cron(request):
	# ----------------------------------------------
	#	Fetch 100 yet-not-analyzed detail records from the database
	# ----------------------------------------------
	details = Detail.objects.using('replica').extra(where=["nlp_pversion < 6 OR nlp_pversion IS NULL"]).order_by('-id')
	details.query.set_limits(low=0, high=90)
	#
	i = 0
	for detail in details:
		try:
			detailRec = Detail.objects.filter(id=detail.id)[0]
			detailRec.nlp_pversion = 6
			detailRec.save()
			#
			# Schedule a task
			urlpart = '/nlp-analyze-task/?id='+str(detail.id)
			namepart = 'nlp-analyze-v5-'+detail.statistic.product+'-of-'+str(detail.id)
			if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
				taskqueue.add(url=urlpart, method='GET', queue_name='nlp', name=namepart, countdown=i*5)
			else:
				print(urlpart)
			i += 1
		except:
			logging.error('Probably a TombstonedTaskError: "nlp-analyze-v5-'+detail.statistic.product+'-of-'+str(detail.id)+'"')
	#
	return HttpResponse('{"result": "scheduled"}', content_type = 'application/json')

# ==============================================
# Analize a single detail record to identify entities and correlations
# ==============================================
def analyze_task(request):
	if 'id' not in request.GET:
		return HttpResponse('{"error": "id required"}', content_type = 'application/json')

	detail = Detail.objects.filter(id=request.GET['id'])
	if len(detail) == 0:
		return HttpResponse('{"error": "record not found"}', content_type = 'application/json')
	detail = detail[0]

	# ----------------------------------------------
	people = []
	cgoods = []
	organizations = []

	# Parse data and flatten based on product
	flattened = flatten_and_initial_nlp(detail, people, cgoods, organizations)

	# =====================================================================
	#	Get string analyzed from Cloud NLP for entities
	# =====================================================================
	if flattened != '':

		try:	# This is shameful to have to do this - but no other way to get around this
			flattened = flattened.decode('unicode_escape').encode('ascii', 'ignore')
		except:
			flattened = flattened.encode('ascii', 'ignore').decode("utf8")
		flattened = flattened.replace('&lt;', '').replace('&gt;', '').replace('&nbsp;', ' ').replace('_', ' ')
		flattened = ' '.join(flattened.split())
		#
		if len(flattened) > 1000:
			flattened = flattened[:1000]

		alreadyprocessed = False
		if detail.nlp_entities != '':
			try:
				res = eval(detail.nlp_entities)
			except:
				try:
					res = json.loads(detail.nlp_entities)
				except:
					res = {}
			#
			if type(res) is dict and 'entities' in res:
				alreadyprocessed = True
		#
		if alreadyprocessed == False:
			#	First, check in MemCached if we have got this presynaptic already NLProcessed
			res = None
			cachekey = hashlib.sha512(flattened).hexdigest()
			if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
				res = memcache.get(cachekey)
			#
			if res is None:
				# =====================================================================
				# Establish authorization for NLP API service account - Use the HTTP authentication and open Cloud NLP API
				# =====================================================================
				credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRETS, scopes=['https://www.googleapis.com/auth/cloud-platform'])
				http_auth = credentials.authorize(Http())
				language = build('language', 'v1', http=http_auth)
				#
				try:
					res = language.documents().annotateText(body={"document": {"content": flattened,"type": "plain_text"}, "encodingType": "UTF8",
															"features": {"extractEntities": True, "extractDocumentSentiment": True, "extractSyntax": False}
															}).execute()
				except HttpError as error:
					res = {'entities': [], 'error': error._get_reason(), 'documentSentiment': {'magnitude': 0, 'score': 0}}
				#
				#	Store in MemCached for 32 days for future use - Google NLP may have learnt new words in future after 32 days let's say..
				if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
					memcache.add(cachekey, res, 32*24*3600)
			#
			detail.nlp_entities = res

	else:
		res = {'entities': [], 'error': 'Empty presynaptic'}
		detail.nlp_entities = res

	detail.nlp_presynaptic = flattened
	detail.save()

	# =====================================================================
	# =====================================================================
	if flattened != '':
		# Iterate Entities returned by G Cloud NLP
		prevEntity = False
		entities = defragment_entities(res['entities'])
#		print('==================')
		for entity in entities:
			if 'mentions' in entity:
				del entity['mentions']
			if 'metadata' in entity:
				del entity['metadata']
			#
			if prevEntity != False and prevEntity['type'] == 'PERSON' and re.match("[^@]+@[^@]+\.[^@]+", entity['name']):
				prevEntity['email'] = entity['name']

			elif entity['type'] == 'ORGANIZATION':
				organizations.append(entity['name'])

			elif entity['type'] == 'CONSUMER_GOOD':
				cgoods.append(entity['name'])

			elif entity['type'] == 'PERSON':
				if entity['name'][0] == '@' or entity['name'][4:] == '.com':
					pass	# Avoid the ridiculousness of identifying domains names as people
				else:
					found = False
					for person in people:
						if 'name' in person:
							if person['name'] == entity['name']:
								found = True
							elif entity['name'].lower() in person['name'].lower():
								found = True
							elif person['name'].lower() in entity['name'].lower():
								person['name'] = entity['name']
								found = True
					#
					if not found:
						people.append({'name': entity['name']})
			else:
				prevEntity = entity

		# =====================================================================
		# Let's see if we can find phone numbers and email addresses that can be correlated to people
		# =====================================================================
		nearestPerson = False
		nearestPNum = False
		nearestEmail = False
		words = flattened.split()
		for word in words:
			if len([person
					for person in people
						if ( ('name' in person and word.lower() in person['name'].lower()) or ('email' in person and word.lower() in person['email'].lower()) )
				]) > 0:
				nearestPerson = person

			if pNumPattern.match(word.strip('.,-+[]')): #	Detect phone numbers
				nearestPNum = word.strip('.,-+[]')

			elif re.match("[^@]+@[^@]+\.[^@]+", word): 	#	Detect email addresses
				nearestEmail = word

			if nearestPerson != False:					#	Assign phone number or email to the name/mention found nearest to it
				if nearestPNum != False: 				#	Correlate phone number to person mentioned closest to it
					nearestPerson['phone'] = re.sub('[^0-9]', '', nearestPNum)#nearestPNum.replace('-', '').replace('.', '')
					nearestPerson = False
					nearestPNum = False
				elif nearestEmail != False: 				#	Correlate email address to person mentioned closest to it
					nearestPerson['email'] = nearestEmail
					nearestPerson = False
					nearestEmail = False

	# Remove the stat owner and cowerkers from people list.
	cowerkers = colleagues(detail.statistic.employee)
	for person in people:
		pdomain = []
		if 'email' in person:
			pdomain = person['email'].split('@')
		#
		if ('name' in person and detail.statistic.employee.fullname == person['name']) or ('email' in person and detail.statistic.employee.email == person['email']):
			people.remove(person)
		elif len(pdomain) > 1 and pdomain[1] == detail.statistic.employee.email.split('@')[1]:
			people.remove(person)
		else:
			if 'email' in person and person['email'] in cowerkers:
				people.remove(person)

	# If only one organization found - assume everyone from that organization. consider matching email address (if email/domain known for domain)
	if len(organizations) == 1:
		for person in people:
			person['organization'] = organizations[0]

	with transaction.atomic():	# THIS LINE CAUSES a TransactionManagementError IN RARE CASES
		relate_entities(detail, detail.statistic.employee, people, organizations)
		detail.nlp_pversion = 6
		detail.save()

	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		return HttpResponse(json.dumps({'result': 'ok'}), content_type = 'application/json')
	else:
		return HttpResponse(json.dumps({'input': flattened, 'product': detail.statistic.product, 'nlp': entities, 'pple': people, 'orgs': organizations, 'cgoods': cgoods}), content_type = 'application/json')


# =====================================================================
#	Parse data and flatten (to string -> NLP presynaptic) based on product
# =====================================================================
def flatten_and_initial_nlp(detail, people, cgoods, organizations):
	flattened = ''

	try:
		data = json.loads(detail.data)
	except:
		try:
			data = eval(detail.data)
		except:
			data = {}

	# =====================================================================
	if detail.statistic.product == 'gmail':
		for kv in data:

			# Parse email to addreses to correlate to people
			if kv['name'] in ['To', 'Cc', 'Bcc']:
				kv['value'] = kv['value'].split('>')
				for person in kv['value']:
					person = person.split('<')
					if len(person) == 1 and '@' in person[0]:
						people.append({'email': person[0].strip(' \'')})
					elif len(person) == 2 and person[1] != '':
						people.append({'name': person[0].replace('"', '').strip(', \'\t\r\n'), 'email': person[1].strip(' \'')})

			elif kv['name'] == 'Subject':
				flattened += kv['value'].replace('Re', '').replace('Fwd', '').replace('FW', '').replace('ATTN', '').replace('*', '').replace(':', '').strip()+'.'

			elif kv['name'] == 'body':
				if type(kv['value']) is dict and 'data' in kv['value']:
					kv['value'] = kv['value']['data']
				body = base64.b64decode(kv['value'].replace('_', '/').replace('-', '+'))
				try:	# This is shameful to have to do this - but no other way to get around this
					body = body.encode('ascii', 'ignore').decode("utf8")
				except:
					body = body.decode("utf8").encode('ascii', 'ignore')
				# ----------------------------------------------
				if len(body) > 2048:
					body = body[:2048]
				# ----------------------------------------------
				flattened += ' '+re.sub('<[^<]+?>', ' ', body)

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'hangouts':
		if 'from' in data:
			people.append({'name': data['from'][0], 'email': data['from'][1]})
		if 'data' in data:
			flattened = base64.b64decode(data['data'].replace('_', '/').replace('-', '+'))

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'calendar':
		if 'summary' in data:
			flattened += ' '+data['summary']+'.'
		if 'location' in data:
			flattened += ' '+data['location']
		if 'description' in data:
			flattened += ' '+data['description'].replace('\n', ' ').replace('\r', ' ')
		#
		if 'attendees' in data:
			for attendee in data['attendees']:
				person = {'email': attendee['email']}
				if 'displayName' in attendee:
					person['name'] = attendee['displayName']
				people.append(person)

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'docs':
		flattened = detail.title
		if 'displayName' in data:
			flattened += ' '+data['displayName']
		elif 'name' in data:
			flattened += ' '+data['name']
		#
		if 'comment' in data:
			flattened += ': '+data['comment']
		#
		if 'content' in data:
			if data['content'] == False:
				pass
			elif len(data['content']) > 2048:
				flattened += ': '+data['content'][:2048]
			else:
				flattened += ': '+data['content']

	# =====================================================================
	elif detail.statistic.product == 'call':
		try:
			if data['type'] == 1:
				people.append({'phone': data['number']})
		except KeyError:
			logging.error("this is a KeyError for call + data['type']",data)
	#
	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'sms':
		try:
			if data['type'] == 1:
				people.append({'phone': data['number']})
			if data['content'] != False:
				flattened = data['content']
		except KeyError:
			logging.error("this is a KeyError for sms + data['type']",data)

	# =====================================================================
	elif detail.statistic.product == 'turbobridge':
		people.append({'name': data['fromName'], 'phone': data['fromNumber']})
		people.append({'name': data['name'], 'phone': data['toNumber']})
		for kv in data:
			if kv in flattenFields['turbobridge']:
				flattened += data[kv]+' '

	# =====================================================================
	elif detail.statistic.product == 'ztasks':
		for kv in data:
			if kv in flattenFields['ztasks']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'zleads':
		try:
			if 'First Name' in data and 'Last Name' in data:
				data['Name'] = data['First Name']+' '+data['Last Name']
			elif 'First Name' in data:
				data['Name'] = data['First Name']
			elif 'Last Name' in data:
				data['Name'] = data['Last Name']
			#
			person = {'name': data['Name']}
			if 'Phone' in data:
				person['phone'] = data['Phone'].replace('+', '').replace('-', '')
			if 'Email' in data:
				person['email'] = data['Email']
			people.append(person)
			#
			organizations.append(data['Company'])
		except KeyError:
			logging.error("this is a KeyError",data)
		for kv in data:
			if kv in flattenFields['zleads']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'zaccounts':
		zaccounts_address = {'Billing Street': '', 'Billing City': '', 'Billing State': ''}
		address = ''
		for key in data:
			if key in zaccounts_address:
				zaccounts_address[key] = data[key]
		flattened = 'Billing Address '+zaccounts_address['Billing Street']+' '+zaccounts_address['Billing City']+' '+zaccounts_address['Billing State']+' '
		for kv in data:
			if kv in flattenFields['zaccounts']:
				flattened += data[kv]+' '
		#
		person = {}
		try:
			if 'Phone' in data:
				person['phone'] = data['Phone'].replace('+', '').replace('-', '')
			if 'Email Address' in data:
				person['email'] = data['Email Address']
			people.append(person)
			#
			organizations.append(data['Account Name'])
		except KeyError:
			logging.error("this is a KeyError",data)

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'zevents':
		for kv in data:
			if kv in flattenFields['zevents']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'zcalls':
		for kv in data:
			if kv in flattenFields['zcalls']:
				flattened += data[kv]+' '

	# =====================================================================
	elif detail.statistic.product == 'ch_messages':
		try:
			flattened = data['Body']
		except KeyError:
			logging.error("this is a KeyError for ch_messages",data)
	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'ch_activts':
		try:
			flattened = data['CommentBody']
		except KeyError:
			logging.error("this is a KeyError for ch_activts", data)
	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'sugar_calls':
		for kv in data:
			if kv in flattenFields['sugar_calls']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'sugar_meets':
		for kv in data:
			if kv in flattenFields['sugar_meets']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'sugar_leads':
		for kv in data:
			if kv in flattenFields['sugar_leads']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'sugar_opps':
		for kv in data:
			if kv in flattenFields['sugar_opps']:
				flattened += data[kv]+' '

	# =====================================================================
	return flattened


# =====================================================================
# Relate entities back to detail records -> to database
# =====================================================================
def relate_entities(detail, employee, people, organizations):

	#	Iterare for organizations
	foundOrg = [False, 0]
	for organization in organizations:
		proapp.cron.log(organization, 'info')
		#	First see if the organization already there for this domain
		attribs = Nlp_entity_attribs.objects.filter(attrib='title').extra(
					where=['MATCH(value) AGAINST (%s IN NATURAL LANGUAGE MODE) AND nlp_entity_id IN (SELECT id FROM proapp_nlp_entity WHERE etype = \'organization\' AND domain_id = '+str(employee.domain_id)+')'],
					params=[organization.encode('ascii', 'ignore')]).order_by('-confidence')
		#
		foundOrg = [False, 0]
		for attrib in attribs:
			match = SequenceMatcher(None, organization, attrib.value).ratio()
			if organization.lower() == attrib.value.lower() or match == 1:
				foundOrg = [attrib.nlp_entity, 1]
				attrib.confidence += 1
				attrib.save()
				break
			elif match > 0.95 and match > foundOrg[1]:
				foundOrg = [attrib.nlp_entity, match]

		#	Insert new entity if no satisfactory match found
		if foundOrg[0] == False:
			entity = Nlp_entity(domain=employee.domain, etype='organization')
			entity.save()
			foundOrg[0] = entity
			try:
				attrib = Nlp_entity_attribs(nlp_entity=entity, confidence=1, attrib='title', value=organization)
				attrib.save()
			except OperationalError:
				print('Invalid Char Sequence: ')
		else:
			entity = foundOrg[0]

		#	Connect the entity to stat-detail record
		conn = Nlp_entity2detail(detail=detail, nlpentity=entity, date=detail.statistic.date, domain=detail.statistic.employee.domain)
		conn.save()

	#	Iterare for people
	for person in people:
		proapp.cron.log(json.dumps(person), 'info')
		foundName = [False, 0]
		foundEmail = [False, 0]
		foundPhone = [False, 0]
		# =====================================================================
		#	First see if the person already there for this domain
		if 'name' in person and person['name'].strip() != '':
			attribs = Nlp_entity_attribs.objects.filter(attrib='name').extra(
						where=['MATCH(value) AGAINST (%s IN NATURAL LANGUAGE MODE) AND nlp_entity_id IN (SELECT id FROM proapp_nlp_entity WHERE etype = \'person\' AND domain_id = '+str(employee.domain_id)+')'],
						params=[person['name'].encode('ascii', 'ignore')]).order_by('-confidence')
			#
			for attrib in attribs:
				match = SequenceMatcher(None, person['name'], attrib.value).ratio()
				if person['name'].lower() == attrib.value.lower():
					foundName = [attrib.nlp_entity, 1, attrib.value]
					attrib.confidence += 1
					attrib.save()
					break
				elif match > 0.95 and match > foundName[1]:
					foundName = [attrib.nlp_entity, match, attrib.value]

		# =====================================================================
		if 'email' in person and person['email'].strip() != '':
			attribs = Nlp_entity_attribs.objects.filter(attrib='email').extra(
						where=['MATCH(value) AGAINST (%s IN NATURAL LANGUAGE MODE) AND nlp_entity_id IN (SELECT id FROM proapp_nlp_entity WHERE etype = \'person\' AND domain_id = '+str(employee.domain_id)+')'],
						params=[person['email'].encode('ascii', 'ignore')]).order_by('-confidence')
			#
			for attrib in attribs:
				match = SequenceMatcher(None, person['email'], attrib.value).ratio()
				if person['email'].lower() == attrib.value.lower():
					foundEmail = [attrib.nlp_entity, 1, attrib.value]
					attrib.confidence += 1
					attrib.save()
					break
				elif match > 0.95 and match > foundEmail[1]:
					foundEmail = [attrib.nlp_entity, match, attrib.value]

		# =====================================================================
		if 'phone' in person and person['phone'].strip() != '':
			attribs = Nlp_entity_attribs.objects.filter(attrib='phone').extra(
						where=['MATCH(value) AGAINST (%s IN NATURAL LANGUAGE MODE) AND nlp_entity_id IN (SELECT id FROM proapp_nlp_entity WHERE etype = \'person\' AND domain_id = '+str(employee.domain_id)+')'],
						params=[person['phone'].encode('ascii', 'ignore')]).order_by('-confidence')
			#
			for attrib in attribs:
				match = SequenceMatcher(None, person['phone'], attrib.value).ratio()
				if person['phone'].lower() == attrib.value.lower():
					foundPhone = [attrib.nlp_entity, 1, attrib.value]
					attrib.confidence += 1
					attrib.save()
					break
				elif match > 0.95 and match > foundPhone[1]:
					foundPhone = [attrib.nlp_entity, match, attrib.value]
		# =====================================================================
		#	If no trace was found - insert new
		if foundName[0] == False and foundEmail[0] == False and foundPhone[0] == False:
			entity = Nlp_entity(domain=employee.domain, etype='person')
			entity.save()
		# =====================================================================
		# =====================================================================
		# If found - link new attributes to that
		else:
			if foundName[0] != False:
				entity = foundName[0]
				#
				if foundEmail[0] != False:
					if foundName[0].id != foundEmail[0].id:
						link_nlp_attribs(foundEmail[0], foundName[0])
				#
				if foundPhone[0] != False:
					if foundName[0].id != foundPhone[0].id:
						link_nlp_attribs(foundPhone[0], foundName[0])
				#
			elif foundEmail[0] != False:
				entity = foundEmail[0]
				#
				if foundPhone[0] != False:
					if foundEmail[0].id != foundPhone[0].id:
						link_nlp_attribs(foundPhone[0], foundEmail[0])
			elif foundPhone[0] != False:
				entity = foundPhone[0]
		# =====================================================================
		# If only one organization found - assume all people are from that
		if len(organizations) == 1:
			entity.p_entity = foundOrg[0]
			# This is supposed to work - but does not work :-(
			#print('--------------')
			#print(foundOrg[0].id)
			#print(entity.p_entity.id)
			#print(entity.id)
			#print('--------------')
			entity.save()
		# =====================================================================
		# Add missing attributes to fill blanks of partial information
		if foundName[0] == False:
			if 'name' in person and person['name'].strip() != '':
				try:
					attrib = Nlp_entity_attribs(nlp_entity=entity, confidence=1, attrib='name', value=person['name'])
					attrib.save()
				except OperationalError:
					print('Invalid Char Sequence: ')
		#
		if foundEmail[0] == False:
			if 'email' in person and person['email'].strip() != '':
				attrib = Nlp_entity_attribs(nlp_entity=entity, confidence=1, attrib='email', value=person['email'])
				attrib.save()
		#
		if foundPhone[0] == False:
			if 'phone' in person and person['phone'].strip() != '':
				attrib = Nlp_entity_attribs(nlp_entity=entity, confidence=1, attrib='phone', value=person['phone'])
				attrib.save()
		# =====================================================================
		conn = Nlp_entity2detail(detail=detail, nlpentity=entity, date=detail.statistic.date, domain=detail.statistic.employee.domain)
		conn.save()
		# =====================================================================
		#
		# Search for entities in database
		# If found, check if attributes match
		# Add missing attributes
		# If comflictng attributes are found, also keep those as varieties with occurence count to verify later
		# If not found; create new entities
		# Connect N:N with detail record on another DB table
		#
	return True


# =====================================================================
# Link attributes from a different entity
# =====================================================================
def link_nlp_attribs(pParent, nParent):
	with transaction.atomic():
		attribs = Nlp_entity_attribs.objects.filter(nlp_entity=pParent)
		e2details = Nlp_entity2detail.objects.filter(nlpentity=pParent)
		for attrib in attribs:
			attrib.nlp_entity = nParent
			attrib.save()
		for e2detail in e2details:
			e2detail.nlpentity = nParent
			e2detail.save()
		pParent.domain_id = -1
		pParent.save()
		pParent.delete()


# =====================================================================
# Often the G Cloud NLP entities are badly fragmented, and we need to consolidate them
# =====================================================================
def defragment_entities(entities):
	for entity in entities:
		for en2 in entities:
			if entity in entities and en2 in entities and en2['type'] == entity['type']:
				if en2 == entity:
					pass
				elif en2['name'].lower() in entity['name'].lower():
					entity['salience'] = max(en2['salience'], entity['salience'])
					entities.remove(en2)
				elif entity['name'].lower() in en2['name'].lower():
					en2['salience'] = max(en2['salience'], entity['salience'])
					entities.remove(entity)
	return entities


# =====================================================================
# List employee emails for the domain of the given user. Caches for 3 hours
# =====================================================================
def colleagues(employee):
	cachekey = 'colleagues-'+str(employee.domain_id)

	# =====================================================================
	# Check on the cache
	# =====================================================================
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		data = memcache.get(cachekey)
	else:
		data = cache.get(cachekey)

	if data is None:
		data = []
		employees = Employee.objects.using('replica').filter(domain_id=employee.domain_id)
		for employee in employees:
			data.append(employee.email)

		# Store in cache
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			memcache.add(cachekey, data, 3*3600)
		else:
			cache.add(cachekey, data, 3*3600)

	return data

