
import os, logging, urllib, json, datetime, time, pytz, requests
import base64, hashlib, uuid, operator

#from django.core import serializers
#from django.shortcuts import redirect
from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect

from httplib2 import Http
from datetime import timedelta

from .models import Domain, Employee, Leave, Statistic, Detail, Notification, Chart
import proapp.calc

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import taskqueue


# ========================================================================
# Schedule task queues to send weekly report emails
# ========================================================================
def mail_reports(request):
	# =====================================================================
	#	Iterate for all domains
	# =====================================================================
	domains = Domain.objects.all()
	count = 0
	for domain in domains:

		#	Skip cron for the example domain
		if domain.id != 6:
			count += schedule_email_domain(domain, 'default')
	logging.info('Total number of email task scheduled : {}'.format(count))
	return HttpResponse('["Report emails scheduled"]', content_type = 'application/json')


def schedule_email_domain(domain, mailto):
	schedule_count = 0
	logging.info('Scheduling emails for domain : {}'.format(domain.title))

	utcnow = datetime.datetime.utcnow()
	utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
	#
	today = datetime.datetime.now()
	yesterday = (today - timedelta(days=1)).strftime('%Y-%m-%d')
	days7ago = (today - timedelta(days=7)).strftime('%Y-%m-%d')
	weekday = 1 + today.weekday()
	if weekday == 7:
		weekday = 0
	workingdays = json.loads(domain.workingdays)
	#
	#	Check if this is a working day for the organization
	if weekday in workingdays:
		employees = Employee.objects.filter(domain=domain).filter(status=1, role__gt=0)
		i = 0
		#
		#	Check if this is the first working day of the week for the organization
		if weekday == workingdays[0]:
			# =============================================================
			#	Iterate employees on each domain
			# =============================================================
			for employee in employees:
				report_enabled = str(employee.report_enabled)
				if ( len(report_enabled) < 2 and report_enabled == '1' ) or ( len(report_enabled) > 2 and report_enabled[3] == '1' ) or mailto == 'qa':	#	( Config V1 AND set to 1 ) OR least significant bit in config V2 set to 1
					# ========================================================
					# Queue tasks to send emails for each employee
					# ========================================================
					if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
						try:
							taskqueue.add(url='/mail-one-report/?fromdate='+days7ago+'&todate='+yesterday+'&emp='+str(employee.id)+'&mailto='+mailto,
								method='GET', countdown=i + 5*domain.id, queue_name='emails',
								name='weekly_report_'+yesterday+'-'+utcnow+'_for_'+employee.email.replace('@', '-at-').replace('.', '-dot-'))
							schedule_count+=1
						except Exception as e:
							logging.error('Caught exception : {}'.format(e.message))
					else:
						print('/mail-one-report/?fromdate='+days7ago+'&todate='+yesterday+'&emp='+str(employee.id)+'&mailto='+mailto)
						schedule_count += 1
					i += 1

		#	Send the daily report
		else:
			# =============================================================
			#	Iterate employees on each domain
			# =============================================================
			for employee in employees:
				report_enabled = str(employee.report_enabled)
				if ( len(report_enabled) > 2 and report_enabled[2] == '1' ) or mailto == 'qa':	#	Config V2 AND 2nd least significant bit set to 1
					# ========================================================
					# Queue tasks to send emails for each employee
					# ========================================================
					if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
						try:
							taskqueue.add(url='/mail-one-report/?date='+yesterday+'&emp='+str(employee.id)+'&mailto='+mailto,
								method='GET', countdown=i + 5*domain.id, queue_name='emails',
								name='daily_report_'+yesterday+'-'+utcnow+'_for_'+employee.email.replace('@', '-at-').replace('.', '-dot-').replace('\'', '-'))
							schedule_count += 1
						except Exception as e:
							logging.error('Caught exception : {}'.format(e.message))
					else:
						print('/mail-one-report/?date='+yesterday+'&emp='+str(employee.id)+'&mailto='+mailto)
						schedule_count += 1
					i += 1
	logging.info('Scheduled {0} email tasks for domain {1}'.format(schedule_count, domain.title))
	return schedule_count


# ========================================================================
# Send weekly report email
# ========================================================================
def mail_one_report(request):
	from collections import OrderedDict

	# =====================================================================
	# Know to whom the mail to be sent
	# =====================================================================
	employee = Employee.objects.filter(id=request.GET['emp'])[0]
	subs = Employee.objects.filter(manager=employee)

	# =====================================================================
	# Prepare input parameters
	# =====================================================================
	if 'fromdate' in request.GET and 'todate' in request.GET:
		fromdate = request.GET['fromdate'].encode('ascii', 'ignore')
		todate = request.GET['todate'].encode('ascii', 'ignore')
	elif 'date' in request.GET:
		fromdate = todate = request.GET['date'].encode('ascii', 'ignore')
	mailto = 'default'
	if 'mailto' in request.GET:
		mailto = request.GET['mailto'].encode('ascii', 'ignore')
	#
	# Make list of emils to Cc the email
	#admins = Employee.objects.filter(domain=employee.domain, role=80, status=1)
	#admins_list = []
	#for admin in admins:
	#	report_enabled = str(admin.report_enabled)
	#	if employee != admin:
	#		if (fromdate == todate and len(report_enabled) > 2 and report_enabled[2] == '1') or (fromdate != todate and (( len(report_enabled) < 2 and report_enabled == '1' ) or ( len(report_enabled) > 3 and report_enabled[3] == '1' ) )):
	#			if not (admin.report_email is None) and '@' in admin.report_email:
	#				admins_list.append(str(admin.report_email))
	#			elif '@' in admin.email:
	#				admins_list.append(str(admin.email))
	workingdays = json.loads(employee.domain.workingdays)
	#
	# =====================================================================
	# Fetch statistics for users in that domain
	# =====================================================================
	# Get scores calculated for the period
	if employee.role > 70:
		# Admin
		if fromdate == todate:
			return HttpResponse('["No Daily Email For Admin"]', content_type = 'application/json')
		else:
			sum_array = proapp.calc.scores(employee.domain_id, employee,
							'domain_id = '+str(employee.domain_id)+' AND role > 0 AND status > 0',
							fromdate, todate,
							{'cache': False, 'prods': True});
			sum_array['reporttype'] = 'admin'
	elif len(subs) == 0:
		# Employee
		sum_array = proapp.calc.scores(employee.domain_id, employee,
						'domain_id = '+str(employee.domain_id)+' AND role = '+str(employee.role)+' AND status > 0',
						fromdate, todate,
						{'cache': False, 'prods': True});
		sum_array['reporttype'] = 'employee'
	else:
		# Manager
		sum_array = proapp.calc.scores(employee.domain_id, employee,
						'manager_id = '+request.GET['emp']+' AND role > 0 AND status > 0',
						fromdate, todate,
						{'cache': False, 'prods': True});
		sum_array['reporttype'] = 'manager'

	# Sort descending
	sum_array['employees'] = OrderedDict(sorted(sum_array['employees'].items(), key=(lambda emp: 0 if emp == False or emp[1] == False or emp[1]['scr'] == False else emp[1]['scr']['l']), reverse=True))

	# Prepare additional information for the email template
	sum_array['managerfirstname'] = employee.fullname.split(' ')[0]
	sum_array['employee'] = employee
	sum_array['fromdate'] = datetime.datetime.strptime(fromdate, '%Y-%m-%d')
	sum_array['fromdate'] = sum_array['fromdate'].strftime('%B ')+datestring(sum_array['fromdate'].strftime('%d'))
	sum_array['todate'] = datetime.datetime.strptime(todate, '%Y-%m-%d')
	sum_array['todate'] = sum_array['todate'].strftime('%B ')+datestring(sum_array['todate'].strftime('%d'))
	#
	sum_array['average'] = sum_array['organization']['score']
	sum_array['average_strata'] = sum_array['organization']['score_strata']
	sum_array['team_delta'] = sum_array['organization']['score_delta']
	#
	# Prepare data for the Chart
	sum_array['chart_data'] = {}
	fromdate = (int(datetime.datetime.strptime(fromdate, '%Y-%m-%d').strftime('%s')) / 86400) - 16800
	#
	todate = datetime.datetime.strptime(todate, '%Y-%m-%d')
	while int(todate.strftime('%w')) not in workingdays:
		todate = todate - timedelta(days=1)
	todate = (int(todate.strftime('%s')) / 86400) - 16800
	#
	for vardate in range(fromdate, todate+1):
		day_of_week = (vardate+4) % 7
		if day_of_week in workingdays:
			sum_array['chart_data'][vardate] = {}
	emp_names = {}
	#
	# Determine employee with max score and lowest score - populate data for the chart in the same time
	min = 200
	max = -1
	#
	#newemployees = Employee.objects.filter(domain=employee.domain, manager_id=52, role=0, status__lt=1)
	sum_array['alerts'] = {'yesterdayinactive': 0, 'inactive': 0, 'yesterdaybelow': 0, 'below': 0}#'newemployee': len(newemployees),
	#
	if sum_array['reporttype'] == 'admin':
		emp_names['org'] = employee.domain.title
	#
	for empid in sum_array['employees']:
		if 'scr' in sum_array['employees'][empid]:
			if sum_array['reporttype'] != 'admin':
				emp_names[empid] = sum_array['employees'][empid]['fullname']
			sum_array['employees'][empid]['id'] = empid
			#
			# Find employee with min score
			if sum_array['employees'][empid]['scr'] and sum_array['employees'][empid]['scr']['l'] < min:
				min = sum_array['employees'][empid]['scr']['l']
				sum_array['min_emp'] = empid
			#
			# Find employee with max score
			if sum_array['employees'][empid]['scr'] and sum_array['employees'][empid]['scr']['l'] > max:
				max = sum_array['employees'][empid]['scr']['l']
				sum_array['max_emp'] = empid
			#
			# Fill data for the chart
			for vardate in sum_array['chart_data']:
				if sum_array['reporttype'] == 'admin':
					sum_array['chart_data'][vardate]['org'] = sum_array['days'][vardate]['score']
				else:
					if vardate in sum_array['employees'][empid]['days']:
						sum_array['chart_data'][vardate][empid] = sum_array['employees'][empid]['days'][vardate]['scr']['l']
			#
			# Calculate alerts
			if len(sum_array['employees'][empid]['days']) == 0:
				sum_array['alerts']['inactive'] += 1
			elif todate not in sum_array['employees'][empid]['days']:
				sum_array['alerts']['yesterdayinactive'] += 1
			elif sum_array['employees'][empid]['days'][todate]['strata'] == 0:
				sum_array['alerts']['yesterdaybelow'] += 1
			if 'strata' in sum_array['employees'][empid] and sum_array['employees'][empid]['strata'] == 0:
				sum_array['alerts']['below'] += 1
	#
	# Remove alerts which are not relevent for the context
	if sum_array['reporttype'] == 'employee':
		sum_array['alerts'] = {}
	if fromdate == todate:
		sum_array['alerts']['below'] = 0
		sum_array['alerts']['inactive'] = 0
	#
	#	START -> STUPID DJANGO - DOES NOT SUPPORT VARIABLES ON TEMPLATE - NOT GOOD FOR ANY SERIOUS BUSINESS
	#	THE FOLLOWING CODE IS UTTER NONSENSE THANKS TO THE IMMATURITY OF DJANGO
	#	PUTTING ALERTS INTO A COLUMN STRUCTURE TO BE SPOONFED TO THE TEMPLATE ENGINE
	alerts = sum_array['alerts']
	alert_strings = {
		#'newemployee': '<b>New</b> employees found',
		'yesterdayinactive': 'Employees were <b>inactive</b> yesterday',
		'inactive': 'Employees were <b>inactive</b> last week',
		'yesterdaybelow': 'Employees were <b>below average</b> yesterday',
		'below': 'Employees were <b>below average</b> last week'}
	odd_even = 0
	i = 0
	sum_array['alerts'] = {0: {}}
	for alert in alerts:
		if alerts[alert] > 0:
			if odd_even == 0:
				sum_array['alerts'][i] = {}
			sum_array['alerts'][i][odd_even] = {'count': alerts[alert], 'alert': alert_strings[alert].replace('Employees were', 'Employee'+(' was' if alerts[alert] == 1 else 's were'))}
			odd_even += 1
			if odd_even == 2:
				odd_even = 0
				i += 1
	#
	#	END -> STUPID DJANGO - DOES NOT SUPPORT VARIABLES ON TEMPLATE - NOT GOOD FOR ANY SERIOUS BUSINESS
	#
	if 'max_emp' not in sum_array and 'min_emp' not in sum_array:
		return HttpResponse('["No Data"]', content_type = 'application/json')
	#
	sum_array['max_emp'] = sum_array['employees'][sum_array['max_emp']]
	sum_array['min_emp'] = sum_array['employees'][sum_array['min_emp']]
	#
	# Get the chart drawn in PNG format
	sum_array['graph'] = {'data': sum_array['chart_data'], 'width': 583, 'height': 333, 'colors': emp_names, 'options': {}}
	if sum_array['reporttype'] == 'employee':
		sum_array['graph']['options']['highlight_emp'] = request.GET['emp']
	sum_array['graph'] = base64.b64encode(json.dumps(sum_array['graph']), '-_')
	#
#	sum_array['graph'] = requests.get('http://localhost:8081/draw/barchart?data='+sum_array['graph'])
	sum_array['graph'] = requests.get('http://'+settings.VERSIONS['charts_api'][settings.VERSIONS['this']]+'/draw/barchart?data='+sum_array['graph'])
	#
	# Hide ewmployee names for employees
	if sum_array['reporttype'] == 'employee':
		for emp in sum_array['employees']:
			if str(emp) == request.GET['emp']:
				sum_array['employees'][emp]['fullname'] += ' (You)'
			else:
				sum_array['employees'][emp]['fullname'] = ''
	#
	# Show only top 5 employees to admin
	if sum_array['reporttype'] == 'admin':
		if len(sum_array['employees']) > 5:
			tmp = OrderedDict({})
			for emp in sum_array['employees']:
				if len(tmp) == 5:
					break
				tmp[emp] = sum_array['employees'][emp]
			sum_array['employees'] = tmp
	#
	sum_array['graph'] = json.loads(sum_array['graph'].content)
	if 'id' in sum_array['graph']:
#		sum_array['graph'] = 'http://localhost:8081/draw/deliver/'+str(sum_array['graph']['id'])
		sum_array['graph'] = 'http://'+settings.VERSIONS['charts_api'][settings.VERSIONS['this']]+'/draw/deliver/'+str(sum_array['graph']['id'])
	else:
		sum_array['graph'] = 'http://app.prodoscore.com/static/img/broken_ruler.png'
	#
	# Get the email HTML rendered
	res = render(request, 'email/email-report.html', sum_array)
	#
	# Send the email to employee
	report_email = employee.report_email
	if employee.report_email is None or employee.report_email == '':
		report_email = employee.email
	#
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine') and 'dryrun' not in request.GET:
		employee_names = employee.fullname.split();
		postdata = {"from": settings.MAILGUN['FROM'],
				"subject": "Hi "+employee_names[0]+", here's your "+("daily" if fromdate == todate else "weekly")+" Prodoscore report",
				"html": res}
		if mailto == 'default':
			postdata['to'] = [report_email]
			# if len(admins_list) > 0:
				# postdata['cc'] = admins_list
			postdata['bcc'] = ['support@prodoscore.com']
		elif mailto == 'qa':
			postdata['to'] = ['qa-team@prodoscore.com', 'villvay.mailman@gmail.com']
		requests.post(settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']), data=postdata)
		return HttpResponse('["Email Sent"]', content_type = 'application/json')
	else:
		return res

# ------------------------------------------------------------------------------------------------------------------------------------

def datestring(day):
	day = int(day)
	if 4 <= day <= 20 or 24 <= day <= 30:
		suffix = "th"
	else:
		suffix = ["st", "nd", "rd"][day % 10 - 1]
	return str(day)+suffix

def autolabel(ax, rects):
	# attach some text labels
	for rect in rects:
		height = rect.get_height()
		ax.text(rect.get_x() + rect.get_width()/2., 1.05*height, '%d' % int(height), ha='center', va='bottom')

# ========================================================================
#		EOF
# ========================================================================

# POST DEPRICATED - REMOVED
#def convert_graph(svg, attempt):
#	# So long suckers. We don't need this any more.. Actually it was a good, very good API. Only thing was that the monthly subscription is manual.
#	keys = ['2dbae824bff90719d75bc69e97c2333e', 'dfa088a1c998d4cafb02bbf58209b03d', '14fae8bc0dc2058fbcd531982fc6647b']
#	if attempt == len(keys):
#		return {'error': 'Ran out of conversion minutes - need more API keys'}
#	#
#	output = requests.post(
#		'https://api.convertio.co/convert',
#		json={'apikey': keys[attempt],
#			'input': 'base64',
#			'file': svg,
#			'filename': 'chart.svg',
#			'ImageResolutionH':96,
#			'ImageResolutionV':96,
#			'outputformat': 'PNG'})
#	#
#	output = json.loads(output.content)
#	if 'error' in output:
#		return convert_graph(svg, attempt+1)
#	else:
#		return output

# ------------------------------------------------------------------------------------------------------------------------------------

# POST DEPRICATED - Left to support email reports already dispatched
#def email_graph(request):
#	try:
#		data_check = Chart.objects.filter(id=request.GET['id'])
#	except ValueError:
#		data_check = Chart.objects.filter(code=request.GET['id'])
#	if len(data_check) == 0:
#		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+'/static/img/broken_ruler.png')
#		#data_get = 'Graph still being rendered'
#	else:
#		data_get = data_check[0].data
#	#
#	return HttpResponse(base64.b64decode(data_get), content_type = 'image/png')

# ------------------------------------------------------------------------------------------------------------------------------------
