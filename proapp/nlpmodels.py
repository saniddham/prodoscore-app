#from __future__ import unicode_literals
import datetime
from django.db import models
from .models import Domain, Detail


class Nlp_entity(models.Model):
	domain = models.ForeignKey(Domain)			# Denormalization
	etype = models.CharField(max_length=16)

class Nlp_entity_attribs(models.Model):
	nlp_entity = models.ForeignKey(Nlp_entity)
	confidence = models.IntegerField()
	attrib = models.CharField(max_length=16)
	value = models.CharField(max_length=80)

	def __str__(self):
		return self.attrib+': '+self.value+' (x'+str(self.confidence)+')'

class Nlp_entity2detail(models.Model):
	detail = models.ForeignKey(Detail)
	nlpentity = models.ForeignKey(Nlp_entity)
	salience = models.FloatField()
	int_ext = models.IntegerField()
	product = models.CharField(max_length=24)
	date = models.CharField(max_length=20)  		# Denormalization
	domain = models.ForeignKey(Domain)			# Denormalization
