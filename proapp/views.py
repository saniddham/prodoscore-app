
import os, requests, json, datetime, time, base64, operator

import dateutil
from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect

from httplib2 import Http
from datetime import timedelta

from .models import Onboard, Domain, Employee, Statistic, Employee_prodoscore, Detail, Notification, Feedback, Click, Broadsoft_admin, Broadsoft_user, RingCentral, Sugar_settings, Vbc, Transcript_job

from django.db.models import Q
from .nlpmodels import Nlp_entity2detail, Nlp_entity, Nlp_entity_attribs
import proapp.cron, proapp.calc, proapp.cache, proapp.settings

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import memcache, taskqueue
else:
	from django.core.cache import cache


# ========================================================================
# Authorize given user request to access the system or redirect to relevent page
# ========================================================================
def _authorize(request):

	# =====================================================================
	# SESSION - User not authenticated - authenticate
	# =====================================================================
	if 'email' not in request.session.keys():
		return {'redirect': '/sign-in/', 'error': 'not logged in'}

	# =====================================================================
	# USER RECORD - Employee not recognized
	# =====================================================================
	employee_check = Employee.objects.filter(email=request.session['email'])
	if len(employee_check) == 0:
		return {'redirect': '/register', 'error': 'user not recognized'}
	else:
		# ONBOARD - RECORD
		try:
			if employee_check[0].last_login_domain_name and employee_check[0].role ==80:
				request.session['domain'] = employee_check[0].last_login_domain_name
		except Exception:
			print(Exception)
		session_domain = Domain.objects.using('replica').filter(title=request.session['domain'])[0]
		employee_check[0].domain_id = session_domain.id
		onboard = Onboard.objects.filter(domain=session_domain).values('status')
		if len(onboard) == 0 or onboard[0]['status'] < 7:
			return {'redirect': '/register', 'error': 'onboard incomplete'}

	# =====================================================================
	# SUBORDINATES - Check if role => manager or has subordinates
	# =====================================================================
	manager_check = Employee.objects.using('replica').filter(manager_id=employee_check[0].id, status__gt=0, role__gt=0).exists()
	if not manager_check and employee_check[0].role < 70:
		return {'redirect': '/dashboard/', 'user': employee_check[0]}

	proapp.cron.log('User: '+employee_check[0].email+' ['+str(employee_check[0].id)+']', 'info')
	return {'user': employee_check[0], 'has-subordinates': manager_check, 'domain' : session_domain}


# ========================================================================
# Manager Dashboard UI - Set initial report time frame and render the UI HTML
# ========================================================================
def index(request):

	if request.get_host() == 'app.prodoscore.com' and not request.is_secure():
		return HttpResponseRedirect('https://app.prodoscore.com/')

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	# =====================================================================
	# Proceed loading dashboard
	# =====================================================================
	try:
		user_domain = auth['user'].email.split('@')[1]
	except Exception:
		if auth['user'].last_login_domain_name:
			user_domain = auth['user'].last_login_domain_name
		else :
			user_domain = auth['user'].domain.title
	domain = Domain.objects.filter(title=user_domain)[0]#request.session['domain']
	workingdays = json.loads(domain.workingdays)
	#
	# Fix to 16th Nov 2016 for example domain
	if domain.id == 6:
		today = datetime.datetime.strptime('2017-04-14', "%Y-%m-%d")
	else:
		today = datetime.datetime.now()
	timezone_offset = proapp.calc.get_timezone_offset(today, domain.timezone)
	yesterday = today - timedelta(days=1) + timedelta(hours=timezone_offset)
	while int(yesterday.strftime('%w')) not in workingdays and len(workingdays) > 0:
		yesterday = yesterday - timedelta(days=1)
	#
	days7ago = yesterday - timedelta(days=6)
	while int(days7ago.strftime('%w')) not in workingdays and len(workingdays) > 0:
		days7ago = days7ago + timedelta(days=1)
	#
	days7ago = days7ago.strftime('%Y-%m-%d')
	yesterday = yesterday.strftime('%Y-%m-%d')
	return render(request, 'mgr-dashboard.html', {'today': yesterday, 'days7ago': days7ago, 'user': auth['user'], 'domain': domain})#, 'user_domain': user_domain


# ========================================================================
# Settings Dashboard UI
# ========================================================================
def settings_dashboard(request):

	if request.get_host() == 'app.prodoscore.com' and not request.is_secure():
		return HttpResponseRedirect('https://app.prodoscore.com/')

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#elif auth['user'].role < 80:
	#	return HttpResponseRedirect(request.build_absolute_uri('/')+'#dashboard')

	# =====================================================================
	# Proceed loading dashboard
	# =====================================================================
	domain = Domain.objects.filter(title=request.session['domain'])[0]
	#
	workingdays = json.loads(domain.workingdays)
	today = datetime.datetime.now()
	timezone_offset = proapp.calc.get_timezone_offset(today, domain.timezone)
	yesterday = today - timedelta(days=1) + timedelta(hours=timezone_offset)
	while int(yesterday.strftime('%w')) not in workingdays and len(workingdays) > 0:
		yesterday = yesterday - timedelta(days=1)
	#
	days7ago = yesterday - timedelta(days=6)
	while int(days7ago.strftime('%w')) not in workingdays and len(workingdays) > 0:
		days7ago = days7ago + timedelta(days=1)
	#
	days7ago = days7ago.strftime('%Y-%m-%d')
	yesterday = yesterday.strftime('%Y-%m-%d')
	#
	try:
		user_domain = auth['user'].email.split('@')[1]
	except Exception:
		if auth['user'].last_login_domain_name:
			user_domain = auth['user'].last_login_domain_name
		else :
			user_domain = auth['user'].domain.title
	return render(request, 'settings-dashboard.html', {'today': yesterday, 'days7ago': days7ago, 'user': auth['user'], 'user_domain': user_domain})


# ========================================================================
# Employee View
# ========================================================================
def employee_dashboard(request):

	if request.get_host() == 'app.prodoscore.com' and not request.is_secure():
		return HttpResponseRedirect('https://app.prodoscore.com/dashboard/')

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth and 'redirect' in auth and auth['redirect'] != '/dashboard/':
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	if auth['user'].status < 1:
		#return HttpResponseRedirect(request.build_absolute_uri('/#dashboard'))
		return HttpResponseRedirect(request.build_absolute_uri('/')+'sign-in/?error=You%20do%20not%20have%20a%20Prodoscore%20dashboard.')

	domain = Domain.objects.filter(title=request.session['domain'])[0]
	# Fix to 16th Nov 2016 for example domain
	if domain.id == 6:
		today = datetime.datetime.strptime('2016-11-15', "%Y-%m-%d")
	else:
		today = datetime.datetime.now()
	# =====================================================================
	# Proceed loading dashboard
	# =====================================================================
	timezone_offset = proapp.calc.get_timezone_offset(today, domain.timezone)
	yesterday = (today - timedelta(days=1) + timedelta(hours=timezone_offset)).strftime('%Y-%m-%d')
	days7ago = (today - timedelta(days=7)).strftime('%Y-%m-%d')
	return render(request, 'emp-dashboard.html', {'today': yesterday, 'days7ago': days7ago, 'user': auth['user']})


# ========================================================================
# Statistics data for a time period for the relevent domain
# 	to be stored in browser localStorage and used in all calculations of prodoScore
# 	requested in an Ajax call when user changes the report time frame
# ========================================================================
def ajax_dashboard(request, is_local = False):

	# =====================================================================
	# Authorize the request
	# =====================================================================

	if not is_local:
		auth = _authorize(request)
		if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
			return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	else:
		auth = {
			'user': Employee.objects.get(id = request.user)
		}

	# sum_array = {'employees': {}, 'days': {}, 'organization': {'strata': [[], [], []]}}

	# if auth['user'].role == 70: # for manager users

	# 	##-# Getting Subordinates' id list
	# 	def get_subordinates_using_query(manager_id):
	# 		subordinates = []
	# 		subordinates_query = Employee.objects.using('replica').filter(manager_id=manager_id, role__gt = 0, status__gt = 0).values('id')
	# 		for sub in subordinates_query:
	# 			subordinates.append(sub['id'])
	# 		lower_level_subs = []
	# 		for subordinate in subordinates:
	# 			lower_level_subs += get_subordinates_using_query(subordinate)

	# 		subordinates += lower_level_subs
	# 		return subordinates

	# 	##-# End of Method



	# 	subordinates = get_subordinates_using_query(auth['user'].id)
	# 	if subordinates:
	# 		query = 'domain_id = {0} AND id in ({1})'.format(auth['user'].domain_id, ','.join([str(sub) for sub in subordinates]))

	# 		cache_key = 'dashboard_user_id-{}'.format(auth['user'].id)
	# 		sum_array = proapp.calc.scores(auth['user'].domain_id, auth['user'], query, request.GET['fromdate'], request.GET['todate'], {'cache': True, 'cache_key':cache_key})
	# 	else:
	# 		query = 'domain_id = ' + str(auth['user'].domain_id) + ' AND role > 0 AND status > 0'
	# 		sum_array = proapp.calc.scores(auth['user'].domain_id, auth['user'], query, request.GET['fromdate'], request.GET['todate'], {'cache': True})

	# 	this_todate = request.GET['todate']
	# 	this_fromdate = request.GET['fromdate']

	# 	score, score_delta, score_strata = proapp.calc.get_org_prodosocre_for_current_n_previous_periods(auth['user'], this_fromdate, this_todate)


	# 	sum_array['organization']['score'] = score
	# 	sum_array['organization']['score_delta'] = score_delta
	# 	sum_array['organization']['score_strata'] = score_strata

	# elif auth['user'].role == 80: # for admin users
	# 	query = 'domain_id = ' + str(auth['user'].domain_id) + ' AND role > 0 AND status > 0'
	# 	sum_array = proapp.calc.scores(auth['user'].domain_id, auth['user'], query,
	# 								   request.GET['fromdate'], request.GET['todate'],
	# 								   {'cache': True})

	# else:
	# 	query = 'domain_id = ' + str(auth['user'].domain_id) + ' AND role > 0 AND status > 0'
	# 	sum_array = proapp.calc.scores(auth['user'].domain_id, auth['user'], query, request.GET['fromdate'], request.GET['todate'], {'cache': True})


	# self = Employee.objects.using('replica').all().filter(id=auth['user'].id)[0]
	# sum_array['self'] = {'fullname': self.fullname, 'email': self.email, 'status': self.status, 'role': self.role, 'manager': self.manager_id, 'picture': self.picture, 'is_app_user': self.is_app_user }
	# #
	# sum_array['self']['id'] = auth['user'].id

	# ##-# Manager 52 is 'No Manager' option
	# if sum_array['self']['manager'] == 52:
	# 	del sum_array['self']['manager'] #= {'id': 52, 'email': 'admin', 'fullname': 'Admin'}
	# else:
	# 	manager_id = sum_array['self']['manager']
	# 	manager = Employee.objects.using('replica').all().filter(id=manager_id).values('email', 'fullname')[0]
	# 	sum_array['self']['manager'] = {'id': manager_id, 'email': manager['email'], 'fullname': manager['fullname']}

	query = 'domain_id = ' + str(auth['user'].domain_id) + ' AND role > 0 AND status > 0'
	sum_array = proapp.calc.scores(auth['user'].domain_id, auth['user'], query,
					   request.GET['fromdate'], request.GET['todate'],
					   {'cache': True})

	org_sum_array = sum_array

	if auth['user'].role == 70: # for manager users
		subordinates = get_subordinates_of_employee(org_sum_array['employees'], auth['user'].id)
		if subordinates:
			query = 'domain_id = {0} AND id in ({1})'.format(auth['user'].domain_id,
															 ','.join([str(sub) for sub in subordinates]))

			cache_key = 'dashboard_user_id-{}'.format(auth['user'].id)
			sum_array = proapp.calc.scores(auth['user'].domain_id, auth['user'], query,
								request.GET['fromdate'], request.GET['todate'],
								{'cache': True, 'cache_key':cache_key});

			sum_array['organization']['score'] = org_sum_array['organization']['score']
			sum_array['organization']['score_delta'] = org_sum_array['organization']['score_delta']
			sum_array['organization']['score_strata'] = org_sum_array['organization']['score_strata']
			
			##-# For Managers Organization Prodoscore Details for Chart
			sum_array['days'] = org_sum_array['days']

	if auth['user'].id in org_sum_array['employees']:
		sum_array['self'] = org_sum_array['employees'][ auth['user'].id ]
	else:
		self = Employee.objects.using('replica').all().filter(id=auth['user'].id)[0]
		sum_array['self'] = {'fullname': self.fullname, 'email': self.email,
				'status': self.status, 'role': self.role, 'manager': self.manager_id, 'picture': self.picture, 'is_app_user': self.is_app_user }
	#
	sum_array['self']['id'] = auth['user'].id
	if sum_array['self']['manager'] == 52:
		del sum_array['self']['manager']
	elif sum_array['self']['manager'] in org_sum_array['employees']:
		manager = org_sum_array['employees'][ sum_array['self']['manager'] ]
		sum_array['self']['manager'] = {'id': sum_array['self']['manager'], 'email': manager['email'],
										'fullname': manager['fullname']}

	if is_local:
		return sum_array

	return HttpResponse(json.dumps(sum_array), content_type = 'application/json')


# ========================================================================
#  Returns the list of all subordinates under the given employee.
#  This includes subordinates of particular employees subordinates
# ========================================================================
def get_subordinates_of_employee(employee_list, manager_id):
	subordinates = [emp for emp in employee_list if employee_list[emp]['manager']==manager_id]
	lower_level_subs = []
	for subordinate in subordinates:
		lower_level_subs += get_subordinates_of_employee(employee_list, subordinate)
	subordinates+=lower_level_subs
	return subordinates


# ========================================================================
# Alerts for the organization for a time period
# 	requested in an Ajax call when user changes the report time frame
# ========================================================================
def ajax_alerts(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)

	if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	# Fetch organization/employee scores
	data = proapp.calc.scores(auth['user'].domain_id, auth['user'], 'domain_id = '+str(auth['user'].domain_id),
						request.GET['fromdate'], request.GET['todate'],
						{'cache': True, 'prods': True, 'filter': 'appusers'});
	subordinates = get_subordinates_of_employee(data['employees'], auth['user'].id)

	# Prepare parameters and output data struct
	lastDay = max(data['days'].keys())
	self = auth['user']
	alerts = {'new_emp': [], 'yester_inact': [], 'yester_below': [], 'inact': [], 'below': [], 'employees': {}}

	# Iterate employees and calculate score based alerts
	for employee in data['employees']:
		if not data['employees'][employee]['is_app_user']:
			if data['employees'][employee]['manager'] == 52 and data['employees'][employee]['status'] < 1 and data['employees'][employee]['role'] == 0:
				# New Employee - needs activated (assigned)
				if self.role == 80:
					alerts['new_emp'].append([employee, '-']);
					_alerts_add_employee(alerts['employees'], employee, data['employees'][employee])
			elif self.role == 80 or employee in subordinates :
				# Is a Subbordinate
				if data['employees'][employee]['status'] < 1 or data['employees'][employee]['role'] < 1:
					# Employee Not Activated
					pass
				else:
					#
					if lastDay not in data['employees'][employee]['days'] or data['employees'][employee]['days'][lastDay]['scr']['l'] == 0:
						# Inactive on the last date
						alerts['yester_inact'].append([employee, 0])
						_alerts_add_employee(alerts['employees'], employee, data['employees'][employee])
					#
					elif data['employees'][employee]['days'][lastDay]['strata'] == 0:
						# Below Average for the last date
						alerts['yester_below'].append([ employee, data['employees'][employee]['days'][lastDay]['scr']['l'] ])
						_alerts_add_employee(alerts['employees'], employee, data['employees'][employee])
					#
					if len(data['employees'][employee]['days'].keys()) == 0 or data['employees'][employee]['scr']['l'] == 0:
						# Inactive for the whole period
						alerts['inact'].append([employee, 0])
						_alerts_add_employee(alerts['employees'], employee, data['employees'][employee])
					#
					elif data['employees'][employee]['strata'] == 0:
						# Below Average for the whole period
						alerts['below'].append([ employee, data['employees'][employee]['scr']['l'] ])
						_alerts_add_employee(alerts['employees'], employee, data['employees'][employee])
				#

	return HttpResponse(json.dumps(alerts), content_type = 'application/json')

def _alerts_add_employee(dictionary, id, emp):
	if id not in dictionary:
		dictionary[ id ] = {'fullname': emp['fullname'], 'email': emp['email'], 'picture': emp['picture']}


# ========================================================================
# Statistics data for a time period for an employee
# 	requested in an Ajax call when user selects an employee
# ========================================================================
def ajax_employee(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)

	if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	query = 'domain_id = '+str(auth['user'].domain_id)+' AND id = '+request.GET['employee']
	sum_array = proapp.calc.scores(auth['user'].domain_id, auth['user'], query,
						request.GET['fromdate'], request.GET['todate'],
						{'cache': False, 'prods': True});
	employee = sum_array['employees'][ int(request.GET['employee']) ]
	data = {'days': employee['days'] if 'days' in employee else {},
				'scr': employee['scr'] if 'scr' in employee else {'l': 0}}
	#
	if 'strata' in employee:
		data['strata'] = employee['strata']

	return HttpResponse(json.dumps(data), content_type = 'application/json')


def ajax_employees(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)

	if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	employees = []
	if request.method == 'POST':
		# Parse JSON request body
		received_json_data = json.loads(request.body)
		from_date = received_json_data.get('fromdate')
		to_date = received_json_data.get('todate')
		employees = received_json_data.get('employee')

		in_query = 'id IN ('+(','.join(employees))+')'

	sum_array = proapp.calc.scores(auth['user'].domain_id, auth['user'], in_query,
								   from_date, to_date,
						{'cache': False, 'prods': True, 'employeeList' : True});
	employees = sum_array['employees']

	data = {}
	for employee in employees:
		data[employee] = employees[employee]
		data[employee] = {'days': employees[employee]['days'] if 'days' in employees[employee] else {},
				'scr': employees[employee]['scr'] if 'scr' in employees[employee] else {'l': 0}}
		if 'strata' in employees[employee]:
			data[employee]['strata'] = employees[employee]['strata']

	return HttpResponse(json.dumps(data), content_type = 'application/json')


# ========================================================================
# Details for a given statistic record
# 	requested in an Ajax call when drill down a statistic value
# ========================================================================
def ajax_details(request):

	# =====================================================================
	# =====================================================================
	# Authorize the request
	auth = _authorize(request)

	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	# =====================================================================
	# Check on the cache
	# =====================================================================
	cacheKeyDetails = 'details'+json.dumps(request.GET).replace('": "', '=').replace('", "', '&').replace('"', '')

	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine') and ('cache' not in request.GET or request.GET['cache'] != 'invalidate'):
		data = memcache.get(cacheKeyDetails)
		if data is not None:
			return HttpResponse(data, content_type = 'application/json')

	# =====================================================================
	# Prepare data structures to be populated
	# =====================================================================
	details_array = {}
	limits = {}
	checkForTranscripts = []

	# =====================================================================
	# Fetch details for a given statistic
	# =====================================================================
	if 'product' in request.GET:
		stat_record = Statistic.objects.using('replica').filter(date=request.GET['date'], employee_id=request.GET['employee'], product=proapp.calc.products[ request.GET['product'] ][0] )[0]

		details = Detail.objects.using('replica').filter(statistic=stat_record)
		for detail in details:
			if stat_record.product == 'hangouts' and auth['user'].domain.show_details == 0:
				detail.title = '[ Chat Message ]'
			record = {'resource': detail.resource_id, 'title': detail.title, 'flag': detail.flag, 'data': detail.data.replace('u\'', '\''),
					'time': [detail.start_time, detail.end_time], 'time_range':detail.time_range}
					#'sentiment': detail.nlp_entities['documentSentiment'] if 'documentSentiment' in detail.nlp_entities else {'magnitude': 0, 'score': 0},
			if auth['user'].domain.nlp_enabled == 1:
				record['sentiment'] = {'magnitude': detail.sentiment_magnitude, 'score': detail.sentiment_score}
			details_array[detail.id] = record
			#
			if stat_record.product in ['turbobridge', 'ringc_calls']:
				checkForTranscripts.append(detail.id)

	else:
		stat_record = Statistic.objects.using('replica').filter(date=request.GET['date'], employee_id=request.GET['employee'])

		for stat_rec in stat_record:
			details = Detail.objects.using('replica').filter(statistic=stat_rec)
			total_records = len(details)
			if total_records > 210:
				details = Detail.objects.using('replica').filter(statistic=stat_rec, flag=0)
				details.query.set_limits(low=0, high=200)
				limits[ stat_rec.product ] = total_records
			#
			for detail in details:
				if stat_rec.product == 'hangouts' and auth['user'].domain.show_details == 0:
					detail.title = '[ Chat Message ]'
				record = {'title': detail.title, 'flag': detail.flag, 'product': proapp.calc.prod_dic[stat_rec.product],
						'time': [detail.start_time, detail.end_time],'time_range':detail.time_range}#, 'data': detail.data
				if auth['user'].domain.nlp_enabled == 1:
					record['sentiment'] = {'magnitude': detail.sentiment_magnitude, 'score': detail.sentiment_score}
				details_array[detail.id] = record
				#try:
				#	if 'documentSentiment' in detail.nlp_entities:
				#		nlp_entities = json.loads(detail.nlp_entities)
				#		details_array[detail.id]['sentiment'] = nlp_entities['documentSentiment']
				#	else:
				#		details_array[detail.id]['sentiment'] = {'magnitude': 0, 'score': 0}
				#except Exception:
				#	details_array[detail.id]['sentiment'] = {'magnitude': 0, 'score': 0}
				#
				if stat_rec.product in ['turbobridge', 'ringc_calls']:
					checkForTranscripts.append(detail.id)

	if len(checkForTranscripts) > 0:
		checkForTranscripts = Transcript_job.objects.using('replica').filter(detail_id__in=checkForTranscripts, status=10, percentage=100)
		for checkForTranscript in checkForTranscripts:
			details_array[ checkForTranscript.detail_id ]['hasTranscript'] = True

	# =====================================================================
	# Write JSON to the response
	# =====================================================================
	tmp_array = sorted(details_array.items(), key=sortKeyTime)
	details_array = {}
	for rec in tmp_array:
		details_array[rec[0]] = rec[1]
	#
	details_array = {'detail': details_array, 'limits': limits}
	try:
		details_array = json.dumps(details_array, ensure_ascii=False)
	except:
		details_array = json.dumps(details_array, encoding='latin1')

	# Store on cache
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		try:
			memcache.add(cacheKeyDetails, details_array, 12*3600)
		except ValueError:
			print('Too large: '+cacheKeyDetails)

	return HttpResponse(details_array, content_type = 'application/json')


# ========================================================================
# Detaildata for a given statistic detail
# 	requested in an Ajax call when drill down for statistic meta-data
# ========================================================================
def ajax_detail_data(request):
	import re
	import logging

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	# If we process authorization to restict access - copying data from live to dev would not work..

	sum_array = {'data': {}, 'entities': {}, 'sentiment': {'magnitude': 0, 'score': 0}}
	if auth['user'].domain.show_details == 0:
		sum_array['notice'] = 'feature disabled by admin'
	else:
		title_data = None
		# Fetch details record
		details = Detail.objects.using('replica').filter(id=request.GET['id'])
		if len(details) > 0:
			detail = details[0]
			#
			try:
				nlp_entities = json.loads(detail.nlp_entities)
				if 'documentSentiment' in nlp_entities:
					sum_array['sentiment'] = nlp_entities['documentSentiment']
			except:
				logging.warning('JSON Expected: '+str(detail.nlp_entities))
			#
			# Fetch related stat record
			stat_rec = detail.statistic

			# Unserialize JSON or Python serialized string to data object
			if stat_rec.product == 'broadsoft':
				tmp = eval(detail.data)
				title_data = tmp
				if 'remoteParty' in tmp:
					detail.data = tmp['remoteParty']
					detail.data['personality'] = tmp['personality']
				else:
					detail.data = {}

				if 'Status' in title_data and 'Direction' in title_data and 'network' in title_data:
					detail.data['Status'] = title_data['Status']
					detail.data['network'] = title_data['network']
					detail.data['Direction'] = title_data['Direction']
					try:
						detail.data['duration'] = title_data['duration']
					except KeyError: # No duration available for missed calls
						logging.error("Caught KeyError : duration for detail id : {}".format(detail.id))
						pass
				else:
					try:
						if 'callType' in title_data['call']['remoteParty']:
							if title_data['call']['remoteParty']['callType'] == 'Group':
								detail.data['network'] = 'Internal'
							elif title_data['call']['remoteParty']['callType'] == 'Network':
								detail.data['network'] = 'External'
					except KeyError:
						logging.error("Caught KeyError : callType : {}".format(detail.id))
						pass

					try:
						if 'personality' in title_data['call']:
							if title_data['call']['personality'] == 'Originator':
								detail.data['Direction'] = 'Outbound'
							elif title_data['call']['personality'] == 'Terminator':
								detail.data['Direction'] = 'Inbound'
					except KeyError:
						logging.error("Caught KeyError : Direction : {}".format(detail.id))
						pass

					try:
						if "answerTime" in title_data['call'] and "detachedTime" in title_data['call']:
							detail.data['Status'] = 'voice mail'
						elif "answerTime" in title_data['call']:
							detail.data['Status'] = 'Answered call'
						else:
							detail.data['Status'] = 'Missed call'
					except KeyError:
						logging.error("Caught KeyError : Status : {}".format(detail.id))
						pass

					try:
						if 'duration' not in detail.data and detail.start_time != detail.end_time and detail.data['Status'] != 'Missed call':  # stat_rec.product != 'call'
							detail.data['duration'] = detail.end_time - detail.start_time
					except KeyError:
						logging.error("Caught KeyError : legacy data : {}".format(detail.id))
						pass

			elif detail.data is None:
				detail.data = {}
			else:
				detail.data = json.loads(detail.data.replace('u\'', '\''))
				#
				if isinstance(detail.data, unicode) or isinstance(detail.data, str):
					try:
						detail.data = json.loads(detail.data)
					except:
						detail.data = eval(detail.data)

				# If a name-value array such as email headers - load in to key-value paired object
				if isinstance(detail.data, list):
					tmp = {}
					for row in detail.data:
						tmp[ row['name'] ] = row['value']
					detail.data = tmp

				if 'duration' not in detail.data and detail.start_time != detail.end_time:
					duration =  detail.end_time - detail.start_time
					if duration > 0:
						detail.data['duration'] = detail.end_time - detail.start_time
					else:
						detail.data['duration'] = 1440 + (detail.end_time - detail.start_time)

			# Parse Gmail multipart body
			if stat_rec.product == 'gmail' and 'body' in detail.data:
				detail.data['body'] = base64.b64decode(detail.data['body'].replace('_', '/').replace('-', '+'))
				detail.data['body'] = re.sub(r'<style(.|\n)*?<\/style>', '', detail.data['body'])
				detail.data['body'] = ' '.join( re.sub(r'<[^>]*?>', '', detail.data['body']).split() )
				if len(detail.data['body']) > 280:
					detail.data['body'] = detail.data['body'][:detail.data['body'].find(' ', 270)]+' ...'
				if 'References' in detail.data:
					del detail.data['References']
     
			if stat_rec.product == 'outlook' and 'toRecipients' in detail.data:
				to = ''
				for emailAddress in detail.data['toRecipients']:
					emailName = ''
					emailAddres = ''
					if 'name' in emailAddress['emailAddress']:
						emailName = emailAddress['emailAddress']['name'];
					if 'address' in emailAddress['emailAddress']:
						emailAddres = emailAddress['emailAddress']['address']
					to = to + '{0} <{1}> '.format(emailName, emailAddres)
				detail.data['To'] = to

			# Trubo Bridge transcriptions
			#elif stat_rec.product == 'turbobridge' and 'transcript' in detail.data:
			#	if len(detail.data['transcript']) > 280:
			#		detail.data['transcript'] = detail.data['transcript'][:detail.data['transcript'].find(' ', 270)]+' ...'

			# Google Drive documents
			elif stat_rec.product == 'docs' and 'content' in detail.data:
				if detail.data['content'] and len(detail.data['content']) > 280:
					detail.data['content'] = detail.data['content'][:detail.data['content'].find(' ', 270)]+' ...'
			#
			entities = {}
			entitiesList = Nlp_entity2detail.objects.using('nlp').filter(detail_id=detail.id, salience__gte=0.025)
			for entity in entitiesList:
				try:
					entities[entity.nlpentity_id] = {'etype': entity.nlpentity.etype, 'salience': entity.salience}
					attribs = Nlp_entity_attribs.objects.using('nlp').filter(nlp_entity=entity.nlpentity)
					for attrib in attribs:
						entities[entity.nlpentity_id][attrib.attrib] = attrib.value
				except:
					pass
			#
			sum_array['data'] = detail.data
			sum_array['entities'] = entities
	try:
		sum_array = json.dumps(sum_array)
	except:
		sum_array = json.dumps(sum_array, encoding='latin1')
	return HttpResponse(sum_array, content_type = 'application/json')


def ajax_detail_data_raw(request):
	details = Detail.objects.using('replica').filter(id=request.GET['id'])
	sum_array = {'error': 'not-found'}
	if len(details) > 0:
		if request.GET['filename'] == 'data':
			sum_array = details[0].data
		elif request.GET['filename'] == 'presynaptic':
			sum_array = details[0].nlp_presynaptic
		elif request.GET['filename'] == 'entities':
			sum_array = details[0].nlp_entities
	#
	return HttpResponse(sum_array, content_type = 'application/json')


def sortKeyTime(rec):
	key, d = rec
	if 'time' in d and len(d['time']) > 0:
		return int(d['time'][0])
	return 0


# ========================================================================
# Subordinates Query-String
# ========================================================================
def subordinates_querystring(request, auth):
	# =====================================================================
	# Fetch data from database
	# =====================================================================
	if 'employee' not in request.GET and auth['user'].role < 80:
		subordinates = get_subs_for_filter(auth)
		subordinates = ','.join(str(sub) for sub in subordinates)
	#
	if 'employee' in request.GET:
		subordinates = '&employee='+request.GET['employee']
	elif auth['user'].role < 80:
		subordinates = '&employee='+subordinates
	else:
		subordinates = ''
	# print(subordinates)
	return subordinates


# ========================================================================
# Correlation Entities
# ========================================================================
def ajax_correlations(request, is_local = False):
	from django.db.models import Count

	# =====================================================================
	# Authorize the request
	# =====================================================================
	if not is_local:
		auth = _authorize(request)
		if 'user' not in auth or 'redirect' in auth:
			return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	else:
		auth = {
			'user': Employee.objects.get(id = request.user)
		}

	subordinates = subordinates_querystring(request, auth)
	apidata = requests.get(settings.VERSIONS['correlations-api'][ settings.VERSIONS['this'] ]+'correlations-ajax/'+
				'?domain='+str(auth['user'].domain.id)+subordinates+
				'&fromdate='+request.GET['fromdate']+
				'&todate='+request.GET['todate']+
				('&intext='+request.GET['intext'] if 'intext' in request.GET else '')+
				('&etype='+request.GET['etype'] if 'etype' in request.GET else '')+
				('&q='+request.GET['q'] if 'q' in request.GET else '')+
				('&limit='+request.GET['limit'] if 'limit' in request.GET else ''),
			verify=False,
			timeout=666,
			headers={"authorization":"bearer VGhpcyBpcyBhIHNlY3VyaXR5IHRva2VuIHRvIGF1dGhvcml6ZSBwcm9kb3Njb3JlIGFwcCB0byBhY2Nlc3MgY29ycmVsYXRpb25zIGNvcnRleA=="}
		).content

	if is_local:
		return json.loads(apidata)
	else:
		return HttpResponse(apidata, content_type = 'application/json')


# ========================================================================
# Correlation Entities
# ========================================================================
def correlations_graph(request):
	from django.db.models import Count

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	subordinates = subordinates_querystring(request, auth)
	apidata = requests.get(settings.VERSIONS['correlations-api'][ settings.VERSIONS['this'] ]+'correlations-graph/'+
				'?domain='+str(auth['user'].domain.id)+subordinates+
				'&fromdate='+request.GET['fromdate']+
				'&todate='+request.GET['todate']+
				('&intext='+request.GET['intext'] if 'intext' in request.GET else ''),
			verify=False,
			timeout=666,
			headers={"authorization":"bearer VGhpcyBpcyBhIHNlY3VyaXR5IHRva2VuIHRvIGF1dGhvcml6ZSBwcm9kb3Njb3JlIGFwcCB0byBhY2Nlc3MgY29ycmVsYXRpb25zIGNvcnRleA=="}
		).content

	return HttpResponse(apidata, content_type = 'application/json')


# ========================================================================
# Correlation Detail Aggregate
# ========================================================================
def ajax_correlation_aggr(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	# =====================================================================
	# Prepare data structures to be populated
	# =====================================================================
	output = {'prod': {}, 'emp': {}, 'days': {}}
	subordinates = subordinates_querystring(request, auth)
	apidata = requests.get(settings.VERSIONS['correlations-api'][ settings.VERSIONS['this'] ]+'correlated-aggr/'+
				'?entity='+str(request.GET['entity'])+subordinates+
				'&fromdate='+request.GET['fromdate']+
				'&todate='+request.GET['todate']+
				('&intext='+request.GET['intext'] if 'intext' in request.GET else ''),
			verify=False,
			timeout=666,
			headers={"authorization":"bearer VGhpcyBpcyBhIHNlY3VyaXR5IHRva2VuIHRvIGF1dGhvcml6ZSBwcm9kb3Njb3JlIGFwcCB0byBhY2Nlc3MgY29ycmVsYXRpb25zIGNvcnRleA=="}
		).content

	# =====================================================================
	# Fetch data from database
	# =====================================================================
	apidata = json.loads(apidata)
	#print(apidata)
	total = 0
	for product in apidata['products']:
		total += product['count']
		prod = proapp.calc.prod_dic[product['product']]
		output['prod'][prod] = {'name': prod, 'count': product['count']}
	#
	for employee in apidata['employees']:
		emp = Employee.objects.get(id=employee['id'])
		output['emp'][employee['id']] = {'id': employee['id'], 'fullname': emp.fullname, 'picture': emp.picture, 'count': employee['count']}
	#
	for day in apidata['days']:
		sdate = (int(datetime.datetime.strptime(day['date'], '%Y-%m-%d').strftime('%s')) / 86400) - 16800
		output['days'][sdate] = day['count']

	prods = []
	for prod in output['prod']:
		output['prod'][prod]['perc'] = 100 * output['prod'][prod]['count'] / total
		prods.append(output['prod'][prod])
	emps = []
	others = []
	totPerc = 0
	totEmp = 0
	for emp in output['emp']:
		output['emp'][emp]['perc'] = 100 * output['emp'][emp]['count'] / total
		if output['emp'][emp]['perc'] > 5:
			totPerc += output['emp'][emp]['perc']
			totEmp += output['emp'][emp]['count']
			emps.append(output['emp'][emp])
		else:
			others.append(str(emp))
	if totPerc < 99 and total-totEmp > 0:
		emps.append({'id': ','.join(others), 'count': total-totEmp, 'perc': 100-totPerc, 'fullname': 'Others'})#, 'picture': ''

	return HttpResponse(json.dumps({
			'prod': sorted(prods, key=operator.itemgetter('count'), reverse=True),
			'emp': sorted(emps, key=operator.itemgetter('count'), reverse=True),
			'days': output['days']
		}), content_type = 'application/json')


def get_subs_for_filter(auth):
	allEmployees = Employee.objects.using('replica').filter(domain_id=auth['user'].domain_id).values('id', 'manager_id')
	subordinates = {}
	for emp in allEmployees:
		subordinates[emp['id']] = {'manager': emp['manager_id']}
	subordinates = get_subordinates_of_employee(subordinates, auth['user'].id)
	subordinates.append(auth['user'].id)

	return subordinates


# ========================================================================
# Correlation Details
# ========================================================================
def ajax_correlation_details(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	# =====================================================================
	# Prepare data structures to be populated
	# =====================================================================
	output = []
	subordinates = subordinates_querystring(request, auth)
	apidata = requests.get(settings.VERSIONS['correlations-api'][ settings.VERSIONS['this'] ]+'correlated-details/'+
				'?entity='+str(request.GET['entity'])+subordinates+
				'&fromdate='+request.GET['fromdate']+
				'&todate='+request.GET['todate']+
				('&page='+request.GET['page'] if 'page' in request.GET else '')+
				('&product='+request.GET['product'] if 'product' in request.GET else '')+
				('&intext='+request.GET['intext'] if 'intext' in request.GET else ''),
			verify=False,
			timeout=666,
			headers={"authorization":"bearer VGhpcyBpcyBhIHNlY3VyaXR5IHRva2VuIHRvIGF1dGhvcml6ZSBwcm9kb3Njb3JlIGFwcCB0byBhY2Nlc3MgY29ycmVsYXRpb25zIGNvcnRleA=="}
		).content

	# =====================================================================
	# Fetch data from database
	# =====================================================================
	details = Detail.objects.using('replica').filter(id__in=json.loads(apidata))
	pKey = ''
	pRec = {}
	for detail in details:
		statistic = detail.statistic
		product = proapp.calc.prod_dic[ statistic.product ]
		tKey = str(statistic.employee_id) + '-' + product+ '-' + detail.title
		if tKey == pKey:
			pRec['x'] += 1
		else:
			pRec = {
				'id': detail.id,
				'x': 1,
				'product': product,
				'date': statistic.date,
				'end_time': detail.end_time,
				'start_time': detail.start_time,
				'flag': detail.flag,
				'sentiment': {
					'score': detail.sentiment_score,
					'magnitude': detail.sentiment_magnitude
				},
				'emp_id': statistic.employee_id,
				'employee': statistic.employee.fullname,
				'title': detail.title
			}
			output.append(pRec)
			pKey = tKey
	#
	return HttpResponse(json.dumps(output), content_type = 'application/json')


# ========================================================================
# Correlation Entities for an employee for a given day
# ========================================================================
'''def emp_day_correlations(request):
	from django.db.models import Count

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	# =====================================================================
	# Prepare data structures to be populated
	# =====================================================================
	oentities = {}
	timeline = {}

	# =====================================================================
	# Fetch data from database
	# =====================================================================
	apidata = requests.get(settings.VERSIONS['correlations-api'][ settings.VERSIONS['this'] ]+'correlations-ajax/'+
				'?domain='+str(auth['user'].domain.id)+
				'&employee='+request.GET['employee']+
				'&fromdate='+request.GET['date']+
				'&todate='+request.GET['date'],
			verify=False,
			timeout=666,
			headers={"authorization":"bearer VGhpcyBpcyBhIHNlY3VyaXR5IHRva2VuIHRvIGF1dGhvcml6ZSBwcm9kb3Njb3JlIGFwcCB0byBhY2Nlc3MgY29ycmVsYXRpb25zIGNvcnRleA=="}
		).content
	print(apidata)
	entities = Nlp_entity.objects.using('replica').filter().extra(where=[
				'id IN (SELECT nlpentity_id FROM proapp_nlp_entity2detail e2d '+
				'LEFT JOIN proapp_detail pd ON pd.id = e2d.detail_id '+
				'LEFT JOIN proapp_statistic ps ON ps.id = pd.statistic_id '+
				'WHERE ps.employee_id = '+request.GET['employee']+' AND ps.date = \''+request.GET['date']+'\')'
			])
	#
	# Get all stat details relevent for the employee for the day
	details = Detail.objects.using('replica').filter().extra(where=['statistic_id IN (SELECT id FROM proapp_statistic WHERE employee_id = '+request.GET['employee']+' AND date = \''+request.GET['date']+'\')'])
	#
	# Map correlations to work-timeline of day
	for entity in entities:
		attribs = Nlp_entity_attribs.objects.using('replica').filter(nlp_entity=entity).order_by('-confidence')
		oentities[ entity.id ] = {'type': entity.etype}
		for attrib in attribs:
			if attrib.attrib not in oentities[ entity.id ]:
				oentities[ entity.id ][ attrib.attrib ] = attrib.value
		#
		e2detail = Nlp_entity2detail.objects.using('nlp').filter(nlpentity=entity, detail_id__in=details)
		for detail in e2detail:
			etime = 10 * int(detail.detail.start_time / 10)
			if etime in timeline:
				timeline[ etime ].append(entity.id)
			else:
				timeline[ etime ] = [entity.id]

	return HttpResponse(json.dumps({'timeline': timeline, 'entities': oentities}), content_type = 'application/json')
'''

# ========================================================================
# Manually flag or unflag stat details
# ========================================================================
def flag_stats(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	received_json_data=json.loads(request.body)
	if isinstance(received_json_data['id'], list):
		statDetail = Detail.objects.filter(id__in=received_json_data['id'])
	else:
		statDetail = Detail.objects.filter(id=received_json_data['id'])
	#
	if len(statDetail) == 0:
		return HttpResponse(json.dumps({'error': 'record not found'}), content_type = 'application/json')

	# First, check if stat is older than a week
	age = (datetime.datetime.now() - datetime.datetime.strptime(statDetail[0].statistic.date, '%Y-%m-%d')).days
	if age > 8:
		return HttpResponse(json.dumps({'error': 'expired'}), content_type = 'application/json')

	# Check if the user is authorized to do this action.
	employee = statDetail[0].statistic.employee
	if employee.domain_id != auth['user'].domain_id:
		return HttpResponse(json.dumps({'error': 'unautorized'}), content_type = 'application/json')
	if not (auth['user'].role == 80 or
		(auth['user'].role == 70 and (
			employee.manager_id == auth['user'].id or
			employee.manager.manager_id == auth['user'].id))):
		return HttpResponse(json.dumps({'error': 'unautorized'}), content_type = 'application/json')

	didAnything = False
	for statDetailRec in statDetail:
		# Get the current status and related records.
		if (received_json_data['action'] == 'flag' and statDetailRec.flag == 0) or (received_json_data['action'] == 'unflag' and statDetailRec.flag != 0):
			didAnything = True

		# Update the related records.
		score = statDetailRec.end_time - statDetailRec.start_time
		if score == 0:
			score = 1
		if received_json_data['action'] == 'flag':
			statDetailRec.flag = 4
			if statDetailRec.counted == 1:
				statDetailRec.statistic.score -= score
			statDetailRec.counted = 0
		elif received_json_data['action'] == 'unflag':
			statDetailRec.flag = 0
			if statDetailRec.counted == 0:
				statDetailRec.statistic.score += score
			statDetailRec.counted = 1

		statDetailRec.save()
		statDetailRec.statistic.save()
	#
	if not didAnything:
		return HttpResponse(json.dumps({'error': 'nothing to do.'}), content_type = 'application/json') #' already '+received_json_data['action']+'ged'

	# Update, no just delete the related memcached entries.
	# WOULD BE GREAT IF WE CAN JUST UPDATE THE MEMCACHED ENTRIES
	sdate = (int(datetime.datetime.strptime(statDetail[0].statistic.date, '%Y-%m-%d').strftime('%s')) / 86400) - 16800
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		data = memcache.delete('score-'+str(employee.id)+'-on-'+str(sdate))
		data = memcache.delete('domain-'+str(employee.domain_id)+'-on-'+str(sdate))
		data = memcache.delete('details{date='+str(statDetail[0].statistic.date)+'&employee='+str(employee.id)+'}')
	#
	store_score = Employee_prodoscore.objects.filter(employee_id=employee.id, date=statDetail[0].statistic.date)
	if len(store_score) > 0:
		store_score[0].score = -1
		store_score[0].save()

	proapp.settings.schedule_file_creating_for_domain(employee.domain_id) ##-# Creating Files
	new_score = proapp.calc.emps_day_score([employee.id], sdate, True)
	return HttpResponse(json.dumps({'message': 'success', 'new-score': new_score}), content_type = 'application/json')


# ========================================================================
# Alerts feed for the current domain
# 	requested in an Ajax call
# ========================================================================
def alerts(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	# =====================================================================
	# Retrieve alerts
	# =====================================================================
	alerts_array = {}
	alerts = Detail.objects.filter(flag__gt=1).extra(where=['statistic_id IN ('+
		'SELECT id FROM proapp_statistic WHERE (date > \''+request.GET['fromdate']+'\' AND date < \''+request.GET['todate']+'\') AND employee_id IN ('+
			'SELECT id FROM proapp_employee WHERE '+('domain_id = '+str(auth['user'].domain_id) if auth['user'].role == 80 else 'manager_id = '+str(auth['user'].id))+
		'))'])
	for alert in alerts:
		alerts_array[alert.id] = {'employee': alert.statistic.employee_id, 'fullname': alert.statistic.employee.fullname, 'date': alert.statistic.date, 'time': [alert.start_time, alert.end_time], 'product': alert.statistic.product, 'resource_id': alert.resource_id, 'title': alert.title}#, 'data': json.loads(alert.data.replace('u\'', '\''))

	tmp_array = sorted(alerts_array.items(), key=sortKeyTime)
	alerts_array = []
	for rec in tmp_array:
		alerts_array.append(rec[1])

	return HttpResponse(json.dumps(alerts_array), content_type = 'application/json')


# ========================================================================
# Notification feed for the current domain
# 	requested in an Ajax call periodically
# ========================================================================
def ajax_notifications(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	# =====================================================================
	# Retrieve notifications since the last offset
	# =====================================================================
	notification_array = {}
	if 'offset' in request.GET:
		offset = int(request.GET['offset'])
	else:
		offset = 0
	notifications = Notification.objects.filter(domain=auth['user'].domain_id, id__gt=offset)[:8]
	for notification in notifications:
		notification_array[notification.id] = {'level': notification.level, 'message': notification.message}

	return HttpResponse(json.dumps(notification_array), content_type = 'application/json')


# ========================================================================
# Receive user feedback
# ========================================================================
def feedback(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	received_json_data = json.loads(request.body)

	# =====================================================================
	# Create issue on Jira
	# =====================================================================
	jiraId = 'villvay.admin@prodoscore.com'
	jiraPassword = 'RQnePxkFfAgc9B0mTDTP9363'
	jira = requests.post('https://villvay.atlassian.net/rest/api/2/issue',
			data = json.dumps({"fields": {
					   "project": {"key": "PROD"},
					   "summary": "User feedback from "+auth['user'].email,
					   "description": (received_json_data['feedback']+
					       '\n\n----------\nUser: '+auth['user'].email+
					       '\nUser agent: '+received_json_data['useragent']+
					       '\nURL: '+received_json_data['hash']+
					       ('\nDate filter: '+received_json_data['from_date']+' to '+received_json_data['to_date'] if 'from_date' in received_json_data else '')),
					   "issuetype": {"name": "CR"},
					   "customfield_11400": {"id": "10404"},
					   "labels":["UserFeedback"]
					  }
					}),
			headers={'Authorization': 'Basic '+base64.b64encode(jiraId+':'+jiraPassword), 'Content-Type': "application/json"})
	jira = json.loads(jira.content)
	print(jira)

	# =====================================================================
	# Save feedback to database
	# =====================================================================
	feedback = Feedback(employee=auth['user'], status='new', feedback=received_json_data['feedback'], screenshot=received_json_data['screenshot'],
					jira_key=jira['key'],
					useragent=received_json_data['useragent'], url_hash=received_json_data['hash'])
	if 'from_date' in received_json_data:
		feedback.filter_fromdate = received_json_data['from_date']
		feedback.filter_todate = received_json_data['to_date']
	else:
		feedback.filter_fromdate = datetime.datetime.now().strftime('%Y-%m-%d')
		feedback.filter_todate = datetime.datetime.now().strftime('%Y-%m-%d')
	#
	feedback.save()
	#
	data = {'employee': auth['user'], 'feedback': received_json_data['feedback'], 'screenshot': received_json_data['screenshot'], 'fid': feedback.id, 'jira_key': jira['key']}
	#
	# Get the email HTML rendered
	res = render(request, 'email/feedback.html', data)
	#
	# Send notification email to support team
	requests.post(
		settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']),
		data={"from": settings.MAILGUN['FROM'],
			"to": ["support@prodoscore.com"],
			"subject": "User feedback from "+auth['user'].fullname,
			"html": res})
	#
	# Get the email HTML rendered
	res = render(request, 'email/feedback-thx.html', data)
	#
	# Send thank-you/received email to user
	res = requests.post(
		settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']),
		data={"from": settings.MAILGUN['FROM'],
			"to": [auth['user'].email],
			"subject": "We have received your feedback. Thank you.!",
			"html": res})
	#
	# Add screenshot as attachment to Jira issue
	if received_json_data['screenshot'] == '[NONE]':
		image = False
	else:
		image = base64.b64decode(received_json_data['screenshot'])
		jira_attachment = requests.post('https://villvay.atlassian.net/rest/api/2/issue/'+jira['key']+'/attachments',
							files={'file': ('screenshot.html', image)},
							headers={'Authorization': 'Basic '+base64.b64encode(jiraId+':'+jiraPassword), 'X-Atlassian-Token': 'no-check'})
	#
	return HttpResponse(json.dumps({'message': 'received'}), content_type = 'application/json')


# ========================================================================
# Receive usage statistics
# ========================================================================
def usage(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = _authorize(request)
	if 'user' in auth and 'redirect' not in auth:

		# ==================================================================
		# Save usage statistics to database
		# ==================================================================
		received_json_data = json.loads(request.body)
		for click in received_json_data:
			click = Click(employee=auth['user'], date=click[0], page=click[1], dompath=click[2], x=click[3], y=click[4], screenwidth=click[5], scrolltop=click[6])
			click.save()

	return HttpResponse(json.dumps({'message': 'received'}), content_type = 'application/json')


def get_employees(domain_id):
	domain_all_users = {}
	users = Employee.objects.filter(domain_id=domain_id)
	for user in users:
		domain_all_users[user.id] = {}
		domain_all_users[user.id]['fullname'] = user.fullname
		domain_all_users[user.id]['email'] = user.email
		domain_all_users[user.id]['role'] = user.role
		domain_all_users[user.id]['manager'] = user.manager_id
		domain_all_users[user.id]['status'] = 1 if user.status == 1 else -1
		domain_all_users[user.id]['picture'] = user.picture
		domain_all_users[user.id]['is_app_user'] = user.is_app_user
	return domain_all_users


def disconnect_products(request):
	auth = _authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	domain = auth['user'].domain
	product = request.GET['product']

	product_disconnect(domain, product, request, auth['user'])

	return HttpResponse(json.dumps({'message': 'Success'}), content_type = 'application/json')


def product_disconnect(domain, product, request, by):
	if product == 'turbobridge':
		domain.conf_system = None
		domain.conf_account = None
		domain.conf_partner = None
		domain.conf_admin = None
		domain.conf_password = None
		employees = Employee.objects.filter(domain_id=domain.id).exclude(conf_id__isnull=True).exclude(conf_id__exact='')
		if len(employees) > 0:
			for employee in employees:
				employee.conf_id = None
				employee.save()
	if product == 'zoho' or product == 'salesforce' or product == 'prosperworks' or product == 'sugar':
		domain.crm_system = None
		domain.crm_admin = None
		domain.crm_token = None
		domain.crm_modules = '63'
		domain.salesforce_userid = None
		domain.salesforce_password = None
		domain.salesforce_token = None
		domain.salesforce_id = None
		employees = Employee.objects.filter(domain_id=domain.id).exclude(crm_id__isnull=True).exclude(crm_id__exact='')
		if len(employees) > 0:
			for employee in employees:
				employee.crm_id = None
				employee.salesforce_userid = None
				employee.save()
		if product == 'sugar':
			sugar_settings = Sugar_settings.objects.filter(domain_id=domain.id)
			for sugar_setting in sugar_settings:
				sugar_setting.delete()
	if product == 'ringcentral' or product == 'broadsoft' or product == 'vbc':
		domain.phone_system = None
		domain.phone_system_id = None
		domain.phone_system_password = None
		domain.phone_system_admin = None
		domain.phone_system_token = None
		domain.broadsoft_xsi = None
		domain.broadsoft_userid = None
		domain.broadsoft_password = None
		employees = Employee.objects.filter(domain_id=domain.id).exclude(phone_system_id__isnull=True).exclude(phone_system_id__exact='')
		if len(employees) > 0:
			for employee in employees:
				employee.phone_system_id = None
				employee.save()
		#
		to_delete = Broadsoft_admin.objects.filter(domain=domain)
		for rec in to_delete:
			bs_users = Broadsoft_user.objects.filter(admin=rec.id)
			for rec2 in bs_users:
				rec2.delete()
			#
			to_dissociate = Employee.objects.filter(broadsoft_admin=rec)
			for rec2 in to_dissociate:
				rec2.broadsoft_admin_id = None
				rec2.broadsoft_userid = None
				rec2.save()
			rec.delete()
		#
		ringc_objects = RingCentral.objects.filter(domain_id=domain.id)
		for ringc_object in ringc_objects:
			ringc_object.delete()
		#
		vbc_objects = Vbc.objects.filter(domain_id=domain.id)
		for vbc_object in vbc_objects:
			vbc_object.delete()
	#
	# Send email
	from django.shortcuts import render
	from .models import Product
	product = Product.objects.using('replica').filter(slug=product)
	if len(product) > 0:
		product = product[0]
		res = render(request, 'email/admin-product-disconnect.html', {'domain': domain, 'product': product, 'user': by})
		postdata = {"from": settings.MAILGUN['FROM'], 'to': [],
				"subject": "You have disconnected "+product.title+" from Prodoscore for "+domain.title,
				"html": res}
		admins = Employee.objects.filter(role=80, status=1, domain_id=domain.id)
		for admin in admins:
			if not (admin.report_email is None) and '@' in admin.report_email:
				postdata['to'].append(str(admin.report_email))
			elif '@' in admin.email:
				postdata['to'].append(str(admin.email))
		#
		postdata['bcc'] = ['support@prodoscore.com']
		requests.post(settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']), data=postdata)
	#
	domain.save()

def ajax_dashboard_load_details(request):

	auth = _authorize(request)
	if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	if 'period' not in request.GET:
		return HttpResponse(json.dumps({'message': 'No Period in URL Param'}), content_type='application/json')

	period = request.GET['period']
	if period not in ['yesterday', 'this_week', 'last_7_days', 'this_month', 'last_30_days']:
		return HttpResponse(json.dumps({'message': 'Invalid Period'}), content_type='application/json')

	user = auth['user']

	##-# ############################################################################
	##-# Getting Details from Memcache
	##-# ############################################################################
	tag = 'dashboard-loading-details'
	cachekey = tag + '-' + str(user.id) + '-' + period
	file_content = proapp.cache.get(cachekey)

	##-# ############################################################################
	##-# Getting Details from Cloud Storage
	##-# ############################################################################
	if file_content is None: ##-# Cache is not Foundable... Trying to Read the File
		file_content = proapp.storage.read_file_with_path(user.id, user.domain_id, period, tag)

	##-# ############################################################################
	##-# Getting Details by ON THE FLY CALCULATIONS
	##-# ############################################################################
	if file_content is None: ##-# File is not Foundable... Trying to Calculate on the Fly

		class Request:
			def __init__(self, user_id, date, period):
				self.GET = {
				'employee': user_id,
				'date': date,
				'period': period
				}

		if 'date' not in request.GET:
			return HttpResponse(json.dumps({'message': 'Date URL Param is Neede. File and Cache is nor Foundable'}), content_type='application/json')

		date = request.GET['date']
		fake_request = Request(user.id, date, period)
		file_content = proapp.cron.dashboard_details_cron(fake_request, {'require': tag})
	else:
		proapp.cron.add_to_cache(user.id, period, tag, file_content)

	if type(file_content) is dict:
		file_content = json.dumps(file_content)
	return HttpResponse(file_content, content_type = 'application/json')


def ajax_dashboard_employee_list(request):

	auth = _authorize(request)
	if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	if 'period' not in request.GET:
		return HttpResponse(json.dumps({'message': 'No Period in URL Param'}), content_type='application/json')

	period = request.GET['period']
	if period not in ['yesterday', 'this_week', 'last_7_days', 'this_month', 'last_30_days']:
		return HttpResponse(json.dumps({'message': 'Invalid Period'}), content_type='application/json')

	user = auth['user']

	##-# ############################################################################
	##-# Getting Details from Memcache
	##-# ############################################################################
	tag = 'employee-list-with-score'
	cachekey = tag + '-' + str(user.id) + '-' + period
	file_content = proapp.cache.get(cachekey)

	##-# ############################################################################
	##-# Getting Details from Cloud Storage
	##-# ############################################################################
	if file_content is None: ##-# Cache is not Foundable... Trying to Read the File
		file_content = proapp.storage.read_file_with_path(user.id, user.domain_id, period, tag)

	##-# ############################################################################
	##-# Getting Details by ON THE FLY CALCULATIONS
	##-# ############################################################################
	if file_content is None: ##-# File is not Foundable... Trying to Calculate on the Fly

		class Request:
			def __init__(self, user_id, date, period):
				self.GET = {
				'employee': user_id,
				'date': date,
				'period': period
				}

		if 'date' not in request.GET:
			return HttpResponse(json.dumps({'message': 'Date URL Param is Neede. File and Cache is nor Foundable'}), content_type='application/json')

		date = request.GET['date']
		fake_request = Request(user.id, date, period)
		file_content = proapp.cron.dashboard_details_cron(fake_request, {'require': tag})
	else:
		proapp.cron.add_to_cache(user.id, period, tag, file_content)

	if type(file_content) is dict:
		file_content = json.dumps(file_content)
	return HttpResponse(file_content, content_type = 'application/json')



def ajax_dashboard_correlation_list(request):

	auth = _authorize(request)
	if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	if 'period' not in request.GET:
		return HttpResponse(json.dumps({'message': 'No Period in URL Param'}), content_type='application/json')

	period = request.GET['period']
	if period not in ['yesterday', 'this_week', 'last_7_days', 'this_month', 'last_30_days']:
		return HttpResponse(json.dumps({'message': 'Invalid Period'}), content_type='application/json')

	user = auth['user']

	##-# ############################################################################
	##-# Getting Details from Memcache
	##-# ############################################################################
	tag = 'correlation-list-with-significance'
	cachekey = tag + '-' + str(user.id) + '-' + period
	file_content = proapp.cache.get(cachekey)

	##-# ############################################################################
	##-# Getting Details from Cloud Storage
	##-# ############################################################################
	if file_content is None: ##-# Cache is not Foundable... Trying to Read the File
		file_content = proapp.storage.read_file_with_path(user.id, user.domain_id, period, tag)

	##-# ############################################################################
	##-# Getting Details by ON THE FLY CALCULATIONS
	##-# ############################################################################
	if file_content is None: ##-# File is not Foundable... Trying to Calculate on the Fly

		class Request:
			def __init__(self, user_id, date, period):
				self.GET = {
				'employee': user_id,
				'date': date,
				'period': period
				}

		if 'date' not in request.GET:
			return HttpResponse(json.dumps({'message': 'Date URL Param is Neede. File and Cache is nor Foundable'}), content_type='application/json')

		date = request.GET['date']
		fake_request = Request(user.id, date, period)
		file_content = proapp.cron.dashboard_details_cron(fake_request, {'require': tag})
	else:
		proapp.cron.add_to_cache(user.id, period, tag, file_content)

	if type(file_content) is dict:
		file_content = json.dumps(file_content)
	return HttpResponse(file_content, content_type = 'application/json')


def schedule_gateway(request):

	try:
		domain = request.GET['domain']
	except:
		return HttpResponse(json.dumps({'message': 'Need URL Param: Domain'}), content_type = 'application/json')

	try:
		date = request.GET['date']
	except:
		return HttpResponse(json.dumps({'message': 'Need URL Param: Date'}), content_type = 'application/json')

	utcnow = datetime.datetime.utcnow()
	utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
	
	urlpart = '/schedule-cron/?domain=' + domain + '&date=' + date
	name = 'schedule-gateway' + '-' + utcnow + '-' + domain + '-on-' + date

	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		taskqueue.add(url = urlpart,
								method = 'GET', queue_name = 'schedule', 
								name = name,
								countdown = 0)
		return HttpResponse(json.dumps({'message': 'APPEngine: Taskqueue Added Successfully'}), content_type = 'application/json')

	return HttpResponse(json.dumps({'message': 'Localhost: Taskqueue Did Not Added'}), content_type = 'application/json')


# ========================================================================
#		EOF
# ========================================================================
