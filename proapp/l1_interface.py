import base64
import hashlib
import json
import uuid
# import bcrypt

import requests
from django.conf import settings



def call(method, path = None, parameters = None, payload = None):
    """
    Generic API Sending
        :param method: 
        :param url: 
        :param token: 
        :param payload=None: 
        :param parameters=None: 
    """
    token = settings.SECRET_KEY
    request_id = str(uuid.uuid4())
    # token = json.dumps({'requestId' : request_id, 'secretKey': settings.SECRET_KEY}).replace(' ','')
    token = '{0},{1}'.format(request_id, settings.SECRET_KEY)
    hashed_token = hashlib.sha256(token).hexdigest()
    headers = { 'Token' : hashed_token,
                'Accept' : 'application/json' }
    instrumentation = { 'client-request-id' : request_id,
                        'return-client-request-id' : 'true' }
    headers.update(instrumentation)
    url = '{0}{1}'.format(settings.VERSIONS['kube_l1'][settings.VERSIONS['this']], path)
    response = None
    if (method.upper() == 'GET'):
        response = requests.get(url, headers = headers, params = parameters)
    elif (method.upper() == 'DELETE'):
        response = requests.delete(url, headers = headers, params = parameters)
    elif (method.upper() == 'PATCH'):
        headers.update({ 'Content-Type' : 'application/json' })
        response = requests.patch(url, headers = headers, data = json.dumps(payload), params = parameters)
    elif (method.upper() == 'POST'):
        headers.update({ 'Content-Type' : 'application/json' })
        response = requests.post(url, headers = headers, data = json.dumps(payload), params = parameters)
    return response


def test():
    password = 'foobar'
    token = json.dumps({ 'sign-key': settings.SECRET_KEY })
    password_hashed = hashlib.sha256(token).hexdigest()
    print(password_hashed)