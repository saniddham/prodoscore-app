
import base64
import datetime
import json
import logging
import math
import os
import random
import urllib
from collections import OrderedDict
from datetime import time, timedelta
from httplib import HTTPException
from random import randint
from ssl import SSLError

import httplib2
import requests
from django.conf import settings
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import render
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client.client import HttpAccessTokenRefreshError
from oauth2client.service_account import ServiceAccountCredentials
from pytz import timezone
from simple_salesforce import Salesforce
from simple_salesforce.exceptions import (SalesforceMalformedRequest,
                                          SalesforceRefusedRequest)

import proapp.auth
import proapp.cache
import proapp.calc
import proapp.models
import proapp.products.broadsoft
import proapp.products.calendar
import proapp.products.chatter_activity
import proapp.products.chatter_messages
import proapp.products.drive
import proapp.products.gmail
import proapp.products.hangouts
import proapp.products.prosperworks
import proapp.products.ringcentral_calls
import proapp.products.ringcentral_sms
import proapp.products.salesforce
import proapp.products.salesforce_calls
import proapp.products.salesforce_interactions
import proapp.products.salesforce_leads
import proapp.products.sugarCRM
import proapp.products.turbobridge
import proapp.products.zoho
import proapp.settings
import proapp.storage
import proapp.l1_interface

from .models import (Detail, Domain, Employee, Employee_prodoscore, Onboard,
                     Organization_prodoscore, Statistic, Sugar_settings,
                     Transcript_job)

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import memcache, taskqueue, urlfetch
	from google.appengine.api.taskqueue import TaskAlreadyExistsError
	urlfetch.set_default_fetch_deadline(240)
else:
	from django.core.cache import cache


token_url = settings.VERSIONS['token_host'][settings.VERSIONS['this']] + "token/sugar/"


role_dic = {
	-2: 'System',
	-1: 'Terminated',
	0: 'Not activated',
	1: 'Contractor',
	2: 'Sales',
	3: 'Sales Eng',#
	4: 'Operations',
	6: 'Tech Support',

	70: 'Manager',
	80: 'Administrator',

	10: 'Human Resource',
	11: 'Finance & Acc',#
	#12: 'Legal',
	13: 'Marketing',
	14: 'IT',
	15: 'Support',
	#16: 'Outside Sales',
	17: 'Inside Sales',
	18: 'Channel Sales',
	19: 'Sales Support',
	20: 'Business Development',# Rep
	#21: 'Account Manager',
	#22:'Supply Chain Management',
	23:'Procurement',
	24:'Project Management',
	25:'Customer Service'
}

# ========================================================================
# These are the scopes for which we require permission for service account
# ========================================================================
scope = ['https://www.googleapis.com/auth/admin.directory.user.readonly',
		 'https://www.googleapis.com/auth/calendar.readonly',
		 'https://www.googleapis.com/auth/drive.readonly',
		 'https://www.googleapis.com/auth/gmail.readonly']


# ========================================================================
# Load Client Secret file for the service account authorization
# ========================================================================
CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secret.json')
credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRETS, scopes=scope)


# ========================================================================
# Executes an HTTP request, Cache or retrieve from cache
# 	Returns JSON object
#
# @param		req			HTTP Request to be made
# @params		user_id		The user account behalf of whom the request is made
# ========================================================================
def exec_req(req, user_id):

	# =====================================================================
	# See if the request is user specific - in which case we don't want to cache it across users
	# =====================================================================
	cacheKey = user_id+'-'+req.uri

	# =====================================================================
	# Check on the cache
	# =====================================================================
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		data = memcache.get(cacheKey)
	else:
		data = cache.get(cacheKey)
	#
	if data is None:
		try:
			data = req.execute()
		except HttpAccessTokenRefreshError:
			return {}
		except HTTPException:	# Probably a Deadline exceeded exception
			return {}
		#
		try:
			if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
				memcache.add(cacheKey, data, 1200)
			else:
				cache.add(cacheKey, data, 1200)
		except:
			pass

	return data


def to_dict(input_ordered_dict):
	return json.loads(json.dumps(input_ordered_dict))


def is_int(s):
	try:
		int(s)
		return True
	except ValueError:
		return False


# ========================================================================
# Fetch domain users lis from Google Directory API
# ========================================================================
def get_domain_users(domain_name, admin_email):

	# =====================================================================
	# Check on the cache	-	We are anyways storing this in session
	# =====================================================================
#	cacheKey = 'users-in-'+str(domain_name)
#	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
#		data = memcache.get(cacheKey)
#		if data is not None:
#			return data

	# =====================================================================
	# Establish authorization for the domain admin
	# =====================================================================
	delegated_credentials = credentials.create_delegated(admin_email)
	http_auth = delegated_credentials.authorize(Http())

	# =====================================================================
	# Use the HTTP authentication and open Directory API service entry to the domain
	# =====================================================================
	service = build('admin', 'directory_v1', http=http_auth)

	# =====================================================================
	# Get the list of employees from the Google Directory API
	# =====================================================================
	users = []
	nextPageToken = False
	while True:
		user100s = service.users().list(domain=domain_name, fields='users(id,isAdmin,name/fullName,primaryEmail,suspended,thumbnailPhotoUrl),nextPageToken',
			**({'pageToken': nextPageToken} if nextPageToken else {})).execute()
		nextPageToken = user100s.get('nextPageToken', False)
		user100s = user100s.get('users', [])
		for user in user100s:
			users.append(user)
		if not nextPageToken:
			break
	#
	# Store on cache
#	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
#		memcache.add(cacheKey, results, 3600)
	#
	return users


# ========================================================================
# Main cron process - Iterate through all domains and users and create dummy records for each employee, day and product
# ========================================================================
def schedule_cron(request):

	# =====================================================================
	#	Iterate for all domains
	# =====================================================================
	if 'domain' in request.GET:
		domains = Domain.objects.filter(id=request.GET['domain'])
	# 	#
	# else:
	# 	# Calculate the Cron Zone for scheduling
	# 	tz0 = timezone("UTC")
	# 	utc_now = datetime.datetime.now(tz0)
	# 	utc_0 = datetime.datetime(year=utc_now.year, month=utc_now.month, day=utc_now.day, tzinfo=tz0)
	# 	t_delta = utc_now - utc_0 # delta from mid night
	# 	displacement = int(t_delta.seconds/7200) # 2 hour displacement
	# 	cron_zone = (12 - displacement) if (displacement>0) else displacement

	# 	print datetime.datetime.now().strftime("%Y-%m-%d-%-H:%M:%S")+":running schedule cron at utc :"+\
	# 		  utc_now.strftime("%Y-%m-%d-%-H:%M:%S")+" for cron zone :"+str(cron_zone)

	# 	domains = Domain.objects.filter(cronzone=cron_zone)
	# 	#
	# 	# We need to schedule individual task queue tasks for each domain to maintain fault tolerance
	# 	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	# 		utcnow = datetime.datetime.utcnow()
	# 		utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
	# 		#
	# 		urlpart = '/schedule-cron?'
	# 		if 'date' in request.GET:
	# 			urlpart += 'date='+request.GET['date']+'&'
	# 		domainList = []
	# 		for domain in domains:
	# 			onboardRec = Onboard.objects.filter(domain=domain)[0]
	# 			#print (onboardRec)
	# 			if onboardRec.status == 7:
	# 				taskqueue.add(url=urlpart+'domain='+str(domain.id),
	# 							method='GET', queue_name='schedule', 
	# 							name='schedule-cron-'+domain.title.replace('.', '-dot-')+'-on-'+utcnow,
	# 							countdown = domain.id * 50)
	# 				domainList.append(domain.title)
	# 				#
	# 				# Schedule calculating organization baselines for each roles in the domain
	# 				if domain.orgbaseline_enabled == 1:
	# 					roles = Employee.objects.using('replica').filter(domain_id=domain.id, role__gt=0).values('role').distinct()
	# 					for role in roles:
	# 						taskqueue.add(url='/calc-org-baselines?domain='+str(domain.id)+'&role='+str(role['role']),
	# 									method='GET', queue_name='schedule', 
	# 									name='calc-org-baselines-'+domain.title.replace('.', '-dot-')+'-for-role-'+str(role['role'])+'-on-'+utcnow,
	# 									countdown = 480 + (domain.id * 50))
	# 				#
	# 				if domain.phone_system == 'vbc':
	# 					try:
	# 						token_refresh_url = settings.VERSIONS['token_host'][settings.VERSIONS['this']]+'token/vbc/'+str(domain.id)+'/force-refresh'
	# 						taskqueue.add(url=token_refresh_url, method='GET', queue_name='schedule', name='force_refresh1_vbc_token_'+domain.title.replace('.', '-dot-')+'_'+utcnow)
	# 						taskqueue.add(url=token_refresh_url, method='GET', queue_name='schedule', name='force_refresh2_vbc_token_'+domain.title.replace('.', '-dot-')+'_'+utcnow, countdown=12*3600)
	# 					except:
	# 						pass
	# 		#
	# 		return HttpResponse(json.dumps({'message': 'Cron scheduled', 'domains': domainList}), content_type = 'application/json')
	#
	domainList = []
	days = []

	doTaskqueue = True
	if 'taskqueue' in request.GET and request.GET['taskqueue'] == 'false':
		doTaskqueue = False

	for domain in domains:
		#	Skip cron for the example domain
		if domain.id != 6:
			# domainList.append(domain.title)
			# # ==================================================================
			# #	Find the admin for the domain
			# # ==================================================================
			# account_users = False
			# onboardRec = Onboard.objects.filter(domain=domain)[0]
			# adminEmail = onboardRec.email
			# log('domain=' + str(domain.title) + ' admin=' + str(adminEmail), 'info')

			# try:
			# 	account_users = get_domain_users(domain.title, adminEmail)
			# except Exception as e:
			# 	log(e.message, 'error')
			# 	admin = Employee.objects.filter(domain=domain, level=1, status=1, email__icontains='@')
			# 	if len(admin) > 0:
			# 		for adm in admin:
			# 			adminEmail = adm.email
			# 			log('domain=' + str(domain.title) + ' admin=' + str(adminEmail), 'info')
			# 			try:
			# 				account_users = get_domain_users(domain.title, adminEmail)
			# 				break
			# 			except Exception as e: # 403 HttpAccessTokenRefreshError, HttpError, error
			# 				log(e.message, 'error')
			# 				pass
			# 	else:
			# 			log('['+ str(domain.title) +' Schedule]$ No admin defined for domain', 'error')

			# if account_users == False:
			# 	onboardRec = Onboard.objects.filter(domain=domain)[0]
			# 	onboardRec.status = -1
			# 	onboardRec.save()
			# 	account_users = []
			# 	#
			# 	# Get the email HTML rendered
			# 	res = render(request, 'email/domain-uninstall.html', {'domain': domain})
			# 	#
			# 	# Send notification email to support team
			# 	requests.post(
			# 		settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']),
			# 		data={"from": settings.MAILGUN['FROM'],
			# 			"to": ["dev@prodoscore.com"],
			# 			"subject": "Domain "+domain.title+" has uninstalled Prodoscore from GSuite",
			# 			"html": res})

			# else:
			# 	# Synchronize user list with google domain user list
			# 	synchronize_employees(account_users, domain)

				# ===============================================================
				# Determine the time-scope for the cron to be run
				# ===============================================================

			##########################################################################################
				##-# Intendent of Block Bellow Should be Same as This
			if 'fromdate' in request.GET and 'todate' in request.GET:

				# ============================================================
				# Cron is for a range of dates
				# ============================================================
				todate = datetime.datetime.strptime(request.GET['todate'].encode('ascii', 'ignore'), "%Y-%m-%d")
				fromdate = datetime.datetime.strptime(request.GET['fromdate'].encode('ascii', 'ignore'), "%Y-%m-%d")

				# ============================================================
				# Iterate the Cron for each day in the date range
				# ============================================================
				while (todate - fromdate).days > -1:
					schedule_cron_for_domain_users(domain.title, fromdate, doTaskqueue)
					Organization_prodoscore.objects.create(domain_id=domain.id, date=fromdate, score=-1)
					days.append(fromdate.strftime('%Y-%m-%d'))
					fromdate = fromdate + timedelta(days = 1)

			elif 'date' in request.GET:
				# ============================================================
				# Cron is for a specific date - in the past
				# ============================================================
				today = datetime.datetime.strptime(request.GET['date'].encode('ascii', 'ignore'), "%Y-%m-%d")
				schedule_cron_for_domain_users(domain.title, today, doTaskqueue)
				Organization_prodoscore.objects.create(domain_id=domain.id, date=today, score=-1)
				days.append(today.strftime('%Y-%m-%d'))

			else:
				# ============================================================
				# The usual Cron - for the previous day
				# ============================================================
				tz = timezone(domain.timezone)
				today = datetime.datetime.now(tz) - timedelta(days = 1)
				schedule_cron_for_domain_users(domain.title, today, doTaskqueue)
				today = today.strftime("%Y-%m-%d")
				Organization_prodoscore.objects.create(domain_id=domain.id, date=today, score=-1)
				days.append(today)

	# =====================================================================
	# Render a simple HTML to mark that the process was a success
	# =====================================================================
	return HttpResponse(json.dumps({'message': 'Cron scheduled', 'domains': domainList, 'days': days}), content_type = 'application/json')


# Calculate org baselines for a given rolw of a given domain
def calc_org_baselines(request):
	cacheKey = 'org-baseline-'+request.GET['domain']+'-'+request.GET['role']
	baselines = proapp.settings.calc_role_baselines(request.GET['domain'], request.GET['role'])
	data = proapp.cache.delete(cacheKey)
	data = proapp.cache.add(cacheKey, baselines, 88000)
	#
	return HttpResponse(json.dumps({'message': 'Calculated'}), content_type = 'application/json')


# Synchronize employees in a domain with Google domain users
def synchronize_employees(account_users, domain):
	with transaction.atomic():
		for account_user in account_users:
			account_id 		= account_user['id']
			user_id 			= account_user['primaryEmail']
			user_name 		= account_user['name']['fullName']
			admin_level 		= 1 if account_user['isAdmin'] else 0

			employee_check = Employee.objects.filter(profileId=account_id)

			if account_user['suspended'] == True:
				log('['+user_id+' Provisioning]$ Suspended', 'info')

				# ===============================================================
				# Set employee as terminated on database
				# ===============================================================
				if len(employee_check) > 0:
					employee_check[0].role = -1
					employee_check[0].save()
					'''if employee_check[0].role == 0:
						# Delete employees not activated and now suspended
						employee_check[0].domain_id = -1
						employee_check[0].save()
						employee_check[0].delete()
					else:
						employee_check[0].role = -1
						#
						# Disconnect products from terminated users
						employee_check[0].conf_id = None
						employee_check[0].crm_id = None
						employee_check[0].broadsoft_admin_id = None
						employee_check[0].broadsoft_userid = None
						employee_check[0].salesforce_userid = None
						employee_check[0].salesforce_updated = None
						employee_check[0].phone_system_id = None
						employee_check[0].chatter_id = None
						#
						employee_check[0].save()'''

			# ==================================================================
			# Proceed if the account is not suspended from provisioning to Google Apps
			# ==================================================================
			else:

				# ===============================================================
				# Check if employee record exists
				# ===============================================================
				if len(employee_check) == 0:
					# ============================================================
					# Create a new employee record
					# ============================================================
					employee_object = Employee(domain=domain, email=user_id, report_email=user_id, role=0,#last_login=datetime.datetime.now(),
											manager_id=52, profileId=account_id, fullname=user_name, password='[NOPASSWORD]',
											level=admin_level, report_enabled=domain.default_report_flag, is_app_user=0)#, admin=admin_level
					if 'thumbnailPhotoUrl' in account_user and '/public/' in account_user['thumbnailPhotoUrl']:
						employee_object.picture = account_user['thumbnailPhotoUrl']
					employee_object.save()

				else:
					employee_object = employee_check[0]

					# ============================================================
					# Add photo URL for existing users
					# ============================================================
					updated = False
					if 'thumbnailPhotoUrl' in account_user and '/public/' in account_user['thumbnailPhotoUrl']:
						employee_object.picture = account_user['thumbnailPhotoUrl']
						updated = True
					if employee_object.email != user_id:
						employee_object.email = user_id
						updated = True
					if employee_object.fullname != user_name:
						employee_object.fullname = user_name
						updated = True
					if employee_object.level != admin_level:
						employee_object.level = admin_level
						updated = True
					#if employee_object.admin == 0 and admin_level == 1:
					#	employee_object.admin = admin_level
					#	updated = True
					if updated:
						employee_object.salesforce_updated = 0
						employee_object.save()


# ========================================================================
# Process stats for a single domain for a one day	-	This only schedules the actual cron
# 	Returns JSON object
#
# @params		domain_name 	Domain name to be processed
# @params		date			Date for whence we are going to process this
# ========================================================================
def schedule_cron_for_domain_users(domain_name, date, do_task_queue):

	# =====================================================================
	# Read the domain record and get the timezone offset for the given date
	# =====================================================================
	current_domain = Domain.objects.filter(title=domain_name)[0]

	# Get the active user list to schedule crons for (user role > 0 and non app users)
	account_users = Employee.objects.filter(domain=current_domain, role__gt=0, is_app_user=0)

	# =====================================================================
	# Iterate through the employee list received from the API
	# =====================================================================
	with transaction.atomic():
		countdown = 5
		for account_user in account_users:
			do_scheduling_for_employee(current_domain, account_user, date, do_task_queue, countdown)
			countdown = countdown + 1


# =====================================================================
# Method for scheduling tasks gor an individual user depending on the available products to user
# =====================================================================
def do_scheduling_for_employee(domain, employee_object, date, do_task_queue, count_down=10):
	today_date = date.strftime('%Y-%m-%d')
	crmprods = ['zleads', 'zinvoices', 'zaccounts', 'ztasks', 'zevents', 'zcalls']
	crmmodules = ('000000' + bin(domain.crm_modules)[2:])[::-1]

	sfprods = ['sfleads_n', 'salesforce_n', 'ch_messages', 'ch_activts', 'sfcalls', 'sfints']
	sfmodules = ('000000' + bin(domain.crm_modules)[2:])[::-1]

	scprods = ['sugar_calls', 'sugar_meets', 'sugar_opps', 'sugar_leads']
	scmodules = bin(domain.crm_modules)[2:].zfill(4)

	utcnow = datetime.datetime.utcnow()
	utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')

	if employee_object.role > -1:

		# ============================================================
		# Check if stats are scheduled for the given employee for the given day
		# ============================================================
		# stat_check = Statistic.objects.filter(employee=employee_object, date=today_date)
		# if len(stat_check) == 0:
		##-# Abov line is unnecessary conddition anymore
		##-# Reindent the code block bellow
		
		# =========================================================
		# Create dummy records for each of employee-date-product stats
		# =========================================================
		# Statistic_object = Statistic(employee=employee_object, date=today_date, product="gmail", score=-1)
		# Statistic_object.save()
		# Statistic_object = Statistic(employee=employee_object, date=today_date, product="calendar", score=-1)
		# Statistic_object.save()
		# Statistic_object = Statistic(employee=employee_object, date=today_date, product="docs", score=-1)
		# Statistic_object.save()
		# Statistic_object = Statistic(employee=employee_object, date=today_date, product="hangouts", score=-1)
		# Statistic_object.save()

		#
		if not (employee_object.conf_id is None) and employee_object.conf_id != '' and not (employee_object.domain.conf_system is None) and employee_object.domain.conf_system != '':
			Statistic_object = Statistic(employee=employee_object, date=today_date, product=employee_object.domain.conf_system, score=-1)
			Statistic_object.save()
		#
		if not (employee_object.broadsoft_userid is None) and employee_object.broadsoft_userid != '' and hasattr(employee_object, 'broadsoft_admin'): 
			Statistic_object = Statistic(employee=employee_object, date=today_date, product='broadsoft', score=-1)
			Statistic_object.save()
		#
		if not (employee_object.phone_system_id is None) and employee_object.phone_system_id != '':
			if employee_object.domain.phone_system == 'vbc':
				Statistic_object = Statistic(employee=employee_object, date=today_date, product='vbc', score=-1)
				Statistic_object.save()
			else:
				Statistic_object = Statistic(employee=employee_object, date=today_date, product='ringc_calls', score=-1)
				Statistic_object.save()
				#
				Statistic_object = Statistic(employee=employee_object, date=today_date, product='ringc_sms', score=-1)
				Statistic_object.save()
		#
		if not (employee_object.crm_id is None) and employee_object.crm_id != '' and not ( employee_object.domain.crm_system is None) and employee_object.domain.crm_system != '':
			if employee_object.domain.crm_system == 'zoho':
				for i in range(0, 6):
					if crmmodules[i] == '1':
						Statistic_object = Statistic(employee=employee_object, date=today_date, product=crmprods[i], score=-1)
						Statistic_object.save()
			elif employee_object.domain.crm_system == 'salesforce':
				for i in range(0, 6):
					if sfmodules[i] == '1':
						Statistic_object = Statistic(employee=employee_object, date=today_date, product=sfprods[i], score=-1)
						Statistic_object.save()

			elif employee_object.domain.crm_system == 'prosperworks':
				Statistic_object = Statistic(employee=employee_object, date=today_date, product='pwoprtunts', score=-1)
				Statistic_object.save()
				Statistic_object = Statistic(employee=employee_object, date=today_date, product='pwleads', score=-1)
				Statistic_object.save()
			elif employee_object.domain.crm_system == 'sugarCRM':
				for i in range(0, len(scprods)):
					if scmodules[i] == '1':
						Statistic_object = Statistic(employee=employee_object, date=today_date, product=scprods[i], score=-1)
						Statistic_object.save()
			else:
				Statistic_object = Statistic(employee=employee_object, date=today_date, product=employee_object.domain.crm_system, score=-1)
				Statistic_object.save()
		# =========================================================
		# scheduling to get employee daily prodoscore
		# =========================================================
		emp_prod = Employee_prodoscore(employee=employee_object, domain=employee_object.domain, date=today_date, role=employee_object.role, score=-1)
		emp_prod.save()

		# =========================================================
		# Queue a task to process stats for each employee here
		# =========================================================
		if do_task_queue and os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			urlpart = '/process-cron/?date=' + today_date + '&employee=' + str(employee_object.id)
			namepart = employee_object.email.\
							replace(' ','_').\
							replace('@', '-at-').\
							replace('.','-dot-').\
							replace('\'','-').\
							replace('#', '-').replace('"','-').\
							replace('$','-').\
							replace('&','-and-').\
							replace('*','-') + '-on-' + today_date + '-' + utcnow
			#
			try:
				# taskqueue.add(url=urlpart + '&product=gmail', method='GET', queue_name='process-cron', name=namepart + '_for_gmail', countdown=count_down + 2)
				# taskqueue.add(url=urlpart + '&product=calendar', method='GET', queue_name='process-cron', name=namepart + '_for_calendar', countdown=count_down + 2)
				# taskqueue.add(url=urlpart + '&product=docs', method='GET', queue_name='process-cron', name=namepart + '_for_docs', countdown=count_down + 2)
				# taskqueue.add(url=urlpart + '&product=hangouts', method='GET', queue_name='process-cron', name=namepart + '_for_hangouts', countdown=count_down + 2)
				#
				
				if not (employee_object.conf_id is None) and employee_object.conf_id != '' and not (
					employee_object.domain.conf_system is None) and employee_object.domain.conf_system != '':
					taskqueue.add(url=urlpart + '&product=' + employee_object.domain.conf_system, method='GET', queue_name='process-cron', name=namepart + '_for_' + employee_object.domain.conf_system, countdown=count_down + 5)
				#
				if not (
					employee_object.broadsoft_userid is None) and employee_object.broadsoft_userid != '' and hasattr(employee_object, 'broadsoft_admin'):
					taskqueue.add(url=urlpart + '&product=broadsoft', method='GET', queue_name='process-cron', name=namepart + '_for_broadsoft', countdown=count_down + 3)
				#
				if not (employee_object.phone_system_id is None) and employee_object.phone_system_id != '':  # not (employee_object.broadsoft_admin.broadsoft_userid is None) and employee_object.broadsoft_admin.broadsoft_userid != '':
					if employee_object.domain.phone_system == 'vbc':
						taskqueue.add(url=urlpart + '&product=vbc', method='GET', queue_name='process-cron',
										name=namepart + '_for_vbc', countdown=count_down + 3)
					else:
						taskqueue.add(url=urlpart + '&product=ringc_calls', method='GET', queue_name='process-cron',
										name=namepart + '_for_ringc_calls', countdown=count_down + 3)  #
						#
						taskqueue.add(url=urlpart + '&product=ringc_sms', method='GET', queue_name='process-cron',
										name=namepart + '_for_ringc_sms', countdown=count_down + 3)

				# Schedule tasks to fetch CRM system data
				if not (employee_object.crm_id is None) and employee_object.crm_id != '' and not (
					employee_object.domain.crm_system is None) and employee_object.domain.crm_system != '':
					if employee_object.domain.crm_system == 'zoho':
						for j in range(0, 6):
							if crmmodules[j] == '1':
								taskqueue.add(url=urlpart + '&product=' + crmprods[j], method='GET',
												queue_name='process-cron', name=namepart + '_for_' + crmprods[j],
												countdown=count_down + 2)
					elif employee_object.domain.crm_system == 'prosperworks':
						urlpart = '/process-cron-prosperworks/?date=' + today_date + '&employee=' + str(
							employee_object.id)
						taskqueue.add(url=urlpart, method='GET', queue_name='process-cron',
										name=namepart + '_for_prosperworks', countdown=count_down + 2)
					elif employee_object.domain.crm_system == 'sugarCRM':
						for j in range(0, len(scprods)):
							if scmodules[j] == '1':
								taskqueue.add(url=urlpart + '&product=' + scprods[j], method='GET', queue_name='sugarcrm', name=namepart + '_for_' + scprods[j], countdown=count_down + 2)
					elif employee_object.domain.crm_system == 'salesforce':
						for i in range(0, 6):
							if sfmodules[i] == '1':
								taskqueue.add(url=urlpart + '&product=' + sfprods[i], method='GET', queue_name='process-cron', name=namepart + '_for_' + sfprods[i], countdown=count_down + 2)
				#
				# Schedule tasks to fetch Android mobile activity
				urlpart = '/process-cron-android/?date=' + today_date + '&employee=' + str(employee_object.id)
				taskqueue.add(url=urlpart + '&product=call', method='GET', queue_name='process-cron',
								name=namepart + '_for_call', countdown=count_down + 5)
				taskqueue.add(url=urlpart + '&product=sms', method='GET', queue_name='process-cron',
								name=namepart + '_for_sms', countdown=count_down + 6)
			#
			except TaskAlreadyExistsError as e:
				print('TaskAlreadyExistsError')
				print(namepart)
				print(e)


# ========================================================================
# Schedule process crons for left behind docs
# ========================================================================
def schedule_docs_processing(request):
	logging.info('Scheduling docs processing')
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		import time
		try:
			pendings = Statistic.objects.filter(score=-1, product='docs')[:50]
		except IndexError:
			pendings = Statistic.objects.filter(score=-1, product='docs')

		url = '/process-cron/?product=docs'
		name_part = 'process-cron-docs-{}'
		counter = 0
		for item in pendings:
			task_name = name_part.format(int(time.time()*100))
			taskqueue.add(url=url, method='GET', queue_name='process-docs',
						  name=task_name, countdown=counter + 5)
			counter += 1
		logging.info('Scheduled {} task queues for docs processing'.format(counter))
		return HttpResponse(
			json.dumps({'status': 'success', 'progress': counter}), content_type='application/json')
	else:
		return HttpResponse(
			json.dumps({'status': 'failed', 'cause': 'Environment is not App Engine', 'completed': 'false'}),
			content_type='application/json')


# ========================================================================
# Schedule process cron tasks for left behind unprocessed statistics except for docs
# ========================================================================
def schedule_tasks_for_unprocessed_stats(request):
	logging.info('Scheduling tasks for left behind tasks')
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		import time
		try:
			pendings = Statistic.objects.filter(score=-1).exclude(product='docs').order_by('-id')[:60]
		except IndexError:
			pendings = Statistic.objects.filter(score=-1).exclude(product='docs').order_by('-id')

		url = '/process-cron/?product={}'
		name_part = 'process-cron-left-behind-{}-{}'
		counter = 0
		results = dict()

		for item in pendings:
			uri = url.format(item.product)
			task_name = name_part.format(item.product, int(time.time() * 1000))
			taskqueue.add(url=uri, method='GET', queue_name='process-left-behinds',
						  name=task_name, countdown=counter + 5)
			if item.product in results:
				results[item.product] += 1
			else:
				results[item.product] = 1
			counter += 1

		logging.info('Scheduled {} task queues for left behind tasks processing'.format(counter))
		return HttpResponse(
			json.dumps({'status': 'success', 'total': counter, 'results':results}), content_type='application/json')
	else:
		return HttpResponse(
			json.dumps({'status': 'failed', 'cause': 'Environment is not App Engine', 'completed': 'false'}),
			content_type='application/json')


# ========================================================================
# Get statistics of a cron process - to be used in progress bar
# ========================================================================
def cron_stats(request):
	# =====================================================================
	if 'fromdate' in request.GET:
		fromdate = datetime.datetime.strptime(request.GET['fromdate'].encode('ascii', 'ignore'), "%Y-%m-%d")
	else:
		fromdate = datetime.datetime.now() - timedelta(days = 93)
	# =====================================================================
	if 'todate' in request.GET:
		todate = datetime.datetime.strptime(request.GET['todate'].encode('ascii', 'ignore'), "%Y-%m-%d")
	else:
		todate = datetime.datetime.now()
	# =====================================================================
	if 'domain' in request.GET:
		if is_int(request.GET['domain']):
			domains = Domain.objects.filter(id=request.GET['domain'])
		else:
			domains = Domain.objects.filter(title=request.GET['domain'])
	elif 'domain' in request.session.keys():
		domains = Domain.objects.filter(title=request.session['domain'])
	else:
		return HttpResponse(json.dumps({'error': 'domain un-specified'}), content_type = 'application/json')
	#
	if len(domains) == 0:
		return HttpResponse(json.dumps({'error': 'domain not found'}), content_type = 'application/json')
	# =====================================================================
	total = len(Statistic.objects.filter(date__range=(fromdate, todate)).extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE domain_id = '+str(domains[0].id)+')']))
	done = len(Statistic.objects.filter(date__range=(fromdate, todate)).extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE domain_id = '+str(domains[0].id)+')']).exclude(score__in=[-1, -3]))
	onboard = Onboard.objects.filter(domain=domains[0])
	progress = 0
	if total > 0:
		progress = 100 * done / total
	return HttpResponse(json.dumps({'stage': onboard[0].status, 'total': total, 'completed': done, 'progress': progress}), content_type = 'application/json')


# ========================================================================
# Schedule tasks to collect data
# ========================================================================
def schedule333(domain_id):
	utcnow = datetime.datetime.utcnow()
	utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
	#
	# -------------------------------------------------------------------------------------------------------------------------------------------------------
	# Get 300 unprocessed stat records for the domain at a time
	stats = (Statistic.objects.filter(score=-1)
		.extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE domain_id = '+str(domain_id)+')'])
		.order_by('-date'))
	#
	stats = stats[:180]	# This looks so inefficient. But do we have a b-etter way with Django ORM.?
	i = 0
	for statistic in stats:
		# Flag the record so it is not picked up on next round
		statistic.score = -3
		statistic.save()
		#
		urlpart = '/process-cron/?date='+statistic.date+'&employee='+str(statistic.employee.id)+'&product='+statistic.product
		namepart = statistic.employee.email.replace('@', '-at-').replace('.', '-dot-')+'-on-'+statistic.date+'-'+utcnow+'-for-'+statistic.product
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			taskqueue.add(url=urlpart, method='GET', name=namepart, queue_name='onboarding',  countdown=i)
		else:
			print(urlpart)
		i += 1

	return len(stats)

# ========================================================================
# Do the work for onboarding new organizations
# ========================================================================
def add_task_queue(request):
	product = request.GET['product'].encode('ascii', 'ignore')
	date = request.GET['date'].encode('ascii', 'ignore')
	employee_id = request.GET['employee'].encode('ascii', 'ignore')
	utcnow = datetime.datetime.utcnow()
	utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
	urlpart = '/process-cron/?date=' + date + '&employee=' + str(employee_id)
	namepart = str(employee_id) + '-on-' + date + '-' + utcnow + '_for_'+product
	countdown = randint(0, 9)
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		taskqueue.add(url=urlpart + '&product='+product, method='GET', queue_name='process-cron',name=namepart, countdown=countdown + 2)
	return HttpResponse(json.dumps({'message': 'Cron scheduled', 'employee': employee_id, 'days': date}), content_type = 'application/json')

# ========================================================================
# Do the work for onboarding new organizations
# ========================================================================
def onboarding_tweak(request):
	return HttpResponse(json.dumps({'scheduled': schedule333(request.GET['domain'])}), content_type = 'application/json')

# ========================================================================
# Do the work for onboarding new organizations
# ========================================================================
def onboarding(request):
	# Check if we have new organizations onboard
	# onboards = Onboard.objects.filter(status=5)
	# #
	# # Let's take one at a time and process next in queue on next round
	# if len(onboards) > 0:
	# 	onboard = onboards[0]
	# 	if schedule333(onboard.domain.id) == 0:
	# 		# Ok, we are done with scheduling tasks for this domain
	# 		onboard.status = 6
	# 		# This domain/organization will be handled in the next block
	# 		onboard.save()
	#
	# -------------------------------------------------------------------------------------------------------------------------------------------------------
	# Check if we have stat-collect-all-scheduled organizations onboard
	onboards = Onboard.objects.filter(status=6)
	for onboard in onboards:
		#
		# Check how many tasks remaining
		stats = Statistic.objects.filter(score=-3).extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE domain_id = '+str(onboard.domain.id)+')'])
		if len(stats) == 0:
			# Yay.! We have completed the processing for this domain. Let's send an email to the admin to call them over here..
			#
			# Populate data to MemCached
			today = datetime.datetime.now()
			timezone_offset = proapp.calc.get_timezone_offset(today, onboard.domain.timezone)
			yesterday = (today - timedelta(days=1) + timedelta(hours=timezone_offset))
			days7ago = (yesterday - timedelta(days=35))
			tobediscarded = proapp.calc.scores(onboard.domain.id, {},
									'domain_id = '+str(onboard.domain.id),
									days7ago.strftime('%Y-%m-%d'), yesterday.strftime('%Y-%m-%d'),
									{'cache': True, 'prods': True})
			#
			# Send Emails to Admins
			admins = Employee.objects.filter(domain=onboard.domain, role=80)
			for admin in admins:
				try:
					res = render(request, 'email/welcome-aboard-2.html', {'fullname': admin.fullname})
					requests.post(
						settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']),
						data={"from": settings.MAILGUN['FROM'],
							"to": [admin.email],
							"bcc": ["support@prodoscore.com"],
							"subject": "Prodoscore is ready",
							"html": res})
				except Exception as e:
					logging.error("Error sending email" + str(e.message))
			onboard.status = 7
			onboard.save()
	#
	return HttpResponse(json.dumps({'message': 'scheduled'}), content_type = 'application/json')


# ========================================================================
# Schedule cron for past 3 months for a given domain
# ========================================================================
def schedule_onboard_task(domain_id):
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		try:
			# This may throw TombstonedTaskError when imbeciles would go back and next several times from last step. We will be ignoring that exception.
			taskqueue.add(url='/register/schedule-onboard-tasks-for-past-90-days/?domain='+str(domain_id),
				method='GET', queue_name='onboarding',
				name='schedule-90day-analysis-n'+str(domain_id), countdown=2)
		except Exception as e:
			logging.error('Caught exception : {}'.format(e.message))
	else:
		todate = datetime.datetime.now()
		fromdate = todate - timedelta(days=93)
		todate = todate.strftime('%Y-%m-%d')
		fromdate = fromdate.strftime('%Y-%m-%d')
		res = proapp.l1_interface.call(method = 'GET', path = '/schedule-cron?domain=' + str(domain_id) +  '&fromdate=' + str(fromdate) + '&todate=' + str(todate) + '&onboard=true')
		print(res)
		print('taskqueue.add(url=\'/register/schedule-onboard-tasks-for-past-90-days/?domain='+str(domain_id)+'\')')
	return True


def schedule_onboard_tasks(request):
	todate = datetime.datetime.now()
	fromdate = todate - timedelta(days=93)
	todate = todate.strftime('%Y-%m-%d')
	fromdate = fromdate.strftime('%Y-%m-%d')
	domain = request.GET['domain']
	try:
		url = str(settings.VERSIONS['kube_l1'][settings.VERSIONS['this']]) + '/schedule-cron?domain=' + str(domain) +  '&fromdate=' + str(fromdate) + '&todate=' + str(todate) + '&onboard=true'
		res = requests.get(url)
	except Exception as e:
		logging.error(e)	


	# req = GetReq({'fromdate': from_date.strftime('%Y-%m-%d'), 'todate': todate.strftime('%Y-%m-%d'), 'domain': request.GET['domain'], 'taskqueue': 'false'})
	# res = schedule_cron(req, True)

	return HttpResponse(json.dumps({'message': 'success'}), content_type = 'application/json')


# ============================================================================
# Task Queue failure recovery mechanism. set the score = -1 for static
# records which have a score of -2 and have not yet being processed again for
# more than 12 mins.
# ============================================================================
def failure_retry(request):
	# calculate the time to be used in the query to filter out the records
	filter_time = datetime.datetime.now() - timedelta(minutes=12)
	exclude_time = datetime.datetime.now() - timedelta(days=3)
	# filter the set of statistic records with a score less than -2 and last_updated time<filter_time
	try:
		stat_rec = Statistic.objects.filter(score=-2, last_updated__lte=filter_time).exclude(date__lte=exclude_time)[:200]
	except IndexError as e:
		stat_rec = []
		logging.error("Index Error" + str(e.message))
	count_for_one = 0
	count_for_five = 0
	if len(stat_rec) != 0:
		logging.info("number of stat records taken to process: " + str(len(stat_rec)))
		for stat_record in stat_rec:
			# if the record can be processed again
			if stat_record.retry_count < 3:
				count_for_one += 1
				# for each statistic record reset the score to -1
				stat_record.score = -1
			else:
				count_for_five += 1
				# set the score of the record to -5 to indicate that the record can no longer be processed
				# (record processing has failed 5 times)
				stat_record.score = -5
			stat_record.save()
	logging.info("number of records set as one : " + str(count_for_one))
	logging.info("number of records set as five : " + str(count_for_five))
	return HttpResponse(json.dumps({'message': 'success'}), content_type='application/json')


# ========================================================================
# Process scheduled cron for each employee for each day in the lowest granular level
# 	This is run in Task Queues parallely
# ========================================================================
def process_cron(request):

	# =====================================================================
	# See if it is requested to be processed for a specific date or specific employee-date or product specific
	# 	These are entirely for testing purpose, and on production it will be down to [employee, day, product]
	# =====================================================================
	try:
		if 'date' in request.GET and 'employee' in request.GET:
			stat_rec = Statistic.objects.filter(date=request.GET['date'], employee_id=request.GET['employee'], product=request.GET['product'])[:1]
			##-# #######################################
			print(request.GET['date'])
			print(request.GET['employee'])
			print(request.GET['product'])
			##-# ######################################
		elif 'product' in request.GET:
			stat_rec = Statistic.objects.filter(score=-1, product=request.GET['product']).order_by('-id')[:1]
		else:
			stat_rec = Statistic.objects.filter(score=-1).exclude(product='docs').order_by('-id')[:1] # v.2.11.5 : limiting to a single entry at the query time to avoid memory outage
	except IndexError:
		stat_rec=[]

	# =====================================================================
	# See if there is pending stat fetch to be run
	# =====================================================================
	if len(stat_rec) != 0:
		stat_rec = stat_rec[0]
		date_str = stat_rec.date

		# ==================================================================
		# Lock the records for this transaction, so another parallel instance would not attach to this set of records
		# ==================================================================
		if stat_rec.score > -2:		#locking:
			stat_rec.score = -2
			stat_rec.retry_count = stat_rec.retry_count + 1
			stat_rec.save()

		employee = stat_rec.employee
		date = datetime.datetime.strptime(date_str, '%Y-%m-%d')
		timezone_offset = proapp.calc.get_timezone_offset(date, employee.domain.timezone)
		#
		working_hours = [map(int, employee.domain.daystart.split(':')), map(int, employee.domain.dayend.split(':'))]
		log('['+employee.email+' Cron]$ Processing Cron for: '+date_str, 'info')

		# ==================================================================
		# Establish authorization for the employee
		# ==================================================================
		if stat_rec.product in ['gmail', 'calendar', 'docs', 'hangouts']:
			delegated_credentials = credentials.create_delegated(employee.email)
			http_auth = delegated_credentials.authorize(Http())

		# ==================================================================
		# Fetch statistics and fill the database records - one employee-date at a time
		# ==================================================================
		score = {}
		with transaction.atomic():

			# ===============================================================
			# Iterate through products for a user for a given day
			# ===============================================================
			#for stat_rec in stat_check:

			# ============================================================
			# Get Gmail statistics/details and fill to the dastabase
			# ============================================================
			if stat_rec.product == 'gmail':
				score = proapp.products.gmail.get_stats(http_auth, employee.email, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get Calendar statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'calendar':
				score = proapp.products.calendar.get_stats(http_auth, timezone_offset, employee.email, date, working_hours)
				stat_rec.score = score['score']
				stat_rec.save()
				for id in score['details']:
					# Check if there is also a zoho event for the same time
					chk_duplicate_on_zevts = Statistic.objects.filter(date=stat_rec.date, employee_id=stat_rec.employee_id, product='zevents')
					if len(chk_duplicate_on_zevts) > 0:
						chk_duplicate_on_zevts = Detail.objects.filter(statistic=chk_duplicate_on_zevts[0], start_time = score['details'][id][3], end_time = score['details'][id][4])
						# Mark as a flagged event is there exists a matching Zoho event and reduce the score
						if len(chk_duplicate_on_zevts) > 0:
							score['details'][id][2] = 1
							stat_rec.score -= (score['details'][id][4] - score['details'][id][3])
							stat_rec.save()

			# # ============================================================
			# Get Docs statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'docs':
				score = proapp.products.drive.get_stats(http_auth, employee, date_str, timezone_offset, working_hours)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get Hangouts statistics/details and fill to the database
			# ============================================================
			elif stat_rec.product == 'hangouts':
				score = proapp.products.hangouts.get_stats(http_auth, employee.email, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get Chatter Message statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'ch_messages':
				score = proapp.products.chatter_messages.get_stats(employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get Chatter Activity statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'ch_activts':
				score = proapp.products.chatter_activity.get_stats(employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get TurboBridge statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'turbobridge':
				score = proapp.products.turbobridge.get_stats(employee, date_str, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get Ringcentral statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'ringc_calls':
				score = proapp.products.ringcentral_calls.get_stats(employee, date_str, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get Ringcentral statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'ringc_sms':
				score = proapp.products.ringcentral_sms.get_stats(employee, date_str, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get Vonage Business Cloud statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'vbc':
				score = proapp.products.vbc.get_stats(employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get ZOHO statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'zcalls':
				score = proapp.products.zoho.get_calls(employee, date_str, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			elif stat_rec.product == 'zevents':
				score = proapp.products.zoho.get_events(employee, date_str, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()
				for id in score['details']:
					# Check if there is also a G-calendar event for the same time
					chk_duplicate_on_gcal = Statistic.objects.filter(date=stat_rec.date, employee_id=stat_rec.employee_id, product='calendar')
					if len(chk_duplicate_on_gcal) > 0:
						chk_duplicate_on_gcal = Detail.objects.filter(statistic=chk_duplicate_on_gcal[0], start_time = score['details'][id][3], end_time = score['details'][id][4])
						# Mark as flagged and reduce the score if there exists a matching calendar event
						if len(chk_duplicate_on_gcal) > 0:
							score['details'][id][2] = 1
							stat_rec.score -= (score['details'][id][4] - score['details'][id][3])
							stat_rec.save()

			# ============================================================
			elif stat_rec.product == 'ztasks':
				score = proapp.products.zoho.get_tasks(employee, date_str, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			elif stat_rec.product == 'zleads':
				score = proapp.products.zoho.get_leads(employee, date_str, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			elif stat_rec.product == 'zinvoices':
				score = proapp.products.zoho.get_invoices(employee, date_str, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			elif stat_rec.product == 'zaccounts':
				score = proapp.products.zoho.get_accounts(employee, date_str, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get salesforce statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'salesforce_n':
				score = proapp.products.salesforce.get_stats(employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get salesforce Leads statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'sfleads_n':
				score = proapp.products.salesforce_leads.get_stats(employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get salesforce Calls statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'sfcalls':
				score = proapp.products.salesforce_calls.get_stats(employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ============================================================
			# Get salesforce Interactions statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'sfints':
				score = proapp.products.salesforce_interactions.get_stats(employee, date, working_hours,timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# =================================================================
			# Get SugarCRM Leads statistics/details and fill to the database
			# =================================================================
			elif stat_rec.product == 'sugar_leads':
				score = proapp.products.sugarCRM.get_leads(employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ========================================================================
			# Get SugarCRM Opportunities statistics/details and fill to the database
			# ========================================================================
			elif stat_rec.product == 'sugar_opps':
				score = proapp.products.sugarCRM.get_opportunities(employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ========================================================================
			# Get SugarCRM Calls statistics/details and fill to the database
			# ========================================================================
			elif stat_rec.product == 'sugar_calls':
				score = proapp.products.sugarCRM.get_calls(employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			# ========================================================================
			# Get SugarCRM Meetings statistics/details and fill to the database
			# ========================================================================
			elif stat_rec.product == 'sugar_meets':
				score = proapp.products.sugarCRM.get_calender(employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			#elif stat_rec.product == 'sugar_tasks':
				#score = proapp.products.sugarCRM.get_tasks(employee, date, working_hours, timezone_offset)
				#print ("processing sugar_tasks")
				#print score
				#stat_rec.score = score['score']
				#stat_rec.save()

			# ============================================================
			# Get BroadSoft statistics/details and fill to the dastabase
			# ============================================================
			elif stat_rec.product == 'broadsoft':
				score = proapp.products.broadsoft.get_stats(request, employee, date, working_hours, timezone_offset)
				stat_rec.score = score['score']
				stat_rec.save()

			_add_detail_records(employee, stat_rec, score)
		check_inputs = Statistic.objects.filter(employee=employee, date=date_str, score__lt=0)
		if len(check_inputs) == 0:
			date_id = (int(date.strftime('%s')) / 86400) - 16800
			emps_day_score = proapp.calc.emps_day_score([employee.id], date_id, False)
			if 'scr' in emps_day_score[employee.id]:
				emp_prods = Employee_prodoscore.objects.filter(employee_id=employee.id, date=date_str)[0]
				emp_prods.score = emps_day_score[employee.id]['scr']['l']
				emp_prods.save()
		# ==================================================================
		# Display status on console and render template for client with info on processed data
		# ==================================================================
		log('['+employee.email+' Results]$ '+date_str+' : '+employee.fullname+' : '+stat_rec.product+' = '+str(score['score']), 'success')
		score['details'] = '[archived]';
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			log({'date': date_str, 'fullname': employee.fullname, 'email': employee.email, 'product': stat_rec.product, 'results': score}, 'success')
			return HttpResponse(json.dumps({'status': 'done'}), content_type = 'application/json')
		else:
			return HttpResponse(json.dumps({'date': date_str, 'fullname': employee.fullname, 'email': employee.email, 'product': stat_rec.product, 'results': score}), content_type = 'application/json')

	# =====================================================================
	# No more scheduled Cron tasks for the day
	# TO-DO: Prevent calling this function till the next midnight
	# =====================================================================
	return HttpResponse(json.dumps({'message': 'empty queue'}), content_type = 'application/json')


# ========================================================================
# salesforce cron for each employee
# ========================================================================
def salesforce_cron(request):
	from simple_salesforce import Salesforce
	from simple_salesforce.exceptions import SalesforceMalformedRequest, SalesforceRefusedRequest
	from .models import Domain, Onboard, Employee
	#
	#Obataining Salesforce prodoscore account credentials
	domain = Domain.objects.filter(id=9)[0]
	url = 'https://login.salesforce.com/services/oauth2/token'
	token_id = domain.crm_token
	payload = {'grant_type': 'password',
		   'client_id':'3MVG9szVa2RxsqBZcaYqIR5nZljlWeftvhfUfdS.9S9xsc81cBSxDTR2WfNQMKzN3bhZoCT.ZzWY1D61FihRT',
		   'client_secret':'7790752435074856812',
		   'username':domain.salesforce_userid,
		   'password':domain.salesforce_password+'{}'.format(token_id)
	}
	try:
		headers = {'Accept-Charset': 'UTF-8'}
		r = requests.post(url, data=payload, headers=headers)
		response_data = r.json()
		sf = Salesforce(instance_url=response_data['instance_url'], session_id=response_data['access_token'])
		#
		# Fetch and iterate all employee records in database
		employee_array = Employee.objects.filter(salesforce_updated=0, password='[NOPASSWORD]')
		if len(employee_array) > 300:
			employee_array = employee_array[:300]
		for employee in employee_array:
			try:
				#
				user_onboardId = Onboard.objects.filter(domain_id=employee.domain_id)[0]
				report_enabled = str(employee.report_enabled)
				if ( len(report_enabled) < 2 and report_enabled == '1' ) or ( len(report_enabled) > 2 and report_enabled[3] == '1' ) or ( len(report_enabled) > 2 and report_enabled[2] == '1' ):
					email_check = True
				else:
					email_check = False
				#
				# Get organization record from database
				# Domain not found in salesforce
				if employee.domain.salesforce_id == '' or employee.domain.salesforce_id is None:
					resp = proapp.auth.add_to_salesforce(user_onboardId)
					if resp:
						salesforce_employee(sf, employee, email_check)

				else:
					salesforce_employee(sf, employee, email_check)

			except UnicodeEncodeError:
				print(employee.email+'UnicodeEncodeError')
			except SalesforceMalformedRequest:
				print(employee.email+"SalesforceMalformedRequest")
	except KeyError:
		print(domain.title+"outerloop Keyerror")
	except UnicodeEncodeError:
		print(domain.title+'UnicodeEncodeError')
	except SalesforceMalformedRequest:
		print(domain.title+"SalesforceMalformedRequest")
	except SalesforceRefusedRequest:
		print(domain.title+"SalesforceRefusedRequest")

	return HttpResponse(json.dumps({'message': 'salesforce cron'}), content_type='application/json')


# ==========================================================
# Add or Update employees into Salesforce
# Update the flag 'salesforce_updated'
# ==========================================================
def salesforce_employee(sf,employee,email_check):
	from simple_salesforce.exceptions import SalesforceMalformedRequest

	# Check if the employee already in SalesForce
	records = sf.query("SELECT Id FROM Contact WHERE Email = '" + employee.email + "'")
	records = records['records']
	resp = 0
	if len(records) > 0:
		contact = to_dict(records[0]["Id"])
		resp = sf.Contact.update('{}'.format(contact),
								 {'LastName': employee.fullname, 'User_ID__c': employee.id,
								  'Email': employee.email, 'Email_Opt_In__c': email_check,
								  'Role__c': role_dic[employee.role]})
		print ("Up " + employee.email)
	else:
		# check if the lead is already in salesforce
		records = sf.query("SELECT Id, Name FROM Lead WHERE Email = '" + employee.email + "' ")
		records = records['records']
		try:

			employee_name = employee.fullname if len(records) == 0 else '[dup] ' + employee.fullname ##No LEAD##
			resp = sf.Contact.create({'AccountId': employee.domain.salesforce_id,
										  'LastName': employee_name,
										  'User_ID__c': employee.id, 'Email': employee.email,
										  'Email_Opt_In__c': email_check,
										  'Role__c': role_dic[employee.role]})
			
		except SalesforceMalformedRequest:
			print(employee.email + "SalesforceMalformedRequest")
		except IndexError:
			print(employee.email + 'this domain is not in salesforce')
		except KeyError:
			print(employee.email + "innserloop keyerror")
	try:
		from collections import OrderedDict
		if (type(resp) == type(1) and resp in [204, 201, 200]) or (
			type(resp) is type(OrderedDict() and 'success' in resp and resp['success'] == True)):
			employee.salesforce_updated = 1
			employee.save()
	except KeyError:
		print(employee.email + "Employee save ERROR")
	return


# ========================================================================
# ANDROID - Int6egrate data from Android API Proxy
# ========================================================================
def process_cron_android(request):
	employee = Employee.objects.filter(id=request.GET['employee'])
	if len(employee) > 0:
		employee = employee[0]
		date = datetime.datetime.strptime(request.GET['date'], '%Y-%m-%d')
		timezone_offset = proapp.calc.get_timezone_offset(date, employee.domain.timezone)
		calltypes = ['', 'Incoming', 'Outgoing', 'Missed','']
		res = requests.get('http://35.185.205.108/'+request.GET['product']+'s/'+request.GET['employee']+'/'+request.GET['date']+'/'+str(timezone_offset*60))
		#
		res = json.loads(res.content)
		score = 0.0
		detailRecs = {}

		if len(res) > 0:
			for rec in res:
				# correct date for each record [records contain the UTC time]
				c_date = datetime.datetime.strptime(rec['date'][:19],'%Y-%m-%dT%H:%M:%S')
				correct_date = c_date  + datetime.timedelta(hours=timezone_offset)
				comm_date = (c_date - date + datetime.timedelta(hours=timezone_offset)).seconds / 60

				if rec['number'] is None:  #drafts are also sent along the response
					continue

				# record's date is the standard UTC date
				if correct_date.date() == date.date() :
					if request.GET['product'] == 'sms':
						detailRec = [ rec['number']+': '+calltypes[ rec['type'] ], rec, 1, comm_date, comm_date]
						if rec['type'] == 2 or rec['type'] == 4:
							score += 1
					#
					elif request.GET['product'] == 'call':
						if rec['type'] == 3:
							detailRec = [ rec['number']+': '+calltypes[ rec['type'] ], rec, 1, comm_date, comm_date]
						else:
							detailRec = [ rec['number']+': '+calltypes[ rec['type'] ], rec, 1, comm_date - rec['duration']/60, comm_date]
							if rec['type'] == 2:
								score += rec['duration']
					#
					if rec['type'] == 2 or rec['type'] == 4:
						detailRec[2] = 0
					#
					detailRecs[ str(employee.id)+'-'+str(rec['log_id']) ] = detailRec

			#convert the total CALL time [seconds] into minutes
			score = score/60 if request.GET['product'] == 'call' else score
			stat_rec = Statistic.objects.filter(date=request.GET['date'], employee_id=request.GET['employee'], product=request.GET['product'])

			if len(stat_rec) == 0:
				stat_rec = Statistic(employee=employee, date=request.GET['date'], product = request.GET['product'], score = score)
			else:
				stat_rec = stat_rec[0]
				stat_rec.score = score
			stat_rec.save()
			print(detailRecs)
			_add_detail_records(employee, stat_rec, {'details': detailRecs})

		return HttpResponse(json.dumps({'raw': res, 'date': request.GET['date'], 'fullname': employee.fullname, 'email': employee.email, 'product': request.GET['product'], 'results': score}), content_type = 'application/json')

	return HttpResponse(json.dumps({'message': 'empty queue'}), content_type = 'application/json')


def _add_detail_records(employee, statRec, score):
	resourceids = {}
	for id in score['details']:
		details = Detail.objects.filter(statistic=statRec, resource_id=id)
		try:
			a = score['details'][id][5]
		except IndexError:
			a = 0
		if len(details) > 0:
			details = details[0]
			details.title = score['details'][id][0]
			details.data = score['details'][id][1]
			details.counted = (1 if score['details'][id][2] == 0  else 0)
			details.flag = score['details'][id][2]
			details.start_time = score['details'][id][3]
			details.end_time = score['details'][id][4]
			details.time_range = a
			details.save()
		else:
			nlp_pversion = 0
			if statRec.product == 'turbobridge' or statRec.product == 'ringc_calls':
				nlp_pversion = 666	# Wait for transcript to process NLP
			if employee.domain.nlp_enabled == 0:
				nlp_pversion = 999
			#

			details = Detail(statistic=statRec, resource_id=id, title=score['details'][id][0], data=score['details'][id][1],
						counted=(1 if score['details'][id][2] == 0  else 0), flag= score['details'][id][2],
						start_time=score['details'][id][3], end_time=score['details'][id][4], nlp_pversion=nlp_pversion,time_range=a)
			details.save()

			try:
				if statRec.product == 'ringc_calls' and employee.domain.nlp_enabled == 1:
					recordingURL = score['details'][id][1]['recording']['contentUri'].split('/') # ['recording']['contentUri']
					account_id = recordingURL[6]
					recording_id = recordingURL[8]
					tsjob = Transcript_job(employee=employee, detail_id=details.id, status=0,
						params='accountId='+str(account_id)+'&recordingId='+str(recording_id)+'&domain='+str(employee.domain.id))
					tsjob.save()
			except:
				pass
			#

			resourceids[id] = details.id
		#
	# Create transcript job records to be processed by a relevent compute engine instance
	if statRec.product == 'turbobridge' and employee.domain.nlp_enabled == 1 and 'conferences' in score:
		for conference in score['conferences']:
			# Has top level detail record
			if score['conferences'][conference][0] in resourceids:
				tsjob = Transcript_job(employee=employee, detail_id=resourceids[ score['conferences'][conference][0] ], status=0,
						params='cdrID='+str(conference)+'&emp='+str(employee.id))
				tsjob.save()

# ========================================================================
#		ProsperWorks
# ========================================================================
def process_cron_prosperworks(request):
	if 'date' in request.GET and 'employee' in request.GET:
		stat_rec1 = Statistic.objects.filter(date=request.GET['date'], employee_id=request.GET['employee'], product='pwoprtunts')
		stat_rec2 = Statistic.objects.filter(date=request.GET['date'], employee_id=request.GET['employee'], product='pwleads')
	else:
		stat_rec1 = Statistic.objects.filter(score=-1)
		stat_rec2 = Statistic.objects.filter(score=-1)

	# =====================================================================
	# See if there is pending stat fetch to be run
	# =====================================================================
	if len(stat_rec1) != 0:
		stat_rec1 = stat_rec1[0]
		date_str1 = stat_rec1.date
	if len(stat_rec2) != 0:
		stat_rec2 = stat_rec2[0]
		date_str2 = stat_rec2.date

		# ==================================================================
		# Lock the records for this transaction, so another parallel instance would not attach to this set of records
		# ==================================================================
		if stat_rec1.score > -2:		#locking:
			stat_rec1.score = -2
			stat_rec1.save()
		if stat_rec2.score > -2:		#locking:
			stat_rec2.score = -2
			stat_rec2.save()

		employee = stat_rec1.employee
		date = datetime.datetime.strptime(date_str1, '%Y-%m-%d')
		timezone_offset = proapp.calc.get_timezone_offset(date, employee.domain.timezone)
		#
		working_hours = [map(int, employee.domain.daystart.split(':')), map(int, employee.domain.dayend.split(':'))]
		log('['+employee.email+' Cron]$ Processing Cron for: '+date_str1, 'info')


		# ============================================================
		score = {}
		score1 = proapp.products.prosperworks.get_opportunities(employee, date_str1, working_hours, timezone_offset)
		stat_rec1.score = score1['score']
		stat_rec1.save()
		prosperworks_details(stat_rec1, score1)

		# ============================================================
		score2 = proapp.products.prosperworks.get_leads(employee, date_str2, working_hours, timezone_offset)
		stat_rec2.score = score2['score']
		stat_rec2.save()
		prosperworks_details(stat_rec2, score2)

		# ==================================================================
		# Display status on console and render template for client with info on processed data
		# ==================================================================
		#
		log('['+employee.email+' Results]$ '+date_str1+' : '+employee.fullname+' : '+stat_rec1.product+' = '+str(score1['score']), 'success')
		score['details'] = '[archived]';
		return HttpResponse(json.dumps({'date': date_str1, 'fullname': employee.fullname, 'email': employee.email, 'product': stat_rec1.product, 'results': score1}), content_type = 'application/json')
	#
	return HttpResponse(json.dumps({'message': 'empty queue'}), content_type = 'application/json')


def prosperworks_details(stat_rec,score):
	for id in score['details']:
		details = Detail.objects.filter(statistic=stat_rec, resource_id=id)
		if len(details) > 0:
			details = details[0]
			details.title = score['details'][id][0]
			details.data = score['details'][id][1]
			details.counted = (1 if score['details'][id][2] == 0  else 0)
			details.flag = score['details'][id][2]
			details.start_time = score['details'][id][3]
			details.end_time = score['details'][id][4]
		else:
			details = Detail(statistic=stat_rec, resource_id=id, title=score['details'][id][0],
							 data=score['details'][id][1],
							 counted=(1 if score['details'][id][2] == 0  else 0), flag=score['details'][id][2],
							 start_time=score['details'][id][3], end_time=score['details'][id][4],
							 nlp_pversion=0)
		details.save()
	return 0


def schedule_salesforce_users(request):
	domains = Domain.objects.filter(crm_system='salesforce')
	urlpart = '/get-salesforce-users/?'
	utcnow = datetime.datetime.utcnow()
	domain_list = []
	utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
	for domain in domains:
		log('[' + domain.title + ' schedule task queues to get salesforce users', 'info')
		domain_list.append(domain.title)
		task_name = 'get-salesforce-users-' + domain.title.replace('.', '-dot-') + '-on-' + utcnow
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			taskqueue.add(url=urlpart + 'domain=' + str(domain.id)+ '&y=0', method='GET', queue_name='salesforce-users-cron',
						  name=task_name, countdown=3)

	return HttpResponse(json.dumps({'message': 'schedule-salesforce-users cron success', 'domain': domain_list}),
						content_type='application/json')


def get_salesforce_users(request):
	domain = Domain.objects.filter(id=request.GET['domain'])[0]
	y = request.GET['y']
	y = int(y)
	cacheKey = str(domain.id) + '-sf-matched-users-list'
	log('[' + domain.title + ' get salesforce users', 'info')
	cacheKey_name = str(domain.id) + '-sf-users-list-page-{}'
	if domain.salesforce_userid is not None and domain.salesforce_password is not None and domain.crm_token is not None:
		salesforce_userid = domain.salesforce_userid
		salesforce_password = domain.salesforce_password
		token_id = domain.crm_token
	else:
		return HttpResponse(json.dumps({'message': 'Authentication Failure. Invalid Credentials'}),
							content_type='application/json')
	url = 'https://login.salesforce.com/services/oauth2/token'
	payload = {'grant_type': 'password',
			   'client_id': '3MVG9szVa2RxsqBZcaYqIR5nZljlWeftvhfUfdS.9S9xsc81cBSxDTR2WfNQMKzN3bhZoCT.ZzWY1D61FihRT',
			   'client_secret': '7790752435074856812',
			   'username': salesforce_userid,
			   'password': salesforce_password + '{}'.format(token_id)
			   }
	headers = {'Accept-Charset': 'UTF-8'}
	try:
		r = requests.post(url, data=payload, headers=headers)
		response_data = r.json()
		if r.status_code == 200:
			sf = Salesforce(instance_url=response_data['instance_url'],
							session_id=response_data['access_token'])
			if y == 0:
				cache_data = memcache.get(cacheKey)
				if cache_data is not None:
					memcache.delete(cacheKey)
					log('Deleting the memcache key ' + cacheKey, 'info')
			user_type = 'Standard'
			records = sf.query("SELECT Name, Email, Id, Username, CreatedDate, UserType FROM User WHERE UserType = '%s'"%user_type)
			get_relevant_data(records['records'], domain)
			total_size = records['totalSize']
			max = total_size
			log('[' + str(max) + ' totalSize of records', 'info')
			if 'nextRecordsUrl' in records:
				x = 2000
				if x < max:
					try:
						y = y + 1
						urlpart = '/get-salesforce-users-page/?'
						utcnow = datetime.datetime.utcnow()
						utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
						record_url = records['nextRecordsUrl']

						task_name = 'get-salesforce-page-' + domain.title.replace('.', '-dot-') + '-on-' + utcnow
						if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
							taskqueue.add(url=urlpart + 'domain=' + str(domain.id) + '&y=' + str(
								y) + '&nextRecordsUrl=' + urllib.unquote(record_url.encode("utf8")), method='GET',
										  queue_name='salesforce-users-cron',
										  name=task_name, countdown=3)
					except KeyError:
						get_relevant_data(records['records'], domain)

		else:
			return HttpResponse(json.dumps({'message': 'Authentication Failure. Invalid Credentials'}),
								content_type='application/json')

	except SalesforceRefusedRequest:
		log('[' + domain.title + ' SalesforceRefusedRequest]$ Your password has expired or the REST API is not enabled for your company', 'error')

	return HttpResponse(json.dumps({'message': 'success'}), content_type='application/json')


def get_salesforce_users_page(request):
	domain = Domain.objects.filter(id=request.GET['domain'])[0]
	log('[' + domain.title + ' get salesforce users page', 'info')
	if domain.salesforce_userid is not None and domain.salesforce_password is not None and domain.crm_token is not None:
		salesforce_userid = domain.salesforce_userid
		salesforce_password = domain.salesforce_password
		token_id = domain.crm_token
	else:
		return HttpResponse(json.dumps({'message': 'Authentication Failure. Invalid Credentials'}),
							content_type='application/json')
	url = 'https://login.salesforce.com/services/oauth2/token'
	payload = {'grant_type': 'password',
			   'client_id': '3MVG9szVa2RxsqBZcaYqIR5nZljlWeftvhfUfdS.9S9xsc81cBSxDTR2WfNQMKzN3bhZoCT.ZzWY1D61FihRT',
			   'client_secret': '7790752435074856812',
			   'username': salesforce_userid,
			   'password': salesforce_password + '{}'.format(token_id)
			   }
	headers = {'Accept-Charset': 'UTF-8'}
	r = requests.post(url, data=payload, headers=headers)
	response_data = r.json()
	if r.status_code == 200:
		sf = Salesforce(instance_url=response_data['instance_url'], session_id=response_data['access_token'])
	else:
		return HttpResponse(json.dumps({'message': 'Authentication Failure. Invalid Credentials'}),
							content_type='application/json')
	y = int(request.GET['y'])
	next_record_url = urllib.unquote(request.GET['nextRecordsUrl'].decode("utf8"))
	records = sf.query_more(next_record_url, True)
	total_data = records['records']
	get_relevant_data(total_data, domain)
	y = y + 1
	if 'nextRecordsUrl' in records:
		urlpart = '/get-salesforce-users-page/?'
		utcnow = datetime.datetime.utcnow()
		utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
		record_url = records['nextRecordsUrl']

		task_name = 'get-salesforce-page-' + domain.title.replace('.', '-dot-') + '-on-' + utcnow
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			taskqueue.add(
				url=urlpart + 'domain=' + str(domain.id) + '&y=' + str(y) + '&nextRecordsUrl=' + urllib.unquote(
					record_url.encode("utf8")), method='GET', queue_name='salesforce-users-cron',
				name=task_name, countdown=3)
			log('[' + domain.title + ' Added new Task', 'info')

		else :
			log('[' + domain.title + ' Task Not Added', 'info')

	return HttpResponse(json.dumps({'message': 'success'}), content_type='application/json')


def get_relevant_data(total_data,domain):
	cacheKey = str(domain.id) + '-sf-matched-users-list'
	log('[' + cacheKey + ' cach key of salesforce users records', 'info')
	employees = Employee.objects.filter(domain_id=domain.id, role__gt=0)
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		cache_data = memcache.get(cacheKey)
	else:
		cache_data = cache.get(cacheKey)

	if cache_data is None:
		cache_data = []
		log('[' + domain.title + ' cache data is None', 'info')
	for record in total_data:
		record = dict(record)
		tmp = filter_record(record)
		cache_data.append(tmp)
		#
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			memcache.set(cacheKey, cache_data, 86400)
		else:
			cache.add(cacheKey, cache_data, 86400)


	return

def filter_record(record):
	tmp =  {}
	try:
		tmp['Id'] = record['Id']
		tmp['Name'] = record['Name']
		tmp['Email'] = record['Email']
		tmp['Username'] = record['Username']
	except KeyError:
		log('Record Filtering KeyError', 'error')

	return tmp


def schedule_sugarcrm_users(request):
	domains = Domain.objects.filter(crm_system='sugarcrm')
	urlpart = '/get-sugarcrm-users-page/?'
	utcnow = datetime.datetime.utcnow()
	utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
	domain_list = []
	for domain in domains:
		log('[' + domain.title + '] Scheduling task queue to get SugarCRM users', 'info')
		domain_list.append(domain.title)
		#
		memcache.delete(str(domain.id) + '-sugarcrm-matched-users-list')
		log('Deleting memcache user-list for '+domain.title, 'info')
		#
		task_name = 'get-sugarcrm-users-page-'+domain.title.replace('.', '-dot-')+'-on-'+utcnow
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			taskqueue.add(url = urlpart+'domain='+str(domain.id)+'&y=0&next_offset=0', method='GET', queue_name='sugarcrm-users-cron', name=task_name, countdown=3)

	return HttpResponse(json.dumps({'message': 'schedule-sugarcrm-users cron success', 'domain': domain_list}), content_type='application/json')

def get_sugarcrm_users_page(request):
	domain = Domain.objects.filter(id=request.GET['domain'])[0]
	sugar = Sugar_settings.objects.filter(domain=domain)[0]
	#cacheKey = str(domain.id) + '-sugarcrm-matched-users-list'
	log('['+domain.title+'] get sugarcrm users', 'info')
	if sugar.sugar_instance is not None:
		sugar_instance = sugar.sugar_instance
	else:
		return HttpResponse(json.dumps({'message': 'Authentication Failure. Invalid Credentials'}), content_type='application/json')
	#
	token_request = requests.get(token_url + str(domain.id))
	token_data = json.loads(token_request.text)
	access_token = token_data['token']
	#
	next_offset = request.GET['next_offset']
	y = int(request.GET['y'])
	#
	url = "https://" + sugar_instance + "/rest/v10/Users?&max_num=500&offset="+next_offset
	headers = {"Content-Type": "application/json", "OAuth-Token": access_token}
	r = requests.get(url, headers=headers)
	response = json.loads(r.text)
	#
	if 'records' in response and response['records'] != '':
		records = response['records']
		get_sugarcrm_relevant_data(records, domain)
	y = y + 1
	if response['next_offset'] != -1:
		urlpart = '/get-sugarcrm-users-page/?'
		utcnow = datetime.datetime.utcnow()
		utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
		next_offset = int(next_offset) + 500

		task_name = 'get-sugarcrm-page-' + domain.title.replace('.', '-dot-') + '-on-' + utcnow
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			taskqueue.add(url=urlpart + 'domain='+str(domain.id)+'&y='+str(y)+'&next_offset='+str(next_offset), method='GET', queue_name='sugarcrm-users-cron', name=task_name, countdown=3)
			log('['+domain.title+'] Added new Task', 'info')
		else:
			log('['+domain.title+'] Task Not Added', 'info')

	return HttpResponse(json.dumps({'message': 'success'}), content_type='application/json')

def get_sugarcrm_relevant_data(total_data, domain):
	cacheKey = str(domain.id) + '-sugarcrm-matched-users-list'
	log('[' + cacheKey + ' cach key of sugarcrm users records', 'info')
	#
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		cache_data = memcache.get(cacheKey)
	else:
		cache_data = cache.get(cacheKey)
	#
	if cache_data is None:
		cache_data = []
		log('[' + domain.title + ' cache data is None', 'info')
	for record in total_data:
		tmp = filter_sugarcrm_record(record)
		cache_data.append(tmp)
	#next_offset
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		memcache.set(cacheKey, cache_data, 86400)
	else:
		cache.add(cacheKey, cache_data, 86400)
	#
	return

def filter_sugarcrm_record(record):
	tmp =  {}
	try:
		if not record['email']:
			email = ''
		else:
			email = record['email'][0]['email_address']
		tmp['id'] = record['id']
		tmp['name'] = record['name']
		tmp['email'] = email
	except KeyError:
		log('Record Filtering KeyError', 'error')

	return tmp

'''
def get_sugarcrm_users(request):
	sugar = Sugar_settings.objects.filter(domain_id=request.GET['domain'])[0]
	domain = Domain.objects.filter(id=request.GET['domain'])[0]
	y = request.GET['y']
	y = int(y)
	cacheKey = str(domain.id) + '-sugarcrm-matched-users-list'
	log('[' + domain.title + ' get sugarcrm users', 'info')
	if sugar.sugar_instance is not None:
		sugar_instance = sugar.sugar_instance
	else:
		return HttpResponse(json.dumps({'message': 'Authentication Failure. Invalid Credentials'}),
							content_type='application/json')

	try:
		token_request = requests.get(token_url + str(domain.id))
		token_data = json.loads(token_request.text)
		access_token = token_data['token']
		url = "https://" + sugar_instance + "/rest/v10/Users?&max_num=500"
		headers = {"Content-Type": "application/json", "OAuth-Token": access_token}
		r = requests.get(url, headers=headers)
		response = json.loads(r.text)

		if y == 0:
			cache_data = memcache.get(cacheKey)
			if cache_data is not None:
				memcache.delete(cacheKey)
				log('Deleting the memcache key ' + cacheKey, 'info')
		if 'records' in response and response['records'] != '':
			records = response['records']
			get_sugarcrm_relevant_data(records, domain)
			if response['next_offset'] != -1 :
				try:
					y = y + 1
					urlpart = '/get-sugarcrm-users-page/?'
					utcnow = datetime.datetime.utcnow()
					utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
					next_offset = 500

					task_name = 'get-sugarcrm-page-' + domain.title.replace('.', '-dot-') + '-on-' + utcnow
					if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
						taskqueue.add(url=urlpart + 'domain=' + str(domain.id) + '&y=' + str(
							y) + '&next_offset=' + str(next_offset), method='GET',
									  queue_name='sugarcrm-users-cron',
									  name=task_name, countdown=3)
				except KeyError:
					get_sugarcrm_relevant_data(records, domain)

			else:
				return HttpResponse(json.dumps({'message': 'Authentication Failure. Invalid Credentials'}),
									content_type='application/json')

	except KeyError:
		log('[' + domain.title , 'KeyError')

	return HttpResponse(json.dumps({'message': 'success'}), content_type='application/json')
'''


# ========================================================================
#		LIB
# ========================================================================

def convert_ascii(value):
	b = value
	a = [ord(c) for c in b]
	c = sum(a)
	return c

# ========================================================================
# Check if a given time is within working hours
# ========================================================================
def within(given_time, working_hours):
	if datetime.time(working_hours[0][0], working_hours[0][1]) < given_time.time() < datetime.time(working_hours[1][0], working_hours[1][1]):
		return True
	else:
		return False

logger = logging.getLogger(__name__)
def log(str, level):
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		if level == 'error':
			logger.error(str)
		elif level == 'warning':
			logger.warning(str)
		else:
			logger.info(str)
	else:
		colors = {'error': '\033[91m', 'success': '\033[92m', 'warning': '\033[93m', 'info': ''}#\033[94m
		print(colors[level]+str+'\033[0m')

class GetReq(object):
	GET = {}
	META = {}
	def __init__(self, params):
		self.GET = params
		self.META = params
# ========================================================================
#		EOF
# ========================================================================


def copy_hangouts_msg_to_title(request):
	# ----------------------------------------------
	#	Fetch 100 yet-not-copied detail records from the database
	# ----------------------------------------------
	details = Detail.objects.extra(where=["title = '[chat-message]'"]).order_by('-id')
	details.query.set_limits(low=0, high=50)
	#
	i = 0
	msgs = []
	for detail in details:
		try:
			data = json.loads(detail.data)
		except:
			try:
				data = eval(detail.data)
			except:
				data = {}
		#
		if 'data' in data:
			message = base64.b64decode(data['data'].replace('_', '/').replace('-', '+'))
		else:
			message = ''
		#
		if len(message) > 128:
			message = message[:125] + '..'
		#
		msgs.append(message)
		detail.title = message
		try:
			detail.save()
		except:
			# Probably a Unicode sequence
			detail.title = '[ chat-message ]'
			detail.save()
		i += 1
	#
	return HttpResponse(json.dumps({'processed': i}), content_type = 'application/json')


def doc_content_diff_migration(request):
	from .models import Doc
	proc100 = Doc.objects.filter(migrated=0).order_by('-id')
	proc100.query.set_limits(low=0, high=333)
	#
	for proc1 in proc100:
		#print(proc1.content)
		#if proc1.content != '':
		#	proapp.storage.write_file(proc1, '.content.json', proc1.content)
		#if proc1.diff != '':
		#	proapp.storage.write_file(proc1, '.diff.json', proc1.diff)
		proc1.save()
	#
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine') and len(proc100) == 333:
		utcnow = datetime.datetime.utcnow()
		utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
		taskqueue.add(url='/doc_content_diff_migration',
					method='GET', queue_name='schedule', 
					name='doc-content-diff-migration-on-'+utcnow,
					countdown = 5)
	#
	return HttpResponse(json.dumps({'message': 'processed '+str(len(proc100))}), content_type = 'application/json')


def org_prodoscore_cron(request):
	##-#
		# socre = -1 : not yet processed
		# score = -2 : score not calculated because non-working day
		# score > -1 : organization prodoscore value

		# finalized 0 : organization proddoscore is not finalized
		# finalized 1 : organization proddoscore is finalized
	##-#

	if 'domain' not in request.GET:
		org_prod_recs = Organization_prodoscore.objects.using('replica').filter(score=-1).order_by('-id').values('domain_id', 'date')[:200]
		domain_list = []

		utcnow = datetime.datetime.utcnow()
		utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')

		for domain in org_prod_recs:
			domain_id = domain['domain_id']
			date = domain['date']
			domain_list.append(str(domain_id) + ':' + str(date))
			if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
				urlpart = '/org-prod-cron/?domain=' + str(domain_id) + '&date=' + str(date)
				taskqueue.add(url=urlpart,
							  method='GET', queue_name='org-prod-cron',
							  name='org-prod-cron-' + str(domain_id) + utcnow + str(date),
							  countdown=domain_id * 1)

		return HttpResponse(json.dumps({'message': 'Tasks Added for Domain List ' + str(domain_list)}), content_type='application/json')

	else:

		domain_id = request.GET['domain']

		if 'date' in request.GET:
			date = request.GET['date']
		else:
			date = datetime.datetime.now() - timedelta(days=1)
			date = date.strftime("%Y-%m-%d")

		##-# check weather organization prodoscore is scheduled
		org_prod_check = Organization_prodoscore.objects.using('replica').filter(domain_id=domain_id, date=date).exists()
		if not org_prod_check:
			return HttpResponse(json.dumps({'message': 'No Records in Employee Prodoscore'}), content_type='application/json')

		##-# check weather stat records are remaining which is not processed yet
		stat_recs_not_failed = Statistic.objects.using('replica').filter(date=date, retry_count__lt=6).extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE domain_id = ' + str(domain_id) + ' AND role > 0 AND status > 0) AND score IN (-1, -2, -5)']).exists()
		if stat_recs_not_failed:
			return HttpResponse(json.dumps({'message': 'Still Statistic Records are not Processed'}), content_type='application/json')

		##-#
		# if
			# organization prodoscore is not calculated yet
			# all statistic record are processed or failed permanently
		# then calculates the organization_prodoscore, employee_prodoscore and store
		##-#
		users = Employee.objects.using('replica').filter(domain_id=domain_id, role__gt=-1, status__gt=-1)
		date_short = (int(datetime.datetime.strptime(date, '%Y-%m-%d').strftime('%s')) / 86400) - 16800
		workingday = json.loads(Domain.objects.using('replica').values('workingdays').get(id=domain_id)['workingdays'])
		day_of_week = (date_short + 4) % 7

		if day_of_week in workingday:
			org_prod = proapp.calc.process_scores(domain_id, users, date_short, date_short, workingday, {'cache': True})['organization']['score']
		else:
			org_prod = -2

		org_prod_rec = Organization_prodoscore.objects.filter(domain_id=domain_id, date=date).get()
		org_prod_rec.score = org_prod
		org_prod_rec.finalized = 1
		stat_recs_failed = Statistic.objects.using('replica').filter(date=date, score__lt=0, retry_count=3).extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE domain_id = ' + str(domain_id) + ' AND role > 0 AND status > 0)']).exists()
		if stat_recs_failed:
			org_prod_rec.finalized = 0
		org_prod_rec.save()


		##-# ########################################################################################################################
		##-# Schedule the dashboard_details_cron only if the Relevent Day
		##-# ########################################################################################################################
		today = datetime.datetime.now() - timedelta(days=1)
		today = today.strftime("%Y-%m-%d")
		if date == today:
			utcnow = datetime.datetime.utcnow()
			utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
			urlpart = '/dashboard-details-cron/?domain=' + str(domain_id)
			if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
				taskqueue.add(url=urlpart,
									method='GET', queue_name='dashboard-loading-details', 
									name='dashboard-loading-details-'+str(domain_id)+'-on-'+str(utcnow),
									countdown = 0)
			return HttpResponse(json.dumps({'message': 'Cron Succeeded for Domain ' + domain_id + ' for ' + date + ' and dashboard_details_cron Scheduled'}), content_type='application/json')

		
		return HttpResponse(json.dumps({'message': 'Cron Succeeded for Domain ' + domain_id + ' for ' + date + ' and dashboard_details_cron NOT Scheduled'}), content_type='application/json')


	return HttpResponse(json.dumps({'message': 'No Any Employee Prodosore Records'}), content_type='application/json')


def dashboard_details_cron(request, is_local = None):

	class Request:
			def __init__(self, fromdate, todate, user):
				self.GET = {
				'fromdate': fromdate,
				'todate': todate
				}
				self.user = user

			
	if 'date' in request.GET:
			todate = request.GET['date']
	else:
		todate = datetime.datetime.now() - timedelta(days=1)
		todate = todate.strftime("%Y-%m-%d")

	predefined_periods = ['yesterday', 'this_week', 'last_7_days', 'this_month', 'last_30_days']


	##-# ########## ==========  ########## ========== ########## ========== ########## ========== ########## ==========
	##-# First Level (With Domain)
	##-# ########## ==========  ########## ========== ########## ========== ########## ========== ########## ==========
	
	if 'domain' in request.GET:
		domain = request.GET['domain']

		try:
			domain = Domain.objects.get(id = domain)
		except:
			return HttpResponse(json.dumps({'error': 'No Domian'}), content_type='application/json')

		
		date_short = (int(datetime.datetime.strptime(todate, '%Y-%m-%d').strftime('%s')) / 86400) - 16800
		workingdays = json.loads(Domain.objects.using('replica').values('workingdays').get(id=domain.id)['workingdays'])
		day_of_week = (date_short + 4) % 7

		if day_of_week not in workingdays:	
			return HttpResponse(json.dumps({'message': 'Non Working Day'}), content_type='application/json')
		
		try:
			managers_and_admin = Employee.objects.filter(domain = domain.id).extra(where=['role in (70,80)'])
		except:
			return HttpResponse(json.dumps({'message': 'No Any Manager or Admin in Domain'}), content_type='application/json')

		user_id_list = []
		for user in managers_and_admin:
			user_id_list.append(str(user.id))

			for period in predefined_periods:
				urlpart = '/dashboard-details-cron/?employee=' + str(user.id) + '&period=' + period + '&date=' + todate

				utcnow = datetime.datetime.utcnow()
				utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
				
				if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
					taskqueue.add(url=urlpart,
								method='GET', queue_name='dashboard-loading-details', 
								name='dashboard-loading-details-'+str(user.id)+'-of-'+str(domain.title).replace('.', '-dot-')+'-for-'+period+'-on-'+str(utcnow),
								countdown = 2)

		return HttpResponse(json.dumps({'message': 'Scheuled for User: ' + str(user_id_list)}), content_type='application/json')



	##-# ########## ==========  ########## ========== ########## ========== ########## ========== ########## ==========
	##-# Second Level (With Employee)
	##-# ########## ==========  ########## ========== ########## ========== ########## ========== ########## ==========
	
	elif 'employee' in request.GET and 'period' in request.GET:

		is_real_request = False
		if not is_local:
			is_local = {'require': 'dashboard-loading-details'}
			is_real_request = True


		employee = request.GET['employee']
		employee = Employee.objects.get(id = employee)
		date_short = (int(datetime.datetime.strptime(todate, '%Y-%m-%d').strftime('%s')) / 86400) - 16800
		workingdays = json.loads(employee.domain.workingdays)
		day_of_week = (date_short + 4) % 7

		if day_of_week not in workingdays:	
			return HttpResponse(json.dumps({'message': 'Non Working Day'}), content_type='application/json')

		period = request.GET['period']
		if period not in predefined_periods:
			return HttpResponse(json.dumps({'message': 'Non Predefined Period'}), content_type='application/json')

		

		if period == 'yesterday':
			fromdate = todate
		elif period == 'this_week':
			fromdate = (datetime.datetime.strptime(todate, '%Y-%m-%d') - datetime.timedelta(days = day_of_week - 1)).strftime("%Y-%m-%d")
		elif period == 'last_7_days':
			fromdate = (datetime.datetime.strptime(todate, '%Y-%m-%d') - datetime.timedelta(days = 6)).strftime("%Y-%m-%d")
		elif period == 'this_month':
			temp = todate.split('-')
			fromdate  = temp[0] + '-' + temp[1] + '-01'
		elif period == 'last_30_days':
			fromdate = (datetime.datetime.strptime(todate, '%Y-%m-%d') - datetime.timedelta(days = 29)).strftime("%Y-%m-%d")

		fake_request = Request(fromdate, todate, employee.id)
		file_content = {}

		if 'dashboard-loading-details' in is_local['require'] or 'employee-list-with-score' in is_local['require']:
			##-# ##########################################################################
			##-# Ajax_Dashboard Part
			##-# ##########################################################################
			sum_array = proapp.views.ajax_dashboard(fake_request, True)
			emp_list = sum_array['employees']

			##-# ##########################################################################
			##-# Writing and Caching Employee Details with Scores (emp_list)
			##-# ##########################################################################
			tag = 'employee-list-with-score'
			proapp.storage.write_file_with_path(employee.id, employee.domain.id, period, tag, emp_list)
			add_to_cache(employee.id, period, tag, emp_list)
			
			if 'employee-list-with-score' in is_local['require']:
				# return_data = {}
				# return_data['employee_list'] = sum_array['employees']
				# return_data['details'] = {
				# 	'employee': employee.id,
				# 	'fromdate': fromdate,
				# 	'todate': todate,
				# 	'period': period
				# }
				# return return_data
				return sum_array['employees']

			##-# ##########################################################################
			##-# Getting Top Eight Employees
			##-# ##########################################################################
			emp_dic = {}
			emp_score_list = []

			for user_id, emp in emp_list.iteritems():
				if 'scr' not in emp or emp['scr'] == False:
					emp['scr'] = {'l': 0}
					
				temp = int(math.floor(emp['scr']['l']/10))
				emp_score_list.append(temp)
				emp_dic[('000000' + str(int(emp['scr']['l']*1000)))[-6:] + '-' + str(user_id)] = emp 
			emp_dic = OrderedDict(sorted(emp_dic.items(), key=lambda t: t[0], reverse=True))

			emp_score_dic = {
				'0': emp_score_list.count(0),
				'1': emp_score_list.count(1),
				'2': emp_score_list.count(2),
				'3': emp_score_list.count(3),
				'4': emp_score_list.count(4),
				'5': emp_score_list.count(5),
				'6': emp_score_list.count(6),
				'7': emp_score_list.count(7),
				'8': emp_score_list.count(8),
				'9': emp_score_list.count(9)
			}
			sum_array['organization']['histogram_details'] = emp_score_dic

			try:
				emp_dic = OrderedDict(emp_dic.items()[:8])
			except:
				pass
			
			top_user_list = OrderedDict()
			for key, value in emp_dic.iteritems():
				user_id = key.split('-')[1]
				value['hasSubordinates'] = Employee.objects.filter(manager_id = user_id, role__gt=0, status__gt=0).exists()
				top_user_list[user_id] = value
				
			
			sum_array['employees'] = top_user_list

			sum_array['organization']['strata'][0] = len(sum_array['organization']['strata'][0])
			sum_array['organization']['strata'][1] = len(sum_array['organization']['strata'][1])
			sum_array['organization']['strata'][2] = len(sum_array['organization']['strata'][2])


		if 'dashboard-loading-details' in is_local['require'] or 'correlation-list-with-significance' in is_local['require']:
			##-# ##########################################################################
			##-# Ajax_Correlations Part
			##-# ##########################################################################
			correlation_array = proapp.views.ajax_correlations(fake_request, True)

			##-# ##########################################################################
			##-# Writing and Caching Correlation Details (correlation_array)
			##-# ##########################################################################
			tag = 'correlation-list-with-significance'
			proapp.storage.write_file_with_path(employee.id, employee.domain.id, period, tag, correlation_array)
			add_to_cache(employee.id, period, tag, correlation_array)

			if 'correlation-list-with-significance' in is_local['require']:
				# return_data = {}
				# return_data['correlation_list'] = correlation_array
				# return_data['details'] = {
				# 	'employee': employee.id,
				# 	'fromdate': fromdate,
				# 	'todate': todate,
				# 	'period': period
				# }
				# return return_data
				return correlation_array

			##-# ##########################################################################
			##-# Correlations for Organizations
			##-# ##########################################################################
			for etype in correlation_array:
				entity_dic = {}
				try: 
					entities_list = correlation_array[etype]

					for entity_id, entity in entities_list.iteritems():
						entity_dic[('000000' + str(entity['significance']))[-6:] + '-' + str(entity_id)] = entity

					##-# ##########################################################################
					##-# Getting Top Eight Organizations
					##-# ##########################################################################
					entity_dic = OrderedDict(sorted(entity_dic.items(), key=lambda t: t[0], reverse=True))
					try:
						entity_dic = OrderedDict(entity_dic.items()[:8])
					except:
						pass

					top_org_list = OrderedDict()
					for key, value in entity_dic.iteritems():
						entity_id =  str(key.split('-')[1])
						top_org_list[entity_id] = value

					correlation_array[etype] = top_org_list

				except:
					entity_dic = {}

			##-# ##########################################################################
			##-# Correlations for Persons
			##-# ##########################################################################
			'''persons_dic = {}
			try:
				persons_list = correlation_array['coos']

				for persons_id, person in persons_list.iteritems():
					persons_dic[('000000' + str(person['significance']))[-6:] + '-' + str(persons_id)] = person

				##-# ##########################################################################
				##-# Getting Top Eight Persons
				##-# ##########################################################################
				try:
					persons_dic = OrderedDict(persons_dic.items()[:8])
				except:
					pass

				top_person_list = OrderedDict()	
				for key, value in persons_dic.iteritems():
					persons_id =  str(key.split('-')[1])
					top_person_list[persons_id] = value

				correlation_array['coos'] = top_person_list
			except:
				persons_dic = {}'''

		file_content['details'] = {
			'employee': employee.id,
			'fromdate': fromdate,
			'todate': todate,
			'period': period
		}

		file_content['ajax_dashboard'] = sum_array
		file_content['ajax_correlations'] = correlation_array

		##-# ##########################################################################
		##-# Writing and Caching Dashboard Loading Details (file_content)
		##-# ##########################################################################
		tag = 'dashboard-loading-details'
		proapp.storage.write_file_with_path(employee.id, employee.domain.id, period, tag, file_content)
		add_to_cache(employee.id, period, tag, file_content)

		if not is_real_request and 'dashboard-loading-details' in is_local['require']:
			return file_content

		return HttpResponse(json.dumps({'message': 'Cron Successfull'}), content_type='application/json')

	return HttpResponse(json.dumps({'message': 'Something Went Wrong'}), content_type='application/json')


def add_to_cache(employee, period, tag, data):
		cache_key = tag + '-' + str(employee) + '-' + period
		proapp.cache.set(cache_key, data, 24*3600)

