
import os
if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import memcache
else:
	from django.core.cache import cache as dcache

def get(cacheKey):
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		return memcache.get(cacheKey)
	else:
		return dcache.get(cacheKey)

def add(cacheKey, data, timeout):
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		memcache.add(cacheKey, data, timeout)
	else:
		dcache.add(cacheKey, data, timeout)
	#
	return True

def set(cacheKey, data, timeout):
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		memcache.set(cacheKey, data, timeout)
	else:
		dcache.set(cacheKey, data, timeout)
	#
	return True

def delete(cacheKey):
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		memcache.delete(cacheKey)
	else:
		dcache.delete(cacheKey)
	#
	return True