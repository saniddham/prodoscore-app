
import os, json, datetime, time, operator, logging
import hashlib, requests, base64
from requests.exceptions import ConnectionError, InvalidURL
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.conf import settings

from datetime import timedelta
import threading
import products.sugarCRM
from .models import Domain, Employee, Statistic, Leave, Onboard, Broadsoft_admin, Sugar_settings
import proapp.views
import proapp.products.ringcentral_calls

from proapp.products.sugarCRM import token_url
from proapp.products.ringcentral_calls import token_url_rc
from proapp.products.vbc import token_url_vbc

import proapp.cache

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import memcache, taskqueue, urlfetch
else:
	from django.core.cache import cache

# ========================================================================
# Save employee/user settings for a single employee/user or multiple employees/users
# 	Receives JSON data in an Ajax POST request
# ========================================================================
def update_employee(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	#if 'user' not in auth and 'redirect' in auth:
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	data = {'employees': {}}
	data['seats'] = auth['user'].domain.subscription_volume
	if data['seats'] is None:
		data['seats'] = float('inf')
	data['activated'] = Employee.objects.filter(domain=auth['user'].domain, role__gt=0).count()

	output = []
	errors = {}
	hasErrors = False
	# ===============================================================
	# BLOCK COMMENTS ARE CAUSING INDENTATION MISTAKES MAKING IT HARDER TO TRACK BLOCKS OF CODE
	# ===============================================================
	if request.method=='POST':
		# Parse JSON request body
		received_json_data = json.loads(request.body)
		new_activated = []
		remaining = data['seats'] - data['activated']

		# Iterate through user objects in request
		for item in received_json_data:
			emp_id = item.get('id')

			# Find if the email/username is a duplicate
			if 'email' in item:
				if emp_id == 'new':
					chk_duplicate = Employee.objects.filter(email=item.get('email'))
				else:
					chk_duplicate = Employee.objects.filter(email=item.get('email')).exclude(id=emp_id)
				if len(chk_duplicate) != 0:
					#return HttpResponse("username duplicate")
					errors[item.get('email')] = "Username is duplicate"
					hasErrors = True
					continue

			# A new user account to be created
			if (emp_id == 'new'):
				employee = Employee(
					domain_id=auth['user'].domain_id,
					role=item.get('role'),
					fullname=item.get('fullname'),
					email=item.get('email'),
					password=hashlib.sha512(item.get('password')).hexdigest(),
					status=-1,
					profileId=0,
					level=0,
###				admin=1,
					is_app_user = item.get('appuser', 0))

			# Open an existing user account to edit
			else:
				employee = Employee.objects.get(id=emp_id)

				# Copy attributes from JSON to database object //update_organization
				if 'role' in item:
					if int(employee.role) < 1 and int(item['role']) > 0:
						remaining -= 1
						if remaining >= 0:
							new_activated.append({'id': emp_id, 'email': employee.email, 'fullname': employee.fullname})
						else:
							item['role'] = employee.role
							errors[employee.email] = "Number of users in the uploaded file exceeds the number of licenses available"
							hasErrors = True
						#	return HttpResponse(json.dumps(data), content_type = 'application/json')
					#
					# Not to allow changing the role of self
					if int(emp_id) != int(auth['user'].id):
						employee.role = int(item.get('role'))
					#
					#	Disconnect products from terminated users
					if int(item.get('role')) in [-1, -2]:
						employee.conf_id = None
						employee.crm_id = None
						employee.broadsoft_admin_id = None
						employee.broadsoft_userid = None
						employee.salesforce_userid = None
						employee.salesforce_updated = None
						employee.phone_system_id = None
						employee.chatter_id = None
				if 'fullname' in item:
					employee.fullname = item.get('fullname')
				if 'email' in item:
					employee.email = item.get('email')
				if 'password' in item and item.get('password') != '[ENCRYPTED]':
					employee.password = hashlib.sha512(item.get('password')).hexdigest()

			if 'status'in item:
				employee.status = int(item.get('status'))
			if 'manager' in item:
				employee.manager_id = item.get('manager')
			if 'reportEmail'in item:
				employee.report_email = item.get('reportEmail')
			if 'reportFrequency'in item:
				employee.report_enabled = int(item.get('reportFrequency'))

			# Write (commit) the database object
			employee.save()
			output.append(employee.id)

		# Check if new employees activated
		if len(new_activated) > 0:
			# Get the email HTML rendered
			total_employees = Employee.objects.filter(domain=auth['user'].domain, role__gt=0).count()
			res = render(request, 'email/users-added.html', {'domain': auth['user'].domain, 'users': new_activated, 'new_count': len(new_activated), 'total_employees': total_employees})
			#
			# Send notification email to support team
			requests.post(
				settings.MAILGUN['API_ENDPOINT'], auth=("api", settings.MAILGUN['API_KEY']),
				data={"from": settings.MAILGUN['FROM'],
					"to": ["support@prodoscore.com"], #support@prodoscore.com
					"subject": "Domain "+auth['user'].domain.title+" has activated "+str(len(new_activated))+" new users",
					"html": res})
		#
		# Acknowledge the client
		if len(output) > 0:
			data = {'message': 'database updated', 'data-ids': output}
		elif not hasErrors:
			data = {'message': 'nothing to do'}
		else:
			data = {}
		#
		if hasErrors:
			data['errors'] = errors

	else:
		if 'terminated' in request.GET and eval(request.GET['terminated']) == True:
			users = Employee.objects.filter(domain=auth['user'].domain, role__lt=0).order_by('fullname')
		#elif 'new-employees' in request.GET and eval(request.GET['new-employees']) == True:
		#	users = Employee.objects.filter(domain=auth['user'].domain, role=0).order_by('fullname')
		else:
			users = Employee.objects.filter(domain=auth['user'].domain,role__gte=0).order_by('fullname')

			if auth['user'].role == 70: # manager user
				employee_dict = {user.id: user for user in users}
				subordinate_list = get_subordinates_of_employee(employee_dict, auth['user'].id)
				subordinate_list.append(employee_dict[auth['user'].id])
				if auth['user'].manager_id != 52 and hasattr(auth['user'], 'manager'): #No manager assigned
					subordinate_list.append(employee_dict[auth['user'].manager.id])
				#
				users = subordinate_list

		if len(users) > 0:
			for user in users:
				if ('filter' not in request.GET or request.GET['filter'] != 'appusers') or user.is_app_user:
					data['employees'][user.id] = {'fullname': user.fullname, 'email': user.email,'report_enabled': user.report_enabled, 'report_email': user.report_email,
										'level': user.level, 'role': user.role, 'manager': user.manager_id,'status': 1 if user.status == 1 else -1,#, 'admin': user.admin
										'picture': user.picture,'is_subordinate': user.manager_id == auth['user'].id, 'is_app_user': user.is_app_user}

	schedule_file_creating_for_domain(auth['user'].domain_id) ##-# Creating Files

	return HttpResponse(json.dumps(data), content_type = 'application/json')


##-# ########################################################################################
##-# Crearting Files for Dashboard Loadding
##-# ########################################################################################
def schedule_file_creating_for_domain(domain_id):
	utcnow = datetime.datetime.utcnow()
	utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
	urlpart = '/dashboard-details-cron/?domain=' + str(domain_id)
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		taskqueue.add(url=urlpart,
							method='GET', queue_name='dashboard-loading-details', 
							name='dashboard-loading-details-'+str(domain_id)+'-on-'+str(utcnow),
							countdown = 0)

def get_subordinates_of_employee(employee_list, manager_id):
	subordinates = [employee_list[emp] for emp in employee_list if employee_list[emp].manager_id==manager_id]
	lower_level_subs = []
	for subordinate in subordinates:
		lower_level_subs += get_subordinates_of_employee(employee_list, subordinate.id)
	subordinates+=lower_level_subs
	return subordinates

# ========================================================================
# Single employee details and leaves
# 	to be used in employee settings pages
# 	requested in an Ajax call
# ========================================================================
def employee_settings(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	data = {}
	users = Employee.objects.filter(domain=auth['user'].domain, id=request.GET['id'])
	mgrs = Employee.objects.filter(domain=auth['user'].domain, role__gte=70, status = 1)
	if len(users) > 0:
		data['employee'] = {'fullname': users[0].fullname, 'email': users[0].email,
						'level': users[0].level, 'picture': users[0].picture,#, 'admin': users[0].admin
						'report_email': users[0].report_email, 'report_enabled': users[0].report_enabled,
						'role': users[0].role, 'manager': users[0].manager_id, 'status': users[0].status}
		#
		data['managers'] = {}
		for mgr in mgrs:
			data['managers'][mgr.id] = mgr.fullname
	return HttpResponse(json.dumps(data), content_type = 'application/json')


# ========================================================================
# Register employee leave days to the database
# 	to be used in employee settings pages
# 	requested in an Ajax call
# ========================================================================
def register_leaves(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	data = []
	result = False
	if request.method=='POST':
		# ==================================================================
		# Parse JSON request body
		# ==================================================================
		received_json_data=json.loads(request.body)

		# ==================================================================
		# Iterate through user objects in request
		# ==================================================================
		workingdays = json.loads(auth['user'].domain.workingdays)
		if 'from_date' in received_json_data:
			from_date = datetime.datetime.strptime(received_json_data['from_date'], "%Y-%m-%d")
			to_date = datetime.datetime.strptime(received_json_data['to_date'], "%Y-%m-%d")
			for i in range(0, (to_date - from_date).days+1):
				result = _save_leave(from_date, workingdays, received_json_data['employee'], received_json_data['description'])
				if result:
					data.append(result)
				from_date += timedelta(days=1)
		else:
			date = datetime.datetime.strptime(received_json_data['date'], "%Y-%m-%d")
			result = _save_leave(date, workingdays, received_json_data['employee'], received_json_data['description'])
			if result:
				data.append(result)

	# =====================================================================
	# Acknowledge the client
	# =====================================================================
	return HttpResponse(json.dumps(data), content_type = 'application/json')

def _save_leave(date, workingdays, employee_id, description):
	if int(date.strftime('%w')) in workingdays:
		chkLeaves = Leave.objects.filter(employee_id=employee_id, date=date)
		if len(chkLeaves) == 0:
			leave = Leave(employee_id=employee_id, date=date, description=description)
			leave.save()
			return {'date': date.strftime('%Y-%m-%d'), 'description': description}
		else:
			chkLeaves[0].description = description
			chkLeaves[0].save()
			return {'date': date.strftime('%Y-%m-%d'), 'description': description}
	return False

# ========================================================================
# Returns a list of all timezones in JSON
# 	to be used in dropdown on settings form
# 	requested in an Ajax call
# ========================================================================
def timezone_request(request):
	import pytz
	allTimezones = pytz.all_timezones

	# =====================================================================
	# Build an array of time zones
	# =====================================================================
	time_zones = {'Other': []}
	for tz in allTimezones:
		if '/' in tz:
			str = tz.split('/', 1)
			if str[0] not in time_zones:
				time_zones[str[0]] = []
			time_zones[str[0]].append([tz, str[1].replace('_', ' ')])
		else:
			time_zones['Other'].append([tz, tz.replace('_', ' ')])

	# =====================================================================
	# Write JSON to the response
	# =====================================================================
	keys = sorted(time_zones)
	data = []
	for key in keys:
		data.append([key, time_zones[key]])
	return HttpResponse(json.dumps(data), content_type = 'application/json')


# ========================================================================
# Calculate baselines per domain for a given role
# ========================================================================
def role_baselines(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	baselines = calc_role_baselines(auth['user'].domain_id, request.GET['role'])
	calculated_avgs = {'products': baselines}
	#
	from .models import Baseline
	recs = Baseline.objects.filter(domain_id=auth['user'].domain_id, role=request.GET['role'])
	calculated_avgs['has_org_baseline'] = len(recs)
	#
	return HttpResponse(json.dumps(calculated_avgs), content_type = 'application/json')


# ========================================================================
# Calculate baselines per domain for a given role
# ========================================================================
def calc_role_baselines(domain, role):
	cacheKey = 'org-baseline-'+str(domain)+'-'+str(role)
	data = proapp.cache.get(cacheKey)
	if data is not None:
		return data
	#
	products = {}
	#
	todate = datetime.datetime.now()
	emp_scores_this = ( Statistic.objects.using('replica')
						.extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE domain_id = '+str(domain)+')'])
						.exclude(score__lt=0) ).order_by('-id')[:1]
	if len(emp_scores_this) > 0:
		todate = datetime.datetime.strptime(emp_scores_this[0].date, '%Y-%m-%d')
	#
	fourmonths = todate - timedelta(days=180)
	calcAvgs = Statistic.objects.using('replica').raw(
		'SELECT pb.id, code AS pcode, AVG(score) AS baseline '+
		'FROM proapp_statistic ps '+
		'LEFT JOIN proapp_employee pe ON pe.id = ps.employee_id '+
		'LEFT JOIN proapp_product pp ON pp.slug = product '+
		'LEFT JOIN proapp_baseline pb ON pb.role = pe.role AND pb.pcode = pp.code '+
		'WHERE score > 0 AND code != \'\' '+
			'AND pe.role = '+str(role)+' '+
			'AND pe.domain_id = '+str(domain)+' '+
			'AND date > \''+fourmonths.strftime('%Y-%m-%d')+'\' '+
		'GROUP BY pb.id, code')
	for calcAvg in calcAvgs:
		orgAvg = float(str(calcAvg.baseline))
		globAvg = 999
		delta = False
		intRole = int(role)
		if intRole in proapp.calc.products[ calcAvg.pcode ][1]:
			globAvg = proapp.calc.products[ calcAvg.pcode ][1][ intRole ]
			delta = round(100 * (orgAvg - globAvg) / globAvg, 3)
		products[ calcAvg.pcode ] = {'org': orgAvg, 'glob': globAvg, 'delta': delta}
	#
	return products


# ========================================================================
# Get organization product weights per domain
# ========================================================================
def org_prod_weights(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	domain = auth['user'].domain
	if domain.origin == 'office365':
		pcodes = ['outlook', 'onedrive', 'oevents']
	else:
		pcodes = ['calendar', 'docs', 'gmail', 'hangouts']#, 'sms', 'call'
	#
	if domain.conf_system != '' and not domain.conf_system is None:
		pcodes.append(domain.conf_system)
	#
	if domain.phone_system == 'broadsoft':
		pcodes.append('broadsoft')
	elif domain.phone_system == 'ringcentral':
		pcodes.append('ringc_calls')
		pcodes.append('ringc_sms')
	elif domain.phone_system == 'vbc':
		pcodes.append('vbc')
	#
	crm_modules = ('000000'+bin(domain.crm_modules)[2:])[::-1]
	if domain.crm_system == 'zoho':
		zoho_prods = ['zleads', 'zinvoices', 'zaccounts', 'ztasks', 'zevents', 'zcalls']
		for i in range(0, 6):
			if crm_modules[i] == '1':
				pcodes.append(zoho_prods[i])
	elif domain.crm_system == 'salesforce':
		sfprods = ['sfleads_n', 'salesforce_n', 'ch_messages', 'ch_activts', 'sfcalls', 'sfints']
		for i in range(0, 6):
			if crm_modules[i] == '1':
				pcodes.append(sfprods[i])
	elif domain.crm_system == 'prosperworks':
		pcodes.append('pwleads')
		pcodes.append('pwoprtunts')
	elif domain.crm_system == 'sugarCRM':
		scprods = ['sugar_calls', 'sugar_meets', 'sugar_opps', 'sugar_leads']
		for i in range(0, 4):
			if crm_modules[i] == '1':
				pcodes.append(scprods[i])
	#
	data = {}
	for pcode in pcodes:
		data[ proapp.calc.prod_dic[pcode] ] = {'org': proapp.calc.get_prod_weight(auth['user'].domain_id, pcode), 'default': proapp.calc.products[ proapp.calc.prod_dic[pcode] ][3]}
	return HttpResponse(json.dumps(data), content_type = 'application/json')


# ========================================================================
# Save organization product weightages
# ========================================================================
def save_org_weights(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	from .models import Org_weight
	#
	if 'delete' in request.POST and request.POST['delete'] == 'all':
		recs = Org_weight.objects.filter(domain_id=auth['user'].domain_id)
		for rec in recs:
			cacheKey = 'prod-weight-'+str(auth['user'].domain_id)+'-'+proapp.calc.dorp_dic[rec.pcode]
			proapp.cache.delete(cacheKey)
			rec.delete()
	else:
		for key in request.POST:
			tmp = key.split(':')
			if len(tmp) == 2:
				cacheKey = 'prod-weight-'+str(auth['user'].domain_id)+'-'+proapp.calc.dorp_dic[tmp[1]]
				rec = Org_weight.objects.filter(pcode=tmp[1], domain_id=auth['user'].domain_id)
				if len(rec) == 0:
					rec = Org_weight(pcode=tmp[1], domain_id=auth['user'].domain_id, weight=request.POST[key])
				else:
					rec = rec[0]
					rec.weight = request.POST[key]
					proapp.cache.delete(cacheKey)
				rec.save()
				#
				proapp.cache.add(cacheKey, int(request.POST[key]), 48*3600)
	#
	return HttpResponse(json.dumps({'message': 'saved'}), content_type = 'application/json')


# ========================================================================
# Save organization product baselines
# ========================================================================
def save_org_baselines(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])
	#
	from .models import Baseline
	#
	if 'delete' in request.POST and request.POST['delete'] == 'all':
		recs = Baseline.objects.filter(domain_id=auth['user'].domain_id)
		for rec in recs:
			cacheKey = 'baseline-'+str(auth['user'].domain_id)+'-'+str(rec.role)+'-'+proapp.calc.dorp_dic[rec.pcode]
			proapp.cache.delete(cacheKey)
			rec.delete()
	else:
		for key in request.POST:
			tmp = key.split(':')
			if len(tmp) == 2 and tmp[1] in proapp.calc.dorp_dic:
				cacheKey = 'baseline-'+str(auth['user'].domain_id)+'-'+str(tmp[0])+'-'+proapp.calc.dorp_dic[tmp[1]]
				rec = Baseline.objects.filter(role=tmp[0], pcode=tmp[1], domain_id=auth['user'].domain_id)
				if len(rec) == 0:
					rec = Baseline(role=tmp[0], pcode=tmp[1], domain_id=auth['user'].domain_id, baseline=request.POST[key])
				else:
					rec = rec[0]
					rec.baseline = request.POST[key]
					proapp.cache.delete(cacheKey)
				rec.save()
				#
				proapp.cache.add(cacheKey, float(request.POST[key]), 48*3600)
	#
	return HttpResponse(json.dumps({'message': 'saved'}), content_type = 'application/json')


# ========================================================================
# Manager alert settings
# 	to be used in alert settings form
# 	requested in an Ajax call
# ========================================================================
def alert_settings(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	if request.method=='POST':
		# ==================================================================
		# Parse JSON request body
		# ==================================================================
		received_json_data = json.loads(request.body)

		# ==================================================================
		# Save Settings
		# ==================================================================
		auth['user'].alert_below_yesterday = 1 if received_json_data['enabled_alert_below'] else 0
		auth['user'].alert_below_period = 1 if received_json_data['enabled_alert_below_period'] else 0
		auth['user'].alert_inactive_yesterday = 1 if received_json_data['enabled_alert_inactive'] else 0
		auth['user'].alert_inactive_period = 1 if received_json_data['enabled_alert_inactive_period'] else 0
		auth['user'].alert_new_employee = 1 if 'enabled_alert_new_employee' in received_json_data and received_json_data['enabled_alert_new_employee'] else 0
		auth['user'].save()
		return HttpResponse(json.dumps({'result': 'success'}), content_type = 'application/json')

	# =====================================================================
	# Write JSON to the response
	# =====================================================================
	return HttpResponse(json.dumps({
		'enabled_alert_below': True if auth['user'].alert_below_yesterday == 1 else False,
		'enabled_alert_below_period': True if auth['user'].alert_below_period == 1 else False,
		'enabled_alert_inactive': True if auth['user'].alert_inactive_yesterday == 1 else False,
		'enabled_alert_inactive_period': True if auth['user'].alert_inactive_period == 1 else False,
		'enabled_alert_new_employee': True if auth['user'].alert_new_employee == 1 else False}), content_type = 'application/json')


# ========================================================================
# Conference Call settings
# 	to be used in conference call settings form
# 	requested in an Ajax call
# ========================================================================
def turbobridge_settings(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	domain = auth['user'].domain
	details = {}
	# ==============================================================================
	# Validation of turbobridge credentials which are already on the db and also
	# to make sure that the credentials the user input are valid
	# ==============================================================================
	def validate_turbobridge(method,details):
		tdata = []
		tvalid = False
		if (details['conf_admin'] == '' and details['conf_password'] == '') or (details['conf_admin'] is None and details['conf_password'] is None):
			tdata = {'error': 'credentials-not-set'}
		else:
			# ==================================================================
			# Get bridge information from TurboBridge
			# ==================================================================
			try:
				bridges = requests.post(
					'http://api.turbobridge.com/Bridge',
					json={'request': {
							'authAccount': {
								'email': details['conf_admin'],
								'password': details['conf_password'],
								'partnerID': details['conf_partner'],
								'accountID': details['conf_account']
							},
							'requestList': [{'getBridges': {}}],
							'outputFormat': 'json'
						}}).content
				bridges = json.loads(bridges)
				if 'responseList' in bridges:
					tvalid = True
					if method == 'POST':
						return tvalid
					bridges = bridges['responseList']['requestItem'][0]['result']['bridge']
					for bridge in bridges:
						tdata.append({
							'name': bridge['name'],
							'tollNumber': bridge['tollNumber'],
							'sipURI': bridge['sipURI'],
							'activeFlag': bridge['activeFlag'],
							'conferenceID': bridge['conferenceID'],
							'pin': bridge['pin'],
							'createdDate': bridge['createdDate']})

				else:
					tdata = bridges
			except KeyError:
				tdata = {'error': 'Authentication Failure'}

		if method == 'POST':
			return tvalid
		else:
			return tdata
	##
	# =====================================================================
	# Receive JSON data - Save settings
	# =====================================================================
	if request.method == 'POST':
		received_json_data = json.loads(request.body)
		# ==================================================================
		# Save bridge assignments
		# ==================================================================
		if 'bridges' in received_json_data:
			for id in received_json_data['bridges']:
				employee = Employee.objects.get(id=id)
				employee.conf_id = received_json_data['bridges'][id]
				if employee.conf_id == 'null':
					employee.conf_id = ''
				employee.save()
			return HttpResponse(json.dumps({'result': 'success'}), content_type='application/json')
		# ==================================================================
		# Save Settings
		# ==================================================================
		else:
			domain.conf_system = details['conf_system'] = 'turbobridge'  # received_json_data['system']
			domain.conf_partner = details['conf_partner'] = received_json_data['partner']
			domain.conf_account = details['conf_account'] = received_json_data['account']
			domain.conf_admin = details['conf_admin'] = received_json_data['admin']
			if received_json_data['password'] != '[ENCRYPTED]':  # Not actually encrypted
				domain.conf_password = details['conf_password'] = received_json_data['password']

			valid = validate_turbobridge('POST',details)
			if valid:
				domain.save()
				return HttpResponse(json.dumps({'result': 'success'}), content_type='application/json')
			else :
				return HttpResponse(json.dumps({'result': 'failure'}), content_type='application/json')

	# =====================================================================
	# Fetch all user accounts on users domain
	# =====================================================================
	if auth['user'].role == 80:
		users = Employee.objects.filter(domain_id=auth['user'].domain_id, role__gt=0, status__gt=0)
	else:
		users = Employee.objects.filter(domain_id=auth['user'].domain_id, role__gt=0, status__gt=0, manager=auth['user'])

	employees = {}
	for user in users:
		employees[user.id] = {'fullname': user.fullname, 'role': user.role, 'conf_id': user.conf_id}

	details = {'conf_admin' : domain.conf_admin,
			'conf_password' : domain.conf_password,
			'conf_partner' : domain.conf_partner,
			'conf_account' : domain.conf_account}
	data = validate_turbobridge('GET',details)

	# =====================================================================
	# Write JSON to the response
	# =====================================================================
	settings = {
		'system': 'turbobridge',
		'partner': domain.conf_partner,
		'account': domain.conf_account,
		'admin': domain.conf_admin,
		'password': '[ENCRYPTED]'}
	return HttpResponse(json.dumps({'settings': settings, 'bridges': data, 'employees': employees}), content_type = 'application/json')


# Ringcentral settings Salesforce
# 	to be used in Ringcentral settings form
# 	requested in an Ajax call
# ========================================================================
def ringcentral_settings(request):
	# # =====================================================================
	# # Authorize the request
	# # =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	domain = auth['user'].domain

	#
	# # =====================================================================
	# # Receive JSON data - Save settings
	# # =====================================================================
	if request.method == 'POST':
		received_json_data = json.loads(request.body)
		try:
			domain.phone_system = received_json_data['system']
			domain.save()
		except KeyError:
			pass  # A temporary fix to avoid the key error.

		# ==================================================================
		# Save bridge assignments
		# ==================================================================
		if 'accounts' in received_json_data:
			for id in received_json_data['accounts']:
				employee = Employee.objects.get(id=id)
				employee.phone_system_id = received_json_data['accounts'][id]
				if employee.phone_system_id == 'null':
					employee.phone_system_id = ''
				employee.save()
			return HttpResponse(json.dumps({'result': 'success'}), content_type='application/json')

	# =====================================================================
	# Fetch all user accounts on users domain
	# =====================================================================
	if auth['user'].role == 80:
		users = Employee.objects.filter(domain_id=auth['user'].domain_id, role__gt=0, status__gt=0)
	else:
		users = Employee.objects.filter(domain_id=auth['user'].domain_id, role__gt=0, status__gt=0, manager=auth['user'])

	employees = {}
	for user in users:
		employees[user.id] = {'fullname': user.fullname, 'email': user.email, 'role': user.role, 'ringcentral_id': user.phone_system_id}

	# Obtaining data from ringcentral
	data = []
	try:
		# get the access token through token-service
		token_request = requests.get(token_url_rc + str(domain.id))
		rc_data = json.loads(token_request.text)
		admin_id = rc_data['owner_id']
		access_token = rc_data['access_token']
		if access_token:
			url = 'https://platform.ringcentral.com/restapi/v1.0/account/~/extension'
			headers = {'Accept': 'application/json', 'Content-Type': 'application/json',
					   'Authorization': 'Bearer ' + access_token}
			k = requests.get(url, headers=headers)
			if k.status_code == 200:
				response_data = k.json()
				try:
					for record in response_data['records']:
						if record['status'] != 'Unassigned':
							data.append({
								'id': record['id'],
								'extensionNumber': record['extensionNumber'],
								'name': record['name'] if 'name' in record else '',
								'email': record['contact']['email'] if 'email' in record['contact'] else '',
								'admin':  record['permissions']['admin']['enabled'] if 'admin' in record['permissions'] else ''})
				except Exception as error:
					data = {'error': 'Authentication Failure'}
					print('ringcentral-settings-error: ' + repr(error))

			else:
				data = {'error': 'Authentication Failure'}
		else:
			data = {'error': 'credentials-not-set'}

	except KeyError:
		data = {'error': 'Authentication Failure'}
	except ValueError:
		data = {'error': 'credentials-not-set'}

	# =====================================================================
	# Write JSON to the response
	# =====================================================================
	settings ={
		'user_id': '',
		'system': domain.phone_system,
		'password': '[ENCRYPTED]'}
	return HttpResponse(json.dumps({'settings': settings, 'accounts': data, 'employees': employees}), content_type = 'application/json')# 	return HttpResponse(json.dumps({'settings': settings, 'accounts': data, 'employees': employees}),content_type='application/json')


# ===========================================================

# Vbc settings Salesforce
# 	to be used in Ringcentral settings form
# 	requested in an Ajax call
# ========================================================================
def vbc_settings(request):
	# # =====================================================================
	# # Authorize the request
	# # =====================================================================
	auth = proapp.views._authorize(request)
	# #if 'user' not in auth and 'redirect' in auth:
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	domain = auth['user'].domain

	#
	# # =====================================================================
	# # Receive JSON data - Save settings
	# # =====================================================================
	if request.method == 'POST':
		received_json_data = json.loads(request.body)
		try:
			domain.phone_system = received_json_data['system']
			domain.save()
		except KeyError:
			pass  # A temporary fix to avoid the key error.

		# ==================================================================
		# Save bridge assignments
		# ==================================================================
		if 'accounts' in received_json_data:
			for id in received_json_data['accounts']:
				employee = Employee.objects.get(id=id)
				employee.phone_system_id = received_json_data['accounts'][id]
				if employee.phone_system_id == 'null':
					employee.phone_system_id = ''
				employee.save()
			return HttpResponse(json.dumps({'result': 'success'}), content_type='application/json')

	# =====================================================================
	# Fetch all user accounts on users domain
	# =====================================================================
	if auth['user'].role == 80:
		users = Employee.objects.filter(domain_id=auth['user'].domain_id, role__gt=0, status__gt=0)
	else:
		users = Employee.objects.filter(domain_id=auth['user'].domain_id, role__gt=0, status__gt=0, manager=auth['user'])

	employees = {}
	for user in users:
		employees[user.id] = {'fullname': user.fullname, 'email': user.email, 'role': user.role, 'ringcentral_id': user.phone_system_id}

	# Obtaining data from ringcentral
	data = []
	try:
		# get the access token through token-service
		token_request = requests.get(token_url_vbc + str(domain.id))
		rc_data = json.loads(token_request.text)
#		rc_data = eval("{u'access_token': u'4f7e3df2-a4bf-3db1-975a-474d339b0b85'}")
#		print('===========================')
		#
		access_token = rc_data['access_token']
		if access_token:
			headers = {'accept': 'application/json',
					   'authorization': 'Bearer '+access_token}
			#
			url = 'https://api.vonage.com/t/vbc.prod/general/v1/appserver/rest/user/null'
			res = requests.get(url, headers=headers)
			user = json.loads(res.text)
			#
			try:
				url = 'https://api.vonage.com/t/vbc.prod/general/v1/appserver/rest/account/'+str(user['accountId'])+'/userKeywordSearch'
				res = requests.get(url, headers=headers)
				#
				if res.status_code == 200:
					users = json.loads(res.text)
					#try:
					for record in users['results']:
#						print(record)
#						print('===========================')
						if len(record['extensions']) > 0:
							data.append({
								'id': record['userName'],
								'extensionNumber': (record['extensions'][0]['directNumber']+' ['+record['extensions'][0]['extensionNumber']+']') if 'directNumber' in record['extensions'][0] else record['extensions'][0]['extensionNumber'],
								'name': record['firstName']+' '+record['lastName'],
								'email': record['email'],
								'admin':  ('ACCOUNT_ADMINISTRATOR' in record['roleMemberships'])
							})
					#except Exception as error:
					#	data = {'error': 'Authentication Failure'}
					#	print('ringcentral-settings-error: ' + repr(error))

				else:
					data = {'error': 'Authentication Failure'}
			except Exception as error:
				data = {'error': 'Authentication Failure'}
				print('ringcentral-settings-error: ' + res.text)
		else:
			data = {'error': 'credentials-not-set'}

	except KeyError:
		data = {'error': 'Authentication Failure'}
	except ValueError:
		data = {'error': 'credentials-not-set-2'}

	# =====================================================================
	# Write JSON to the response
	# =====================================================================
	settings ={
		'user_id': '',
		'system': domain.phone_system,
		'password': '[ENCRYPTED]'}
	return HttpResponse(json.dumps({'settings': settings, 'accounts': data, 'employees': employees}), content_type = 'application/json')


# ========================================================================
# Conference Call settings
# 	to be used in conference call settings form
# 	requested in an Ajax call
# ========================================================================
def broadsoft_settings(request):
	import proapp.products.broadsoft

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	domain = auth['user'].domain

	# =====================================================================
	# Receive JSON data - Save settings
	# =====================================================================
	if request.method=='POST':
		received_json_data = json.loads(request.body)
		try:
			domain.phone_system = 'broadsoft'
			domain.save()
		except KeyError:
			pass

		# ==================================================================
		# Save bridge assignments
		# ==================================================================
		if 'bridges' in received_json_data:
			for id in received_json_data['bridges']:
				employee = Employee.objects.get(id=id)
				received_json_data['bridges'][id] = received_json_data['bridges'][id].split(':')
				if employee.broadsoft_admin_id != received_json_data['bridges'][id][0] or employee.broadsoft_userid != received_json_data['bridges'][id][1]:
					if received_json_data['bridges'][id][0] == 'null':
						employee.broadsoft_userid = ''
						employee.broadsoft_admin_id = ''
						employee.save()
					else:
						employee.broadsoft_admin_id = received_json_data['bridges'][id][0]
						employee.broadsoft_userid = received_json_data['bridges'][id][1]
						employee.save()
						#print(tmp)
						tmp = proapp.products.broadsoft.subscribe(request, employee.broadsoft_admin, received_json_data['bridges'][id][1])

		# ==================================================================
		# Save Settings
		# ==================================================================
		else:
			found_ids = []
			# Iterate received group admin credential set
			for bridge in received_json_data:
				if bridge['id'] == 'new':
					# Add new BS credential record
					admin_rec = Broadsoft_admin(domain=domain, broadsoft_xsi=bridge['xsiface'], broadsoft_userid=bridge['userid'],
										broadsoft_password=bridge['password'], is_valid=0)
					admin_rec.save()
					found_ids.append(admin_rec.id)
				else:
					# Find existing BS credential record
					admin_rec = Broadsoft_admin.objects.filter(domain=domain, id=bridge['id'])
					if len(admin_rec) == 1:
						found_ids.append(bridge['id'])
						# Update credential record
						admin_rec[0].broadsoft_xsi = bridge['xsiface']
						admin_rec[0].broadsoft_userid = bridge['userid']
						if bridge['password'] != '[ENCRYPTED]':	# Not actually encrypted
							admin_rec[0].broadsoft_password = bridge['password']
						admin_rec[0].is_valid = 0
						admin_rec[0].save()
			#
			# Delete removed records
			to_delete = Broadsoft_admin.objects.filter(domain=domain).exclude(id__in=found_ids)
			for rec in to_delete:
				to_dissociate = Employee.objects.filter(broadsoft_admin=rec)
				for rec2 in to_dissociate:
					rec2.broadsoft_admin_id = None
					rec2.broadsoft_userid = None
					rec2.save()
				rec.domain_id = None
				rec.broadsoft_userid = None
				rec.save()
				rec.delete()

		return HttpResponse(json.dumps({'result': 'success'}), content_type = 'application/json')

	# =====================================================================
	# Fetch all user accounts on users domain
	# =====================================================================
	if auth['user'].role == 80:
		users = Employee.objects.filter(domain_id=domain.id, role__gt=0, status__gt=0)
	else:
		users = Employee.objects.filter(domain_id=domain.id, role__gt=0, status__gt=0, manager=auth['user'])

	employees = {}
	for user in users:
		employees[user.id] = {'fullname': user.fullname, 'email': user.email, 'role': user.role, 'broadsoft': {'admin': user.broadsoft_admin_id, 'userid': user.broadsoft_userid}}

	#
	settings = {}
	admins = Broadsoft_admin.objects.filter(domain=domain)
	for admin in admins:
		if admin.is_valid == -1:
			settings[admin.id] = {'xsiface': admin.broadsoft_xsi, 'userid': admin.broadsoft_userid, 'error': 'credentials-wrong'}
		else:
			settings[admin.id] = {'xsiface': admin.broadsoft_xsi, 'userid': admin.broadsoft_userid}

	# =====================================================================
	# Write JSON to the response
	# =====================================================================
	return HttpResponse(json.dumps({'settings': settings, 'employees': employees}), content_type = 'application/json')


def broadsoft_bridges(request):
	import proapp.calc, xml.etree.ElementTree
	from xml.etree.ElementTree import ParseError
	# =====================================================================
	# Fetch all user id of Broadsoft Group
	# =====================================================================
	admin = Broadsoft_admin.objects.filter(broadsoft_userid=request.GET['userid'])[0]
	user_id = str(admin.broadsoft_userid)
	bridges = []
	try:
		pagination_size = 50 # we prefer to have 200, but broadsoft only sends 50 records even we request more.
		result = requests.get('https://xsp2.telesphere.com/com.broadsoft.xsi-actions/v2.0/user/'+admin.broadsoft_xsi+'@voip.tnltd.net/directories/Group?results=' + str(pagination_size),
			headers={'Authorization': 'Basic '+base64.b64encode(user_id+':'+str(admin.broadsoft_password))})
		result = result.content
		#
		if result == '':
			bridges = {'error': 'credentials-wrong'}
			admin.is_valid = -1
			admin.save()
		else:
			result = xml.etree.ElementTree.fromstring(result)

			if result.tag == '{http://schema.broadsoft.com/xsi}ErrorInfo':
				raise Exception(result.find('{http://schema.broadsoft.com/xsi}summaryEnglish').text)

			start_index = int(result.find('{http://schema.broadsoft.com/xsi}startIndex').text)
			logging.debug("broadsoft bridges start index : "+str(start_index))
			total_records = int(result.find('{http://schema.broadsoft.com/xsi}totalAvailableRecords').text)
			logging.debug("broadsoft bridges total records : " + str(total_records))
			pages = total_records/pagination_size

			itter = 1
			while True:
				_populate_broadsoft_bridges(bridges, result, admin, user_id)
				if itter <= pages:
					start_index += pagination_size
					logging.debug("broadsoft bridges start index : " + str(start_index))
					result = requests.get(
						'https://xsp2.telesphere.com/com.broadsoft.xsi-actions/v2.0/user/' + admin.broadsoft_xsi + '@voip.tnltd.net/directories/Group?start=' + str(start_index) + '&results=' + str(pagination_size),
						headers={'Authorization': 'Basic ' + base64.b64encode(
							user_id + ':' + str(admin.broadsoft_password))})
					result = result.content
					result = xml.etree.ElementTree.fromstring(result)
					itter += 1
				else:
					logging.debug("exiting broadsoft bridge pagination")
					break

			admin.is_valid = 1
			admin.save()
	except ParseError:
		bridges = {'error': 'credentials-wrong'}
		admin.is_valid = -1
		admin.save()
	#
	except requests.ConnectionError:
		bridges = {'error': 'credentials-wrong'}

	except Exception as e:
		bridges = {'error': e.message}
		admin.is_valid = -1
		admin.save()
		logging.error(e.message)

	# =====================================================================
	# Write JSON to the response
	# =====================================================================
	return HttpResponse(json.dumps(bridges), content_type = 'application/json')


def _populate_broadsoft_bridges(bridges, result, admin, user_id):
	for c1 in result:
		if c1.tag == '{http://schema.broadsoft.com/xsi}groupDirectory':
			for c2 in c1:
				emp_object = {}
				for c3 in c2:
					if c3.tag == '{http://schema.broadsoft.com/xsi}userId':
						emp_object['userid'] = c3.text + '@' + user_id.split('@')[1]
					if c3.tag == '{http://schema.broadsoft.com/xsi}firstName':
						emp_object['firstName'] = c3.text
					if c3.tag == '{http://schema.broadsoft.com/xsi}lastName':
						emp_object['lastName'] = c3.text
					if c3.tag == '{http://schema.broadsoft.com/xsi}groupId':
						emp_object['groupId'] = c3.text
					if c3.tag == '{http://schema.broadsoft.com/xsi}additionalDetails':
						for c4 in c3:
							if c4.tag == '{http://schema.broadsoft.com/xsi}emailAddress':
								emp_object['email'] = c4.text
				if 'email' not in emp_object:
					emp_object['email'] = ''
				emp_object['name'] = emp_object['firstName']
				if 'lastName' in emp_object and emp_object['firstName'] != emp_object['lastName']:
					emp_object['name'] += ' ' + emp_object['lastName']
				if emp_object['firstName'] != 'Hunt Group':
					bridges.append(
						{'admin': admin.id, 'userid': emp_object['userid'], 'email': emp_object['email'].lower(),
						'name': emp_object['name']})
	return bridges


# ========================================================================
# CRM settings
# 	to be used in CRM settings form
# 	requested in an Ajax call
# ========================================================================
def crm_settings(request):
	# =====================================
	# Validation Process for CRM systems
	# =====================================
	def validate_crm(method, crm_details):
		# =====================================================================
		# Fetch all user accounts on users domain
		# =====================================================================
		valid = False
		data = []
		try:
			if crm_details['system'] == 'zoho' and not (crm_details['token'] == '' or crm_details['token'] == None):
				# ==================================================================
				# Get bridge information from TurboBridge
				# ==================================================================
				accounts = requests.get('https://crm.zoho.com/crm/private/json/Users/getUsers?authtoken=' +
										crm_details['token'] + '&type=AllUsers').content
				try:
					accounts = json.loads(accounts)
					if 'users' in accounts:
						valid = True
						if method == 'POST':
							return valid
						accounts = accounts['users']['user']
						for account in accounts:
							if account['confirm'] == 'true':
								data.append({
									'id': account['id'],
									'name': account['content'],
									'role': account['role'],
									'profile': account['profile']})
						data = sorted(data, key=operator.itemgetter('name'))

					else:
						data = accounts['response']
				except KeyError:
					data = {'error': KeyError}
				except ValueError:
					data = {'error': 'Authentication Failure. Invalid Credentials.'}
			elif crm_details['system'] == 'prosperworks' and not (crm_details['token'] == '' or crm_details['token'] == None):
				# ==================================================================
				# Get user information from Prosperworks
				# ==================================================================
				url = 'https://api.prosperworks.com/developer_api/v1/users'
				headers = {'Content-Type': 'application/json',
						   'X-PW-UserEmail': crm_details['crm_admin'],
						   'X-PW-Application': 'developer_api',
						   'X-PW-AccessToken': crm_details['token']}
				r = requests.get(url, headers=headers)
				accounts = r.json()
				if 'status' in accounts:
					data = accounts['message']
				elif 'error' in accounts:
					data = accounts['error']
				else:
					valid = True
					if method == 'POST':
						return valid
					for account in accounts:
						data.append({
							'id': account['id'],
							'name': account['name'],
							'role': account['email']})
					data = sorted(data, key=operator.itemgetter('name'))
			elif crm_details['system'] == 'salesforce' and crm_details['admin'] is not None and crm_details[
				'password'] is not None and crm_details['token'] is not None:
				from simple_salesforce import Salesforce
				from simple_salesforce.exceptions import SalesforceRefusedRequest
				salesforce_userid = crm_details['admin']
				salesforce_password = crm_details['password']
				token_id = crm_details['token']
				url = 'https://login.salesforce.com/services/oauth2/token'
				payload = {'grant_type': 'password',
						   'client_id': '3MVG9szVa2RxsqBZcaYqIR5nZljlWeftvhfUfdS.9S9xsc81cBSxDTR2WfNQMKzN3bhZoCT.ZzWY1D61FihRT',
						   'client_secret': '7790752435074856812',
						   'username': salesforce_userid,
						   'password': salesforce_password + '{}'.format(token_id)
						   }
				headers = {'Accept-Charset': 'UTF-8'}
				try:
					r = requests.post(url, data=payload, headers=headers)
					response_data = r.json()
					if r.status_code == 200:
						valid = True
						if method == 'POST':
							return valid
						employees = Employee.objects.filter(domain_id=domain.id).exclude(crm_id__isnull=True)
						cacheKey = str(domain.id) + '-sf-matched-users-list'
						if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
							cache_records = memcache.get(cacheKey)
						else:
							cache_records = cache.get(cacheKey)
						if cache_records is None:
							sf = Salesforce(instance_url=response_data['instance_url'],session_id=response_data['access_token'])
							user_type = 'Standard'
							records = sf.query("SELECT Name, Email, Id, Username FROM User WHERE UserType = '%s'"%user_type)
							cache_records = records['records']
						for record in cache_records:
							data.append({
								'id': record['Id'],
								'name': record['Name'],
								'email': record['Email'],
								'userid': record['Username']})

					else:
						data = {'error': 'Authentication Failure. Invalid Credentials.'}


				except SalesforceRefusedRequest:
					data = {'error': 'Your password has expired or the REST API is not enabled for your company'}
				except KeyError:
					return HttpResponse(json.dumps({'error': response_data}), content_type='application/json')
			elif method == 'POST' and crm_details['system'] == 'sugarCRM' and crm_details['crm_admin'] is not None and crm_details['password'] is not None and crm_details['instance'] is not None:
				crm_details['domain_id'] = domain.id

				try:
					check_sugar_credntls = Sugar_settings.objects.filter(sugar_instance = crm_details['instance'], sugar_admin=crm_details['crm_admin']).exclude(domain_id = domain.id)
					if len(check_sugar_credntls) > 0:
						valid = False
					else:
						token_request = requests.post(token_url, data=crm_details)
						token_data = json.loads(token_request.text)
						valid = token_data['valid']

				except ConnectionError or ValueError:
					valid = False

		except KeyError:
			data = {'error':'CRM system not installed'}

		if method == 'POST':
			return valid
		else:
			return data

	# =====================================================================
	# Authorize the request
	# =====================================================================\
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	domain = auth['user'].domain

	crm_details = {}  # To hold data to be called on to 'def validate_crm(..)
	# =====================================================================
	# Receive JSON data
	# =====================================================================
	if request.method=='POST':
		received_json_data = json.loads(request.body)

		if 'accounts' in received_json_data:
			for id in received_json_data['accounts']:
				employee = Employee.objects.get(id=id)
				employee.crm_id = received_json_data['accounts'][id]
				if employee.crm_id == 'null' or employee.crm_id == '':
					employee.crm_id = None
				employee.save()
			valid = True
		# ==================================================================
		# Save Modules
		# ==================================================================
		elif 'modules' in received_json_data:
			domain.crm_modules = received_json_data['modules']
			domain.save()
			valid = True
		# ==================================================================
		# Save Settings
		# ==================================================================
		else:
			if domain.crm_system != received_json_data['system']:
				domain.crm_modules = 0
				employees = Employee.objects.filter(domain=domain)
				for employee in employees:
					employee.crm_id = None
					employee.save()
			domain.crm_system = received_json_data['system']
			#
			if received_json_data['system'] != 'sugarCRM' and received_json_data['token'] != '[ENCRYPTED]':
				domain.crm_token  = received_json_data['token']
			if received_json_data['system'] == 'salesforce':
				domain.salesforce_userid = received_json_data['admin']
				if received_json_data['password'] != '[ENCRYPTED]':
					domain.salesforce_password = received_json_data['password']
			elif received_json_data['system'] == 'prosperworks':
				domain.crm_admin = received_json_data['crm_admin']
			elif received_json_data['system'] == 'sugarCRM':
				domain.crm_admin = received_json_data['crm_admin']
			valid = validate_crm('POST', received_json_data)

			if valid:
				domain.save()
				if domain.crm_system == 'salesforce':
					utcnow = datetime.datetime.utcnow()
					utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
					urlpart = '/get-salesforce-users-page/?'
					logging.debug("delete cache before saving the credentails : " + str(domain.title))
					task_name = 'get-salesforce-users-' + domain.title.replace('.', '-dot-') + '-on-' + utcnow
					if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
						taskqueue.add(url=urlpart + 'domain=' + str(domain.id) + '&y=0', method='GET',
									  queue_name='salesforce-users-cron',
									  name=task_name, countdown=3)
				elif domain.crm_system == 'sugarCRM':
					memcache.delete(str(domain.id) + '-sugarcrm-matched-users-list')
					utcnow = datetime.datetime.utcnow()
					utcnow = str((utcnow - datetime.datetime(1970, 1, 1)).total_seconds()).replace('.', '-')
					urlpart = '/get-sugarcrm-users-page/?'
					logging.debug("delete cache before saving the credentails : " + str(domain.title))
					task_name = 'get-sugarcrm-users-' + domain.title.replace('.', '-dot-') + '-on-' + utcnow
					if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
						taskqueue.add(url=urlpart + 'domain=' + str(domain.id) + '&y=0&next_offset=0', method='GET',
									  queue_name='sugarcrm-users-cron',
									  name=task_name, countdown=3)
		if valid:
			return HttpResponse(json.dumps({'result': 'Success'}),content_type='application/json')
		else:
			return HttpResponse(json.dumps({'error': 'The credentials you have entered are either invalid or are already in use'}), content_type='application/json')

	# =====================================================================
	# Load Data from the Database
	# =====================================================================
	data = []
	if domain.crm_system != '' and domain.crm_system != 'sugarCRM':
		crm_details['system'] = domain.crm_system
		crm_details['token']  = domain.crm_token
		if domain.crm_system == 'salesforce':
			crm_details['admin']  = domain.salesforce_userid
			crm_details['password']  = domain.salesforce_password
		if domain.crm_system == 'prosperworks':
			crm_details['crm_admin']  = domain.crm_admin
		if domain.crm_system == 'zoho' or domain.crm_system == 'prosperworks':
			crm_details['modules']  = domain.crm_modules

		data = validate_crm('GET', crm_details)     # Validation of CRM credentials of the db
		if type(data) is HttpResponse:
				data = data.getvalue()
	elif domain.crm_system == 'sugarCRM':
		cacheKey = str(domain.id) + '-sugarcrm-matched-users-list'
		sugar = Sugar_settings.objects.filter(domain_id=domain.id)[0]
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			cache_records = memcache.get(cacheKey)
		else:
			cache_records = cache.get(cacheKey)
		#
		if cache_records is not None:
			for record in cache_records:
				data.append({
					'id': record['id'],
					'name': record['name'],
					'email': record['email']})
		if cache_records is None:
			sugar_data = products.sugarCRM.load_settings(domain.id)
			data = sugar_data['data']

	if auth['user'].role == 80:
		users = Employee.objects.filter(domain_id=auth['user'].domain_id, role__gt=0, status__gt=0)
	else:
		users = Employee.objects.filter(domain_id=auth['user'].domain_id, role__gt=0, status__gt=0, manager=auth['user'])

	employees = {}
	for user in users:
		employees[user.id] = {'fullname': user.fullname, 'email': user.email, 'role': user.role, 'crm_id': user.crm_id}

	settings = {
		'system': domain.crm_system,
		'token': '[ENCRYPTED]',
		'crm_modules' :  domain.crm_modules,
		'crm_admin' :  domain.crm_admin if domain.crm_system == 'zoho' or domain.crm_system == 'prosperworks' or domain.crm_system == 'sugarCRM' else domain.salesforce_userid,
		'password' : '[ENCRYPTED]' if domain.crm_system == 'salesforce' or domain.crm_system == 'sugarCRM' else '',
		'crm_instance':sugar.sugar_instance if domain.crm_system == 'sugarCRM' else '',
		'sugar_key_name':sugar.sugar_client_key_name if domain.crm_system == 'sugarCRM' else ''}

	return HttpResponse(json.dumps({'settings': settings, 'accounts': data, 'employees': employees}), content_type = 'application/json')


# ========================================================================
# Returns organization settings in JSON
# 	to be used in settings form
# 	requested in an Ajax call
# ========================================================================
def organization_settings(request):
	import proapp.calc, proapp.cron

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or ('redirect' in auth and auth['redirect'] != '/dashboard/'):
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	# =====================================================================
	# Read the domain record for the current user
	# =====================================================================
	#domain = Domain.objects.get(id=auth['user'].domain_id)
	onboard = Onboard.objects.using('replica').select_related('domain').get(domain=auth['user'].domain_id)
	alerts = Employee.objects.using('replica').values('alert_below_yesterday', 'alert_below_period','alert_inactive_yesterday', 'alert_inactive_period','alert_new_employee').get(id=auth['user'].id)
	latest = Statistic.objects.using('replica').filter().extra(where=['employee_id IN (SELECT id FROM proapp_employee WHERE domain_id = '+str(auth['user'].domain_id)+')']).exclude(score__lt=0).values('date').order_by('-date')[0]

	# =====================================================================
	# Get timezone offset
	# =====================================================================
	tzoffset = 60 * proapp.calc.get_timezone_offset(datetime.datetime.utcnow(), onboard.domain.timezone)

	# =====================================================================
	# Write JSON to the response
	# =====================================================================
	return HttpResponse(json.dumps({'daystart': onboard.domain.daystart, 'dayend': onboard.domain.dayend, 'workingdays': onboard.domain.workingdays, 'roles': proapp.cron.role_dic,
								'timezone': onboard.domain.timezone, 'tzoffset': tzoffset, 'since': onboard.started_on.strftime('%Y-%m-%d'), 'upto': latest['date'],
								'alert_below_yesterday':alerts['alert_below_yesterday'], 'alert_below_period':alerts['alert_below_period'], 'alert_inactive_yesterday':alerts['alert_inactive_yesterday'],
								'alert_inactive_period':alerts['alert_inactive_period'], 'alert_new_employee':alerts['alert_new_employee'], 'show_details': onboard.domain.show_details, 'domain': onboard.domain.title,
								'myself': {
									'id':auth['user'].id,
									'level': auth['user'].level,
									#'admin': auth['user'].admin,
									'role': auth['user'].role,
									'email': auth['user'].email,
									'status': auth['user'].status,
									'picture': auth['user'].picture,
									'fullname': auth['user'].fullname
								},
								'correlations_enabled':onboard.domain.nlp_enabled,
								'subscription':{
									'type': onboard.domain.subscription_type,
									'volume':onboard.domain.subscription_volume,
									'features': {
										'crm': onboard.domain.crm_enabled,
										'telco': onboard.domain.telco_enabled,
										'nlp': onboard.domain.nlp_enabled,
										'orgbl': onboard.domain.orgbaseline_enabled
									}
								}
							}), content_type = 'application/json')


# ========================================================================
# Save organization settings for the domain: working days/time, timezone
# 	Receives JSON data in an Ajax POST request
# ========================================================================
def update_organization(request):

	# =====================================================================
	# Authorize the request
	# =====================================================================
	auth = proapp.views._authorize(request)
	if 'user' not in auth or 'redirect' in auth:
		return HttpResponseRedirect(request.build_absolute_uri('/')[:-1]+auth['redirect'])

	if request.method == 'POST':
		# ==================================================================
		# Parse JSON request body
		# ==================================================================
		received_data = json.loads(request.body)

		# ==================================================================
		# Open the organization record to edit
		# ==================================================================
		domain = Domain.objects.get(id=auth['user'].domain_id)

		# ==================================================================
		# Copy attributes from JSON to database object
		# ==================================================================
		domain.daystart = received_data.get('daystart')
		domain.dayend = received_data.get('dayend')
		domain.workingdays = received_data.get('workingdays')
		domain.timezone = received_data.get('timezone')
		domain.show_details = received_data.get('show_details')

		# ==================================================================
		# Write (commit) the database object
		# ==================================================================
		domain.save()

	# =====================================================================
	# Acknowledge the client
	# =====================================================================
	schedule_file_creating_for_domain(auth['user'].domain_id) ##-# Creating Files

	return HttpResponse('database updated')

# ========================================================================
#		EOF
# ========================================================================
