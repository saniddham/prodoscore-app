
import os, re, json, base64, hashlib, logging
from difflib import SequenceMatcher

from django.db import transaction
from django.db.utils import OperationalError
from django.http import HttpResponse
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from httplib2 import Http
from django.db.models import Q
if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	from google.appengine.api import memcache, taskqueue
else:
	from django.core.cache import cache

from .models import Employee, Detail
from .nlpmodels import Nlp_entity, Nlp_entity_attribs, Nlp_entity2detail
import proapp.cron, proapp.cache

from oauth2client.service_account import ServiceAccountCredentials
from nlp_process_text import romove_special_patterns, remove_specific_words
import traceback
# ========================================================================
# Load Client Secret file for the service account authorization
# ========================================================================
CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secret.json')

# ========================================================================
# Analyze text on details for each stat
# ========================================================================
flattenFields = {
	#'gmail': ['Subject', 'To'],#, 'From'
	#'docs': ['Subject', 'To'],
	'turbobridge': ['name', 'location', 'fromName', 'fromNumber', 'toNumber', 'transcript'],
	'ringc_calls': ['transcript'],
	'ztasks': ['Subject', 'Description', 'Related To'],
	'zleads': ['Lead Source Description'],#, 'First Name', 'Last Name', 'Name', 'Email', 'Company', 'Phone', 'Lead ID'
	'zaccounts': ['Industry', 'Website'],#'Email Address', 'Phone',
	'zevents': ['Subject'],
	'zcalls': ['Subject', 'Call Owner', 'Related To'],
	'sugar_calls': ["name", "description",  "status",  "direction"],
	'sugar_leads': ["full_name",  "lead_source", "status",  "account_name", "email1"],
	'sugar_opps': ["name", "opportunity_type", "lead_source", "account_name"],
	'sugar_meets': ["name",  "description", "status"]
}

pNumPattern = re.compile(r'''			# don't match beginning of string, number can start anywhere
						(\d{3})	# area code is 3 digits (e.g. '800')
						\D*		# optional separator is any number of non-digits
						(\d{3})	# trunk is 3 digits (e.g. '555')
						\D*		# optional separator
						(\d{4})	# rest of number is 4 digits (e.g. '1212')
						\D*		# optional separator
						(\d*)		# extension is optional and can be any number of digits
						$		# end of string
						''', re.VERBOSE)

MIN_NAME_LEN = 3
MAX_NAME_LEN = 50
MAX_EMAIL_LEN = 40
CAHRTO_RM = ["'", '"', '\'', '\t', '\r', '\n', ',' ]
EMAIL_RECOG_PATTERN = r"[a-zA-Z][a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.][a-zA-Z0-9-.]+"
NON_PERSON_EMAIL_RECOG = ['reply-', 'google.profiles']


def extract_person_name(name):
	try:
		for chrm in CAHRTO_RM:
			name = ' '.join(w.replace(chrm, '') for w in list(filter(None, name.split() )) )
		validate = all(name_part.isalpha() for name_part in name.split())
		assert validate == True, 'not  valied name'
		assert len(name) > MIN_NAME_LEN, 'name len less than 3'
		return name
	except Exception as e:
		proapp.cron.log ('NAME DROPPED: %s : ERROR: %s' % (str(name), str(e)), 'info')
		return ''


def extract_email(text_content):
	try:
		email = re.findall(EMAIL_RECOG_PATTERN, text_content)[0]
		assert len(email) < MAX_EMAIL_LEN, 'unusual len in email'
		for string_rm in NON_PERSON_EMAIL_RECOG:
			assert string_rm not in email, 'NON person email'
		return email
	except Exception as e:
		proapp.cron.log ('EMAIL DROPPED FROM: %s :ERROR: %s' % (str(text_content), str(e)), 'info')
	return ''


def is_same_domain(detail, email):
	try:
		proapp.cron.log ('EMP DOM: %s' % detail.statistic.employee.domain.title.lower(), 'info')
		return detail.statistic.employee.domain.title.lower() in email.split('@')[1].lower()
	except Exception as e:
		proapp.cron.log ('email not in domain %s :ERROR: %s' % (email, str(e)), 'info')
		return False


def analyze_cron(request):
	"""
	schedule other functions -> Main cron entry point
	:param request:
	:return:
	"""
	# Fetch 100 yet-not-analyzed detail records from the database
	details = Detail.objects.using('replica').extra(
		where=["nlp_pversion < 6 OR nlp_pversion IS NULL"]).order_by('-id')
	details.query.set_limits(low=0, high=90)

	# details = Detail.objects.using('replica').filter(
	# 	(Q(nlp_pversion__lt=6) | Q(nlp_pversion__isnull=True)) &
	# 	Q(statistic__date__gt='2019-05-01')
	# )
	# details.query.set_limits(low=0, high=90)
	#
	# if all(param in request.GET for param in ['fromdate', 'domain', 'product', 'TRUE']):
	# 	details = Detail.objects.filter(
	# 		statistic__employee__domain=request.GET['domain'],
	# 		statistic__date__gte=request.GET['fromdate'],
	# 		statistic__product=request.GET['product']
	# 	)
	# 	details.query.set_limits(low=0, high=90)

	i = 0
	for detail in details:
		try:
			detailRec = Detail.objects.filter(id=detail.id)[0]
			detailRec.nlp_pversion = 6
			detailRec.save()
			#
			# Schedule a task
			urlpart = '/nlp-analyze-task/?id='+str(detail.id)
			namepart = 'nlp-analyze-v6-'+detail.statistic.product+'-of-'+str(detail.id)
			if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
				taskqueue.add(url=urlpart, method='GET', queue_name='nlp', name=namepart, countdown=i*5)
			else:
				proapp.cron.log(urlpart, 'info')
				analyze_task(detail.id)
			i += 1
		except Exception:
			tb = traceback.format_exc()
			proapp.cron.log ("\n\n ERROR TASK %s\n\n" % str(tb), 'error')
			logging.error('Probably a TombstonedTaskError: "nlp-analyze-v6-'+detail.statistic.product+'-of-'+str(detail.id)+'"')

	return HttpResponse('{"result": "scheduled"}', content_type = 'application/json')

# -------------BLOCK FOR  TESTING ---------------
# def analyze_task(id):
# 	try:
# 		detail = Detail.objects.filter(id=id)[0]
# 		print detail.data
# 	except Exception:
# 		print "no data"
# 		detail = []
# -------------END TEST BLOCK--------------------
#
def analyze_task(request):
	"""Analize a single detail record to identify entities and correlations"""

	if 'id' not in request.GET:
		return HttpResponse('{"error": "id required"}', content_type = 'application/json')

	try:
		detail = Detail.objects.filter(id=request.GET['id'])[0]
	except IndexError:
		proapp.cron.log('No detail record-detail id: %s' % request.GET['id'], 'info')
		return HttpResponse('{"error": "record not found"}', content_type = 'application/json')

	people = []
	cgoods = []
	organizations = []

	# Parse data and flatten based on product
	flattened = flatten_and_initial_nlp(detail, people, cgoods, organizations)
	try:
		proapp.cron.log ('1. flattened: %s' % str(flattened), 'info')
	except:
		pass
	# =====================================================================
	#	Get string analyzed from Cloud NLP for entities
	# =====================================================================
	if flattened != '':

		try:	# This is shameful to have to do this - but no other way to get around this
			flattened = flattened.decode('unicode_escape').encode('ascii', 'ignore')
		except:
			flattened = flattened.encode('ascii', 'ignore').decode("utf8")
		flattened = flattened.replace('&lt;', '').replace('&gt;', '').replace('&nbsp;', ' ').replace('_', ' ')
		flattened = ' '.join(flattened.split())
		#
		if len(flattened) > 1000:
			flattened = flattened[:1000]

		alreadyprocessed = False
		if detail.nlp_entities != '':
			try:
				res = eval(detail.nlp_entities)
			except:
				try:
					res = json.loads(detail.nlp_entities)
				except:
					res = {}
			#
			if type(res) is dict and 'entities' in res:
				alreadyprocessed = True
				proapp.cron.log ('alreadyprocessed %s' % alreadyprocessed, 'info')

		if alreadyprocessed == False:
			#	First, check in MemCached if we have got this presynaptic already NLProcessed
			res = None
			cachekey = hashlib.sha512(flattened).hexdigest()
			if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
				res = memcache.get(cachekey)
			#
			if res is None:
				# Establish authorization for NLP API service account - Use the HTTP authentication and open Cloud NLP API
				credentials = ServiceAccountCredentials.from_json_keyfile_name(CLIENT_SECRETS, scopes=['https://www.googleapis.com/auth/cloud-platform'])
				http_auth = credentials.authorize(Http())
				language = build('language', 'v1', http=http_auth)
				#
				try:
					res = language.documents().annotateText(
						body={
							"document": {
								"content": flattened,
								"type": "plain_text"
							},
							"encodingType": "UTF8",
							"features": {
								"extractEntities": True,
								"extractDocumentSentiment": True,
								"extractSyntax": False
							}
						}
					).execute()
					proapp.cron.log ('GOOGLE NLP: %s' % res, 'info')
				except HttpError as error:
					res = {
						'entities': [],
						'error': error._get_reason(),
						'documentSentiment': {'magnitude': 0, 'score': 0}
					}
				#
				#	Store in MemCached for 32 days for future use - Google NLP may have learnt new words in future after 32 days let's say..
				if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
					memcache.add(cachekey, res, 32*24*3600)
			#
			detail.nlp_entities = res

	else:
		res = {'entities': [], 'error': 'Empty presynaptic'}
		detail.nlp_entities = res

	detail.nlp_presynaptic = flattened
	detail.save()

	if flattened != '':
		# Iterate Entities returned by G Cloud NLP
		prevEntity = False
		proapp.cron.log ('res[entities]: %s' % res['entities'], 'info')
		entities = defragment_entities(res['entities'], detail)

		for entity in entities:
			if 'mentions' in entity:
				del entity['mentions']
			if 'metadata' in entity:
				del entity['metadata']
			#
			if prevEntity != False and prevEntity['type'] == 'PERSON':
				try:
					email = extract_email(entity['name'])
					if email:
						prevEntity['email'] = email
				except:
					pass
			elif entity['type'] == 'ORGANIZATION':
				organizations.append(entity['name'])

			elif entity['type'] == 'CONSUMER_GOOD':
				cgoods.append(entity['name'])

			elif entity['type'] == 'PERSON':
				if entity['name'][0] == '@' or entity['name'][4:] == '.com':
					pass	# Avoid the ridiculousness of identifying domains names as people
				else:
					found = False
					for person in people:
						if 'name' in person:
							if person['name'] == entity['name']:
								found = True
							elif entity['name'].lower() in person['name'].lower():
								found = True
							elif person['name'].lower() in entity['name'].lower():
								person['name'] = entity['name']
								found = True

					person_name = extract_person_name(entity['name'])
					if not found and person_name:
						people.append({'name': entity['name']})
			else:
				prevEntity = entity


	# filter indentified people
	cowerkers = colleagues(detail.statistic.employee)

	# remove persons in same domain
	proapp.cron.log ('poeple list befor:\n %s' % people, 'info')
	for person in people[:]:
		if (
			('email' in person and is_same_domain(detail, person['email']))
				or ('email' in person and extract_email(person['email']) == '')
				or ('name' in person and extract_person_name(person['name'])=='')
				# or ('name' in person and person['name'] in [emp.fullname for emp in cowerkers ])
				or ('email' in person and person['email'] in [emp.email for emp in cowerkers])
		):
			people.remove(person)
	proapp.cron.log('poeple list after:\n %s' % people, 'info')

	proapp.cron.log('ORG:: %s' % organizations, 'info')
	processed_orgs = []
	for organization in organizations:
		processed_text = romove_special_patterns(organization)
		processed_text = remove_specific_words(processed_text)
		if processed_text:
			processed_orgs.append(processed_text)
	proapp.cron.log('ORG after process:: %s' % processed_orgs, 'info')

	# If only one organization found - assume everyone from that organization. consider matching email address (if email/domain known for domain)
	if len(organizations) == 1:
		for person in people:
			person['organization'] = organizations[0]

	with transaction.atomic():	# THIS LINE CAUSES a TransactionManagementError IN RARE CASES
		relate_entities(detail, detail.statistic.employee, people, processed_orgs)
		detail.nlp_pversion = 6
		detail.save()

	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		return HttpResponse(json.dumps({'result': 'ok'}), content_type = 'application/json')
	else:
		pass
		# try:
		# 	return HttpResponse(json.dumps({'input': flattened, 'product': detail.statistic.product, 'nlp': entities, 'pple': people, 'orgs': organizations, 'cgoods': cgoods}), content_type = 'application/json')
		# except Exception:
		# 	tb = traceback.format_exc()
		# 	proapp.cron.log ('Error: Analyze task: %s' % tb, 'error')


def flatten_and_initial_nlp(detail, people, cgoods, organizations):
	"""
	Parse data and flatten (to string -> NLP presynaptic) based on product
	:param detail:
	:param people:
	:param cgoods:
	:param organizations:
	:return:
	"""
	flattened = ''

	try:
		data = json.loads(detail.data)
	except:
		try:
			data = eval(detail.data)
		except:
			data = {}

	if detail.statistic.product == 'gmail':
		for kv in data:
			# Parse email to addreses to correlate to people
			if kv['name'] in ['To', 'Cc', 'Bcc']:
				kv['value'] =  list(filter(None, re.split('[>,]', kv['value'])))

				for person in kv['value']:
					person = person.split('<')
					try:
						if len(person) == 1:
							person_email = extract_email(person[0])
							if person_email:
								people.append({'email': person_email})

						elif len(person) == 2:
							person_name = extract_person_name(person[0])
							person_email = extract_email(person[1])
							if person_name and person_email:
								people.append({'name': person_name, 'email': person_email})

					except Exception:
						tb = traceback.format_exc()
						proapp.cron.log ('gmail failed: %s :: error: %s' % (str(person), str(tb)), 'info')

			elif kv['name'] == 'Subject':
				flattened += kv['value'].replace('Re', '').replace('Fwd', '').replace('FW', '').replace('ATTN', '').replace('*', '').replace(':', '').strip()+'.'

			elif kv['name'] == 'body':
				if type(kv['value']) is dict and 'data' in kv['value']:
					kv['value'] = kv['value']['data']
				body = base64.b64decode(kv['value'].replace('_', '/').replace('-', '+'))
				try:	# This is shameful to have to do this - but no other way to get around this
					body = body.encode('ascii', 'ignore').decode("utf8")
				except:
					body = body.decode("utf8").encode('ascii', 'ignore')
				# ----------------------------------------------
				if len(body) > 2048:
					body = body[:2048]
				# ----------------------------------------------
				flattened += ' '+re.sub('<[^<]+?>', ' ', body)

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'hangouts':
		if 'from' in data:
			people.append({'name': data['from'][0], 'email': data['from'][1]})
		if 'data' in data:
			flattened = base64.b64decode(data['data'].replace('_', '/').replace('-', '+'))

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'calendar':
		proapp.cron.log ('calendar data - %s' % data, 'info')
		if 'summary' in data:
			# organizations.append(str(data['summary']))
			flattened += ' '+data['summary']+'.'
		if 'location' in data:
			flattened += ' '+data['location']
		if 'description' in data:
			flattened += ' '+data['description'].replace('\n', ' ').replace('\r', ' ')
		#
		if 'attendees' in data:
			for attendee in data['attendees']:
				person = {'email': attendee['email']}
				if 'displayName' in attendee:
					person['name'] = attendee['displayName']
				people.append(person)

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'docs':
		proapp.cron.log ('docs data - %s' % data, 'info')
		flattened = detail.title
		if 'displayName' in data:
			flattened += ' '+data['displayName']
		elif 'name' in data:
			flattened += ' '+data['name']
		#
		if 'comment' in data:
			flattened += ': '+data['comment']
		#
		if 'content' in data:
			if data['content'] == False:
				pass
			elif len(data['content']) > 2048:
				flattened += ': '+data['content'][:2048]
			else:
				flattened += ': '+data['content']

	# =====================================================================
	elif detail.statistic.product == 'call':
		try:
			if data['type'] == 1:
				people.append({'phone': data['number']})
		except KeyError:
			logging.error("this is a KeyError for call + data['type']",data)
	#
	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'sms':
		try:
			if data['type'] == 1:
				people.append({'phone': data['number']})
			if data['content'] != False:
				flattened = data['content']
		except KeyError:
			logging.error("this is a KeyError for sms + data['type']",data)

	# =====================================================================
	elif detail.statistic.product == 'turbobridge':
		people.append({'name': data['fromName'], 'phone': data['fromNumber']})
		people.append({'name': data['name'], 'phone': data['toNumber']})
		for kv in data:
			if kv in flattenFields['turbobridge']:
				flattened += data[kv]+' '

	# =====================================================================
	elif detail.statistic.product == 'ringc_calls':
		#
		'''fromPhone = ''
		if 'extensionId' in data['from']:
			fromPhone += data['from']['extensionId']
		if 'extensionNumber' in data['from']:
			fromPhone += '+'+data['from']['extensionNumber']
		fromUser = {'name': data['from']['name']}
		if fromPhone != '':
			fromUser['phone'] = fromPhone
		people.append(fromUser)
		#
		toUser = {}
		if 'name' in data['to']:
			toUser['name'] = data['to']['name']
		if 'phoneNumber' in data['to']:
			toUser['phone'] = data['to']['phoneNumber']
		else:
			toPhone = ''
			if 'id' in data['extension']:
				toPhone += str(data['extension']['id'])
			if 'extensionNumber' in data['to']:
				toPhone += '+'+data['to']['extensionNumber']
			if toPhone != '':
				toUser['phone'] = toPhone
		people.append(toUser)'''
		#
		for kv in data:
			if kv in flattenFields['ringc_calls']:
				flattened += data[kv]+' '

	# =====================================================================
	elif detail.statistic.product == 'ztasks':
		for kv in data:
			if kv in flattenFields['ztasks']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'zleads':
		try:
			if 'First Name' in data and 'Last Name' in data:
				data['Name'] = data['First Name']+' '+data['Last Name']
			elif 'First Name' in data:
				data['Name'] = data['First Name']
			elif 'Last Name' in data:
				data['Name'] = data['Last Name']
			#
			person = {'name': data['Name']}
			if 'Phone' in data:
				person['phone'] = data['Phone'].replace('+', '').replace('-', '')
			if 'Email' in data:
				person['email'] = data['Email']
			people.append(person)
			#
			organizations.append(data['Company'])
		except KeyError:
			logging.error("this is a KeyError",data)
		for kv in data:
			if kv in flattenFields['zleads']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'zaccounts':
		zaccounts_address = {'Billing Street': '', 'Billing City': '', 'Billing State': ''}
		address = ''
		for key in data:
			if key in zaccounts_address:
				zaccounts_address[key] = data[key]
		flattened = 'Billing Address '+zaccounts_address['Billing Street']+' '+zaccounts_address['Billing City']+' '+zaccounts_address['Billing State']+' '
		for kv in data:
			if kv in flattenFields['zaccounts']:
				flattened += data[kv]+' '
		#
		person = {}
		try:
			if 'Phone' in data:
				person['phone'] = data['Phone'].replace('+', '').replace('-', '')
			if 'Email Address' in data:
				person['email'] = data['Email Address']
			people.append(person)
			#
			organizations.append(data['Account Name'])
		except KeyError:
			logging.error("this is a KeyError",data)

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'zevents':
		for kv in data:
			if kv in flattenFields['zevents']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'zcalls':
		for kv in data:
			if kv in flattenFields['zcalls']:
				flattened += data[kv]+' '

	# =====================================================================
	elif detail.statistic.product == 'ch_messages':
		try:
			flattened = data['Body']
		except KeyError:
			logging.error("this is a KeyError for ch_messages",data)
	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'ch_activts':
		try:
			flattened = data['CommentBody']
		except KeyError:
			logging.error("this is a KeyError for ch_activts", data)
	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'sugar_calls':
		for kv in data:
			if kv in flattenFields['sugar_calls']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'sugar_meets':
		for kv in data:
			if kv in flattenFields['sugar_meets']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'sugar_leads':
		for kv in data:
			if kv in flattenFields['sugar_leads']:
				flattened += data[kv]+' '

	# ---------------------------------------------------------------------
	elif detail.statistic.product == 'sugar_opps':
		for kv in data:
			if kv in flattenFields['sugar_opps']:
				flattened += data[kv]+' '

	# =====================================================================
	return flattened


def relate_entities(detail, employee, people, organizations):
	"""
	Relate entities back to detail records -> to database
	:param detail:
	:param employee:
	:param people:
	:param organizations:
	:return:
	"""
	# Iterare for organizations
	foundOrg = [False, 0]
	for organization in organizations:
		proapp.cron.log(organization, 'info')
		#	First see if the organization already there for this domain
		attribs = Nlp_entity_attribs.objects.filter(
			attrib='title').extra(
			where=['value LIKE %s AND nlp_entity_id IN (SELECT id FROM proapp_nlp_entity WHERE etype = \'organization\' AND domain_id = '+str(employee.domain_id)+')'],
			params=[organization.encode('ascii', 'ignore')]).order_by('-confidence')

		foundOrg = [False, 0]
		for attrib in attribs:
			match = SequenceMatcher(None, organization, attrib.value).ratio()
			if organization.lower() == attrib.value.lower() or match == 1:
				foundOrg = [attrib.nlp_entity, 1]
				attrib.confidence += 1
				attrib.save()
				break
			elif match > 0.95 and match > foundOrg[1]:
				foundOrg = [attrib.nlp_entity, match]

		proapp.cron.log ('foundOrg %s' % foundOrg, 'info')

		#	Insert new entity if no satisfactory match found
		if foundOrg[0] == False:
			entity = Nlp_entity(domain=employee.domain, etype='organization')
			entity.save()
			foundOrg[0] = entity
			try:
				attrib = Nlp_entity_attribs(
					nlp_entity=entity, confidence=1, attrib='title', value=organization)
				attrib.save()
				proapp.cron.log ("DATA SAVED %s" % organization, 'info')
			except OperationalError:
				proapp.cron.log('Invalid Char Sequence: ', 'info')
		else:
			entity = foundOrg[0]

		#	Connect the entity to stat-detail record
		conn = Nlp_entity2detail(
			detail=detail,
			nlpentity=entity,
			date=detail.statistic.date,
			domain=detail.statistic.employee.domain
		)
		conn.save()

	# ENTITY_START_DATE = '2019-05-10'
	# # exclude entities older than ENTITY_START_DATE
	# e2ds_of_newdata = Nlp_entity2detail.objects.filter(
	# 	date__gte=ENTITY_START_DATE).values_list('nlpentity', flat=True)

	# Iterare for people
	for person in people:
		proapp.cron.log ('saving data for: %s' % person, 'info')
		proapp.cron.log(json.dumps(person), 'info')
		foundName = [False, 0]
		foundEmail = [False, 0]
		foundPhone = [False, 0]

		#	First see if the person already there for this domain
		if 'name' in person and person['name'].strip():
			attribs = Nlp_entity_attribs.objects.filter(
				attrib='name',
				nlp_entity__domain=str(employee.domain_id),
				nlp_entity__etype='person',
				value__icontains=person['name'].encode('ascii', 'ignore')
			).order_by('-confidence')
			proapp.cron.log ('NEW attribs: %s' % len(attribs), 'info')
			for attrib in attribs:
				if person['name'].lower() == attrib.value.lower():
					foundName = [attrib.nlp_entity, 1, attrib.value]
					attrib.confidence += 1
					attrib.save()
					break

		if 'email' in person and person['email'].strip():
			attribs = Nlp_entity_attribs.objects.filter(
				attrib='email',
				nlp_entity__domain=str(employee.domain_id),
				nlp_entity__etype='person',
				value__icontains=person['email'].encode('ascii', 'ignore')
			).order_by('-confidence')

			for attrib in attribs:
				if person['email'].lower() == attrib.value.lower():
					foundEmail = [attrib.nlp_entity, 1, attrib.value]
					attrib.confidence += 1
					attrib.save()
					break

		if 'phone' in person and person['phone'].strip():
			attribs = Nlp_entity_attribs.objects.filter(attrib='phone').extra(
						where=['value LIKE %s AND nlp_entity_id IN (SELECT id FROM proapp_nlp_entity WHERE etype = \'person\' AND domain_id = '+str(employee.domain_id)+')'],
						params=[person['phone'].encode('ascii', 'ignore')]).order_by('-confidence')

			for attrib in attribs:
				match = SequenceMatcher(None, person['phone'], attrib.value).ratio()
				if person['phone'].lower() == attrib.value.lower():
					foundPhone = [attrib.nlp_entity, 1, attrib.value]
					attrib.confidence += 1
					attrib.save()
					break
				elif match > 0.95 and match > foundPhone[1]:
					foundPhone = [attrib.nlp_entity, match, attrib.value]

		#	If no trace was found - insert new
		if foundName[0] == False and foundEmail[0] == False and foundPhone[0] == False:
			entity = Nlp_entity(domain=employee.domain, etype='person')
			entity.save()

		# If found - link new attributes to that
		else:
			if foundEmail[0] != False:
				entity = foundEmail[0]

				if foundName[0] != False:
					if foundName[0].id != foundEmail[0].id:
						link_nlp_attribs(foundEmail[0], foundName[0])

				# if foundPhone[0] != False:
				# 	if foundName[0].id != foundPhone[0].id:
				# 		link_nlp_attribs(foundPhone[0], foundName[0])

			if foundName[0] != False:
				entity = foundName[0]
				#
				# if foundPhone[0] != False:
				# 	if foundEmail[0].id != foundPhone[0].id:
				# 		link_nlp_attribs(foundPhone[0], foundEmail[0])

			# elif foundPhone[0] != False:
			# 	entity = foundPhone[0]

		# If only one organization found - assume all people are from that
		if len(organizations) == 1:
			entity.p_entity = foundOrg[0]
			entity.save()

		# Add missing attributes to fill blanks of partial information
		if foundName[0] == False:
			if 'name' in person and person['name'].strip() != '':
				try:
					attrib = Nlp_entity_attribs(nlp_entity=entity, confidence=1, attrib='name', value=person['name'])
					attrib.save()
					proapp.cron.log ('\n attrib saved %s\n' % person['name'], 'info')
				except OperationalError:
					proapp.cron.log('Invalid Char Sequence: ', 'info')

		if foundEmail[0] == False:
			if 'email' in person and person['email'].strip() != '':
				attrib = Nlp_entity_attribs(nlp_entity=entity, confidence=1, attrib='email', value=person['email'])
				attrib.save()
				proapp.cron.log ('\n attrib saved %s\n' % person['email'], 'info')

		if foundPhone[0] == False:
			if 'phone' in person and person['phone'].strip() != '':
				attrib = Nlp_entity_attribs(nlp_entity=entity, confidence=1, attrib='phone', value=person['phone'])
				attrib.save()
				proapp.cron.log ('\n attrib saved %s\n' % person['phone'], 'info')

		conn = Nlp_entity2detail(detail=detail, nlpentity=entity, date=detail.statistic.date, domain=detail.statistic.employee.domain)
		conn.save()
		proapp.cron.log ('Nlp_entity2detail SAVED', 'info')
		#
		# Search for entities in database
		# If found, check if attributes match
		# Add missing attributes
		# If comflictng attributes are found, also keep those as varieties with occurence count to verify later
		# If not found; create new entities
		# Connect N:N with detail record on another DB table
		#
	return True


def link_nlp_attribs(pParent, nParent):
	"""
	Link attributes from a different entity
	:param pParent:
	:param nParent:
	:return:
	"""
	with transaction.atomic():
		attribs = Nlp_entity_attribs.objects.filter(nlp_entity=pParent)
		e2details = Nlp_entity2detail.objects.filter(nlpentity=pParent)
		for attrib in attribs:
			attrib.nlp_entity = nParent
			attrib.save()
		for e2detail in e2details:
			e2detail.nlpentity = nParent
			e2detail.save()
		pParent.domain_id = -1
		pParent.save()
		pParent.delete()


def defragment_entities(entities, detail):
	"""
	Often the G Cloud NLP entities are badly fragmented, and we need to consolidate them
	:param entities:
	:param detail:
	:return:
	"""
	for entity in entities:
		for en2 in entities:
			if entity in entities and en2 in entities and en2['type'] == entity['type']:
				if en2 == entity:
					pass
				elif en2['name'].lower() in entity['name'].lower():
					entity['salience'] = max(en2['salience'], entity['salience'])
					entities.remove(en2)
				elif entity['name'].lower() in en2['name'].lower():
					en2['salience'] = max(en2['salience'], entity['salience'])
					entities.remove(entity)

	for entity in entities[:]: # usig as on the fly object manipulation will corrupt the object
		if entity['type'] == 'PERSON':
			if 'mentions' in entity:
				if (entity['mentions'][0]['type'] != 'PROPER'
						or detail.statistic.product in ['docs', 'calendar', 'gmail', 'hangouts']):
					entities.remove(entity)
			else:
				entities.remove(entity)

	proapp.cron.log ("PROCESSED ENTS: %s" % entities, 'info')
	return entities


def colleagues(employee):
	"""
	List employee emails for the domain of the given user. Caches for 3 hours
	:param employee:
	:return:
	"""
	cachekey = 'colleagues-'+str(employee.domain_id)

	# Check on the cache
	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
		data = memcache.get(cachekey)
	else:
		data = cache.get(cachekey)

	if data is None:
		data = Employee.objects.using('replica').filter(domain_id=employee.domain_id)

		# Store in cache
		if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
			memcache.add(cachekey, data, 3*3600)
		else:
			cache.add(cachekey, data, 3*3600)

	return data

