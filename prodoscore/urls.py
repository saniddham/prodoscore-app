# Copyright 2015 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os

from django.conf.urls import include, url
from django.views.generic.base import RedirectView
from django.http import HttpResponse

#from django.contrib.auth import views
#from django.contrib import admin
#admin.autodiscover()

import proapp.auth
import proapp.views
import proapp.admin
import proapp.cron
import proapp.nlp
import proapp.emails
import proapp.settings
import proapp.api
import proapp.products.broadsoft

urlpatterns = [

    url(r'^$', proapp.views.index, name='home'),
    url(r'^settings/', proapp.views.settings_dashboard, name='settings-dashboard'),
    url(r'^dashboard/', proapp.views.employee_dashboard, name='employee-dashboard'),
    url(r'^dashboard-ajax/', proapp.views.ajax_dashboard, name='dashboard-ajax'),
    url(r'^dashboard-alerts-ajax/', proapp.views.ajax_alerts, name='dashboard-alerts-ajax'),
    url(r'^employee-ajax/', proapp.views.ajax_employee, name='employee-ajax'),
    url(r'^employees-ajax/', proapp.views.ajax_employees, name='employees-ajax'),
    url(r'^dashboard-detail/', proapp.views.ajax_details, name='dashboard-detail'),
    url(r'^dashboard-detail-data/', proapp.views.ajax_detail_data, name='dashboard-detail-data'),
    url(r'^dashboard-detail-data-raw/', proapp.views.ajax_detail_data_raw, name='dashboard-detail-data-raw'),
    url(r'^feedback/', proapp.views.feedback, name='feedback'),
    url(r'^set-flag', proapp.views.flag_stats, name='flag_stats'),
    url(r'^usage/', proapp.views.usage, name='usage'),
    url(r'^alerts/', proapp.views.alerts, name='alerts'),
    url(r'^correlations-ajax/', proapp.views.ajax_correlations, name='correlations-ajax'),
    url(r'^correlations-ajax-aggr/', proapp.views.ajax_correlation_aggr, name='correlations-ajax-aggr'),
    url(r'^correlations-ajax-details/', proapp.views.ajax_correlation_details, name='correlations-ajax-details'),
    url(r'^correlations-graph/', proapp.views.correlations_graph, name='correlations-graph'),
    #url(r'^emp-day-correlations/', proapp.views.emp_day_correlations, name='emp-day-correlations'),
    url(r'^failure-retry', proapp.cron.failure_retry, name='failure-retry'),
    url(r'^schedule-cron', proapp.cron.schedule_cron, name='schedule_cron'),
    url(r'^admin/schedule-task', proapp.cron.add_task_queue, name='schedule_task_'),
    url(r'^process-cron-prosperworks', proapp.cron.process_cron_prosperworks, name='process_cron_prosperworks'),
    url(r'^process-cron-android', proapp.cron.process_cron_android, name='process_cron_android'),
    url(r'^process-cron', proapp.cron.process_cron, name='process_cron'),
    url(r'^calc-org-baselines', proapp.cron.calc_org_baselines, name='calc_org_baselines'),
    url(r'^salesforce-cron', proapp.cron.salesforce_cron, name='salesforce_cron'),
    url(r'^schedule-docs-processing', proapp.cron.schedule_docs_processing, name='schedule-docs-processing'),
    url(r'^schedule-left-behinds', proapp.cron.schedule_tasks_for_unprocessed_stats, name='schedule-left-behinds'),
    url(r'^nlp-analyze-cron', proapp.nlp.analyze_cron, name='nlp_analyze_cron'),
    url(r'^nlp-analyze-task', proapp.nlp.analyze_task, name='nlp_analyze_task'),
    url(r'^onboarding-worker', proapp.cron.onboarding, name='onboarding'),
    url(r'^cron-stats', proapp.cron.cron_stats, name='cron_stats'),
    url(r'^broadsoft/events', proapp.products.broadsoft.rx_events, name='rx_broadsoft_events'),

    url(r'^turbobridge/fetch-transcripts', proapp.products.turbobridge.fetch_transcripts, name='fetch_transcripts'),
    url(r'^schedule-salesforce-users', proapp.cron.schedule_salesforce_users, name='schedule_salesforce_users'),
    url(r'^get-salesforce-users/', proapp.cron.get_salesforce_users, name='get_salesforce_users'),
    url(r'^get-salesforce-users-page/', proapp.cron.get_salesforce_users_page, name='get_salesforce_users_page'),
    url(r'^schedule-sugarcrm-users', proapp.cron.schedule_sugarcrm_users, name='schedule_sugarcrm_users'),
    #url(r'^get-sugarcrm-users/', proapp.cron.get_sugarcrm_users, name='get_sugarcrm_users'),
    url(r'^get-sugarcrm-users-page/', proapp.cron.get_sugarcrm_users_page, name='get_sugarcrm_users_page'),

    url(r'^mail-reports', proapp.emails.mail_reports, name='mail_reports'),
    url(r'^mail-one-report', proapp.emails.mail_one_report, name='mail_one_report'),
    url(r'^disconnect-products', proapp.views.disconnect_products, name='disconnect_products'),

    url(r'^copy_hangouts_msg_to_title', proapp.cron.copy_hangouts_msg_to_title, name='copy_hangouts_msg_to_title'),
    url(r'^doc_content_diff_migration', proapp.cron.doc_content_diff_migration, name='doc_content_diff_migration'),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
    # to INSTALLED_APPS to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    # url(r'^accounts/login/$', views.login, {'template_name': 'login.html'}, name='login'),
    # url(r'^logout/$', views.logout, {'next_page': 'home'}, name='logout'),

    url(r'^admin/schedule-task', proapp.cron.add_task_queue, name='schedule_task_'),
    url(r'^admin/domain-details/$', proapp.admin.domain_details, name='domain_details'),
    url(r'^admin/domain-users/$', proapp.admin.domain_users, name='domain_users'),
    url(r'^admin/on-progress/$', proapp.admin.on_progress, name='on_progress'),
    url(r'^admin/average-calc/$', proapp.admin.average_calc, name='average_calc'),
    url(r'^admin/emp-day-score/$', proapp.admin.emp_day_score, name='emp_day_score'),
    url(r'^admin/feedback/$', proapp.admin.feedback, name='feedback'),
    url(r'^admin/feedback-details/$', proapp.admin.feedback_details, name='feedback_details'),
    #url(r'^admin/usage/$', proapp.admin.usage, name='usage'),
    #url(r'^admin/usage-details/$', proapp.admin.usage_details, name='usage_details'),
    url(r'^admin/impersonate/$', proapp.admin.impersonate, name='impersonate'),
    url(r'^admin/test-emails', proapp.admin.test_emails, name='test_emails'),
    url(r'^admin/memcached', proapp.admin.memcached, name='memcached'),
    url(r'^admin/synccdn', proapp.admin.synccdn, name='synccdn'),
    #url(r'^admin/taskqd', proapp.admin.failed_task_details, name='taskqd'),
    #url(r'^admin/taskq', proapp.admin.get_failed_tasks, name='taskq'),
    url(r'^admin/', proapp.admin.index, name='admin'),

    url(r'^api/sub-roles/', proapp.api.subroles, name='sub-roles'),
    url(r'^organization-settings/', proapp.settings.organization_settings, name='organization-settings'),
    url(r'^role-baselines/', proapp.settings.role_baselines, name='role_baselines'),
    url(r'^org-prod-weights/', proapp.settings.org_prod_weights, name='org_prod_weights'),
    url(r'^save-org-baselines/', proapp.settings.save_org_baselines, name='save_org_baselines'),
    url(r'^save-org-weights/', proapp.settings.save_org_weights, name='save_org_weights'),
    url(r'^employee-settings/', proapp.settings.employee_settings, name='employee-settings'),
    url(r'^timezone/', proapp.settings.timezone_request, name='timezone'),
    url(r'^update-employee/', proapp.settings.update_employee, name='update-employee'),
    url(r'^register-leaves', proapp.settings.register_leaves, name='register-leaves'),
    url(r'^update-organization/', proapp.settings.update_organization, name='update-organization'),
    url(r'^turbobridge-settings/', proapp.settings.turbobridge_settings, name='turbobridge-settings'),
    url(r'^broadsoft-settings/', proapp.settings.broadsoft_settings, name='broadsoft-settings'),
    url(r'^broadsoft-bridges/', proapp.settings.broadsoft_bridges, name='broadsoft-bridges'),
    url(r'^salesforce-settings/', proapp.settings.crm_settings, name='salesforce-settings'),
    url(r'^sugarcrm-settings/', proapp.settings.crm_settings, name='sugarcrm-settings'),
    url(r'^ringcentral-settings/', proapp.settings.ringcentral_settings, name='ringcentral-settings'),
    url(r'^vbc-settings/', proapp.settings.vbc_settings, name='vbc-settings'),
    url(r'^alert-settings/', proapp.settings.alert_settings, name='alert-settings'),
    url(r'^crm-settings/', proapp.settings.crm_settings, name='crm-settings'),

    url(r'^register/progress', proapp.auth.register_progress, name='register-progress'),
    url(r'^register/update-employees/', proapp.auth.update_employees, name='update_employees'),
    url(r'^register/time-settings/', proapp.auth.time_settings, name='time_settings'),
    url(r'^register/upload-xlsx/', proapp.auth.upload_user_file, name='upload_xlsx'),
    url(r'^register/add-employees/', proapp.auth.add_employees, name='add_employees'),
    url(r'^register/schedule-onboard-tasks-for-past-90-days/', proapp.cron.schedule_onboard_tasks, name='schedule_onboard_tasks'),
    url(r'^register/get-accounts/', proapp.auth.get_accounts, name='get_accounts'),
    url(r'^register/get-domains/', proapp.auth.get_domains, name='get_domains'),
    url(r'^register/select-domain/$', proapp.auth.select_domain, name='select_domain'),
    url(r'^register/assign-managers/', proapp.auth.assign_managers, name='assign_managers'),
    url(r'^register/', proapp.auth.register, name='register'),
    url(r'^process-file/', proapp.auth.process_user_file, name='process-file'),
    #
    url(r'^google-auth/', proapp.auth.google_auth, name='google_auth'),
    url(r'^microsoft-auth/', proapp.auth.microsoft_auth, name='microsoft_auth'),
    url(r'^rc-auth/', proapp.auth.rc_auth, name='rc_auth'),
    url(r'^vbc-auth/', proapp.auth.vbc_auth, name='vbc_auth'),
    url(r'^sign-in/', proapp.auth.sign_in, name='sign-in'),
    url(r'^sign-out/', proapp.auth.sign_out, name='sign-out'),
    url(r'^limbo/', proapp.auth.limbo, name='limbo'),

    url(r'^robots\.txt$', lambda x: HttpResponse('disallow all')),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico', permanent=True)),

    url(r'^org-prod-cron', proapp.cron.org_prodoscore_cron, name='org_prod_cron'),
    url(r'^dashboard-details-cron', proapp.cron.dashboard_details_cron, name='dashboard_details_cron'),
    url(r'^dashboard-load-details-ajax', proapp.views.ajax_dashboard_load_details, name='ajax_dashboard_load_details'),
    url(r'^dashboard-employee-list-ajax', proapp.views.ajax_dashboard_employee_list, name='ajax_dashboard_employee_list'),
    url(r'^dashboard-correlation-list-ajax', proapp.views.ajax_dashboard_correlation_list, name='ajax_dashboard_correlation_list'),
    url(r'^schedule-gateway', proapp.views.schedule_gateway, name='schedule_gateway'),
    # (r'^static/(?P<path>.*)$', 'django.views.static.serve',
    # {'document_root': os.path.join(os.path.dirname(__file__), 'static')
    # }),
]
