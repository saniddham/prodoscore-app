var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var gcmq = require('gulp-group-css-media-queries');

var src = 'static/static/',
    des = 'static/static/';

var bootstrapSass = { in: './node_modules/bootstrap-sass/'
};

var bourbon = { in: './node_modules/bourbon/'
};

var scss = {
    in: src + 'scss/*.scss',
    out: des + 'css/',
    watch: src + 'scss/**/*',
    sassOpts: {
        outputStyle: 'expanded',
        precison: 3,
        errLogToConsole: true,
        includePaths: [bootstrapSass.in + 'assets/stylesheets', bourbon.in + 'app/assets/stylesheets', './node_modules/mathsass/dist']
    }
};

gulp.task('sass', function() {
    return gulp.src(scss.in)
        .pipe(sourcemaps.init())
        .pipe(sass(scss.sassOpts))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(scss.out));
});

gulp.task('groupCSS', ['sass'], function() {
    return gulp.src(scss.out + 'style.css')
        .pipe(gcmq())
        .pipe(gulp.dest(scss.out));
});

gulp.task('cleanCSS', ['groupCSS'], function() {
    return gulp.src(scss.out + 'style.css')
        .pipe(cleanCSS({
            advanced: true,
            aggressiveMerging: false,
            keepSpecialComments: 1
        }))
        // .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(scss.out));
});


gulp.task('styles', ['sass', 'groupCSS', 'cleanCSS']);

gulp.task('default', ['sass'], function() {
    gulp.watch(scss.watch, ['sass']);
});
